//
//  Encyption.swift
//  C00Peeps
//
//  Created by OSX on 15/07/16.
//  Copyright © 2016 SOTSYS011. All rights reserved.
//

import Foundation


class Encryption {
    
    //MARK: - Create Singleton Instance

    class var sharedInstance: Encryption {
        struct Static {
            static let instance = Encryption()
            //static let token: dispatch_once_t = 0
        }
        
        //Initialize AES 128 bit Key & IV
            initializeKeys()
        
        
        return Static.instance
    }

    static var dataRandomKey : Data?
    static var dataRandomiv : Data?
    
   
    
    //MARK: - Initialize AES 128 bit key & IV
    class func initializeKeys(){
        
        let bytesCount = 16 // number of bytes for 128 bit key
        
        //128 bit random key
        var randomKey = [UInt8](repeating: 0, count: bytesCount) // array to hold randoms bytes
        
        // Gen random bytes
        SecRandomCopyBytes(kSecRandomDefault, bytesCount, &randomKey)
        
        //128 bit random key
        var randomiv = [UInt8](repeating: 0, count: bytesCount) // array to hold randoms bytes
        
        // Gen random bytes
        SecRandomCopyBytes(kSecRandomDefault, bytesCount, &randomiv)
        
        dataRandomKey = Data(bytes: UnsafePointer<UInt8>(randomKey), count: bytesCount)
        
        dataRandomiv = Data(bytes: UnsafePointer<UInt8>(randomiv), count: bytesCount)
    }
    
    //MARK: - Encrypt data with AES & RSA keys
    //func encryptData(data:NSData, RSAPublicKey:String) ->NSData{
    func encryptData(_ data:Data) ->Data{
        
        //Encrypt data with AES key
        
        let encryptedData = (data as NSData).aes128EncryptedData(withKey: Encryption.dataRandomKey, iv: Encryption.dataRandomiv)

        /*
        let pubKeyBData = NSData(base64EncodedString: RSAPublicKey)
        
        Initialize RSA object
        let obj = RsaHelper()
        
        Encrypt AES key with RSA public key
        let encryptedAESKey = obj.encryptAESKey(Encryption.dataRandomKey!, pubKeyBData)
        
        Encrypt AES IV with RSA public key
        let encryptedAESIV = obj.encryptAESKey(Encryption.dataRandomiv!, pubKeyBData)
        */
        
//        print("encryptedData count: " + (String(describing: encryptedData?.count)))
//        print("dataRandomKey Count: " + (String(describing: Encryption.dataRandomKey?.count)))
//        print("dataRandomiv Count: " + (String(describing: Encryption.dataRandomiv?.count)))
        
        //building whole message
        let fullMessage = NSMutableData()
        fullMessage.append(Encryption.dataRandomKey!) //128 bytes
        fullMessage.append(Encryption.dataRandomiv!) //128 bytes
        fullMessage.append(encryptedData!) //content bytes
        
         //print("After Append FullMessageCount: " + (String(fullMessage.length)))
        
        return fullMessage as Data
    }
    
    //MARK: - Decrypt data with AES & RSA keys
    func decryptData(_ fullMessage:Data) ->Data{
        
        //Splitting key, IV & data from full message
        
        var position = 0
        var length = 16
        
        //print("Encrypted FullMessage Count: " + (String(fullMessage.count)))
        
        //Fetch first 128 bytes of AES encrypted key from full message
        let keydata = fullMessage.subdata(in: Range.init(NSMakeRange(position, length))!)
        print("keydata count: " + (String(keydata.count)))
        
        position = length + position
        length = 16
        
        //print("Position: " + (String(position)))
        //print("Length: " + (String(length)))
        
        //Fetch second 128 bytes of AES encrypted IV from full message
        //let IVdata = fullMessage.subdata(in: position..<length)
        let IVdata = fullMessage.subdata(in: Range.init(NSMakeRange(position, length))!)
        //print("IVdata count: " + (String(IVdata.count)))
        
        position = length + position
        length = fullMessage.count - position
        
        //print("Position: " + (String(position)))
        //print("Length: " + (String(length)))
        
        //Fetch remaining bytes of AES encrypted content
         let contentData = fullMessage.subdata(in: Range.init(NSMakeRange(position, length))!)
        //print("contentData count: " + (String(contentData.count)))
        
        /*
        //Decrypt, encrypted AES key with RSA private key
        let obj = RsaHelper()
        
        //replace "username" with actual unique username
        let myPrivateTag = Utilities.getUserDetails().username + "privatetag"
        
        let decryptedAESKey = obj.decryptAESKey(keydata, privateTag:myPrivateTag)
        
        //Decrypt, encrypted AES IV with RSA private key
        let decryptedAESIV = obj.decryptAESKey(IVdata, privateTag:myPrivateTag)
        
        //Decrypt, encrypted content using AES key & IV
        let decryptedData = contentData.AES128DecryptedDataWithKey(decryptedAESKey, iv: decryptedAESIV)
        */
        
        let decryptedData = (contentData as NSData).aes128DecryptedData(withKey: keydata, iv: IVdata)
        //print("decryptedData count: " + (String(describing: decryptedData?.count)))
        return decryptedData!
    }
    
    //MARK: - Check RSA keys
    func createRSAPublicKey (_ username:String) ->String{
        
        //This method should be call before login API and 
        //we should send publickey in login API
        
        //Initialize RSA object
        let obj = RsaHelper()
        
        //replace "username" with actual unique username
        let myPublicTag = username + "publictag"
        //let myPublicTag = "publictag"
        let myPrivateTag = username + "privatetag"
        
        let publicKeyString = obj.initializeKeyPair(myPublicTag, myPrivateTag)
        
        printCustom(publicKeyString!)
        
        return publicKeyString!
    }
}
