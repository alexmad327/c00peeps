//
//  RsaHelper.h
//  RsaEncryption
//
//  Created by ozgur sahin on 24/01/15.
//

#import <Foundation/Foundation.h>
#import "CryptoUtil.h"
#import "Base64.h"
#import "KeyHelper.h"

@interface RsaHelper : NSObject

-(NSString*)initializeKeyPair:(NSString*)publicTag : (NSString*)privateTag;
-(NSData*) encryptAESKey:(NSData*)AESKeyData :(NSData*) publickey;
-(NSData*) decryptAESKey:(NSData*)encryptedAESKeyData PrivateTag:(NSString*)privateTag;

@property (nonatomic, strong) NSString *publicKey;
@end
