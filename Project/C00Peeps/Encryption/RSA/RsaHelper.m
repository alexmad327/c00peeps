//
//  RsaHelper.m
//  RsaEncryption
//
//  Created by Ozgur Sahin on 24/01/15.
//

#import "RsaHelper.h"

@implementation RsaHelper

-(NSString*)initializeKeyPair:(NSString*)publicTag : (NSString*)privateTag
{
    
    //creating key pair
    [self createKeyPairWithTag:publicTag privateTag:privateTag];
    
    KeyHelper* keyHelper =[[KeyHelper alloc]init];
    NSData* pubkeyData=  [keyHelper getPublicKeyBitsWithtag:publicTag];
    
    NSString *publicKey = [pubkeyData base64Encoding];
    
    //SecKeyRef publicKey = [CryptoUtil loadRSAPublicKeyRefWithAppTag:@"publicTagB"];
    
    if(publicKey == nil)
    {
        //creating key pair
        [self createKeyPairWithTag:publicTag privateTag:privateTag];
        
        KeyHelper* keyHelper =[[KeyHelper alloc]init];
        NSData* pubkeyData=  [keyHelper getPublicKeyBitsWithtag:publicTag];
        publicKey = [pubkeyData base64Encoding];
        
        //    NSLog(@"pubKey :%@",[pubkeyData base64Encoding]);
        //    NSData *modulusData=  [NSData dataWithBase64EncodedString:modulus];
        //    NSData *expoData=  [NSData dataWithBase64EncodedString:exponent];
        //    NSData* publicKeyData= [CryptoUtil generateRSAPublicKeyWithModulus:modulusData exponent:expoData];
        //    [self writePublicKeyModAndExpWithTag:publicTag];
        //    bool success = [CryptoUtil saveRSAPublicKey:pubkeyData appTag:publicTag overwrite:YES];
    }
    
    return publicKey;
}

//encrypt AES key
-(NSData*) encryptAESKey:(NSData*)AESKeyData :(NSData*) pubkeyData{
    
    [self addPeerPublicKey:@"key" keyBits:pubkeyData];
    SecKeyRef publicKey = [self getPublicKeyReference:@"key"];
    
    //encryption
    NSData* encryptedAESKey = [self encryptWithModulus:[self getModulusWithTag:pubkeyData] exponent:[self getExponentWithTag:pubkeyData] content:AESKeyData:publicKey];
    
    return encryptedAESKey;
}

//decrypt AES encrypted key
-(NSData*) decryptAESKey:(NSData*)encryptedAESKeyData PrivateTag:(NSString*)privateTag{
    
    KeyHelper* keyHelper =[[KeyHelper alloc]init];
    SecKeyRef privateKey=[keyHelper getPrivateKeyRefWithTag:privateTag];
    NSData* decryptedAESKey = [self rsaDecryptWithData:encryptedAESKeyData key:privateKey];
    return decryptedAESKey;
}

/**
 *  Generate and Save key pair to the keychain with tags
 */
-(void)createKeyPairWithTag:(NSString*)publicTag privateTag:(NSString*)privateTag
{
    bool result=[CryptoUtil generateRSAKeyWithKeySizeInBits:1024 publicKeyTag:publicTag     privateKeyTag:privateTag];
}

/**
 *  Get modulus of public key with tag
 */
//-(NSString*)getModulusWithTag:(NSString*)publicTag
//{
//    KeyHelper* keyHelper =[[KeyHelper alloc]init];
//    NSData* pubkeyData=  [keyHelper getPublicKeyBitsWithtag:publicTag];
//    NSData *modData=  [keyHelper getPublicKeyModFromKeyData:pubkeyData];
//    return [modData base64Encoding];
//}
///**
// *  Get exponent of public key with tag
// */
//-(NSString*)getExponentWithTag:(NSString*)publicTag
//{
//    KeyHelper* keyHelper =[[KeyHelper alloc]init];
//    NSData* pubkeyData=  [keyHelper getPublicKeyBitsWithtag:publicTag];
//    NSData *expoData=  [keyHelper getPublicKeyExpFromKeyData:pubkeyData];
//    return [expoData base64Encoding];
//}

/**
 *  Get modulus of public key with tag
 */
-(NSData*)getModulusWithTag:(NSData*) pubkeyData
{
    KeyHelper* keyHelper =[[KeyHelper alloc]init];
    NSData *modData=  [keyHelper getPublicKeyModFromKeyData:pubkeyData];
    return modData;
    //return [modData base64Encoding];
}
/**
 *  Get exponent of public key with tag
 */
-(NSData*)getExponentWithTag:(NSData*) pubkeyData
{
    KeyHelper* keyHelper =[[KeyHelper alloc]init];
    NSData *expoData=  [keyHelper getPublicKeyExpFromKeyData:pubkeyData];
    return expoData;
    //return [expoData base64Encoding];
}

/**
 *  Encrypt Using Modulus and Exponent
 */
-(NSData*)encryptWithModulus:(NSData*)modulusData exponent:(NSData*)expoData content:(NSData*)content :(SecKeyRef)publicKey
{
    NSData* publicKeyData= [CryptoUtil generateRSAPublicKeyWithModulus:modulusData exponent:expoData];
    
    [CryptoUtil deleteRSAPublicKeyWithAppTag:@"PublicKeyTag"];
    
    bool success= [CryptoUtil saveRSAPublicKey:publicKeyData appTag:@"PublicKeyTag" overwrite:YES];
    
    if (success)
    {
        SecKeyRef publicKey= [CryptoUtil loadRSAPublicKeyRefWithAppTag:@"PublicKeyTag"];
        
        NSData* encryptedData= [CryptoUtil encryptData:content RSAPublicKey:publicKey padding:kSecPaddingPKCS1];
        return encryptedData;
    }
    else
    {
        NSLog(@"RSA Public key couldn't be saved.");
    }
    return nil;
}

-(NSData *)rsaDecryptWithData:(NSData*)content key:(SecKeyRef)key{
    
    size_t cipherLen = [content length];
    void *cipher = malloc(cipherLen);
    [content getBytes:cipher length:cipherLen];
    

    size_t plainBufferSize = SecKeyGetBlockSize(key);
    uint8_t *plainBuffer = malloc(plainBufferSize);
    
    size_t keyBufferSize = [content length];
    
    NSMutableData *bits = [NSMutableData dataWithLength:keyBufferSize];
    OSStatus sanityCheck = SecKeyDecrypt(key,
                                         kSecPaddingPKCS1,
                                         cipher,
                                         cipherLen,
                                         [bits mutableBytes],
                                         &plainBufferSize);
    
   
    
    if (sanityCheck != 0) {
        NSError *error = [NSError errorWithDomain:NSOSStatusErrorDomain code:sanityCheck userInfo:nil];
        NSLog(@"Error: %@", [error description]);
    }
    
    [bits setLength:plainBufferSize];
    
    return bits;
}



- (void)addPeerPublicKey:(NSString *)peerName keyBits:(NSData *)publicKeyData {
    
    OSStatus sanityCheck = noErr;
    CFTypeRef persistPeer = NULL;
    //[self removePeerPublicKey:peerName];
    
    NSData * peerTag = [[NSData alloc] initWithBytes:(const void *)[peerName UTF8String] length:[peerName length]];
    NSMutableDictionary * peerPublicKeyAttr = [[NSMutableDictionary alloc] init];
    [peerPublicKeyAttr setObject:(id)kSecClassKey forKey:(id)kSecClass];
    [peerPublicKeyAttr setObject:(id)kSecAttrKeyTypeRSA forKey:(id)kSecAttrKeyType];
    [peerPublicKeyAttr setObject:peerTag forKey:(id)kSecAttrApplicationTag];
    [peerPublicKeyAttr setObject:publicKeyData forKey:(id)kSecValueData];
    [peerPublicKeyAttr setObject:[NSNumber numberWithBool:YES] forKey:(id)kSecReturnData];
    sanityCheck = SecItemAdd((CFDictionaryRef) peerPublicKeyAttr, (CFTypeRef *)&persistPeer);
    
    if(sanityCheck == errSecDuplicateItem){
       
    }

    persistPeer = NULL;
    [peerPublicKeyAttr removeObjectForKey:(id)kSecValueData];
    sanityCheck = SecItemCopyMatching((CFDictionaryRef) peerPublicKeyAttr, (CFTypeRef*)&persistPeer);
    

    //        TRC_DBG(@"SecItem copy matching returned this public key data %@", persistPeer);
    // The nice thing about persistent references is that you can write their value out to disk and
    // then use them later. I don't do that here but it certainly can make sense for other situations
    // where you don't want to have to keep building up dictionaries of attributes to get a reference.
    //
    // Also take a look at SecKeyWrapper's methods (CFTypeRef)getPersistentKeyRefWithKeyRef:(SecKeyRef)key
    // & (SecKeyRef)getKeyRefWithPersistentKeyRef:(CFTypeRef)persistentRef.
    
    if (persistPeer) CFRelease(persistPeer);
}

-(SecKeyRef)getPublicKeyReference:(NSString*)peerName{
    
    OSStatus sanityCheck = noErr;
    
    SecKeyRef pubKeyRefData = NULL;
    NSData * peerTag = [[NSData alloc] initWithBytes:(const void *)[peerName UTF8String] length:[peerName length]];
    NSMutableDictionary * peerPublicKeyAttr = [[NSMutableDictionary alloc] init];
    
    [peerPublicKeyAttr setObject:(id)kSecClassKey forKey:(id)kSecClass];
    [peerPublicKeyAttr setObject:(id)kSecAttrKeyTypeRSA forKey:(id)kSecAttrKeyType];
    [peerPublicKeyAttr setObject:peerTag forKey:(id)kSecAttrApplicationTag];
    [peerPublicKeyAttr setObject:[NSNumber numberWithBool:YES] forKey:       (id)kSecReturnRef];
    sanityCheck = SecItemCopyMatching((CFDictionaryRef) peerPublicKeyAttr, (CFTypeRef*)&pubKeyRefData);
    
    if(pubKeyRefData){

        return pubKeyRefData;
    }else{

        return nil;
    }
}

@end
