//
//  Utilities.swift
//  C00Peeps
//
//  Created by OSX on 18/07/16.
//  Copyright © 2016 SOTSYS011. All rights reserved.
//

import Foundation
import FBSDKShareKit
import FBSDKLoginKit
import TwitterKit
import InstagramKit
import LocalAuthentication

// Custom Log
func printCustom(_ message: String, function: String = #function) {
    if ENABLE_LOGS == 1 {
        print("\(function): \(message)")
    }
}

//MARK: - Set RGB
func ColorWithRGB(_ red:CGFloat, green:CGFloat, blue:CGFloat, alpha:CGFloat) -> UIColor {
    return UIColor(red: red/255, green: green/255, blue: blue/255, alpha: alpha)
}

func ProximaNovaRegular(_ size:CGFloat) -> UIFont {
    return UIFont(name: "ProximaNova-Regular", size: size)!
}

func ProximaNovaBold(_ size:CGFloat) -> UIFont {
    return UIFont(name: "ProximaNova-Bold", size: size)!
}

func ProximaNovaLight(_ size:CGFloat) -> UIFont {
    return UIFont(name: "ProximaNova-Light", size: size)!
}

func ProximaNovaSemibold(_ size:CGFloat) -> UIFont {
    return UIFont(name: "ProximaNova-Semibold", size: size)!
}

//MARK: - Set Padding
func SetPaddingView(_ browseTextFld:UITextField){
    let paddingView = UIView()
    paddingView.frame = CGRect(x: 0, y: 0, width: 5, height: 5)
    browseTextFld.leftView = paddingView;
    browseTextFld.leftViewMode = UITextFieldViewMode.always;
}

func setSubViewBorder(_ viewSelected:UIView, color:UIColor){
    let border = CALayer()
    let width = CGFloat(2.0)
    border.borderColor = color.cgColor
    border.frame = CGRect(x: 0, y: viewSelected.frame.size.height-1, width:  viewSelected.frame.size.width, height: 1)
    border.borderWidth = width
    viewSelected.layer.addSublayer(border)
    viewSelected.layer.masksToBounds = true
}

extension Date {
    
    func getElapsedInterval() -> String {
        
        var interval = (Calendar.current as NSCalendar).components(.year, from: self, to: Date(), options: []).year
        
        if interval! > 0 {
            return interval == 1 ? "\(interval!)" + " " + "year" :
                "\(interval!)" + " " + "years"
        }
        
        interval = (Calendar.current as NSCalendar).components(.month, from: self, to: Date(), options: []).month
        if interval! > 0 {
            return interval == 1 ? "\(interval!)" + " " + "month" :
                "\(interval!)" + " " + "months"
        }
        
        interval = (Calendar.current as NSCalendar).components(.day, from: self, to: Date(), options: []).day
        if interval! > 0 {
            return interval == 1 ? "\(interval!)" + " " + "day" :
                "\(interval!)" + " " + "days"
        }
        
        interval = (Calendar.current as NSCalendar).components(.hour, from: self, to: Date(), options: []).hour
        if interval! > 0 {
            return interval == 1 ? "\(interval!)" + " " + "hour" :
                "\(interval!)" + " " + "hours"
        }
        
        interval = (Calendar.current as NSCalendar).components(.minute, from: self, to: Date(), options: []).minute
        if interval! > 0 {
            return interval == 1 ? "\(interval!)" + " " + "minute" :
                "\(interval!)" + " " + "minutes"
        }
        
        return "a moment"
    }
    
    func getFutureInterval() -> String {
        
        let dt = Date()
        
        var interval:Int = (Calendar.current as NSCalendar).components(.year, from: Date(), to:self , options: []).year!
        
        if interval > 0 {
            
            return interval == 1 ? "\(String(interval))" + " " + "year" :
                "\(String(interval))" + " " + "years"
        }
        
        interval = (Calendar.current as NSCalendar).components(.month, from: Date(), to: self, options: []).month!
        if interval > 0 {
            
            let intervalDays:Int = (Calendar.current as NSCalendar).components(.day, from: Date(), to: self, options: []).day!
            if intervalDays > 0 {
                
                let days = intervalDays - interval*30
                
                if days > 0
                {
                    interval = interval + 1
                    
                    return interval == 1 ? "\(String(interval))" + " " + "month" :
                        "\(String(interval))" + " " + "months"
                }
                else
                {
                    return interval == 1 ? "\(String(interval))" + " " + "month" :
                        "\(String(interval))" + " " + "months"
                }
            }
            else
            {
                return interval == 1 ? "\(String(interval))" + " " + "month" :
                    "\(String(interval))" + " " + "months"
            }
        }
        
        var intervalDays:Int = (Calendar.current as NSCalendar).components(.day, from: Date(), to: self, options: []).day!
        if intervalDays > 0 {
            
            let intervalHrs:Int = (Calendar.current as NSCalendar).components(.hour, from: Date(), to: self, options: []).hour!
            
            if intervalHrs > 0 {
            
                if intervalHrs%24 > 0
                {
                    intervalDays = intervalHrs/24 + 1
                    
                    return intervalDays == 1 ? "\(String(intervalDays))" + " " + "day" :
                        "\(String(intervalDays))" + " " + "days"
                }
                else
                {
                    return intervalDays == 1 ? "\(String(intervalDays))" + " " + "day" :
                        "\(String(intervalDays))" + " " + "days"
                }
            }
            else if intervalHrs == 0{
                
                return intervalDays == 1 ? "\(String(intervalDays))" + " " + "day" :
                    "\(String(intervalDays))" + " " + "days"
            }
        }
        else
        {
            var intervalHrs:Int = (Calendar.current as NSCalendar).components(.hour, from: Date(), to: self, options: []).hour!
            if intervalHrs > 0 {
                
                let intervalMinutes:Int = (Calendar.current as NSCalendar).components(.minute, from: Date(), to: self, options: []).minute!
                if intervalMinutes > 0 {
                    
                    if intervalMinutes%60 > 0
                    {
                        intervalHrs = intervalHrs/60 + 1
                        
                        return intervalHrs == 1 ? "\(String(intervalHrs))" + " " + "hour" :
                            "\(String(intervalHrs))" + " " + "hours"
                    }
                    else
                    {
                        return intervalHrs == 1 ? "\(String(intervalHrs))" + " " + "hour" :
                            "\(String(intervalHrs))" + " " + "hours"
                    }
                    
                }
                else if intervalMinutes == 0 {
                    
                    return intervalHrs == 1 ? "\(String(intervalHrs))" + " " + "hour" :
                        "\(String(intervalHrs))" + " " + "hours"
                }
            }
            else
            {
                interval = (Calendar.current as NSCalendar).components(.minute, from: Date(), to: self, options: []).minute!
                if interval > 0 {
                    
                    return interval == 1 ? "\(String(interval))" + " " + "minute" :
                        "\(String(interval))" + " " + "minutes"
                }
            }
        }
        
        
        
        return "a moment"
    }
}

@objc class Utilities:NSObject {

    //MARK: - Video Filter Variables
    let GPUIMAGE_BRIGHTNESS = 1
    let GPUIMAGE_GRAYSCALE = 2
    let GPUIMAGE_COLORINVERT = 3
    
    
    //MARK: - DeviceToken
    
    class func setDeviceToken(_ deviceToken:String)
    {
        let prefs = UserDefaults.standard
        prefs.setValue(deviceToken, forKey: "DEVICETOKEN")
    }
    
    class func getDeviceToken() ->String
    {
        let prefs = UserDefaults.standard
        
        var deviceToken = "67c5aaa7de122bc5e8a2726c081be79204faede76829e617b625c1"//dummy udid
        
        if let str = prefs.string(forKey: "DEVICETOKEN"){
            deviceToken = str
        }
        
        return deviceToken
    }
    
    //MARK: - BraintreeToken

    class func setBraintreeToken(_ braintreeToken:String) {
        let prefs = UserDefaults.standard
        prefs.setValue(braintreeToken, forKey: "BRAINTREETOKEN")
    }
    
    class func getBraintreeToken() ->String {
        let prefs = UserDefaults.standard
        
        var braintreeToken = ""
        
        if let str = prefs.string(forKey: "BRAINTREETOKEN"){
            braintreeToken = str
        }
        
        return braintreeToken
    }

    
    //MARK: - User Details
    
    class func setUserDetails(_ userDetails:UserDetail) {
        let prefs = UserDefaults.standard
        let encodedObject:Data = NSKeyedArchiver.archivedData(withRootObject: userDetails)
        prefs.setValue(encodedObject, forKey: "USERDETAILS")
    }
    
    class func getUserDetails() ->UserDetail {
        let prefs = UserDefaults.standard
        
        var userDetails:UserDetail = UserDetail()
        if let dict = prefs.object(forKey: "USERDETAILS"){
            let encodedObject:Data = dict as! Data
            userDetails = NSKeyedUnarchiver.unarchiveObject(with: encodedObject) as! UserDetail
        }
        
        return userDetails
    }
    
    class func removeUserDetails() {
        let prefs = UserDefaults.standard
        prefs.removeObject(forKey: "USERDETAILS")
        prefs.synchronize()
    }
    
    //MARK: - Sign Up State
    
    class func setSignUpState(_ state:SignUpState) {
        let prefs = UserDefaults.standard
        prefs.set(state.rawValue, forKey: "SIGNUPSTATE")
    }
    
    class func getSignUpState() -> SignUpState {
        let prefs = UserDefaults.standard
        
        var signUpState:Int? = 0
        
        if let signUpStateRawValue = prefs.object(forKey: "SIGNUPSTATE"){
            signUpState = signUpStateRawValue as? Int
        }
        return SignUpState(rawValue: signUpState!)!
    }
    
    //MARK: - Is Failed To Upgrade Package
    
    class func setIsFailedToUpgradePackage(_ isFailed:String) {
        let prefs = UserDefaults.standard
        prefs.setValue(isFailed, forKey: "ISFAILEDTOUPGRADEPACKAGE")
    }
    
    class func getIsFailedToUpgradePackage() ->String {
        let prefs = UserDefaults.standard
        
        var isFailed = ""
        
        if let str = prefs.string(forKey: "ISFAILEDTOUPGRADEPACKAGE"){
            isFailed = str
        }
        
        return isFailed
    }
   
    //MARK: - Themed Image
    
    func themedImage(_ imageName:String) -> UIImage {
        var themeId = Utilities.getUserDetails().theme_id
        if themeId == "" {
            themeId = "1"
        }
        let updatedImageName = self.updatedThemeImageName(imageName)
        var requiredImage:UIImage?

        if themeId == "1" {//Fetch images from bundle
            if let image:UIImage? = UIImage(named: updatedImageName) { // Return image with image name.
                if image != nil {
                    requiredImage = image!
                    currentThemeId = "1"
                }
                else {//If image is not available in any other theme id, then use image from theme id "1".
                    requiredImage = self.updatedTheme1Image(updatedImageName)
                }
            }
            else {//If image is not available in any other theme id, then use image from theme id "1".
                requiredImage = self.updatedTheme1Image(updatedImageName)
            }
        }
        else {//Fetch images from documents directory
            let paths = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.allDomainsMask, true)
            let documentsDirectory:String = paths.first!
            //printCustom("documentsDirectory:\(documentsDirectory)")
            let themeName = "theme" + Utilities.getUserDetails().theme_id
            var fileName = URL(fileURLWithPath:documentsDirectory).appendingPathComponent(themeName)
            fileName = fileName.appendingPathComponent(self.themeName())
            fileName = fileName.appendingPathComponent(updatedImageName + ".png")
            //printCustom("fileName:\(fileName!.path!)")

            let image:UIImage? = UIImage(contentsOfFile: fileName.path)
            if image != nil {
//                printCustom("updatedImageName:\(updatedImageName)")
//                printCustom("bg_t:\("bg_t" + Utilities.getUserDetails().theme_id)")

                if updatedImageName == "header_t" + Utilities.getUserDetails().theme_id {
                    currentThemeId = themeId
                }
                printCustom("Image available in doc directory")
                requiredImage = image!
            }
            else {//If image is not available in any other theme id, then use image from theme id "1".
//                printCustom("Image not available in doc directory, load theme 1 image from bundle")
                requiredImage = self.updatedTheme1Image(updatedImageName)
            }
        }
        return requiredImage!
    }
    
    func updatedTheme1Image(_ imageName:String) -> UIImage {
        var updatedImageName = imageName
        if imageName.contains("_t") {//If image is not from common folder, then only append theme id.
            updatedImageName = String(updatedImageName.characters.dropLast())// Remove existing theme id from image name.
            updatedImageName += "1" // Append 1st theme id in image name.
            if imageName == "header_t" + Utilities.getUserDetails().theme_id {
                currentThemeId = "1"
            }
        }
         return UIImage(named: updatedImageName)!
    }
    
    func updatedThemeImageName(_ imageName:String) -> String {
        var themeId = Utilities.getUserDetails().theme_id
        if themeId == "" {
            themeId = "1"
        }
        var updatedImageName = imageName
        if String(imageName.characters.last!) != themeId {//If theme id is not same as contained in image name, then only replace theme id in image name. Default image name has theme 1, so if theme id is 1, then need not to update image name.
            if imageName.contains("_t") {//If image name contains themeing i.e. "_t", then only update image name with latest theme id.
                updatedImageName = String(updatedImageName.characters.dropLast())// Remove existing theme id from image name.
                updatedImageName += themeId // Append latest theme id in image name.
            }
        }
        return updatedImageName
    }
    
     //MARK: - Theme Name
    func themeName() -> String {
        let themeId = Utilities.getUserDetails().theme_id
        var name:String!
        switch themeId {
        case "1":
            name = themeName_t1
        case "2":
            name = themeName_t2
        case "3":
            name = themeName_t3
        case "4":
            name = themeName_t4
        case "5":
            name = themeName_t5
        default:
            name = themeName_t6
        }
        return name + "Theme_t" + themeId
    }
    
    //MARK: - Themed Color
  
    /*func themedMultipleColor() -> (UIColor, UIColor, UIColor) {
        let themeId = currentThemeId//Utilities.getUserDetails().theme_id
        var themeColor1:UIColor!
        var themeColor2:UIColor!
        var themeColor3:UIColor!
        switch themeId {
        case "1"://Only Theme 1 has three color combos to be used. In other themes, 3rd color is same as 1st color.
            themeColor1 = themeColor1_t1
            themeColor2 = themeColor2_t1
            themeColor3 = themeColor3_t1
        case "2":
            themeColor1 = themeColor1_t2
            themeColor2 = themeColor2_t2
            themeColor3 = themeColor1_t2
        case "3":
            themeColor1 = themeColor1_t3
            themeColor2 = themeColor2_t3
            themeColor3 = themeColor1_t3
        case "4":
            themeColor1 = themeColor1_t4
            themeColor2 = themeColor2_t4
            themeColor3 = themeColor1_t4
        case "5":
            themeColor1 = themeColor1_t5
            themeColor2 = themeColor2_t5
            themeColor3 = themeColor1_t5
        default:
            themeColor1 = themeColor1_t6
            themeColor2 = themeColor2_t6
            themeColor3 = themeColor1_t6
        }
        return (themeColor1, themeColor2, themeColor3)
    }*/
    
    func themedMultipleColor() -> [UIColor] {
        let themeId = currentThemeId//Utilities.getUserDetails().theme_id
        var themeColor1:UIColor!
        var themeColor2:UIColor!
        var themeColor3:UIColor!
        switch themeId {
        case "1"://Only Theme 1 has three color combos to be used. In other themes, 3rd color is same as 1st color.
            themeColor1 = themeColor1_t1
            themeColor2 = themeColor2_t1
            themeColor3 = themeColor3_t1
        case "2":
            themeColor1 = themeColor1_t2
            themeColor2 = themeColor2_t2
            themeColor3 = themeColor1_t2
        case "3":
            themeColor1 = themeColor1_t3
            themeColor2 = themeColor2_t3
            themeColor3 = themeColor1_t3
        case "4":
            themeColor1 = themeColor1_t4
            themeColor2 = themeColor2_t4
            themeColor3 = themeColor1_t4
        case "5":
            themeColor1 = themeColor1_t5
            themeColor2 = themeColor2_t5
            themeColor3 = themeColor1_t5
        default:
            themeColor1 = themeColor1_t6
            themeColor2 = themeColor2_t6
            themeColor3 = themeColor1_t6
        }
        return [themeColor1, themeColor2, themeColor3]
    }
    
    //MARK: - Push/Pop Animation
    
    func addPushAnimation () -> CATransition {
        let animation:CATransition = CATransition()
        animation.duration = 0.3
        animation.type = kCATransitionPush
        animation.subtype = kCATransitionFromRight
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        return animation
    }
    
    func addPopAnimation () -> CATransition {
        let animation:CATransition = CATransition()
        animation.duration = 0.3
        animation.type = kCATransitionPush
        animation.subtype = kCATransitionFromLeft
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        return animation
    }
    
    func addPresentModalAnimation () -> CATransition {
        let animation:CATransition = CATransition()
        animation.duration = 0.3
        animation.type = kCATransitionMoveIn
        animation.subtype = kCATransitionFromTop
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        return animation
    }
    
    func addDismissModalAnimation () -> CATransition {
        let animation:CATransition = CATransition()
        animation.duration = 0.3
        animation.type = kCATransitionMoveIn
        animation.subtype = kCATransitionFromBottom
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        return animation
    }
    
    // MARK: - Genralize Price
    
    func generalizePrice(_ price:String) -> String {
        // Convert amount like "2" into "2.00"
        if price == "" {
            return "0.00"
        }
        else {
            let validatedPrice = self.validatedDecimalValue(price)
            let amount = Double(validatedPrice)
            let strAmount = NSString(format:"%.2f", amount!) as String
            return strAmount
        }
    }
    
    func validatedDecimalValue(_ value:String) -> String {
        var text = value
        var decimalCount = 0
        var count = 0
        for character in value.characters {
            if character == "." {
                decimalCount += 1
                
                if decimalCount > 1 {
                    //array.removeAtIndex(decimalCount)
                    let otherRange = value.characters.index(value.characters.startIndex, offsetBy: count)..<value.characters.endIndex
                    text.replaceSubrange(otherRange, with: "")
                    printCustom("text:\(text)")
                    return text
                }
            }
            count += 1
        }
        return value
    }
  
    //MARK:- Resizing image
    
    func imageResize(_ imageObj:UIImage, sizeChange:CGSize)-> UIImage {
        
        let hasAlpha = false
        let scale: CGFloat = 0.0 // Automatically use scale factor of main screen
        
        UIGraphicsBeginImageContextWithOptions(sizeChange, !hasAlpha, scale)
        imageObj.draw(in: CGRect(origin: CGPoint.zero, size: sizeChange))
        
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext() // !!!
        return scaledImage!
    }
    
    //MARK:- Crop image
    
    func cropToBounds(_ image: UIImage, cgwidth: CGFloat, cgheight: CGFloat) -> UIImage {
        
        let contextImage: UIImage = UIImage(cgImage: image.cgImage!)
        
        let contextSize: CGSize = contextImage.size
        
        var posX: CGFloat = 0.0
        var posY: CGFloat = 0.0
        
        var aWidth  = cgwidth
        var bHeight = cgheight
        
        // See what size is longer and create the center off of that
        if contextSize.width > contextSize.height {
            posX = ((contextSize.width - contextSize.height) / 2)
            posY = 0
            aWidth = contextSize.height
            bHeight = contextSize.height
        } else {
            posX = 0
            posY = ((contextSize.height - contextSize.width) / 2)
            aWidth = contextSize.width
            bHeight = contextSize.width
        }
        
        let rect: CGRect = CGRect(x: posX, y: posY, width: aWidth, height: bHeight)
        
        // Create bitmap image from context using the rect
        let imageRef: CGImage = contextImage.cgImage!.cropping(to: rect)!
        
        // Create a new image based on the imageRef and rotate back to the original orientation
        let image: UIImage = UIImage(cgImage: imageRef, scale: image.scale, orientation: image.imageOrientation)
        
        return image
    }

    //MARK: - Cropped Image
    func croppedImage (_ imageToCrop:UIImage) -> Data {
        let image = self.cropToBounds(imageToCrop, cgwidth: UIScreen.main.bounds.width, cgheight: UIScreen.main.bounds.width)
        let data:Data = UIImageJPEGRepresentation(image, 0.5)!
        return data
    }
    
    //MARK:- Get Date in Ago format
    func getDateAgo(_ dateStr:String)->String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone(identifier: "UTC")
        let dateCreatedUTC = dateFormatter.date(from: dateStr)
        return (dateCreatedUTC?.getElapsedInterval())! + " ago"
    }
    
    //MARK:- Get Future Date (expiry time) in Ago format
    func getFutureDateAgo(_ dateStr:String)->String
        
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone(identifier: "UTC")
        let dateExpiryUTC = dateFormatter.date(from: dateStr)
        
        if let text: String = dateExpiryUTC?.getFutureInterval() {
            return text
        }
        return "NA"
        //return (dateExpiryUTC?.getFutureInterval())!
    }
    
    // MARK: - Convert String to Base 64
    func convertStringToBase64(_ string:String) -> String {
        
        let utf8str = string.data(using: String.Encoding.utf8)
        
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        {
            
            printCustom("Encoded:  \(base64Encoded)")
            return base64Encoded
        }
        else {
            return ""
        }
    }
    
    // MARK: - Convert recieved date to required format
    
    func convertDateToAnotherFormat(_ strDate:String, fromFormat:String, toFormat:String) -> String {
        if strDate != "" {
            // Convert date into required date format
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = fromFormat
            //printCustom("strDate:\(strDate)")

            if let receivedDate = dateFormatter.date(from: strDate) {
                let anotherDateFormatter = DateFormatter()
                anotherDateFormatter.dateFormat = toFormat
                let strFormattedDate = anotherDateFormatter.string(from: receivedDate)
                //printCustom("strFormattedDate:\(strFormattedDate)")
                return strFormattedDate
            }
            else {
                return ""
            }
        }
        else {
            return ""
        }
    }
    
    func convertStringToDate(_ strDate:String) -> Date {
        if strDate != "" {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = dateFormatLocal
            let date = dateFormatter.date(from: strDate)
            return date!
        }
        else {
            return Date()
        }
    }
    
    func convertDateToString(_ date:Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormatLocal
        let strDate = dateFormatter.string(from: date)
        return strDate
    }
    
    // MARK: - Is Valid Email
    func isValidEmail(_ email:String) -> Bool {
        // printCustom("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPredicate.evaluate(with: email)
    }
    
    //MARK: - Link/Unlink Social Accounts
    func linkSocialAccount(_ isLinkingRequired:Bool, accountType:AccountType) -> Bool {
        switch accountType {
        case AccountType.facebook:
           return self.linkUnlinkFacebookAccount(isLinkingRequired)
            
        case AccountType.twitter:
           return self.linkUnlinkTwitterAccount(isLinkingRequired)
            
        case AccountType.instagram:
           return self.linkUnlinkInstagramAccount(isLinkingRequired)
            
        case AccountType.all:
            self.linkUnlinkFacebookAccount(isLinkingRequired)
            self.linkUnlinkTwitterAccount(isLinkingRequired)
            self.linkUnlinkInstagramAccount(isLinkingRequired)
            return false

        default:
            printCustom("Normal")
            return false
        }
    }
    
    func linkUnlinkFacebookAccount(_ isLinkingRequired:Bool) -> Bool {
        if FBSDKAccessToken.current() != nil {
            let facebookLogin = FBSDKLoginManager()
            facebookLogin.logOut()
            printCustom("FBSDKAccessToken.currentAccessToken():\(FBSDKAccessToken.current())")
        }
        else {
            if isLinkingRequired {
                return true
            }
        }
        return false
    }
    
    func linkUnlinkTwitterAccount(_ isLinkingRequired:Bool) -> Bool {
        
        let store = TWTRTwitter.sharedInstance().sessionStore
        if store.session() != nil {
            if let userID:String = store.session()!.userID {
                store.logOutUserID(userID)
            }
        }
        else {
            if isLinkingRequired {
                return true
            }
        }
        return false
    }
    
    func linkUnlinkInstagramAccount(_ isLinkingRequired:Bool) -> Bool {
        if InstagramEngine.shared().isSessionValid() {
            InstagramEngine.shared().logout()
        }
        else {
            if isLinkingRequired {
                return true
            }
        }
        return false
    }
    
    //MARK: - Get Social Username from linked account
    func socialUsername(_ accountType:AccountType) -> String {
        var username = ""
        switch accountType {
        case AccountType.facebook:
            if FBSDKAccessToken.current() != nil {
                FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, email"]).start(completionHandler: { (connection, result, error) -> Void in
                    if (error == nil){
                        let resultdict = result as! NSDictionary
                        //printCustom("profile:\(result)")
                        username = resultdict["name"] as! String
                    }
                })
            }
            
        case AccountType.twitter:
            let store = TWTRTwitter.sharedInstance().sessionStore
            if store.session() != nil {
                if let userID:String = store.session()!.userID {
                    let client = TWTRAPIClient(userID: userID)
                    client.loadUser(withID: userID) { (user, error) -> Void in
                        printCustom("twitter user profile: \(user)");
                        username = user!.name
                    }
                }
            }
            
        case AccountType.instagram:
            if InstagramEngine.shared().isSessionValid() {
                InstagramEngine.shared().getSelfUserDetails(success: { (user) in
                    printCustom("instagram user username:\(user.username)")
                    username = user.username
                    
                    }, failure: nil)
            }
            
        default:
            printCustom("Normal User")
        }
        return username
    }

    // MARK: - FBSDKShareLinkContent & FBSDKShareDialog
    func getShareLinkContentWithContentURL(_ objectURL:URL) -> FBSDKShareLinkContent {
        let content: FBSDKShareLinkContent = FBSDKShareLinkContent()
        content.contentURL = objectURL
        return content
    }
    
    func getShareDialogWithContentURL(_ objectURL:URL) -> FBSDKShareDialog {
        let shareDialog: FBSDKShareDialog = FBSDKShareDialog()
        shareDialog.shareContent = self.getShareLinkContentWithContentURL(objectURL)
        return shareDialog
    }
    
    //MARK: - Authenticate User
    func isTouchIdConfigured () -> Bool {
        let context = LAContext()
        var error: NSError?
        // let reasonString = "Authentication is needed to access your app."
        // Check if the device can evaluate the policy.
        if context.canEvaluatePolicy(LAPolicy.deviceOwnerAuthenticationWithBiometrics, error: &error) {
               return true
        }
        else{
            // If the security policy cannot be evaluated then show a short message depending on the error.
            switch error!.code{
            case LAError.Code.touchIDNotEnrolled.rawValue:
                    return false
                
            case LAError.Code.passcodeNotSet.rawValue:
                return false
                
            default:
                // The LAError.TouchIDNotAvailable case.
                    return true
            }
        }
    }
    
    //MARK: - Get Optional Decoded String
    func optionalDecodedString(_ aDecoder: NSCoder, forKey key: String) -> String {
        if let object = aDecoder.decodeObject(forKey: key) as? String {
            return object
        }
        else {
            return ""
        }
    }
    
    //MARK: - Get Optional Decoded Int
    func optionalDecodedInt(_ aDecoder: NSCoder, forKey key: String) -> Int {
        if let object = aDecoder.decodeObject(forKey: key) as? Int {
            printCustom("decodedobject Int:\(object)")

            return object
        }
        else {
            return 0
        }
    }
    
    //MARK: - Get saveOriginalPhoto setting
    func saveOriginalPhoto()->Bool{
        
        let prefs = UserDefaults.standard
        
        var saveOrigPic = false
        
        if let str = prefs.bool(forKey: "saveOriginalPhoto") as? Bool
        {
            saveOrigPic = str
        }
        
        return saveOrigPic
    }
    
    //MARK: - Set saveOriginalPhoto setting
    func setSaveOriginalPhoto(_ flag:Bool){
        
        let prefs = UserDefaults.standard
        prefs.setValue(flag, forKey: "saveOriginalPhoto")
    }
    
    //MARK: - Get clear theme data setting
    func getClearThemeDataSetting()->Bool{
        
        let prefs = UserDefaults.standard
        
        var clearThemeData = false
        
        if let clearThemeDataSetting:Bool? = prefs.bool(forKey: "clearThemeData")
        {
            clearThemeData = clearThemeDataSetting!
        }
        
        return clearThemeData
    }
    
    //MARK: - Set clear theme data setting
    func setClearThemeDataSetting(_ flag:Bool){
        
        let prefs = UserDefaults.standard
        prefs.setValue(flag, forKey: "clearThemeData")
    }

    //MARK: - Get UUID
    func getUUIDForObjectUpload()->String {
        
        let uuid = NSUUID().uuidString.lowercased()
        
        return uuid
    }
    
    func truncatePhoneNo(_ phone:String) -> String {
        printCustom("phone:\(phone)")
        let phoneNo:NSString = phone as NSString
        if phoneNo.length > 10 {
            let normalizedNo = phone.substring(with: Range<String.Index>( phone.characters.index(phone.endIndex, offsetBy: -10) ..< phone.endIndex))// Fetching last 10 digits from phone number
            printCustom("normalizedNo:\(normalizedNo)")
            return normalizedNo
        }
        else {
            return phone
        }
    }

    func changedPhoneNumberFormat(_ number:String) -> String {
        return number.replacingOccurrences(of: "(\\d{3})(\\d{3})(\\d+)", with: "($1) $2-$3", options: .regularExpression, range: nil)
       // return txtNo.replacingOccurrences(of: "(\\d{3})(\\d{3})(\\d+)", with: "($1) $2-$3", options: .regularExpression, range: nil)
    }
    
    //MARK: - Added Password Rule
    func checkPasswordRule(_ password: String) -> String {
        
        //return ""
        
        //password length should not be less than 8
        if (password.characters.count < 8) {
            return alertMsg_PasswordLengthRule
        }
        
        //password should have atleast one upper case
        let hasUpperCase = matchesForRegexInText("[A-Z]", text: password).count > 0
        if !hasUpperCase {
            return alertMsg_PasswordUppercaseRule
        }
        
        //password should have atleast one lower case
        let hasLowerCase = matchesForRegexInText("[a-z]", text: password).count > 0
        if !hasLowerCase {
            return alertMsg_PasswordLowercaseRule
        }
        
        //password should have atleast one number
        let hasNumbers = matchesForRegexInText("\\d", text: password).count > 0
        if !hasNumbers {
            return alertMsg_PasswordNumericRule
        }
        
        return ""
    }
    
    func matchesForRegexInText(_ regex: String, text: String) -> [String] {
        do {
            let regex = try NSRegularExpression(pattern: regex, options: [])
            let nsString = text as NSString
            let results = regex.matches(in: text,
                                                options: [], range: NSMakeRange(0, nsString.length))
            return results.map { nsString.substring(with: $0.range)}
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            return []
        }
    }
    
    //MARK: - Save media file locally in document directory
    func saveMediaLocally(_ mediaName:String, mediaData:Data, folderName:String){
        
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
        let documentsDirectory = paths.first! as String
        
        let dataPath = documentsDirectory + ("/media/"+folderName)
        
        do {
            
            try FileManager.default.createDirectory(atPath: dataPath, withIntermediateDirectories: true, attributes: nil)
            
        } catch let error as NSError {
            print(error.localizedDescription);
        }
        
        let dataPath1 = documentsDirectory + ("/media/" + folderName + "/" + mediaName)
        printCustom(dataPath1)
        
        try? mediaData.write(to: URL(fileURLWithPath: dataPath1), options: [.atomic])
        
//        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) { () -> Void in
//         
//           
//        }
    }
    
    //MARK: - Get media file path locally from document directory
    func getLocalMediaPath(_ mediaURL:String, mediaAttachmentType:Int, folderName:String)->String{
        
        if mediaURL.contains(".dat")
        {
            var mediaName =  mediaURL.replacingOccurrences(of: BASE_CLOUD_FRONT_URL, with: "")
            
            if mediaAttachmentType == 0  // only pic
            {
                mediaName = mediaName.replacingOccurrences(of: ".dat", with: ".png")
            }
            else if mediaAttachmentType == 2  //audio file
            {
                mediaName = mediaName.replacingOccurrences(of: ".dat", with: ".m4a")
            }
            else if mediaAttachmentType == 3  //video file
            {
                mediaName = mediaName.replacingOccurrences(of: ".dat", with: ".mp4")
            }
            
            let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
            let documentsDirectory = paths.first! as String
            let dataPath1 = documentsDirectory + ("/media/" + folderName + "/" + mediaName)
            printCustom(dataPath1)
            
            if FileManager().fileExists(atPath: dataPath1) == true
            {
                return dataPath1
            }

        }
        else if mediaURL.contains(".png")
        {
            return mediaURL
        }
        
        return ""
    }
        
    func setPeepTextSentUser (_ sentUserId: Int, sentUserName: String){
        isComingFromChatFlow = true
        let cnts:Contact = Contact()
        cnts.pId = sentUserId
        cnts.pUserName = sentUserName
        printCustom("cnts.pId:\( cnts.pId)")
        printCustom("cnts.pUserName:\( cnts.pUserName)")

        let prefs = UserDefaults.standard
        let encodedObject:Data = NSKeyedArchiver.archivedData(withRootObject: cnts)
        prefs.setValue(encodedObject, forKey: "PeepTextSentUser")
    }
    
    func getPeepTextSentUser() -> [Contact]{
        let prefs = UserDefaults.standard
        var con:Contact = Contact()
        if let dict = prefs.object(forKey: "PeepTextSentUser"){
            let encodedObject:Data = dict as! Data
            con = NSKeyedUnarchiver.unarchiveObject(with: encodedObject) as! Contact
            printCustom("con.pid:\( con.pId)")
            printCustom("con.pUserName:\( con.pUserName)")

        }
        var arrContacts = [Contact]()
        arrContacts.append(con)
        return arrContacts
    }
    
    
}
