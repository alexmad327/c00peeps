//
//  Constant.swift
//  C00Peeps
//
//  Created by OSX on 17/06/16.
//  Copyright © 2016 SOTSYS011. All rights reserved.
//

import Foundation

//MARK: - Common Constants
let ENABLE_LOGS = 1 //Enable/Disable Logs
let MainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main) // Main Storyboard
let ApplicationDelegate = UIApplication.shared.delegate as! AppDelegate // Shared App Delegate
let kDeviceToken = "DeviceToken"
let heightNavigationBar:CGFloat = 0.0
let Limit = 10
let SearchLimit = 12
let maxLimitEventDescription = 400 //Event Description - Max Limit
var isEditingEvent = false //Event being edited
var peepTextConstant = ""//Peep Text
var isGalleryMediaSent = false
var isEditingEventMedia = false
var checkAccountActivatedFromAddContacts = false
var phoneNoFormat = "XXX-XXX-XXXX"
let localSentFolderName = "sent"
let localReceivedFolderName = "received"
let maxVideoRecordingTime:Double = 120.0//60.0
var sendPeepTextToContact:Contact = Contact()


//MARK: - Base URLs

//AWS EC2 URLs
//let BASE_URL = "http://54.177.24.190/coopeeps/index.php/"
//let BASE_URL_LOCAL =  "http://54.177.24.190/coopeeps/index.php/"

let BASE_URL = "http://18.208.57.159/coopeeps/index.php/"
let BASE_URL_LOCAL =  "http://18.208.57.159/coopeeps/index.php/"

//In below API URL email sending will not work will not work
//let BASE_URL = "http://112.196.86.42/coopeeps/coopeeps/index.php/"
//let BASE_URL_LOCAL =  "http://112.196.86.42/coopeeps/coopeeps/index.php/"  // This is used for events

//In below API URL event payment will not work
//let BASE_URL = "http://coopeeps.trantorinc.com/coopeeps/"//"http://coopeeps.trantorinc.com/coopeeps/"
//let BASE_URL_LOCAL = "http://coopeeps.trantorinc.com/coopeeps/"//"http://10.50.1.110/coopeeps/coopeeps/"//"http://coopeeps.trantorinc.com/coopeeps/"

let S3_BASE_URL = "https://s3.amazonaws.com/pecxstagingbucket/"
let BASE_CLOUD_FRONT_URL = "http://dixtqoo95vlco.cloudfront.net/"
let PARAM_EVENT_SURVEY = "survey.php?"

//MARK: - App Bundle Id
let appBundleId = "com.pecx.lgibranding"

//MARK: - Apple Pay Merchant Id
let applePayMerchantId = "merchant.com.pecx.lgibranding"

//MARK: - Privacy Policy & Terms URL
//let TERMS_URL = "http://coopeeps.trantorinc.com/coopeeps/terms.html"
//let PRIVACY_POLICY_URL = "http://coopeeps.trantorinc.com/coopeeps/privacy.html"

let TERMS_URL = "http://18.208.57.159/coopeeps/terms.html"
let PRIVACY_POLICY_URL = "http://18.208.57.159//coopeeps/privacy.html"

//MARK: - Twitter API Urls
let FETCH_TWITTER_FOLLOWERS_URL = "https://api.twitter.com/1.1/followers/list.json"
let SEND_DIRECT_MSG_TO_TWITTER_FOLLOWER_URL = "https://api.twitter.com/1.1/direct_messages/new.json"

//MARK: - Flurry
let FLURRY_APIKEY_PRODUCTION = "Y6BB2346B9S4X7WVWCCN"
let FLURRY_APIKEY_STAGING = "XDZXMNRCCDDMVYRCKWQ5"

//MARK: - Facebook
let facebookAppLink = "https://fb.me/679143348926867"
let facebookAppStoreURL = "https://itunes.apple.com/in/app/facebook/id284882215?mt=8"
let socialMediaLink = "http://pecx.info"
let socialMediaText = "Hi! Please join us on PECX! PECX is live now!"

//MARK: - Twitter Consumer Key and Secret
let twitterConsumerKey = "2yFJ5cLpAlqjorMwrW7W5amRB"
let twitterConsumerSecret = "QrsbltXFTABVWMZ5AGnme2bjzzVlWGGHzw9nPHo7O1On3sB1xf"

//MARK: - Amazon Cognito Pool Id & Bucket Name
let S3BucketName: String = "pecxstagingbucket"
let S3UploadKeyName: String = "YourDownloadKeyName"
let CognitoPoolId: String =   "us-west-2:3b795181-76be-4738-a18c-9e2141a40f87"

//MARK: - Notification Constants
let NOTIFIACTION_LOGIN = "notification_login"
let NOTIFIACTION_LOGOUT = "notification_logout"
let NOTIFIACTION_SIGNUP = "notification_signup"
let NOTIFIACTION_SETACCOUNTTYPE = "notification_setaccounttype"
let NOTIFIACTION_FORGOTPASSWORD = "notification_forgotpassword"
let NOTIFIACTION_CHANGEPASSWORD = "notification_changepassword"
let NOTIFIACTION_EDITPROFILE = "notification_editprofile"
let NOTIFIACTION_GETUSERDETAILS = "notification_getuserdetails"
let NOTIFIACTION_SETPEEPCODE = "notification_setpeepcode"
let NOTIFIACTION_GETFEEDS = "notification_getfeeds"
let NOTIFIACTION_ADDCOMMENT = "notification_addcomment"
let NOTIFIACTION_GETCOMMENTS = "notification_getcomments"
let NOTIFIACTION_LIKE = "notification_like"
let NOTIFIACTION_DISLIKE = "notification_dislike"
let NOTIFIACTION_THEMETYPE = "notification_setthemetype"
let NOTIFICATION_FETCHTHEMEIMAGESFOLDERURL = "notification_fetchthemeimagesfolderurl"
let NOTIFICATION_DOWNLOADTHEMEIMAGESFILE = "notification_downloadthemeimages"
let NOTIFIACTION_GETPEOPLE = "notification_getpeople"
let NOTIFIACTION_GETLIKERPEOPLE = "notification_getlikerpeople"
let NOTIFIACTION_GETDISLIKERPEOPLE = "notification_getdislikerpeople"

let NOTIFIACTION_SEARCHPEOPLE = "notification_searchpeople"
let NOTIFIACTION_FOLLOWPEOPLE = "notification_followpeople"
let NOTIFIACTION_UNFOLLOWPEOPLE = "notification_unfollowpeople"
let NOTIFICATION_USERTYPE           = "notification_usertype"
let NOTIFICATION_SIGNUP             = "notification_signup"
let NOTIFICATION_CHECKSOCIALUSEREXISTS           = "notification_checksocialuserexists"
let NOTIFIACTION_POSTUPLOADDIGITALMEDIA = "notification_uploaddigitalmedia"
let NOTIFIACTION_POSTSENDPEEPTEXT = "notification_sendpeeptext"
let NOTIFIACTION_GETCONTACTS = "notification_getcontacts"
let NOTIFIACTION_GETFOLLOWERS = "notification_getfollowers"
let NOTIFIACTION_SEARCHCONTACTS = "notification_searchcontacts"
let NOTIFIACTION_REPORTMEDIA = "notification_reportmedia"
let NOTIFIACTION_GETPUBLICMOVIEMEDIA = "notification_getpublicmoviemedia"
let NOTIFIACTION_GETPUBLICMUSICMEDIA = "notification_getpublicmusicmedia"
let NOTIFIACTION_GETPUBLICVIDEOGAMESMEDIA = "notification_getpublicvideogamesmedia"
let NOTIFIACTION_SEARCHPUBLICMEDIA = "notification_searchpublicmedia"
let NOTIFICATION_FETCHEVENTCATEGORIES = "notification_fetcheventcategories"
let NOTIFICATION_SENDEVENT = "notification_sendevent"
let NOTIFIACTION_GETGALLERYMOVIES = "notification_getGallerymovies"
let NOTIFIACTION_GETGALLERYMUSIC = "notification_getGallerymusic"
let NOTIFIACTION_GETGALLERYVIDEOGAMES = "notification_getGalleryvideogames"
let NOTIFIACTION_GETGALLERYPHOTOS = "notification_getGalleryphotos"
let NOTIFIACTION_GETGALLERYPEEPTEXT = "notification_getGallerypeeptext"
let NOTIFICATION_GETPOSTSLIKED = "notification_getpostsliked"
let NOTIFICATION_FETCHEVENTS = "notification_fetchevents"
let NOTIFIACTION_SETPUSHNOTIFICATION = "notification_setpushnotification"
let NOTIFIACTION_GETPUSHNOTIFICATION = "notification_getpushnotification"
let NOTIFICATION_FETCHMYEVENTS = "notification_fetchmyevents"
let NOTIFICATION_FETCHALLEVENTS = "notification_fetchallevents"
let NOTIFICATION_DELETEEVENT = "notification_deleteevent"
let NOTIFICATION_DELETEEVENTINVITE = "notification_deleteeventinvite"
let NOTIFICATION_EDITEVENT = "notification_editevent"
let NOTIFICATION_RELOADEDITEDEVENT = "notifReloadEditedEvent"
let NOTIFICATION_RELOADCREATEDEVENT = "notifReloadCreatedEvent"
let NOTIFICATION_FETCHNOTREGISTEREDPECXUSERS = "notifFetchNotRegisteredPecXUsers"
let NOTIFICATION_FETCHNOTFOLLOWINGPECXUSERS = "notifFetchNotFollowingPecXUsers"
let NOTIFICATION_GETNOTIFICATIONLIST = "notification_getList"
let NOTIFICATION_GETUNREADNOTIFICATIONCOUNT = "notification_getUnreadCount"
let NOTIFICATION_SETREADNOTIFICATION = "notification_setRead"
let NOTIFICATION_SHAREMEDIA = "notification_sharemedia"
let NOTIFICATION_SETFEEDBACK = "notification_setfeedback"
let NOTIFIACTION_LIKE_PEEPTEXT = "notification_like_peeptext"
let NOTIFIACTION_DISLIKE_PEEPTEXT = "notification_dislike_peeptext"
let NOTIFIACTION_APPROVEDISAPPROVEFOLLOW = "notification_approvedisapprovefollow"
let NOTIFIACTION_PEEPCODEVERIFICATION = "notification_peepcodeverification"
let NOTIFIACTION_GETPEEPTEXTCONVERSATION = "notification_gePeeptextconversation"
let NOTIFICATION_SENDINVITETOCONTACTS = "notifSendInviteToContacts"
let NOTIFICATION_ACCEPTEVENTINVITATION = "notifAcceptEventInvitation"
let NOTIFICATION_REJECTEVENTINVITATION = "notifRejectEventInvitation"
let NOTIFICATION_FETCHCOUNTRIES = "notification_fetchcountries"
let NOTIFICATION_FETCHREGIONS = "notification_fetchregions"
let NOTIFICATION_FETCHLOCALITIES = "notification_fetchlocalities"
let NOTIFICATION_CHECKSUBMERCHANTACCOUNTEXISTS = "notification_checksubmerchantaccountexists"
let NOTIFICATION_CREATESUBMERCHANTACCOUNT = "notification_createsubmerchantaccount"
let NOTIFICATION_CREATEBRAINTREETOKEN = "notification_createbraintreetoken"
let NOTIFICATION_PROCESSBRAINTREEPAYMENT = "notification_processbraintreepayment"
let NOTIFICATION_GETSAVEDCARDS = "notification_getsavedcards"
let NOTIFIACTION_GETFOLLOWINGPEOPLE = "notification_getfollowingpeople"
let NOTIFIACTION_GETFOLLOWERPEOPLE = "notification_getfollowerpeople"
let NOTIFIACTION_GETUSERPOSTS = "notification_getuserposts"
let NOTIFIACTION_FORWARDMEDIA = "notification_forwardmedia"
let NOTIFIACTION_GETTOPFIVEUSERS = "notification_gettopfiveusers"
let NOTIFICATION_UPDATEPACKAGE = "notification_updatepackage"

let NOTIFICATION_BRAINTREETOKENRECEIVED = "notification_braintreetokenrecieved"

//MARK: - Error messages
let ERROR_KEY = "error"
let ERROR_SERVER = "Something went wrong..Try again."
let ERROR_NO_INTERNET = "No Internet Connection"

//MARK: - Media Content Types
let ContentType_Image: String =                       "image/png"
let ContentType_Movie: String =                       "video/quicktime"  //@"movie/mov"
let ContentType_Audio: String =                       "audio/m4a"

//MARK: - Enums

//MARK: Message Type
public enum MessageType {
    
    case success
    
    /// A styling preset for showing information messages, neutral in color.
    case info
    
    /// A styling preset for showing warning messages. Can be styled with yellow/orange colors.
    case warning
    
    /// A styling preset for showing critical error messages. Usually styled with red color.
    case error
}

//MARK: Media Type
var mediaType = MediaType.none
public enum MediaType {
    case photo //1
    case audio //2
    case video //3
    case none
}

//MARK: Media Category
var mediaCategory = MediaCategory.none
public enum MediaCategory {
    case music //1
    case movie //2
    case photo //3
    case videoGame //4
    case none
}

//MARK: Media Attachment Type
var mediaAttachment = MediaAttachment.none
public enum MediaAttachment {
    case photo //1
    case audio //2
    case video //3
    case none
}

//MARK: Add Contact Type
enum AddContactType: Int {
    case phoneBook    = 1 // Phone book Contacts
    case facebook     = 2 // Facebook
    case twitter      = 3 // Twitter
}

//MARK: Account Type
enum AccountType: Int {
    case normal       = 0 // Normal User
    case facebook     = 1 // Facebook
    case twitter      = 2 // Twitter
    case instagram    = 3 // Instagram
    case all          = 4 // All Social Accounts
}

//MARK: Sync Type
enum SyncType: Int {
    case notFollowingUsers   = 1 // Users who are part of PecX app, but whom logged in user is not following. (Can be followed)
    case notPecXUsers        = 2 // Users who are not part of PecX app. (Can be sent invite to become part of PecX)
}

//MARK: Event Details Source Screen
enum SourceScreenEventDetails: Int {//Screen from which event details screen opened.
    case eventList       = 0 // Event List
    case createEvent     = 1 // Create Event
    case eventInvitation = 2 // Event Invitation
}

//MARK: Event Type
enum EventType: Int {
    case myEvents      = 1 // Private/Public Events - created by me only
    case allEvents     = 2 // Public Events - not created by me
}

//MARK: Sign Up State
enum SignUpState: Int {
    case none      = 0 //None
    case selectPackage      = 1 //Choose a package - basic/silver/gold/platinum
    case registerTouchId    = 2 //Register Touch Id
    case registerPeepCode   = 3 //Register peep code - user generated/system generated
    case setPrivacyLevel    = 4 //Choose account type - Private/Public/Anonymous
    case setTheme           = 5 //Choose Theme - Purple/Orange/Pink/Yellow/Blue/Green
    case inviteFriends      = 6 //Send invite to phone book/facebook/twitter friends
    case home               = 7 //Sign up completed, move to home
}

//MARK: - Date formats
let dateFormatLocal = "MMM d, yyyy"
let dateFormatServer = "yyyy-MM-dd"
let timeFormatLocal = "h:mm a"
let timeFormatServer = "HH:mm:ss"

//MARK: - Theme Colors
let themeColor1_t1 = UIColor(red: 134.0/255.0, green: 20.0/255.0, blue: 160.0/255.0, alpha: 1.0) // Purple - Purple/Pink Theme
let themeColor1_t2 = UIColor(red: 229.0/255.0, green: 119.0/255.0, blue: 8.0/255.0, alpha: 1.0) // Dark Orange - Orange/Mango Theme
let themeColor1_t3 = UIColor(red: 221.0/255.0, green: 52.0/255.0, blue: 147.0/255.0, alpha: 1.0) // Dark Pink - Hot Pink/Light Pink Theme
let themeColor1_t4 = UIColor(red: 46.0/255.0, green: 150.0/255.0, blue: 4.0/255.0, alpha: 1.0) // Dark Green - Yellow/Green Theme
let themeColor1_t5 = UIColor(red: 2.0/255.0, green: 91.0/255.0, blue: 158.0/255.0, alpha: 1.0) // Dark Blue - Royal Blue/Turquoise Theme
let themeColor1_t6 = UIColor(red: 10.0/255.0, green: 104.0/255.0, blue: 0.0/255.0, alpha: 1.0) // Dark Green - Parrot Green Theme

let themeColor2_t1 = UIColor(red: 181.0/255.0, green: 51.0/255.0, blue: 211.0/255.0, alpha: 1.0) // Pink - Purple/Pink Theme
let themeColor2_t2 = UIColor(red: 255.0/255.0, green: 143.0/255.0, blue: 30.0/255.0, alpha: 1.0) // Light Orange - Orange/Mango Theme
let themeColor2_t3 = UIColor(red: 238.0/255.0, green: 83.0/255.0, blue: 170.0/255.0, alpha: 1.0) // Light Pink - Hot Pink/Light Pink Theme
let themeColor2_t4 = UIColor(red: 55.0/255.0, green: 167.0/255.0, blue: 9.0/255.0, alpha: 1.0) // Light Green - Yellow/Green Theme
let themeColor2_t5 = UIColor(red: 6.0/255.0, green: 103.0/255.0, blue: 176.0/255.0, alpha: 1.0) // Light Blue - Royal Blue/Turquoise Theme
let themeColor2_t6 = UIColor(red: 16.0/255.0, green: 138.0/255.0, blue: 3.0/255.0, alpha: 1.0) // Light Green - Parrot Green Theme

//Only Theme 1 has three color combos to be used. In other themes, 3rd color is same as 1st color.
let themeColor3_t1 = UIColor(red: 45.0/255.0, green: 15.0/255.0, blue: 104.0/255.0, alpha: 1.0)  // Dark Purple - Purple/Pink Theme

//MARK: - Theme Names
let themeName_t1 = "Purple"
let themeName_t2 = "Orange"
let themeName_t3 = "Pink"
let themeName_t4 = "Yellow"
let themeName_t5 = "Blue"
let themeName_t6 = "Green"

//MARK: - Theme Id in use
var currentThemeId = "1"

let anonymousUsername = "Anonymous"

// Package Product ID's
let productIDSilver = "1001"
let productIDGold = "1002"
let productIDPlatinum = "1003"
