//
//  AlertConstants.swift
//  C00Peeps
//
//  Created by Arshdeep on 29/09/16.
//  Copyright © 2016 SOTSYS011. All rights reserved.
//

import Foundation
@objc class AlertConstants:NSObject {
    fileprivate override init() {}
    class func loaderTitle_SavingFilteredVideoObjc() -> String {return loaderTitle_SavingFilteredVideo}
    class func alertMsg_prepareVideoErrorObjc() -> String {return alertMsg_prepareVideoError}
}

// MARK: - Alert Titles & Messages

// MARK: Common
let alertMsg_ApiInProgress = "API in Progress"
let alertMsg_SomethingWentWrong = "There is problem in connectivity with server. Please try again"
let alertMsg_NoInternetConnection = "No Internet Connection."

//MARK: - Contact Screen

// MARK: Sign In
let alertMsg_EmptyUsername = "Enter User Name"
let alertMsg_EmptyPasscode = "Enter Password"
let alertMsg_PasswordLengthRule = "Password length should not be less than 8 characters."
let alertMsg_PasswordUppercaseRule = "Password should have at least one uppercase."
let alertMsg_PasswordLowercaseRule = "Password should have at least one lowercase."
let alertMsg_PasswordNumericRule = "Password should have at least one numeric."
let alertMsg_ErrorConnectingTwitter = "Error connecting Twitter. Try again."
let alertMsg_ActivateAccountToLogin = "Please activate your account and login again."


// MARK: Forgot Password
let alertMsg_EmptyEmailId = "Enter Email Address"

//MARK: - Contact Screen

let alertMsg_selectContact = "Please select contact"
let alert_filtererror = "There is some problem in applying the filter on video. Please try again."
let alert_videoerror = "There is some problem in creating video. Please try again."
let alertMsg_sendMedia = "Media sent successfully"
let alertMsg_sendPeepText = "Peep Text Sent"
let loaderTitle_noContact = "No contacts exist..."
let loaderTitle_sendPeepText = "Sending PeepText..."
let loaderTitle_noFollowers = "No followers exist..."
let loaderTitle_searchingContacts = "Searching Contacts..."
let alertMsg_prepareVideoError = "Error in processing video. Please try again."//"Error in preparing video. Please try again."

// MARK: Package
let alertMsg_AskToProceed = "Do you want to proceed?"
let alertMsg_FailedToBuyPackage = "Failed to buy package. Please try again."

// MARK: Sign Up
let alertMsg_EnterRequiredFields = "Please enter required fields."
let alertMsg_EnterValidEmailId = "Please enter valid email address."
let alertMsg_PhoneNumberLimit = "Please enter 10 digit phone number."

// MARK: Touch
let alertMsg_TouchIdNotCongigured = "Touch Id is not configured. Please tap on thumb icon to configure it."

// MARK: Change Account Type
let alertMsg_Confirmation = "Confirmation"
let alertMsg_AccountTypeSetAs = "Your account will be set as "
let alertMsg_ContinueWithThis = ". Do you want to continue with this?"

//MARK: - DigitalMedia Tab
let alertMsg_enterPeepText = "Please enter peep text"
let alertMsg_SelectAttachment = "Select Attachment Type"
let alertMsg_AttachAudio = "Attach Audio"
let alertMsg_AttachPhoto = "Attach Photo"
let alertMsg_AttachVideo = "Attach Video"
let alertMsg_AttachGallery = "Attach Gallery"
let alertMsg_PeepTextDefaultText = "Write Message...max 140 characters"
let alertMsg_PublishMediaDefaultText = "Write a caption ...100 characters limit"
let alertMsg_RecordAudio = "Please record audio"
let alertMsg_SelectCategory = "Please Select Category"
let alertMsg_Movie = "Movie"
let alertMsg_Music = "Music"
let alertMsg_Photo = "Photo"
let alertMsg_VideoGames = "Video Games"
let alertMsg_CameraAccess = "It looks like your privacy settings are preventing you from accessing your camera. You can fix this by doing the following:\n\n1. Touch the OK button below to open the Settings app.\n\n2. Touch Privacy.\n\n3. Turn the Camera on.\n\n4. Open this app and try again."
let alertMsg_AudioAccess = "It looks like your privacy settings are preventing you from recording audio. You can fix this by doing the following:\n\n1. Touch the OK button below to open the Settings app.\n\n2. Touch Privacy.\n\n3. Turn the Microphone on.\n\n4. Open this app and try again."

// MARK: Generate Code
let alertMsg_EmptyCode = "Enter Code"
let alertMsg_CodeRange = "Please enter code between 5 to 9 characters."

let alertMsg_CustomizeScreenConfirmation = "Customizing the theme may take 1 minute time to download images. Are you sure you want to continue?"

// MARK: Add to Contacts
let alertMsg_AllowAppToAccessContacts = "Please allow the app to access your contacts through the Settings."
let alertMsg_SelectFriendsToSendInvite = "Please select friend(s) in order to send invitation."
let alertMsg_AskSendInviteToAll = "Do you want to send invitation to all?"
let alertMsg_AskSendInviteToSelected = "Do you want to send invitation to %d person(s)?"
let alertMsg_InvitesSent = "Your invitation(s) sent successfully."

// MARK: Social Media
let alertMsg_InvalidFBShareableLink = "Please enter a valid shareable link."
let alertMsg_DownloadFacebookToSendAppInvites = "Please download facebook app to send invite to facebook friends."
let alertMsg_DownloadFacebookToShareLink = "Please download facebook app to share this link."

// MARK: Events
let alertMsg_EventDescMaxLength = "Event description can not be more than 400 characters."
let alertMsg_EventDateSmallerThanCurrentDate = "Date/Time can not be less than current date."
let alertMsg_GreaterStartDate = "Start Date/Time can not be greater than End Date/Time."
let alertMsg_StartDateTimeSameAsEndDateTime = "Start Date/Time cannot be same as End Date/Time"
let alertMsg_SmallerExpDate = "Event expiration date should not be less than End date."
let alertMsg_SameFromToTime = "From time can not be same as to time."
let alertMsg_GreaterFromTime = "From time can not be greater than to time."
let alertMsg_EventInvitationSent = "Event invitation sent successfully."
let alertMsg_PaymentDone = "Payment has been done successfully."
let alertMsg_EmptyAmount = "Enter Amount"
let alertMsg_EmptyNoOfTickets = "Enter Quantity of Tickets"
let alertMsg_EmptyExpDate = "Enter Event Expiry Date"
let alertMsg_AcceptEvent = "Please accept event."
let alertMsg_AlreadyRejectedEvent = "Event is already rejected"
let alertMsg_AlreadyAcceptedEvent = "Event is already accepted"

// MARK: Event Payment
let alertMsg_EnterShorterName = "Please enter a shorter first name and last name."
let alertMsg_AllowedCharactersInName = "You can only use letters, _ and - in first name and last name."//You can only use letters, numbers, _ and - in first name and last name."
let alertMsg_UniqueName = "Name already exists. Please choose a different name."
let alertMsg_AllowedWordsInName = "You can not choose all or new as your name."
let alertMsg_InvalidRoutingNumber = "Please enter a valid routing number."
let alertMsg_InvalidAddress = "Please enter a valid address."
let alertMsg_InvalidRegion = "Please enter a valid region."
let alertMsg_InvalidPostalCode = "Please enter a valid postal code."
let alertMsg_InvalidPhone = "Please enter a valid phone number."
let alertMsg_InvalidDOB = "Please enter a valid date of birth."
let alertMsg_InvalidAccountNumber = "Please enter a valid account number."
let alertMsg_InvalidLocality = "Please enter a valid locality."
let alertMsg_InvalidCardNo = "Card number is invalid."
let alertMsg_UnsupportedCardType = "This card type is not supported. Please enter another card number."
let alertMsg_CardAlreadyExists = "This card already exists in your saved cards. Please select this card and initiate payment."
let alertMsg_IncorrectCVV = "CVV is incorrect."
let alertMsg_IncorrectPostalCode = "Postal code is incorrect."
let alertMsg_CardNoMinMaxLimit = "Credit card number must be 12-19 digits."
let alertMsg_CVVMinMaxLimit = "CVV must be 3-4 digits."
let alertMsg_PostalCodeMaxLimit = "Postal Code may contain no more thsn 9 letter or number characters."
let alertMsg_FailedToCreateBraintreeToken = "Failed to initialise payment."

//MARK: - Home
let alertMsg_Report = "Report"
let alertMsg_ShareToFacebook = "Share to Facebook"
let alertMsg_Tweet = "Tweet"
let alertMsg_CopyShareURL = "Copy Share URL"
let alertMsg_NoOneLikedPost =  "No one has liked this post yet !"
let alertMsg_DidNotLikedPost =  "You did not like this post yet !"

//MARK: - Search
let alertMsg_NoUsers = "No users exist"
let alertMsg_NoData = "No data exist"

//MARK: - Comment Screen
let alertMsg_Comment = "Please enter text"
let alertMsg_FetchingComments = "Fetching comments..."
let alertMsg_AddingComments = "Adding comment ..."
let alertMsg_NoComments = "No comments exist"

//MARK: - Settings Screen
let alertMsg_SettingsSaved = "Settings saved."
let alertMsg_AccountUnlinked = "Account Unlinked."
let alertMsg_PasswordChanged = "Password changed successfully."
let alertMsg_PasswordMismatch = "Passwords should match."
let alertMsg_ProfileSaved = "Profile saved successfully."
let alertMsg_NoUsersToInvite = "No users available to invite."
let alertMsg_NoUsersToFollow = "No users available to follow."
let alertMsg_SelectUsersToInvite = "Please select users to invite."
let alertMsg_SelectUsersToFollow = "Please select users to follow."
let alertMsg_EmptyFeedback = "Please give your feedback."
let alertMsg_ReLoginToUpdateTheme = "Please logout to see updated theme."
let alertMsg_SwitchToDefaultTheme = "Are you sure you want to logout and switch to default purple theme?"

//MARK: - Notification Screen
let alertMsg_NoNotification = "No notification exist"

let alertMsg_MaxVideoLimit = "You can upload max 2 minute video"

// MARK: - Alert Button Titles
let alertBtnTitle_OK = "OK"
let alertBtnTitle_Yes = "Yes"
let alertBtnTitle_Cancel = "Cancel"
let alertBtnTitle_Buy = "Buy"
let alertBtnTitle_Paypal = "Paypal"
let alertBtnTitle_CreditCard = "Credit Card"
let alertBtnTitle_PECxNotWorking = "PECx not working"
let alertBtnTitle_GeneralFeedback = "General Feedback"
let alertBtnTitle_AttachPhoto = "Attach Photo"
let alertBtnTitle_AttachVideo = "Attach Video"
let alertBtnTitle_ChooseOptionsFromGallery = "Choose Options From Gallery"
let alertBtnTitle_RemoveDigitalMedia = "Remove Digital Media"
let alertBtnTitle_Gallery = "Gallery"
let alertBtnTitle_Camera = "Camera"
let alertBtnTitle_Logout = "Logout"
let alertBtnTitle_ApplePay = "Apple Pay"

// MARK: - Loader Titles

// MARK: Common
let loaderTitle_PleaseWait = "Please wait..."
let loaderTitle_PullToRefresh = "Pull to refresh"
let loading_Title = "Loading..."
let loadingNoMoreData = "No more data"

// MARK: Sign In
let loaderTitle_SigningUp = "Signing up..."
let loaderTitle_SigningIn = "Signing in..."
let loaderTitle_FetchingProfile = "Fetching profile..."

// MARK: Sign Up
let loaderTitle_UpdatingFields = "Updating fields..."

// MARK: Forgot Password
let loaderTitle_SendingEmail = "Sending email..."

// MARK: Add to Contacts
let loaderTitle_FetchingContacts = "Fetching contacts..."
let loaderTitle_FetchingFriends = "Fetching friends..."
let loaderTitle_SendingInvitation = "Sending Invitation..."

// MARK: Filter
let loaderTitle_SavingFilteredVideo = "Saving filtered video..."

// MARK: Generate Code
let loaderTitle_Submitting = "Submitting..."

// MARK: Customize Screen
let loaderTitle_SavingTheme = "Saving theme..."
let loaderTitle_DownloadingTheme = "Downloading theme..."

// MARK: Social Media
let loaderTitle_ConnectingFacebook = "Connecting Facebook"
let loaderTitle_ConnectingTwitter = "Connecting Twitter"

//MARK: - Home Screen
let loaderTitle_HomeFeed = "Fetching feeds..."
let loaderTitle_ReportinMedia = "Reporting Media ..."
let loaderTitle_LikeMedia = "Like Media ..."
let loaderTitle_DislikeMedia = "Dislike Media ..."
let loaderTitle_SharingMedia = "Sharing Media ..."

//MARK: - Search Screen
let loaderTitle_FetchingUsers = "Fetching users..."

//MARK: - Settings Screen
let loaderTitle_FetchingLikes = "Fetching likes..."
let loaderTitle_Fetching = "Fetching..."
let loaderTitle_Syncing = "Syncing..."
let loaderTitle_SavingProfile = "Saving profile..."
let loaderTitle_SendingFeedback = "Sending feedback..."
let loaderTitle_LoggingOut = "Logging out"

//MARK: - Events
let loaderTitle_FetchingEvents = "Fetching events..."
let loaderTitle_Deleting = "Deleting..."
let loaderTitle_AcceptingInvitation = "Accepting Invitation..."
let loaderTitle_RejectingInvitation = "Rejecting Invitation..."
let loaderTitle_Creating = "Creating..."
let loaderTitle_Updating = "Updating..."
let loaderTitle_InitialisingPayment = "Initialising payment..."
let loaderTitle_FetchingPaymentMethods = "Fetching payment methods..."
let loaderTitle_ProcessingPayment = "Processing Payment..."

//MARK: - Package
let loaderTitle_UpgradingPackage = "Upgrading Package..."
let loaderTitle_InitialisingPurchase = "Initialising purchase..."

