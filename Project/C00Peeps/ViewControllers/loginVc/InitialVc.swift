//
//  loginVc.swift
//  C00Peeps
//
//  Created by SOTSYS011 on 2/25/16.
//  Copyright © 2016 SOTSYS011. All rights reserved.
//

import UIKit

class InitialVc: BaseVC {
    
    @IBOutlet weak var imgVwBg: UIImageView!
    @IBOutlet weak var imgVwLogo: UIImageView!
    @IBOutlet weak var btnSignUp: UIButton!
    @IBOutlet weak var btnSignIn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.

        //open home tab bar if user is already logged in
        navigateToHomeScreen()
        
    }
    
    func navigateToHomeScreen(){
        let userId: String? = Utilities.getUserDetails().id
        if userId != nil && userId != ""
        {
            //self.navigationController?.navigationBarHidden = true
            /*let homeScreen = self.storyboard?.instantiateViewControllerWithIdentifier("tabBarcontroller") as? UITabBarController
            self.presentViewController(homeScreen!, animated: true, completion: nil)*/
            
            
            let st = UIStoryboard(name: "Main", bundle:nil)
            let homeScreen = st.instantiateViewControllerWithIdentifier("tabBarcontroller") as! UITabBarController
            let nav = UINavigationController.init(rootViewController: homeScreen)
            nav.navigationBarHidden = true
            ApplicationDelegate.window?.rootViewController = nav
        }
    }

    
    override func viewWillAppear(animated: Bool) {
        self.navigationController?.navigationBarHidden = true
        
        self.updateViewAccordingToTheme()

    }
    override func viewWillDisappear(animated: Bool) {
        self.navigationController?.navigationBarHidden = false
    }
    
    // MARK: - Update View According To Selected Theme
    func updateViewAccordingToTheme() {
        UINavigationBar.appearance().setBackgroundImage(Utilities().themedImage(img_header), forBarMetrics: .Default)

        imgVwBg.image = Utilities().themedImage(img_bg)
        imgVwLogo.image = Utilities().themedImage(img_logo)
        btnSignUp.setBackgroundImage(Utilities().themedImage(img_btn), forState: .Normal)
        btnSignUp.setBackgroundImage(Utilities().themedImage(img_btnSelected), forState: .Highlighted)
        btnSignIn.setBackgroundImage(Utilities().themedImage(img_btn), forState: .Normal)
        btnSignIn.setBackgroundImage(Utilities().themedImage(img_btnSelected), forState: .Highlighted)
    }
    
    //MARK: - IBAction
    @IBAction func btnSignUp_Clciked(sender: AnyObject)
    {
    }
    @IBAction func btnSignin_Clicked(sender: AnyObject)
    {
        self.performSegueWithIdentifier("LoginSegue", sender: nil)
    }
}