//
//  TouchVc.swift
//  C00Peeps
//
//  Created by SOTSYS011 on 3/10/16.
//  Copyright © 2016 SOTSYS011. All rights reserved.
//

import UIKit
import LocalAuthentication

class TouchVc: BaseVC
{
    @IBOutlet weak var staticLabel: UILabel!
    @IBOutlet weak var btnThumb: UIButton!
    @IBOutlet weak var btnNext: UIButton!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.title = "Touch ID Registration"
        self.navigationItem.setHidesBackButton(true, animated: false)

        let attributedString = NSMutableAttributedString (string:staticLabel.text! as String)
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 5 // Whatever line spacing you want in points
        attributedString.addAttribute(kCTParagraphStyleAttributeName as NSAttributedStringKey, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))
        staticLabel.attributedText = attributedString;

        
        
        // Update View According To Selected Theme
        self.updateViewAccordingToTheme()
    }
    
    // MARK: - Update View According To Selected Theme
    func updateViewAccordingToTheme() {
        btnThumb.setImage(Utilities().themedImage(img_thumb), for: UIControlState())
        btnNext.setBackgroundImage(Utilities().themedImage(img_btnSelected), for: UIControlState())
        btnNext.setBackgroundImage(Utilities().themedImage(img_btnSelected), for: .highlighted)
    }

    // MARK: - Button Actions
    @IBAction func btnTouch_Clicked(_ sender: AnyObject)
    {
        //commented by Anubha
        UIApplication.shared.openURL(URL(string:UIApplicationOpenSettingsURLString)!);
    }
    override func viewWillDisappear(_ animated: Bool) {
        
        super.viewWillDisappear(animated)
    }
    @IBAction func btnNext_Clicked(_ sender: AnyObject) {
         authenticateUser ()
       /*commented by ANubha  for testing
        let GenratedVcObject = self.storyboard?.instantiateViewControllerWithIdentifier("GenrateCodeVc") as? GenrateCodeVc
        self.navigationController?.pushViewController(GenratedVcObject!, animated: true)
        return*/
    }
        /*let GenratedVcObject = self.storyboard?.instantiateViewControllerWithIdentifier("GenrateCodeVc") as? GenrateCodeVc
        self.navigationController?.pushViewController(GenratedVcObject!, animated: true)*/

    func authenticateUser () {
        
        let context = LAContext()
        var error: NSError?
       // let reasonString = "Authentication is needed to access your app."
        // Check if the device can evaluate the policy.
        if context.canEvaluatePolicy(LAPolicy.deviceOwnerAuthenticationWithBiometrics, error: &error) {
                DispatchQueue.main.async(execute: {
                    if Utilities.getSignUpState() != SignUpState.home {
                        Utilities.setSignUpState(SignUpState.registerPeepCode)
                    }

                    let GenratedVcObject = self.storyboard?.instantiateViewController(withIdentifier: "GenrateCodeVc") as? GenrateCodeVc
                    self.navigationController?.pushViewController(GenratedVcObject!, animated: true)
                })

            /*Implemented if we need to authenticate touch id and don't delete this code it may be required further
            [context .evaluatePolicy(LAPolicy.DeviceOwnerAuthenticationWithBiometrics, localizedReason: reasonString, reply: { (success: Bool, evalPolicyError: NSError?) -> Void in
                
                if success {
                    dispatch_async(dispatch_get_main_queue(),{
                        let GenratedVcObject = self.storyboard?.instantiateViewControllerWithIdentifier("GenrateCodeVc") as? GenrateCodeVc
                        self.navigationController?.pushViewController(GenratedVcObject!, animated: true)
                    })
                    
                }
                else{
                    // If authentication failed then show a message to the console with a short description.
                    // In case that the error is a user fallback, then show the password alert view.
                    printCustom(evalPolicyError?.localizedDescription)
                    
                    switch evalPolicyError!.code {
                        
                    case LAError.SystemCancel.rawValue:
                        dispatch_async(dispatch_get_main_queue(),{
                            let ac = UIAlertController(title: "", message: "Authentication was cancelled by the system", preferredStyle: .Alert)
                            ac.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
                            self.presentViewController(ac, animated: true, completion: nil)
                            return
                        })
                    case LAError.UserCancel.rawValue:
                        dispatch_async(dispatch_get_main_queue(),{
                            
                            let ac = UIAlertController(title: "", message: "Authentication was cancelled by the user", preferredStyle: .Alert)
                            ac.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
                            self.presentViewController(ac, animated: true, completion: nil)
                            return

                        })
                    case LAError.UserFallback.rawValue:
                        dispatch_async(dispatch_get_main_queue(),{
                            let ac = UIAlertController(title: "", message: "User selected to enter custom password", preferredStyle: .Alert)
                            ac.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
                            self.presentViewController(ac, animated: true, completion: nil)
                            return
                        })
                        
                    default:
                        dispatch_async(dispatch_get_main_queue(),{
                            let ac = UIAlertController(title: "", message: "Authentication failed", preferredStyle: .Alert)
                            ac.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
                            self.presentViewController(ac, animated: true, completion: nil)
                            return
                        })
                    }
                }
                
            })]*/
        }
        else{
            // If the security policy cannot be evaluated then show a short message depending on the error.
            switch error!.code{
                
            case LAError.Code.touchIDNotEnrolled.rawValue:
             
                DispatchQueue.main.async(execute: {
                    self.showCommonAlert(alertMsg_TouchIdNotCongigured)
                    })
                return
                
            case LAError.Code.passcodeNotSet.rawValue:
                
                DispatchQueue.main.async(execute: {
                    self.showCommonAlert(alertMsg_TouchIdNotCongigured)
                })
                return
                
            default:
                // The LAError.TouchIDNotAvailable case.
                printCustom("Error occurred")
            }
            // Show the custom alert view to allow users to enter the password.
        }
    }
}

