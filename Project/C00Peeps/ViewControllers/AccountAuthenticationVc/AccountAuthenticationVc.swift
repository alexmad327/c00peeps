//
//  AccountAuthenticationVc.swift
//  C00Peeps
//
//  Created by SOTSYS011 on 3/16/16.
//  Copyright © 2016 SOTSYS011. All rights reserved.
//

import UIKit
import SwiftyJSON

class AccountAuthenticationVc:  BaseVC {
    @IBOutlet var btnAsyncrons: UIButton!
    @IBOutlet var btnPublicAccount: UIButton!
    @IBOutlet var btnPrivateAccount: UIButton!
    @IBOutlet weak var imgVwHeader: UIImageView!
    
    var accountType: String?
    var isComingFromSettings:Bool = false

    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        switch Utilities.getUserDetails().privacy_level_id {
        case "2":
            btnPrivateAccount.isSelected = true
            btnPrivateAccount.isUserInteractionEnabled = false
        case "4":
            btnAsyncrons.isSelected = true
            btnAsyncrons.isUserInteractionEnabled = false
        default://"1"
            btnPublicAccount .isSelected = true
            btnPublicAccount.isUserInteractionEnabled = false

        }
       
        if self.isComingFromSettings {
            self.navigationItem.rightBarButtonItem = nil
            self.title = "Change Account Type"
        }
        else {
            self.title = "Set Privacy Level"
            self.navigationItem.setHidesBackButton(true, animated: false)
        }
        
        let userId: String? = Utilities.getUserDetails().id
        if userId == nil || userId == "" {
            Utilities.setSignUpState(SignUpState.setPrivacyLevel)
        }

        // Update View According To Selected Theme
        self.updateViewAccordingToTheme()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Hide tab bar for this controller.
        self.tabBarController?.tabBar.isHidden = true
    }

    // MARK: - Update View According To Selected Theme
    func updateViewAccordingToTheme() {
        imgVwHeader.image = Utilities().themedImage(img_headerPrivacy)
        
        btnPrivateAccount.setImage(Utilities().themedImage(img_private), for: UIControlState())
        btnPrivateAccount.setImage(Utilities().themedImage(img_privateSelected), for: .selected)
        
        btnPublicAccount.setImage(Utilities().themedImage(img_public), for: UIControlState())
        btnPublicAccount.setImage(Utilities().themedImage(img_publicSelected), for: .selected)
        
        btnAsyncrons.setImage(Utilities().themedImage(img_Anonymous), for: UIControlState())
        btnAsyncrons.setImage(Utilities().themedImage(img_AnonymousSelected), for: .selected)
    }
    
    // MARK: - Button Actions
    @IBAction func skipAction(){
        self.navigateToCustomizeScreen()
    }
    
    @IBAction func accountType_Clicked(_ sender: UIButton)
    {
        var accountName = ""
        if sender.tag == 101 {//Private
            accountName = "Private"
        }
        if sender.tag == 102 {//Public
            accountName = "Public"
        }
        if sender.tag == 103 {//Anonymous
            accountName = "Anonymous"
        }
        let alert = UIAlertController(title: alertMsg_Confirmation, message: alertMsg_AccountTypeSetAs + accountName + alertMsg_ContinueWithThis, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: alertBtnTitle_OK, style: UIAlertActionStyle.default, handler: { (UIAlertAction) -> Void in
          self.changeAccountType(sender)
        }))
        alert.addAction(UIAlertAction(title: alertBtnTitle_Cancel, style: UIAlertActionStyle.cancel, handler:nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK: - Call API
    func changeAccountType(_ sender: UIButton) {
        sender.isUserInteractionEnabled = false

        if sender.tag == 101 {//Private
            sender.isSelected = true
            btnPublicAccount .isSelected = false
            btnAsyncrons .isSelected = false
            btnPublicAccount.isUserInteractionEnabled = true
            btnAsyncrons.isUserInteractionEnabled = true
            accountType = "2"
        }
        if sender.tag == 102 {//Public
            sender.isSelected = true
            btnPrivateAccount .isSelected = false
            btnAsyncrons .isSelected = false
            btnPrivateAccount.isUserInteractionEnabled = true
            btnAsyncrons.isUserInteractionEnabled = true
            accountType = "3"
            
        }
        if sender.tag == 103 {//Anonymous
            sender.isSelected = true
            btnPrivateAccount .isSelected = false
            btnPublicAccount.isSelected = false
            btnPrivateAccount.isUserInteractionEnabled = true
            btnPublicAccount.isUserInteractionEnabled = true
            accountType = "4"
        }
        
        ApplicationDelegate.showLoader(loaderTitle_Submitting)
        var parameters = [String: AnyObject]()
        parameters["privacy_level_id"] = accountType! as AnyObject
        parameters["user_id"] = Utilities.getUserDetails().id as AnyObject
        APIManager.sharedInstance.requestSaveAccountType(parameters, Target: self)
    }
    
    //MARK: - PeepCode Response
    func responseAccountType (_ notify: Foundation.Notification)
    {
         NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFIACTION_SETACCOUNTTYPE), object: nil)
        ApplicationDelegate.hideLoader ()
        let swiftyJsonVar = JSON(notify.object!)
        let message = swiftyJsonVar["message"].stringValue
        let status = swiftyJsonVar["status"].intValue
        
        if swiftyJsonVar [ERROR_KEY].exists()
        {
            self.showCommonAlert(swiftyJsonVar[ERROR_KEY].stringValue)
        }
        else if (status == 1)
        {
            // Update Privacy Level ID.
            let userDetail:UserDetail = Utilities.getUserDetails()
            userDetail.privacy_level_id = accountType!
            Utilities.setUserDetails(userDetail)
            printCustom("updated user details privacy_level_id: \(Utilities.getUserDetails().privacy_level_id)")
            
            if self.isComingFromSettings {
                self.navigationController?.popViewController(animated: true)
            }
            else {
                self.navigateToCustomizeScreen()
            }
        }
        else if (status == 0) //error response
        {
            let responseErrorMessage = swiftyJsonVar["response"].stringValue
            self.showCommonAlert(responseErrorMessage)
        }
        else{
           self.showCommonAlert(message)
        }

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        btnPublicAccount .setImage(UIImage(named: ""), for: UIControlState())
        btnPrivateAccount .setImage(UIImage(named: ""), for: UIControlState())
        btnAsyncrons .setImage(UIImage(named: ""), for: UIControlState())

    }
    
    func navigateToCustomizeScreen() {
        if Utilities.getSignUpState() != SignUpState.home {
            Utilities.setSignUpState(SignUpState.setTheme)
        }
        
        let controller : CustomizeScreenVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CustomizeScreenVc") as! CustomizeScreenVc
        self.navigationController?.pushViewController(controller, animated: true)

    }
}
