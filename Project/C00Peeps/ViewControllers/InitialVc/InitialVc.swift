//
//  loginVc.swift
//  C00Peeps
//
//  Created by SOTSYS011 on 2/25/16.
//  Copyright © 2016 SOTSYS011. All rights reserved.
//

import UIKit

class InitialVc: BaseVC {
    
    @IBOutlet weak var imgVwBg: UIImageView!
    @IBOutlet weak var imgVwLogo: UIImageView!
    @IBOutlet weak var btnSignUp: UIButton!
    @IBOutlet weak var btnSignIn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.

        let userId: String? = Utilities.getUserDetails().id
        if userId != nil && userId != "" {
            switch Utilities.getSignUpState() {
            case .selectPackage:
                self.navigateToSelectPackage()
                
            case .registerTouchId:
                self.navigateToTouchIdOrRegisterPeepCode()
                
            case .registerPeepCode:
                self.navigateToTouchIdOrRegisterPeepCode()
                
            case .setPrivacyLevel:
                self.navigatePrivacyLevelSetup()
                
            case .setTheme:
                self.navigateToThemeSetup()
                
            case .inviteFriends:
                self.navigateToInviteFriends()
                
            case .home:
                self.navigateToHomeScreen()
                
            case .none:
                printCustom("None")
            }
        }
    }
  
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        
        self.updateViewAccordingToTheme()

    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    // MARK: - Update View According To Selected Theme
    func updateViewAccordingToTheme() {
        UINavigationBar.appearance().setBackgroundImage(Utilities().themedImage(img_header), for: .default)

        imgVwBg.backgroundColor = UIColor.red
        imgVwBg.image = Utilities().themedImage(img_bg)
        imgVwLogo.image = Utilities().themedImage(img_logo)
        btnSignUp.setBackgroundImage(Utilities().themedImage(img_btn), for: UIControlState())
        btnSignUp.setBackgroundImage(Utilities().themedImage(img_btnSelected), for: .highlighted)
        btnSignIn.setBackgroundImage(Utilities().themedImage(img_btn), for: UIControlState())
        btnSignIn.setBackgroundImage(Utilities().themedImage(img_btnSelected), for: .highlighted)
    }
    
    //MARK: - IBAction
    @IBAction func btnSignUp_Clciked(_ sender: AnyObject)
    {
    }
    @IBAction func btnSignin_Clicked(_ sender: AnyObject)
    {
        self.performSegue(withIdentifier: "LoginSegue", sender: nil)
    }
    
    //MARK: - Navigate
    func navigateToSelectPackage() {
        self.navigationController?.isNavigationBarHidden = false

        let packageScreenVC = self.storyboard?.instantiateViewController(withIdentifier: "SelectPackageVC") as? SelectPackageVC
        self.navigationController?.pushViewController(packageScreenVC!, animated: false)
    }
    
    func navigateToTouchIdOrRegisterPeepCode() {
        self.navigationController?.isNavigationBarHidden = false

        if Utilities().isTouchIdConfigured() == true {
            DispatchQueue.main.async(execute: {
                let GenratedVcObject = self.storyboard?.instantiateViewController(withIdentifier: "GenrateCodeVc") as? GenrateCodeVc
                self.navigationController?.pushViewController(GenratedVcObject!, animated: false)
            })
        }
        else {
            DispatchQueue.main.async(execute: {
                let TouchVcObject = self.storyboard?.instantiateViewController(withIdentifier: "TouchVc") as? TouchVc
                self.navigationController?.pushViewController(TouchVcObject!, animated: false)
            })
        }
    }
    
    func navigatePrivacyLevelSetup() {
        self.navigationController?.isNavigationBarHidden = false

        let AuthenticationObject = self.storyboard?.instantiateViewController(withIdentifier: "AccountAuthenticationVc") as? AccountAuthenticationVc
        self.navigationController?.pushViewController(AuthenticationObject!, animated: false)
    }
    
    
    func navigateToThemeSetup() {
        self.navigationController?.isNavigationBarHidden = false

        let controller : CustomizeScreenVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CustomizeScreenVc") as! CustomizeScreenVc
        self.navigationController?.pushViewController(controller, animated: false)
    }
    
    func navigateToInviteFriends() {
        self.navigationController?.isNavigationBarHidden = false

        let ContactSectionObject = self.storyboard?.instantiateViewController(withIdentifier: "ContactSelectionVc") as? ContactSelectionVc
        ContactSectionObject?.syncType = SyncType.notPecXUsers
        self.navigationController?.navigationBar.setBackgroundImage(Utilities().themedImage(img_header), for: .default)
        self.navigationController?.pushViewController(ContactSectionObject!, animated: false)
    }
    
    func navigateToHomeScreen(){
        let userId: String? = Utilities.getUserDetails().id
        if userId != nil && userId != ""
        {
            let st = UIStoryboard(name: "Main", bundle:nil)
            let homeScreen = st.instantiateViewController(withIdentifier: "tabBarcontroller") as! UITabBarController
            let nav = UINavigationController.init(rootViewController: homeScreen)
            nav.isNavigationBarHidden = true
            ApplicationDelegate.window?.rootViewController = nav
        }
    }
}
