//
//  ForgotPasswordVC.swift
//  C00Peeps
//
//  Created by Anubha on 15/07/16.
//  Copyright © 2016 SOTSYS011. All rights reserved.
//

import UIKit
import SwiftyJSON

class ForgotPasswordVC: BaseVC {

    @IBOutlet var txtEmail: UITextField!
    @IBOutlet weak var imgVwBg: UIImageView!
    @IBOutlet weak var imgVwLogo: UIImageView!
    @IBOutlet weak var btnSubmit: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Forgot Password"
        txtEmail.text = "ashish.khurana@trantorinc.com"
        SetPaddingView(txtEmail)
        self.updateViewAccordingToTheme()
        self.perform(#selector (ForgotPasswordVC.setTextFieldLines), with: nil, afterDelay: 0.1)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Update View According To Selected Theme
    func updateViewAccordingToTheme() {
        imgVwBg.image = Utilities().themedImage(img_bg)
        imgVwLogo.image = Utilities().themedImage(img_signInLogo)
        
        // Submit
        btnSubmit.setBackgroundImage(Utilities().themedImage(img_btn), for: UIControlState())
        btnSubmit.setBackgroundImage(Utilities().themedImage(img_btnSelected), for: .highlighted)
    }
    
    // MARK: - Design Textfield
    func setTextFieldLines(){
        setSubViewBorder(txtEmail, color: UIColor.white)
    }
    
    // MARK: - Button Actions
    @IBAction func submit_Clicked(_ sender: AnyObject) {
        if txtEmail.text!.isEmpty {
            self.showCommonAlert(alertMsg_EmptyEmailId)
            return
        }
     
        self.forgotPwdAPI()
    }
    
    // MARK: - Call API
    func forgotPwdAPI() {
        ApplicationDelegate.showLoader(loaderTitle_SendingEmail)
        var parameters = [String: AnyObject]()
        parameters["email_id"] = txtEmail.text as AnyObject
        APIManager.sharedInstance.requestForgotPassword(parameters, Target: self)
    }
   
    // MARK: - API Response
    func responseForgotPassword (_ notify: Foundation.Notification) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFIACTION_FORGOTPASSWORD), object: nil)
        
        ApplicationDelegate.hideLoader ()
        
        let swiftyJsonVar = JSON(notify.object!)
        let message = swiftyJsonVar["message"].stringValue
        let status = swiftyJsonVar["status"].intValue
        
        if swiftyJsonVar [ERROR_KEY].exists() {
            self.showCommonAlert(swiftyJsonVar[ERROR_KEY].stringValue)
        }
        else if (status == 1) {
            
            let response = swiftyJsonVar["response"]
            self.showCommonAlert(response.string!)
        }
        else if (status == 0) {
            let response = swiftyJsonVar["response"]
            self.showCommonAlert(response.string!)
        }
        else {
            self.showCommonAlert(message)
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
