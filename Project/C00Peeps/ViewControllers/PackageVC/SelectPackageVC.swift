//
//  SelectPackageVC.swift
//  C00Peeps
//
//  Created by Anubha on 15/09/16.
//  Copyright © 2016 SOTSYS011. All rights reserved.
//

import UIKit
import LocalAuthentication

class SelectPackageVC: BaseVC {
    @IBOutlet weak var imgVwBg: UIImageView!
    @IBOutlet weak var imgVwLogo: UIImageView!
    @IBOutlet weak var tblVwPackages: UITableView!
  
    var packages = ["Basic", "Silver", "Gold", "Platinum"]
    var isComingFromSettings = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Packages"
        
        if isComingFromSettings {
            self.navigationItem.rightBarButtonItem = nil
            self.updatePackageList()
        }
        else {
            self.navigationItem.hidesBackButton = true
        }
        
        // Update View According To Selected Theme
        self.updateViewAccordingToTheme()
    }
    
    func updatePackageList() {
        if isComingFromSettings {
            // User can upgrade to only higher package
            switch Utilities.getUserDetails().package_id {
            case "2":
                packages = ["Gold", "Platinum"]
            case "3":
                packages = ["Platinum"]
            default://1 or None
                packages = ["Silver", "Gold", "Platinum"]
            }
            
            //self.tblVwPackages.reloadData()
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Reload Packages
    func reloadPackages() {
        self.updatePackageList()
        self.tblVwPackages.reloadData()
    }
    
    // MARK: - Update View According To Selected Theme
    func updateViewAccordingToTheme() {
        imgVwBg.image = Utilities().themedImage(img_bg)
        imgVwLogo.image = Utilities().themedImage(img_signInLogo)
    }

    @IBAction func skipAction(){
        if Utilities().isTouchIdConfigured() == true {
            DispatchQueue.main.async(execute: {
                self.navigateToRegisterPeepCode()
            })
        }
        else {
            DispatchQueue.main.async(execute: {
                self.navigateToRegisterTouchId()
            })
        }
        //self.authenticateUser()
        /* Commented by Anubha in order to direct push on skip button

        let alert = UIAlertController(title: "Alert", message: "Do you want to skip?", preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default, handler: { (UIAlertAction) -> Void in
            self.authenticateUser()
        }))
        alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.Cancel, handler:nil))
        self.presentViewController(alert, animated: true, completion: nil)*/
    }
    func authenticateUser () {
        let context = LAContext()
        var error: NSError?
        // let reasonString = "Authentication is needed to access your app."
        // Check if the device can evaluate the policy.
        if context.canEvaluatePolicy(LAPolicy.deviceOwnerAuthenticationWithBiometrics, error: &error) {
            DispatchQueue.main.async(execute: {
                 self.navigateToRegisterPeepCode()
            })
        }
        else{
            // If the security policy cannot be evaluated then show a short message depending on the error.
            switch error!.code{
            case LAError.Code.touchIDNotEnrolled.rawValue:
                DispatchQueue.main.async(execute: {
                    self.navigateToRegisterTouchId()
                })
                return
                
            case LAError.Code.passcodeNotSet.rawValue:
                
                DispatchQueue.main.async(execute: {
                   self.navigateToRegisterTouchId()
                })
                return
                
            default:
                // The LAError.TouchIDNotAvailable case.
                DispatchQueue.main.async(execute: {
                   self.navigateToRegisterPeepCode()
                })
            }
        }
    }
    @IBAction func selectPackageAction(_ sender: AnyObject) {
        let controller : PackageDetailVC = MainStoryboard.instantiateViewController(withIdentifier: "PackageDetailVC") as! PackageDetailVC
        controller.isComingFromSettings = isComingFromSettings
        
        let btnId:NSString = sender.accessibilityIdentifier! as! NSString
        switch btnId {
        case "1":
            controller.packageType = PackageType.basic.rawValue
            break;
            
        case "2":
            controller.packageType = PackageType.silver.rawValue
            break;
            
        case "3":
            controller.packageType = PackageType.gold.rawValue
            break;
            
        case "4":
            controller.packageType = PackageType.platinum.rawValue
            break;
            
        default:
            break;
        }
        controller.selectPackageDelegate = self
//        // Temporarily Updating Selected Package ID here, need to update it on actually buying that package in PackageDetails screen (In App Purchase Pending).
//        let userDetail:UserDetail = Utilities.getUserDetails()
//        userDetail.package_id = String(controller.packageType)
//        Utilities.setUserDetails(userDetail)
//        printCustom("updated user details package_id: \(Utilities.getUserDetails().package_id)")
        self.navigationController?.pushViewController(controller, animated: true)

    }
    
    //MARK: - UITableViewDataSource
    
    func numberOfSectionsInTableView(_ tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return packages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "PackageCell",for: indexPath)
        
        let packageName = packages[indexPath.row]
        
        let btnPackage:UIButton = cell.viewWithTag(1) as! UIButton
        
        if isComingFromSettings == true{
            
            btnPackage.accessibilityIdentifier = String(Int(Utilities.getUserDetails().package_id)! + indexPath.row + 1)
        }
        else
        {
            btnPackage.accessibilityIdentifier = String(indexPath.row + 1)
        }
        
        btnPackage.setTitle(packageName, for: UIControlState())
        btnPackage.setBackgroundImage(Utilities().themedImage(img_btn), for: UIControlState())
        btnPackage.setBackgroundImage(Utilities().themedImage(img_btnSelected), for: .highlighted)
        btnPackage.addTarget(self, action: #selector(selectPackageAction(_:)), for: .touchUpInside)
        
        return cell
    }
    
    // MARK: - Navigate to screen
    func navigateToRegisterTouchId() {
        if Utilities.getSignUpState() != SignUpState.home {
            Utilities.setSignUpState(SignUpState.registerTouchId)
        }
        
        let TouchVcObject = self.storyboard?.instantiateViewController(withIdentifier: "TouchVc") as? TouchVc
        self.navigationController?.pushViewController(TouchVcObject!, animated: true)
    }
    
    func navigateToRegisterPeepCode() {
        if Utilities.getSignUpState() != SignUpState.home {
            Utilities.setSignUpState(SignUpState.registerPeepCode)
        }
        
        let GenratedVcObject = self.storyboard?.instantiateViewController(withIdentifier: "GenrateCodeVc") as? GenrateCodeVc
        self.navigationController?.pushViewController(GenratedVcObject!, animated: true)
    }
}
