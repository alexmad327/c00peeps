//
//  PackageCell.swift
//  C00Peeps
//
//  Created by Anubha on 16/09/16.
//  Copyright © 2016 SOTSYS011. All rights reserved.
//

import UIKit

class PackageCell: UITableViewCell {

    @IBOutlet weak var lblPackageProperty: UILabel!
    @IBOutlet weak var imgVwTick: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
