//
//  PackageDetailVC.swift
//  C00Peeps
//
//  Created by Anubha on 15/09/16.
//  Copyright © 2016 SOTSYS011. All rights reserved.
//

import UIKit
import StoreKit
import SwiftyJSON

//MARK: - Package Type
public enum PackageType:Int {
    case basic = 1,silver,gold,platinum
}

//MARK: - Package Values
let BasicPackageVaues : NSMutableArray = ["Create up to 2 events per month",
                                          "Public account level",
                                          "Anonymous account level",
                                          "Media expires after 7 days"]

let SilverPackageVaues : NSMutableArray = ["Create up to 5 events per month",
                                           "Create up to 5 custom surveys per month",
                                           "Create up to 5 custom reports per month",
                                           "Private Account Level",
                                           "Media expires after 3 months"]

let GoldPackageVaues : NSMutableArray = ["Create up to 10 events per month",
                                         "Create up to 10 custom surveys per month",
                                         "Create up to 10 custom reports per month",
                                         "Private Account Level",
                                         "Media expires after 6 months"]

let PlatinumPackageVaues : NSMutableArray = ["Create unlimited number of events per month",
                                             "Create unlimited custom surveys per month",
                                             "Create unlimited advance custom reports per month",
                                             "Private Account Level",
                                             "Media expires after 1 year"]

class PackageDetailVC: BaseVC, SKProductsRequestDelegate, SKPaymentTransactionObserver {
    @IBOutlet weak var tblPackageProperties: UITableView!
    @IBOutlet weak var buyBtn: UIButton!
    @IBOutlet weak var rateBtn: UIButton!
    @IBOutlet weak var packageName: UILabel!

    var imageTick:UIImage!
    var packageArray : NSMutableArray = []
    var productIDs: Array<String?> = []
    var productsArray: Array<SKProduct?> = []
    var transactionInProgress = false
    var packageType : Int = 0
    var isComingFromSettings = false
    var selectPackageDelegate:SelectPackageVC?
    
    
    override func viewWillDisappear(_ animated: Bool) {
        
        SKPaymentQueue.default().remove(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Package"
        rateBtn.titleLabel?.numberOfLines = 0
        rateBtn.titleLabel?.textAlignment = NSTextAlignment.center
        tblPackageProperties.rowHeight = UITableViewAutomaticDimension;
        tblPackageProperties.estimatedRowHeight = 44.0;
        tblPackageProperties.setNeedsLayout()
        tblPackageProperties.layoutIfNeeded()
        
        ApplicationDelegate.showLoader(loaderTitle_PleaseWait)
        checkPackage()
        setInAppProducts()
        // Do any additional setup after loading the view.
        
        if packageType == 1 && isComingFromSettings == false
        {
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Next", style: .plain, target: self, action: #selector(btnNextClicked))
        }

        self.buyBtn.isEnabled = false
        
        // Update View According To Selected Theme
        self.updateViewAccordingToTheme()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Update View According To Selected Theme
    func updateViewAccordingToTheme() {
        rateBtn.setBackgroundImage(Utilities().themedImage(img_circle), for: UIControlState())
        buyBtn.setBackgroundImage(Utilities().themedImage(img_btnSelected), for: UIControlState())
        packageName.textColor = Utilities().themedMultipleColor()[0]

        // Used in table view.
        imageTick = Utilities().themedImage(img_tick)
    }
    
    //MARK: - Buttons Actions
    func btnNextClicked() {
        if Utilities().isTouchIdConfigured() == true {
            DispatchQueue.main.async(execute: {
                let GenratedVcObject = self.storyboard?.instantiateViewController(withIdentifier: "GenrateCodeVc") as? GenrateCodeVc
                self.navigationController?.pushViewController(GenratedVcObject!, animated: true)
            })
        }
        else {
            DispatchQueue.main.async(execute: {
                let TouchVcObject = self.storyboard?.instantiateViewController(withIdentifier: "TouchVc") as? TouchVc
                self.navigationController?.pushViewController(TouchVcObject!, animated: true)
            })
        }
    }
    
    @IBAction func btnBuy_Clciked(_ sender: AnyObject)
    {
        if transactionInProgress {
            return
        }
        self.buyBtn.isEnabled = false
        self.showAlertAskToProceedWithTransaction()
    }
    
    // MARK: - TableView Datasource

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return packageArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell
    {
        let cell : PackageCell! = tblPackageProperties.dequeueReusableCell(withIdentifier: "PackageCell",for: indexPath) as! PackageCell

        cell.lblPackageProperty.text = packageArray.object(at: indexPath.row) as? String
        cell.imgVwTick.image = imageTick
        
        return cell
    }
    
    //MARK: - Set In App Products
    func setInAppProducts(){
        
//        productIDs.append("com.coopeep.pecx.basic")
       /* productIDs.append("com.coopeep.pecx.silver")
        productIDs.append("com.coopeep.pecx.gold")
        productIDs.append("com.coopeep.pecx.platinum")*/
        productIDs.append(productIDSilver) //- Silver
        productIDs.append(productIDGold) //- Gold
        productIDs.append(productIDPlatinum) //- Platinum
        requestProductInfo()
        SKPaymentQueue.default().add(self)
    }
    
    func requestProductInfo() {
        if SKPaymentQueue.canMakePayments() {
            let productIdentifiers = NSSet(array: productIDs)
            let productRequest = SKProductsRequest(productIdentifiers  : productIdentifiers as! Set<String>)
            productRequest.delegate = self
            productRequest.start()
        }
        else {
            printCustom("Cannot perform In App Purchases.")
        }
    }
    
    func checkPackage ()
    {
        switch packageType {
        case 1:
            packageName.text = "The “Basic” package is free."
            //Users can upload photos and record 15 second videos
            rateBtn.setTitle("Free", for: UIControlState())
            packageArray = BasicPackageVaues
            self.buyBtn.isHidden = true
            break;
        case 2:
            packageName.text = "The Silver package costs $15 per month."
            //  User can upload movie trailers (2 min.)
            rateBtn.setTitle("$15\nmonth", for: UIControlState())
            packageArray = SilverPackageVaues
            
            break;
        case 3:
            packageName.text = "The Gold package costs $30 per month."
            // Users can upload up to 10 movies and videos, etc
            rateBtn.setTitle("$30\nmonth", for: UIControlState())
            packageArray = GoldPackageVaues
            
            break;
        case 4:
            packageName.text = "The Platinum package costs $50 per month."
            //  Users can upload a unlimited number of movies and videos
            rateBtn.setTitle("$50\nmonth", for: UIControlState())
            packageArray = PlatinumPackageVaues
            break;
        default:
            break;
        }
    }
    
    // MARK: - SKProductsRequestDelegate method implementation
    
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        printCustom("SKProductsRequest didReceiveResponse")
        if response.products.count != 0 {
            for product in response.products {
                productsArray.append(product )
            }
                   }
        else {
            printCustom("There are no products.")
        }
        ApplicationDelegate.hideLoader()
        self.buyBtn.isEnabled = true

        if response.invalidProductIdentifiers.count != 0 {
            printCustom("invalidProductIdentifiers: \(response.invalidProductIdentifiers.description)")
        }
    }
    
    // MARK: - SKPaymentTransactionObserver method implementation
    
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        
        for transaction in transactions  {
            switch transaction.transactionState {
            case SKPaymentTransactionState.purchased:
                printCustom("Transaction completed successfully.")
                SKPaymentQueue.default().finishTransaction(transaction)
                transactionInProgress = false
                
                // Save selected package for user locally.
                self.saveSelectedPackage()
                
                // Call API to update selected package on server, but don't need to wait for API response to navigate back. If failed to update package at this time, then whenever launching the app, it will be checked that whether selected package was sent to server and if not sent, then its sent again.
                ApplicationDelegate.updatePackageAPI(self.packageType)
                
                if isComingFromSettings {
                    // Navigate back.
                    self.navigateBack()
                }
                else {
                    // Move Next.
                    self.btnNextClicked()
                }
              
            case SKPaymentTransactionState.failed:
                printCustom("Transaction Failed");
                SKPaymentQueue.default().finishTransaction(transaction)
                transactionInProgress = false
                self.buyBtn.isEnabled = true
                ApplicationDelegate.hideLoader()
            default:
                printCustom("transaction.transactionState:\(transaction.transactionState.rawValue)")
            }
        }
    }
    
    // MARK: - Save Selected Package
    func saveSelectedPackage() {
        let userDetail:UserDetail = Utilities.getUserDetails()
        userDetail.package_id = String(self.packageType)
        Utilities.setUserDetails(userDetail)
        printCustom("updated user details package_id: \(Utilities.getUserDetails().package_id)")
    }
    
    // MARK: - Show Alert
    func showAlertAskToProceedWithTransaction() {
        let actionSheetController = UIAlertController(title: "", message: alertMsg_AskToProceed, preferredStyle: UIAlertControllerStyle.actionSheet)
        let buyAction = UIAlertAction(title: alertBtnTitle_Buy, style: UIAlertActionStyle.default) { (action) -> Void in
            var packageIndex = 0
            if self.isComingFromSettings {
                packageIndex = self.packageType - 2
            }
            else {
                packageIndex = self.packageType - 1
            }
            let payment = SKPayment(product: self.productsArray[packageIndex] as! SKProduct)
            
            SKPaymentQueue.default().add(payment)
            self.transactionInProgress = true
        }
        let cancelAction = UIAlertAction(title: alertBtnTitle_Cancel, style: UIAlertActionStyle.cancel, handler: nil)
        actionSheetController.addAction(buyAction)
        actionSheetController.addAction(cancelAction)
        present(actionSheetController, animated: true, completion: nil)
    }

  
    // MARK: - Navigate Back
    
    func navigateBack() {
        if Utilities.getUserDetails().package_id == "4" {
            for viewController in self.navigationController!.viewControllers {
                if viewController.isKind(of: SettingMainViewController.self) {
                    // Move back to settings screen.
                    self.navigationController?.popToViewController(viewController, animated: true)
                    break
                }
            }
        }
        else {
            //Move back to upgrade package screen.
            self.navigationController?.popViewController(animated: true)
            self.selectPackageDelegate?.reloadPackages()
        }
    }
    
}
