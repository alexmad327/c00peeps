//
//  CreateSubmerchantViewController.swift
//  C00Peeps
//
//  Created by Arshdeep on 12/12/16.
//  Copyright © 2016 SOTSYS011. All rights reserved.
//

import UIKit
import IQDropDownTextField
import SwiftyJSON
import REFormattedNumberField

class CreateSubmerchantViewController: BaseVC {
    //MARK: - Outlets
    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPhone: REFormattedNumberField!
    @IBOutlet weak var txtDOB: IQDropDownTextField!
    @IBOutlet weak var txtCountry: IQDropDownTextField!
    @IBOutlet weak var txtRegion: IQDropDownTextField!
    @IBOutlet weak var txtLocality: UITextField!
    @IBOutlet weak var txtStreetAddress: UITextField!
    @IBOutlet weak var txtPostalCode: UITextField!
    @IBOutlet weak var txtAccountNo: UITextField!
    @IBOutlet weak var txtRoutingNo: UITextField!
    @IBOutlet weak var btnCreate: UIButton!
   
    var submerchant:Submerchant?
    var event:Event?
    var isMandatoryAlertShown = false
    var isDOBSelected = false
    var isCountrySelected = false
    var isRegionSelected = false
    var isLocalitySelected = false
    var isPrefilledDataLoaded = false

    var arrCountries = [JSON]()
    var arrRegions = [JSON]()
    var arrLocalities = [JSON]()

    //MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        printCustom("self.submerchant?.id:\(self.submerchant?.id)")
        // Check if updating submerchant account
        if self.submerchant?.id != nil {
            // Change title and create button
            self.title = "Update Submerchant Account"
            self.btnCreate.setTitle("Update", for: UIControlState())
        }
        
        // Design textfields
        self.designTextField()
        
        // Update View According To Selected Theme
        self.updateViewAccordingToTheme()
        
        // Change Placeholder Color
        self.changeTextfieldPlaceholderColor(txtFirstName, placeholderText: txtFirstName.placeholder!, color:UIColor.darkGray)
        self.changeTextfieldPlaceholderColor(txtLastName, placeholderText: txtLastName.placeholder!, color:UIColor.darkGray)
        self.changeTextfieldPlaceholderColor(txtEmail, placeholderText: txtEmail.placeholder!, color:UIColor.darkGray)
        self.changeTextfieldPlaceholderColor(txtPhone, placeholderText: txtPhone.placeholder!, color:UIColor.darkGray)
        self.changeTextfieldPlaceholderColor(txtDOB, placeholderText: txtDOB.placeholder!, color:UIColor.darkGray)
        self.changeTextfieldPlaceholderColor(txtCountry, placeholderText: txtCountry.placeholder!, color:UIColor.darkGray)
        self.changeTextfieldPlaceholderColor(txtRegion, placeholderText: txtRegion.placeholder!, color:UIColor.darkGray)
        self.changeTextfieldPlaceholderColor(txtLocality, placeholderText: txtLocality.placeholder!, color:UIColor.darkGray)
        self.changeTextfieldPlaceholderColor(txtStreetAddress, placeholderText: txtStreetAddress.placeholder!, color:UIColor.darkGray)
        self.changeTextfieldPlaceholderColor(txtPostalCode, placeholderText: txtPostalCode.placeholder!, color:UIColor.darkGray)
        self.changeTextfieldPlaceholderColor(txtAccountNo, placeholderText: txtAccountNo.placeholder!, color:UIColor.darkGray)
        self.changeTextfieldPlaceholderColor(txtRoutingNo, placeholderText: txtRoutingNo.placeholder!, color:UIColor.darkGray)

        // DOB Format
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormatLocal
        self.txtDOB.isOptionalDropDown = true
        self.txtDOB.dropDownMode = IQDropDownMode.datePicker
        self.txtDOB.dateFormatter = dateFormatter
        self.txtDOB.datePicker.maximumDate = Date()
        
        // Enable Phone Format while typing
        self.txtPhone.format = phoneNoFormat
        
        self.txtCountry.isOptionalDropDown = false
        self.txtRegion.isOptionalDropDown = false
        //self.txtCountry.setCustomDoneTarget(self, action: #selector(countrySelected))
        //self.txtRegion.setCustomDoneTarget(self, action: #selector(regionSelected))

        self.txtCountry.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(countrySelected))
        self.txtRegion.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(regionSelected))

        // Call API - Fetch Countries
        self.fetchCountries()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Update View According To Selected Theme
    func updateViewAccordingToTheme() {
        self.btnCreate.setBackgroundImage(Utilities().themedImage(img_btnSelected), for: UIControlState())
    }
    
    //MARK: - Prefill Submerchant Details
    func prefillSubmerchantAccountDetails() {
        self.isPrefilledDataLoaded = true
        
        self.txtFirstName.text = self.submerchant?.firstName
        self.txtLastName.text = self.submerchant?.lastName
        self.txtEmail.text = self.submerchant?.email
        self.txtPhone.text = self.submerchant?.phone
        //Navdeep Following error left
        self.txtPhone.format = txtPhone.text
        //self.txtPhone.formatInput(txtPhone)//show prefilled phone with format
        self.txtDOB.selectedItem = Utilities().convertDateToAnotherFormat(self.submerchant!.dateOfBirth, fromFormat: dateFormatServer, toFormat: dateFormatLocal)
        self.txtCountry.selectedItem = self.submerchantCountryName()
        self.txtRegion.selectedItem = self.submerchantRegionName()
        //self.txtLocality.selectedItem = self.submerchantLocalityName()
        self.txtLocality.text = self.submerchant?.locality
        self.txtStreetAddress.text = self.submerchant?.streetAddress
        self.txtPostalCode.text = self.submerchant?.postalCode
        self.txtAccountNo.text = self.submerchant?.accountNumber
        self.txtRoutingNo.text = self.submerchant?.routingNumber
        
        self.isCountrySelected = true
        self.isRegionSelected = true
        //self.isLocalitySelected = true
        self.isDOBSelected = true
    }
    
    //MARK: - Fetch Country/Region/Locality Name from Id
    func submerchantCountryName() -> String {
        let indexOfCountry = self.arrCountries.map({$0["id"].stringValue}).index(of: self.submerchant!.country)
        let countryNames = self.arrCountries.map({$0["name"].stringValue})
        return countryNames[indexOfCountry!]
    }
    
    func submerchantRegionName() -> String {
        let indexOfRegion = self.arrRegions.map({$0["id"].stringValue}).index(of: self.submerchant!.region)
        let regionNames = self.arrRegions.map({$0["region_name"].stringValue})
        return regionNames[indexOfRegion!]
    }

    /*func submerchantLocalityName() -> String {
        let indexOfLocality = self.arrLocalities.map({$0["id"].stringValue}).indexOf(self.submerchant!.locality)
        let localityNames = self.arrLocalities.map({$0["name"].stringValue})
        return localityNames[indexOfLocality!]
    }*/
    
   /* //MARK: - IQDropDownTextField Delegate
    func textField(textField: IQDropDownTextField, didSelectItem item: String?) {
        printCustom("didSelectItem item:\(item!)")
        if textField == self.txtCountry {
            if item != nil && item != "Select" {
                self.isCountrySelected = true
                let countryIds = self.arrCountries.map({$0["id"].stringValue})
                self.fetchRegions(countryIds[self.txtCountry.selectedRow])
            }
            else {
                self.isCountrySelected = false
            }
        }
        if textField == self.txtRegion {
            if item != nil && item != "Select" {
                self.isRegionSelected = true
                let regionIds = self.arrRegions.map({$0["id"].stringValue})
                self.fetchLocalities(regionIds[self.txtRegion.selectedRow])
            }
            else {
                self.isRegionSelected = false
            }
        }
        if textField == self.txtLocality {
            if item != nil && item != "Select" {
                self.isLocalitySelected = true
            }
            else {
                self.isLocalitySelected = false
            }
        }
    }*/
    
    func textField(_ textField: IQDropDownTextField, didSelectDate date: Date?) {
        if textField == self.txtDOB {
            if date != nil {
                self.isDOBSelected = true
            }
            else {
                self.isDOBSelected = false
            }
        }
    }
    
    
    func countrySelected() {
        printCustom("self.txtCountry.selectedItem:\(self.txtCountry.selectedItem)")
       
        if self.txtCountry.selectedItem != nil && self.txtCountry.selectedItem != "Select" {
            self.txtCountry.setSelectedItem(self.txtCountry.selectedItem, animated: false)

            self.isCountrySelected = true
          
            if self.arrRegions.count == 0 {
                let countryIds = self.arrCountries.map({$0["id"].stringValue})
                self.fetchRegions(countryIds[self.txtCountry.selectedRow])
            }
        }
        else {
            self.isCountrySelected = false
            
            self.txtRegion.setSelectedItem("Select", animated: true)
            //self.txtLocality.setSelectedItem("Select", animated: true)
        }
        
       
    }
    
    func regionSelected() {
       
        printCustom("self.txtRegion.selectedItem:\(self.txtRegion.selectedItem)")
        if self.txtRegion.selectedItem != nil && self.txtRegion.selectedItem != "Select" {
            self.txtRegion.setSelectedItem(self.txtRegion.selectedItem, animated: false)
            self.isRegionSelected = true

//            let regionIds = self.arrRegions.map({$0["id"].stringValue})
//            self.fetchLocalities(regionIds[self.txtRegion.selectedRow])
        }
        else {
            self.isRegionSelected = false
           
            
            //self.txtLocality.setSelectedItem("Select", animated: true)
        }
     
    }
    
    // MARK: - Mark Fields Mandatory
    func markFieldsMandatory() {
        if self.isMandatoryAlertShown {
            // Change Fields Placeholder Color - If text is empty, then show mark red color, otherwise dark gray to indicatory mandatory field.
            var color = UIColor()
            if self.txtFirstName.text == "" {
                color = UIColor.red
            }
            else {
                color = UIColor.darkGray
            }
            self.changeTextfieldPlaceholderColor(self.txtFirstName, placeholderText: self.txtFirstName.placeholder!, color: color)
            
            if self.txtLastName.text == "" {
                color = UIColor.red
            }
            else {
                color = UIColor.darkGray
            }
            self.changeTextfieldPlaceholderColor(self.txtLastName, placeholderText: self.txtLastName.placeholder!, color: color)
            
            if self.txtEmail.text == "" {
                color = UIColor.red
            }
            else {
                color = UIColor.darkGray
            }
            self.changeTextfieldPlaceholderColor(self.txtEmail, placeholderText: self.txtEmail.placeholder!, color: color)
            
            if self.txtPhone.text == "" {
                color = UIColor.red
            }
            else {
                color = UIColor.darkGray
            }
            self.changeTextfieldPlaceholderColor(self.txtPhone, placeholderText: self.txtPhone.placeholder!, color: color)
            
            if !self.isDOBSelected {
                color = UIColor.red
            }
            else {
                color = UIColor.darkGray
            }
            self.changeTextfieldPlaceholderColor(self.txtDOB, placeholderText: self.txtDOB.placeholder!, color: color)
            
            if !self.isCountrySelected {
                color = UIColor.red
            }
            else {
                color = UIColor.darkGray
            }
            self.changeTextfieldPlaceholderColor(self.txtCountry, placeholderText: self.txtCountry.placeholder!, color: color)
            
            if !self.isRegionSelected {
                color = UIColor.red
            }
            else {
                color = UIColor.darkGray
            }
            self.changeTextfieldPlaceholderColor(self.txtRegion, placeholderText: self.txtRegion.placeholder!, color: color)
            
            if self.txtLocality.text == "" {
                color = UIColor.red
            }
            else {
                color = UIColor.darkGray
            }
            self.changeTextfieldPlaceholderColor(self.txtLocality, placeholderText: self.txtLocality.placeholder!, color: color)
            
            if self.txtStreetAddress.text == "" {
                color = UIColor.red
            }
            else {
                color = UIColor.darkGray
            }
            self.changeTextfieldPlaceholderColor(self.txtStreetAddress, placeholderText: self.txtStreetAddress.placeholder!, color: color)
            
            if self.txtPostalCode.text == "" {
                color = UIColor.red
            }
            else {
                color = UIColor.darkGray
            }
            self.changeTextfieldPlaceholderColor(self.txtPostalCode, placeholderText: self.txtPostalCode.placeholder!, color: color)
            
            if self.txtAccountNo.text == "" {
                color = UIColor.red
            }
            else {
                color = UIColor.darkGray
            }
            self.changeTextfieldPlaceholderColor(self.txtAccountNo, placeholderText: self.txtAccountNo.placeholder!, color: color)
            
            if self.txtRoutingNo.text == "" {
                color = UIColor.red
            }
            else {
                color = UIColor.darkGray
            }
            self.changeTextfieldPlaceholderColor(self.txtRoutingNo, placeholderText: self.txtRoutingNo.placeholder!, color: color)
        }
    }
    
    
    // MARK: - Text field Border line
    func designTextField() {
        for scrollView in self.view.subviews {
            if scrollView .isKind(of: UIScrollView.self) {
                for textfield in scrollView.subviews {
                    // Set border line for all textfields in screen.
                    if textfield .isKind(of: UITextField.self) {
                        setSubViewBorder(textfield, color: UIColor.lightGray)
                    }
                }
            }
        }
    }
    
    // MARK: - Textfield Delegates
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        printCustom("textField.placeholder:\(textField.placeholder)")
         self.markFieldsMandatory()
    }
    
    //MARK: - Button Actions
    @IBAction func btnCreateClicked(_ sender: AnyObject) {
        if self.txtFirstName.text == "" || self.txtLastName.text == "" || self.txtEmail.text == "" || self.txtPhone.text == "" || !self.isDOBSelected || !self.isCountrySelected || !self.isRegionSelected || self.txtLocality.text == "" || self.txtStreetAddress.text == "" || self.txtPostalCode.text == "" || self.txtAccountNo.text == "" || self.txtRoutingNo.text == ""  {
            self.isMandatoryAlertShown = true
            self.markFieldsMandatory()
            self.showCommonAlert(alertMsg_EnterRequiredFields)
        }
        else if !Utilities().isValidEmail(txtEmail.text!) {
            self.showCommonAlert(alertMsg_EnterValidEmailId)
        }
        else {
            if self.submerchant?.id == nil {
                //Hit API to create submerchant account and then move to contact screen
                self.createSubmerchantAccount()
            }
            else {
                // Update submercchant account
                self.updateSubmerchantAccount()
            }
        }
    }

    //MARK: - Call API
    func fetchCountries() {
        ApplicationDelegate.showLoader(loaderTitle_UpdatingFields)
        APIManager.sharedInstance.fetchCountries(nil, Target: self)
    }
    
    func fetchRegions(_ countryId:String) {
        ApplicationDelegate.showLoader(loaderTitle_UpdatingFields)
        var parameters = [String: AnyObject]()
        parameters["country_id"] = countryId as AnyObject
        APIManager.sharedInstance.fetchRegions(parameters, Target: self)
    }
    
    /*func fetchLocalities(regionId:String) {
        ApplicationDelegate.showLoader(loaderTitle_UpdatingFields)
        var parameters = [String: AnyObject]()
        parameters["region_id"] = regionId
        APIManager.sharedInstance.fetchLocalities(parameters, Target: self)
    }*/
    
    func createSubmerchantAccount() {
        //2603-151 Glenwood Ave, Raleigh, NC 27608
        /*
         Test account number = 1123581321
         Test routing number = 071101307
         Test addresses = 311 Grand Avenue, Suite 105, Bellingham, WA 98225
         85 West Street, New York City, NY 10006
         202 Martin Hall Auburn, AL 36849
         
        */
        
        ApplicationDelegate.showLoader(loaderTitle_Creating)
      
        APIManager.sharedInstance.requestCreateSubMerchantAccount(paramsSubmerchantAccount(), Target: self)
    }
    
    func updateSubmerchantAccount() {
        ApplicationDelegate.showLoader(loaderTitle_Updating)
        var parameters: [String: AnyObject] = paramsSubmerchantAccount()
        parameters["sub_merchant_id"] = self.submerchant?.sub_merchant_id as AnyObject
        APIManager.sharedInstance.requestUpdateSubMerchantAccount(parameters, Target: self)
    }
    
    func paramsSubmerchantAccount() -> [String: AnyObject] {
        var parameters = [String: AnyObject]()
        parameters["user_id"] = Utilities.getUserDetails().id as AnyObject
        parameters["firstName"] = self.txtFirstName.text as AnyObject
        parameters["lastName"] = self.txtLastName.text as AnyObject
        parameters["email"] = self.txtEmail.text as AnyObject
        parameters["phone"] = self.txtPhone.unformattedText! as AnyObject
        
        parameters["dateOfBirth"] = Utilities().convertDateToAnotherFormat(self.txtDOB.selectedItem!, fromFormat: dateFormatLocal, toFormat: dateFormatServer) as AnyObject
        
        let countryIds = self.arrCountries.map({$0["id"].stringValue})
        parameters["country_id"] = countryIds[self.txtCountry.selectedRow] as AnyObject
        
        let regionIds = self.arrRegions.map({$0["id"].stringValue})
        parameters["region"] = regionIds[self.txtRegion.selectedRow] as AnyObject
        
//        let localityIds = self.arrLocalities.map({$0["id"].stringValue})
//        parameters["locality"] = localityIds[self.txtLocality.selectedRow]
        parameters["locality"] = self.txtLocality.text as AnyObject
        parameters["streetAddress"] = self.txtStreetAddress.text as AnyObject
        parameters["postalCode"] = self.txtPostalCode.text as AnyObject
        parameters["accountNumber"] = self.txtAccountNo.text as AnyObject
        parameters["routingNumber"] = self.txtRoutingNo.text as AnyObject
        return parameters
    }
    
    //MARK: - API Response
    func responseFetchCountries (_ notify: Foundation.Notification) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFICATION_FETCHCOUNTRIES)
            , object: nil)
        
        ApplicationDelegate.hideLoader ()
        let swiftyJsonVar = JSON(notify.object!)
        let message = swiftyJsonVar["message"].stringValue
        let status = swiftyJsonVar["status"].intValue
        
        if swiftyJsonVar [ERROR_KEY].exists() {
            self.txtCountry.itemList = []
            self.showCommonAlert(swiftyJsonVar[ERROR_KEY].stringValue)
        }
        else if (status == 1) {
            let response = swiftyJsonVar["response"]
            printCustom("response:\(response)")
            
            self.isCountrySelected = true
            self.arrCountries = response.arrayValue
            self.txtCountry.itemList = self.arrCountries.map({$0["name"].stringValue})
            
            if self.submerchant?.id != nil && !isPrefilledDataLoaded {
                if self.arrRegions.count == 0 {
                    self.fetchRegions(self.submerchant!.country)
                }
            }
            else if self.submerchant?.id == nil && !isPrefilledDataLoaded
            {
                if self.arrRegions.count == 0 {
                    
                    let countryId:String = self.arrCountries[0]["id"].stringValue
                    self.fetchRegions(countryId)
                }
            }
        }
        else if (status == 0) { // Error response
            self.txtCountry.itemList = []

            let responseErrorMessage = swiftyJsonVar["response"].stringValue
            self.showCommonAlert(responseErrorMessage)
        }
        else {
            self.txtCountry.itemList = []

            self.showCommonAlert(message)
        }
    }
    
    func responseFetchRegions (_ notify: Foundation.Notification) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFICATION_FETCHREGIONS), object: nil)
        
        ApplicationDelegate.hideLoader ()
        let swiftyJsonVar = JSON(notify.object!)
        let message = swiftyJsonVar["message"].stringValue
        let status = swiftyJsonVar["status"].intValue
        
        if swiftyJsonVar [ERROR_KEY].exists() {
            self.txtRegion.itemList = []

            self.showCommonAlert(swiftyJsonVar[ERROR_KEY].stringValue)
        }
        else if (status == 1) {
            let response = swiftyJsonVar["response"]
            printCustom("response:\(response)")
            
            self.isRegionSelected = true
            self.arrRegions = response.arrayValue
            self.txtRegion.itemList = self.arrRegions.map({$0["region_name"].stringValue})
            
            
            if self.submerchant?.region != nil && !isPrefilledDataLoaded  {
                //self.fetchLocalities(self.submerchant!.region)
                self.prefillSubmerchantAccountDetails()
            }
//            else if self.submerchant?.region != nil
//            {
//                self.txtRegion.setSelectedItem("Select", animated: true)
//                //self.txtLocality.setSelectedItem("Select", animated: true)
//                self.txtRegion.becomeFirstResponder()
//            }
        }
        else if (status == 0) { // Error response
            self.txtRegion.itemList = []
            let responseErrorMessage = swiftyJsonVar["response"].stringValue
            self.showCommonAlert(responseErrorMessage)
        }
        else {
            self.txtRegion.itemList = []

            self.showCommonAlert(message)
        }
    }
    
    /*func responseFetchLocalities (notify: NSNotification) {
        NSNotificationCenter.defaultCenter().removeObserver(self, name: NOTIFICATION_FETCHLOCALITIES, object: nil)
        
        ApplicationDelegate.hideLoader ()
        let swiftyJsonVar = JSON(notify.object!)
        let message = swiftyJsonVar["message"].stringValue
        let status = swiftyJsonVar["status"].intValue
        
        if swiftyJsonVar [ERROR_KEY].exists() {
            self.txtLocality.itemList = []
            self.showCommonAlert(swiftyJsonVar[ERROR_KEY].stringValue)
        }
        else if (status == 1) {
            let response = swiftyJsonVar["response"]
            printCustom("response:\(response)")
            
            self.arrLocalities = response.arrayValue
            self.txtLocality.itemList = self.arrLocalities.map({$0["name"].stringValue})
            
            if self.submerchant?.locality != nil && !isPrefilledDataLoaded {
                self.prefillSubmerchantAccountDetails()
                self.enableAllDropdowns()
            }
            else {
                self.txtLocality.setSelectedItem("Select", animated: true)
                self.txtLocality.becomeFirstResponder()
            }
        }
        else if (status == 0) { // Error response
            self.txtLocality.itemList = []

            let responseErrorMessage = swiftyJsonVar["response"].stringValue
            self.showCommonAlert(responseErrorMessage)
        }
        else {
            self.txtLocality.itemList = []

            self.showCommonAlert(message)
        }
    }*/
    
    func responseCreateSubmerchantAccount (_ notify: Foundation.Notification) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFICATION_CREATESUBMERCHANTACCOUNT), object: nil)
        
        ApplicationDelegate.hideLoader ()
        let swiftyJsonVar = JSON(notify.object!)
        let message = swiftyJsonVar["message"].stringValue
        let status = swiftyJsonVar["status"].intValue
        
        if swiftyJsonVar [ERROR_KEY].exists() {
            self.showCommonAlert(swiftyJsonVar[ERROR_KEY].stringValue)
        }
        else if (status == 1) {
            let response = swiftyJsonVar["response"]
            printCustom("response:\(response)")
            
            /*
             {
             message = Success;
             response =     {
             id = "arsh_kaur_805-461-0216";
             message = "SubMerchant Created";
             };
             status = 1;
             }

             */
            if response.count > 0 {
                if self.submerchant?.id == nil {
                    let user:UserDetail = Utilities.getUserDetails()
                    user.submerchantExists = "1"
                    Utilities.setUserDetails(user)
                    
                    self.moveToContactsSelection()
                }
                else {
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
        else if (status == 0) { // Error response
            var responseErrorMessage = swiftyJsonVar["response"].stringValue
            let responseErrorCode = swiftyJsonVar["error_code"].stringValue
            if responseErrorCode != "0" {
                switch responseErrorCode {
                case "82602":
                    responseErrorMessage = alertMsg_EnterShorterName
                case "82603":
                    responseErrorMessage = alertMsg_AllowedCharactersInName
                case "82604":
                    responseErrorMessage = alertMsg_UniqueName
                case "82605":
                    responseErrorMessage = alertMsg_AllowedWordsInName
                default:
                    if responseErrorCode == "82635" || responseErrorCode == "82649" {
                        responseErrorMessage = alertMsg_InvalidRoutingNumber
                    }
                    else if responseErrorCode == "82629" || responseErrorCode == "82661" {
                        responseErrorMessage = alertMsg_InvalidAddress
                    }
                    else if responseErrorCode == "82664" || responseErrorCode == "82668" {
                        responseErrorMessage = alertMsg_InvalidRegion
                    }
                    else if responseErrorCode == "82630" || responseErrorCode == "82662" {
                        responseErrorMessage = alertMsg_InvalidPostalCode
                    }
                    else if responseErrorCode == "82636" || responseErrorCode == "82656" {
                        responseErrorMessage = alertMsg_InvalidPhone
                    }
                    else if responseErrorCode == "82663" || responseErrorCode == "82666" {
                        responseErrorMessage = alertMsg_InvalidDOB
                    }
                    else if responseErrorCode == "82670" || responseErrorCode == "82671" {
                        responseErrorMessage = alertMsg_InvalidAccountNumber
                    }
                    else if responseErrorCode == "82690" || responseErrorCode == "82691" {
                        responseErrorMessage = alertMsg_InvalidLocality
                    }
                    else {
                        //No change in error message
                    }

                }
            }
            self.showCommonAlert(responseErrorMessage)
        }
        else {
            self.showCommonAlert(message)
        }
    }

    //MARK: - Move to screen
    func moveToContactsSelection() {
        //Move to Contacts screen
        let st = UIStoryboard(name: "Digital", bundle:nil)
        let obj = st.instantiateViewController(withIdentifier: "ContactsScreen") as! ContactsScreen
        obj.isComingFromPeepTextFlow = false
        obj.isComingFromEventsFlow = true
        obj.isComingFromCreateSubmerchantFlow = true
        obj.event = event
        
        if self.event!.mediaUrl != nil {
            if self.event!.mediaUrl!.absoluteString.lowercased().hasPrefix("file:///") {// New media - Audio + Photo /Video + Thumbnail
                if event!.mediaImage != nil {
                    obj.imageData = UIImageJPEGRepresentation(self.event!.mediaImage!, 0.5)  // passing image data
                }
                else {
                    mediaAttachment = MediaAttachment.none
                }
            }
            else {//Previous media
                // Media URL
                if self.event!.mediaUrl != nil {
                    obj.mediaFile = self.event!.mediaUrl!.absoluteString
                }
                
                // Media Attachment URL
                if self.event!.mediaAttachmentUrl != nil {
                    obj.mediaFileAttachment = self.event!.mediaAttachmentUrl!.absoluteString
                }
            }
        }
        else {//New Media - Photo only
            if self.event!.mediaImage != nil {
                obj.imageData = Utilities().croppedImage(self.event!.mediaImage!)  // passing image data
            }
            else {
                mediaAttachment = MediaAttachment.none
            }
        }
        
        
        obj.dataPath = self.event?.mediaUrl?.path //passing audio path
        
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension CreateSubmerchantViewController: UITextFieldDelegate {
//MARK: - UITextField Delegates

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == txtFirstName || textField == txtLastName {
            // Create an `NSCharacterSet` set which includes only alphabets.
            let inverseSet = NSCharacterSet(charactersIn:"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_-").inverted
            
            // At every character in this "inverseSet" contained in the string,
            // split the string up into components which exclude the characters
            // in this inverse set
            let components = string.components(separatedBy: inverseSet)
            
            // Rejoin these components
            let filtered = components.joined(separator: "")
            
            // If the original string is equal to the filtered string, i.e. if no
            // inverse characters were present to be eliminated, the input is valid
            // and the statement returns true; else it returns false
            if string == filtered {
                return true
            }
            else {
                return false
            }
        }
        return true
    }
}
