//
//  SavedCardsViewController.swift
//  C00Peeps
//
//  Created by Arshdeep on 04/01/17.
//  Copyright © 2017 SOTSYS011. All rights reserved.
//

import UIKit

class SavedCardsViewController: BaseVC {

    @IBOutlet weak var imgVwHeaderLogo: UIImageView!
    
    var arrCards:[Card]?
    var imageCache = [String:UIImage]()
    var event:Event?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Button Actions
    @IBAction func btnAddPaymentClicked(_ sender: AnyObject) {
        self.performSegue(withIdentifier: "Payment", sender: nil)
    }
   
    //MARK: - UITableViewDataSource
    func numberOfSectionsInTableView(_ tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrCards!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        let cell: SavedCardTableViewCell = tableView.dequeueReusableCell(withIdentifier: "SavedCardCell",for: indexPath) as! SavedCardTableViewCell
        
        let card:Card = arrCards![indexPath.row]
        
        cell.lblMaskedCardNo.text = card.maskedNumber
        
        cell.imgVwCardType.contentMode = .scaleAspectFit
        cell.imgVwCardType.layer.masksToBounds = false
        cell.imgVwCardType.clipsToBounds = true
        if let img = imageCache[card.imageUrl] {
            cell.imgVwCardType.image = img
        }
        else if imageCache[card.imageUrl] == nil {
            //Downloading card image
            URLSession.shared.dataTask(with: URL(string: card.imageUrl)!, completionHandler: { (data, response, error) -> Void in
                
                if error != nil {
                    
                    cell.imgVwCardType.image = nil
                    
                    print(error)
                    return
                }
                
                DispatchQueue.main.async(execute: { () -> Void in
                    let image = UIImage(data: data!)
                    
                    if (image == nil)
                    {
                        cell.imgVwCardType.image = nil
                    }
                    else
                    {
                        cell.imgVwCardType.image = image
                        self.imageCache[card.imageUrl] = cell.imgVwCardType.image
                    }
                })
            }).resume()
        }

        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAtIndexPath indexPath: IndexPath) {
        let card:Card = arrCards![indexPath.row]
        
        let alert =  UIAlertController(title: "Pay with this card?", message: card.maskedNumber + "\n" + card.expirationMonth + "/" + card.expirationYear, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Pay", style: UIAlertActionStyle.default, handler:{ action -> Void in
            
            self.eventBase = self.event!
            self.processPayment(self.event!.id, amount: self.event!.ticketCost, nonce: "", token: card.token, storeInVault: "0")
            
        }))
        alert.addAction(UIAlertAction(title: alertBtnTitle_Cancel, style: UIAlertActionStyle.cancel, handler:nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let cell: UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "AddPaymentMethodCell")!
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 44
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        printCustom("segue.identifier:\(segue.identifier)")
        if segue.identifier == "Payment" {
            let destinationViewController:EventPaymentViewController = segue.destination as! EventPaymentViewController
            destinationViewController.event = self.event
            
        }
    }
    

}
