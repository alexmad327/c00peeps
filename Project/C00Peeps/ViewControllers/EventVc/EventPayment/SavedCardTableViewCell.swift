//
//  SavedCardTableViewCell.swift
//  C00Peeps
//
//  Created by Arshdeep on 04/01/17.
//  Copyright © 2017 SOTSYS011. All rights reserved.
//

import UIKit

class SavedCardTableViewCell: UITableViewCell {

    @IBOutlet weak var imgVwCardType: UIImageView!
    @IBOutlet weak var lblMaskedCardNo: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
