 //
//  EventPaymentViewController.swift
//  C00Peeps
//
//  Created by Arshdeep on 20/10/16.
//  Copyright © 2016 SOTSYS011. All rights reserved.
//

import UIKit
import IQDropDownTextField
import Braintree
import PassKit
import SwiftyJSON

class EventPaymentViewController: BaseVC, PKPaymentAuthorizationViewControllerDelegate {

    @IBOutlet weak var imgVwHeaderLogo: UIImageView!
    @IBOutlet weak var btnCreditCard: UIButton!
    @IBOutlet weak var txtCardNumber: UITextField!
    @IBOutlet weak var txtExpiryMonth: IQDropDownTextField!
    @IBOutlet weak var txtExpiryYear: IQDropDownTextField!
    @IBOutlet weak var txtSecurityCode: UITextField!
    @IBOutlet weak var txtZipCode: UITextField!
    @IBOutlet weak var btnSaveInfo: UIButton!
    @IBOutlet weak var btnProceed: UIButton!
    @IBOutlet weak var topConstraintProceedButton: NSLayoutConstraint!
    
    var braintreeAPIClient: BTAPIClient!
    var event:Event?
    var isExpiryMonthSelected = false
    var isExpiryYearSelected = false

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.

        self.updateViewAccordingToTheme()
        self.designTextField()
        setSubViewBorder(btnCreditCard, color: UIColor.lightGray)
        
        // By default, save payment info.
        btnSaveInfo.isSelected = true
        
        let date:Date = Date()
        let calendar: Calendar = Calendar.current
        let dateComponentsYear:DateComponents = (calendar as NSCalendar).components(NSCalendar.Unit.year, from: date)
        let dateComponentsMonth:DateComponents = (calendar as NSCalendar).components(NSCalendar.Unit.month, from: date)

        let year = dateComponentsYear.year!
        let month = dateComponentsMonth.month
        let dateFormatter:DateFormatter = DateFormatter()
        var months:[String] = []
        for monthCount in 0...11 {
            let monthName = dateFormatter.monthSymbols[monthCount]
            if month! - 1 == monthCount {
                printCustom("current month: \(month)")
            }
            months.append(monthName)
        }
        printCustom("months:\(months)")
        txtExpiryMonth.itemList = months
       
      
        var years:[String] = []
        for yearCount in year...2200 {//As per braintree, expiration year can be between 1976 and 2200. But, we are keeping that starting year as current year rather than 1976.
            years.append(String(yearCount))
        }
        
        txtExpiryYear.itemList = years

        self.braintreeAPIClient = BTAPIClient(authorization: Utilities.getBraintreeToken())
        
        // Conditionally show Apple Pay button based on device availability
        if !PKPaymentAuthorizationViewController.canMakePayments() {
            self.btnCreditCard.isUserInteractionEnabled = false
        }
      
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Hide tab bar for this controller.
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Update View According To Selected Theme
    func updateViewAccordingToTheme() {
        self.imgVwHeaderLogo.image = Utilities().themedImage(img_logoTopEvents)
        
//        self.btnSaveInfo.setImage(Utilities().themedImage(img_CheckBox), forState: .
//        Normal)
//        
//        self.btnSaveInfo.setImage(Utilities().themedImage(img_checkedCheckBox), forState: .Selected)

        self.btnSaveInfo.setImage(Utilities().themedImage(img_radio), for: UIControlState())
        
        self.btnSaveInfo.setImage(Utilities().themedImage(img_radioSelected), for: .selected)
        
        
        
        self.btnProceed.setBackgroundImage(Utilities().themedImage(img_btnSelected), for: UIControlState())
    }

    // MARK: - Text field Border line
    func designTextField() {
        for scrollView in self.view.subviews {
            if scrollView .isKind(of: UIScrollView.self) {
                for textfield in scrollView.subviews {
                    // Set border line for all textfields in screen.
                    if textfield .isKind(of: UITextField.self) {
                        setSubViewBorder(textfield, color: UIColor.lightGray)
                    }
                }
            }
        }
    }

    //MARK: - IQDropDownTextField Delegate
    func textField(_ textField: IQDropDownTextField, didSelectItem item: String?) {
        if textField == self.txtExpiryMonth {
            if item != nil && item != "Select" {
                self.isExpiryMonthSelected = true
            }
            else {
                self.isExpiryMonthSelected = false
            }
        }
        if textField == self.txtExpiryYear {
            if item != nil && item != "Select" {
                self.isExpiryYearSelected = true
            }
            else {
                self.isExpiryYearSelected = false
            }
        }
    }
    
    // MARK: - Button Actions
    @IBAction func btnCreditCardClicked(_ sender: AnyObject) {
        let actionSheet =  UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        var btnTitle = alertBtnTitle_CreditCard
        if self.btnCreditCard.currentTitle == alertBtnTitle_CreditCard {
            btnTitle = alertBtnTitle_ApplePay
        }
        actionSheet.addAction(UIAlertAction(title: btnTitle, style: UIAlertActionStyle.default, handler:{ action -> Void in
            // Credit Card
            if self.btnCreditCard.currentTitle == alertBtnTitle_CreditCard {
                self.btnCreditCard.setTitle(alertBtnTitle_ApplePay, for: UIControlState())
                self.btnCreditCard.setImage(Utilities().themedImage(img_applePayLogo), for: UIControlState())
                
//                self.txtCardNumber.hidden = true
//                self.txtExpiryMonth.hidden = true
//                self.txtExpiryYear.hidden = true
//                self.txtSecurityCode.hidden = true
//                self.txtZipCode.hidden = true
              
                for scrollView in self.view.subviews {
                    if scrollView .isKind(of: UIScrollView.self) {
                        let scrollVw:UIScrollView = scrollView as! UIScrollView
                        scrollVw.isScrollEnabled = false
                        
                        for view in scrollView.subviews {
                            // Hide all views whose tag is 0.
                            if view.tag == 0 {
                                if view.isKind(of: UITextField.self) {
                                    view.layer.sublayers?.forEach { $0.removeFromSuperlayer() }
                                }
                                view.isHidden = true
                            }
                        }
                    }
                }
                
                self.topConstraintProceedButton.constant = 30
            }
            else {//Apple Pay
                self.btnCreditCard.setTitle(alertBtnTitle_CreditCard, for: UIControlState())
                self.btnCreditCard.setImage(Utilities().themedImage(img_card), for: UIControlState())
                
//                self.btnSaveInfo.hidden = true
//                self.txtCardNumber.hidden = false
//                self.txtExpiryMonth.hidden = false
//                self.txtExpiryYear.hidden = false
//                self.txtSecurityCode.hidden = false
//                self.txtZipCode.hidden = false
//                self.btnSaveInfo.hidden = false
                
                for scrollView in self.view.subviews {
                    if scrollView .isKind(of: UIScrollView.self) {
                        for view in scrollView.subviews {
                            let scrollVw:UIScrollView = scrollView as! UIScrollView
                            scrollVw.isScrollEnabled = true

                            // Show again all views whose tag is 0.
                            if view.tag == 0 {
                                if view.isKind(of: UITextField.self) {
                                    setSubViewBorder(view, color: UIColor.lightGray)
                                }
                                view.isHidden = false
                            }
                        }
                    }
                }

                
                self.topConstraintProceedButton.constant = 283
            }
        }))
        actionSheet.addAction(UIAlertAction(title: alertBtnTitle_Cancel, style: UIAlertActionStyle.cancel, handler:nil))
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    @IBAction func btnSaveInfoClicked(_ sender: AnyObject) {
        if btnSaveInfo.isSelected {
            btnSaveInfo.isSelected = false
        }
        else {
            btnSaveInfo.isSelected = true
        }
    }

    @IBAction func btnProceedClicked(_ sender: AnyObject) {
        // Credit Card
        if self.btnCreditCard.currentTitle == alertBtnTitle_CreditCard {
            let cardNo:NSString = self.txtCardNumber.text! as NSString
            let cvvNo:NSString = self.txtSecurityCode.text! as NSString
            let zipCode:NSString = self.txtZipCode.text! as NSString

            if txtCardNumber.text == "" || !isExpiryMonthSelected || !isExpiryYearSelected || txtSecurityCode.text == "" || txtZipCode.text == "" {
                self.showCommonAlert(alertMsg_EnterRequiredFields)
            }
            else if cardNo.length < 12 || cardNo.length > 19 {
                self.showCommonAlert(alertMsg_CardNoMinMaxLimit)
            }
            else if cvvNo.length < 3 || cvvNo.length > 4 {
                self.showCommonAlert(alertMsg_CVVMinMaxLimit)
            }
            else if zipCode.length > 9 {
                self.showCommonAlert(alertMsg_PostalCodeMaxLimit)
            }
            else {
               // self.showCommonAlert("In Progress")

                self.payWithCreditCard()
//                let alert:UIAlertController = UIAlertController(title: "", message: alertMsg_PaymentDone, preferredStyle: .Alert)
//                let alertAction = UIAlertAction(title: alertBtnTitle_OK, style: .Default, handler: { (UIAlertAction) -> Void in
//                    // ---- Move to my events ----
//                    self.moveToMyEvents()
//                })
//                alert.addAction(alertAction)
//                self.presentViewController(alert, animated: true, completion: nil)
            }
        }
        else {//Apple Pay
            //self.showCommonAlert("In Progress")

          self.payWithApplePay()
        }
    }
    
    
    func payWithCreditCard() {
        ApplicationDelegate.showLoader(loaderTitle_ProcessingPayment)
        /*
         Card Verification
         If enabled, the gateway will verify that credit cards are valid and pass configured AVS/CVV rules before they are stored in the vault. Cards that are not valid will not be stored in the vault. The gateway verifies cards by running a $0 or $1 transaction and then automatically voids it. You can also choose to verify cards on an individual basis if you prefer.
         */
        let cardClient = BTCardClient(apiClient: self.braintreeAPIClient!)
        let card = BTCard(number: self.txtCardNumber.text!, expirationMonth: String(self.txtExpiryMonth.selectedRow + 1), expirationYear: self.txtExpiryYear.selectedItem!, cvv: self.txtSecurityCode.text!)
        card.postalCode = self.txtZipCode.text!
        cardClient.tokenizeCard(card) { (tokenizedCard, error) in
            // Communicate the tokenizedCard.nonce to your server, or handle error
            printCustom("tokenizedCard.nonce:\(tokenizedCard!.nonce)")
            printCustom("self.event!.id:\(self.event!.id)")
            printCustom("self.event!.ticketCost:\(self.event!.ticketCost)")
            printCustom("self.btnSaveInfo.selected:\(self.btnSaveInfo.isSelected)")

            self.eventBase = self.event!
            // Call API to process payment
            self.processPayment(self.event!.id, amount: self.event!.ticketCost, nonce: tokenizedCard!.nonce, token: "", storeInVault: self.btnSaveInfo.isSelected && self.btnSaveInfo.isEnabled ? "1": "0")
        }
    }
    
    func payWithApplePay() {
        let paymentRequest = self.paymentRequest()
        // Example: Promote PKPaymentAuthorizationViewController to optional so that we can verify
        // that our paymentRequest is valid. Otherwise, an invalid paymentRequest would crash our app.
        if let vc = PKPaymentAuthorizationViewController(paymentRequest: paymentRequest)
            as PKPaymentAuthorizationViewController?
        {
            vc.delegate = self
            self.present(vc, animated: true, completion: nil)
        } else {
            self.showCommonAlert("There are no cards in your iPhone Wallet.")
            print("Error: Payment request is invalid.")
        }
    }
    
    func paymentRequest() -> PKPaymentRequest {
        let paymentRequest = PKPaymentRequest()
        paymentRequest.merchantIdentifier = applePayMerchantId;
        paymentRequest.supportedNetworks = [PKPaymentNetwork.amex,
            PKPaymentNetwork.discover,
            PKPaymentNetwork.masterCard,
            PKPaymentNetwork.visa];
        paymentRequest.merchantCapabilities = PKMerchantCapability.capability3DS
        paymentRequest.countryCode = "US"; // e.g. US
        paymentRequest.currencyCode = "USD"; // e.g. USD
        paymentRequest.paymentSummaryItems = [
            PKPaymentSummaryItem(label: self.event!.title, amount: NSDecimalNumber(string: self.event!.ticketCost)),
        ]
        return paymentRequest
    }
    
    func paymentAuthorizationViewController(_ controller: PKPaymentAuthorizationViewController, didAuthorizePayment payment: PKPayment, completion: @escaping (PKPaymentAuthorizationStatus) -> Void) {
        
        // Example: Tokenize the Apple Pay payment
        let applePayClient = BTApplePayClient(apiClient: self.braintreeAPIClient)
        applePayClient.tokenizeApplePay(payment) { (tokenizedApplePayPayment, error) in
            self.dismiss(animated: true, completion: nil)
            
            self.eventBase = self.event!
            self.processPayment(self.event!.id, amount: self.event!.ticketCost, nonce: tokenizedApplePayPayment!.nonce, token: "", storeInVault: "0")
        }
    }
    
    // Be sure to implement paymentAuthorizationViewControllerDidFinish.
    // You are responsible for dismissing the view controller in this method.
    func paymentAuthorizationViewControllerDidFinish(_ controller: PKPaymentAuthorizationViewController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK: - Navigate to screen
   
    func moveToMyEvents() {
        // Pop to root controller in home tab.
        self.navigationController?.popToRootViewController(animated: false)
        
        // Open events tab
        self.tabBarController?.selectedIndex = 4
        
        // Add Pop animation to show pop animation to my events.
        ApplicationDelegate.window!.layer.add(Utilities().addPopAnimation(), forKey: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
