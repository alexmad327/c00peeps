//
//  EventTableViewCell.swift
//  C00Peeps
//
//  Created by Arshdeep on 04/10/16.
//  Copyright © 2016 SOTSYS011. All rights reserved.
//

import UIKit
import SWTableViewCell

class EventTableViewCell: SWTableViewCell {
    @IBOutlet weak var lblEventTitle: UILabel!
    @IBOutlet weak var lblEventDescription: UILabel!
    @IBOutlet weak var lblEventCategory: UILabel!
    @IBOutlet weak var lblEventCategoryName: UILabel!
    @IBOutlet weak var lblEventDate: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
