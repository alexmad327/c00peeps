//
//  SurveyDetailVc.swift
//  C00Peeps
//
//  Created by Mac on 27/01/17.
//  Copyright © 2017 SOTSYS011. All rights reserved.
//

import UIKit

class SurveyDetailVc: BaseVC, AVAudioPlayerDelegate, DownloadLockedMediaDelegate {

    @IBOutlet weak var txtViewEventDescrition: UITextView!
    @IBOutlet weak var lblEventExpiryDate: UILabel!
    
    @IBOutlet weak var btnSurvey: UIButton!
    @IBOutlet weak var imgVwMedia: UIImageView!
    @IBOutlet weak var btnPlayAudio: UIButton!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    
    var event:Event?
    var imageCache = [String:UIImage]()
    let objDownloadLockedMedia = DownloadLockedMedia()
    var audioVideoPlayer:AVPlayer!
    var audioVideoItem:AVPlayerItem!
    var playerLayer:AVPlayerLayer!
    var btnPlayVideo: UIButton!
    var shouldPlayVideo = false
    var isComingFromPaymentSuccess = false
    var isShowSurvey = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.title = event!.title
        
        self.updateViewAccordingToTheme()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Hide tab bar for this controller.
        self.tabBarController?.tabBar.isHidden = true
        
        if isComingFromPaymentSuccess == true
        {
            let leftBtn = UIBarButtonItem(image: Utilities().themedImage(img_backArrow), style: UIBarButtonItemStyle.plain, target: self, action: #selector(SurveyDetailVc.back))
            self.navigationItem.leftBarButtonItem = leftBtn
        }
        
        txtViewEventDescrition.text = event!.desc
        lblEventExpiryDate.text = event!.expirationDate
        
        self.refreshMedia()
    }

    func back(){
        
        self.navigationController?.popToRootViewController(animated: true)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "SurveyLinkSegue" {
            let destinationViewController:UINavigationController = segue.destination as! UINavigationController
            let surveyLinkViewController:SurveyLinkViewController = destinationViewController.childViewControllers.first as! SurveyLinkViewController
            surveyLinkViewController.surveyLinkUrl = self.event!.surveyLink
        }
    }

    @IBAction func btnSurveyClicked(_ sender: AnyObject) {
        if isShowSurvey {
            self.performSegue(withIdentifier: "SurveyLinkSegue", sender: nil)
        }
        else {
            // move back to home - done button pressed.
            self.navigationController?.popToRootViewController(animated: true)
        }
    }
    
    // MARK: - Update View According To Selected Theme
    func updateViewAccordingToTheme() {
        if isShowSurvey {
            btnSurvey.setTitle("Survey", for: .normal)
        }
        else {
            btnSurvey.setTitle("Done", for: .normal)
        }
        btnSurvey.setBackgroundImage(Utilities().themedImage(img_btnSelected), for: .normal)
    }
    
    func showMessage(_ messge:String){
        
        let ac = UIAlertController(title: "", message: messge, preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(ac, animated: true, completion: nil)
    }
    
    func refreshUI(_ indexPathGlobal:IndexPath){
        self.refreshMedia()
    }
    
    func refreshMedia() {
        
        if self.event?.mediaImage == nil {//When fetching event details from api.
            if self.event?.mediaUrl == nil {
             
                //Show default image if there is no image attached
                imgVwMedia.image = Utilities().themedImage(img_movieImage)
            }
            else {
                printCustom("self.event!.mediaUrl!:\(self.event!.mediaUrl!.absoluteString)")
                //Load image from URL
                
                //In case of locked media pick path from local document folder
                if self.event!.isMediaLocked == 1
                {
                    var localPath = ""
                    
                    //if media is sent media
                    if String(self.event!.userId) == Utilities.getUserDetails().id
                    {
                        localPath = Utilities().getLocalMediaPath(self.event!.mediaUrl!.absoluteString, mediaAttachmentType: 0, folderName:localSentFolderName)
                    }
                        //if media is received media
                    else
                    {
                        localPath = Utilities().getLocalMediaPath(self.event!.mediaUrl!.absoluteString, mediaAttachmentType: 0, folderName: localReceivedFolderName)
                    }
                    
                    if localPath.characters.count > 0
                    {
                        let image = UIImage.init(contentsOfFile: localPath)
                        
                        if (image == nil) {
                            
                        }
                        else
                        {
                            self.imgVwMedia.image = image
                            self.imageCache[self.event!.mediaUrl!.absoluteString] = self.imgVwMedia.image
                            
                            self.setMediaAttachmentType()
                        }
                    }
                    else
                    {
                        
                        self.imgVwMedia.image = Utilities().themedImage(img_unlockMedia)
                        
                        //Start media download & unlock process
                        
                        if let mURL = self.event!.mediaUrl
                        {
                            objDownloadLockedMedia.mediaURL = mURL.absoluteString
                        }
                        
                        if let mAttachmentURL = self.event!.mediaAttachmentUrl
                        {
                            objDownloadLockedMedia.mediaAttachmentURL = mAttachmentURL.absoluteString
                        }
                        objDownloadLockedMedia.mediaAttachmentType = self.event!.mediaType
                        objDownloadLockedMedia.downloadLockDeleg = self
                        if String(self.event!.userId) == Utilities.getUserDetails().id {
                            objDownloadLockedMedia.flderName = localSentFolderName
                        }
                        else {
                            objDownloadLockedMedia.flderName = localReceivedFolderName
                        }
                        objDownloadLockedMedia.className = "EventDetailsVc"
                        objDownloadLockedMedia.indexPathGlobal = IndexPath(row: 0, section: 0)// temporary value passed, required but not using in this case.
                        objDownloadLockedMedia.unloackMedia()
                    }
                }
                else
                {
                    if let img = imageCache[self.event!.mediaUrl!.absoluteString] {
                        imgVwMedia.image = img
                        
                        self.setMediaAttachmentType()
                    }
                    else if imageCache[self.event!.mediaUrl!.absoluteString] == nil
                    {
                        //Downloading image
                        self.loadingIndicator.startAnimating()
                        URLSession.shared.dataTask(with: self.event!.mediaUrl!, completionHandler: { (data, response, error) -> Void in
                            DispatchQueue.main.async(execute: { () -> Void in
                                self.loadingIndicator.stopAnimating()
                                
                                if error != nil {
                                    printCustom("error downloading image:\(error)")
                                    
                                }
                                else {
                                    let image = UIImage(data: data!)
                                    if (image == nil) {
                                    
                                    }
                                    else {
                                        self.imgVwMedia.image = image
                                        self.imageCache[self.event!.mediaUrl!.absoluteString] = self.imgVwMedia.image
                                        
                                        self.setMediaAttachmentType()
                                    }
                                }
                            })
                        }).resume()
                    }
                }
            }
        }
        else {//When fetching event details locally.
            imgVwMedia.image = event!.mediaImage!
            
            if mediaAttachment == MediaAttachment.audio {
                self.btnPlayAudio.isHidden = false
            }
            
            if mediaAttachment == MediaAttachment.video {
                self.loadVideo()
            }
        }
        
    }
    
    
    //MARK: - Set Media Attachment Type
    func setMediaAttachmentType() {//Used in case of fetching event details via API.
        switch self.event!.mediaType {
        case 1://Photo
            mediaAttachment = MediaAttachment.photo
            
        case 3://Video
            mediaAttachment = MediaAttachment.video
            self.loadVideo()
            
        default://Audio
            mediaAttachment = MediaAttachment.audio
            self.btnPlayAudio.isHidden = false
        }
    }
    
    @IBAction func btnPlayAudioClicked(_ sender: AnyObject) {
        if audioVideoPlayer != nil {
            //change image of play button
            btnPlayAudio.setImage(Utilities().themedImage(img_play), for: UIControlState())
            
            audioVideoPlayer.pause()
            audioVideoPlayer = nil
        }
        else {
            //change image of play button
            btnPlayAudio.setImage(Utilities().themedImage(img_playSelected), for: UIControlState())
            var url:URL?
            
            if self.event?.isMediaLocked == 1
            {
                var folderName = ""
                if String(self.event!.userId) == Utilities.getUserDetails().id  {
                    folderName = localSentFolderName
                }
                else {
                    folderName = localReceivedFolderName
                }
                let localPath = Utilities().getLocalMediaPath(self.event!.mediaAttachmentUrl!.absoluteString, mediaAttachmentType: 2, folderName:folderName)
                
                url = URL.init(fileURLWithPath: localPath)
            }
            else if self.event?.mediaImage == nil {//When fetching event details locally.
                let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
                let documentsDirectory = paths.first! as String
                let dataPath = documentsDirectory + "/audio.m4a"
                
                url = URL(fileURLWithPath: dataPath)
            }
            else {//When fetching event details from api.
                url = event?.mediaAttachmentUrl
            }
            if (url?.absoluteString.characters.count)! > 0 {
                audioVideoItem = AVPlayerItem.init(url: url!)
                audioVideoPlayer = AVPlayer.init(playerItem: audioVideoItem)
                audioVideoPlayer.play()
                
                NotificationCenter.default.addObserver(self, selector: #selector(itemFailedToPlayToEnd), name: NSNotification.Name.AVPlayerItemFailedToPlayToEndTime, object: nil)
                
                NotificationCenter.default.addObserver(self, selector: #selector(itemDidPlayToEnd), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil)
            }
            
        }
    }
    
    
    func btnPlayVideoPressed() {
        
        if self.btnPlayVideo != nil {
         
            if !self.btnPlayVideo.isHidden {
                pauseOrResumeVideo(true)
            }
            else {
                pauseOrResumeVideo(false)
            }
        }
    }
    
    @IBAction func videoTapped(_ sender: AnyObject) {
        if audioVideoPlayer == nil {
            self.refreshMedia()
        }
        
        if mediaAttachment == MediaAttachment.video {
            self.btnPlayVideoPressed()
        }
    }
    
    // MARK: - Load Video
    func loadVideo() {
        var videoURL:URL?
        
        if self.event?.isMediaLocked == 1
        {
            var folderName = ""
            if String(self.event!.userId) == Utilities.getUserDetails().id  {
                folderName = localSentFolderName
            }
            else {
                folderName = localReceivedFolderName
            }
            
            let localPath = Utilities().getLocalMediaPath(self.event!.mediaAttachmentUrl!.absoluteString, mediaAttachmentType: 3, folderName:folderName)
            
            videoURL = URL.init(fileURLWithPath: localPath)
        }
        else if self.event?.mediaImage == nil {//When fetching event details from API
            videoURL = self.event?.mediaAttachmentUrl
        }
        else {//When fetching event details locally.
            let fileManager = FileManager.default
            let urls = fileManager.urls(for: .documentDirectory, in: .userDomainMask)
            let documentDirectory = urls[0] as URL
            videoURL = documentDirectory.appendingPathComponent("Movie.mp4")
        }
        
        if (videoURL?.absoluteString.count)! > 0
        {
            audioVideoItem = AVPlayerItem.init(url: videoURL!)
            audioVideoPlayer = AVPlayer.init(playerItem: audioVideoItem)
            playerLayer = AVPlayerLayer(player: audioVideoPlayer)
            
            DispatchQueue.main.async {
                self.playerLayer.frame = self.imgVwMedia.frame
                self.playerLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
                self.playerLayer.backgroundColor = UIColor.clear.cgColor
                //self.imgVwMedia.layer.addSublayer(self.playerLayer)
                self.view.layer.addSublayer(self.playerLayer)
                
                let _x: CGFloat = self.imgVwMedia.frame.origin.x/2 + (self.imgVwMedia.frame.size.width/2 - 33/2)
                let _y: CGFloat = self.imgVwMedia.frame.origin.y/2 + (self.imgVwMedia.frame.size.height/2 - 36/2)
                
                self.btnPlayVideo = UIButton.init(frame: CGRect.init(x: _x, y: _y, width: 33, height: 36))
                
                //self.btnPlayVideo = UIButton(frame: CGRect(x: self.imgVwMedia.frame.origin.x/2 + (self.imgVwMedia.frame.size.width/2 - 33/2), y: self.imgVwMedia.frame.origin.y/2 + (self.imgVwMedia.frame.size.height/2 - 36/2), width: 33, height: 36))
                self.btnPlayVideo.setImage(Utilities().themedImage(img_playIcon), for: UIControlState())
                self.btnPlayVideo.addTarget(self, action: #selector(self.btnPlayVideoPressed), for: .touchUpInside)
                self.view.addSubview(self.btnPlayVideo)
            }
            
            if self.btnPlayVideo != nil {
                if self.btnPlayVideo.isHidden {
                    audioVideoPlayer.play()
                }
            }
            
            NotificationCenter.default.addObserver(self, selector: #selector(itemFailedToPlayToEnd), name: NSNotification.Name.AVPlayerItemFailedToPlayToEndTime, object: nil)
            
            NotificationCenter.default.addObserver(self, selector: #selector(itemDidPlayToEnd), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil)
        }
    }
    
    //MARK: - AVPlayerItem Observers
    func itemDidPlayToEnd() {
        
        if self.audioVideoPlayer != nil {
            self.resetPlayerVariables()
            
            if mediaAttachment == MediaAttachment.video {
                // Start video again.
                self.loadVideo()
            }
        }
    }
    
    func itemFailedToPlayToEnd() {
        if self.audioVideoPlayer != nil {
            self.resetPlayerVariables()
        }
    }
    
    // MARK: - Pause/Resume/Stop Video
    
    func pauseOrResumeVideo(_ resume:Bool) {
        if audioVideoPlayer != nil {//Work in case of pause and play
            if resume {
                self.btnPlayVideo.isHidden = true
                audioVideoPlayer.play()
            }
            else {
                self.btnPlayVideo.isHidden = false
                audioVideoPlayer.pause()
            }
        }
        else {//Work in case of stop and play
            if resume {
                self.btnPlayVideo.isHidden = true
                shouldPlayVideo = true
            }
            else {
                self.btnPlayVideo.isHidden = false
                shouldPlayVideo = false
            }
            self.loadVideo()
        }
    }
    
    // MARK: - Reset Video Player
    func resetPlayerVariables() {
        
        if audioVideoPlayer != nil {
            audioVideoPlayer = nil
        }
        
        if audioVideoItem != nil {
            audioVideoItem = nil
        }
        
        if playerLayer != nil{
            playerLayer.removeFromSuperlayer()
            playerLayer = nil
        }
        
        if self.btnPlayVideo != nil {
            self.btnPlayVideo.removeFromSuperview()
            self.btnPlayVideo = nil
        }
        
        if btnPlayAudio != nil {
            if !btnPlayAudio.isHidden {
                btnPlayAudio.setImage(Utilities().themedImage(img_play), for: UIControlState())
            }
        }
        
        NotificationCenter.default.removeObserver(self)
    }
}
