//
//  SurveyLinkViewController.swift
//  C00Peeps
//
//  Created by Arshdeep on 09/01/17.
//  Copyright © 2017 SOTSYS011. All rights reserved.
//

import UIKit

class SurveyLinkViewController: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var webViewSurvey: UIWebView!
    
    //MARK:- Variables
    var surveyLinkUrl:String = ""
    
    //MARK:- View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let url: URL = URL(string: surveyLinkUrl)!
        let urlRequest: URLRequest = URLRequest(url:url)
        self.webViewSurvey.loadRequest(urlRequest)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //MARK:- Button Actions
    @IBAction func btnDoneClicked(_ sender: AnyObject) {
        
        self.dismiss(animated: true, completion: nil)
    }

}
