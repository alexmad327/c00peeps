//
//  EventDetailsVc.swift
//  C00Peeps
//
//  Created by Arshdeep on 06/10/16.
//  Copyright © 2016 SOTSYS011. All rights reserved.
//

import UIKit
import Photos
import SwiftyJSON
import Braintree
//import BraintreeDropIn

class EventDetailsVc: BaseVC, AVAudioPlayerDelegate, DownloadLockedMediaDelegate {
//BTDropInViewControllerDelegate
    @IBOutlet weak var tblVwEventDetails: UITableView!
    @IBOutlet weak var bottomConstraintTblVwEvents: NSLayoutConstraint!
    @IBOutlet weak var rejectIconLayoutCenter: NSLayoutConstraint!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var btnSend: UIButton!
    @IBOutlet weak var imgVwEdit: UIImageView!
    @IBOutlet weak var imgVwSend: UIImageView!
    @IBOutlet weak var imgVwMedia: UIImageView!
    @IBOutlet weak var btnAccept: UIButton!
    @IBOutlet weak var lblStatusAcceptedRejected: UILabel!
    @IBOutlet weak var btnPlayAudio: UIButton!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    
    var event:Event?
    var sourceScreen:SourceScreenEventDetails?
    var isEventAccepted:Bool = false
    var isEventRejected:Bool = false
    var eventNotificationWasAcceptedByOther:Bool = false
    var audioVideoPlayer:AVPlayer!
    var audioVideoItem:AVPlayerItem!
    var playerLayer:AVPlayerLayer!
    var btnPlayVideo: UIButton!
    var shouldPlayVideo = false
    var imageCache = [String:UIImage]()
    let objDownloadLockedMedia = DownloadLockedMedia()
    var notificationDelegate:NotificationViewController?
    var showSurveyLink = false
    var isComingFromSurveyDetailScreen = 0
    var filterApplied = false;
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
       
        self.updateViewAccordingToTheme()
        
        imgVwMedia.layer.borderColor = UIColor(red: 236.0/255.0, green: 237.0/255.0, blue: 236.0/255.0, alpha: 1.0).cgColor
        imgVwMedia.layer.borderWidth = 1.0
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if isComingFromSurveyDetailScreen == 1
        {
            self.navigationController?.popToRootViewController(animated: true)
        }
        else
        {
            // Hide tab bar for this controller.
            self.tabBarController?.tabBar.isHidden = true
            
            resetAcceptRejectStatus()
            
            self.refreshMedia()
            
            NotificationCenter.default.addObserver(self, selector: #selector(self.getSavedCards), name: NSNotification.Name(rawValue: NOTIFICATION_BRAINTREETOKENRECEIVED), object: nil)
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        self.resetPlayerVariables()
        
        NotificationCenter.default.removeObserver(self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func resetAcceptRejectStatus()
    {
        if (sourceScreen == SourceScreenEventDetails.eventList)  {
            
            // Event detail or Free Event or Accepted Event
            bottomConstraintTblVwEvents.constant = 0
            btnAccept.isHidden = true
            self.hideEditAndSendButtons()
        }
        if sourceScreen == SourceScreenEventDetails.eventInvitation {
            
            if eventNotificationWasAcceptedByOther == true
            {
                bottomConstraintTblVwEvents.constant = 0
                btnAccept.isHidden = true
                self.hideEditAndSendButtons()
            }
            else
            {
                rejectIconLayoutCenter.constant = -20
                
                if isEventAccepted == false &&  isEventRejected == false //event is not accepted or rejected
                {
                    //Show both Accept & Reject buttons
                    
                    btnAccept.isHidden = true
                    
                    btnEdit.setTitle("Reject", for: UIControlState())
                    btnSend.setTitle("Accept", for: UIControlState())
                    
                    rejectIconLayoutCenter.constant = -28
                    
                    self.imgVwEdit.image = Utilities().themedImage(img_reject_icon)
                    self.imgVwSend.image = Utilities().themedImage(img_accept_icon)
                }
                else if isEventAccepted == true &&  isEventRejected == false //event is accepted but not rejected
                {
                    //Show already Accepted button
                    
                    btnAccept.isHidden = false
                    btnAccept.setTitle("Accepted", for: UIControlState())
                    
                    self.hideEditAndSendButtons()
                    
                    self.lblStatusAcceptedRejected.text = "Accepted"
                    bottomConstraintTblVwEvents.constant = self.lblStatusAcceptedRejected.frame.size.height+30
                }
                else if isEventAccepted == false &&  isEventRejected == true //event is not accepted but rejected
                {
                    //Show already Rejected button
                    
                    btnAccept.isHidden = false
                    btnAccept.setTitle("Rejected", for: UIControlState())
                    
                    self.hideEditAndSendButtons()
                    
                    self.lblStatusAcceptedRejected.text = "Rejected"
                    bottomConstraintTblVwEvents.constant = self.lblStatusAcceptedRejected.frame.size.height+30
                }
            }
        }
    }
    
    // MARK: - Update View According To Selected Theme
    func updateViewAccordingToTheme() {
        
        btnEdit.setTitle("Edit", for: UIControlState())
        btnSend.setTitle("Send", for: UIControlState())
        
        btnEdit.backgroundColor = Utilities().themedMultipleColor()[0]
        btnSend.backgroundColor = Utilities().themedMultipleColor()[1]
        btnAccept.setBackgroundImage(Utilities().themedImage(img_btnSelected), for: UIControlState())
    }
    
    //MARK: - Download Locked Media Delegate Methods
    
    //MARK: - Response peep code verification
    func responsepeepCodeVerification(_ notify: Foundation.Notification)
    {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFIACTION_PEEPCODEVERIFICATION), object: nil)
        
        let swiftyJsonVar = JSON(notify.object!)
        
        objDownloadLockedMedia.responsepeepCode(swiftyJsonVar)
    }
    
    func showMessage(_ messge:String){
        
        let ac = UIAlertController(title: "", message: messge, preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(ac, animated: true, completion: nil)
    }
    
    func refreshUI(_ indexPathGlobal:IndexPath){
        self.refreshMedia()
    }
    
    func refreshMedia() {
        if self.event?.mediaImage == nil {//When fetching event details from api.
            if self.event?.mediaUrl == nil {
                self.tblVwEventDetails.tableHeaderView = nil
            }
            else {
                printCustom("self.event!.mediaUrl!:\(self.event!.mediaUrl!.absoluteString)")
                //Load image from URL
                
                //In case of locked media pick path from local document folder
                if self.event!.isMediaLocked == 1
                {
                    var localPath = ""
                    
                    //if media is sent media
                    if String(self.event!.userId) == Utilities.getUserDetails().id
                    {
                        localPath = Utilities().getLocalMediaPath(self.event!.mediaUrl!.absoluteString, mediaAttachmentType: 0, folderName:localSentFolderName)
                    }
                        //if media is received media
                    else
                    {
                        localPath = Utilities().getLocalMediaPath(self.event!.mediaUrl!.absoluteString, mediaAttachmentType: 0, folderName: localReceivedFolderName)
                    }
                    
                    if localPath.characters.count > 0
                    {
                        let image = UIImage.init(contentsOfFile: localPath)
                        
                        if (image == nil) {
                            self.tblVwEventDetails.tableHeaderView = nil
                        }
                        else
                        {
                            self.imgVwMedia.image = image
                            self.imageCache[self.event!.mediaUrl!.absoluteString] = self.imgVwMedia.image
                            
                            self.setMediaAttachmentType()
                        }
                    }
                    else
                    {
                        
                        self.imgVwMedia.image = Utilities().themedImage(img_unlockMedia)
                        
                        //Start media download & unlock process
                        
                        if let mURL = self.event!.mediaUrl
                        {
                            objDownloadLockedMedia.mediaURL = mURL.absoluteString
                        }
                        
                        if let mAttachmentURL = self.event!.mediaAttachmentUrl
                        {
                            objDownloadLockedMedia.mediaAttachmentURL = mAttachmentURL.absoluteString
                        }
                        objDownloadLockedMedia.mediaAttachmentType = self.event!.mediaType
                        objDownloadLockedMedia.downloadLockDeleg = self
                        if String(self.event!.userId) == Utilities.getUserDetails().id {
                            objDownloadLockedMedia.flderName = localSentFolderName
                        }
                        else {
                            objDownloadLockedMedia.flderName = localReceivedFolderName
                        }
                        objDownloadLockedMedia.className = "EventDetailsVc"
                        objDownloadLockedMedia.indexPathGlobal = IndexPath(row: 0, section: 0)// temporary value passed, required but not using in this case.
                        objDownloadLockedMedia.unloackMedia()
                    }
                }
                else
                {
                    if let img = imageCache[self.event!.mediaUrl!.absoluteString] {
                        imgVwMedia.image = img
                        
                        self.setMediaAttachmentType()
                    }
                    else if imageCache[self.event!.mediaUrl!.absoluteString] == nil
                    {
                        //Downloading image
                        //self.loadingIndicator.startAnimating()
                       
                        if let imgURL = self.event!.mediaUrl
                        {
                            URLSession.shared.dataTask(with: imgURL, completionHandler: { (data, response, error) -> Void in
                                DispatchQueue.main.async(execute: { () -> Void in
                                    //self.loadingIndicator.stopAnimating()
                                    
                                    if error != nil {
                                        printCustom("error downloading image:\(error)")
                                        self.tblVwEventDetails.tableHeaderView = nil
                                    }
                                    else {
                                        let image = UIImage(data: data!)
                                        if (image == nil) {
                                            self.tblVwEventDetails.tableHeaderView = nil
                                        }
                                        else {
                                            self.imgVwMedia.image = image
                                            self.imageCache[self.event!.mediaUrl!.absoluteString] = self.imgVwMedia.image
                                            
                                            self.setMediaAttachmentType()
                                        }
                                    }
                                })
                            }).resume()
                        }
                        
                    }
                }
            }
        }
        else {//When fetching event details locally.
            imgVwMedia.image = event!.mediaImage!
            
            if mediaAttachment == MediaAttachment.audio {
                self.btnPlayAudio.isHidden = false
            }
            
            if mediaAttachment == MediaAttachment.video {
                self.loadVideo()
            }
        }

    }
    
    
    //MARK: - Set Media Attachment Type
    func setMediaAttachmentType() {//Used in case of fetching event details via API.
        switch self.event!.mediaType {
        case 1://Photo
            mediaAttachment = MediaAttachment.photo
            
        case 3://Video
            mediaAttachment = MediaAttachment.video
            self.loadVideo()
            
        default://Audio
            mediaAttachment = MediaAttachment.audio
            self.btnPlayAudio.isHidden = false
        }
    }
    
    //MARK: - Hide Buttons
    func hideEditAndSendButtons() {
        btnEdit.isHidden = true
        btnSend.isHidden = true
        imgVwEdit.isHidden = true
        imgVwSend.isHidden = true
    }

    //MARK: - Table View Datasource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Return the number of rows
        if showSurveyLink {
            return 10
        }
        else {
            return 9
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 9 {
            let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "eventDetailsSurveyIdentifier")!
            let lblSurvey:UILabel = cell.viewWithTag(2) as! UILabel
            let lblSurveyLink:UILabel = cell.viewWithTag(3) as! UILabel
            
            lblSurvey.textColor = Utilities().themedMultipleColor()[2]
            lblSurveyLink.text = self.event!.surveyLink
            return cell
        }
        else {
            let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "eventDetailsIdentifier")!
            let lblKey:UILabel = cell.viewWithTag(1) as! UILabel
            let lblValue:UILabel = cell.viewWithTag(2) as! UILabel
            switch indexPath.row {
            case 0:
                lblKey.text = "Event Category"
                lblValue.text = event!.category
            case 1:
                lblKey.text = "Event Title"
                lblValue.text = event!.title
            case 2:
                lblKey.text = "Event Description"
                lblValue.text = event!.desc
            case 3:
                lblKey.text = "Location"
                lblValue.text = event!.location
            case 4:
                lblKey.text = "Start Date"
                lblValue.text = event!.fromDate + " at " + event!.fromTime + " to " + event!.toTime
            case 5:
                lblKey.text = "End Date"
                lblValue.text = event!.toDate + " at " + event!.fromTime + " to " + event!.toTime
            case 6:
                lblKey.text = "Ticket Cost"
                lblValue.text = "$" + Utilities().generalizePrice(event!.ticketCost)
            case 7:
                lblKey.text = "Total Tickets"
                lblValue.text = event!.totalTickets
            default:
                lblKey.text = "Event Expiration"
                lblValue.text = event!.expirationDate
            }
            
            return cell
        }
    }
   
    // MARK: - Table view delegate
    
    func tableView(_ tableView: UITableView, didSelectRowAtIndexPath indexPath: IndexPath) {
        if indexPath.row == 9 {
            self.performSegue(withIdentifier: "SurveyLinkSegue", sender: nil)
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: IndexPath) -> CGFloat {
        // To automatically adjust row height according to cell content height.
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAtIndexPath indexPath: IndexPath) -> CGFloat {
        // To automatically adjust row height according to cell content height.
        return UITableViewAutomaticDimension
    }
    

    // MARK: - Button Actions
    @IBAction func btnEditClicked(_ sender: AnyObject) {
        // New Flow
        if sourceScreen == SourceScreenEventDetails.eventInvitation {
            
            //Reject Button
            if isEventAccepted == false && isEventRejected == false
            {
                rejectEventInvitation()
            }
        }
        else
        {
            //Edit Button
            self.moveBackToCreateEventFirstScreen()
        }
    }
    
    @IBAction func btnSendClicked(_ sender: AnyObject) {
        // New Flow
        if sourceScreen == SourceScreenEventDetails.eventInvitation {
            
            //Accept Button
            if isEventAccepted == false && isEventRejected == false
            {
                self.acceptEvent()
            }
        }
        else
        {
           //Send Button
           self.sendEvent()
        }
    }
    
    @IBAction func btnAcceptClicked(_ sender: AnyObject) {
        
        //if event is not accepted and not rejected
        if isEventAccepted == false &&  isEventRejected == false
        {
            self.acceptEvent()
        }
        else if isEventAccepted == true &&  isEventRejected == false
        {
            showMessage(alertMsg_AlreadyAcceptedEvent)
        }
        else if isEventAccepted == false &&  isEventRejected == true
        {
            showMessage(alertMsg_AlreadyRejectedEvent)
        }
    }
  
    
    @IBAction func btnPlayAudioClicked(_ sender: AnyObject) {
        if audioVideoPlayer != nil {
            //change image of play button
            btnPlayAudio.setImage(Utilities().themedImage(img_play), for: UIControlState())
            
            audioVideoPlayer.pause()
            audioVideoPlayer = nil
        }
        else {
            //change image of play button
            btnPlayAudio.setImage(Utilities().themedImage(img_playSelected), for: UIControlState())
            var url:URL?
            
            if self.event?.isMediaLocked == 1
            {
                var folderName = ""
                if String(self.event!.userId) == Utilities.getUserDetails().id  {
                    folderName = localSentFolderName
                }
                else {
                    folderName = localReceivedFolderName
                }
                let localPath = Utilities().getLocalMediaPath(self.event!.mediaAttachmentUrl!.absoluteString, mediaAttachmentType: 2, folderName:folderName)
                
                url = URL.init(fileURLWithPath: localPath)
            }
            else if self.event?.mediaImage == nil {//When fetching event details locally.
                let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
                let documentsDirectory = paths.first! as String
                let dataPath = documentsDirectory + "/audio.m4a"
                
                url = URL(fileURLWithPath: dataPath)
            }
            else {//When fetching event details from api.
                url = event?.mediaAttachmentUrl
            }
            if (url?.absoluteString.characters.count)! > 0 {
                audioVideoItem = AVPlayerItem.init(url: url!)
                audioVideoPlayer = AVPlayer.init(playerItem: audioVideoItem)
                audioVideoPlayer.play()
                
//                NotificationCenter.default.addObserver(self, selector: #selector(itemFailedToPlayToEnd), name: NSNotification.Name.AVPlayerItemFailedToPlayToEndTime, object: nil)
                
                NotificationCenter.default.addObserver(self, selector: #selector(itemDidPlayToEnd), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil)
            }
          
        }
    }
  
    
    func btnPlayVideoPressed() {
        if self.btnPlayVideo != nil {
            if sourceScreen == SourceScreenEventDetails.eventInvitation && !isEventAccepted && !eventNotificationWasAcceptedByOther { //Paid Event/Free Event Invitation - Message needs not to be shown to user who gets event accepted notification OR if event is already accepted by user.
                var alertMsg = alertMsg_AcceptEvent //In order to play video, user needs to first accept event.
                if isEventRejected {
                    alertMsg = alertMsg_AlreadyRejectedEvent
                }
                self.showMessage(alertMsg)
            }
            else {
                if !self.btnPlayVideo.isHidden {
                    pauseOrResumeVideo(true)
                }
                else {
                    pauseOrResumeVideo(false)
                }
            }
        }
       
    }
    
    @IBAction func videoTapped(_ sender: AnyObject) {
        if audioVideoPlayer == nil {
            self.refreshMedia()
        }

        if mediaAttachment == MediaAttachment.video {
            self.btnPlayVideoPressed()
        }
    }
    
    
    // MARK: - Accept Event/Send Event
    func sendEvent() {
        if Utilities.getUserDetails().submerchantExists != "1" && self.event!.ticketCost != "0" {//If merchant account not exists and is paid event, then create submerchant account.
            self.moveToCreateSubmerchantAccount()
            //self.moveToContactsSelection()
        }
        else {
            //If submerchant account already exists then directly move to contact screen.
            self.moveToContactsSelection()
        }
    }
    
    func acceptEvent() {
        
        if self.event!.ticketCost == "0" {//Free Event
            
            //Call API to accept free event invitation.
            self.acceptEventInvitation()
        }
        else {//Paid Event
            printCustom("Utilities.getBraintreeToken():\(Utilities.getBraintreeToken())")
            if Utilities.getBraintreeToken() == "" {//Braintree token not recieved when app started, then fetch here before sending nonce to server.
                ApplicationDelegate.showLoader(loaderTitle_InitialisingPayment)
                ApplicationDelegate.createBraintreeToken()
            }
            else {
                self.getSavedCards()
                
                // self.showDropIn()
                // Move to payment screen to accept paid event invitation.
                // self.moveToPayment()
                
            }
        }
    }
  
    // MARK: - Load Video
    func loadVideo() {
        var videoURL:URL?
        
        if self.event?.isMediaLocked == 1
        {
            var folderName = ""
            if String(self.event!.userId) == Utilities.getUserDetails().id  {
                folderName = localSentFolderName
            }
            else {
                folderName = localReceivedFolderName
            }
         
            let localPath = Utilities().getLocalMediaPath(self.event!.mediaAttachmentUrl!.absoluteString, mediaAttachmentType: 3, folderName:folderName)

            videoURL = URL.init(fileURLWithPath: localPath)
        }
        else if self.event?.mediaImage == nil {//When fetching event details from API
            videoURL = self.event?.mediaAttachmentUrl
        }
        else {//When fetching event details locally.
            let fileManager = FileManager.default
            let urls = fileManager.urls(for: .documentDirectory, in: .userDomainMask)
            let documentDirectory = urls[0] as URL
            videoURL = documentDirectory.appendingPathComponent("Movie.mp4")
        }
        
        if (videoURL?.absoluteString.characters.count)! > 0
        {
            audioVideoItem = AVPlayerItem.init(url: videoURL!)
            audioVideoPlayer = AVPlayer.init(playerItem: audioVideoItem)
            playerLayer = AVPlayerLayer(player: audioVideoPlayer)
            
            DispatchQueue.main.async {
                self.playerLayer.frame = self.imgVwMedia.frame//CGRectMake(self.imgVwMedia.frame.origin.x, self.imgVwMedia.frame.origin.y, self.imgVwMedia.frame.size.width, self.imgVwMedia.frame.size.height)
                self.playerLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
                self.playerLayer.backgroundColor = UIColor.clear.cgColor
                self.tblVwEventDetails.tableHeaderView?.layer.addSublayer(self.playerLayer)//addSublayer(self.playerLayer)
                
                let _x1: CGFloat = self.imgVwMedia.frame.origin.x/2
                let _x: CGFloat =  _x1 + (self.imgVwMedia.frame.size.width/2 - 33/2)
                let _y: CGFloat = self.imgVwMedia.frame.origin.y/2 + (self.imgVwMedia.frame.size.height/2 - 36/2)
                self.btnPlayVideo = UIButton(frame: CGRect(x: _x, y: _y, width: 33, height: 36))
                self.btnPlayVideo.setImage(Utilities().themedImage(img_playIcon), for: UIControlState())
                self.btnPlayVideo.addTarget(self, action: #selector(self.btnPlayVideoPressed), for: .touchUpInside)
                self.tblVwEventDetails.tableHeaderView?.addSubview(self.btnPlayVideo)
            }
            
            if self.btnPlayVideo != nil {
                if self.btnPlayVideo.isHidden {
                    audioVideoPlayer.play()
                }
            }
            
            NotificationCenter.default.addObserver(self, selector: #selector(itemFailedToPlayToEnd), name: NSNotification.Name.AVPlayerItemFailedToPlayToEndTime, object: nil)
            
            NotificationCenter.default.addObserver(self, selector: #selector(itemDidPlayToEnd), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil)
        }
    }

    //MARK: - AVPlayerItem Observers
    func itemDidPlayToEnd() {
        //MARK: - TODO
        
        // Show survey link after video is finished playing.
        if !self.showSurveyLink && sourceScreen == SourceScreenEventDetails.eventInvitation {
            self.showSurveyLink = true
            self.tblVwEventDetails.reloadData()
        }
        if sourceScreen == SourceScreenEventDetails.eventInvitation {
            self.tblVwEventDetails.scrollToRow(at: IndexPath(row: 9, section: 0), at: UITableViewScrollPosition.bottom, animated: true)
        }
     
        if self.audioVideoPlayer != nil {
            self.resetPlayerVariables()
            
            if mediaAttachment == MediaAttachment.video {
                // Start video again.
                self.loadVideo()
            }
        }
    }
    
    func itemFailedToPlayToEnd() {
        if self.audioVideoPlayer != nil {
            self.resetPlayerVariables()
        }
    }
   
    // MARK: - Pause/Resume/Stop Video
    
    func pauseOrResumeVideo(_ resume:Bool) {
        if audioVideoPlayer != nil {//Work in case of pause and play
            if resume {
                self.btnPlayVideo.isHidden = true
                audioVideoPlayer.play()
            }
            else {
                self.btnPlayVideo.isHidden = false
                audioVideoPlayer.pause()
            }
        }
        else {//Work in case of stop and play
            if resume {
                self.btnPlayVideo.isHidden = true
                shouldPlayVideo = true
            }
            else {
                self.btnPlayVideo.isHidden = false
                shouldPlayVideo = false
            }
            self.loadVideo()
        }
    }
    
    // MARK: - Reset Video Player
    func resetPlayerVariables() {
        
        if audioVideoPlayer != nil {
            audioVideoPlayer = nil
        }
        
        if audioVideoItem != nil {
            audioVideoItem = nil
        }
        
        if playerLayer != nil{
            playerLayer.removeFromSuperlayer()
            playerLayer = nil
        }
        
        if self.btnPlayVideo != nil {
            self.btnPlayVideo.removeFromSuperview()
            self.btnPlayVideo = nil
        }
        
        if btnPlayAudio != nil {
            if !btnPlayAudio.isHidden {
                btnPlayAudio.setImage(Utilities().themedImage(img_play), for: UIControlState())
            }
        }
       
        NotificationCenter.default.removeObserver(self)
    }
    
    //MARK: - Move to Screen
    func moveBackToCreateEventFirstScreen() {
        for viewController in self.navigationController!.viewControllers {
            if viewController.isKind(of: CreateEventFirstViewController.self) {
                self.navigationController?.popToViewController(viewController, animated: true)
                break
            }
        }
    }
    
    func moveToCreateSubmerchantAccount() {
        self.performSegue(withIdentifier: "createSubmerchantSegue", sender: nil)
    }
    
    func moveToContactsSelection() {
        //Move to Contacts screen
        let st = UIStoryboard(name: "Digital", bundle:nil)
        let obj = st.instantiateViewController(withIdentifier: "ContactsScreen") as! ContactsScreen
        obj.isComingFromPeepTextFlow = false
        obj.isComingFromEventsFlow = true
        obj.event = event
        obj.filterApplied = filterApplied
     
        if event!.mediaUrl != nil {
            if event!.mediaUrl!.absoluteString.lowercased().hasPrefix("file:///") {// New media - Audio + Photo /Video + Thumbnail
                if event!.mediaImage != nil {
                    obj.imageData = UIImageJPEGRepresentation(event!.mediaImage!, 0.5)  // passing image data
                }
                else {
                    mediaAttachment = MediaAttachment.none
                }
            }
            else {//Previous media
                // Media URL
                if event!.mediaUrl != nil {
                    obj.mediaFile = self.event!.mediaUrl!.absoluteString
                }
                
                // Media Attachment URL
                if event!.mediaAttachmentUrl != nil {
                    obj.mediaFileAttachment = event!.mediaAttachmentUrl!.absoluteString
                }
            }
        }
        else {//New Media - Photo only
            if event!.mediaImage != nil {
                obj.imageData = Utilities().croppedImage(event!.mediaImage!)  // passing image data
            }
            else {
                mediaAttachment = MediaAttachment.none
            }
        }
       
        
        obj.dataPath = event?.mediaUrl?.path //passing audio path
        
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    //MARK: - Call API
    func acceptEventInvitation() {
        ApplicationDelegate.showLoader(loaderTitle_AcceptingInvitation)
        var parameters = [String: AnyObject]()
        parameters["user_id"] = Utilities.getUserDetails().id as AnyObject
        parameters["event_id"] = event!.id as AnyObject
        
        APIManager.sharedInstance.requestAcceptEventInvitation(parameters, Target: self)
    }
    
    func rejectEventInvitation() {
        ApplicationDelegate.showLoader(loaderTitle_RejectingInvitation)
        var parameters = [String: AnyObject]()
        parameters["user_id"] = Utilities.getUserDetails().id as AnyObject
        parameters["event_id"] = event!.id as AnyObject
        
        APIManager.sharedInstance.requestRejectEventInvitation(parameters, Target: self)
    }

    func getSavedCards() {
        if Utilities.getBraintreeToken() == "" {//Braintree token failed to create, show alert.
           self.showCommonAlert(alertMsg_FailedToCreateBraintreeToken)
        }
        else {
            ApplicationDelegate.showLoader(loaderTitle_FetchingPaymentMethods)
            
            var parameters = [String: AnyObject]()
            parameters["user_id"] = Utilities.getUserDetails().id as AnyObject
            APIManager.sharedInstance.requestGetSavedCards(parameters, Target: self)
        }
    }
  
    //MARK: - Reject Event API Response
    func responseRejectEventInvitation(_ notify: Foundation.Notification) {
        
        ApplicationDelegate.hideLoader()
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFICATION_REJECTEVENTINVITATION), object: nil)
        
        let swiftyJsonVar = JSON(notify.object!)
        
        let message = swiftyJsonVar["message"].stringValue
        let status = swiftyJsonVar["status"].intValue
        
        if swiftyJsonVar [ERROR_KEY].exists()
        {
            self.showCommonAlert(swiftyJsonVar[ERROR_KEY].stringValue)
        }
        else if (status == 1) //success response
        {
            // move back to home.
            self.navigationController?.popToRootViewController(animated: true)
        }
        else if (status == 0) //error response
        {
            let responseErrorMessage = swiftyJsonVar["response"].stringValue
            self.showCommonAlert(responseErrorMessage)
        }
        else
        {
            self.showCommonAlert(message)
        }
    }
    
    //MARK: - Accept Event API Response
    func responseAcceptEventInvitation(_ notify: Foundation.Notification) {
        ApplicationDelegate.hideLoader()
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFICATION_ACCEPTEVENTINVITATION), object: nil)
        
        let swiftyJsonVar = JSON(notify.object!)
        
        let message = swiftyJsonVar["message"].stringValue
        let status = swiftyJsonVar["status"].intValue
        
        if swiftyJsonVar [ERROR_KEY].exists()
        {
            self.showCommonAlert(swiftyJsonVar[ERROR_KEY].stringValue)
        }
        else if (status == 1) //success response
        {
            if self.event!.isSurveyAttached == 1
            {
                isEventAccepted = true
                
                //If survey attached then move to Survey Detail screen
                self.performSegue(withIdentifier: "MoveToSurveyDetail", sender: true)
            }
            else
            {
                // If event media type is Video, then show video in survey detail screen by hiding survey button.
                if self.event!.mediaType == 3 {
                    //If survey attached then move to Survey Detail screen
                    self.performSegue(withIdentifier: "MoveToSurveyDetail", sender: false)
                }
                else {
                    // move back to home.
                    self.navigationController?.popToRootViewController(animated: true)
                }
               
            }
        }
        else if (status == 0) //error response
        {
            let responseErrorMessage = swiftyJsonVar["response"].stringValue
            self.showCommonAlert(responseErrorMessage)
        }
        else
        {
            self.showCommonAlert(message)
        }
    }
    
    func responseGetSavedCards(_ notify: Foundation.Notification) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFICATION_GETSAVEDCARDS), object: nil)
        
        ApplicationDelegate.hideLoader ()
        let swiftyJsonVar = JSON(notify.object!)
        let message = swiftyJsonVar["message"].stringValue
        let status = swiftyJsonVar["status"].intValue
        
        if swiftyJsonVar [ERROR_KEY].exists() {
            self.showCommonAlert(swiftyJsonVar[ERROR_KEY].stringValue)
        }
        else if (status == 1) {
            printCustom("response:\(swiftyJsonVar["response"])")
            
            let cards: [Card] = Parser.getParsedSavedCardsArrayFromData(swiftyJsonVar["response"])
            self.moveToSavedCards(cards)
        }
        else if (status == 0) { // Error response
            /*
             {
             message = Error;
             response = "No saved cards found";
             status = 0;
             }
             */
            let responseErrorMessage = swiftyJsonVar["response"].stringValue
            if responseErrorMessage == "No saved cards found" {
                self.moveToPayment()
            }
            else {
                self.showCommonAlert(responseErrorMessage)
            }
        }
        else {
            self.showCommonAlert(message)
        }
    }
    
    // MARK: - Navigate to Screen
    func moveToPayment() {
        // Move to payment screen to accept paid event invitation.
        self.performSegue(withIdentifier: "Payment", sender: nil)
    }
    
    func moveToSavedCards(_ cards:[Card]?) {
        self.performSegue(withIdentifier: "SavedCardSegue", sender: cards)
    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "MoveToSurveyDetail" {
            let destinationViewController:SurveyDetailVc = segue.destination as! SurveyDetailVc
            destinationViewController.event = self.event
            destinationViewController.isShowSurvey = sender as! Bool
        }
        if segue.identifier == "createSubmerchantSegue" {
            let destinationViewController:CreateSubmerchantViewController = segue.destination as! CreateSubmerchantViewController
            destinationViewController.event = self.event

        }
        if segue.identifier == "Payment" {
            let destinationViewController:EventPaymentViewController = segue.destination as! EventPaymentViewController
            destinationViewController.event = self.event

        }
        if segue.identifier == "SavedCardSegue" {
            let destinationViewController:SavedCardsViewController = segue.destination as! SavedCardsViewController
            let arrSavedCards:[Card] = sender as! [Card]
            destinationViewController.arrCards = arrSavedCards
            destinationViewController.event = self.event
        }
        if segue.identifier == "SurveyLinkSegue" {
            let destinationViewController:UINavigationController = segue.destination as! UINavigationController
            let surveyLinkViewController:SurveyLinkViewController = destinationViewController.childViewControllers.first as! SurveyLinkViewController
            surveyLinkViewController.surveyLinkUrl = self.event!.surveyLink
        }
    }
    

}
