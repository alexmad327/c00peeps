//
//  CreateEventFirstViewController.swift
//  C00Peeps
//
//  Created by SOTSYS027 on 4/1/16.
//  Copyright © 2016 SOTSYS011. All rights reserved.
//

import UIKit
import IQDropDownTextField
import SwiftyJSON

class CreateEventFirstViewController: BaseVC {
    // MARK: - Outlets
    @IBOutlet weak var lblEventDescription: UILabel!
    @IBOutlet weak var txtEventCategory: IQDropDownTextField!
    @IBOutlet weak var txtEventTitle: UITextField!
    @IBOutlet weak var txtVwEventDescription: UITextView!
    @IBOutlet weak var txtEventLocation: UITextField!
    @IBOutlet weak var txtStartDate: IQDropDownTextField!
    @IBOutlet weak var txtEndDate: IQDropDownTextField!
    @IBOutlet weak var txtStartTime: IQDropDownTextField!
    @IBOutlet weak var txtEndTime: IQDropDownTextField!

    var event:Event = Event()
    var passedEvent:Event = Event()
    var arrCategories = [JSON]()
    
    var isMandatoryAlertShown = false

    // MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Create Event"
        self.designTextField()
    
        self.fetchEventCategories()
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormatLocal
        
        //Set Date Picker mode for start date and end date.
        txtStartDate.placeholder = "Start Date"
        txtStartDate.isOptionalDropDown = false
        txtStartDate.dropDownMode = IQDropDownMode.datePicker
        txtStartDate.datePicker.minimumDate = Date()
        txtStartDate.dateFormatter = dateFormatter
        txtStartDate.setSelectedItem(txtStartDate.selectedItem, animated: true)//Set date with changed date format.
        txtStartDate.keyboardToolbar.doneBarButton.tintColor = UIColor.black
        txtEndDate.placeholder = "End Date"
        txtEndDate.isOptionalDropDown = false
        txtEndDate.dropDownMode = IQDropDownMode.datePicker
        txtEndDate.datePicker.minimumDate = Date()
        txtEndDate.dateFormatter = dateFormatter
        txtEndDate.setSelectedItem(txtEndDate.selectedItem, animated: true)//Set date with changed date format.
        
        txtStartTime.placeholder = "Start Time"
        txtStartTime.isOptionalDropDown = false
        txtStartTime.dropDownMode = IQDropDownMode.timePicker
        
        txtEndTime.placeholder = "End Time"
        txtEndTime.isOptionalDropDown = false
        txtEndTime.dropDownMode = IQDropDownMode.timePicker
        
        // Change Placeholder Color
        self.changeTextfieldPlaceholderColor(txtEventCategory, placeholderText: txtEventCategory.placeholder!, color:UIColor.darkGray)
        self.changeTextfieldPlaceholderColor(txtEventTitle, placeholderText: txtEventTitle.placeholder!, color:UIColor.darkGray)
        self.changeTextfieldPlaceholderColor(txtEventLocation, placeholderText: txtEventLocation.placeholder!, color:UIColor.darkGray)
        self.changeTextfieldPlaceholderColor(txtStartDate, placeholderText: txtStartDate.placeholder!, color:UIColor.darkGray)
        self.changeTextfieldPlaceholderColor(txtEndDate, placeholderText: txtEndDate.placeholder!, color:UIColor.darkGray)
        
        // Save values(to be used for editing) from passed event.
        event.id = passedEvent.id
        event.userId = passedEvent.userId
        event.title = passedEvent.title
        event.desc = passedEvent.desc
        event.category = passedEvent.category
        event.categoryId = passedEvent.categoryId
        event.location = passedEvent.location
        event.fromDate = passedEvent.fromDate
        event.toDate = passedEvent.toDate
        event.fromTime = passedEvent.fromTime
        event.toTime = passedEvent.toTime
        event.ticketCost = passedEvent.ticketCost
        event.totalTickets = passedEvent.totalTickets
        event.expirationDate = passedEvent.expirationDate
        event.eventType = passedEvent.eventType
        event.mediaImage = passedEvent.mediaImage
        event.mediaUrl = passedEvent.mediaUrl
        event.mediaAttachmentUrl = passedEvent.mediaAttachmentUrl
        event.mediaType = passedEvent.mediaType
        event.isMediaLocked = passedEvent.isMediaLocked
      
        NotificationCenter.default.addObserver(self, selector: #selector(textDidChange(_:)), name: NSNotification.Name.UITextViewTextDidChange, object: txtVwEventDescription)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.tabBarController?.tabBar.isHidden = false
        
        // Prefill values for edit event feature.
        txtEventTitle.text = event.title
        txtVwEventDescription.text = event.desc
        if txtVwEventDescription.text != "" {
            lblEventDescription.text = ""
        }
        txtEventLocation.text = event.location
        txtStartDate.selectedItem = event.fromDate
        txtEndDate.selectedItem = event.toDate
        txtStartTime.selectedItem = event.fromTime
        txtEndTime.selectedItem = event.toTime
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
        
    // MARK: - Text field Border line
    func designTextField() {
        for scrollView in self.view.subviews {
            if scrollView .isKind(of: UIScrollView.self) {
                for textfield in scrollView.subviews {
                    // Set border line for all textfields in screen.
                    if textfield .isKind(of: UITextField.self) {
                        setSubViewBorder(textfield, color: UIColor.lightGray)
                    }
                }
            }
        }
    }
    
    // MARK: - Textfield Delegates
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.markFieldsMandatory()
    }
    
    // MARK: - Textview Notification Method
    func textDidChange(_ textView:UITextView) {
        let text:NSString = txtVwEventDescription.text! as NSString
        if text.length == 0 {
            lblEventDescription.isHidden = false
        }
        else {
            if text.length > maxLimitEventDescription {
                txtVwEventDescription.text = String(txtVwEventDescription.text.characters.dropLast())
                self.showCommonAlert(alertMsg_EventDescMaxLength)
            }
            lblEventDescription.isHidden = true
        }
    }
    
    // MARK: - Button Actions
    @IBAction func btnNextClicked(_ sender: AnyObject) {
        printCustom("txtStartDate.selectedItem:\(txtStartDate.selectedItem)")
        if txtStartDate.date!.equalToDate(txtEndDate.date!) {
            printCustom("equal dates")
        }
        if txtEventCategory.selectedItem == nil || txtEventTitle.text == "" || txtVwEventDescription.text == "" || txtEventLocation.text == "" || txtStartDate.selectedItem == nil || txtEndDate.selectedItem == nil || txtStartTime.selectedItem == nil || txtEndTime.selectedItem == nil  {
            isMandatoryAlertShown = true
            self.markFieldsMandatory()
            self.showCommonAlert(alertMsg_EnterRequiredFields)
        }
        else if txtStartDate.date!.equalToDate(txtEndDate.date!) && txtStartTime.selectedItem == txtEndTime.selectedItem  {
            self.showCommonAlert(alertMsg_StartDateTimeSameAsEndDateTime)
        }
        else if txtStartDate.date!.equalToDate(txtEndDate.date!) && txtStartTime.date!.isGreaterThanDate(txtEndTime.date!)  {
            self.showCommonAlert(alertMsg_GreaterStartDate)
        }
        else if txtStartDate.date!.isGreaterThanDate(txtEndDate.date!)  {
            self.showCommonAlert(alertMsg_GreaterStartDate)
        }//same dates, same as current date, but old time.
//        else if txtStartTime.selectedItem == txtEndTime.selectedItem  && txtStartDate.date!.equalToDate(txtEndDate.date!) {
//            self.showCommonAlert(alertMsg_SameFromToTime)
//        }
//        else if txtStartTime.date!.isGreaterThanDate(txtEndTime.date!) && txtStartDate.date!.equalToDate(txtEndDate.date!) {
//            self.showCommonAlert(alertMsg_GreaterFromTime)
//        }
        else {
            self.performSegue(withIdentifier: "createEventContinuedId", sender: nil)
        }
    }
    
    // MARK: - Mark Fields Mandatory
    func markFieldsMandatory() {
        if isMandatoryAlertShown {
            // Change Fields Placeholder Color - If text is empty, then show mark red color, otherwise dark gray to indicatory mandatory field.
            var color = UIColor()
            if txtEventCategory.selectedItem == nil {
                color = UIColor.red
            }
            else {
                color = UIColor.darkGray
            }
            self.changeTextfieldPlaceholderColor(txtEventCategory, placeholderText: txtEventCategory.placeholder!, color: color)
            
            if txtEventTitle.text == "" {
                color = UIColor.red
            }
            else {
                color = UIColor.darkGray
            }
            self.changeTextfieldPlaceholderColor(txtEventTitle, placeholderText: txtEventTitle.placeholder!, color: color)
            
            if txtVwEventDescription.text == "" {
                color = UIColor.red
            }
            else {
                color = UIColor.darkGray
            }
            self.lblEventDescription.textColor = color
            
            if txtEventLocation.text == "" {
                color = UIColor.red
            }
            else {
                color = UIColor.darkGray
            }
            self.changeTextfieldPlaceholderColor(txtEventLocation, placeholderText: txtEventLocation.placeholder!, color: color)
            
            if txtStartDate.selectedItem == nil {
                color = UIColor.red
            }
            else {
                color = UIColor.darkGray
            }
            self.changeTextfieldPlaceholderColor(txtStartDate, placeholderText: txtStartDate.placeholder!, color: color)
            
            if txtEndDate.selectedItem == nil {
                color = UIColor.red
            }
            else {
                color = UIColor.darkGray
            }
            self.changeTextfieldPlaceholderColor(txtEndDate, placeholderText: txtEndDate.placeholder!, color: color)
            
            if txtStartTime.selectedItem == nil {
                color = UIColor.red
            }
            else {
                color = UIColor.darkGray
            }
            self.changeTextfieldPlaceholderColor(txtStartTime, placeholderText: txtStartTime.placeholder!, color: color)
            
            if txtEndTime.selectedItem == nil {
                color = UIColor.red
            }
            else {
                color = UIColor.darkGray
            }
            self.changeTextfieldPlaceholderColor(txtEndTime, placeholderText: txtEndTime.placeholder!, color: color)
        }
    }
    
    //MARK: - Call API
    func fetchEventCategories() {
        ApplicationDelegate.showLoader(loaderTitle_UpdatingFields)
        APIManager.sharedInstance.fetchEventCategories(nil, Target: self)
    }

    //MARK: - API Response
    func responseFetchEventCategories (_ notify: Foundation.Notification) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFICATION_FETCHEVENTCATEGORIES), object: nil)
        
        ApplicationDelegate.hideLoader ()
        let swiftyJsonVar = JSON(notify.object!)
        let message = swiftyJsonVar["message"].stringValue
        let status = swiftyJsonVar["status"].intValue
        
        if swiftyJsonVar [ERROR_KEY].exists() {
            self.showCommonAlert(swiftyJsonVar[ERROR_KEY].stringValue)
        }
        else if (status == 1) {
            let response = swiftyJsonVar["response"]
            printCustom("response:\(response)")

            arrCategories = response.arrayValue
            txtEventCategory.itemList = arrCategories.map({$0["category_name"].stringValue})
            
            // Prefill value for edit event feature.
            txtEventCategory.setSelectedItem(event.category, animated: true)
        }
        else if (status == 0) { // Error response
            let responseErrorMessage = swiftyJsonVar["response"].stringValue
            self.showCommonAlert(responseErrorMessage)
        }
        else {
            self.showCommonAlert(message)
        }
    }
    
    func categoryId() -> String {
        let index = arrCategories.map({$0["category_name"].stringValue}).index(of: txtEventCategory.selectedItem!)
        printCustom("index:\(index)")
        
        let id:String = arrCategories.map({$0["id"].stringValue})[index!]
        printCustom("categoryId:\(id)")
        return id
    }
    
    // MARK: - Prepare for Segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        let destinationViewController:CreateEventSecondViewController = segue.destination as! CreateEventSecondViewController
        destinationViewController.eventTitle = self.txtEventTitle.text!
        
        event.title = txtEventTitle.text!
        event.desc = txtVwEventDescription.text
        event.categoryId = self.categoryId()
        event.category = txtEventCategory.selectedItem!
        event.location = txtEventLocation.text!
        event.fromDate = txtStartDate.selectedItem!
        event.toDate = txtEndDate.selectedItem!
        event.fromTime = txtStartTime.selectedItem!
        event.toTime = txtEndTime.selectedItem!
        destinationViewController.event = event
    }
    
}

extension Date {
    func isGreaterThanDate(_ dateToCompare: Date) -> Bool {
        //Declare Variables
        var isGreater = false
        
        //Compare Values
        if self.compare(dateToCompare) == ComparisonResult.orderedDescending {
            isGreater = true
        }
        
        //Return Result
        return isGreater
    }
    
    func isLessThanDate(_ dateToCompare: Date) -> Bool {
        //Declare Variables
        var isLess = false
        
        //Compare Values
        if self.compare(dateToCompare) == ComparisonResult.orderedAscending {
            isLess = true
        }
        
        //Return Result
        return isLess
    }
    
    func equalToDate(_ dateToCompare: Date) -> Bool {
        //Declare Variables
        var isEqualTo = false
        
        //Compare Values
        if self.compare(dateToCompare) == ComparisonResult.orderedSame {
            isEqualTo = true
        }
        
        //Return Result
        return isEqualTo
    }
    
    func addDays(_ daysToAdd: Int) -> Date {
        let secondsInDays: TimeInterval = Double(daysToAdd) * 60 * 60 * 24
        let dateWithDaysAdded: Date = self.addingTimeInterval(secondsInDays)
        
        //Return Result
        return dateWithDaysAdded
    }
    
    func addHours(_ hoursToAdd: Int) -> Date {
        let secondsInHours: TimeInterval = Double(hoursToAdd) * 60 * 60
        let dateWithHoursAdded: Date = self.addingTimeInterval(secondsInHours)
        
        //Return Result
        return dateWithHoursAdded
    }
}
