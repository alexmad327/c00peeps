//
//  CreateEventSecondViewController.swift
//  C00Peeps
//
//  Created by SOTSYS027 on 4/1/16.
//  Copyright © 2016 SOTSYS011. All rights reserved.
//

import UIKit
import IQDropDownTextField
@objc public protocol CreateEventSecondControllerDelegate {
    
    @objc func filterSelected(_ applyFilter:String?)
    @objc func mediaSelected(_ mediaImage:UIImage?, mediaUrl:URL?)
}

class CreateEventSecondViewController: BaseVC, UIImagePickerControllerDelegate, UINavigationControllerDelegate, CreateEventSecondControllerDelegate {
    
    var eventTitle:String = ""
    var isMediaAttached = false
    var event:Event?
    var isMandatoryAlertShown = false
    var videoPreparingDelay = 0
    var isExpiryDateSelected = false
    var isSurveyAttached = 0
    var filterApply = false
    
    @IBOutlet weak var btnAttachDigitalMedia: UIButton!
    @IBOutlet weak var btnAttachSurveys: UIButton!
    @IBOutlet weak var btnPaid: UIButton!
    @IBOutlet weak var btnUnpaid: UIButton!
    @IBOutlet weak var imgVwAttachDigitalMedia: UIImageView!
    @IBOutlet weak var imgVwAttachSurveys: UIImageView!
    @IBOutlet weak var lblAttachDigitalMedia: UILabel!
    @IBOutlet weak var lblAttachSurveys: UILabel!
    @IBOutlet weak var txtAmount: UITextField!
    @IBOutlet weak var txtQuantityOfTickets: UITextField!
    @IBOutlet weak var txtEventExpDate: IQDropDownTextField!
    @IBOutlet weak var btnPrivate: UIButton!
    @IBOutlet weak var btnPublic: UIButton!
    
    func filterSelected(_ applyFilter:String?)
    {
        if (applyFilter == "TRUE")
        {
            filterApply = true
        }
        else
        {
            filterApply = false
        }
    }
    
    // MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = eventTitle
        self.designTextField()
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormatLocal
        
        txtEventExpDate.isOptionalDropDown = true
        txtEventExpDate.dropDownMode = IQDropDownMode.datePicker
        txtEventExpDate.dateFormatter = dateFormatter
        txtEventExpDate.datePicker.minimumDate = Date()

        btnPaid.isSelected = true
        btnPrivate.isSelected = true
        
        isEditingEventMedia = false
        
        // Change Placeholder Color
        self.changeTextfieldPlaceholderColor(txtAmount, placeholderText: txtAmount.placeholder!, color:UIColor.darkGray)
        self.changeTextfieldPlaceholderColor(txtQuantityOfTickets, placeholderText: txtQuantityOfTickets.placeholder!, color:UIColor.darkGray)
        self.changeTextfieldPlaceholderColor(txtEventExpDate, placeholderText: txtEventExpDate.placeholder!, color:UIColor.darkGray)
        
        self.updateViewAccordingToTheme()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
        
        // Prefill values for edit event feature.
        if event?.mediaImage != nil || event?.mediaUrl != nil {
            self.isMediaAttached = true
            self.showDigitalMediaSelection()
        }
        else {
            self.isMediaAttached = false
            self.hideDigitalMediaSelection()
        }
        
        if isSurveyAttached == 1
        {
            self.showAttachSurveySelection()
        }
        else
        {
            self.hideAttachSurveySelection()
        }
        
        if isEditingEvent
        {
            if  event?.ticketCost == nil || event?.ticketCost == "0" {
                self.btnPaid.isSelected = false
                self.btnUnpaid.isSelected = true
                txtAmount.isUserInteractionEnabled = false
            }
            else
            {
                self.txtAmount.text = event?.ticketCost
                self.btnPaid.isSelected = true
                self.btnUnpaid.isSelected = false
                txtAmount.isUserInteractionEnabled = true
            }
        }
        else
        {
            
        }
        
        //public event type
        if event?.eventType == 2 {
            
            self.btnPrivate.isSelected = false
            self.btnPublic.isSelected = true
        }
        else {
            
            self.btnPrivate.isSelected = true
            self.btnPublic.isSelected = false
            
            
        }
        self.txtQuantityOfTickets.text = event?.totalTickets
        self.txtEventExpDate.selectedItem = event?.expirationDate
        
        if event?.expirationDate != "" {
            isExpiryDateSelected = true
        }
        
        if isEditingEvent {
            // When editing event, payment related values can't be changed.
            self.btnPaid.isUserInteractionEnabled = false
            self.btnUnpaid.isUserInteractionEnabled = false
            self.txtAmount.isEnabled = false
            self.txtQuantityOfTickets.isEnabled = false
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Update View According To Selected Theme
    func updateViewAccordingToTheme() {
        btnAttachDigitalMedia.backgroundColor = UIColor.clear
        btnAttachDigitalMedia.layer.borderColor = Utilities().themedMultipleColor()[1].cgColor
        btnAttachDigitalMedia.layer.borderWidth = 1.0
        btnAttachDigitalMedia.layer.cornerRadius = 20
        lblAttachDigitalMedia.textColor = Utilities().themedMultipleColor()[2]
        imgVwAttachDigitalMedia.image = Utilities().themedImage(img_attachIconEvents)
        
        btnAttachSurveys.backgroundColor = UIColor.clear
        btnAttachSurveys.layer.borderColor = Utilities().themedMultipleColor()[1].cgColor
        btnAttachSurveys.layer.borderWidth = 1.0
        btnAttachSurveys.layer.cornerRadius = 20
        lblAttachSurveys.textColor = Utilities().themedMultipleColor()[2]
        imgVwAttachSurveys.image = Utilities().themedImage(img_attachIconEvents)
        
        //        btnAttachDigitalMedia.setImage(Utilities().themedImage(img_contactbtn), forState: .Normal)
        //        btnAttachDigitalMedia.setImage(Utilities().themedImage(img_contactbtnSelected), forState: .Selected)
        //
        //        btnAttachSurveys.setImage(Utilities().themedImage(img_contactbtn), forState: .Normal)
        //        btnAttachSurveys.setImage(Utilities().themedImage(img_contactbtnSelected), forState: .Selected)
        
        btnPaid.setImage(Utilities().themedImage(img_radio), for: UIControlState())
        btnPaid.setImage(Utilities().themedImage(img_radioSelected), for: .selected)
        
        btnUnpaid.setImage(Utilities().themedImage(img_radio), for: UIControlState())
        btnUnpaid.setImage(Utilities().themedImage(img_radioSelected), for: .selected)
        
        btnPrivate.setImage(Utilities().themedImage(img_radio), for: UIControlState())
        btnPrivate.setImage(Utilities().themedImage(img_radioSelected), for: .selected)
        
        btnPublic.setImage(Utilities().themedImage(img_radio), for: UIControlState())
        btnPublic.setImage(Utilities().themedImage(img_radioSelected), for: .selected)
        
    }
    
    // MARK: - Text field Border line
    func designTextField() {
        for scrollView in self.view.subviews {
            if scrollView .isKind(of: UIScrollView.self) {
                for textfield in scrollView.subviews {
                    // Set border line for all textfields in screen.
                    if textfield .isKind(of: UITextField.self) {
                        setSubViewBorder(textfield, color: UIColor.lightGray)
                    }
                }
            }
        }
    }
    
    // MARK: - Textfield Delegates
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.markFieldsMandatory()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == txtAmount {
            //txtAmount.text = Utilities().generalizePrice(txtAmount.text!)
            
            txtAmount.text = txtAmount.text!
        }
    }
    
    // MARK: - IQDropDownTextField Delegate
    func textField(_ textField: IQDropDownTextField, didSelectDate date: Date?) {
        if textField == txtEventExpDate {
            if date != nil {
                isExpiryDateSelected = true
            }
            else {
                isExpiryDateSelected = false
            }
        }
    }
    
    // MARK: - Button Actions
    @IBAction func btnAttachDigitalMediaClicked(_ sender: AnyObject) {
        
        saveEventDetails()
        
        let actionSheet =  UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        actionSheet.addAction(UIAlertAction(title: alertBtnTitle_AttachPhoto, style: UIAlertActionStyle.default, handler:{ action -> Void in
            // Photo
            self.moveToPhoto()
            
        }))
        actionSheet.addAction(UIAlertAction(title: alertBtnTitle_AttachVideo, style: UIAlertActionStyle.default, handler:{ action -> Void in
            // Video
            self.moveToVideo()
        }))
        actionSheet.addAction(UIAlertAction(title: alertBtnTitle_ChooseOptionsFromGallery, style: UIAlertActionStyle.default, handler:{ action -> Void in
            // Photo Library
            self.moveToLibrary()
        }))
        if isEditingEvent && self.isMediaAttached {
            actionSheet.addAction(UIAlertAction(title: alertBtnTitle_RemoveDigitalMedia, style: UIAlertActionStyle.default, handler:{ action -> Void in
                // Delete Digital Media
                self.deleteDigitalMedia()
            }))
        }
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler:nil))
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    @IBAction func btnAttachSurveysClicked(_ sender: AnyObject) {
        
        if isSurveyAttached == 0
        {
            isSurveyAttached = 1
            event?.isSurveyAttached = 1
            
            self.showAttachSurveySelection()
        }
        else
        {
            isSurveyAttached = 0
            event?.isSurveyAttached = 0
            
            self.hideAttachSurveySelection()
        }
    }
    
    @IBAction func btnPaidClicked(_ sender: AnyObject) {
        if btnPaid.isSelected {
            btnPaid.isSelected = false
            txtAmount.isUserInteractionEnabled = false
            txtAmount.text = ""
        }
        else {
            btnPaid.isSelected = true
            btnUnpaid.isSelected = false
            txtAmount.isUserInteractionEnabled = true
        }
        
        self.markFieldsMandatory()
    }
    
    @IBAction func btnUnpaidClicked(_ sender: AnyObject) {
        if btnUnpaid.isSelected {
            btnUnpaid.isSelected = false
        }
        else {
            btnUnpaid.isSelected = true
            btnPaid.isSelected = false
            txtAmount.isUserInteractionEnabled = false
            txtAmount.text = ""
        }
        self.markFieldsMandatory()
    }
    
    @IBAction func btnPrivateClicked(_ sender: AnyObject) {
        btnPrivate.isSelected = true
        btnPublic.isSelected = false
    }
    
    @IBAction func btnPublicClicked(_ sender: AnyObject) {
        btnPrivate.isSelected = false
        btnPublic.isSelected = true
    }
    
    @IBAction func btnDoneClicked(_ sender: AnyObject) {
        let endDate = Utilities().convertStringToDate(event!.toDate)
        self.view.endEditing(true)
        var showAlert = true
        if btnPaid.isSelected && (txtAmount.text == "" || txtAmount.text == "0.00") {
            self.showCommonAlert(alertMsg_EmptyAmount)
        }
        else if btnPaid.isSelected && txtQuantityOfTickets.text == "" {
            self.showCommonAlert(alertMsg_EmptyNoOfTickets)
        }
        else if !isExpiryDateSelected {
            self.showCommonAlert(alertMsg_EmptyExpDate)
        }
        else if txtEventExpDate.date!.isLessThanDate(endDate) {
            self.showCommonAlert(alertMsg_SmallerExpDate)
        }
        else {
            showAlert = false
            self.performSegue(withIdentifier: "eventDetailsId", sender: nil)
        }
        
        if showAlert {
            isMandatoryAlertShown = true
            self.markFieldsMandatory()
        }
    }

    // MARK: - Mark Fields Mandatory
    func markFieldsMandatory() {
        if isMandatoryAlertShown {
            // Change Fields Placeholder Color - If text is empty, then show mark red color, otherwise dark gray to indicatory mandatory field.
            var color = UIColor()
            if btnPaid.isSelected && (txtAmount.text == "" || txtAmount.text == "0.00") {
                color = UIColor.red
            }
            else {
                color = UIColor.darkGray
            }
            self.changeTextfieldPlaceholderColor(txtAmount, placeholderText: txtAmount.placeholder!, color: color)
            
            if btnPaid.isSelected && txtQuantityOfTickets.text == "" {
                color = UIColor.red
            }
            else {
                color = UIColor.darkGray
            }
            self.changeTextfieldPlaceholderColor(txtQuantityOfTickets, placeholderText: txtQuantityOfTickets.placeholder!, color: color)
            
            if txtEventExpDate.selectedItem == nil {
                color = UIColor.red
            }
            else {
                color = UIColor.darkGray
            }
            self.changeTextfieldPlaceholderColor(txtEventExpDate, placeholderText: txtEventExpDate.placeholder!, color: color)
        }
    }
    
    //MARK:- Save Event Details
    func saveEventDetails() {
        event?.ticketCost = txtAmount.text! != "" ? txtAmount.text! : "0"
        event?.totalTickets = txtQuantityOfTickets.text!
        if isExpiryDateSelected {
            event?.expirationDate = txtEventExpDate.selectedItem!
        }
        if btnPrivate.isSelected {
            event?.eventType = 1
        }
        else {
            event?.eventType = 2
        }
    }
    
    //MARK:- Show/Hide Selection for Digital Media
    
    func showDigitalMediaSelection() {
        btnAttachDigitalMedia.backgroundColor = Utilities().themedMultipleColor()[1]
        btnAttachDigitalMedia.layer.borderColor = UIColor.clear.cgColor
        btnAttachDigitalMedia.layer.borderWidth = 0.0
        lblAttachDigitalMedia.textColor = UIColor.white
        imgVwAttachDigitalMedia.image = Utilities().themedImage(img_attachIconEventsSelected)
    }
    
    func hideDigitalMediaSelection() {
        btnAttachDigitalMedia.backgroundColor = UIColor.clear
        btnAttachDigitalMedia.layer.borderColor = Utilities().themedMultipleColor()[1].cgColor
        btnAttachDigitalMedia.layer.borderWidth = 1.0
        lblAttachDigitalMedia.textColor = Utilities().themedMultipleColor()[2]
        imgVwAttachDigitalMedia.image = Utilities().themedImage(img_attachIconEvents)
        
    }
    
    //MARK:- Show/Hide Selection for Attach Survey
    
    func showAttachSurveySelection() {
        btnAttachSurveys.backgroundColor = Utilities().themedMultipleColor()[1]
        btnAttachSurveys.layer.borderColor = UIColor.clear.cgColor
        btnAttachSurveys.layer.borderWidth = 0.0
        lblAttachSurveys.textColor = UIColor.white
        imgVwAttachSurveys.image = Utilities().themedImage(img_attachIconEventsSelected)
    }
    
    func hideAttachSurveySelection() {
        btnAttachSurveys.backgroundColor = UIColor.clear
        btnAttachSurveys.layer.borderColor = Utilities().themedMultipleColor()[1].cgColor
        btnAttachSurveys.layer.borderWidth = 1.0
        lblAttachSurveys.textColor = Utilities().themedMultipleColor()[2]
        imgVwAttachSurveys.image = Utilities().themedImage(img_attachIconEvents)
        
    }
    
    //MARK:- Move To Photo
    func moveToPhoto() {
        let st = UIStoryboard(name: "Digital", bundle:nil)
        let obj = st.instantiateViewController(withIdentifier: "CapturePhoto") as! CapturePhoto
        obj.isComingFromEventsFlow = true
        obj.isComingFromPeepTextFlow = false
        obj.delegate = self
        // mediaType = MediaType.Photo
        mediaAttachment = MediaAttachment.photo
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    //MARK:- Move to Video
    func moveToVideo() {
        let st = UIStoryboard(name: "Digital", bundle:nil)
        let obj = st.instantiateViewController(withIdentifier: "CaptureVideo") as! CaptureVideo
        obj.isComingFromEventsFlow = true
        obj.isComingFromPeepTextFlow = false
        obj.delegate = self
        //mediaType = MediaType.Video
        mediaAttachment = MediaAttachment.video
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    //MARK:- Move to Library
    func moveToLibrary() {
        // Show Fusuma
        let fusuma = FusumaViewController()
        //        fusumaCropImage = false
        //fusuma.delegate = self
        fusuma.eventDelegate = self
        self.navigationController?.pushViewController(fusuma, animated: true)
    }
    
    //MARK:- Delete Digital Media
    func deleteDigitalMedia() {
        self.event?.mediaUrl = nil
        self.event?.mediaAttachmentUrl = nil
        self.hideDigitalMediaSelection()
    }
    
    // MARK: - Media selected from Digital Media
    func mediaSelected(_ mediaImage:UIImage?, mediaUrl:URL?) {
        printCustom("mediaSelected")
        
        if mediaImage != nil {
            self.isMediaAttached = true
            
            if isEditingEvent {
                isEditingEventMedia = true
            }
            
            if mediaAttachment == MediaAttachment.video {
                self.saveThumbnailVideo()
            }
            else {
                event?.mediaImage = mediaImage
                event?.mediaUrl = mediaUrl
                
                self.showDigitalMediaSelection()
                
            }
        }
        else {
            self.isMediaAttached = false
            
            self.hideDigitalMediaSelection()
            
            event?.mediaImage = nil
            event?.mediaUrl = nil
        }
    }
    
    func saveThumbnailVideo() {
        // Filtered Media Url
        let fileManager = FileManager.default
        let urls = fileManager.urls(for: .documentDirectory, in: .userDomainMask)
        let documentDirectory = urls[0] as URL
        let videoURL = documentDirectory.appendingPathComponent("Movie.mp4")
        printCustom("videoURL:\(videoURL )")
        if FileManager.default.fileExists(atPath: videoURL.path) {
            event?.mediaImage = self.thumbnailVideo(videoURL)
            event?.mediaUrl = videoURL
            
            self.showDigitalMediaSelection()
        }
        else if (self.videoPreparingDelay < 32) {
            //wait for video saving
            
            DispatchQueue.main.async(execute: {
                self.videoPreparingDelay += 1
                self.perform(#selector(self.saveThumbnailVideo), with:nil, afterDelay: 1.0)
            })
        }
        else {
            event?.mediaImage = Utilities().themedImage(img_movieImage)
            event?.mediaUrl = videoURL
        }
    }
    
    func thumbnailVideo(_ VideoURL:URL)->UIImage{
        
        var uiImage = UIImage()
        //var imageData1 = NSData()
        
        do {
            let asset = AVURLAsset(url: VideoURL, options: nil)
            let imgGenerator = AVAssetImageGenerator(asset: asset)
            imgGenerator.appliesPreferredTrackTransform = true
            let cgImage = try imgGenerator.copyCGImage(at: CMTimeMake(0, 1), actualTime: nil)
            uiImage = UIImage(cgImage: cgImage)
            
            printCustom("width: \(uiImage.size.width) height \(uiImage.size.height)")
            
            //imageData1 = UIImageJPEGRepresentation(uiImage, 0.5)!
            
            // lay out this image view, or if it already exists, set its image property to uiImage
        } catch let error as NSError {
            printCustom("Error generating thumbnail: \(error)")
        }
        
        return uiImage
    }
    
    // MARK: - Prepare for Segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "eventDetailsId" {
            self.saveEventDetails()
            
            if isEditingEvent && !isEditingEventMedia {
                if event?.mediaUrl != nil {
                    switch event!.mediaType {
                    case 1:
                        mediaAttachment = MediaAttachment.photo
                        
                    case 2:
                        mediaAttachment = MediaAttachment.audio
                        
                    case 3:
                        mediaAttachment = MediaAttachment.video
                    default:
                        mediaAttachment = MediaAttachment.none
                    }
                }
            }

            let destinationViewController:EventDetailsVc = segue.destination as! EventDetailsVc
            destinationViewController.event = event
            
            if filterApply == true
            {
                destinationViewController.filterApplied = true
            }
            else
            {
                destinationViewController.filterApplied = false
            }
            
            destinationViewController.sourceScreen = SourceScreenEventDetails.createEvent
        }
    }
    
}
