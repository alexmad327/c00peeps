//
//  EventVc.swift
//  C00Peeps
//
//  Created by SOTSYS011 on 3/19/16.
//  Copyright © 2016 SOTSYS011. All rights reserved.
//

import UIKit
import SWTableViewCell
import SwiftyJSON
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class EventVc: BaseVC, SWTableViewCellDelegate
{
    @IBOutlet weak var tblVwEvents: UITableView!
    @IBOutlet weak var btnMyEvents: UIButton!
    @IBOutlet weak var btnAllEvents: UIButton!
    @IBOutlet weak var searchBarEvents: UISearchBar!
    @IBOutlet weak var lblNoRecordsFound: UILabel!
    @IBOutlet weak var btnRefresh: UIButton!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    @IBOutlet var barBtnItemCreate: UIBarButtonItem!
    
    var pageMenu : CAPSPageMenu?
    var myEvents:[Event] = []
    var allEvents:[Event] = []
    var isSearchOn:Bool = false
    var selectedEventIndex:Int = 0
    var offsetMyEvents = 0
    var pageNumberMyEvents = 1
    var totalRecordsMyEvents = 0
    var offsetAllEvents = 0
    var pageNumberAllEvents = 1
    var totalRecordsAllEvents = 0
    var refreshControl: UIRefreshControl!
    var filterArrayEvents = [Event]()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.navigationItem.title = "Events"
        
        addRefreshControl()
        
        // Update View According To Selected Theme
        self.updateViewAccordingToTheme()
        
        btnMyEvents.isSelected = true

        // Show Loader.
        loadingIndicator.startAnimating()
        self.tblVwEvents.isHidden = true
        self.fetchEvents()
    }
 
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationItem.rightBarButtonItem = self.barBtnItemCreate
        
        printCustom("selected index tab:\(String(describing: self.tabBarController?.selectedIndex))")
        
        
        /*
        // For basic user, create event function is not allowed.
        if Utilities.getUserDetails().package_id == "1" {
            self.navigationItem.rightBarButtonItem = nil
        }
        else {
                self.navigationItem.rightBarButtonItem = self.barBtnItemCreate
        }*/

        // Results should not refresh when coming back to the screen, only when tabs are switched.
        if self.tabBarController?.selectedIndex != 4 {
            pageNumberAllEvents = 1
            offsetAllEvents = 0
            pageNumberMyEvents  = 1
            offsetMyEvents = 0
            
            searchBarEvents.text = ""
            searchBarEvents.resignFirstResponder()
            searchBarEvents.showsCancelButton = false
            filterArrayEvents.removeAll()
            isSearchOn = false
            
            btnMyEvents.isSelected = true
            btnAllEvents.isSelected = false
            btnMyEvents.isUserInteractionEnabled = false
            btnAllEvents.isUserInteractionEnabled = true
            
            // Show Loader.
            loadingIndicator.startAnimating()
            self.tblVwEvents.isHidden = true
            self.fetchEvents()
        }
    
        
        // Show tab bar for this controller.
        self.tabBarController?.tabBar.isHidden = false
    }

    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    func addRefreshControl(){
        
        refreshControl = UIRefreshControl()
        refreshControl.tintColor = UIColor.darkGray
        refreshControl.attributedTitle = NSAttributedString(string: loaderTitle_PullToRefresh)
        refreshControl.addTarget(self, action: #selector(EventVc.handleRefresh(_:)), for: UIControlEvents.valueChanged)
        self.tblVwEvents.addSubview(refreshControl)
        
    }
    
    //MARK:- Pull to refresh for people table view
    func handleRefresh(_ refreshControl: UIRefreshControl) {
        
        
        if btnMyEvents.isSelected {
            offsetMyEvents = 0
            pageNumberMyEvents = 1
        }
        else
        {
            offsetAllEvents = 0
            pageNumberAllEvents = 1
        }
        
        fetchEvents()

    }
    
    
    // MARK: - Update View According To Selected Theme
    func updateViewAccordingToTheme() {
        btnMyEvents.setImage(Utilities().themedImage(img_myEvents), for: UIControlState())
        btnMyEvents.setImage(Utilities().themedImage(img_myEventsActive), for: .selected)
        
        btnAllEvents.setImage(Utilities().themedImage(img_allEvents), for: UIControlState())
        btnAllEvents.setImage(Utilities().themedImage(img_allEventsActive), for: .selected)
    }

    
    //MARK: - Button Actions
    @IBAction func btnMyEventsClicked(_ sender: AnyObject) {
        
        tblVwEvents.scrollsToTop = true
        
        pageNumberMyEvents = 1
        offsetMyEvents = 0
        
        searchBarEvents.text = ""
        searchBarEvents.resignFirstResponder()
        searchBarEvents.showsCancelButton = false
        filterArrayEvents.removeAll()
        isSearchOn = false
        
        btnMyEvents.isSelected = true
        btnAllEvents.isSelected = false
        btnMyEvents.isUserInteractionEnabled = false
        btnAllEvents.isUserInteractionEnabled = true

        tblVwEvents.reloadData()
        if self.myEvents.count == 0 {
            tblVwEvents.isHidden = true
            loadingIndicator.startAnimating()
            self.fetchEvents()
        }
        else {
            self.hideErrorMessage()
        }
    }
    
    @IBAction func btnAllEventsClicked(_ sender: AnyObject) {
        
        tblVwEvents.scrollsToTop = true
        
        pageNumberAllEvents = 1
        offsetAllEvents = 0
        
        searchBarEvents.text = ""
        searchBarEvents.resignFirstResponder()
        searchBarEvents.showsCancelButton = false
        filterArrayEvents.removeAll()
        isSearchOn = false
        
        btnMyEvents.isSelected = false
        btnAllEvents.isSelected = true
        btnMyEvents.isUserInteractionEnabled = true
        btnAllEvents.isUserInteractionEnabled = false
        
        tblVwEvents.reloadData()
        
        if self.allEvents.count == 0 {
            tblVwEvents.isHidden = true
            loadingIndicator.startAnimating()
            self.fetchEvents()
        }
        else {
            self.hideErrorMessage()
        }
    }
   
    @IBAction func btnRefreshClicked(_ sender: AnyObject) {
        self.tblVwEvents.isHidden = true
        self.lblNoRecordsFound.isHidden = true
        self.btnRefresh.isHidden = true
        self.loadingIndicator.startAnimating()
        self.fetchEvents()
    }
    
    //MARK: - Right Table View Buttons
    func rightTableViewButtonsEditDelete() -> NSArray {
        let rightUtilityButtons:NSMutableArray = NSMutableArray()
        rightUtilityButtons.sw_addUtilityButton(with: Utilities().themedMultipleColor()[0], icon: Utilities().themedImage(img_editEvents))
        rightUtilityButtons.sw_addUtilityButton(with: Utilities().themedMultipleColor()[1], icon: Utilities().themedImage(img_delete))
        return rightUtilityButtons
    }
    
    func rightTableViewButtonDelete() -> NSArray {
        let rightUtilityButtons:NSMutableArray = NSMutableArray()
        rightUtilityButtons.sw_addUtilityButton(with: Utilities().themedMultipleColor()[1], icon: Utilities().themedImage(img_delete))
        return rightUtilityButtons
    }
    
    //MARK: - Table View Datasource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Return the number of rows
        printCustom("btnMyEvents.selected:\(btnMyEvents.isSelected)")
        
        if isSearchOn
        {
            return filterArrayEvents.count
        }
        else
        {
            return btnMyEvents.isSelected ? myEvents.count : allEvents.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        
        var events = [Event]()
        var offsetEvents = 0
        var pageNumberEvents = 1
        var totalRecordsEvents = 0
        
        if isSearchOn
        {
            events = filterArrayEvents
        }
        else
        {
            events = btnMyEvents.isSelected ? myEvents : allEvents
        }
        if btnMyEvents.isSelected {
            offsetEvents = offsetMyEvents
            pageNumberEvents = pageNumberMyEvents
            totalRecordsEvents = totalRecordsMyEvents
        }
        else {
            offsetEvents = offsetAllEvents
            pageNumberEvents = pageNumberAllEvents
            totalRecordsEvents = totalRecordsAllEvents
        }
        printCustom("events.count:\(events.count)")
        printCustom("Limit:\(Limit)")
        printCustom("indexPath.row + 1:\(indexPath.row + 1)")
        if (events.count >= Limit) && (indexPath.row + 1  == events.count) {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "LoadMoreCell")
            
            if let lbl5176 =  cell?.viewWithTag(5176) as? UILabel
            {
                lbl5176.text = loading_Title
                
                if (totalRecordsEvents) > (pageNumberEvents) * Limit {
                    offsetEvents = (pageNumberEvents) * Limit
                    pageNumberEvents += 1
                    if btnMyEvents.isSelected {
                        offsetMyEvents = offsetEvents
                        pageNumberMyEvents = pageNumberEvents
                        totalRecordsMyEvents = totalRecordsEvents
                    }
                    else {
                        offsetAllEvents = offsetEvents
                        pageNumberAllEvents = pageNumberEvents
                        totalRecordsAllEvents = totalRecordsEvents
                    }
                    
                    fetchEvents()
                }
                else
                {
                    lbl5176.text = loadingNoMoreData
                    
                    lbl5176.isHidden = true
                    
                    return refreshCellForRow(indexPath)
                }
            }
            
            return cell!
        }
            
        else{
            
            return refreshCellForRow(indexPath)
        }
        /*if btnMyEvents.isSelected {
            printCustom("myEvents.count:\(myEvents.count)")
            printCustom("Limit:\(Limit)")
            printCustom("indexPath.row + 1:\(indexPath.row + 1)")
          
            if (myEvents.count >= Limit) && (indexPath.row + 1  == myEvents.count) {
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "LoadMoreCell")
                
                if let lbl5176 =  cell?.viewWithTag(5176) as? UILabel
                {
                    lbl5176.text = loading_Title
                    
                    if (totalRecordsMyEvents) > (pageNumberMyEvents) * Limit {
                        offsetMyEvents = (pageNumberMyEvents) * Limit
                        pageNumberMyEvents += 1
                        
                        fetchEvents()
                    }
                    else
                    {
                        lbl5176.text = loadingNoMoreData
                        
                        lbl5176.isHidden = true
                        
                        return refreshCellForRow(indexPath)
                    }
                }
                
                return cell!
            }
                
            else{
                
                return refreshCellForRow(indexPath)
            }
        }
        else
        {
            if (allEvents.count >= Limit) && (indexPath.row + 1  == allEvents.count) {
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "LoadMoreCell")
                
                if let lbl5176 =  cell?.viewWithTag(5176) as? UILabel
                {
                    lbl5176.text = loading_Title
                    
                    if (totalRecordsAllEvents) > (pageNumberAllEvents) * Limit {
                        offsetAllEvents = (pageNumberAllEvents) * Limit
                        pageNumberAllEvents += 1
                        
                        fetchEvents()
                    }
                    else
                    {
                        lbl5176.text = loadingNoMoreData
                        
                        lbl5176.isHidden = true
                        
                        return refreshCellForRow(indexPath)
                    }
                }
                
                return cell!
            }
                
            else{
                
                return refreshCellForRow(indexPath)
            }
        }*/
        //Load More
        
    }
    
    func refreshCellForRow(_ indexPath:IndexPath)->EventTableViewCell
    {
        
        let event: Event!
        if isSearchOn
        {
            event = filterArrayEvents[indexPath.row]
        }
        else
        {
            event = btnMyEvents.isSelected ? myEvents[indexPath.row] : allEvents[indexPath.row]
        }
        
        let cell:EventTableViewCell = tblVwEvents.dequeueReusableCell(withIdentifier: "eventIdentifier") as! EventTableViewCell
        
        cell.lblEventTitle.font = ProximaNovaRegular(15)
        cell.lblEventTitle.text = event.title
        
        cell.lblEventDescription.font = ProximaNovaRegular(15)
        cell.lblEventDescription.text = event.desc
        
        cell.lblEventCategory.font = ProximaNovaRegular(15)
        cell.lblEventCategory.textColor = Utilities().themedMultipleColor()[0]
        
        cell.lblEventCategoryName.font = ProximaNovaRegular(15)
        cell.lblEventCategoryName.text = event.category
        
        cell.lblEventDate.font = ProximaNovaRegular(14)
        cell.lblEventDate.text = "From " + event.fromDate + " to " + event.toDate + " at " + event.fromTime + " to "  + event.toTime //"From Aug 22, 2016 to Aug 23, 2016 at 9:00 am to 12:00 pm"
        if btnMyEvents.isSelected {
            if Int(Utilities.getUserDetails().id) == event.userId {
                cell.rightUtilityButtons = self.rightTableViewButtonsEditDelete() as [AnyObject]
            }
            else {
                cell.rightUtilityButtons = self.rightTableViewButtonDelete() as [AnyObject]
            }
        }
        else {
            cell.rightUtilityButtons = nil
        }
        cell.delegate = self
        return cell
    }
    
    func swipeableTableViewCellShouldHideUtilityButtons(onSwipe cell: SWTableViewCell!) -> Bool {
        return true
    }
    
    // MARK: - Table view delegate
    
    func tableView(_ tableView: UITableView, didSelectRowAtIndexPath indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        selectedEventIndex = indexPath.row
        self.performSegue(withIdentifier: "eventDetailsId", sender: nil)
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: IndexPath) -> CGFloat {
        // To automatically adjust row height according to cell content height.
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAtIndexPath indexPath: IndexPath) -> CGFloat {
        // To automatically adjust row height according to cell content height.
        return UITableViewAutomaticDimension
    }
    
    // MARK: - Swipeable Table View Cell Delegate
    
    func swipeableTableViewCell(_ cell: SWTableViewCell!, didTriggerRightUtilityButtonWith index: Int) {
        let indexPath = self.tblVwEvents.indexPath(for: cell)
        //    NSIndexPath *indexPath = [self.yourTableView indexPathForCell:cell];

        let event: Event!
        if isSearchOn {
            event = filterArrayEvents[indexPath!.row]
        }
        else {
            event = btnMyEvents.isSelected ? myEvents[indexPath!.row] : allEvents[indexPath!.row]
        }
        
        if btnMyEvents.isSelected {
            if Int(Utilities.getUserDetails().id) == event.userId {
                switch index {
                case 0:
                    printCustom("Edit")
                    self.moveToEditEvent(event)
                    
                default:
                    printCustom("Delete")
                    self.deleteEvent(event.id)
                }
            }
            else {
                printCustom("Delete Invite")
                self.deleteEventInvite(event.id)
            }
        }
    }
    
    // MARK: - Search Bar Delegates
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        searchBar.showsCancelButton = false
        
        if searchBar.text?.characters.count > 0
        {
            filterArrayEvents.removeAll()
            
            offsetMyEvents = 0
            pageNumberMyEvents = 1
            
            offsetAllEvents = 0
            pageNumberAllEvents = 1
            
            fetchEvents()
        }
        else
        {
            filterArrayEvents = btnMyEvents.isSelected ? myEvents : allEvents
        }
        // Resign keyboard.
        searchBar.resignFirstResponder()
        
        
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar){
        
        isSearchOn = true

        searchBar.text = ""
        
        filterArrayEvents.removeAll()
        tblVwEvents.reloadData()
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        let text:NSString = searchBar.text! as NSString
        if text.length == 0 {
            isSearchOn = false
        }
        else {
            isSearchOn = true
        }
        searchBar.showsCancelButton = true
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        isSearchOn = false
        
        // Clear search bar text and resign keyboard.
        searchBar.text = ""
        searchBar.resignFirstResponder()
        searchBar.showsCancelButton = false
        filterArrayEvents.removeAll()
         tblVwEvents.reloadData()
    }
    
    //MARK: - Call API
    func fetchEvents() {
        APIManager.sharedInstance.cancelAllRequests()

        var parameters = [String: AnyObject]()
        parameters["user_id"] = Utilities.getUserDetails().id as AnyObject
        parameters["limit"] = Limit as AnyObject
        parameters["offset"] = btnMyEvents.isSelected ? offsetMyEvents as AnyObject: offsetAllEvents as AnyObject
        parameters["type"] = btnMyEvents.isSelected ? EventType.myEvents.rawValue as AnyObject: EventType.allEvents.rawValue as AnyObject
        //if self.searchBarEvents.text?.characters.count > 0 {
        parameters["keyword"] = self.searchBarEvents.text as AnyObject//"Test" as AnyObject//
        //}
        APIManager.sharedInstance.fetchEvents(parameters, Target: self)
    }
    
    func deleteEvent(_ eventId:Int) {
        ApplicationDelegate.showLoader(loaderTitle_Deleting)
        APIManager.sharedInstance.cancelAllRequests()
        
        var parameters = [String: AnyObject]()
        parameters["id"] = eventId as AnyObject
              
        APIManager.sharedInstance.deleteEvent(parameters, Target: self)
    }
    
    func deleteEventInvite(_ eventId:Int) {
        ApplicationDelegate.showLoader(loaderTitle_Deleting)

        APIManager.sharedInstance.cancelAllRequests()
        
        var parameters = [String: AnyObject]()
        parameters["user_id"] = Utilities.getUserDetails().id as AnyObject
        parameters["event_id"] = eventId as AnyObject
        
        APIManager.sharedInstance.deleteEventInvite(parameters, Target: self)
    }
     
    //MARK: - API Response
    func responseFetchMyEvents (_ notify: Foundation.Notification) {
        if btnMyEvents.isSelected {
            refreshControl.endRefreshing()
            
            NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFICATION_FETCHMYEVENTS), object: nil)
            
            if loadingIndicator.isAnimating {
                loadingIndicator.stopAnimating()
            }
            let swiftyJsonVar = JSON(notify.object!)
            let message = swiftyJsonVar["message"].stringValue
            let status = swiftyJsonVar["status"].intValue
            
            if swiftyJsonVar [ERROR_KEY].exists() {
                self.showErrorMessage(swiftyJsonVar[ERROR_KEY].stringValue)
            }
            else if (status == 1) {
                let response = swiftyJsonVar["response"]["events"]
                printCustom("response:\(response)")
                
                let recordsCount = swiftyJsonVar["response"]["count"].intValue
                if recordsCount > 0 {
                    if btnMyEvents.isSelected && offsetMyEvents == 0 {
                        if isSearchOn {
                            filterArrayEvents.removeAll()
                        }
                        else {
                            self.myEvents.removeAll()
                        }
                    }
                    
                    var events = [Event]()
                    events = Parser.getParsedEventsArrayFromData(response)
                    self.totalRecordsMyEvents = recordsCount//common for both search and normal nlist

                    if isSearchOn {
                        filterArrayEvents.append(contentsOf: events)
                    }
                    else if btnMyEvents.isSelected {
                       // self.totalRecordsMyEvents = recordsCount
                        self.myEvents.append(contentsOf: events)
                    }
                    
                    self.hideErrorMessage()
                }
            }
            else if (status == 0) { // Error response
                let responseErrorMessage = swiftyJsonVar["response"].stringValue
                self.showErrorMessage(responseErrorMessage)
            }
            else {
                self.showErrorMessage(message)
            }
        }
    }
    
    func responseFetchAllEvents (_ notify: Foundation.Notification) {
        if btnAllEvents.isSelected {
            refreshControl.endRefreshing()
            
            NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFICATION_FETCHALLEVENTS), object: nil)
            
            if loadingIndicator.isAnimating {
                loadingIndicator.stopAnimating()
            }
            let swiftyJsonVar = JSON(notify.object!)
            let message = swiftyJsonVar["message"].stringValue
            let status = swiftyJsonVar["status"].intValue
            
            if (swiftyJsonVar [ERROR_KEY] != nil) {
                self.showErrorMessage(swiftyJsonVar[ERROR_KEY].stringValue)
            }
            else if (status == 1) {
                let response = swiftyJsonVar["response"]["events"]
                printCustom("response:\(response)")
                
                let recordsCount = swiftyJsonVar["response"]["count"].intValue
                if recordsCount > 0 {
                    if btnAllEvents.isSelected && offsetAllEvents == 0{
                        if isSearchOn {
                            filterArrayEvents.removeAll()
                        }
                        else {
                            self.allEvents.removeAll()
                        }
                    }
                    
                    var events = [Event]()
                    events = Parser.getParsedEventsArrayFromData(response)
                    self.totalRecordsAllEvents = recordsCount

                    if isSearchOn {
                        filterArrayEvents.append(contentsOf: events)
                    }
                    else if btnAllEvents.isSelected {
                        //self.totalRecordsAllEvents = recordsCount
                        self.allEvents.append(contentsOf: events)
                    }
                    
                    self.hideErrorMessage()
                }
            }
            else if (status == 0) { // Error response
                let responseErrorMessage = swiftyJsonVar["response"].stringValue
                self.showErrorMessage(responseErrorMessage)
            }
            else {
                self.showErrorMessage(message)
            }
        }
    }
    
    func responseDeleteEvent(_ notify: Foundation.Notification) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFICATION_DELETEEVENT), object: nil)
        
        ApplicationDelegate.hideLoader ()
        let swiftyJsonVar = JSON(notify.object!)
        let message = swiftyJsonVar["message"].stringValue
        let status = swiftyJsonVar["status"].intValue
        
        if swiftyJsonVar [ERROR_KEY].exists() {
            self.showCommonAlert(swiftyJsonVar[ERROR_KEY].stringValue)
        }
        else if (status == 1) {
            let responseEventId = swiftyJsonVar["response"]["event_id"]
            printCustom("response:\(responseEventId)")
            
            let index = self.myEvents.index(where: { $0.id == responseEventId.intValue})

            self.myEvents.remove(at: index!)
           
            
            self.tblVwEvents.deleteRows(at:[IndexPath(row: index!,section: 0)], with: .fade)
        }
        else if (status == 0) { // Error response
            let responseErrorMessage = swiftyJsonVar["response"].stringValue
            self.showCommonAlert(responseErrorMessage)
        }
        else {
            self.showCommonAlert(message)
        }
    }
    
    func responseDeleteEventInvite(_ notify: Foundation.Notification) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFICATION_DELETEEVENTINVITE), object: nil)
        
        ApplicationDelegate.hideLoader ()
        let swiftyJsonVar = JSON(notify.object!)
        let message = swiftyJsonVar["message"].stringValue
        let status = swiftyJsonVar["status"].intValue
        
        if swiftyJsonVar [ERROR_KEY].exists() {
            self.showCommonAlert(swiftyJsonVar[ERROR_KEY].stringValue)
        }
        else if (status == 1) {
            let responseEventId = swiftyJsonVar["response"]["event_id"]
            printCustom("response:\(responseEventId)")
            
            let index = self.myEvents.index(where: { $0.id == responseEventId.intValue})
            
            self.myEvents.remove(at: index!)
            self.tblVwEvents.deleteRows(at:[IndexPath(row: index!, section: 0)], with: .fade)
        }
        else if (status == 0) { // Error response
            let responseErrorMessage = swiftyJsonVar["response"].stringValue
            self.showCommonAlert(responseErrorMessage)
        }
        else {
            self.showCommonAlert(message)
        }
    }
    
    // MARK: - Show/Hide Error Message
    func showErrorMessage(_ message:String) {
        self.tblVwEvents.isHidden = true
        self.btnRefresh.isHidden = false
        self.lblNoRecordsFound.isHidden = false
        self.lblNoRecordsFound.text = message
    }
    
    func hideErrorMessage() {
        self.btnRefresh.isHidden = true
        self.lblNoRecordsFound.isHidden = true
        self.tblVwEvents.isHidden = false
        self.tblVwEvents.reloadData()
    }
    
    // MARK: - Move to screen
    func moveToEditEvent(_ event:Event) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFICATION_RELOADEDITEDEVENT), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reloadEditedEvent(_:)), name: NSNotification.Name(rawValue: NOTIFICATION_RELOADEDITEDEVENT), object: nil)

        isEditingEvent = true
        
        let createEventFirstVC = UIStoryboard(name: "Events", bundle: nil).instantiateViewController(withIdentifier: "CreateEventFirstId") as? CreateEventFirstViewController
        createEventFirstVC?.passedEvent = event
        self.navigationController?.pushViewController(createEventFirstVC!, animated: true)
    }
    
    // MARK: - Reload Edited Event
    func reloadEditedEvent(_ notif:Foundation.Notification) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFICATION_RELOADEDITEDEVENT), object: nil)
        
        let eventDetail = notif.object as! Event
        let index = self.myEvents.index(where: { $0.id == eventDetail.id})
        
        self.myEvents[index!] = eventDetail
        self.tblVwEvents.reloadRows(at: [IndexPath(row: index!, section: 0)], with: .fade)
    }
    
    func reloadCreatedEvent(_ notif:Foundation.Notification) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFICATION_RELOADCREATEDEVENT), object: nil)
        
        self.fetchEvents()
    }
    
    // MARK: - Prepare for Segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "eventDetailsId" {
            let destinationViewController:EventDetailsVc = segue.destination as! EventDetailsVc
            printCustom("myEvents:\(myEvents)")
            printCustom("index:\(selectedEventIndex)")
            let events:[Event]?
            if isSearchOn
            {
                events = filterArrayEvents
            }
            else
            {
                events = btnMyEvents.isSelected ? myEvents : allEvents
            }
            let event = events?[selectedEventIndex]
            destinationViewController.event = event
            destinationViewController.sourceScreen = SourceScreenEventDetails.eventList
        }
        else {
            isEditingEvent = false
            
            NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFICATION_RELOADCREATEDEVENT), object: nil)
            NotificationCenter.default.addObserver(self, selector: #selector(reloadCreatedEvent(_:)), name: NSNotification.Name(rawValue: NOTIFICATION_RELOADCREATEDEVENT), object: nil)
        }
    }

    
}
