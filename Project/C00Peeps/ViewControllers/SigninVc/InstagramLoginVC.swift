//
//  InstagramLoginVC.swift
//  C00Peeps
//
//  Created by Arshdeep on 27/09/16.
//  Copyright © 2016 SOTSYS011. All rights reserved.
//

import UIKit
import InstagramKit

class InstagramLoginVC: BaseVC {

    @IBOutlet var webView: UIWebView!
    var signUpDelegate:SignUpVc_Form?
    var signInDelegate:SigninVc?
    var linkedAccountDelegate:LinkedAccountViewController?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
//        if signUpDelegate != nil {
//            ApplicationDelegate.showLoader(loaderTitle_FetchingProfile)
//        }
//        else {
            ApplicationDelegate.showLoader(loaderTitle_PleaseWait)
        //}
        
        self.webView.scrollView.bounces = false
        
        let authURL: URL = InstagramEngine.shared().authorizationURL()
        printCustom("authURL: \(authURL)")
        let authURLRequest: URLRequest = URLRequest(url:authURL)
        self.webView.loadRequest(authURLRequest)
      
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        ApplicationDelegate.hideLoader()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    // MARK:- Web View Delegates
    func webViewDidFinishLoad(_ webView: UIWebView) {
        // When login page of instagram finishes loading, hide loader.
        ApplicationDelegate.hideLoader()
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: NSError?) {
        printCustom("didFailLoadWithError error:\(error)")
          ApplicationDelegate.hideLoader()
        
        let alert:UIAlertController = UIAlertController(title: "", message: alertMsg_SomethingWentWrong, preferredStyle: .alert)
        let alertAction = UIAlertAction(title: alertBtnTitle_OK, style: .default, handler:{ (UIAlertAction) -> Void in
            self.dismiss(animated: true, completion: nil)
        })
        alert.addAction(alertAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func webView(_ webView: UIWebView!, shouldStartLoadWithRequest request: URLRequest!, navigationType: UIWebViewNavigationType) -> Bool {
        do {
            try InstagramEngine.shared().receivedValidAccessToken(from: request.url!)
            self.authenticationSuccess()

            InstagramEngine.shared().getSelfUserDetails(success: { (user) in
                printCustom("instagram user full name:\(user.fullName)")
                printCustom("instagram user username:\(user.username)")
                printCustom("instagram user id:\(user.id)")
                
                if self.linkedAccountDelegate != nil {
                    self.linkedAccountDelegate!.instagramAccountLinkedWithUsername(user.username)
                }
                else if self.signUpDelegate != nil {
                    self.signUpDelegate!.socialMediaID = user.id
                    self.signUpDelegate!.prefillSocialMediaProfile(name: user.fullName!, email: "")
                }
                else {
                    self.signInDelegate!.instagramProfile(id: user.id, name: user.fullName!)
                    //self.signInDelegate!.callAPI_CheckSocialUserExists(AccountType.Instagram, socialMediaID: user.Id)
                }

                }, failure: nil)
            
        }
        catch _ {
            printCustom("No token found")
        }
      
        return true
    }
    
    // MARK:- Successful authentication
    func authenticationSuccess () {        
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnCancelClicked(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
