 //
 //  SigninVc.swift
 //  C00Peeps
 //
 //  Created by SOTSYS011 on 3/9/16.
 //  Copyright © 2016 SOTSYS011. All rights reserved.
 //
 
 import UIKit
 import SwiftyJSON
 import FBSDKLoginKit
 import TwitterKit
 
 class SigninVc: BaseVC
 {
    @IBOutlet var txtUserName: UITextField!
    @IBOutlet var txtPassword: UITextField!
    @IBOutlet weak var imgVwBg: UIImageView!
    @IBOutlet weak var imgVwLogo: UIImageView!
    @IBOutlet weak var btnSignIn: UIButton!
    @IBOutlet weak var btnFacebook: UIButton!
    @IBOutlet weak var btnTwitter: UIButton!
    @IBOutlet weak var btnInstagram: UIButton!
    var socialDetailsFromLogin = (accountType: AccountType.normal.rawValue, id:"", name: "", email: "")
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.title = "Sign In"
        SetPaddingView(txtUserName)
        SetPaddingView(txtPassword)
        txtUserName.text = "ashishkhurana"
        txtPassword.text = "Trantor123"
        
        //        txtUserName.text = "aditya"
        //        txtPassword.text = "admin"
        
        self.updateViewAccordingToTheme()
        
        self.perform(#selector (SigninVc.setTextFieldLines), with: nil, afterDelay: 0.1)
        
        NotificationCenter.default.addObserver(self, selector: #selector(navigateToSignUpScreen), name: NSNotification.Name(rawValue: "NavigateToSignup"), object: nil)
    }
    
    // MARK: - Update View According To Selected Theme
    func updateViewAccordingToTheme() {
        imgVwBg.image = Utilities().themedImage(img_bg)
        imgVwLogo.image = Utilities().themedImage(img_signInLogo)
        
        // Submit
        btnSignIn.setBackgroundImage(Utilities().themedImage(img_btn), for: .normal)
        btnSignIn.setBackgroundImage(Utilities().themedImage(img_btnSelected), for: .highlighted)
        
        // Facebook
        btnFacebook.setImage(Utilities().themedImage(img_facebook), for: .normal)
        btnFacebook.setImage(Utilities().themedImage(img_facebookSelected), for: .highlighted)
        
        // Twitter
        btnTwitter.setImage(Utilities().themedImage(img_twitter), for: .normal)
        btnTwitter.setImage(Utilities().themedImage(img_twitterSelected), for: .highlighted)
        
        // Instagram
        btnInstagram.setImage(Utilities().themedImage(img_instagram), for: .normal)
        btnInstagram.setImage(Utilities().themedImage(img_instagramSelected), for: .highlighted)
    }
    
    
    func setTextFieldLines(){
        setSubViewBorder(txtUserName, color: UIColor.white)
        setSubViewBorder(txtPassword, color: UIColor.white)
    }
    
    override func viewDidLayoutSubviews() {
        //        setSubViewBorder(txtUserName, color: UIColor.whiteColor())
        //        setSubViewBorder(txtPassword, color: UIColor.whiteColor())
    }
    
    // MARK: - Fetch Facebook Profile
    func fetchFacebookProfile() {
        ApplicationDelegate.showLoader(loaderTitle_FetchingProfile)
        FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, email"]).start(completionHandler: { (connection, result, error) -> Void in
            ApplicationDelegate.hideLoader()
            if (error == nil){
                
                let resultDict = result as! NSDictionary
                printCustom("profile:\(result)")
                self.socialDetailsFromLogin.accountType = AccountType.facebook.rawValue
                self.socialDetailsFromLogin.id = resultDict["id"] as! String
                self.socialDetailsFromLogin.name = resultDict["name"] as! String
                self.socialDetailsFromLogin.email = resultDict["email"] as! String
                self.callAPI_CheckSocialUserExists(AccountType.facebook, socialMediaID: resultDict["id"] as! String)
            }
        })
    }
    
    // MARK: - Instagram Login Callback
    func instagramProfile(id:String, name:String) {
        self.socialDetailsFromLogin.accountType = AccountType.instagram.rawValue
        self.socialDetailsFromLogin.id = id
        self.socialDetailsFromLogin.name = name
        self.socialDetailsFromLogin.email = ""
        self.callAPI_CheckSocialUserExists(AccountType.instagram, socialMediaID: id)
    }
    
    //MARK: - IBAction
    
    @IBAction func btnLogin_Clicked(sender: AnyObject)
    {
        //        self.navigationController?.navigationBarHidden = true
        //        let homeScreen = self.storyboard?.instantiateViewControllerWithIdentifier("tabBarcontroller") as? UITabBarController
        //        self.navigationController?.pushViewController(homeScreen!, animated: true)
        
        //        let ContactListObject = self.storyboard?.instantiateViewControllerWithIdentifier("ContactSelectionVc") as? ContactSelectionVc
        //        self.navigationController?.pushViewController(ContactListObject!, animated: true)
        //
        //
        //        return
        
        if txtUserName.text!.isEmpty {
            self.showCommonAlert(alertMsg_EmptyUsername)
            return
        }
        else if txtPassword.text!.isEmpty {
            self.showCommonAlert(alertMsg_EmptyPasscode)
            return
        }
        else
        {
            checkAccountActivatedFromAddContacts = false
            self.callAPI_SignIn(self.txtUserName.text!, password:self.txtPassword.text!)
        }
    }
    
    @IBAction func btnLoginWithFBClicked(sender: AnyObject) {
        if FBSDKAccessToken.current() != nil {
            self.fetchFacebookProfile()
        }
        else {
            let facebookLogin = FBSDKLoginManager()
            
            facebookLogin.logIn(withReadPermissions: ["email"], from: self, handler: { (facebookResult: FBSDKLoginManagerLoginResult!, facebookError: Error!) in
                if facebookResult != nil && !facebookResult.isCancelled {
                    self.fetchFacebookProfile()
                }
            })
//            
//            facebookLogin.logInWithReadPermissions(["email"],  fromViewController: self) { (facebookResult: FBSDKLoginManagerLoginResult!, facebookError: NSError!) -> Void in
//                if facebookResult != nil && !facebookResult.isCancelled {
//                    self.fetchFacebookProfile()
//                }
//            }
        }
    }
    
    @IBAction func btnLoginWithTwitterClicked(sender: AnyObject) {
        ApplicationDelegate.showLoader(loaderTitle_PleaseWait)
        
        /*let store = Twitter.sharedInstance().sessionStore
         if store.session() != nil {
         if let userID:String = store.session()!.userID {
         store.logOutUserID(userID)
         ApplicationDelegate.hideLoader()
         
         if ACAccountStore().accountTypeWithAccountTypeIdentifier(ACAccountTypeIdentifierTwitter).accessGranted {
         ApplicationDelegate.showLoader(loaderTitle_ConnectingTwitter)
         }
         }
         }
         else {
         self.performSelector(#selector(hideLoaderConnectingTwitter), withObject: nil, afterDelay: 2)
         }
         
         Twitter.sharedInstance().logInWithCompletion { session, error in
         ApplicationDelegate.hideLoader()
         
         if error != nil {
         printCustom("error:\(error?.code)")
         printCustom("description:\(error?.description)")
         if error?.code != 1 {
         self.showCommonAlert(alertMsg_ErrorConnectingTwitter)
         }
         
         return
         }
         if (session != nil) {
         printCustom("signed in as \(session!.userName)");
         
         let client = TWTRAPIClient(userID: session?.userID)
         client.loadUserWithID(session!.userID) { (user, error) -> Void in
         printCustom("twitter user profile: \(user)");
         self.socialDetailsFromLogin.accountType = AccountType.Twitter.rawValue
         self.socialDetailsFromLogin.id = session!.userID
         self.socialDetailsFromLogin.name = user!.name
         self.socialDetailsFromLogin.email = ""
         self.callAPI_CheckSocialUserExists(AccountType.Twitter, socialMediaID: session!.userID)
         }
         }
         }*/
        
        let store = TWTRTwitter.sharedInstance().sessionStore
        if store.session() == nil {
            ApplicationDelegate.showLoader(loaderTitle_ConnectingTwitter)
            
            self.perform(#selector(hideLoaderConnectingTwitter), with: nil, afterDelay: 2)
            TWTRTwitter.sharedInstance().logIn { session, error in
                ApplicationDelegate.hideLoader()
                
                if error != nil {
                    printCustom("error:\(error?.code)")
                    //printCustom("description:\(error?.description)")
                    if error?.code != 1 {
                        self.showCommonAlert(alertMsg_ErrorConnectingTwitter)
                    }
                    
                    return
                }
                else {
                    ApplicationDelegate.showLoader(loaderTitle_FetchingProfile)
                }
                if (session != nil) {
                    printCustom("signed in as \(session!.userName)");
                    
                    self.signedInWithTwitter(userID: session!.userID)
                }
                else {
                    ApplicationDelegate.hideLoader()
                }
            }
        }
        else {
            ApplicationDelegate.showLoader(loaderTitle_FetchingProfile)
            
            self.signedInWithTwitter(userID: store.session()!.userID)
        }
    }
    
    func signedInWithTwitter(userID:String) {
        let client = TWTRAPIClient(userID: userID)
        client.loadUser(withID: userID) { (user, error) -> Void in
            printCustom("twitter user profile: \(user)");
            self.socialDetailsFromLogin.accountType = AccountType.twitter.rawValue
            self.socialDetailsFromLogin.id = userID
            self.socialDetailsFromLogin.name = user!.name
            self.socialDetailsFromLogin.email = ""
            self.callAPI_CheckSocialUserExists(AccountType.twitter, socialMediaID: userID)
        }
    }
    
    @IBAction func btnLoginWithInstagramClicked(sender: AnyObject) {
        let navController:UINavigationController = self.storyboard?.instantiateViewController(withIdentifier: "InstagramLoginID") as! UINavigationController
        let instagramLoginVC:InstagramLoginVC = navController.topViewController as! InstagramLoginVC
        instagramLoginVC.signInDelegate = self
        self.present(navController, animated: true, completion: nil)
        
    }
    
    //MARK: - Hide Loader
    func hideLoaderConnectingTwitter() {
        ApplicationDelegate.hideLoader()
    }
    
    
    override func navigateToSignUpScreen() {
        printCustom("signin vc navigateToSignUpScreen")
        
        let signUpScreen = self.storyboard?.instantiateViewController(withIdentifier: "SignUpVc_Main") as? SignUpVc_Main
        signUpScreen?.socialDetailsFromLogin.accountType = self.socialDetailsFromLogin.accountType
        signUpScreen?.socialDetailsFromLogin.id = self.socialDetailsFromLogin.id
        signUpScreen?.socialDetailsFromLogin.name = self.socialDetailsFromLogin.name
        signUpScreen?.socialDetailsFromLogin.email = self.socialDetailsFromLogin.email
        self.navigationController?.pushViewController(signUpScreen!, animated: true)
    }
    
    //    //MARK: - Call API
    //
    //    func callAPI_CheckSocialUserExists(accountType: AccountType, socialMediaID: String) {
    //        var parameters = [String: AnyObject]()
    //
    //        parameters["social_account_type"] = accountType.rawValue
    //        if accountType.rawValue != 0 {
    //            parameters["social_unique_id"] = socialMediaID
    //        }
    //
    //        printCustom("check social user exists parameters:\(parameters)")
    //
    //        ApplicationDelegate.showLoader(loaderTitle_SigningIn)
    //        APIManager.sharedInstance.requestCheckSocialUserExists(parameters, Target: self)
    //    }
    //
    //    //MARK: - API Response
    //
    //    func responseCheckSocialUserExists (notify: NSNotification) {
    //        NSNotificationCenter.defaultCenter().removeObserver(self, name: NOTIFICATION_CHECKSOCIALUSEREXISTS, object: nil)
    //
    //        ApplicationDelegate.hideLoader ()
    //
    //        let swiftyJsonVar = JSON(notify.object!)
    //        let message = swiftyJsonVar["message"].stringValue
    //        let status = swiftyJsonVar["status"].intValue
    //
    //        if swiftyJsonVar [ERROR_KEY].exists() {
    //            self.showCommonAlert(swiftyJsonVar[ERROR_KEY].stringValue)
    //        }
    //        else if (status == 1) {
    //            let response = swiftyJsonVar["response"]
    //            let responseUserDetail:[String:AnyObject] = response.dictionaryObject!
    //            let userDetail:UserDetail = UserDetail()
    //            Parser.parseUserDetails(responseUserDetail, userDetail: userDetail)
    //
    //            self.navigationController?.navigationBarHidden = true
    //            let homeScreen = self.storyboard?.instantiateViewControllerWithIdentifier("tabBarcontroller") as? UITabBarController
    //            self.navigationController?.pushViewController(homeScreen!, animated: true)
    //        }
    //        else if (status == 0) {//If user not registered as social user, then redirect sign up flow with social info prefilled.
    //
    //            let signUpScreen = self.storyboard?.instantiateViewControllerWithIdentifier("SignUpVc_Main") as? SignUpVc_Main
    //            signUpScreen?.socialDetailsFromLogin.accountType = self.socialDetailsFromLogin.accountType
    //            signUpScreen?.socialDetailsFromLogin.id = self.socialDetailsFromLogin.id
    //            signUpScreen?.socialDetailsFromLogin.name = self.socialDetailsFromLogin.name
    //            signUpScreen?.socialDetailsFromLogin.email = self.socialDetailsFromLogin.email
    //            self.navigationController?.pushViewController(signUpScreen!, animated: true)
    //        }
    //        else {
    //            self.showCommonAlert(message)
    //        }
    //    }
    
 }
