//
//  SettingViewCell.swift
//  C00Peeps
//
//  Created by SOTSYS027 on 3/23/16.
//  Copyright © 2016 SOTSYS011. All rights reserved.
//

import UIKit

class SettingViewCell: UITableViewCell
{

    @IBOutlet weak var lblFeatureText: UILabel!
    @IBOutlet weak var swtichNot: UISwitch!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
