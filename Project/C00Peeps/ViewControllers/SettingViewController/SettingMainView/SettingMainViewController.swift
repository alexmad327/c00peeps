//
//  SettingMainViewController.swift
//  C00Peeps
//
//  Created by SOTSYS027 on 3/23/16.
//  Copyright © 2016 SOTSYS011. All rights reserved.
//

import UIKit
import SwiftyJSON
import Contacts
import TwitterKit
class SettingMainViewController: BaseVC,UITableViewDataSource,UITableViewDelegate
{

    
    @IBOutlet weak var tblSelection: UITableView!
    
    let contactStore = CNContactStore()
    var settings: [Setting] = []
    
    // Setting 1
    let headerFollowPeople = "Follow People"
    //let settingFacebookFriends = "Facebook Friends"
    let settingTwitterFriends = "Twitter Friends"
    //let settingInstagramFriends = "Instagram Friends"
    let settingFindContacts = "Find Contacts"
    let settingInviteFriends = "Invite Friends"
    
    // Setting 2
    let headerAccount = "Account"
    let settingEditProfile = "Edit Profile"
    let settingEditSubmerchantAccount = "Edit Submerchant Account"
    let settingChangePassword = "Change Password"
    let settingPostsYouLiked = "Posts You've Liked"
    let settingChangeAccountType = "Change Account Type"
    let settingUpgradePackage = "Upgrade Package"

    // Setting 3
    let headerSetting = "Setting"
    let settingLinkedAccounts = "Linked Accounts"
    let settingPushNotificationSettings = "Push Notification Settings"
    //let settingCellularDataUse = "Cellular Data Use"
    let settingChangeScreenColors = "Change Screen Colors"
    let settingChangePECxCode = "Change PECx Code"
    let settingSaveOriginalPhotos = "Save Original Photos"
    let settingClearThemeData = "Clear Theme Data"

    // Setting 4
    let headerSupport = "Support"
    let settingHelpCenter = "Help Center"
    let settingReportProblem = "Report a Problem"
    
    // Setting 5
    let headerAbout = "About"
//    let settingAds = "Ads"
//    let settingBlog = "Blog"
    let settingPrivacyPolicy = "Privacy Policy"
    let settingTerms = "Terms"
    let settingAboutVersion = "Version"
    
    // Setting 6
    let headerOthers = "Others"
//    let settingClearSearchHistory = "Clear Search History"
    let settingLogout = "Logout"

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Show tab bar for this controller.
        self.tabBarController?.tabBar.isHidden = false
        
        self.perform(#selector(prepareSettingsDataSource), with: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Prepate Datasource
    func prepareSettingsDataSource() {
        settings = []
        
        let setting1:Setting = Setting()
        setting1.key = headerFollowPeople
        setting1.values = [settingTwitterFriends, settingFindContacts, settingInviteFriends]
        setting1.keyHeaderImage = Utilities().themedImage(img_followPeople)
        settings.append(setting1)
        
        let setting2:Setting = Setting()
        setting2.key = headerAccount
        setting2.values = [settingEditProfile, settingEditSubmerchantAccount, settingChangePassword, settingPostsYouLiked, settingChangeAccountType, settingUpgradePackage]
        if Utilities.getUserDetails().submerchantExists != "1" {//Don't show edit submerchant account if submerchant doesn't exists.
            setting2.values.remove(at: 1)
        }
        if Utilities.getUserDetails().package_id == "4" {//Don't show upgrade package option if user is already upgraded to highest package (i.e. platinum)
            setting2.values.removeLast()
        }
        setting2.keyHeaderImage = Utilities().themedImage(img_account)
        settings.append(setting2)
        
        let setting3:Setting = Setting()
        setting3.key = headerSetting
        setting3.values = [settingLinkedAccounts, settingPushNotificationSettings, settingChangeScreenColors, settingChangePECxCode, settingSaveOriginalPhotos, settingClearThemeData]
        setting3.keyHeaderImage = Utilities().themedImage(img_settingSettings)
        settings.append(setting3)
        
        let setting4:Setting = Setting()
        setting4.key = headerSupport
        setting4.values = [settingHelpCenter, settingReportProblem]
        setting4.keyHeaderImage = Utilities().themedImage(img_support)
        settings.append(setting4)
        
        let setting5:Setting = Setting()
        setting5.key = headerAbout
        setting5.values = [settingAboutVersion + getBuildVersionNumber(), settingPrivacyPolicy, settingTerms]
        setting5.keyHeaderImage = Utilities().themedImage(img_about)
        settings.append(setting5)
        
        let setting6:Setting = Setting()
        setting6.key = headerOthers
        setting6.values = [settingLogout]
        setting6.keyHeaderImage = Utilities().themedImage(img_others)
        settings.append(setting6)
        
        self.tblSelection.reloadData()
    }

    func getBuildVersionNumber() -> String {
        //First get the nsObject by defining as an optional anyObject
        let buildNumber: AnyObject? = Bundle.main.infoDictionary?["CFBundleVersion"] as AnyObject
        let versionNumber: AnyObject? = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as AnyObject

        let strBuildNumber = buildNumber as! String
        let strVersionNumber = versionNumber as! String

        let version = " " + strBuildNumber + "(" + strVersionNumber + ")"
        return version
    }
    
    //MARK: - UITableViewDataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return settings.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let settingValues = settings[section].values
        return settingValues.count
    }
   
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : SettingViewCell! = tblSelection.dequeueReusableCell(withIdentifier: "SettingCell",for: indexPath) as! SettingViewCell
      
        let settingValues = settings[indexPath.section].values
        cell.lblFeatureText?.text = settingValues[indexPath.row]
        if cell.lblFeatureText?.text == settingSaveOriginalPhotos {
            cell.swtichNot.isHidden = false
            cell.swtichNot.tag = 100
            if Utilities().saveOriginalPhoto()
            {
                cell.swtichNot.isOn = true
            }
            else
            {
                cell.swtichNot.isOn = false
            }
        }
        else if cell.lblFeatureText?.text == settingClearThemeData {
            cell.swtichNot.isHidden = false
            cell.swtichNot.tag = 101
            
            if Utilities.getUserDetails().theme_id != "1" && currentThemeId != "1" {//If no theme to delete, then disable this option.
                cell.swtichNot.isEnabled = true
                
                if Utilities().getClearThemeDataSetting() == true
                {
                    cell.swtichNot.isOn = true
                }
                else
                {
                    cell.swtichNot.isOn = false
                }

            }
            else {
                cell.swtichNot.isEnabled = false
                cell.swtichNot.isOn = false
            }
        }
        else {
            cell.swtichNot.isHidden = true
        }
        return cell
    }
    
    //MARK: - UITableViewDelegate

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderCell") as! SettingsHeaderTableViewCell
        let setting = settings[section]
        cell.lblSettingsHeader.text = setting.key
        cell.imgVwSettingsHeaderIcon.image = setting.keyHeaderImage
       
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        switch indexPath.section {
        case 0:
            printCustom("\(headerFollowPeople)")
            
            switch indexPath.row {
            case 0:
                printCustom("\(settingTwitterFriends)")
                self.connectWithTwitter()
            case 1:
                printCustom("\(settingFindContacts)")
                self.checkIfUserAccessGranted()
            default:
                printCustom("\(settingInviteFriends)")
               self.moveToInviteFriends()
            }
        case 1:
            printCustom("\(headerAccount)")
            let settingValues = settings[indexPath.section].values[indexPath.row]

            switch settingValues {
            case settingEditProfile:
                printCustom("\(settingEditProfile)")
                self.moveToEditProfile()
            case settingEditSubmerchantAccount:
                printCustom("\(settingEditSubmerchantAccount)")
                self.checkSubmerchantAccountExists()
            case settingChangePassword:
                printCustom("\(settingChangePassword)")
                self.moveToChangePassword()
            case settingPostsYouLiked:
                printCustom("\(settingPostsYouLiked)")
                self.moveToPostsLiked()
            case settingChangeAccountType:
                printCustom("\(settingChangeAccountType)")
                self.moveToChangeAccountType()
            default:
                printCustom("\(settingUpgradePackage)")
                self.moveToUpgradePackage()
            }
        case 2:
            printCustom("\(headerSetting)")
            
            switch indexPath.row {
            case 0:
                printCustom("\(settingLinkedAccounts)")
                self.moveToLinkedAccount()
            case 1:
                printCustom("\(settingPushNotificationSettings)")
                self.moveToPushNotificationSettings()
                
            case 2:
                printCustom("\(settingChangeScreenColors)")
                self.moveToChangeScreenColors()
                
            case 3:
                printCustom("\(settingChangePECxCode)")
                self.moveToChangePeepCode()

            default:
                printCustom("\(settingSaveOriginalPhotos)")
            }
            
        case 3:
            printCustom("\(headerSupport)")
            
            switch indexPath.row {
            case 0:
                printCustom("\(settingHelpCenter)")
            default:
                printCustom("\(settingReportProblem)")

                let actionSheet =  UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
                actionSheet.addAction(UIAlertAction(title: alertBtnTitle_PECxNotWorking, style: UIAlertActionStyle.default, handler:{ action -> Void in
                    self.moveToReportProblem(alertBtnTitle_PECxNotWorking)
                }))
                actionSheet.addAction(UIAlertAction(title: alertBtnTitle_GeneralFeedback, style: UIAlertActionStyle.default, handler:{ action -> Void in
                    self.moveToReportProblem(alertBtnTitle_GeneralFeedback)
                 }))
                actionSheet.addAction(UIAlertAction(title: alertBtnTitle_Cancel, style: UIAlertActionStyle.cancel, handler:nil))
                self.present(actionSheet, animated: true, completion: nil)
            }
            
        case 4:
            printCustom("\(headerAbout)")
            switch indexPath.row {
            case 0:
                printCustom("\(settingPrivacyPolicy)")
                self.moveToPrivacyPolicy(PRIVACY_POLICY_URL)
                
            default:
                printCustom("\(settingTerms)")
                self.moveToPrivacyPolicy(TERMS_URL)
            }

        default:
            printCustom("\(headerOthers)")

            printCustom("\(settingLogout)")
            self.callLogout()
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 51
    }
   
       
    
    //MARK: - Request Access for Contacts
    func checkIfUserAccessGranted() {
        requestForAccess { (accessGranted) -> Void in 
            if accessGranted {
                DispatchQueue.main.async(execute: { () -> Void in
                    self.moveToContactsWithTwitterFriends(false, sessionUserID:"")
                })
            }
        }
    }
    
    func requestForAccess(_ completionHandler: @escaping (_ accessGranted: Bool) -> Void) {
        let authorizationStatus = CNContactStore.authorizationStatus(for: CNEntityType.contacts)
        switch authorizationStatus {
        case .authorized:
            completionHandler(true)
            
        case .denied, .notDetermined:
            self.contactStore.requestAccess(for: CNEntityType.contacts, completionHandler: { (access, accessError) -> Void in
                if access {
                    completionHandler(access)
                }
                else {
                    if authorizationStatus == CNAuthorizationStatus.denied {
                        DispatchQueue.main.async(execute: { () -> Void in
                            let message = "\(accessError!.localizedDescription)\n\n" + alertMsg_AllowAppToAccessContacts
                            self.showCommonAlert(message)
                        })
                    }
                }
            })
            
            
        default:
            completionHandler(false)
        }
    }
    
    
    //MARK: - Connect With Twitter
    func connectWithTwitter() {
        
        
        let store = TWTRTwitter.sharedInstance().sessionStore
        if store.session() == nil {
            ApplicationDelegate.showLoader(loaderTitle_ConnectingTwitter)
            
            self.perform(#selector(hideLoaderConnectingTwitter), with: nil, afterDelay: 2)
            TWTRTwitter.sharedInstance().logIn { session, error in
                ApplicationDelegate.hideLoader()
                
                if error != nil {
                    printCustom("error:\(error?.code)")
                   // printCustom("description:\(error?.description)")
                    if error?.code != 1 {
                        self.showCommonAlert(alertMsg_ErrorConnectingTwitter)
                    }
                                       
                    return
                }
                else {
                    ApplicationDelegate.showLoader(loaderTitle_FetchingProfile)
                }
                if (session != nil) {
                    printCustom("signed in as \(session!.userName)");
                    
                   self.signedInWithTwitter(session!.userID)
                }
                else {
                    ApplicationDelegate.hideLoader()
                }
            }
        }
        else {
            ApplicationDelegate.showLoader(loaderTitle_FetchingProfile)
            
            // printCustom("signed in as \(store.session().userName)");
            
            self.signedInWithTwitter(store.session()!.userID)
        }
        /*ApplicationDelegate.showLoader(loaderTitle_ConnectingTwitter)
        
        let store = Twitter.sharedInstance().sessionStore
        if store.session() != nil {
            if let userID:String = store.session()!.userID {
                store.logOutUserID(userID)
                ApplicationDelegate.hideLoader()
                
                if ACAccountStore().accountTypeWithAccountTypeIdentifier(ACAccountTypeIdentifierTwitter).accessGranted {
                    ApplicationDelegate.showLoader(loaderTitle_ConnectingTwitter)
                }
            }
        }
        else {
            self.performSelector(#selector(hideLoaderConnectingTwitter), withObject: nil, afterDelay: 2)
        }
        
        Twitter.sharedInstance().logInWithCompletion { session, error in
            ApplicationDelegate.hideLoader()
            
            if error != nil {
                printCustom("error:\(error?.code)")
                printCustom("description:\(error?.description)")
                if error?.code != 1 {
                    self.showCommonAlert(alertMsg_ErrorConnectingTwitter)
                }
                
                return
            }
            if (session != nil) {
                printCustom("signed in as \(session!.userName)");
                self.moveToContactsWithTwitterFriends(true, sessionUserID: session?.userID)
            }
        }
        */
    }
    
    func signedInWithTwitter(_ userID:String) {
        let client = TWTRAPIClient(userID: userID)
        client.loadUser(withID: userID) { (user, error) -> Void in
            ApplicationDelegate.hideLoader()
            
            printCustom("twitter user profile: \(user)");
            self.moveToContactsWithTwitterFriends(true, sessionUserID: userID)
        }

    }

    
    func hideLoaderConnectingTwitter() {
        ApplicationDelegate.hideLoader()
    }
    
    //MARK:- Change Original Photo Settings
    
    @IBAction func switchOriginalPhotoValueChanged(_ sender: AnyObject) {
        
       let swtichNot = sender as? UISwitch
        
        if swtichNot!.isOn {
            swtichNot!.isOn = true
        }
        else {
            swtichNot!.isOn = false
        }

        if swtichNot?.tag == 100 {
            Utilities().setSaveOriginalPhoto(swtichNot!.isOn)
        }
        else {
            if Utilities.getUserDetails().theme_id != "1" && currentThemeId != "1" {
                let alert:UIAlertController = UIAlertController(title: "", message: alertMsg_SwitchToDefaultTheme, preferredStyle: .alert)
                let alertAction = UIAlertAction(title: alertBtnTitle_Logout, style: .default, handler: { (UIAlertAction) -> Void in
                    Utilities().setClearThemeDataSetting(swtichNot!.isOn)
                    // Update Theme ID.
                    let userDetail:UserDetail = Utilities.getUserDetails()
                    userDetail.theme_id = "1"
                    Utilities.setUserDetails(userDetail)
                    
                    currentThemeId = "1"
                    
                    self.removeThemeFilesFromDocDir()
                    
                    self.callLogout()
                })
                alert.addAction(alertAction)
                
                let alertCancel = UIAlertAction(title: alertBtnTitle_Cancel, style: .cancel, handler: { (UIAlertAction) -> Void in
                    Utilities().setClearThemeDataSetting(false)
                    self.tblSelection.reloadData()
                })
                alert.addAction(alertCancel)
                
                self.present(alert, animated: true, completion: nil)
            }
            
           
        }
    }
    
    
    //MARK: - Remove Theme Files from Docs Directory
    func removeThemeFilesFromDocDir() {
        let fileManager = FileManager()

        let paths = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.allDomainsMask, true)
        let documentsDirectory:String = paths.first!
        printCustom("documentsDirectory:\(documentsDirectory)")
        
        for themeId in 1...6 {
            if themeId != 1 {
                let themeName = "theme" + String(themeId)
                let fileName = URL(fileURLWithPath:documentsDirectory).appendingPathComponent(themeName)
                printCustom("fileName: \(fileName)")

                do {
                    try fileManager.removeItem(atPath: fileName.path)
                }
                catch{
                    printCustom("Could not delete file: \(error)")
                }
            }
        }
    }
    
    //MARK:- Move to Screen
    func moveToContactsWithTwitterFriends(_ showTwitterFriends:Bool, sessionUserID:String?) {
        let contactListVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ContactListVc") as? ContactListVc
        if showTwitterFriends {
            contactListVc!.addContactType = AddContactType.twitter
        }
        else {
            contactListVc!.addContactType = AddContactType.phoneBook
        }
        contactListVc!.isComingFromSettings = true
        contactListVc!.sessionUserID = sessionUserID
        contactListVc!.syncType = SyncType.notFollowingUsers
        self.navigationController?.pushViewController(contactListVc!, animated: true)
    }
    
    func moveToInviteFriends() {
        let contactSelectionVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ContactSelectionVc") as? ContactSelectionVc
        contactSelectionVc?.isComingFromSettings = true
        contactSelectionVc?.syncType = SyncType.notPecXUsers
        self.navigationController?.pushViewController(contactSelectionVc!, animated: true)
    }
    
    func moveToChangePeepCode() {
        let genrateCodeVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "GenrateCodeVc") as? GenrateCodeVc
        genrateCodeVc!.isComingFromSettings = true
        self.navigationController?.pushViewController(genrateCodeVc!, animated: true)
    }
    
    func moveToChangeScreenColors() {
        let customizeScreenVc : CustomizeScreenVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CustomizeScreenVc") as! CustomizeScreenVc
        customizeScreenVc.isComingFromSettings = true
        self.navigationController?.pushViewController(customizeScreenVc, animated: true)
    }
    
    func moveToChangeAccountType() {
        let accountAuthenticationVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AccountAuthenticationVc") as? AccountAuthenticationVc
        accountAuthenticationVc?.isComingFromSettings = true
        self.navigationController?.pushViewController(accountAuthenticationVc!, animated: true)
    }
    
    func moveToUpgradePackage() {
        let accountAuthenticationVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SelectPackageVC") as? SelectPackageVC
        accountAuthenticationVc?.isComingFromSettings = true
        self.navigationController?.pushViewController(accountAuthenticationVc!, animated: true)
    }
    
    func moveToPushNotificationSettings() {
        let pushNotificationSettingsViewController = UIStoryboard(name: "ProfileStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "PushNotifSettingsId") as? PushNotificationSettingsViewController
        self.navigationController?.pushViewController(pushNotificationSettingsViewController!, animated: true)
    }
    
    func moveToLinkedAccount() {
        let linkedAccountViewController = UIStoryboard(name: "ProfileStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "LinkedAccountId") as? LinkedAccountViewController
        self.navigationController?.pushViewController(linkedAccountViewController!, animated: true)
    }
    
    func moveToChangePassword() {
        let changePasscodeViewController = UIStoryboard(name: "ProfileStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "ChangePasscodeId") as? ChangePasscodeViewController
        self.navigationController?.pushViewController(changePasscodeViewController!, animated: true)
    }
    
    func moveToEditProfile() {
        let editProfileViewController = MainStoryboard.instantiateViewController(withIdentifier: "SignUpVc_Form") as? SignUpVc_Form
        editProfileViewController?.signUpType = Int(Utilities.getUserDetails().profile_type_id)!
        editProfileViewController?.isEditingProfile = true
        self.navigationController?.pushViewController(editProfileViewController!, animated: true)
    }
    
    func moveToCreateSubmerchantAccount(_ submerchantDetails:Submerchant) {
        let createSubmerchantViewController = UIStoryboard(name: "Events", bundle: nil).instantiateViewController(withIdentifier: "createSubmerchantId") as? CreateSubmerchantViewController
        createSubmerchantViewController!.submerchant = submerchantDetails
        self.navigationController?.pushViewController(createSubmerchantViewController!, animated: true)
    }
    
    func moveToPostsLiked() {
        let editProfileViewController = UIStoryboard(name: "ProfileStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "PostsLikedId") as? PostsLikedViewController
        self.navigationController?.pushViewController(editProfileViewController!, animated: true)
    }
    
    func moveToReportProblem(_ reportTitle:String) {
        self.performSegue(withIdentifier: "feedbackSegue", sender: reportTitle)
    }
    
    func moveToPrivacyPolicy(_ strUrl:String) {
        let privacyPolicyViewController = UIStoryboard(name: "ProfileStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "PrivacyPolicyViewController") as? PrivacyPolicyViewController
        privacyPolicyViewController!.strUrl = strUrl
        self.navigationController?.pushViewController(privacyPolicyViewController!, animated: true)
    }

    //MARK: - Call API
    func checkSubmerchantAccountExists() {
        ApplicationDelegate.showLoader(loaderTitle_PleaseWait)
        var parameters = [String: AnyObject]()
        parameters["user_id"] = Utilities.getUserDetails().id as AnyObject
        APIManager.sharedInstance.checkSubMerchantAccountExists(parameters, Target: self)
    }
    
    //MARK: - API Response
    func responseCheckSubmerchantAccountExists(_ notify: Foundation.Notification) {
        ApplicationDelegate.hideLoader()
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFICATION_CHECKSUBMERCHANTACCOUNTEXISTS), object: nil)
        
        let swiftyJsonVar = JSON(notify.object!)
        
        let message = swiftyJsonVar["message"].stringValue
        let status = swiftyJsonVar["status"].intValue
        
        if swiftyJsonVar [ERROR_KEY].exists()
        {
            self.showCommonAlert(swiftyJsonVar[ERROR_KEY].stringValue)
        }
        else if (status == 1) //success response
        {
            let response = swiftyJsonVar["response"]

            if response.count > 0 {
                printCustom("submerchant account exists")
                let responseSubmerchantDetail:[String:AnyObject] = response.dictionaryObject! as [String : AnyObject]

                let submerchant:Submerchant = Submerchant()
                self.moveToCreateSubmerchantAccount(Parser.parseSubmerchantDetails(responseSubmerchantDetail, submerchant: submerchant))
            }
        }
        else if (status == 0) //error response
        {
            let responseErrorMessage = swiftyJsonVar["response"].stringValue
            if responseErrorMessage == "No record found" {
                printCustom("create submerchant account")
                let submerchant = Submerchant()
                self.moveToCreateSubmerchantAccount(submerchant)
            }
        }
        else
        {
            self.showCommonAlert(message)
        }
    }

    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destinationViewController:UINavigationController = segue.destination as! UINavigationController
        let feedbackViewController:FeedbackViewController = destinationViewController.childViewControllers.first as! FeedbackViewController
        feedbackViewController.strReportTitle = sender as! String
        
    }


}
