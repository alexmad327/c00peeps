//
//  SettingsHeaderTableViewCell.swift
//  C00Peeps
//
//  Created by Arshdeep on 19/10/16.
//  Copyright © 2016 SOTSYS011. All rights reserved.
//

import UIKit

class SettingsHeaderTableViewCell: UITableViewCell {

    @IBOutlet weak var lblSettingsHeader: UILabel!
    @IBOutlet weak var imgVwSettingsHeaderIcon: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
