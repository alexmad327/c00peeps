//
//  PushNotificationSettingsViewController.swift
//  C00Peeps
//
//  Created by Arshdeep on 21/10/16.
//  Copyright © 2016 SOTSYS011. All rights reserved.
//

import UIKit
import SwiftyJSON

class PushNotificationSettingsViewController: BaseVC {
    
    @IBOutlet weak var tblSettings: UITableView!
    
    var arrPushNotifSettings:[PushNotifSetting] = []
    var arrSettings:[String] = []
    var arrKeys:[String] = []
    var  dictResponseKeys : [String:AnyObject] = [:]
    
    let settingLikes = "Likes"
    //let settingDislikes = "Dislikes"
    let settingComments = "Comments"
    let settingNewFollowers = "New Followers"
    let settingAcceptedFollowRequests = "Accepted Follow Requests"
    let settingDigitalMediaRecieved = "Digital Media Received"
    let settingPeepReceived = "Peep Received"
    let settingEventInvite = "Event Invitation"
    let settingEventAccept = "Event Accept"
    let settingMediaShared = "Media Share"
    let settingVersionUpdate = "Version Update"
    
    
    let keyLikes = "likes"
    let keyDislikes = "dislikes"
    let keyComments = "comments"
    let keyNewFollowers = "new_follow"
    let keyAcceptedFollowRequests = "accepted_follow"
    let keyDigitalMediaRecieved = "digital_media_received"
    let keyPeepReceived = "peep_received"
    let keyEventInvite = "event_invitation"
    let keyEventAccept = "event_accepted"
    let keyMediaShared = "media_shared"
    let keyVersionUpdate = "version_update"

    /* Commented by Anubha
     let settingPeepText = "Peep Text"
    let settingFriendsOnPECx = "Friends on PECx"
    let settingDigitalMediaOfYou = "Digital Media of You"
    let settingReminders = "Reminders"
    let settingProductAnnouncements = "Product Announcements"
    let settingSupportRequests = "Support Requests"*/

    override func viewDidLoad() {
        super.viewDidLoad()
        /* Commented by Anubha        
         self.performSelector(#selector(prepareDataSource), withObject: nil)*/
        
        self.callAPI_GetPushNotification()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Hide tab bar for this controller.
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Prepare Datasource
    func prepareDataSource() {
        arrSettings = [settingLikes, settingComments, settingNewFollowers, settingAcceptedFollowRequests,  settingDigitalMediaRecieved ,settingPeepReceived ,settingEventInvite , settingEventAccept , settingMediaShared ,settingVersionUpdate]
        arrKeys = [keyLikes, keyDislikes, keyComments, keyNewFollowers, keyAcceptedFollowRequests,  keyDigitalMediaRecieved ,keyPeepReceived ,keyEventInvite , keyEventAccept , keyMediaShared ,keyVersionUpdate]

        for i in 0 ..< arrSettings.count
        {
            let pushNotifSetting:PushNotifSetting = PushNotifSetting()
            pushNotifSetting.settingName = arrSettings[i]
            pushNotifSetting.keyName = arrKeys[i]
            arrPushNotifSettings.append(pushNotifSetting)
        }
        
        tblSettings.reloadData()
    }
    
    //MARK: - UITableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrPushNotifSettings.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        let cell : SettingViewCell! = tableView.dequeueReusableCell(withIdentifier: "SettingCell",for: indexPath) as! SettingViewCell
        let setting:PushNotifSetting = arrPushNotifSettings[indexPath.row]
        cell.lblFeatureText?.text = setting.settingName
        let switchValue  =  dictResponseKeys[setting.keyName] as! NSString
        cell.swtichNot.setOn(switchValue.boolValue, animated: true)
        cell.swtichNot.tag = indexPath.row

        return cell
    }
    
    //MARK: - Switch Actions
    @IBAction func switchValueChanged(_ sender: AnyObject) {
        let switchNot:UISwitch = sender as! UISwitch
        let setting:PushNotifSetting = arrPushNotifSettings[switchNot.tag]
         if switchNot.isOn {
            dictResponseKeys[setting.keyName] = "1" as AnyObject
        }
        else {
            dictResponseKeys[setting.keyName] = "0" as AnyObject
        }
    }
    
    //MARK: - Button Actions
    @IBAction func btnSaveSettingsClicked(_ sender: AnyObject) {
        
        self.callAPI_SetPushNotification()
    }
    //MARK: - Call API Set Push Notification
    func callAPI_SetPushNotification() {
        
        ApplicationDelegate.showLoader(loaderTitle_Submitting)
        var parameters = [String: AnyObject]()
        parameters = dictResponseKeys
        APIManager.sharedInstance.requestSetPushNotification(parameters,Target:self)
    }
    //MARK: - Set Push Notification Response
    func responseSetPushNotification (_ notify: Foundation.Notification)
    {
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFIACTION_SETPUSHNOTIFICATION), object: nil)
        ApplicationDelegate.hideLoader ()
        let swiftyJsonVar = JSON(notify.object!)
        let message = swiftyJsonVar["message"].stringValue
        let status = swiftyJsonVar["status"].intValue
        
        if (swiftyJsonVar [ERROR_KEY] != nil) {
            self.showCommonAlert(swiftyJsonVar[ERROR_KEY].stringValue)
        }
        else if (status == 1) {
            self.showCommonAlert(alertMsg_SettingsSaved)
        }
        else if (status == 0) {
            let responseErrorMessage = swiftyJsonVar["response"].stringValue
            self.showCommonAlert(responseErrorMessage)
        }
        else {
            self.showCommonAlert(message)
        }
    }
    //MARK: - Call API Get Push Notification
    func callAPI_GetPushNotification() {
        
        ApplicationDelegate.showLoader(loaderTitle_Fetching)
        var parameters = [String: AnyObject]()
        parameters["user_id"] = Utilities.getUserDetails().id as AnyObject
        APIManager.sharedInstance.requestGetPushNotification(parameters,Target:self)
    }
    //MARK: - Get Push Notification Response

    func responseGetPushNotification (_ notify: Foundation.Notification)
    {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFIACTION_GETPUSHNOTIFICATION), object: nil)
        ApplicationDelegate.hideLoader ()
        let swiftyJsonVar = JSON(notify.object!)
        let message = swiftyJsonVar["message"].stringValue
        let status = swiftyJsonVar["status"].intValue
        
        if (swiftyJsonVar [ERROR_KEY] != nil) {
            self.showCommonAlert(swiftyJsonVar[ERROR_KEY].stringValue)
        }
        else if (status == 1) {
            
            dictResponseKeys = swiftyJsonVar["response"].dictionaryObject! as [String : AnyObject]
            self.perform(#selector(prepareDataSource), with: nil)
        }
        else if (status == 0) {
            let responseErrorMessage = swiftyJsonVar["response"].stringValue
            self.showCommonAlert(responseErrorMessage)
        }
        else {
            self.showCommonAlert(message)
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
