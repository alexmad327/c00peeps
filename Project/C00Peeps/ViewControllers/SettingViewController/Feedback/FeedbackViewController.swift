//
//  FeedbackViewController.swift
//  C00Peeps
//
//  Created by Arshdeep on 21/11/16.
//  Copyright © 2016 SOTSYS011. All rights reserved.
//

import UIKit
import SwiftyJSON

class FeedbackViewController: BaseVC {
    @IBOutlet weak var lblReportTitle: UILabel!
    @IBOutlet weak var lblReportDetail: UILabel!
    @IBOutlet weak var txtVwReportProblem: UITextView!
    @IBOutlet weak var btnSend: UIBarButtonItem!
    
    var strReportTitle:String = ""

    //MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblReportTitle.text = strReportTitle.uppercased()
        if strReportTitle == alertBtnTitle_PECxNotWorking {
            lblReportDetail.text = "Briefly explain what happened."
        }
        else {
            lblReportDetail.text = "Briefly explain what you love, or what could improve."
        }
        
        txtVwReportProblem.textContainerInset = UIEdgeInsetsMake(4, 4, 0, 0)
        //btnSend.enabled = false

       // NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(textDidChange(_:)), name: UITextViewTextDidChangeNotification, object: txtVwReportProblem)

        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
   /* // MARK: - Textview Notification Method
    func textDidChange(textView:UITextView) {
        let text:NSString = txtVwReportProblem.text!
        if text.length == 0 {
            btnSend.enabled = false
        }
        else {
            btnSend.enabled = true
        }
    }*/

    //MARK: - Button Actions
    @IBAction func btnCancelClicked(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnSendClicked(_ sender: AnyObject) {
        let text:NSString = txtVwReportProblem.text! as NSString
        if text.length == 0 {
            self.showCommonAlert(alertMsg_EmptyFeedback)
        }
        else {
            self.sendFeedback()
        }
    }

    //MARK: - Get Likes Data
    func sendFeedback() {
        ApplicationDelegate.showLoader(loaderTitle_SendingFeedback)
        
        var parameters = [String: AnyObject]()
        parameters["user_id"] = Utilities.getUserDetails().id as AnyObject
        parameters["feedback_text"] = txtVwReportProblem.text as AnyObject
        if strReportTitle == alertBtnTitle_PECxNotWorking {
            parameters["type"] = 1 as AnyObject
        }
        else {
            parameters["type"] = 2 as AnyObject
        }
        APIManager.sharedInstance.setFeedback(parameters, Target: self)
    }
    
    //MARK:- Get Likes Response
    func responseSendFeedback (_ notify: Foundation.Notification) {
        ApplicationDelegate.hideLoader()
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFICATION_SETFEEDBACK), object: nil)
        
        let swiftyJsonVar = JSON(notify.object!)
        
        let message = swiftyJsonVar["message"].stringValue
        let status = swiftyJsonVar["status"].intValue
        
        if swiftyJsonVar [ERROR_KEY].exists() {
            self.showCommonAlert(swiftyJsonVar[ERROR_KEY].stringValue)
        }
        else if (status == 1) { //success response
            let responseSuccessMsg = swiftyJsonVar["response"].stringValue
            let alert:UIAlertController = UIAlertController(title: "", message: responseSuccessMsg, preferredStyle: .alert)
            let alertAction = UIAlertAction(title: alertBtnTitle_OK, style: .default, handler: { (UIAlertAction) -> Void in
                self.dismiss(animated: true, completion: nil)
            })
            alert.addAction(alertAction)
            self.present(alert, animated: true, completion: nil)
        }
        else if (status == 0) {//error response
            let responseErrorMessage = swiftyJsonVar["response"].stringValue
            self.showCommonAlert(responseErrorMessage)
        }
        else {
            self.showCommonAlert(message)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
