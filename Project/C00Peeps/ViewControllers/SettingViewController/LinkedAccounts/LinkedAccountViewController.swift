//
//  LinkedAccountViewController.swift
//  C00Peeps
//
//  Created by Arshdeep on 21/10/16.
//  Copyright © 2016 SOTSYS011. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import TwitterKit
import InstagramKit

class LinkedAccountViewController: BaseVC {
    @IBOutlet weak var tblVwLinkedAccount: UITableView!
    var facebookUsername:String = ""
    var twitterUsername:String = ""
    var instagramUsername:String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.

        // Fetch and show social user name if account is already linked.
        self.fetchFacebookUsername()
        self.fetchTwitterUsername()
        self.fetchInstagramUsername()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Show tab bar for this controller.
        self.tabBarController?.tabBar.isHidden = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Fetch Social User name
    func fetchFacebookUsername() {
        if FBSDKAccessToken.current() != nil {
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, email"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    let resultDict = result as! NSDictionary
                    printCustom("profile:\(resultDict)")
                    self.facebookUsername = resultDict["name"] as! String
                    self.tblVwLinkedAccount.reloadData()
                }
            })
        }
    }
    
    func fetchTwitterUsername() {
        let store = TWTRTwitter.sharedInstance().sessionStore
        if store.session() != nil {
            if let userID:String = store.session()!.userID {
                let client = TWTRAPIClient(userID: userID)
                client.loadUser(withID: userID) { (user, error) -> Void in
                    printCustom("twitter user profile: \(user)");
                    self.twitterUsername = user!.screenName
                    self.tblVwLinkedAccount.reloadData()
                }
            }
        }
    }
    
    func fetchInstagramUsername() {
        if InstagramEngine.shared().isSessionValid() {
            InstagramEngine.shared().getSelfUserDetails(success: { (user) in
                printCustom("instagram user username:\(user.username)")
                self.instagramUsername = user.username
                self.tblVwLinkedAccount.reloadData()
                }, failure: nil)
        }

    }
    
    //MARK: - UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: "LinkedAccountCell",for: indexPath)
        let lblAccountType:UILabel = cell.viewWithTag(1) as! UILabel
        let lblAccountName:UILabel = cell.viewWithTag(2) as! UILabel
        let imgVwAccount:UIImageView = cell.viewWithTag(3) as! UIImageView
        
        switch indexPath.row {
        case 0://Facebook
            lblAccountType.text = "Facebook"
            lblAccountName.text = facebookUsername
            imgVwAccount.image = Utilities().themedImage(img_facebookSelected)
            
        case 1://Twitter
            lblAccountType.text = "Twitter"
            lblAccountName.text = twitterUsername
            imgVwAccount.image = Utilities().themedImage(img_twitterSelected)
            
        case 2://Instagram
            lblAccountType.text = "Instagram"
            lblAccountName.text = instagramUsername
            imgVwAccount.image = Utilities().themedImage(img_instagramSelected)
            
        default:
            printCustom("Other rows")
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAtIndexPath indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if indexPath.row == 0 {
            if self.facebookUsername == "" {//Link account
                self.loginSocialAccount(AccountType.facebook)
            }
            else {//Go to Share Settings to Unlink account
                self.moveToShareSettings(AccountType.facebook)
            }
        }
        else if indexPath.row == 1 {
            if self.twitterUsername == "" {//Link account
                self.loginSocialAccount(AccountType.twitter)
            }
            else {//Go to Share Settings to Unlink account
                self.moveToShareSettings(AccountType.twitter)
            }
        }
        else {
            if self.instagramUsername == "" {//Link account
                self.loginSocialAccount(AccountType.instagram)
            }
            else {//Go to Share Settings to Unlink account
                self.moveToShareSettings(AccountType.instagram)
            }
        }
    }

    //MARK: - Move To Screen
    func moveToShareSettings(_ accountType:AccountType) {
        let shareSettingsLinkedAccountViewController = UIStoryboard(name: "ProfileStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "shareSettingsId") as? ShareSettingsLinkedAccountViewController
        shareSettingsLinkedAccountViewController!.accountType = accountType
        switch accountType {
        case AccountType.facebook:
            shareSettingsLinkedAccountViewController!.socialUsername = facebookUsername

        case AccountType.twitter:
            shareSettingsLinkedAccountViewController!.socialUsername = twitterUsername
            
        case AccountType.instagram:
            shareSettingsLinkedAccountViewController!.socialUsername = instagramUsername
            
        default:
            printCustom("Normal")
        }
        shareSettingsLinkedAccountViewController!.linkedAccountVcDelegate = self
        self.navigationController?.pushViewController(shareSettingsLinkedAccountViewController!, animated: true)
    }
    
    
    func loginSocialAccount(_ accountType:AccountType) {
        if Utilities().linkSocialAccount(true, accountType: accountType) == true {
            switch accountType {
            case AccountType.facebook:
                
                let facebookLogin = FBSDKLoginManager()
                facebookLogin.logIn(withReadPermissions: ["email"],  from: self) { (facebookResult: FBSDKLoginManagerLoginResult!, facebookError: Error!) -> Void in
                    if !facebookResult.isCancelled {
                        FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, email"]).start(completionHandler: { (connection, result, error) -> Void in
                            ApplicationDelegate.hideLoader()
                            if (error == nil){
                                let resultDict = result as! NSDictionary
                                printCustom("profile:\(resultDict)")
                               self.facebookUsername = resultDict["name"] as! String
                                self.tblVwLinkedAccount.reloadData()
                            }
                        })
                        
                    }
                }
                
            case AccountType.twitter:
                
                TWTRTwitter.sharedInstance().logIn { session, error in
                    ApplicationDelegate.hideLoader()
                    
                    if error != nil {
                       // printCustom("error:\(error?.code)")
                        //printCustom("description:\(error?.description)")
                        if error?.code != 1 {
                            self.showCommonAlert(alertMsg_ErrorConnectingTwitter)
                        }
                        
                        return
                    }
                    if (session != nil) {
                        printCustom("signed in as \(session!.userName)");
                        self.twitterUsername = session!.userName
                        self.tblVwLinkedAccount.reloadData()
                    }
                }
                
                
            case AccountType.instagram:
                let navController:UINavigationController = MainStoryboard.instantiateViewController(withIdentifier: "InstagramLoginID") as! UINavigationController
                let instagramLoginVC:InstagramLoginVC = navController.topViewController as! InstagramLoginVC
                instagramLoginVC.linkedAccountDelegate = self
                self.present(navController, animated: true, completion: nil)
                
            default:
                printCustom("Normal")
            }
        }
    }

    func accountUnlinked(_ accountType:AccountType) {
        switch accountType {
        case AccountType.facebook:
            facebookUsername = ""
        case AccountType.twitter:
            twitterUsername = ""
        case AccountType.instagram:
            instagramUsername = ""
        default:
            printCustom("Normal")
        }
        
        tblVwLinkedAccount.reloadData()
    }
    
    func instagramAccountLinkedWithUsername(_ username:String) {
        instagramUsername = username
        
        // Reload table to show linked account name.
        self.tblVwLinkedAccount.reloadData()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
