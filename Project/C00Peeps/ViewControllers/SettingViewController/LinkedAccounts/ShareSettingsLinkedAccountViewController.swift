//
//  ShareSettingsLinkedAccountViewController.swift
//  C00Peeps
//
//  Created by Arshdeep on 21/10/16.
//  Copyright © 2016 SOTSYS011. All rights reserved.
//

import UIKit

class ShareSettingsLinkedAccountViewController: BaseVC {
    var accountType:AccountType?
    var socialUsername:String = ""
    var linkedAccountVcDelegate:LinkedAccountViewController?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        switch accountType! {
        case AccountType.facebook:
            self.title = "Facebook"
            
        case AccountType.twitter:
            self.title = "Twitter"
            
        case AccountType.instagram:
            self.title = "Instagram"
            
        default:
            printCustom("Normal")
        }
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Show tab bar for this controller.
        self.tabBarController?.tabBar.isHidden = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
       
    //MARK: - UITableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        let cell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: "ShareSettingsCell",for: indexPath)
       
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        return "Connected as " + socialUsername
    }

    func tableView(_ tableView: UITableView, didSelectRowAtIndexPath indexPath: IndexPath) {
        if Utilities().linkSocialAccount(false, accountType: accountType!) == false {
            self.linkedAccountVcDelegate!.accountUnlinked(accountType!)
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
