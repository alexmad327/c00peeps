//
//  ChangePasscodeViewController.swift
//  C00Peeps
//
//  Created by SOTSYS027 on 3/23/16.
//  Copyright © 2016 SOTSYS011. All rights reserved.
//

import UIKit
import SwiftyJSON

class ChangePasscodeViewController: BaseVC {

    @IBOutlet weak var txtCurrentPass: UITextField!
    @IBOutlet weak var txtNewPass: UITextField!
    @IBOutlet weak var imgVwBg: UIImageView!
    @IBOutlet weak var imgVwLogo: UIImageView!
    @IBOutlet weak var btnSubmit: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
      
        self.perform(#selector (ChangePasscodeViewController.designTextField), with: nil, afterDelay: 0.1)
        self.updateViewAccordingToTheme()
        
        // Change Placeholder Color
        self.changeTextfieldPlaceholderColor(txtCurrentPass, placeholderText: txtCurrentPass.placeholder!, color:UIColor.white)
        self.changeTextfieldPlaceholderColor(txtNewPass, placeholderText: txtNewPass.placeholder!, color:UIColor.white)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Hide tab bar for this controller.
        self.tabBarController?.tabBar.isHidden = true
    }
    
    // MARK: - Update View According To Selected Theme
    func updateViewAccordingToTheme() {
        imgVwBg.image = Utilities().themedImage(img_bg)
        imgVwLogo.image = Utilities().themedImage(img_signInLogo)
        
        btnSubmit.setBackgroundImage(Utilities().themedImage(img_btnSelected), for: .highlighted)
        btnSubmit.setBackgroundImage(Utilities().themedImage(img_btnSelected), for: .selected)
    }

    // MARK: - Text field Border line
    func designTextField() {
        for scrollView in self.view.subviews {
            if scrollView .isKind(of: UIScrollView.self) {
                for textfield in scrollView.subviews {
                    // Set border line for all textfields in screen.
                    if textfield .isKind(of: UITextField.self) {
                        setSubViewBorder(textfield, color: UIColor.lightGray)
                    }
                }
            }
        }
    }

    // MARK: - Textfield Delegates
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    //MARK: - IBAction
    
    @IBAction func btnSubmitClicked(_ sender: AnyObject) {
        let userDetail:UserDetail = Utilities.getUserDetails()

        if self.txtNewPass.text == "" || self.txtCurrentPass.text == "" {
            self.showCommonAlert(alertMsg_EmptyPasscode)
        }
        else
        {
            let passwordRule = Utilities().checkPasswordRule(txtNewPass.text!)
            
            if passwordRule.characters.count > 0{
                txtNewPass.text = ""//Empty password textfield if this field is not validated
                self.showCommonAlert(passwordRule)
            }
            else{
                
                self.callAPI_ChangePassword()
            }
        }
      
        print("\(userDetail.password), \(txtCurrentPass.text)")
    }
    //MARK: - Call API Change Password
    func callAPI_ChangePassword() {
        
        ApplicationDelegate.showLoader(loaderTitle_Submitting)
        var parameters = [String: AnyObject]()
        parameters["user_id"] = Utilities.getUserDetails().id as AnyObject
        parameters["password"] = txtNewPass.text as AnyObject
        parameters["oldpassword"] = txtCurrentPass.text as AnyObject
        APIManager.sharedInstance.requestChangePassword(parameters,Target:self)
    }
    
    //MARK: - Change Password Response
    func responseChangePassword (_ notify: Foundation.Notification)
    {
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFIACTION_CHANGEPASSWORD), object: nil)
        ApplicationDelegate.hideLoader ()
        let swiftyJsonVar = JSON(notify.object!)
        let message = swiftyJsonVar["message"].stringValue
        let status = swiftyJsonVar["status"].intValue
        
        if (swiftyJsonVar [ERROR_KEY] != nil) {
            self.showCommonAlert(swiftyJsonVar[ERROR_KEY].stringValue)
        }
        else if (status == 1) {
            let alert:UIAlertController = UIAlertController(title: "", message: alertMsg_PasswordChanged, preferredStyle: .alert)
            let alertAction = UIAlertAction(title: "OK", style: .default, handler: { (UIAlertAction) -> Void in
                self.navigationController?.popViewController(animated: true)
            })

            alert.addAction(alertAction)
            self.present(alert, animated: true, completion: nil)
            
            //self.navigationController?.pushViewController(homeScreen!, animated: true)
        }
        else if (status == 0) {
            let responseErrorMessage = swiftyJsonVar["response"].stringValue
            self.showCommonAlert(responseErrorMessage)
        }
        else {
            self.showCommonAlert(message)
        }
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
