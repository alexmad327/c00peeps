//
//  PostsLikedViewController.swift
//  C00Peeps
//
//  Created by Arshdeep on 21/10/16.
//  Copyright © 2016 SOTSYS011. All rights reserved.
//

import UIKit
import SwiftyJSON

class PostsLikedViewController: BaseVC, UniversalDelegate {

    //MARK:- Outlets
    @IBOutlet weak var btnGrid: UIButton!
    @IBOutlet weak var btnList: UIButton!
    @IBOutlet weak var collectionVwLikes: UICollectionView!
    @IBOutlet weak var lblNoPosts: UILabel!
    
    //MARK:- Variables
    var delegate:UniversalVc?
    var offset = 0
    var totalRecords = 0
    var pageNumber = 1
    var arrLikedPosts = [Media]()
    var universalVc: UniversalVc? = nil
    var isGridViewOpen = true
    var imageCache = [String:UIImage]()
    var refreshControl: UIRefreshControl!
    var myposts = 0 //is true then myposts will be shown
    var userId = 0
    
    //MARK:- View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnGrid.setImage(Utilities().themedImage(img_gridViewSelected), for: UIControlState())
        btnList.setImage(Utilities().themedImage(img_listView), for: UIControlState())
        lblNoPosts.isHidden = true
        
        collectionVwLikes.alwaysBounceVertical = true
        
        addRefreshControl()
        
        self.collectionVwLikes.register(UINib(nibName: "UniversalCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "universal_cell")
        let layout = self.collectionVwLikes.collectionViewLayout as! UICollectionViewFlowLayout
        layout.minimumLineSpacing = 2.0
        layout.minimumInteritemSpacing = 2.0
        layout.sectionInset = UIEdgeInsets(top: 0, left: 2, bottom: 0, right: 2)
        
        //Fetch user posts data
        if myposts == 1
        {
            self.getPostsData()
        }
        else
        {
            self.getLikesData()
        }

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Show tab bar for this controller.
        self.tabBarController?.tabBar.isHidden = false
    }

    func addRefreshControl(){
        
        refreshControl = UIRefreshControl()
        refreshControl.tintColor = UIColor.darkGray
        refreshControl.attributedTitle = NSAttributedString(string: loaderTitle_PullToRefresh)
        refreshControl.addTarget(self, action: #selector(PostsLikedViewController.handleRefresh(_:)), for: UIControlEvents.valueChanged)
        self.collectionVwLikes.addSubview(refreshControl)
        
    }
    
    func handleRefresh(_ refreshControl: UIRefreshControl) {
        
        offset = 0
        pageNumber = 1
        reloadMediaGallery()
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK:- Button Actions
    @IBAction func btnGridClicked(_ sender: AnyObject) {
        isGridViewOpen = true
        
        btnGrid.setImage(Utilities().themedImage(img_gridViewSelected), for: UIControlState())
        btnList.setImage(Utilities().themedImage(img_listView), for: UIControlState())
        
        self.hideList()
        
        if arrLikedPosts.count == 0 {
            
            if myposts == 1
            {
                self.getPostsData()
            }
            else
            {
                self.getLikesData()
            }
        }
        else {
            self.showGrid()
        }
    }
    
    @IBAction func btnListClicked(_ sender: AnyObject) {
        isGridViewOpen = false
       
        btnGrid.setImage(Utilities().themedImage(img_gridView), for: UIControlState())
        btnList.setImage(Utilities().themedImage(img_listViewSelected), for: UIControlState())

       
        self.hideGrid()
        
        if arrLikedPosts.count == 0 {
            
            if myposts == 1
            {
                self.getPostsData()
            }
            else
            {
                self.getLikesData()
            }
        }
        else {
            self.showList()
        }
    }
    
    //MARK:- Show/Hide Grid
    func showGrid() {
        
        self.collectionVwLikes.reloadData()
        self.collectionVwLikes.isHidden = false
    }
    
    func hideGrid() {
        self.collectionVwLikes.isHidden = true
    }
    
    
    //MARK:- Show/Hide List
    func showList(){
        
        if universalVc == nil {
            universalVc = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "UniversalVc") as? UniversalVc
            
            universalVc!.arrMedia = arrLikedPosts
            universalVc!.totalRecordCount = totalRecords
            universalVc!.universalDeleg = self
            universalVc!.offset = offset
            universalVc!.page = pageNumber
            
            let fr =  CGRect(x: 0, y: self.collectionVwLikes.frame.origin.y, width: universalVc!.view.frame.size.width, height: self.view.frame.height-self.collectionVwLikes.frame.origin.y)
            
            universalVc!.view.frame = fr;
            universalVc!.willMove(toParentViewController: self)
            self.view.addSubview(universalVc!.view)
            self.addChildViewController(universalVc!)
            universalVc!.didMove(toParentViewController: self)
        }
    }
    
    func hideList() {
        if universalVc != nil {
            // Remove List View from Likes Screen.
            universalVc!.view.removeFromSuperview()
            universalVc!.removeFromParentViewController()
            universalVc = nil
        }
    }
    
    //MARK: - Get My Posts
    func getPostsData() {
        
        self.navigationItem.title = "Posts"
        
        APIManager.sharedInstance.cancelAllRequests()
        
        var parameters = [String: AnyObject]()
        parameters["logged_user_id"] = Utilities.getUserDetails().id as AnyObject
        parameters["user_id"] = userId as AnyObject
        parameters["limit"] = SearchLimit as AnyObject
        parameters["offset"] = offset as AnyObject
        
        print(parameters)
        
        APIManager.sharedInstance.requestGetUserPosts(parameters, Target: self)
    }
    
    //MARK:- Get Posts Response
    func responseGetUserPosts (_ notify: Foundation.Notification) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFIACTION_GETUSERPOSTS), object: nil)
        
        refreshControl.endRefreshing()
        
        let swiftyJsonVar = JSON(notify.object!)
        
        let message = swiftyJsonVar["message"].stringValue
        let status = swiftyJsonVar["status"].intValue
        
        if swiftyJsonVar [ERROR_KEY].exists() {
            self.showCommonAlert(swiftyJsonVar[ERROR_KEY].stringValue)
        }
        else if (status == 1) { //success response
            if offset == 0 {
                arrLikedPosts.removeAll()
            }
            
            totalRecords = swiftyJsonVar["response"]["count"].intValue
            
            if (totalRecords > 0)
            {
                var arrFeeds = [Media]()
                arrFeeds = Parser.getParsedMediaArrayFromData(swiftyJsonVar["response"]["list"])
                
                arrLikedPosts.append(contentsOf: arrFeeds)
                
                if arrLikedPosts.count > 0 {
                    if isGridViewOpen {
                        showGrid()
                    }
                    else {
                        showList()
                    }
                }
            }
            else
            {
                lblNoPosts.isHidden = false
            }
        }
        else if (status == 0) {//error response
            let responseErrorMessage = swiftyJsonVar["response"].stringValue
            self.showCommonAlert(responseErrorMessage)
        }
        else {
            self.showCommonAlert(message)
        }
    }

    
    //MARK: - Get Likes Data
    func getLikesData() {
        
        self.navigationItem.title = "Posts Liked"
        
        APIManager.sharedInstance.cancelAllRequests()

        var parameters = [String: AnyObject]()
        parameters["user_id"] = Utilities.getUserDetails().id as AnyObject
        parameters["limit"] = SearchLimit as AnyObject
        parameters["offset"] = offset as AnyObject
        
        APIManager.sharedInstance.requestPostsLiked(parameters, Target: self)
    }
    
    //MARK:- Get Likes Response
    func responseGetPostsLiked (_ notify: Foundation.Notification) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFICATION_GETPOSTSLIKED), object: nil)
        
        refreshControl.endRefreshing()
        
        let swiftyJsonVar = JSON(notify.object!)
        
        let message = swiftyJsonVar["message"].stringValue
        let status = swiftyJsonVar["status"].intValue
        
        if swiftyJsonVar [ERROR_KEY].exists() {
            self.showCommonAlert(swiftyJsonVar[ERROR_KEY].stringValue)
        }
        else if (status == 1) { //success response
            if offset == 0 {
                arrLikedPosts.removeAll()
            }
            
            totalRecords = swiftyJsonVar["response"]["count"].intValue
            
            if (totalRecords > 0)
            {
                var arrFeeds = [Media]()
                arrFeeds = Parser.getParsedMediaArrayFromData(swiftyJsonVar["response"]["media"])
                
                arrLikedPosts.append(contentsOf: arrFeeds)
                
                if arrLikedPosts.count > 0 {
                    if isGridViewOpen {
                        showGrid()
                    }
                    else {
                        showList()
                    }
                }
            }
            else
            {
                lblNoPosts.isHidden = false
            }
        }
        else if (status == 0) {//error response
            let responseErrorMessage = swiftyJsonVar["response"].stringValue
            self.showCommonAlert(responseErrorMessage)
        }
        else {
            self.showCommonAlert(message)
        }
    }

    // MARK:- UpdateContent delegate - Universal collection view
    
    func reloadMediaGallery() {
        
        if myposts == 1
        {
            self.getPostsData()
        }
        else
        {
            self.getLikesData()
        }
    }
    
    func loadMoreMediaGallery(_ page:NSInteger, offset:Int){
        self.offset = offset
        self.pageNumber = page
      
        if myposts == 1
        {
            self.getPostsData()
        }
        else
        {
            self.getLikesData()
        }
    }
    
    func moveToNextScreenGallery(_ WithData:AnyObject) {
        let st = UIStoryboard(name: "Home", bundle:nil)
        let singleDetailVc = st.instantiateViewController(withIdentifier: "SingleDetailVc") as! SingleDetailVc
        
        var arr = [Media]()
        arr.append(WithData as! Media)
        singleDetailVc.arrMedia = arr
        self.navigationController?.pushViewController(singleDetailVc, animated: true)
    }
    
    //MARK: - Universal View Delegate
    
    //MARK:- MoveToNext Screen
    func moveToNextScreen(_ ScreenName:String, WithData:AnyObject)
    {
        
        if(ScreenName == "SingleDetailVc")
        {
            let st = UIStoryboard(name: "Home", bundle:nil)
            let singleDetailVc = st.instantiateViewController(withIdentifier: "SingleDetailVc") as! SingleDetailVc
            
            var arr = [Media]()
            arr.append(WithData as! Media)
            singleDetailVc.arrMedia = arr
            self.navigationController?.pushViewController(singleDetailVc, animated: true)
        }
        else if(ScreenName == "CommentVc")
        {
            let st = UIStoryboard(name: "Home", bundle:nil)
            let commentObject = st.instantiateViewController(withIdentifier: "CommentVc") as! CommentVc
            
            commentObject.md = WithData as! Media
            self.navigationController?.pushViewController(commentObject, animated: true)
        }
        else if (ScreenName == "ForwardMedia")
        {
            let st = UIStoryboard(name: "Digital", bundle:nil)
            let obj = st.instantiateViewController(withIdentifier: "ContactsScreen") as! ContactsScreen
            let md = WithData as! Media
            obj.fwdMediaId = md.pId
            obj.isComingFromForwardMedia = true
            self.navigationController?.pushViewController(obj, animated: true)
        }
    }
    
    //MARK: Reload Media (or pull to refresh)
    func reloadMedia(_ obj:UniversalVc)
    {
        offset = 0
        pageNumber = 1
        delegate = obj
        
        if myposts == 1
        {
            self.getPostsData()
        }
        else
        {
            self.getLikesData()
        }
    }
    
    
    //MARK: Load More Media
    func loadMoreMedia(_ page:Int, offset offset1:Int, obj del:UniversalVc)
    {
        offset = offset1
        pageNumber = page
        delegate = del
        
        if myposts == 1
        {
            self.getPostsData()
        }
        else
        {
            self.getLikesData()
        }
    }
    
    //MARK:- Collection View Datasource
    func numberOfSectionsInCollectionView(_ collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrLikedPosts.count
    }
    
     func collectionView(_ collectionView: UICollectionView, cellForItemAtIndexPath indexPath: IndexPath) -> UICollectionViewCell {
        
        //Load More
        if (arrLikedPosts.count >= SearchLimit) && (indexPath.row + 1  == arrLikedPosts.count) {
            
            if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "LoadMoreCell", for: indexPath) as? UICollectionViewCell
            {
                if let lbl5176 =  cell.viewWithTag(5176) as? UILabel
                {
                    lbl5176.text = loading_Title
                    
                    if (totalRecords) > (pageNumber) * SearchLimit {
                        
                        offset = (pageNumber) * SearchLimit
                        pageNumber += 1
                        
                        loadMoreMediaGallery(pageNumber, offset:offset)
                    }
                    else
                    {
                        lbl5176.isHidden = true
                        
                        lbl5176.text = loadingNoMoreData
                        
                         return refreshCellForRow(indexPath)
                    }
                }
                
                return cell
            }
        }
        else
        {
            return refreshCellForRow(indexPath)
        }
    }
    
    // MARK:- UICollectionViewDelegate protocol
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                               sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
       
        let width: CGFloat = (UIScreen.main.bounds.size.width - 8)/3
        return CGSize(width: width, height: width)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: IndexPath)
    {
        let obj = arrLikedPosts[indexPath.row]
        self.moveToNextScreenGallery(obj)
    }
    
    func refreshCellForRow(_ indexPath:IndexPath)->UniversalCollectionViewCell
    {
        let cell = self.collectionVwLikes.dequeueReusableCell(withReuseIdentifier: "universal_cell", for: indexPath) as? UniversalCollectionViewCell
        
        cell?.tag = indexPath.row
        
        let media = arrLikedPosts[(indexPath as IndexPath).row]
        cell!.backgroundColor = UIColor.clear
        cell!.buttonSource.tag = indexPath.row
        
        //play button enable when a photo has a video attachment
        if media.pMediaAttachmentType == 3
        {
            cell!.imageType.isHidden = false
            cell!.imageType.image = Utilities().themedImage(img_videoIcon)
        }
            //play button enable when a photo has an audio attachment
        else if media.pMediaAttachmentType == 2
        {
            cell!.imageType.isHidden = false
            cell!.imageType.image = Utilities().themedImage(img_audioIcon)
        }
        else
        {
            cell!.imageType.isHidden = true
        }

        //Loading Media image
        cell!.buttonSource.image = Utilities().themedImage(img_movieThumbnail)
        cell!.buttonSource!.contentMode = .scaleAspectFit
        
        if media.pMediaFile.contains(".png")
        {
            if let img = imageCache[media.pMediaFile] {
                cell!.buttonSource!.image = img
            }
            else if imageCache[media.pMediaFile] == nil
            {
                //Downloading media image
                URLSession.shared.dataTask(with: URL(string: media.pMediaFile)!, completionHandler: { (data, response, error) -> Void in
                    
                    if error != nil {
                        
                        if cell != nil
                        {
                            cell!.buttonSource.image = Utilities().themedImage(img_movieThumbnail)
                        }
                        
                        print(error)
                        return
                    }
                    
                    DispatchQueue.main.async(execute: { () -> Void in
                        let image = UIImage(data: data!)
                        
                        if cell != nil
                        {
                            if image == nil
                            {
                                cell!.buttonSource.image = Utilities().themedImage(img_movieThumbnail)
                            }
                            else
                            {
                                cell!.buttonSource!.image = image
                                self.imageCache[media.pMediaFile] = image
                            }
                        }
                    })
                }).resume()
            }
        }
        
        return cell!
    }
}
