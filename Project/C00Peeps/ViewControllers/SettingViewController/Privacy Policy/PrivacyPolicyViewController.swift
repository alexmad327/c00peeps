//
//  PrivacyPolicyViewController.swift
//  C00Peeps
//
//  Created by SOTSYS027 on 3/23/16.
//  Copyright © 2016 SOTSYS011. All rights reserved.
//

import UIKit

class PrivacyPolicyViewController: UIViewController {

    @IBOutlet weak var webView: UIWebView!
    var strUrl = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if strUrl == TERMS_URL {
            self.title = "Terms & Conditions"
            strUrl = TERMS_URL
        }
        else{
            self.title = "Privacy Policy"
            strUrl = PRIVACY_POLICY_URL
        }
        
        let authURL: URL = URL(string: strUrl)!
        let authURLRequest: URLRequest = URLRequest(url:authURL)
        self.webView.loadRequest(authURLRequest)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
