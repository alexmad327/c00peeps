//
//  AboutUSViewController.swift
//  C00Peeps
//
//  Created by SOTSYS027 on 3/23/16.
//  Copyright © 2016 SOTSYS011. All rights reserved.
//

import UIKit

class AboutUSViewController: UIViewController
{

    @IBOutlet weak var lblDescription: UILabel!
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        let str: String = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
        
        lblDescription.text = str
    }

    @IBAction func btnBckclick(_ sender: UIButton)
    {
        let controller : SettingMainViewController = UIStoryboard(name: "ProfileStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "SettingMainViewController") as! SettingMainViewController
        
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
