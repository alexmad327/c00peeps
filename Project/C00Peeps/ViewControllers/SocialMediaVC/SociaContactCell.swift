//
//  SociaContactCell.swift
//  C00Peeps
//
//  Created by SOTSYS027 on 3/18/16.
//  Copyright © 2016 SOTSYS011. All rights reserved.
//

import UIKit

class SociaContactCell: UITableViewCell {

    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var btnSelect: UIButton!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var widthConstraintBtnSelect: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
