//
//  SocialMediaVc.swift
//  C00Peeps
//
//  Created by SOTSYS011 on 3/18/16.
//  Copyright © 2016 SOTSYS011. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import FBSDKShareKit
import TwitterKit

class SocialMediaVc: BaseVC, FBSDKAppInviteDialogDelegate
{
    
    @IBOutlet weak var imgVwBg: UIImageView!
    @IBOutlet weak var imgVwLogo: UIImageView!
    @IBOutlet weak var btnFacebook: UIButton!
    @IBOutlet weak var btnTwitter: UIButton!
    
    var syncType:SyncType?
    var isComingFromSettings:Bool = false
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.title = "Social Media"
      
        if self.isComingFromSettings {
            self.navigationItem.rightBarButtonItem = nil
        }
        
        // Update View According To Selected Theme
        self.updateViewAccordingToTheme()
    }
    
    // MARK: - Update View According To Selected Theme
    func updateViewAccordingToTheme() {
        
        imgVwBg.image = Utilities().themedImage(img_bg)
        imgVwLogo.image = Utilities().themedImage(img_logo)
        btnFacebook.setImage(Utilities().themedImage(img_facebookContact), for: UIControlState())
        btnFacebook.setImage(Utilities().themedImage(img_facebookContactSelected), for: .highlighted)
        btnTwitter.setImage(Utilities().themedImage(img_twitterContact), for: UIControlState())
        btnTwitter.setImage(Utilities().themedImage(img_twitterContactSelected), for: .highlighted)
    }
    
    // MARK: - Button Actions
    @IBAction func btnSkipClicked(_ sender: AnyObject) {
        checkAccountActivatedFromAddContacts = true
        self.callAPI_SignIn(Utilities.getUserDetails().username, password:Utilities.getUserDetails().password)
 
//        self.navigationController?.navigationBarHidden = true
//        
//        let homeScreen = self.storyboard?.instantiateViewControllerWithIdentifier("tabBarcontroller") as? UITabBarController
//        self.navigationController?.pushViewController(homeScreen!, animated: true)
    }
    
    // MARK: - Button Actions
    @IBAction func btnSocialClick(_ sender: UIButton) {
        if sender.tag == 0 {
            let facebookURL: URL = URL(string: "fb://")!
            if UIApplication.shared.canOpenURL(facebookURL) {
                self.sendAppInvitesToFB()
            }
            else {
                let alert:UIAlertController = UIAlertController(title: "", message: alertMsg_DownloadFacebookToSendAppInvites, preferredStyle: .alert)
                let alertAction = UIAlertAction(title: alertBtnTitle_OK, style: .default, handler: { (UIAlertAction) -> Void in
                    self.openFacebookOnAppStore()
                })

                alert.addAction(alertAction)
                self.present(alert, animated: true, completion: nil)
            }
        }
        else if sender.tag == 1 {
            /*ApplicationDelegate.showLoader(loaderTitle_ConnectingTwitter)
            
            let store = Twitter.sharedInstance().sessionStore
            if store.session() != nil {
                if let userID:String = store.session()!.userID {
                    store.logOutUserID(userID)
                    ApplicationDelegate.hideLoader()
                    
                    if ACAccountStore().accountTypeWithAccountTypeIdentifier(ACAccountTypeIdentifierTwitter).accessGranted {
                        ApplicationDelegate.showLoader(loaderTitle_ConnectingTwitter)
                    }
                }
            }
            else {
                self.performSelector(#selector(hideLoaderConnectingTwitter), withObject: nil, afterDelay: 2)
            }
            
            Twitter.sharedInstance().logInWithCompletion { session, error in
                ApplicationDelegate.hideLoader()
                
                if error != nil {
                    printCustom("error:\(error?.code)")
                    printCustom("description:\(error?.description)")
                    if error?.code != 1 {
                        self.showCommonAlert(alertMsg_ErrorConnectingTwitter)
                    }
                    
                    return
                }
                if (session != nil) {
                    printCustom("signed in as \(session!.userName)");
                    
                    let controller = self.storyboard?.instantiateViewControllerWithIdentifier("ContactListVc") as? ContactListVc
                    controller!.addContactType = AddContactType.Twitter
                    controller!.sessionUserID = session?.userID
                    controller!.syncType = self.syncType
                    self.navigationController!.pushViewController(controller!, animated: true)
                    
                }
            }*/
            
            let store = TWTRTwitter.sharedInstance().sessionStore
            if store.session() == nil {
                ApplicationDelegate.showLoader(loaderTitle_ConnectingTwitter)
                
                self.perform(#selector(hideLoaderConnectingTwitter), with: nil, afterDelay: 5)
                TWTRTwitter.sharedInstance().logIn { session, error in
                    ApplicationDelegate.hideLoader()
                    
                    if error != nil {
                       // printCustom("error:\(error?.code)")
                        //printCustom("description:\(error?.description)")
                        if error?.code != 1 {
                            self.showCommonAlert(alertMsg_ErrorConnectingTwitter)
                        }
                      
                        return
                    }
                    else {
                        ApplicationDelegate.showLoader(loaderTitle_FetchingProfile)
                    }
                    if (session != nil) {
                        printCustom("signed in as \(session!.userName)");
                        
                        self.signedInWithTwitter(session!.userID)
                    }
                    else {
                        ApplicationDelegate.hideLoader()
                    }
                }
            }
            else {
                ApplicationDelegate.showLoader(loaderTitle_FetchingProfile)
                
                self.signedInWithTwitter(store.session()!.userID)
            }
        }
    }
    
    func signedInWithTwitter(_ userID:String) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "ContactListVc") as? ContactListVc
        controller!.addContactType = AddContactType.twitter
        controller!.sessionUserID = userID
        controller!.syncType = self.syncType
        self.navigationController!.pushViewController(controller!, animated: true)
    }
    
    @objc func hideLoaderConnectingTwitter() {
        ApplicationDelegate.hideLoader()
    }
    
    //MARK: - Send App Invites On FB/ Open AppStore
    func sendAppInvitesToFB() {
        let url:URL = URL(string: facebookAppLink)!//https://fb.me/679143348926867, http://coopeeps.trantorinc.com/coopeeps/meta.html
        printCustom("url:\(url.absoluteString)")
        let content: FBSDKAppInviteContent = FBSDKAppInviteContent()
        content.appLinkURL = url
        content.appInvitePreviewImageURL = URL(string: BASE_CLOUD_FRONT_URL + img_facebookAppInvite)
        printCustom("prview url:\(content.appInvitePreviewImageURL.absoluteString)")
        FBSDKAppInviteDialog.show(from: self.parent, with: content, delegate: self)
    }
    
    func openFacebookOnAppStore() {
        // open app store.
        let appStoreURL:URL = URL(string: facebookAppStoreURL)!
        if UIApplication.shared.canOpenURL(appStoreURL) {
            UIApplication.shared.openURL(appStoreURL)
        }
        else {
            self.showCommonAlert(alertMsg_DownloadFacebookToSendAppInvites)
        }
    }
    
    //MARK: - FBSDKAppInviteDialogDelegate
    func appInviteDialog(_ appInviteDialog: FBSDKAppInviteDialog!, didCompleteWithResults results: [AnyHashable: Any]!) {
        printCustom("invitation made:\(results)")
        if results != nil {
            if results.count > 0 && results["completionGesture"] == nil {
                self.showCommonAlert(alertMsg_InvitesSent)
            }
        }
        
    }

    func appInviteDialog(_ appInviteDialog: FBSDKAppInviteDialog!, didFailWithError error: Error!) {
        
        
    }
    

}
