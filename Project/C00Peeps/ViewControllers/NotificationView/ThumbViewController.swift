//
//  ThumbViewController.swift
//  C00Peeps
//
//  Created by SOTSYS027 on 3/29/16.
//  Copyright © 2016 SOTSYS011. All rights reserved.
//

import UIKit

class ThumbViewController: UIViewController {

    var sec = 60;
    @IBOutlet weak var lblTime: UILabel!
    var strTime : String!
    var helloWorldTimer : Timer!
   
    override func viewDidLoad()
    {
        super.viewDidLoad()
        strTime = String(format: "00:%d",sec)
           }
    
    override func viewWillAppear(_ animated: Bool)
    {
        helloWorldTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: Selector("sayHello"), userInfo: nil, repeats: true)
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        helloWorldTimer.invalidate()
    }
    
    func sayHello()
    {
        printCustom("sec:\(sec)")
        if sec == 0
        {
            let alert = UIAlertController(title: "Alert", message: "Your Peep Code Expire", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler:{ action in
                self.back()
            }))
            self.present(alert, animated: true, completion: nil)

            helloWorldTimer = nil
        }
        else
        {
            sec = sec - 1
            strTime = String(format: "00:%d",sec)
            lblTime.text = strTime
            
        }
    }
    
    
    
    @IBAction func btnpeepCodeClick(_ sender: UIButton)
    {
//        let controller1 : NotificationEnterPeepCodeView = UIStoryboard(name: "Notification", bundle: nil).instantiateViewControllerWithIdentifier("NotificationEnterPeepCodeView") as! NotificationEnterPeepCodeView
//        
//        self.navigationController?.pushViewController(controller1, animated: true)
    }
    
    @IBAction func btnBack(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    func back()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    
    }

}
