//
//  DownloadLockedMedia.swift
//  C00Peeps
//
//  Created by OSX on 29/11/16.
//  Copyright © 2016 SOTSYS011. All rights reserved.
//

import Foundation
import LocalAuthentication
import SwiftyJSON
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


protocol DownloadLockedMediaDelegate {
    
    func refreshUI(_ indexPathGlobal:IndexPath)
    func showMessage(_ messge:String)
}

class DownloadLockedMedia: NSObject{
    
    var downloadMediaAttachment = false
    var indexPathGlobal:IndexPath?
    var className = String()
    var flderName = String()
    var downloadLockDeleg:DownloadLockedMediaDelegate?
    
    var mediaURL = String()
    var mediaAttachmentURL = String()
    var mediaAttachmentType:Int = 0
    
    func unloackMedia(){
        
        if isTouchIdConfigured() == false {
 
            DispatchQueue.main.async(execute: {
                
                //Show pop up to enter peep code for verification
                self.displayPeepCodeVerification()
            })
        }
        else {
            
            //Touch id authentication
            authenticateUser()
        }
    }
    
    
    //MARK: - Authenticate User
    func isTouchIdConfigured () -> Bool {
        let context = LAContext()
        var error: NSError?
        // let reasonString = "Authentication is needed to access your app."
        // Check if the device can evaluate the policy.
        if context.canEvaluatePolicy(LAPolicy.deviceOwnerAuthenticationWithBiometrics, error: &error) {
            return true
        }
        else{
            // If the security policy cannot be evaluated then show a short message depending on the error.
            switch error!.code{
            case LAError.Code.touchIDNotEnrolled.rawValue:
                return false
                
            case LAError.Code.passcodeNotSet.rawValue:
                return false
                
            default:
                // The LAError.TouchIDNotAvailable case.
                return false
            }
        }
    }
    
    //MARK: - Touch id authentication
    func authenticateUser () {
        
        let context = LAContext()
        var error: NSError?
        
        //let myReasonString = "Need touch id authentication to download & unlock media"
        let myReasonString =  "Unlock digital media using Touch ID button"
        
        // let reasonString = "Authentication is needed to access your app."
        // Check if the device can evaluate the policy.
        if context.canEvaluatePolicy(LAPolicy.deviceOwnerAuthenticationWithBiometrics, error: &error) {
            
            //Implemented if we need to authenticate touch id and don't delete this code it may be required further
            [context .evaluatePolicy(LAPolicy.deviceOwnerAuthenticationWithBiometrics, localizedReason: myReasonString, reply: { (success: Bool, evalPolicyError: Error?) -> Void in
                
                if success {
                    
                    DispatchQueue.main.async(execute: {
                        
                        self.downloadMediaAttachment = false
                        
                        ApplicationDelegate.showLoader("Downloading Photo...")
                        
                        //Start downloading of media
                        self.downloadMedia()
                    })
                }
                else{
                    // If authentication failed then show a message to the console with a short description.
                    // In case that the error is a user fallback, then show the password alert view.
                    printCustom((evalPolicyError?.localizedDescription)!)
                    
                    switch evalPolicyError!.code {
                        
                    case LAError.Code.systemCancel.rawValue:
                        DispatchQueue.main.async(execute: {
                            
                            self.downloadLockDeleg!.showMessage("Authentication was cancelled by the system")

                            return
                        })
                    case LAError.Code.userCancel.rawValue:
                        DispatchQueue.main.async(execute: {
                            
                            self.downloadLockDeleg!.showMessage("Authentication was cancelled by the user")

                            return
                            
                        })
                    case LAError.Code.userFallback.rawValue:
                        DispatchQueue.main.async(execute: {
                            
                            //Show pop up to enter peep code for verification
                            
                            self.displayPeepCodeVerification()
                            
                            return
                        })
                        
                    default:
                        DispatchQueue.main.async(execute: {
                            
                             self.downloadLockDeleg!.showMessage("Authentication failed")
                            
                            return
                        })
                    }
                }
                
            } as! (Bool, Error?) -> Void)]
        }
        else{
            // If the security policy cannot be evaluated then show a short message depending on the error.
            switch error!.code{
                
            case LAError.Code.touchIDNotEnrolled.rawValue:
                
                DispatchQueue.main.async(execute: {
                    
                     self.downloadLockDeleg!.showMessage(alertMsg_TouchIdNotCongigured)
                    
                })
                return
                
            case LAError.Code.passcodeNotSet.rawValue:
                
                DispatchQueue.main.async(execute: {
                    
                     self.downloadLockDeleg!.showMessage(alertMsg_TouchIdNotCongigured)
                    
                })
                return
                
            default:
                // The LAError.TouchIDNotAvailable case.
                printCustom("Error occurred")
            }
            // Show the custom alert view to allow users to enter the password.
        }
    }
    
    //MARK: - Download media
    func downloadMedia(){
        
        //Downloading user's image
        URLSession.shared.dataTask(with: URL(string: mediaURL)!, completionHandler: { (data, response, error) -> Void in
            
            DispatchQueue.main.async(execute: { () -> Void in
                ApplicationDelegate.hideLoader()
            })
            
            if error != nil {
                
                //Show error can't download
                
                if self.downloadMediaAttachment == true{
                    
                    self.downloadMediaAttachment = false
                }
                
                print(error)
                return
            }
            
            DispatchQueue.main.async(execute: { () -> Void in
                
                var mediaName =  self.mediaURL.replacingOccurrences(of: BASE_CLOUD_FRONT_URL, with: "")
                
                //Decrypt data
                let decryptedData = Encryption.sharedInstance.decryptData(data!)
                
                if self.downloadMediaAttachment == true && self.mediaAttachmentType == 3  //video
                {
                    mediaName = mediaName.replacingOccurrences(of: ".dat", with: ".mp4")
                }
                else if self.downloadMediaAttachment == true && self.mediaAttachmentType == 2 //audio
                {
                    mediaName = mediaName.replacingOccurrences(of: ".dat", with: ".m4a")
                }
                else if self.downloadMediaAttachment == false
                {
                    mediaName = mediaName.replacingOccurrences(of: ".dat", with: ".png")
                }
                
                //Save media locally
                Utilities().saveMediaLocally(mediaName, mediaData: decryptedData, folderName: self.flderName)
                
                
                if self.downloadMediaAttachment == false && self.mediaAttachmentType == 3  //video
                {
                    ApplicationDelegate.showLoader("Downloading Video...")
                    
                    self.downloadMediaAttachment = true
                    self.mediaURL = self.mediaAttachmentURL
                    self.downloadMedia()
                }
                else if self.downloadMediaAttachment == false && self.mediaAttachmentType == 2 //audio
                {
                    ApplicationDelegate.showLoader("Downloading Audio...")
                    
                    self.downloadMediaAttachment = true
                    self.mediaURL = self.mediaAttachmentURL
                    self.downloadMedia()
                }
                else
                {
                    self.downloadLockDeleg!.refreshUI(self.indexPathGlobal!)
                }
            })
        }).resume()
    }
    
   
    //MARK: - Peep code verification popup
    func displayPeepCodeVerification()
    {
        //1. Create the alert controller.
        let alert = UIAlertController(title: "", message: "Please enter your peepcode", preferredStyle: .alert)
        
        //2. Add the text field. You can configure it however you need.
        alert.addTextField(configurationHandler: { (textField) -> Void in
            textField.placeholder = "enter peepcode"
        })
        
        //3. Grab the value from the text field, and print it when the user clicks OK.
        alert.addAction(UIAlertAction(title: "Send", style: .default, handler: { [weak alert] (action) -> Void in
            let textField = alert!.textFields![0] as UITextField
            
            if textField.text?.characters.count > 0
            {
                //Call API
                
                var parameters = [String: AnyObject]()
                parameters["user_id"] = Utilities.getUserDetails().id as AnyObject
                parameters["peepcode"] =  Int(textField.text!) as AnyObject
            
                if self.className == "NotificationViewController"
                {
                    let obj : NotificationViewController = self.downloadLockDeleg as! NotificationViewController
                    
                     NotificationCenter.default.addObserver(obj, selector: #selector (NotificationViewController.responsepeepCodeVerification), name:NSNotification.Name(rawValue: NOTIFIACTION_PEEPCODEVERIFICATION), object: nil)
                    
                     APIManager.sharedInstance.peepCodeVerificationRequest(parameters, Target: obj)
                }
                else if self.className ==  "UniversalCollectionView" || self.className == "UniversalCollectionViewPeepText"
                {
                    let obj : UniversalCollectionView = self.downloadLockDeleg as! UniversalCollectionView
                    
                    NotificationCenter.default.addObserver(obj, selector: #selector (UniversalCollectionView.responsepeepCodeVerification), name:NSNotification.Name(rawValue: NOTIFIACTION_PEEPCODEVERIFICATION), object: nil)
                    
                    APIManager.sharedInstance.peepCodeVerificationRequest(parameters, Target: obj)
                }
                else if self.className == "EventDetailsVc"
                {
                    let obj : EventDetailsVc = self.downloadLockDeleg as! EventDetailsVc
                    
                    NotificationCenter.default.addObserver(obj, selector: #selector (EventDetailsVc.responsepeepCodeVerification), name:NSNotification.Name(rawValue: NOTIFIACTION_PEEPCODEVERIFICATION), object: nil)
                    
                    APIManager.sharedInstance.peepCodeVerificationRequest(parameters, Target: obj)
                }
                else if self.className == "UniversalVc"
                {
                    let obj : UniversalVc = self.downloadLockDeleg as! UniversalVc
                    
                    NotificationCenter.default.addObserver(obj, selector: #selector (EventDetailsVc.responsepeepCodeVerification), name:NSNotification.Name(rawValue: NOTIFIACTION_PEEPCODEVERIFICATION), object: nil)
                    
                    APIManager.sharedInstance.peepCodeVerificationRequest(parameters, Target: obj)
                }
            }
            else
            {
                
            }
        }))
        
        // 4. Present the alert.
        
        if className == "NotificationViewController"
        {
            let obj : NotificationViewController = self.downloadLockDeleg as! NotificationViewController
            
            obj.present(alert, animated: true, completion: nil)
        }
        else if className ==  "UniversalCollectionView"
        {
            let obj : UniversalCollectionView = self.downloadLockDeleg as! UniversalCollectionView
            
            let objView = obj.updateDelegate as! GalleryMainViewController
            
            objView.present(alert, animated: true, completion: nil)
        }
        else if className == "UniversalCollectionViewPeepText"
        {
            let obj : UniversalCollectionView = self.downloadLockDeleg as! UniversalCollectionView
            
            let objView = obj.updateDelegate as! PeepTextDetailCollectionViewController
            
            objView.present(alert, animated: true, completion: nil)
        }
        else if className == "EventDetailsVc"
        {
            let obj : EventDetailsVc = self.downloadLockDeleg as! EventDetailsVc
            
            obj.present(alert, animated: true, completion: nil)
        }
        else if className == "UniversalVc"
        {
            let obj : UniversalVc = self.downloadLockDeleg as! UniversalVc
            
            obj.present(alert, animated: true, completion: nil)
        }
    }
    
    //MARK: - Response peep code verification
    func responsepeepCode(_ swiftyJsonVar: JSON)
    {
        let message = swiftyJsonVar["message"].stringValue
        let status = swiftyJsonVar["status"].intValue
        
        if swiftyJsonVar [ERROR_KEY].exists()
        {
            self.downloadLockDeleg!.showMessage(swiftyJsonVar[ERROR_KEY].stringValue)
        }
        else if (status == 1) //success response
        {
            DispatchQueue.main.async(execute: {
                
                self.downloadMediaAttachment = false
                
                ApplicationDelegate.showLoader("Downloading Photo...")
                
                //Start downloading of media
                self.downloadMedia()
                
            })
        }
        else if (status == 0) //error response
        {
            let responseErrorMessage = swiftyJsonVar["response"].stringValue
            self.downloadLockDeleg!.showMessage(responseErrorMessage)
        }
        else
        {
            self.downloadLockDeleg!.showMessage(message)
        }
    }
    
}
