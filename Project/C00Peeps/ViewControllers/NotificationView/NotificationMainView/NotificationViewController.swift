//
//  NotificationViewController.swift
//  C00Peeps
//
//  Created by SOTSYS027 on 3/29/16.
//  Copyright © 2016 SOTSYS011. All rights reserved.
//

import UIKit
import SwiftyJSON
import LocalAuthentication

class NotificationViewController: BaseVC,UITableViewDelegate,UITableViewDataSource,DownloadLockedMediaDelegate {

    @IBOutlet weak var lblNoRecordsFound: UILabel!
    @IBOutlet weak var tblNotification: UITableView!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    @IBOutlet weak var btnRefresh: UIButton!
    
    var notificationArray = [Notification]()
    var imageCache = [String:UIImage]()
    var offset = 0
    var page = 1
    var totalRecords = 0
    var refreshControl: UIRefreshControl!
    let objDownloadLockedMedia = DownloadLockedMedia()
    var eventRow:Int!
    
    override func viewWillAppear(_ animated: Bool) {
    
        super.viewWillAppear(animated)
        
        tblNotification.reloadData()
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.navigationItem.title = "Notification"
        addRefreshControl()        
        //Get Notifications List
        getNotifications()

    }
    
    //MARK: - Refresh
    func addRefreshControl(){
        
        refreshControl = UIRefreshControl()
        refreshControl.tintColor = UIColor.darkGray
        refreshControl.attributedTitle = NSAttributedString(string: loaderTitle_PullToRefresh)
        refreshControl.addTarget(self, action: #selector(NotificationViewController.handleRefresh(_:)), for: UIControlEvents.valueChanged)
        tblNotification.addSubview(refreshControl)
        
    }
    
    func handleRefresh(_ refreshControl: UIRefreshControl) {
        
        imageCache.removeAll()
        page = 1
        notificationArray.removeAll()
        offset = 0
        
        refreshControl.endRefreshing()
        
        //Get Notifications List
        getNotifications()
        
    }
    
    //MARK: - Button Actions
    @IBAction func btnRefreshClicked(_ sender: AnyObject) {
        self.lblNoRecordsFound.isHidden = true
        self.btnRefresh.isHidden = true
        self.loadingIndicator.startAnimating()
        self.getNotifications()
    }
    
    @IBAction func BtnBackClick(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }


    @IBAction func btnFollowAccept(_ sender: AnyObject) {
        
        let btn = sender as! UIButton
        
        let notify = notificationArray[btn.tag]

        //accept other user's follow request
        if notify.pCategoryTypeId == 3
        {

            //Create the AlertController
            let actionSheetController: UIAlertController = UIAlertController(title:"Follow Request", message: "", preferredStyle: .actionSheet)
            
            //Create and add the Cancel action
            let cancelAction: UIAlertAction = UIAlertAction(title: alertBtnTitle_Cancel, style: .cancel) { action -> Void in
                //Just dismiss the action sheet
            }
            actionSheetController.addAction(cancelAction)
            
            //Approve action
            let approveAction: UIAlertAction = UIAlertAction(title: "Approve", style: UIAlertActionStyle.default) { action -> Void in
                
                //dummy value to change button text from "Accept" to "Approved"
                notify.pLikeStatus = 111
                let buttonPosition = sender.convert(CGPoint.zero, to: self.tblNotification)
                let indexPath = self.tblNotification.indexPathForRow(at: buttonPosition)! as IndexPath
                
                self.tblNotification.beginUpdates()
                self.tblNotification.reloadRows(at: [indexPath], with: UITableViewRowAnimation.none)
                self.tblNotification.endUpdates()
                
                //Call API to start follow
                self.acceptFollowRequest(notify.pSentFromUserDetail[0].pId)
            }
            actionSheetController.addAction(approveAction)
            
            //Disapprove action
            let disapproveAction: UIAlertAction = UIAlertAction(title: "Disapprove", style: .destructive) { action -> Void in
                
                //dummy value to change button text from "Accept" to "Approved"
                notify.pLikeStatus = 112
                let buttonPosition = sender.convert(CGPoint.zero, to: self.tblNotification)
                let indexPath = self.tblNotification.indexPathForRow(at: buttonPosition)! as IndexPath
                
                self.tblNotification.beginUpdates()
                self.tblNotification.reloadRows(at: [indexPath], with: UITableViewRowAnimation.none)
                self.tblNotification.endUpdates()
                
                //Call API to start follow
                self.declineFollowRequest(notify.pSentFromUserDetail[0].pId)
            }
            actionSheetController.addAction(disapproveAction)
            
            //Present the AlertController
            self.present(actionSheetController, animated: true, completion: nil)
        }
            
        //if user has received locked media or locked peeptext attachment
        else if notify.pCategoryTypeId == 6 || notify.pCategoryTypeId == 5
        {
            let buttonPosition = sender.convert(CGPoint.zero, to: self.tblNotification)
            let indexPath = self.tblNotification.indexPathForRow(at: buttonPosition)! as IndexPath
            
            //Start media download & unlock process
            
            objDownloadLockedMedia.indexPathGlobal = indexPath
            objDownloadLockedMedia.mediaURL = notify.pMediaDetail.pMediaFile
            objDownloadLockedMedia.mediaAttachmentURL = notify.pMediaDetail.pMediaAttachment
            objDownloadLockedMedia.mediaAttachmentType = notify.pMediaDetail.pMediaAttachmentType
            objDownloadLockedMedia.downloadLockDeleg = self
            objDownloadLockedMedia.flderName = localReceivedFolderName
            objDownloadLockedMedia.className = "NotificationViewController"
            objDownloadLockedMedia.unloackMedia()
        }
         //if user has received locked event
        else if notify.pCategoryTypeId == 7
        {
            let buttonPosition = sender.convert(CGPoint.zero, to: self.tblNotification)
            let indexPath = self.tblNotification.indexPathForRow(at: buttonPosition)! as IndexPath
            
            //Start media download & unlock process
          
            objDownloadLockedMedia.indexPathGlobal = indexPath
           
            if let mURL = notify.pEventDetail.mediaUrl
            {
                objDownloadLockedMedia.mediaURL = mURL.absoluteString
            }
            
            if let mAttachmentURL = notify.pEventDetail.mediaAttachmentUrl
            {
                objDownloadLockedMedia.mediaAttachmentURL = mAttachmentURL.absoluteString
            }

            
            objDownloadLockedMedia.mediaAttachmentType = notify.pEventDetail.mediaType
            objDownloadLockedMedia.downloadLockDeleg = self
            objDownloadLockedMedia.flderName = localReceivedFolderName
            objDownloadLockedMedia.className = "NotificationViewController"
            objDownloadLockedMedia.unloackMedia()
        }
    }
    
    //MARK: - Event Invitation Accepted
    func eventInvitationAccepted() {
        //
       /* //Remove notification from list.
        notificationArray.removeAtIndex(eventRow)
        tblNotification.deleteRowsAtIndexPaths([NSIndexPath(forRow: eventRow, inSection: 0)], withRowAnimation: .Fade)
        
        printCustom("notificationArray.coun: \(notificationArray.count)")*/

    }
    
    //MARK: - Response peep code verification
    func responsepeepCodeVerification(_ notify: Foundation.Notification)
    {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFIACTION_PEEPCODEVERIFICATION), object: nil)
        
        let swiftyJsonVar = JSON(notify.object!)
        
        objDownloadLockedMedia.responsepeepCode(swiftyJsonVar)
    }
    
    //MARK: - Download Locked Media Delegate Methods
    
    func showMessage(_ messge:String){
        
        let ac = UIAlertController(title: "", message: messge, preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(ac, animated: true, completion: nil)
    }
    
    func refreshUI(_ indexPathGlobal:IndexPath){
        
        self.tblNotification.beginUpdates()
        self.tblNotification.reloadRows(at: [indexPathGlobal], with: UITableViewRowAnimation.none)
        self.tblNotification.endUpdates()
        
        navigateAfterUnlockMedia(indexPathGlobal)
    }
    
    
    //MARK: - Accept follow request
    func acceptFollowRequest(_ userId:Int){
        
        //{1=Approved, 2=Pending, 3=Blocked, 4=Unknown}
        
        var parameters = [String: AnyObject]()
        parameters["requester_id"] = Utilities.getUserDetails().id as AnyObject
        parameters["responder_id"] = userId as AnyObject
        parameters["status_id"] = 1 as AnyObject //Approved
        
        APIManager.sharedInstance.approveDisapproveFollowRequest(parameters, Target: self)
    }
    
    //MARK: - Decline follow request
    func declineFollowRequest(_ userId:Int){
        
        //{1=Approved, 2=Pending, 3=Blocked, 4=Unknown}
        
        var parameters = [String: AnyObject]()
        parameters["requester_id"] = Utilities.getUserDetails().id as AnyObject
        parameters["responder_id"] = userId as AnyObject
        parameters["status_id"] = 4 as AnyObject //Unknown
        
        APIManager.sharedInstance.approveDisapproveFollowRequest(parameters, Target: self)
    }
    
    //MARK: - Follow People Response
    func responseapproveDisapproveFollowRequest (_ notify: Foundation.Notification)
    {
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFIACTION_APPROVEDISAPPROVEFOLLOW), object: nil)
        
        let swiftyJsonVar = JSON(notify.object!)
        
         print(swiftyJsonVar)
        
        let message = swiftyJsonVar["message"].stringValue
        let status = swiftyJsonVar["status"].intValue
        
        if swiftyJsonVar [ERROR_KEY].exists()
        {
            self.showCommonAlert(swiftyJsonVar[ERROR_KEY].stringValue)
        }
        else if (status == 1) //success response
        {
            
        }
        else if (status == 0) //error response
        {
            let responseErrorMessage = swiftyJsonVar["response"].stringValue
            self.showCommonAlert(responseErrorMessage)
        }
        else
        {
            self.showCommonAlert(message)
        }
    }
    
    //MARK: - Get Notifications
    func getNotifications(){
        
        loadingIndicator.startAnimating()
        
        var parameters = [String: AnyObject]()
        parameters["user_id"] = Utilities.getUserDetails().id as AnyObject
        parameters["limit"] = Limit as AnyObject
        parameters["offset"] = offset as AnyObject
        
        APIManager.sharedInstance.getNotificationList(parameters, Target: self)
    }
    
    //MARK: - Search People Response
    func responseGetNotificationList (_ notify: Foundation.Notification)
    {
        refreshControl.endRefreshing()
        
        if loadingIndicator.isAnimating {
            loadingIndicator.stopAnimating()
        }
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFICATION_GETNOTIFICATIONLIST), object: nil)
        
        let swiftyJsonVar = JSON(notify.object!)
        
        let message = swiftyJsonVar["message"].stringValue
        let status = swiftyJsonVar["status"].intValue
        
        if swiftyJsonVar [ERROR_KEY].exists()
        {
            self.showErrorMessage(swiftyJsonVar[ERROR_KEY].stringValue)
        }
        else if (status == 1) //success response
        {
            if offset == 0
            {
                notificationArray.removeAll()
            }
            
            totalRecords = swiftyJsonVar["response"]["total_count"].intValue
            if totalRecords > 0
            {
                if swiftyJsonVar["response"]["result"].count > 0
                {
                    var arrFeeds = [Notification]()
                    arrFeeds = Parser.getParsedNotificationArrayFromData(swiftyJsonVar["response"]["result"])
                    
                    notificationArray.append(contentsOf: arrFeeds)
                    
                    if notificationArray.count>0
                    {
                        self.hideErrorMessage()
                        tblNotification.reloadData()
                    }
                }
            }
            else
            {
                tblNotification.reloadData()
                
                self.showErrorMessage(alertMsg_NoNotification)
            }
        }
        else if (status == 0) //error response
        {
            let responseErrorMessage = swiftyJsonVar["response"].stringValue
            self.showErrorMessage(responseErrorMessage)
        }
        else
        {
            self.showErrorMessage(message)
        }
    }
    
    // MARK: - Show/Hide Error Message
    func showErrorMessage(_ message:String) {
        self.btnRefresh.isHidden = false
        self.lblNoRecordsFound.isHidden = false
        self.lblNoRecordsFound.text = message
    }
    
    func hideErrorMessage() {
        self.btnRefresh.isHidden = true
        self.lblNoRecordsFound.isHidden = true
    }
    
    //MARK: - Read Notification
    func readNotification(_ notificationId:Int){
        
        loadingIndicator.startAnimating()
        
        if UIApplication.shared.applicationIconBadgeNumber > 1
        {
            UIApplication.shared.applicationIconBadgeNumber -= 1
        }
        
        
        var parameters = [String: AnyObject]()
        parameters["user_id"] = Utilities.getUserDetails().id as AnyObject
        parameters["notification_id"] = notificationId as AnyObject
        
        APIManager.sharedInstance.readNotification(parameters, Target: self)
    }
    
    //MARK: - Read Notification Response
    func responseReadNotification (_ notify: Foundation.Notification)
    {
        if loadingIndicator.isAnimating {
            loadingIndicator.stopAnimating()
        }
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFICATION_SETREADNOTIFICATION), object: nil)
        
        let swiftyJsonVar = JSON(notify.object!)
        
        let message = swiftyJsonVar["message"].stringValue
        let status = swiftyJsonVar["status"].intValue
        
        if swiftyJsonVar [ERROR_KEY].exists()
        {
            self.showErrorMessage(swiftyJsonVar[ERROR_KEY].stringValue)
        }
        else if (status == 1) //success response
        {
           
        }
        else if (status == 0) //error response
        {
            let responseErrorMessage = swiftyJsonVar["response"].stringValue
            self.showErrorMessage(responseErrorMessage)
        }
        else
        {
            self.showErrorMessage(message)
        }
    }
    
    //MARK: - UITableViewDelegate & UITableViewDataSource

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return notificationArray.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        //Load More
        if (notificationArray.count >= Limit) && (indexPath.row + 1  == notificationArray.count) {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "LoadMoreCell")
            
            if let lbl5176 =  cell?.viewWithTag(5176) as? UILabel
            {
                lbl5176.text = loading_Title
                
                if (totalRecords) > (page) * Limit {
                    
                    offset = (page) * Limit
                    page += 1
                    
                    getNotifications()
                }
                else
                {
                    lbl5176.isHidden = true
                    
                    lbl5176.text = loadingNoMoreData
                    
                    return refreshCellForRow(indexPath)
                }
            }
            
            return cell!
        }
            
        else{
            
            return refreshCellForRow(indexPath)
        }
    }

    func refreshCellForRow(_ indexPath:IndexPath)->NotificationCell{
        
        let cell : NotificationCell! = tblNotification.dequeueReusableCell(withIdentifier: "NotificationCustomCell",for: indexPath) as! NotificationCell
        
        if indexPath.row >= notificationArray.count
        {
            return cell
        }
        
        let notify = notificationArray[indexPath.row]
       
        cell.thumbImage.isHidden = true
        
        cell.lblNotificationTextRightWithSuperView.priority = UILayoutPriority(rawValue: 999)
        cell.lblNotificationTextRightWithThumbImage.priority = UILayoutPriority(rawValue: 900)
        
        cell.lblNotificationText.font = ProximaNovaRegular(15)
        
        var sentFromUsername = notify.pSentFromUserDetail[0].pUserName
       
        var mediaType = String()
        
        //(1 Photo 2 Audio 3 Video )
        
        if notify.pMediaDetail.pMediaType == 1 && notify.pMediaDetail.pMediaAttachmentType == 2
        {
            mediaType = "audio"
        }
        else if notify.pMediaDetail.pMediaType == 1
        {
            mediaType = "photo"
        }
        else if notify.pMediaDetail.pMediaType == 3
        {
            mediaType = "video"
        }
        
        
        switch notify.pCategoryTypeId
        {
        case 1: //Like your Post
            printCustom("notify.pSentFromUserDetail.count:\(notify.pSentFromUserDetail.count)")
            let othersUsersCount = notify.pSentFromUserDetail.count - 1
            printCustom("othersUsersCount:\(othersUsersCount)")
            
            var others = "others"
            if notify.pSentFromUserDetail.count > 1 {
                if othersUsersCount == 1 {
                    others = "other"
                }
                sentFromUsername += " and " + String(othersUsersCount) + " " + others
            }
            
            cell.lblNotificationText.text = sentFromUsername + " likes your " + mediaType
            
            break
            
        case 2:  //Comment on your post
            
            cell.lblNotificationText.text = sentFromUsername + " commented on your post"
            
            break
        case 3:   //New follow request
            
            cell.lblNotificationText.text = sentFromUsername + " sent you follow request"
            cell.thumbImage.tag = indexPath.row
            cell.thumbImage.isHidden = false
            
            if notify.pLikeStatus == 111
            {
                cell.thumbImage.setImage(Utilities().themedImage(img_ApprovedSelected), for: UIControlState())
            }
            else if notify.pLikeStatus == 112
            {
                cell.thumbImage.setImage(Utilities().themedImage(img_disApprovedSelected), for: UIControlState())
            }
            else
            {
                cell.thumbImage.setImage(Utilities().themedImage(img_Accept), for: UIControlState())
                cell.thumbImage.setImage(Utilities().themedImage(img_AcceptSelected), for: .highlighted)
                cell.thumbImage.setImage(Utilities().themedImage(img_AcceptSelected), for: .selected)
            }
            
            
            cell.lblNotificationTextRightWithSuperView.priority = UILayoutPriority(rawValue: 900)
            cell.lblNotificationTextRightWithThumbImage.priority = UILayoutPriority(rawValue: 999)
            
            break
        case 4:   //Accepted follow request
            
            cell.lblNotificationText.text = sentFromUsername + " accepted your follow request"
            
            break
        case 5:   //Peep Received
            
            cell.lblNotificationText.text = sentFromUsername + " sent you peep text"
            
            if notify.pMediaDetail.pIsLocked == 1
            {
                let localPath = Utilities().getLocalMediaPath(notify.pMediaDetail.pMediaFile, mediaAttachmentType: 0, folderName: localReceivedFolderName)
                
                if localPath.characters.count <= 0
                {
                    cell.thumbImage.tag = indexPath.row
                    cell.thumbImage.isHidden = false
                    cell.thumbImage.setImage(Utilities().themedImage(img_unlock), for: UIControlState())
                    cell.thumbImage.setImage(Utilities().themedImage(img_unlockSelected), for: .highlighted)
                    cell.thumbImage.setImage(Utilities().themedImage(img_unlockSelected), for: .selected)
                    
                    cell.lblNotificationTextRightWithSuperView.priority = UILayoutPriority(rawValue: 900)
                    cell.lblNotificationTextRightWithThumbImage.priority = UILayoutPriority(rawValue: 999)
                }
            }
            
            break
        case 6:   // Digital Media Received
            
            cell.lblNotificationText.text = sentFromUsername + " sent you " + mediaType
            
            if notify.pMediaDetail.pIsLocked == 1
            {
                let localPath = Utilities().getLocalMediaPath(notify.pMediaDetail.pMediaFile, mediaAttachmentType: 0, folderName: localReceivedFolderName)
                
                if localPath.characters.count <= 0
                {
                    cell.thumbImage.tag = indexPath.row
                    cell.thumbImage.isHidden = false
                    cell.thumbImage.setImage(Utilities().themedImage(img_unlock), for: UIControlState())
                    cell.thumbImage.setImage(Utilities().themedImage(img_unlockSelected), for: .highlighted)
                    cell.thumbImage.setImage(Utilities().themedImage(img_unlockSelected), for: .selected)
                    
                    cell.lblNotificationTextRightWithSuperView.priority = UILayoutPriority(rawValue: 900)
                    cell.lblNotificationTextRightWithThumbImage.priority = UILayoutPriority(rawValue: 999)
                }
            }
            
            break
        case 7:  //Event Invite Received
            printCustom("event title: \(notify.pEventDetail.title)")
            cell.lblNotificationText.text = sentFromUsername + " sent you an event invitation"
            
            if  notify.pEventDetail.isMediaLocked == 1 
            {
                let localPath = Utilities().getLocalMediaPath(notify.pEventDetail.mediaUrl!.absoluteString, mediaAttachmentType: 0, folderName: localReceivedFolderName)
                
                if localPath.characters.count <= 0
                {
                    cell.thumbImage.tag = indexPath.row
                    cell.thumbImage.isHidden = false
                    cell.thumbImage.setImage(Utilities().themedImage(img_unlock), for: UIControlState())
                    cell.thumbImage.setImage(Utilities().themedImage(img_unlockSelected), for: .highlighted)
                    cell.thumbImage.setImage(Utilities().themedImage(img_unlockSelected), for: .selected)
                    
                    cell.lblNotificationTextRightWithSuperView.priority = UILayoutPriority(rawValue: 900)
                    cell.lblNotificationTextRightWithThumbImage.priority = UILayoutPriority(rawValue: 999)
                }
            }
            
            break
        case 8:  //Event Accepted
            printCustom("event title: \(notify.pEventDetail.title)")
            cell.lblNotificationText.text = sentFromUsername + " accepted your event invitation"
            
            break
        case 9:  //Media Shared
            
            cell.lblNotificationText.text = sentFromUsername + " shared your " + mediaType
            
            break
        case 10:  //Version Update
            
            //cell.lblNotificationText.text = ""
            
            break
        default:
            
            break
        }
        
        cell.lblTime.font = ProximaNovaRegular(14)
        cell.lblTime?.text = Utilities().getDateAgo(notify.pCreatedDate)
        
        //user image
        cell.userImage!.image = Utilities().themedImage(img_defaultUserBig)
        cell.userImage!.contentMode = .scaleAspectFit
        cell.userImage!.layer.masksToBounds = false
        cell.userImage!.layer.cornerRadius = cell.userImage!.frame.height/2
        cell.userImage!.clipsToBounds = true
        
        if let img = imageCache[notify.pSentFromUserDetail[0].pUserImage] {
            cell.userImage!.image = img
        }
        else if imageCache[notify.pSentFromUserDetail[0].pUserImage] == nil && notify.pSentFromUserDetail[0].pUserImage != ""
        {
            //Downloading user's image
            URLSession.shared.dataTask(with: URL(string: notify.pSentFromUserDetail[0].pUserImage)!, completionHandler: { (data, response, error) -> Void in
                
                if error != nil {
                    
                    cell.userImage!.image = Utilities().themedImage(img_defaultUserBig)
                    
                    print(error)
                    return
                }
                
                DispatchQueue.main.async(execute: { () -> Void in
                    let image = UIImage(data: data!)
                    if (image == nil)
                    {
                        cell.userImage!.image = Utilities().themedImage(img_defaultUserBig)
                    }
                    else
                    {
                        cell.userImage!.image = image
                        self.imageCache[notify.pSentFromUserDetail[0].pUserImage] = cell.userImage!.image
                    }
                })
            }).resume()
        }
        
        //notification read
        if notify.pIsRead == 1
        {
            cell.notificationUnReadImage.isHidden = true
        }
        else
        {
            cell.notificationUnReadImage.isHidden = false
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        let cell : NotificationCell! = tblNotification.cellForRow(at: indexPath) as! NotificationCell
        
        cell.backgroundColor = UIColor.white
        
       navigateAfterUnlockMedia(indexPath)
    }
    
    //MARK:- Navigate after unlocking the media
    func navigateAfterUnlockMedia(_ index:IndexPath){
        
        let notify = notificationArray[index.row]
        
        if notify.pIsRead != 1
        {
            //read notification API call
            notify.pIsRead = 1
            readNotification(notify.pId)
        }
        
        switch notify.pCategoryTypeId
        {
        case 1: //Like your Post
            
            moveToSingleMediaScreen(notify.pMediaDetail)
            
            break
        case 2:  //Comment on your post
            
            moveToCommentScreen(notify.pMediaDetail)
            
            break
        case 3:   //New follow request
            
            
            
            break
        case 4:   //Accepted follow request
            
            
            
            break
        case 5:   //Peep Received
            
            if notify.pMediaDetail.pIsLocked == 0
            {
                moveToPeepTextDetailScreen(notify.pMediaDetail)
                
                //TODO:- need to show peep text single detail screen
            }
            else if notify.pMediaDetail.pIsLocked == 1
            {
                //If media is downloaded locally and unlocked
                if Utilities().getLocalMediaPath(notify.pMediaDetail.pMediaFile, mediaAttachmentType: 0, folderName:localReceivedFolderName).characters.count > 0
                {
                    moveToPeepTextDetailScreen(notify.pMediaDetail)
                }
                else
                {
                    //Start media download & unlock process
                    
                    objDownloadLockedMedia.indexPathGlobal = index
                    objDownloadLockedMedia.mediaURL = notify.pMediaDetail.pMediaFile
                    objDownloadLockedMedia.mediaAttachmentURL = notify.pMediaDetail.pMediaAttachment
                    objDownloadLockedMedia.mediaAttachmentType = notify.pMediaDetail.pMediaAttachmentType
                    objDownloadLockedMedia.downloadLockDeleg = self
                    objDownloadLockedMedia.flderName = localReceivedFolderName
                    objDownloadLockedMedia.className = "NotificationViewController"
                    objDownloadLockedMedia.unloackMedia()
                }
                
            }
            
            break
        case 6:   // Digital Media Received
            
            if notify.pMediaDetail.pIsLocked == 0
            {
                moveToSingleMediaScreen(notify.pMediaDetail)
            }
            else if notify.pMediaDetail.pIsLocked == 1
            {
                //If media is downloaded locally and unlocked
                if Utilities().getLocalMediaPath(notify.pMediaDetail.pMediaFile, mediaAttachmentType: 0, folderName:localReceivedFolderName).characters.count > 0
                {
                    moveToSingleMediaScreen(notify.pMediaDetail)
                }
                else
                {
                    
                    //Start media download & unlock process
                    
                    objDownloadLockedMedia.indexPathGlobal = index
                    objDownloadLockedMedia.mediaURL = notify.pMediaDetail.pMediaFile
                    objDownloadLockedMedia.mediaAttachmentURL = notify.pMediaDetail.pMediaAttachment
                    objDownloadLockedMedia.mediaAttachmentType = notify.pMediaDetail.pMediaAttachmentType
                    objDownloadLockedMedia.downloadLockDeleg = self
                    objDownloadLockedMedia.flderName = localReceivedFolderName
                    objDownloadLockedMedia.className = "NotificationViewController"
                    objDownloadLockedMedia.unloackMedia()
                }
            }
            
            break
        case 7:  //Event Invite Received
            printCustom("notificationArray.coun: \(notificationArray.count)")
            eventRow = index.row
            if notify.pEventDetail.isMediaLocked == 0
            {
                if notify.pEventDetail.isEventAccepted == 0 //Event is not accepted or rejected
                {
                    moveToEventDetailScreen(notify.pEventDetail, isEventAccepted: false, isEventRejected: false)
                }
                else if notify.pEventDetail.isEventAccepted == 1 //Event has been accepted
                {
                    moveToEventDetailScreen(notify.pEventDetail, isEventAccepted: true, isEventRejected:false)
                }
                else if notify.pEventDetail.isEventAccepted == 2 //Event has been rejected
                {
                    moveToEventDetailScreen(notify.pEventDetail, isEventAccepted: false, isEventRejected: true)
                }
            }
            else if notify.pEventDetail.isMediaLocked == 1
            {
                //If media is downloaded locally and unlocked
                if Utilities().getLocalMediaPath(notify.pEventDetail.mediaUrl!.absoluteString, mediaAttachmentType: 0, folderName:localReceivedFolderName).characters.count > 0
                {
                    if notify.pEventDetail.isEventAccepted == 0 //Event is not accepted or rejected
                    {
                        moveToEventDetailScreen(notify.pEventDetail, isEventAccepted: false, isEventRejected: false)
                    }
                    else if notify.pEventDetail.isEventAccepted == 1 //Event has been accepted
                    {
                        moveToEventDetailScreen(notify.pEventDetail, isEventAccepted: true, isEventRejected:false)
                    }
                    else if notify.pEventDetail.isEventAccepted == 2 //Event has been rejected
                    {
                        moveToEventDetailScreen(notify.pEventDetail, isEventAccepted: false, isEventRejected: true)
                    }
                }
                else
                {
                    //Start media download & unlock process
                    objDownloadLockedMedia.indexPathGlobal = index
                    
                    if let mURL = notify.pEventDetail.mediaUrl
                    {
                        objDownloadLockedMedia.mediaURL = mURL.absoluteString
                    }
                    
                    if let mAttachmentURL = notify.pEventDetail.mediaAttachmentUrl
                    {
                        objDownloadLockedMedia.mediaAttachmentURL = mAttachmentURL.absoluteString
                    }
                    
                    objDownloadLockedMedia.mediaAttachmentType = notify.pEventDetail.mediaType
                    objDownloadLockedMedia.downloadLockDeleg = self
                    objDownloadLockedMedia.flderName = localReceivedFolderName
                    objDownloadLockedMedia.className = "NotificationViewController"
                    objDownloadLockedMedia.unloackMedia()
                }
            }
            
            break
        case 8:  //Event Accepted - remove accept button and show status as "Accepted"
            
            let st = UIStoryboard(name: "Events", bundle:nil)
            let eventDetailsVc = st.instantiateViewController(withIdentifier: "EventDetailsId") as! EventDetailsVc
            eventDetailsVc.event = notify.pEventDetail
            eventDetailsVc.sourceScreen = SourceScreenEventDetails.eventInvitation
            eventDetailsVc.eventNotificationWasAcceptedByOther = true
            eventDetailsVc.notificationDelegate = self
            self.navigationController?.pushViewController(eventDetailsVc, animated: true)
            
            break
        case 9:  //Media Shared
            
            moveToSingleMediaScreen(notify.pMediaDetail)
            
            break
        case 10:  //Version Update
            
            //TODO:- need to decide
            //cell.lblNotificationText.text = ""
            
            break
        default:
            
            break
        }
    }
    
    //MARK:- Move to single media screen
    func moveToSingleMediaScreen(_ media:Media){
        
        let st = UIStoryboard(name: "Home", bundle:nil)
        let singleDetailVc = st.instantiateViewController(withIdentifier: "SingleDetailVc") as! SingleDetailVc
        
        var arr = [Media]()
        arr.append(media)
        singleDetailVc.arrMedia = arr
        self.navigationController?.pushViewController(singleDetailVc, animated: true)
    }
    
    //MARK:- Move to comment screen
    func moveToCommentScreen(_ media:Media){
        
        let st = UIStoryboard(name: "Home", bundle:nil)
        let commentObject = st.instantiateViewController(withIdentifier: "CommentVc") as! CommentVc
        
        commentObject.md = media
        self.navigationController?.pushViewController(commentObject, animated: true)
    }
    
    //MARK:- Move to event screen
    func moveToEventDetailScreen(_ event:Event, isEventAccepted:Bool, isEventRejected:Bool){
        
        let st = UIStoryboard(name: "Events", bundle:nil)
        let eventDetailsVc = st.instantiateViewController(withIdentifier: "EventDetailsId") as! EventDetailsVc
        eventDetailsVc.event = event
        eventDetailsVc.sourceScreen = SourceScreenEventDetails.eventInvitation
        eventDetailsVc.isEventAccepted = isEventAccepted
        eventDetailsVc.isEventRejected = isEventRejected
        eventDetailsVc.notificationDelegate = self
        self.navigationController?.pushViewController(eventDetailsVc, animated: true)
    }
    
    //MARK:- Move to Peep Text Detail screen
    func moveToPeepTextDetailScreen(_ media:Media){
        
        let st = UIStoryboard(name: "Gallery", bundle:nil)
        let peepTextDetailVc = st.instantiateViewController(withIdentifier: "PeepTextDetailCollectionViewController") as! PeepTextDetailCollectionViewController
        peepTextDetailVc.otherUserId = media.pUserId
        peepTextDetailVc.otherUserName = media.pUserName
        peepTextDetailVc.galleryType = .peepDetailGallery
        self.navigationController?.pushViewController(peepTextDetailVc, animated: true)
    }
    
    //MARK: -
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
