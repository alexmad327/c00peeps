//
//  NotificationCell.swift
//  C00Peeps
//
//  Created by SOTSYS027 on 3/29/16.
//  Copyright © 2016 SOTSYS011. All rights reserved.
//

import UIKit

class NotificationCell: UITableViewCell {


    @IBOutlet weak var lblNotificationText: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var thumbImage: UIButton!
    @IBOutlet weak var notificationUnReadImage: UIImageView!
    @IBOutlet weak var userImage: UIImageView!
    
    @IBOutlet weak var lblNotificationTextRightWithSuperView: NSLayoutConstraint!
    @IBOutlet weak var lblNotificationTextRightWithThumbImage:NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
