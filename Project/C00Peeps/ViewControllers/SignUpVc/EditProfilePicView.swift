//
//  EditProfilePicView.swift
//  C00Peeps
//
//  Created by Arshdeep on 18/11/16.
//  Copyright © 2016 SOTSYS011. All rights reserved.
//

import UIKit

class EditProfilePicView: UIView {

    @IBOutlet weak var btnCamera: UIButton!
    @IBOutlet weak var imgVwProfilePic: UIImageView!
    @IBOutlet weak var vwProfilePic: UIView!
    @IBOutlet var view: UIView!
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.setupXib()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.setupXib()
    }
    
    func setupXib() {
        let arrViews = Bundle.main.loadNibNamed("EditProfilePicView", owner: self, options: nil)
        self.view = arrViews![0] as! UIView
      
        // use bounds not frame or it'll be offset
        self.view.frame = bounds
        
        // Make the view stretch with containing view
        self.view.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        
        self.addSubview(self.view)
    }

}
