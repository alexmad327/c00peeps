
//  SignUpVc.swift
//  C00Peeps
//
//  Created by SOTSYS011 on 3/10/16.
//  Copyright © 2016 SOTSYS011. All rights reserved.
//

import UIKit
import SwiftyJSON
import LocalAuthentication


class SignUpVc_Main:BaseVC  {
    
    @IBOutlet weak var imgBG: UIImageView!
    @IBOutlet weak var imgLogo: UIImageView!
    @IBOutlet weak var viewButtons: UIView!
    var socialDetailsFromLogin = (accountType: AccountType.normal.rawValue, id:"", name: "", email: "")

    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.title = "Select Your Profile"
        self.updateViewAccordingToTheme()
    }
    
    // MARK: - Update View According To Selected Theme
    func updateViewAccordingToTheme() {
        imgBG.image = Utilities().themedImage(img_bg)
        imgLogo.image = Utilities().themedImage(img_signInLogo)
        
        // Basic
        let button1 = viewButtons.viewWithTag(1) as! UIButton
        button1.setBackgroundImage(Utilities().themedImage(img_basic), for: UIControlState())
        button1.setBackgroundImage(Utilities().themedImage(img_basicSelected), for: .highlighted)
        
        // Film
        let button2 = viewButtons.viewWithTag(2) as! UIButton
        button2.setBackgroundImage(Utilities().themedImage(img_film), for: UIControlState())
        button2.setBackgroundImage(Utilities().themedImage(img_filmSelected), for: .highlighted)
        
        // Movie
        let button3 = viewButtons.viewWithTag(3) as! UIButton
        button3.setBackgroundImage(Utilities().themedImage(img_music), for: UIControlState())
        button3.setBackgroundImage(Utilities().themedImage(img_musicSelected), for: .highlighted)
        
        // Game
        let button4 = viewButtons.viewWithTag(4) as! UIButton
        button4.setBackgroundImage(Utilities().themedImage(img_games), for: UIControlState())
        button4.setBackgroundImage(Utilities().themedImage(img_gamesSelected), for: .highlighted)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let viewController: SignUpVc_Form = segue.destination as? SignUpVc_Form {
            viewController.signUpType = (sender! as AnyObject).tag
            viewController.socialDetailsFromLogin = self.socialDetailsFromLogin
            viewController.socialMediaID = self.socialDetailsFromLogin.id
            
            if self.socialDetailsFromLogin.accountType == 1 //Facebook
            {
                viewController.accountType = AccountType.facebook
            }
            else if self.socialDetailsFromLogin.accountType == 2 //Twitter
            {
                viewController.accountType = AccountType.twitter
            }
            else if self.socialDetailsFromLogin.accountType == 3 //Instagram
            {
                viewController.accountType = AccountType.instagram
            }
        }
    }
}


