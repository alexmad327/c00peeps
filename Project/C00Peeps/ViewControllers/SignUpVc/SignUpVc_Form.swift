
//  SignUpVc.swift
//  C00Peeps
//
//  Created by SOTSYS011 on 3/10/16.
//  Copyright © 2016 SOTSYS011. All rights reserved.
//

import UIKit
import AWSS3
import AWSCore
import SwiftyJSON
import LocalAuthentication
import FBSDKLoginKit
import TwitterKit
import IQDropDownTextField
import REFormattedNumberField


class SignUpVc_Form:BaseVC, UITextFieldDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate,UIActionSheetDelegate {
    
    var signUpType:Int = 0
    
    @IBOutlet weak var imgSeparator: UIImageView!
    @IBOutlet weak var viewButtons: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var btnSignUpWithFB: UIButton!
    @IBOutlet weak var btnSignUpWithTwitter: UIButton!
    @IBOutlet weak var btnSignUpWithInstagram: UIButton!
    @IBOutlet weak var btnSubmit: UIButton!
    
    var arrFields : NSMutableArray = []
    var arrFieldType : NSMutableArray = []
    var arrPickerValues : NSMutableArray = []
    var dictResponse = [String: AnyObject]()
    var selectedRow: Int = 0
    var selectedDropdown: String = ""
    var selectedDropdownKey: String = ""
    var socialMediaID: String = ""
    var accountType: AccountType = AccountType.normal
    var isMandatoryAlertShown = false
    var socialId = ""
    var socialName = ""
    var socialEmail = ""
    var socialDetailsFromLogin = (accountType: AccountType.normal.rawValue, id:"", name: "", email: "")
    
    // Edit Profile
    var isEditingProfile:Bool = false
    let vwEditProfilePic: EditProfilePicView = EditProfilePicView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 180))
    @IBOutlet weak var topConstraintScrollViewEditProfile: NSLayoutConstraint!
    @IBOutlet weak var bottomConstraintScrollViewEditProfile: NSLayoutConstraint!
    var imagePicker = UIImagePickerController()
    var completionHandler: AWSS3TransferUtilityUploadCompletionHandlerBlock?
    var progressBlock: AWSS3TransferUtilityProgressBlock?
    var imageFile:String?
    var imageData = NSData()
    var isImageUpdated = false
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        // Hide screens views for 0.1 second to load theme images from docs directory.
        self.hideScreenViews()
        self.perform(#selector(self.designInterface), with: nil, afterDelay: 0.1)
    }
    
    //MARK: - Show/Hide Screen Views
    func showScreenViews() {
        for view in self.view.subviews {
            view.isHidden = false
        }
    }
    
    func hideScreenViews() {
        for view in self.view.subviews {
            view.isHidden = true
        }
    }
    
    //MARK: - Design Interface
    func designInterface() {
        if isEditingProfile {
            // Change title and bar button item.
            self.title = "Edit Profile"
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Save", style: .plain, target: self, action: #selector(btnSaveProfileClicked))
            
            // Remove bottom views used for sign up.
            self.btnSubmit.removeFromSuperview()
            self.imgSeparator.removeFromSuperview()
            self.viewButtons.removeFromSuperview()
            self.bottomConstraintScrollViewEditProfile.constant = 0
            self.topConstraintScrollViewEditProfile.constant = 180
            
            // Edit Profile Pic View
            
            self.view.addSubview(vwEditProfilePic)
            
            let tapGestureView:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(profilePicTapped))
            let tapGestureImageView:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(profilePicTapped))
            vwEditProfilePic.imgVwProfilePic.addGestureRecognizer(tapGestureImageView)
            vwEditProfilePic.vwProfilePic.addGestureRecognizer(tapGestureView)
            vwEditProfilePic.btnCamera.addTarget(self, action: #selector(profilePicTapped), for: .touchUpInside)
        }
        else {
            if (signUpType == 1) {
                self.title = "Basic User Profile"
            } else if (signUpType == 2) {
                self.title = "Film Industry Profile"
            } else if (signUpType == 3) {
                self.title = "Music Industry Profile"
            } else if (signUpType == 4) {
                self.title = "Video Game Industry Profile"
            }
        }
        
        self.updateViewAccordingToTheme()
        
        self.makeArrayForSignUpFields()
        
        if (signUpType != 1) {
            self.fetchFields()
        }
        else {
            if isEditingProfile {
                // Prefil profile if no fields to be updated in case of Edit Profile
                self.prefillProfile()
            }
        }
        
        // If user tries to sign in with social account who is not regsitered yet with PecX, then details are prefilled here to continue with social sign up account.
        if self.socialDetailsFromLogin.id != "" {
            switch self.socialDetailsFromLogin.accountType {
            case AccountType.facebook.rawValue:
                btnSignUpWithFB.setImage(Utilities().themedImage(img_facebookSelected), for: .normal)
                btnSignUpWithTwitter.setImage(Utilities().themedImage(img_twitterSignup), for: .normal)
                btnSignUpWithInstagram.setImage(Utilities().themedImage(img_instagramSignup), for: .normal)
            case AccountType.twitter.rawValue:
                btnSignUpWithFB.setImage(Utilities().themedImage(img_facebookSignup), for: .normal)
                btnSignUpWithTwitter.setImage(Utilities().themedImage(img_twitterSelected), for: .normal)
                btnSignUpWithInstagram.setImage(Utilities().themedImage(img_instagramSignup), for: .normal)
            case AccountType.instagram.rawValue:
                btnSignUpWithFB.setImage(Utilities().themedImage(img_facebookSignup), for: .normal)
                btnSignUpWithTwitter.setImage(Utilities().themedImage(img_twitterSignup), for: .normal)
                btnSignUpWithInstagram.setImage(Utilities().themedImage(img_instagramSelected), for: .normal)
            default:
                printCustom("Normal User")
            }
            
            self.prefillSocialMediaProfile(name: self.socialDetailsFromLogin.name, email: self.socialDetailsFromLogin.email)
        }
        self.showScreenViews()
    }
    
    // MARK: - Update View According To Selected Theme
    func updateViewAccordingToTheme() {
        // Submit
        btnSubmit.setBackgroundImage(Utilities().themedImage(img_btnSelected), for: .normal)
        
        // Sign Up Partition
        imgSeparator.image = Utilities().themedImage(img_partitionSignup)
        
        // Facebook
        btnSignUpWithFB.setImage(Utilities().themedImage(img_facebookSignup), for: .normal)
        btnSignUpWithFB.setImage(Utilities().themedImage(img_facebookSelected), for: .highlighted)
        
        // Twitter
        btnSignUpWithTwitter.setImage(Utilities().themedImage(img_twitterSignup), for: .normal)
        btnSignUpWithTwitter.setImage(Utilities().themedImage(img_twitterSelected), for: .highlighted)
        
        // Instagram
        btnSignUpWithInstagram.setImage(Utilities().themedImage(img_instagramSignup), for: .normal)
        btnSignUpWithInstagram.setImage(Utilities().themedImage(img_instagramSelected), for: .highlighted)
    }
    
    // MARK: - Button Actions
    func btnSaveProfileClicked() {//Used in case of Edit Profile
        if isImageUpdated {
            self.uploadData(data: imageData, contentType:ContentType_Image)
            printCustom("Length is, \(imageData.length))")
        }
        else {
            var imageName = Utilities.getUserDetails().user_image
            if (Utilities.getUserDetails().user_image.range(of:BASE_CLOUD_FRONT_URL) != nil) {
                imageName = Utilities.getUserDetails().user_image.replacingOccurrences(of: BASE_CLOUD_FRONT_URL, with: ""
                )
            }
            imageFile = imageName
            
            ApplicationDelegate.showLoader(loaderTitle_SavingProfile)
            self.callAPISignUpOrEditProfile()
        }
    }
    
    @IBAction func callAPI_SignUp(sender: AnyObject) {
        self.callAPISignUpOrEditProfile()
    }
    
    // MARK: - Sign Up Fields
    func makeArrayForSignUpFields() {
        
        let arrFields_Temp = [ "Name", "Title", "City", "State", "Email", "Phone", "Create User Name", "Password"]
        let arrFieldsType_Temp = [ "String", "String", "String", "String", "String", "String", "String", "String"]
        
        arrFields.addObjects(from: arrFields_Temp)
        arrFieldType.addObjects(from: arrFieldsType_Temp)
        
        if self.isEditingProfile {
            arrFields.remove("Password")
            arrFieldType.removeLastObject()
            
            arrFields.add("Website")
            arrFields.add("Tagline")
            arrFieldType.add("String")
            arrFieldType.add("String")
        }
        
        // Basic
        if (signUpType == 1) {
            // Do Nothing
        }
            // Film Industry
        else if (signUpType == 2) {
            arrFields.add("Media Type")
            arrFieldType.add("DropDown")
            
            arrFields.add("Film Format")
            arrFieldType.add("DropDown")
            
            arrFields.add("Film Genre")
            arrFieldType.add("DropDown")
            
            arrFields.add("Network Affiliation")
            arrFieldType.add("String")
        }
            // Music
        else if (signUpType == 3) {
            arrFields.add("Music Genre")
            arrFieldType.add("DropDown")
            
            arrFields.add("Record Label")
            arrFieldType.add("String")
        }
            // Video Games
        else if (signUpType == 4) {
            arrFields.add("Game Speciality")
            arrFieldType.add("DropDown")
            
            arrFields.add("Game Genre")
            arrFieldType.add("DropDown")
            
            arrFields.add("Gaming Company Affiliation")
            arrFieldType.add("String")
        }
        
        self.makeFormFields(arrFields: arrFields)
    }
    
    func makeFormFields(arrFields: NSMutableArray) {
        
        var yPos:Float = 10.0;
        var cntr:Int = 0;
        let controlHeight:CGFloat = 22.0
        
        for strFieldName in arrFields {
            
            let strType = arrFieldType.object(at: cntr) as! String
            yPos = 20*(Float(cntr)+1)+(Float(controlHeight)*Float(cntr))
            
            if (strType == "String") {
                if (strFieldName as! String == "Phone") {
                    let textField:REFormattedNumberField = REFormattedNumberField(frame: CGRect(x: 10, y: CGFloat(yPos), width: self.view.frame.size.width-20, height: controlHeight))
                    textField.placeholder = (strFieldName as! String)
                    textField.font = UIFont.systemFont(ofSize: 15)
                    textField.borderStyle = UITextBorderStyle.none
                    textField.autocorrectionType = UITextAutocorrectionType.default
                    textField.autocapitalizationType = .none
                    textField.clearButtonMode = UITextFieldViewMode.never;
                    textField.contentVerticalAlignment = UIControlContentVerticalAlignment.center
                    textField.textColor = UIColor.black
                    textField.textAlignment = NSTextAlignment.left
                    textField.delegate = self
                    textField.tag = 100+Int(cntr)
                    textField.keyboardType = UIKeyboardType.phonePad
                    textField.format = phoneNoFormat
                    
                    setSubViewBorder(textField, color:UIColor.lightGray)
                    self.changeTextfieldPlaceholderColor(textField, placeholderText: textField.placeholder!, color:UIColor.darkGray)
                    scrollView.addSubview(textField)
                }
                else {
                    let textField = UITextField(frame: CGRect(x: 10, y: CGFloat(yPos), width: self.view.frame.size.width-20, height: controlHeight))
                    if (strFieldName as! String) == "Name" {
                        textField.placeholder = "First Name - Last Name"
                    }
                    else {
                        textField.placeholder = (strFieldName as! String)
                    }
                    textField.font = UIFont.systemFont(ofSize: 15)
                    textField.borderStyle = UITextBorderStyle.none
                    textField.autocorrectionType = UITextAutocorrectionType.default
                    textField.clearButtonMode = UITextFieldViewMode.never;
                    textField.contentVerticalAlignment = UIControlContentVerticalAlignment.center
                    textField.textColor = UIColor.black
                    
                    textField.textAlignment = NSTextAlignment.left
                    textField.delegate = self
                    textField.tag = 100+Int(cntr)
                    
                    // Set Keyboard Type
                    if (strFieldName as! String == "Email") {
                        textField.keyboardType = UIKeyboardType.emailAddress
                    } else {
                        textField.keyboardType = UIKeyboardType.default
                    }
                    
                    if (strFieldName as! String == "Email") || strFieldName as! String == "Website" {
                        textField.autocapitalizationType = .none
                    }
                    
                    // Set Secure Textfield
                    if (strFieldName as! String == "Password") {
                        textField.isSecureTextEntry = true
                    }
                    
                    if isEditingProfile && (strFieldName as! String == "Create User Name" || strFieldName as! String == "Email") {//User name can't be edited.
                        textField.isEnabled = false
                        textField.textColor = UIColor.lightGray
                    }
                    
                    setSubViewBorder(textField, color:UIColor.lightGray)
                    self.changeTextfieldPlaceholderColor(textField, placeholderText: textField.placeholder!, color:UIColor.darkGray)
                    scrollView.addSubview(textField)
                    
                }
                
                
            }
            else if (strType == "DropDown") {
                let textField:IQDropDownTextField = IQDropDownTextField(frame: CGRect(x:10, y:CGFloat(yPos), width: self.view.frame.size.width-20,height: controlHeight))
                textField.placeholder = (strFieldName as! String)
                textField.font = UIFont.systemFont(ofSize: 15)
                textField.borderStyle = UITextBorderStyle.none
                textField.autocorrectionType = UITextAutocorrectionType.default
                textField.clearButtonMode = UITextFieldViewMode.never;
                textField.contentVerticalAlignment = UIControlContentVerticalAlignment.center
                textField.textColor = UIColor.black
                textField.textAlignment = NSTextAlignment.left
                textField.tag = 100+Int(cntr)
                
                setSubViewBorder(textField, color:UIColor.lightGray)
                self.changeTextfieldPlaceholderColor(textField, placeholderText: textField.placeholder!, color:UIColor.darkGray)
                scrollView.addSubview(textField)
                
                // Add Image as accessory
                let dropDownIage:UIImageView = UIImageView(frame: CGRect(x:textField.frame.size.width-14,y: 6,width: 9, height:5))
                dropDownIage.image = Utilities().themedImage(img_dropDown)
                textField.addSubview(dropDownIage)
            }
            
            cntr += 1
        }
        
        scrollView.contentSize = CGSize(width:self.view.frame.size.width, height:CGFloat(yPos)+50)
    }
    
    func prefillProfile() {
        var cntr:Int = 0
        for strFieldName in arrFields {
            let strType = arrFieldType.object(at: cntr) as! String
            if (strType == "String") {
                let textField = scrollView.viewWithTag(100+cntr) as! UITextField
                textField.isHidden = false
                switch strFieldName as! String {
                case "Name":
                    textField.text = Utilities.getUserDetails().name
                case "Title":
                    textField.text = Utilities.getUserDetails().title
                case "City":
                    textField.text = Utilities.getUserDetails().city
                case "State":
                    textField.text = Utilities.getUserDetails().state
                case "Email":
                    textField.text = Utilities.getUserDetails().email_id
                case "Phone":
                    textField.text = Utilities.getUserDetails().phone
                    if self.isEditingProfile {
                        let textField = scrollView.viewWithTag(100+cntr) as! REFormattedNumberField
                        //textField.formatInput(textField)
                        textField.format = textField.text
                    }
                case "Create User Name":
                    textField.text = Utilities.getUserDetails().username
                case "Website":
                    textField.text = Utilities.getUserDetails().website
                case "Tagline":
                    textField.text = Utilities.getUserDetails().tagline
                case "Network Affiliation":
                    textField.text = Utilities.getUserDetails().networkAffiliation
                case "Record Label":
                    textField.text = Utilities.getUserDetails().recordLabel
                case "Gaming Company Affiliation":
                    textField.text = Utilities.getUserDetails().gamingCompanyAffiliation
                default:
                    printCustom("field need not to prefil")
                }
                printCustom("textField placeholder:\(textField.placeholder)")
                printCustom("textField text:\(textField.text)")
                
            }
                
                // Drop downs
            else if (strType == "DropDown") {
                
                let textfieldDropdown = scrollView.viewWithTag(100+cntr) as! IQDropDownTextField
                var fieldName = (strFieldName as AnyObject).replacingOccurrences(of:" ", with:"_")
                fieldName = fieldName.lowercased()
                
                printCustom("Utilities.getUserDetails().mediaTypeId:\(Utilities.getUserDetails().filmMediaTypeId)")
                switch strFieldName as! String {
                case "Media Type":
                    textfieldDropdown.selectedItem = self.dropdownName(id: Utilities.getUserDetails().filmMediaTypeId, strFieldName: fieldName)
                case "Film Format":
                    textfieldDropdown.selectedItem = self.dropdownName(id: Utilities.getUserDetails().filmFormatId, strFieldName: fieldName)
                case "Film Genre":
                    textfieldDropdown.selectedItem = self.dropdownName(id: Utilities.getUserDetails().filmGenreId, strFieldName: fieldName)
                case "Music Genre":
                    textfieldDropdown.selectedItem = self.dropdownName(id: Utilities.getUserDetails().musicGenreId, strFieldName: fieldName)
                case "Game Speciality":
                    textfieldDropdown.selectedItem = self.dropdownName(id: Utilities.getUserDetails().gameSpecialityId, strFieldName: fieldName)
     
                case "Game Genre":
                    textfieldDropdown.selectedItem = self.dropdownName(id: Utilities.getUserDetails().gameGenreId, strFieldName: fieldName)
                default:
                    printCustom("field need not to prefil")
                }
                printCustom("textField placeholder:\(textfieldDropdown.placeholder)")
                printCustom("textField text:\(textfieldDropdown.selectedItem)")
            }
            cntr += 1
            
        }
        
        var imageCache = [String:UIImage]()
        if let img = imageCache[Utilities.getUserDetails().user_image] {
            vwEditProfilePic.imgVwProfilePic.image = img
        }
        else if imageCache[Utilities.getUserDetails().user_image] == nil
        {
            //Downloading media image
            URLSession.shared.dataTask(with: NSURL(string: Utilities.getUserDetails().user_image)! as URL, completionHandler: { (data, response, error) -> Void in
                if error != nil {
                    //show placeholder Image
                    self.vwEditProfilePic.imgVwProfilePic.image = Utilities().themedImage(img_defaultUserBig)
                    print(error)
                    return
                }
                
                DispatchQueue.main.async(execute: { () -> Void in
                    let image = UIImage(data: data!)
                    
                    if image == nil
                    {
                        //show placeholder Image
                        self.vwEditProfilePic.imgVwProfilePic.image = Utilities().themedImage(img_defaultUserBig)
                    }
                    else
                    {
                        self.vwEditProfilePic.imgVwProfilePic.image = image
                        imageCache[Utilities.getUserDetails().user_image] = self.vwEditProfilePic.imgVwProfilePic.image
                    }
                })
            }).resume()
        }
    }
    
    // MARK: - Prefill Social Media Profile
    func prefillSocialMediaProfile (name:String, email:String) {
        var cntr:Int = 0
        for strFieldName in self.arrFields {
            let strType = self.arrFieldType.object(at: cntr) as! String
            if (strType == "String") {
                let textField = self.scrollView.viewWithTag(100+cntr) as! UITextField
                switch strFieldName as! String {
                case "Name":
                    textField.text = name
                case "Email":
                    textField.text = email
                case "Password":
                    textField.isHidden = true
                    scrollView.contentSize = CGSize(width:self.view.frame.size.width, height: self.scrollView.contentSize.height - 50)
                    
                //textField.enabled = false
                default:
                    printCustom("field need not to prefil")
                }
            }
            
            cntr += 1
        }
    }
    
    func showPasswordField() {
        var cntr:Int = 0
        for strFieldName in self.arrFields {
            let strType = self.arrFieldType.object(at: cntr) as! String
            if (strType == "String") {
                let textField = self.scrollView.viewWithTag(100+cntr) as! UITextField
                switch strFieldName as! String {
                case "Password":
                    if textField.isHidden {
                        textField.isHidden = false
                        scrollView.contentSize = CGSize(width:self.view.frame.size.width, height:self.scrollView.contentSize.height + 50)
                    }
                    
                default:
                    printCustom("field need not to prefil")
                }
            }
            
            cntr += 1
        }
    }
    
    func emptyPrefilledSocialMediaProfile () {
        var cntr:Int = 0
        for _ in self.arrFields {
            let strType = self.arrFieldType.object(at: cntr) as! String
            if (strType == "String") {
                let textField = self.scrollView.viewWithTag(100+cntr) as! UITextField
                textField.text = ""
            }
            
            cntr += 1
        }
    }
    
    // MARK: - Mark Fields Mandatory
    func markFieldsMandatory() {
        if isMandatoryAlertShown {
            // Change Fields Placeholder Color - If text is empty, then show mark red color, otherwise dark gray to indicatory mandatory field.
            var cntr:Int = 0
            for _ in arrFields {
                let textField = scrollView.viewWithTag(100+cntr) as! UITextField
                let strType = arrFieldType.object(at: cntr) as! String
                if (strType == "String" || strType == "DropDown") {
                    var color = UIColor()
                    if textField.text == "" {
                        color = UIColor.red
                    }
                    else {
                        color = UIColor.darkGray
                    }
                    self.changeTextfieldPlaceholderColor(textField, placeholderText: textField.placeholder!, color: color)
                }
                cntr += 1
            }
        }
    }
    
    // MARK: - Textfield Delegates
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        self.markFieldsMandatory()
    }
    
    // MARK: - Fetch Facebook Profile
    func fetchFacebookProfile() {
        ApplicationDelegate.showLoader(loaderTitle_FetchingProfile)
        FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, email"]).start(completionHandler: { (connection, result, error) -> Void in
            ApplicationDelegate.hideLoader()
            if (error == nil){
                
                let resultDict = result as! NSDictionary
                
//                printCustom("profile:\(result)")
//                printCustom("profile email:\(result["email"])")
                self.socialMediaID = resultDict["id"] as! String
                self.prefillSocialMediaProfile(name: resultDict["name"] as! String, email: resultDict["email"] as! String)
            }
            else {
                if self.socialMediaID == "" && self.accountType == AccountType.facebook {
                    self.btnSignUpWithFB.setImage(Utilities().themedImage(img_facebookSignup), for: .normal)
                    self.showPasswordField()
                }
            }
        })
    }
    
    // MARK: - Button Actions
    @IBAction func btnSignUpWithFBClicked(sender: AnyObject) {
        btnSignUpWithFB.setImage(Utilities().themedImage(img_facebookSelected), for: .normal)
        btnSignUpWithTwitter.setImage(Utilities().themedImage(img_twitterSignup), for: .normal)
        btnSignUpWithInstagram.setImage(Utilities().themedImage(img_instagramSignup), for: .normal)
        
        //        btnSignUpWithFB.userInteractionEnabled = false
        //        btnSignUpWithTwitter.userInteractionEnabled = true
        //        btnSignUpWithInstagram.userInteractionEnabled = true
        
        emptyPrefilledSocialMediaProfile()
        
        self.accountType = AccountType.facebook
        self.socialMediaID = ""
        if FBSDKAccessToken.current() != nil {
            self.fetchFacebookProfile()
        }
        else {
            let facebookLogin = FBSDKLoginManager()
            facebookLogin.logIn(withReadPermissions: ["email"],  from: self) { (facebookResult: FBSDKLoginManagerLoginResult!, facebookError: Error!) -> Void in
                if !facebookResult.isCancelled {
                    self.fetchFacebookProfile()
                }
                else {
                    if self.socialMediaID == "" && self.accountType == AccountType.facebook {
                        self.btnSignUpWithFB.setImage(Utilities().themedImage(img_facebookSignup), for: .normal)
                        self.showPasswordField()
                    }
                }
            }
        }
    }
    
    @IBAction func btnSignUpWithTwitterClicked(sender: AnyObject) {
        btnSignUpWithFB.setImage(Utilities().themedImage(img_facebookSignup), for: .normal)
        btnSignUpWithTwitter.setImage(Utilities().themedImage(img_twitterSelected), for: .normal)
        btnSignUpWithInstagram.setImage(Utilities().themedImage(img_instagramSignup), for: .normal)
        
        //        btnSignUpWithFB.userInteractionEnabled = true
        //        btnSignUpWithTwitter.userInteractionEnabled = false
        //        btnSignUpWithInstagram.userInteractionEnabled = true
        
        emptyPrefilledSocialMediaProfile()
        
        self.accountType = AccountType.twitter
        self.socialMediaID = ""
        
        let store = TWTRTwitter.sharedInstance().sessionStore
        if store.session() == nil {
            ApplicationDelegate.showLoader(loaderTitle_ConnectingTwitter)
            
            self.perform(#selector(hideLoaderConnectingTwitter), with: nil, afterDelay: 2)
            TWTRTwitter.sharedInstance().logIn { session, error in
                ApplicationDelegate.hideLoader()
                
                if error != nil {
                    //printCustom("error:\(error?.code)")
                    //printCustom("description:\(error?.description)")
                    if error?.code != 1 {
                        self.showCommonAlert(alertMsg_ErrorConnectingTwitter)
                    }
                    
                    if self.socialMediaID == "" && self.accountType == AccountType.twitter {
                        self.btnSignUpWithTwitter.setImage(Utilities().themedImage(img_twitterSignup), for: .normal)
                        self.showPasswordField()
                    }
                    
                    return
                }
                else {
                    ApplicationDelegate.showLoader(loaderTitle_FetchingProfile)
                }
                if (session != nil) {
                    printCustom("signed in as \(session!.userName)");
                    
                    self.signedInWithTwitter(userID: session!.userID)
                }
                else {
                    ApplicationDelegate.hideLoader()
                }
            }
        }
        else {
            ApplicationDelegate.showLoader(loaderTitle_FetchingProfile)
            
            self.signedInWithTwitter(userID: store.session()!.userID)
        }
    }
    
    func signedInWithTwitter(userID:String) {
        self.socialMediaID = userID
        
        let client = TWTRAPIClient(userID: self.socialMediaID)
        client.loadUser(withID: self.socialMediaID) { (user, error) -> Void in
            ApplicationDelegate.hideLoader()
            
            printCustom("twitter user profile: \(user)");
            self.prefillSocialMediaProfile(name: user!.name, email: "")
        }
    }
    
    func hideLoaderConnectingTwitter() {
        ApplicationDelegate.hideLoader()
    }
    
    @IBAction func btnSignUpWithInstagramClicked(sender: AnyObject) {
        btnSignUpWithFB.setImage(Utilities().themedImage(img_facebookSignup), for: .normal)
        btnSignUpWithTwitter.setImage(Utilities().themedImage(img_twitterSignup), for: .normal)
        btnSignUpWithInstagram.setImage(Utilities().themedImage(img_instagramSelected), for: .normal)
        
        //        btnSignUpWithFB.userInteractionEnabled = true
        //        btnSignUpWithTwitter.userInteractionEnabled = true
        //        btnSignUpWithInstagram.userInteractionEnabled = false
        
        emptyPrefilledSocialMediaProfile()
        
        self.accountType = AccountType.instagram
        self.socialMediaID = ""
        
        let navController:UINavigationController = self.storyboard?.instantiateViewController(withIdentifier: "InstagramLoginID") as! UINavigationController
        let instagramLoginVC:InstagramLoginVC = navController.topViewController as! InstagramLoginVC
        instagramLoginVC.signUpDelegate = self
        self.present(navController, animated: true, completion: nil)
        
    }
    
    //MARK: - Tap Gesture Actions
    func profilePicTapped() {
        printCustom("profilePicTapped")
        self.openActionSheet()
    }
    
    //MARK: - Get Dropdown Id/Name
    func dropDownId(textfieldDropdown:IQDropDownTextField, strFieldName:String) -> String {
        var dropdownKey = strFieldName.replacingOccurrences(of: " ", with: "_")
        dropdownKey = dropdownKey.lowercased()
        printCustom("dropdownKey:\(dropdownKey)")
        
        let dropdownValues:NSArray = dictResponse[dropdownKey] as! NSArray
        printCustom("dropdownValues:\(dropdownValues)")
        
        let dropdownValue = textfieldDropdown.selectedItem!
        printCustom("dropdownValue:\(dropdownValue)")
        
        let index = ((dropdownValues.value(forKey: "name")) as AnyObject).index(of:dropdownValue)
        printCustom("index:\(index)")
        
        let dropdownId:String = (dropdownValues.value(forKey: "id") as AnyObject).object(at:index) as! String
        printCustom("dropdownId:\(dropdownId)")
        return dropdownId
    }
    
    
    func dropdownName(id:String, strFieldName:String) -> String {
        let dropdownValues:NSArray = dictResponse[strFieldName] as! NSArray
        printCustom("dropdownValues:\(dropdownValues)")
        
        let index = ((dropdownValues.value(forKey: "id")) as AnyObject).index(of:id)
        
        let dropdownName:String = (dropdownValues.value(forKey: "name") as AnyObject).object(at:index) as! String
        printCustom("dropdownName:\(dropdownName)")
        
        return dropdownName
    }
    
    //MARK: - Image Picker Delegates
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        let image = info[UIImagePickerControllerEditedImage] as! UIImage
        imageData = UIImageJPEGRepresentation(image, 0.5)! as NSData
        self.vwEditProfilePic.imgVwProfilePic.image = image
        isImageUpdated = true
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        isImageUpdated = false
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK: - Open Action Sheet
    func openActionSheet() {
        let actionSheet =  UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        actionSheet.addAction(UIAlertAction(title: alertBtnTitle_Camera, style: UIAlertActionStyle.default, handler:{ action -> Void in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera){
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = UIImagePickerControllerSourceType.camera
                self.imagePicker.allowsEditing = true
                self.present(self.imagePicker, animated: true, completion: nil)
            }
            
        }))
        actionSheet.addAction(UIAlertAction(title: alertBtnTitle_Gallery, style: UIAlertActionStyle.default, handler:{ action -> Void in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary){
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
                self.imagePicker.allowsEditing = true
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        }))
        
        actionSheet.addAction(UIAlertAction(title: alertBtnTitle_Cancel, style: UIAlertActionStyle.cancel, handler:nil))
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    //MARK: - Upload Image
    func uploadData(data: NSData, contentType:String ) {
        ApplicationDelegate.showLoader(loaderTitle_SavingProfile)
        
        let expression = AWSS3TransferUtilityUploadExpression()
        expression.progressBlock = progressBlock
        let transferUtility = AWSS3TransferUtility.default()
        var S3UploadKeyName1 = ""
        S3UploadKeyName1 = String.init(format: "%@.png",Utilities().getUUIDForObjectUpload())
        imageFile = S3UploadKeyName1
        printCustom("S3UploadKeyName1: \(S3UploadKeyName1)")
        
        transferUtility.uploadData(
            data as Data,
            bucket: S3BucketName,
            key: S3UploadKeyName1,
            contentType: contentType,
            expression: expression,
            completionHandler: completionHandler).continueWith { (task) -> AnyObject! in
                if let error = task.error {
                    ApplicationDelegate.hideLoader()
                    let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                    
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (UIAlertAction) -> Void in
                        
                    }))
                    
                    self.present(alert, animated: true, completion: nil)
                    
                }
                
              
                if task.isFaulted {
                    ApplicationDelegate.hideLoader()
                    let alert = UIAlertController(title: "Error", message: "Error Occured", preferredStyle: UIAlertControllerStyle.alert)
                    
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (UIAlertAction) -> Void in
                        
                    }))
                    
                    self.present(alert, animated: true, completion: nil)
                    
                }
                if let _ = task.result {
                    DispatchQueue.main.async(execute: {
                        self.callAPISignUpOrEditProfile()
                    })
                    printCustom("Upload Starting!")
                }
                
                return nil;
        }
    }
    
    //MARK: - Call API
    func fetchFields() {
        
        ApplicationDelegate.showLoader(loaderTitle_UpdatingFields)
        let strSignUpType = String(signUpType)
        var parameters = [String: AnyObject]()
        parameters["signUpType"] = strSignUpType as AnyObject
        APIManager.sharedInstance.formDataSignUpPage(parameters, Target:self)
    }
    
    func callAPISignUpOrEditProfile() {
        var parameters = [String: AnyObject]()
        var cntr:Int = 0
        for strFieldName in arrFields {
            let strType = arrFieldType.object(at: cntr) as! String
            if (strType == "String") {
                
                let textField = scrollView.viewWithTag(100+cntr) as! UITextField
                
                // Unformatted Phone No
                var phoneNo:NSString = ""
                if strFieldName as! String == "Phone" {
                    let phoneTextField:REFormattedNumberField = scrollView.viewWithTag(100+cntr) as! REFormattedNumberField
                    phoneNo = phoneTextField.unformattedText as! NSString
                }
                
                if ((strFieldName as! String == "Name" || strFieldName as! String == "Email" ||
                    strFieldName as! String == "Phone" || strFieldName as! String == "Create User Name" ||
                    strFieldName as! String == "Password") && self.accountType == AccountType.normal) {
                    
                    // Check if string is empty
                    if (textField.text! as String == "") {
                        isMandatoryAlertShown = true
                        self.markFieldsMandatory()
                        self.showCommonAlert(alertMsg_EnterRequiredFields)
                        return
                    }
                    else if strFieldName as! String == "Email" && !Utilities().isValidEmail(textField.text!) {
                        self.showCommonAlert(alertMsg_EnterValidEmailId)
                        return
                    }
                    else if strFieldName as! String == "Phone" && phoneNo.length != 10 {
                        self.showCommonAlert(alertMsg_PhoneNumberLimit)
                        return
                    }
                    else if strFieldName as! String == "Password"
                    {
                        let passwordRule = Utilities().checkPasswordRule(textField.text! )
                        
                        if passwordRule.characters.count > 0{
                            textField.text = ""//Empty password textfield if this field is not validated
                            self.showCommonAlert(passwordRule)
                            return
                        }
                    }
                }
                else if ((strFieldName as! String == "Name" || strFieldName as! String == "Email" ||
                    strFieldName as! String == "Phone" || strFieldName as! String == "Create User Name") && self.accountType != AccountType.normal) {
                    
                    // Check if string is empty
                    if (textField.text! as String == "") {
                        isMandatoryAlertShown = true
                        self.markFieldsMandatory()
                        self.showCommonAlert(alertMsg_EnterRequiredFields)
                        return
                    }
                    else if strFieldName as! String == "Email" && !Utilities().isValidEmail(textField.text!) {
                        self.showCommonAlert(alertMsg_EnterValidEmailId)
                        return
                    }
                    else if strFieldName as! String == "Phone" && phoneNo.length != 10 {
                        self.showCommonAlert(alertMsg_PhoneNumberLimit)
                        return
                    }
                }
                
                if (strFieldName as! String == "Name") {
                    parameters["name"] = textField.text! as AnyObject
                }
                if (strFieldName as! String == "Title") {
                    parameters["title"] = textField.text! as AnyObject
                }
                if (strFieldName as! String == "City") {
                    parameters["city"] = textField.text! as AnyObject
                }
                if (strFieldName as! String == "State") {
                    parameters["state"] = textField.text! as AnyObject
                }
                if (strFieldName as! String == "Email") {
                    parameters["email"] = textField.text! as AnyObject
                }
                if (strFieldName as! String == "Phone") {
                    let textFieldFormatted = scrollView.viewWithTag(100+cntr) as! REFormattedNumberField
                    parameters["phone"] = textFieldFormatted.unformattedText! as AnyObject
                    printCustom("textFieldFormatted.unformattedText!:\(textFieldFormatted.unformattedText!)")
                }
                if (strFieldName as! String == "Create User Name") {
                    parameters["user_name"] = textField.text! as AnyObject
                }
                if (strFieldName as! String == "Password") {
                    parameters["password"] = textField.text! as AnyObject
                }
                if isEditingProfile {
                    if (strFieldName as! String == "Website") {
                        parameters["website"] = textField.text! as AnyObject
                    }
                    if (strFieldName as! String == "Tagline") {
                        parameters["tagline"] = textField.text! as AnyObject
                    }
                }
                
                // Profile Fields
                // Film Industry
                if (signUpType == 2) {
                    if (strFieldName as! String == "Network Affiliation") {
                        parameters["network_affiliation"] = textField.text! as AnyObject
                    }
                }
                    // Music
                else if (signUpType == 3) {
                    if (strFieldName as! String == "Record Label") {
                        parameters["record_label"] = textField.text! as AnyObject
                    }
                }
                    // Video Games
                else if (signUpType == 4) {
                    if (strFieldName as! String == "Gaming Company Affiliation") {
                        parameters["gaming_company_affiliation"] = textField.text! as AnyObject
                    }
                }
            }
                
                // Drop downs
            else if (strType == "DropDown") {
                
                let textfieldDropdown = scrollView.viewWithTag(100+cntr) as! IQDropDownTextField
                
                // Add parameter only if value is changed
                
                if textfieldDropdown.selectedItem == nil {
                    isMandatoryAlertShown = true
                    self.markFieldsMandatory()
                    self.showCommonAlert(alertMsg_EnterRequiredFields)
                    return
                }
                
                // Film Industry
                if (signUpType == 2) {
                    if (strFieldName as! String == "Media Type") {
                        parameters["film_media_type_id"] = self.dropDownId(textfieldDropdown: textfieldDropdown, strFieldName: strFieldName as! String) as AnyObject
                    }
                    else if (strFieldName as! String == "Film Format") {
                        parameters["film_format_id"] = self.dropDownId(textfieldDropdown: textfieldDropdown, strFieldName: strFieldName as! String) as AnyObject
                    }
                    else if (strFieldName as! String == "Film Genre") {
                        parameters["film_genre_id"] = self.dropDownId(textfieldDropdown: textfieldDropdown, strFieldName: strFieldName as! String) as AnyObject
                    }
                    
                }
                    // Music
                else if (signUpType == 3) {
                    if (strFieldName as! String == "Music Genre") {
                        parameters["music_genre_id"] = self.dropDownId(textfieldDropdown: textfieldDropdown, strFieldName: strFieldName as! String) as AnyObject
                    }
                }
                    // Video Games
                else if (signUpType == 4) {
                    if (strFieldName as! String == "Game Speciality") {
                        parameters["game_speciality_id"] = self.dropDownId(textfieldDropdown: textfieldDropdown, strFieldName: strFieldName as! String) as AnyObject
                    }
                    if (strFieldName as! String == "Game Genre") {
                        parameters["game_genre_id"] = self.dropDownId(textfieldDropdown: textfieldDropdown, strFieldName: strFieldName as! String) as AnyObject
                    }
                }
            }
            
            cntr += 1
        }
        
        parameters["profile_type_id"] = signUpType as AnyObject
        
        if !isEditingProfile {
            
            //Utilities.getDeviceToken()
            parameters["device_token"] = Utilities.getDeviceToken() as AnyObject//"67c5aaa7de122bc5e8a2726c081be79204faede76829e617b625c1"
            parameters["device_type"] = "iPhone" as AnyObject
            
            //MARK:- TODO
            parameters["eot_purchased"] = 0 as AnyObject
            parameters["social_account_type"] = self.accountType.rawValue as AnyObject
            if self.accountType.rawValue != 0 {
                parameters["social_unique_id"] = self.socialMediaID as AnyObject
            }
            
            //parameters["public_key"] = Encryption.sharedInstance.createRSAPublicKey(parameters["user_name"] as! String)
            parameters["public_key"] = "publicKey" as AnyObject
            
            printCustom("sign up parameters:\(parameters)")
            
            ApplicationDelegate.showLoader(loaderTitle_SigningUp)
            APIManager.sharedInstance.requestSignup(parameters,Target:self)
        }
        else {
            parameters["user_id"] = Utilities.getUserDetails().id as AnyObject
            parameters["user_image"] = imageFile as AnyObject
            
            APIManager.sharedInstance.requestEditProfile(parameters,Target:self)
        }
        
    }
    
    //MARK: - API Response
    func responseFetchFields (notify: NSNotification) {
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFICATION_USERTYPE), object: nil)
        
        ApplicationDelegate.hideLoader ()
        let swiftyJsonVar = JSON(notify.object!)
        let message = swiftyJsonVar["message"].stringValue
        let status = swiftyJsonVar["status"].intValue
        
        if swiftyJsonVar [ERROR_KEY].exists() {
            self.showCommonAlert(swiftyJsonVar[ERROR_KEY].stringValue)
        }
        else if (status == 1) {
            let response = swiftyJsonVar["response"]
            dictResponse = response.dictionaryObject! as [String : AnyObject]
            printCustom("dictResponse:\(dictResponse)")
            for key in dictResponse.keys {
                selectedDropdown = key
                printCustom("selectedDropdown:\(selectedDropdown)")
                
                let pickerValues = dictResponse[selectedDropdown] as! NSArray
                printCustom("pickerValues:\(pickerValues)")
                
                var cntr:Int = 0
                for strFieldName in arrFields {
                    let strType = arrFieldType.object(at: cntr) as! String
                    if (strType == "DropDown") {
                        let textfield = scrollView.viewWithTag(100+cntr) as! IQDropDownTextField
                        var fieldName = ((strFieldName) as AnyObject).replacingOccurrences(of:" ", with:"_")
                        fieldName = fieldName.lowercased()
                        printCustom("fieldName:\(fieldName)")
                        if selectedDropdown == fieldName {
                            textfield.itemList = pickerValues.value(forKey: "name") as! [String]
                            printCustom("textfield.itemList:\(textfield.itemList)")
                        }
                    }
                    cntr += 1
                }
                
                if isEditingProfile {
                    self.prefillProfile()
                }
            }
        }
        else if (status == 0) { // Error response
            let responseErrorMessage = swiftyJsonVar["response"].stringValue
            self.showCommonAlert(responseErrorMessage)
        }
        else {
            self.showCommonAlert(message)
        }
    }
    
    func responseCallAPI_SignUp (notify: NSNotification) {
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFIACTION_SIGNUP), object: nil)
        ApplicationDelegate.hideLoader ()
        printCustom("notification called")
        ApplicationDelegate.hideLoader()
        let swiftyJsonVar = JSON(notify.object!)
        let message = swiftyJsonVar["message"].stringValue
        let status = swiftyJsonVar["status"].intValue
        
        if swiftyJsonVar [ERROR_KEY].exists()
        {
            self.showCommonAlert(swiftyJsonVar[ERROR_KEY].stringValue)
        }
            //got success response
        else if (status == 1)
        {
            self.saveUserDetails(userId: swiftyJsonVar["response"]["user_id"].stringValue)
            
            Utilities.setSignUpState(SignUpState.selectPackage)
            
            let responseSuccessMessage = swiftyJsonVar["response"]["message"].stringValue
            let alert:UIAlertController = UIAlertController(title: "", message: responseSuccessMessage, preferredStyle: .alert)
            let alertAction = UIAlertAction(title: alertBtnTitle_OK, style: .default, handler: { (UIAlertAction) -> Void in
                let packageScreenVC = self.storyboard?.instantiateViewController(withIdentifier: "SelectPackageVC") as? SelectPackageVC
                self.navigationController?.pushViewController(packageScreenVC!, animated: true)
            })
            alert.addAction(alertAction)
            self.present(alert, animated: true, completion: nil)
        }
        else if (status == 0) //error response
        {
            let responseErrorMessage = swiftyJsonVar["response"].stringValue
            self.showCommonAlert(responseErrorMessage)
        }
        else{
            self.showCommonAlert(message)
        }
    }
    
    func responseEditProfile (notify: NSNotification)
    {
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFIACTION_CHANGEPASSWORD), object: nil)
        ApplicationDelegate.hideLoader ()
        let swiftyJsonVar = JSON(notify.object!)
        let message = swiftyJsonVar["message"].stringValue
        let status = swiftyJsonVar["status"].intValue
        
        if (swiftyJsonVar [ERROR_KEY] != nil) {
            self.showCommonAlert(swiftyJsonVar[ERROR_KEY].stringValue)
        }
        else if (status == 1) {
            let response = swiftyJsonVar["response"]["user_details"]
            let responseUserDetail:[String:AnyObject] = response.dictionaryObject! as [String : AnyObject]
            let userDetail:UserDetail = Utilities.getUserDetails()
            Parser.parseUserDetails(responseUserDetail, userDetail: userDetail)
            
            let alert:UIAlertController = UIAlertController(title: "", message: alertMsg_ProfileSaved, preferredStyle: .alert)
            let alertAction = UIAlertAction(title: "OK", style: .default, handler: { (UIAlertAction) -> Void in
                self.navigationController?.popViewController(animated: true)
            })
            
            alert.addAction(alertAction)
            self.present(alert, animated: true, completion: nil)
            
            //self.navigationController?.pushViewController(homeScreen!, animated: true)
        }
        else if (status == 0) {
            let responseErrorMessage = swiftyJsonVar["response"].stringValue
            self.showCommonAlert(responseErrorMessage)
        }
        else {
            self.showCommonAlert(message)
        }
    }
    
    // MARK: - Save User Details
    func saveUserDetails(userId:String) {
        let userDetail:UserDetail = UserDetail()
        userDetail.id = userId
        
        var cntr:Int = 0
        for strFieldName in arrFields {
            let strType = arrFieldType.object(at: cntr) as! String
            if (strType == "String") {
                
                let textField = scrollView.viewWithTag(100+cntr) as! UITextField
                
                if (strFieldName as! String == "Name") {
                    userDetail.name = textField.text!
                }
                if (strFieldName as! String == "Title") {
                    userDetail.title = textField.text!
                }
                if (strFieldName as! String == "City") {
                    userDetail.city = textField.text!
                }
                if (strFieldName as! String == "State") {
                    userDetail.state = textField.text!
                }
                if (strFieldName as! String == "Email") {
                    userDetail.email_id = textField.text!
                }
                if (strFieldName as! String == "Phone") {
                    userDetail.phone = textField.text!
                }
                if (strFieldName as! String == "Create User Name") {
                    userDetail.username = textField.text!
                }
                if (strFieldName as! String == "Password") {
                    userDetail.password = textField.text!
                }
                
                // Film Industry
                if (signUpType == 2) {
                    if (strFieldName as! String == "Network Affiliation") {
                        userDetail.networkAffiliation = textField.text!
                    }
                }
                    // Music
                else if (signUpType == 3) {
                    if (strFieldName as! String == "Record Label") {
                        userDetail.recordLabel = textField.text!
                    }
                }
                    // Video Games
                else if (signUpType == 4) {
                    if (strFieldName as! String == "Gaming Company Affiliation") {
                        userDetail.gamingCompanyAffiliation = textField.text!
                    }
                }
            }
            else if strType == "DropDown" {
                let textfieldDropdown = scrollView.viewWithTag(100+cntr) as! IQDropDownTextField
                var fieldName = (strFieldName as AnyObject).replacingOccurrences(of:" ", with:"_")
                fieldName = fieldName.lowercased()
                // Film Industry
                if (signUpType == 2) {
                    if (strFieldName as! String == "Media Type") {
                        userDetail.filmMediaTypeId = self.dropDownId(textfieldDropdown: textfieldDropdown, strFieldName: fieldName)
                    }
                    else if (strFieldName as! String == "Film Format") {
                        userDetail.filmFormatId = self.dropDownId(textfieldDropdown: textfieldDropdown, strFieldName: fieldName)
                    }
                    else if (strFieldName as! String == "Film Genre") {
                        userDetail.filmGenreId = self.dropDownId(textfieldDropdown: textfieldDropdown, strFieldName: fieldName)
                    }
                    
                }
                    // Music
                else if (signUpType == 3) {
                    if (strFieldName as! String == "Music Genre") {
                        userDetail.musicGenreId = self.dropDownId(textfieldDropdown: textfieldDropdown, strFieldName: fieldName)
                    }
                }
                    // Video Games
                else if (signUpType == 4) {
                    if (strFieldName as! String == "Game Speciality") {
                        userDetail.gameSpecialityId = self.dropDownId(textfieldDropdown: textfieldDropdown, strFieldName: fieldName)
                    }
                    if (strFieldName as! String == "Game Genre") {
                        userDetail.gameGenreId = self.dropDownId(textfieldDropdown: textfieldDropdown, strFieldName: fieldName)
                    }
                }
            }
            cntr += 1
        }
        userDetail.profile_type_id = String(signUpType)
        userDetail.social_account_type = String(self.accountType.rawValue)
        if self.accountType.rawValue != 0 {
            userDetail.social_unique_id = self.socialMediaID
        }
        
        //Setting Default values. Need to be updated in next screens
        userDetail.eot_purchased = "0"//Default Extention of Time Purchased is 0
        userDetail.privacy_level_id = "3"//Default level is Public
        userDetail.package_id = "1"//Default package is Basic (Free)
        if Utilities.getUserDetails().theme_id == "" {
            userDetail.theme_id = "1"//Default theme is Purple (t1)
        }
        else {
            // Theme is set to previous one.
            userDetail.theme_id = Utilities.getUserDetails().theme_id
        }
        userDetail.submerchantExists = "0"
        
        //Details need to be taken care in next screens
        userDetail.peepcode = ""
        //userDetail.public_key = Encryption.sharedInstance.createRSAPublicKey(userDetail.username)
        userDetail.public_key = "publicKey"
        
        userDetail.user_image = ""
        
        Utilities.setUserDetails(userDetail)
        
        printCustom("user details city: \(Utilities.getUserDetails().city)")
        printCustom("user details email_id: \(Utilities.getUserDetails().email_id)")
        printCustom("user details eot_purchased: \(Utilities.getUserDetails().eot_purchased)")
        printCustom("user details id: \(Utilities.getUserDetails().id)")
        printCustom("user details name: \(Utilities.getUserDetails().name)")
        printCustom("user details package_id: \(Utilities.getUserDetails().package_id)")
        printCustom("user details password: \(Utilities.getUserDetails().password)")
        printCustom("user details peepcode: \(Utilities.getUserDetails().peepcode)")
        printCustom("user details phone: \(Utilities.getUserDetails().phone)")
        printCustom("user details privacy_level_id: \(Utilities.getUserDetails().privacy_level_id)")
        printCustom("user details profile_type_id: \(Utilities.getUserDetails().profile_type_id)")
        printCustom("user details public_key: \(Utilities.getUserDetails().public_key)")
        printCustom("user details social_account_type: \(Utilities.getUserDetails().social_account_type)")
        printCustom("user details social_unique_id: \(Utilities.getUserDetails().social_unique_id)")
        printCustom("user details state: \(Utilities.getUserDetails().state)")
        printCustom("user details theme_id: \(Utilities.getUserDetails().theme_id)")
        printCustom("user details title: \(Utilities.getUserDetails().title)")
        printCustom("user details user_image: \(Utilities.getUserDetails().user_image)")
        printCustom("user details username: \(Utilities.getUserDetails().username)")
        printCustom("user details gameSpecialityId: \(Utilities.getUserDetails().gameSpecialityId)")
        printCustom("user details gameGenreId: \(Utilities.getUserDetails().gameGenreId)")
        printCustom("Utilities.getUserDetails().mediaTypeId:\(Utilities.getUserDetails().filmMediaTypeId)")
        
    }
}

