
//
//  UniversalViewController.swift
//  C00Peeps
//
//  Created by OSX on 14/07/16.
//  Copyright © 2016 SOTSYS011. All rights reserved.
//

import Foundation
import SwiftyJSON
import FBSDKLoginKit
import FBSDKShareKit
import TwitterKit

protocol UniversalDelegate {
    
    func moveToNextScreen(_ ScreenName:String, WithData:AnyObject)
    func reloadMedia(_ obj:UniversalVc)
    func loadMoreMedia(_ page:Int, offset:Int, obj:UniversalVc)
}

//hack for optional protocol methods in Swift
extension UniversalDelegate {
    
    func reloadMedia(_ obj:UniversalVc){}
    func loadMoreMedia(_ page:Int, offset:Int, obj:UniversalVc){}
}

class UniversalVc: BaseVC, PlayMediaDelegate, UITableViewDataSource , UITableViewDelegate, FBSDKSharingDelegate, DownloadLockedMediaDelegate {
    
    @IBOutlet var tblTimeline: UITableView!
    
    var bComingFromSingleMediaScreen = false
    var arrMedia = [Media]()
    var offset = 0
    var page = 1
    var totalRecordCount = 0
    var imageCache = [String:UIImage]()
    var refreshControl: UIRefreshControl!
    var arrFeeds = [People]()
    
    var universalDeleg:UniversalDelegate?
    var indexPathlastPlayed:IndexPath?
    
    var mediaSharedId: Int = 0
    var mediaSharedOwnerId: Int = 0
    let objDownloadLockedMedia = DownloadLockedMedia()
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        DispatchQueue.main.async{
            
            if (self.indexPathlastPlayed != nil) {
                let cellSel = self.tblTimeline.cellForRow(at: self.indexPathlastPlayed!) as! HomeTabMediaCell
                cellSel.stopVideo()
            }
        }
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Self-sizing magic!
        tblTimeline.rowHeight = UITableViewAutomaticDimension
        
        
        //Don't show pull to refresh & load more on Single Detail Screen
        if bComingFromSingleMediaScreen == false
        {
            addRefreshControl()
        }
    }
    
    func addRefreshControl(){
        
        refreshControl = UIRefreshControl()
         refreshControl.tintColor = UIColor.darkGray
        refreshControl.attributedTitle = NSAttributedString(string: loaderTitle_PullToRefresh)
        refreshControl.addTarget(self, action: #selector(UniversalVc.handleRefresh(_:)), for: UIControlEvents.valueChanged)
        tblTimeline.addSubview(refreshControl)

    }
    
    func handleRefresh(_ refreshControl: UIRefreshControl) {
        
        imageCache.removeAll()
        page = 1
        offset = 0
        
        refreshControl.endRefreshing()
        
        universalDeleg?.reloadMedia(self)
        
    }
    
    func reloadTable()
    {
        
        tblTimeline.reloadData()
    }
    
   
    
    // MARK: -  Actions methods
    
    @IBAction func btnPlayAction(_ sender: AnyObject){
        
        //Refreshing tble cell
        let buttonPosition = sender.convert(CGPoint.zero, to: tblTimeline)
        let indexPath = tblTimeline.indexPathForRow(at: buttonPosition)! as IndexPath
        let cell = self.tblTimeline.cellForRow(at: indexPath) as! HomeTabMediaCell
        
        let btn = sender as! UIButton
        
        //get media obj from index
        let md = arrMedia[btn.tag]
        
        //stop previous video item if any
        stopPreviousPlayedVideo(indexPath)
        
        indexPathlastPlayed = indexPath
        
        //If audio attachment exist
        if md.pMediaAttachmentType == 2
        {
            cell.delegate = self
            cell.cellTag = btn.tag
            
            if md.pIsPlaying == 0
            {
                if md.pIsLocked == 1
                {
                    var localPath = ""
                    
                    //if media is sent media
                    if String(md.pUserId) == Utilities.getUserDetails().id
                    {
                        localPath = Utilities().getLocalMediaPath(md.pMediaAttachment, mediaAttachmentType: 2, folderName:localSentFolderName)
                    }
                        
                        //if media is received media
                    else
                    {
                        localPath = Utilities().getLocalMediaPath(md.pMediaAttachment, mediaAttachmentType: 2, folderName: localReceivedFolderName)
                    }
                    
                    if localPath.characters.count > 0
                    {
                        cell.loadVideo(URL.init(fileURLWithPath: localPath))
                    }
                    else
                    {
                        //Start media download & unlock process
                        
                        objDownloadLockedMedia.indexPathGlobal = indexPath
                        objDownloadLockedMedia.mediaURL = md.pMediaFile
                        objDownloadLockedMedia.mediaAttachmentURL = md.pMediaAttachment
                        objDownloadLockedMedia.mediaAttachmentType = md.pMediaAttachmentType
                        objDownloadLockedMedia.downloadLockDeleg = self
                        objDownloadLockedMedia.flderName = localReceivedFolderName
                        objDownloadLockedMedia.className = "UniversalVc"
                        objDownloadLockedMedia.unloackMedia()
                    }
                }
                else
                {
                    let soundURL = URL.init(string: md.pMediaAttachment)
                    cell.loadVideo(soundURL!)
                }
            }
            else
            {
                cell.stopVideo()
            }
            
        }
        //If video attachment exist
        else if md.pMediaAttachmentType == 3
        {
            cell.delegate = self
            cell.cellTag = btn.tag
            if md.pIsPlaying == 0
            {
                if md.pIsLocked == 1
                {
                    var localPath = ""
                    
                    //if media is sent media
                    if String(md.pUserId) == Utilities.getUserDetails().id
                    {
                        localPath = Utilities().getLocalMediaPath(md.pMediaAttachment, mediaAttachmentType: 3, folderName:localSentFolderName)
                    }
                        
                        //if media is received media
                    else
                    {
                        localPath = Utilities().getLocalMediaPath(md.pMediaAttachment, mediaAttachmentType: 3, folderName: localReceivedFolderName)
                    }
                    
                    if localPath.characters.count > 0
                    {
                        cell.loadVideo(URL.init(fileURLWithPath: localPath))
                    }
                    else
                    {
                        objDownloadLockedMedia.indexPathGlobal = indexPath
                        objDownloadLockedMedia.mediaURL = md.pMediaFile
                        objDownloadLockedMedia.mediaAttachmentURL = md.pMediaAttachment
                        objDownloadLockedMedia.mediaAttachmentType = md.pMediaAttachmentType
                        objDownloadLockedMedia.downloadLockDeleg = self
                        objDownloadLockedMedia.flderName = localReceivedFolderName
                        objDownloadLockedMedia.className = "UniversalVc"
                        objDownloadLockedMedia.unloackMedia()
                    }
                }
                else
                {
                    let videoURL = URL.init(string: md.pMediaAttachment)
                    cell.loadVideo(videoURL!)
                }
            }
            else
            {
                cell.stopVideo()
            }
            
        }
        
    }
    
    func playFinished(_ cellTag:Int)
    {
        let md = arrMedia[cellTag]
        md.pIsPlaying = 0
        indexPathlastPlayed = nil
    }
    
    func playStart(_ cellTag:Int)
    {
        let md = arrMedia[cellTag]
        md.pIsPlaying = 1
    }
    
    //MARK: - Download Locked Media Delegate Methods
    
    func showMessage(_ messge:String){
        
        let ac = UIAlertController(title: "", message: messge, preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(ac, animated: true, completion: nil)
    }
    
    func refreshUI(_ indexPathGlobal:IndexPath){
        
        tblTimeline.reloadData()
    }

    
    //MARK:- Like Action
    @IBAction func btnLikeAction(_ sender: AnyObject){
        if reachability?.currentReachabilityStatus == .notReachable {
            self.showCommonAlert(alertMsg_NoInternetConnection)
        }
        else
        {
            let btn = sender as! UIButton
        
            //get media obj from index
            let md = arrMedia[btn.tag]
            
            //If media is not already liked
            if md.pIsLiked == 0
            {
                if(md.pCountDislikes != 0 && md.pIsDisliked == 1)
                {
                    md.pCountDislikes -= 1
                }
                
                md.pIsLiked = 1
                md.pIsDisliked = 0
                md.pCountLikes += 1
                
                
            }
            else //If media is already liked
            {
                md.pIsLiked = 0
                
                if(md.pCountLikes != 0)
                {
                    md.pCountLikes -= 1
                }
            }

            let buttonPosition = sender.convert(CGPoint.zero, to: self.tblTimeline)
            let indexPath = tblTimeline.indexPathForRow(at: buttonPosition)! as IndexPath
            
            
            self.tblTimeline.beginUpdates()
            tblTimeline.reloadRows(at: [indexPath], with: UITableViewRowAnimation.none)
            self.tblTimeline.endUpdates()
            
            //Calling API
            likeMediaRequest(md.pId,likerId: md.pUserId)
        }
    }
    @IBAction func btnLikeCountAction(_ sender: AnyObject){
        
        let btn = sender as! UIButton
        //get media obj from index
        let md = arrMedia[btn.tag]
        if (md.pCountLikes != 0) {
        let st = UIStoryboard(name: "Gallery", bundle:nil)
        let likersVc = st.instantiateViewController(withIdentifier: "LikersViewController") as! LikersViewController
        likersVc.mediaId = md.pId
        likersVc.likerMediaType = .gallery
        likersVc.screenType = .likersList

        self.navigationController?.pushViewController(likersVc, animated: true)
        }
        else{
            let alert = UIAlertController(title: "Alert", message: "No like exist.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    //MARK:- Dislike Action
    @IBAction func btnDislikeAction(_ sender: AnyObject){
        if reachability?.currentReachabilityStatus == .notReachable {
            self.showCommonAlert(alertMsg_NoInternetConnection)
        }
        else {
        
            let btn = sender as! UIButton
            
            //get media obj from index
            let md = arrMedia[btn.tag]
            
            //If media was not already disliked
            if md.pIsDisliked == 0
            {
                
                if(md.pCountLikes != 0 && md.pIsLiked == 1)
                {
                    md.pCountLikes -= 1
                }
                
                md.pCountDislikes += 1
                md.pIsDisliked = 1
                md.pIsLiked = 0
                
               
            }
            else  //If media was already disliked
            {
                md.pIsDisliked = 0
                
                if(md.pCountDislikes != 0)
                {
                    md.pCountDislikes -= 1
                }
            }
            
            let buttonPosition = sender.convert(CGPoint.zero, to: self.tblTimeline)
            let indexPath = tblTimeline.indexPathForRow(at: buttonPosition)! as IndexPath
            
            self.tblTimeline.beginUpdates()
            tblTimeline.reloadRows(at: [indexPath], with: UITableViewRowAnimation.none)
            self.tblTimeline.endUpdates()
            
            
            //Calling API
            dislikeMediaRequest(md.pId,likerId: md.pUserId)
        }
    }

    @IBAction func btnDisLikeCountAction(_ sender: AnyObject){
        
        let btn = sender as! UIButton
        //get media obj from index
        let md = arrMedia[btn.tag]
        if (md.pCountDislikes != 0) {
            let st = UIStoryboard(name: "Gallery", bundle:nil)
            let likersVc = st.instantiateViewController(withIdentifier: "LikersViewController") as! LikersViewController
            likersVc.mediaId = md.pId
            likersVc.likerMediaType = .gallery
            likersVc.screenType = .dislikersList
            
            self.navigationController?.pushViewController(likersVc, animated: true)
        }
        else{
            let alert = UIAlertController(title: "Alert", message: "No dislikes exist.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    //MARK:- Comment Action
    @IBAction func btnCommentAction(_ sender: AnyObject){
        
        //get media obj from index
        let buttonPosition = sender.convert(CGPoint.zero, to: tblTimeline)
        let indexPath = tblTimeline.indexPathForRow(at: buttonPosition)! as IndexPath
        
        let obj = arrMedia[indexPath.section]
        universalDeleg!.moveToNextScreen("CommentVc", WithData: obj)
    }
    
    //MARK:- Share Action
    @IBAction func btnShareAction(_ sender: AnyObject){
        
        //get media obj from index
        let buttonPosition = sender.convert(CGPoint.zero, to: tblTimeline)
        let indexPath = tblTimeline.indexPathForRow(at: buttonPosition)! as IndexPath
        
        let obj = arrMedia[indexPath.section]
        
        //can't share this media if this is locked or belong to private user
        if obj.pIsLocked == 1 || obj.pPrivacyLevelId == 2
        {
            let alert = UIAlertController(title: "Alert", message: "Can not share this media", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        else  if obj.pIsLocked == 0 && obj.pPrivacyLevelId == 3
        {
            //can share this medis if this is unlocked and belong to public user
            
            //Redirect to Contact screen for forwarding purpose
            universalDeleg!.moveToNextScreen("ForwardMedia", WithData: obj)
 
        }
    }
    
    //MARK:- UserGallery Navigation Action
    @IBAction func btnUserGalleryNavigation(_ sender: AnyObject){
        
        //get media obj from index
        let btn = sender as! UIButton
        let md = arrMedia[btn.tag]

        universalDeleg!.moveToNextScreen("GalleryMainViewController", WithData: md)
    }
    
    //MARK:- Report Media Action
    @IBAction func btnReportMedia(_ sender: AnyObject){
        
        let btn = sender as! UIButton
        let md = arrMedia[btn.tag]
        
        //Create the AlertController
        let actionSheetController: UIAlertController = UIAlertController(title: alertMsg_SelectCategory, message: "", preferredStyle: .actionSheet)
        
        //Create and add the Cancel action
        let cancelAction: UIAlertAction = UIAlertAction(title: alertBtnTitle_Cancel, style: .cancel) { action -> Void in
            //Just dismiss the action sheet
        }
         actionSheetController.addAction(cancelAction)
        
        //Add report media button for other user's post
        if md.pUserId != Int(Utilities.getUserDetails().id)
        {
            //Create and add a fourth option action
            let report: UIAlertAction = UIAlertAction(title: alertMsg_Report, style: .destructive) { action -> Void in
                
                self.reportMediaRequest(md.pId, ownerId: md.pUserId)
                
            }
            actionSheetController.addAction(report)
        }
        
        
        self.mediaSharedId = md.pId
        self.mediaSharedOwnerId = md.pUserId
        
        //Create and add first option action
        let shareFacebook: UIAlertAction = UIAlertAction(title: alertMsg_ShareToFacebook, style: .default) { action -> Void in
            self.shareLinkToFacebook(md.pSlug)
           
        }
        actionSheetController.addAction(shareFacebook)
       
        
        //Create and add a second option action
        let tweet: UIAlertAction = UIAlertAction(title: alertMsg_Tweet, style: .default) { action -> Void in
            self.tweetLinkOnTwitter(md.pSlug)

        }
        actionSheetController.addAction(tweet)
        
        //Create and add a third option action
        let copyShareURL: UIAlertAction = UIAlertAction(title: alertMsg_CopyShareURL, style: .default) { action -> Void in
            self.copyLink(md.pSlug)
        }
        actionSheetController.addAction(copyShareURL)
        
        
        //Present the AlertController
        self.present(actionSheetController, animated: true, completion: nil)
    }
    
    // MARK: - Share/Tweet/Copy Link
    func shareLinkToFacebook(_ link:String) {
        self.shareSocialMediaLinkOnFB(link)
//        let facebookURL: NSURL = NSURL(string: "fb://")!
//        if UIApplication.sharedApplication().canOpenURL(facebookURL) {
            /*if FBSDKAccessToken.currentAccessToken() != nil {
                self.shareSocialMediaLinkOnFB(link)
            }
            else {
                let facebookLogin = FBSDKLoginManager()
                facebookLogin.logInWithReadPermissions(["email"],  fromViewController: self) { (facebookResult: FBSDKLoginManagerLoginResult!, facebookError: NSError!) -> Void in
                    if !facebookResult.isCancelled {
                        self.shareSocialMediaLinkOnFB(link)
                    }
                }
            }*/
        /*}
        else {
            let alert:UIAlertController = UIAlertController(title: "", message: alertMsg_DownloadFacebookToShareLink, preferredStyle: .Alert)
            let alertAction = UIAlertAction(title: alertBtnTitle_OK, style: .Default, handler: { (UIAlertAction) -> Void in
                // open app store.
               self.openFacebookOnAppStore()
            })
            
            alert.addAction(alertAction)
            self.presentViewController(alert, animated: true, completion: nil)
        }*/
    }
    
    func tweetLinkOnTwitter(_ link:String) {
        let composer = TWTRComposer()
        composer.setURL(URL(string: link))
        
        // Called from a UIViewController
        composer.show(from: self) { result in
            if (result == TWTRComposerResult.cancelled) {
                printCustom("Tweet composition cancelled")
            }
            else {
                printCustom("Sending tweet!")
                self.mediaShared(self.mediaSharedId, mediaOwnerId: self.mediaSharedOwnerId, platform: AccountType.twitter)
            }
        }
    }
    
    func copyLink(_ link:String) {
        UIPasteboard.general.string = link
    }
    
    //MARK: - Share On FB/ Open AppStore
    func shareSocialMediaLinkOnFB(_ link:String) {
        let url:URL = URL(string: link)!
        let facebookShareDialog: FBSDKShareDialog = Utilities().getShareDialogWithContentURL(url)
        if facebookShareDialog.canShow() {
            FBSDKShareDialog.show(from: self.parent, with: Utilities().getShareLinkContentWithContentURL(url), delegate: self)
        }
        else {
            self.showCommonAlert(alertMsg_InvalidFBShareableLink)
        }
    }
    
    /*func openFacebookOnAppStore() {
        // open app store.
        let facebookAppStoreURL = "https://itunes.apple.com/in/app/facebook/id284882215?mt=8"
        let appStoreURL:NSURL = NSURL(string: facebookAppStoreURL)!
        if UIApplication.sharedApplication().canOpenURL(appStoreURL) {
            UIApplication.sharedApplication().openURL(appStoreURL)
        }
        else {
            self.showCommonAlert(alertMsg_DownloadFacebookToSendAppInvites)
        }
    }*/
  
    // MARK: - FBSDKSharing Delegate
    func sharer(_ sharer: FBSDKSharing!, didCompleteWithResults results: [AnyHashable: Any]!) {
        printCustom("didCompleteWithResults results:\(results)")
        if results.count > 0 {
            self.mediaShared(self.mediaSharedId, mediaOwnerId: self.mediaSharedOwnerId, platform: AccountType.facebook)
        }
    }
    
    func sharer(_ sharer: FBSDKSharing!, didFailWithError error: Error!) {
        //printCustom("didFailWithError error:\(error.description)")
    }
    
    func sharerDidCancel(_ sharer: FBSDKSharing!) {
        printCustom("sharerDidCancel")
    }
    
    // MARK: - Media Shared
    func mediaShared(_ mediaId:Int, mediaOwnerId:Int, platform:AccountType){
        var parameters = [String: AnyObject]()
        parameters["media_id"] = mediaId as AnyObject
        parameters["sent_to"] = mediaOwnerId as AnyObject
        parameters["sent_by"] = Utilities.getUserDetails().id as AnyObject
        parameters["shared_on"] = platform.rawValue as AnyObject

        APIManager.sharedInstance.shareMedia(parameters, Target: self)
    }
    
    // MARK: - Media Shared Response
    func mediaSharedResponse (_ notify: Foundation.Notification) {
        //NSNotificationCenter.defaultCenter().removeObserver(self, name: NOTIFICATION_SHAREMEDIA, object: nil)
                
        let swiftyJsonVar = JSON(notify.object!)
        
        let message = swiftyJsonVar["message"].stringValue
        let status = swiftyJsonVar["status"].intValue
        let response = swiftyJsonVar["response"].stringValue

        if swiftyJsonVar [ERROR_KEY].exists() {
            printCustom("response:\(swiftyJsonVar[ERROR_KEY].stringValue)")
        }
        else if (status == 0) //API message response
        {
            printCustom("response:\(message)")
        }
        else if (status == 1) //success response
        {
            printCustom("response:\(response)")
        }
    }
    
    
    // MARK: - Like Media
    func likeMediaRequest(_ mediaId:Int, likerId:Int){
        //ApplicationDelegate.showLoader(loaderTitle_LikeMedia)
        var parameters = [String: AnyObject]()
        
        //User Id(Owner of the feed)
        parameters["user_id"] = likerId as AnyObject
        
        //mediaID
        parameters["media_id"] = mediaId as AnyObject
        
        //User Id who wants to like/dislike a feed
        parameters["liker_id"] = Utilities.getUserDetails().id as AnyObject

        //1-like, 2-dislike
        parameters["like_status"] = 1 as AnyObject
        
        APIManager.sharedInstance.requestLike(parameters, Target: self)
    }
    
    // MARK: - Like Media Response
    func likeMediaResponse (_ notify: Foundation.Notification)
    {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFIACTION_LIKE), object: nil)
        
        //ApplicationDelegate.hideLoader ()
        
        let swiftyJsonVar = JSON(notify.object!)
        
        let response = swiftyJsonVar["response"]
        
        let message = swiftyJsonVar["message"].stringValue
        let status = swiftyJsonVar["status"].intValue
        
        if swiftyJsonVar [ERROR_KEY].exists()
        {
            self.showCommonAlert(swiftyJsonVar[ERROR_KEY].stringValue)
        }
        else if (status == 0) //API message response
        {
            self.showCommonAlert(message)
        }
        else if (status == 1) //success response
        {
            
        }
    }
    
    // MARK: - Dislike Media
    func dislikeMediaRequest(_ mediaId:Int, likerId:Int){
        //ApplicationDelegate.showLoader(loaderTitle_DislikeMedia)
        var parameters = [String: AnyObject]()
        
        //User Id(Owner of the feed)
        parameters["user_id"] = likerId as AnyObject
        
        ////mediaID
        parameters["media_id"] = mediaId as AnyObject
        
        //User Id who wants to like/dislike a feed
        parameters["liker_id"] = Utilities.getUserDetails().id as AnyObject
        
        //1-like, 2-dislike
        parameters["like_status"] = 2 as AnyObject
        
        print(parameters)
        
        APIManager.sharedInstance.requestDislike(parameters, Target: self)
    }
    
    // MARK: - Dislike Media Response
    func dislikeMediaResponse (_ notify: Foundation.Notification)
    {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFIACTION_DISLIKE), object: nil)
        
        //ApplicationDelegate.hideLoader ()
        
        let swiftyJsonVar = JSON(notify.object!)
        
        let response = swiftyJsonVar["response"]
        
        let message = swiftyJsonVar["message"].stringValue
        let status = swiftyJsonVar["status"].intValue
        
        if swiftyJsonVar [ERROR_KEY].exists()
        {
            self.showCommonAlert(swiftyJsonVar[ERROR_KEY].stringValue)
        }
        else if (status == 0) //API message response
        {
            self.showCommonAlert(message)
        }
        else if (status == 1) //success response
        {

        }
    }
    
    
    // MARK: - Report Media
    func reportMediaRequest(_ mediaId:Int, ownerId:Int){
        
        //ApplicationDelegate.showLoader(loaderTitle_ReportinMedia)
        var parameters = [String: AnyObject]()
        parameters["user_id"] = Utilities.getUserDetails().id as AnyObject
        parameters["media_id"] = mediaId as AnyObject
        parameters["owner_id"] = ownerId as AnyObject
        
        APIManager.sharedInstance.reportMedia(parameters, Target: self)
    }
    
    // MARK: - Report Media Response
    func reportMediaResponse (_ notify: Foundation.Notification)
    {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFIACTION_REPORTMEDIA), object: nil)
        
        //ApplicationDelegate.hideLoader ()
        
        let swiftyJsonVar = JSON(notify.object!)
        
        let message = swiftyJsonVar["message"].stringValue
        let status = swiftyJsonVar["status"].intValue
        
        if swiftyJsonVar [ERROR_KEY].exists()
        {
            self.showCommonAlert(swiftyJsonVar[ERROR_KEY].stringValue)
        }
        else if (status == 0) //API message response
        {
            self.showCommonAlert(message)
        }
        else if (status == 1) //success response
        {
            let alert = UIAlertController(title: "", message: swiftyJsonVar["response"].stringValue, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler:nil))
            self.present(alert, animated: true, completion: nil)
        }
    }

    func stopPreviousPlayedVideo(_ indexP:IndexPath)
    {
        if (indexPathlastPlayed != nil && indexPathlastPlayed != indexP) {
            
            let cellSel = self.tblTimeline.cellForRow(at: self.indexPathlastPlayed!) as! HomeTabMediaCell
            cellSel.stopVideo()
            
            indexPathlastPlayed = nil;
        }
    }
    
    // MARK: - Table view data source

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        if (section + 1  == arrMedia.count && section + 1 == Limit) {
            
            return 0.0
        }
        
        return 80.0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?{
        
        if (section + 1  == arrMedia.count && section + 1 == Limit) {
        
            let view = UIView.init(frame: CGRect.zero)
            return view
        }
        else
        {
            let cell = self.tblTimeline.dequeueReusableCell(withIdentifier: "headerCell")! as UITableViewCell
            
            let media = arrMedia[section]
            
            if let imgProfile = cell.viewWithTag(111) as? UIImageView
            {
                //Loading User profile image
                imgProfile.image = Utilities().themedImage(img_defaultUserBig)
                imgProfile.contentMode = .scaleAspectFit
                imgProfile.layer.masksToBounds = false
                imgProfile.layer.cornerRadius = imgProfile.frame.height/2
                imgProfile.clipsToBounds = true
                
                if media.pUserImage.isEmpty == true
                {
                    imgProfile.image = UIImage.init(named: img_defaultUserBig)
                }
                if let img = imageCache[media.pUserImage] {
                    imgProfile.image = img
                }
                else if imageCache[media.pUserImage] == nil && media.pUserImage != ""
                {
                    //Downloading user's image
                    URLSession.shared.dataTask(with: URL(string: media.pUserImage)!, completionHandler: { (data, response, error) -> Void in
                        
                        if error != nil {
                            
                            imgProfile.image = Utilities().themedImage(img_defaultUserBig)
                            
                            print(error)
                            return
                        }
                        
                        DispatchQueue.main.async(execute: { () -> Void in
                            let image = UIImage(data: data!)
                            
                            if (image == nil)
                            {
                                imgProfile.image = Utilities().themedImage(img_defaultUserBig)
                            }
                            else
                            {
                                imgProfile.image = image
                                self.imageCache[media.pUserImage] = imgProfile.image
                            }
                        })
                    }).resume()
                }
            }
            
            //username
            
            
            if let lblUsername = cell.viewWithTag(112) as? UILabel
            {
                lblUsername.font = ProximaNovaRegular(15)
                
                //username
                var username = ""
                
                //if user is anonymous
                if media.pPrivacyLevelId == 4
                {
                    username = anonymousUsername + String(media.pUserId)
                }
                else
                {
                    username = media.pUserName
                }
                
                lblUsername.text = username
            }
            
            
            //creation date
            
            if let lblPostedDate = cell.viewWithTag(113) as? UILabel
            {
                lblPostedDate.font = ProximaNovaRegular(14)
                if media.pCreatedDate.isEmpty == false
                {
                    lblPostedDate.text = Utilities().getDateAgo(media.pCreatedDate)
                }
                else
                {
                    lblPostedDate.text = "NA"
                }
            }
            
            //report media
            
            if let btnReport = cell.viewWithTag(114) as? UIButton
            {
                btnReport.addTarget(self, action: #selector(btnReportMedia), for: .touchUpInside)
                btnReport.tag = section
            }
            
            //user image button
            
            if let btnUserImage = cell.viewWithTag(120) as? UIButton
            {
                btnUserImage.addTarget(self, action: #selector(btnUserGalleryNavigation), for: .touchUpInside)
                btnUserImage.tag = section
            }
            
            return cell.contentView
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int{
        
        return arrMedia.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath){
        
        if ((self.indexPathlastPlayed) != nil) {
            
            let cell:HomeTabMediaCell = self.tblTimeline.dequeueReusableCell(withIdentifier: "HomeMediaCell") as! HomeTabMediaCell
            
            if cell.cellTag != -1
            {
                cell.stopVideo()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        //Load More
        if (arrMedia.count >= Limit) && (indexPath.section + 1  == arrMedia.count) {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "LoadMoreCell")
            
            if let lbl5176 =  cell?.viewWithTag(5176) as? UILabel
            {
                lbl5176.text = loading_Title
                
                if (totalRecordCount) > (page) * Limit {
                    
                    offset = (page) * Limit
                    page += 1
                    
                    universalDeleg?.loadMoreMedia((page), offset:(offset), obj: self)
                }
                else
                {
                    lbl5176.text = loadingNoMoreData
                    
                    lbl5176.isHidden = true
                    
                    return refreshCellForRow(indexPath)
                }
            }
            
            return cell!
        }
        
        else{
            
            return refreshCellForRow(indexPath)
            
        }
    }
    
    func refreshCellForRow(_ indexPath:IndexPath)->HomeTabMediaCell
    {
        
        let cell:HomeTabMediaCell = self.tblTimeline.dequeueReusableCell(withIdentifier: "HomeMediaCell") as! HomeTabMediaCell
        
        let media = arrMedia[indexPath.section]
        
        
        //play button enable when a photo has an video or audio attachment
        if media.pMediaAttachmentType == 3 || media.pMediaAttachmentType == 2
        {
            cell.btnPlay.isHidden = false
            cell.btnPlay.setImage(Utilities().themedImage(img_videoPlay), for: UIControlState())
        }
        else
        {
            cell.btnPlay.isHidden = true
        }
        
        
        //Loading Media image
        
        if media.pIsLocked == 1
        {
            var localPath = ""
            
            //if media is sent media
            if String(media.pUserId) == Utilities.getUserDetails().id
            {
                localPath = Utilities().getLocalMediaPath(media.pMediaFile, mediaAttachmentType: 0, folderName:localSentFolderName)
            }
                
                //if media is received media
            else
            {
                localPath = Utilities().getLocalMediaPath(media.pMediaFile, mediaAttachmentType: 0, folderName: localReceivedFolderName)
            }
            
            if localPath.characters.count > 0
            {
                let img = UIImage.init(contentsOfFile: localPath)
                
                if img != nil
                {
                    cell.imgMedia.image = img
                }
                else
                {
                    cell.btnPlay.isHidden = true
                    cell.imgMedia.image = Utilities().themedImage(img_unlockMedia)
                }
            }
            else
            {
                cell.btnPlay.isHidden = true
                cell.imgMedia.image = Utilities().themedImage(img_unlockMedia)
            }
        }
        else
        {
            cell.imgMedia.image = Utilities().themedImage(img_movieImage)
            cell.imgMedia!.contentMode = .scaleAspectFit
            
            if let img = imageCache[media.pMediaFile] {
                cell.imgMedia!.image = img
            }
            else if imageCache[media.pMediaFile] == nil
            {
                //Downloading media image
                URLSession.shared.dataTask(with: URL(string: media.pMediaFile)!, completionHandler: { (data, response, error) -> Void in
                    
                    if error != nil {
                        
                        cell.imgMedia!.image = Utilities().themedImage(img_movieImage)
                        
                        print(error)
                        return
                    }
                    
                    DispatchQueue.main.async(execute: { () -> Void in
                        let image = UIImage(data: data!)
                        
                        if image == nil
                        {
                            cell.imgMedia!.image = Utilities().themedImage(img_movieImage)
                        }
                        else
                        {
                            cell.imgMedia!.image = image
                            self.imageCache[media.pMediaFile] = cell.imgMedia!.image
                        }
                    })
                }).resume()
            }
        }
        
        //media caption
        cell.lblMediaCaption.font = ProximaNovaRegular(15)
        if media.pMediaCaption.isEmpty == false
        {
            cell.lblMediaCaption.text = media.pMediaCaption
        }
        else
        {
            cell.lblMediaCaption.text = "NA"
        }
        
        
        //isliked
        if media.pIsLiked == 1
        {
            cell.btnLike.setImage(Utilities().themedImage(img_likeHighlighted), for: UIControlState())
        }
        else
        {
            cell.btnLike.setImage(Utilities().themedImage(img_liked), for: UIControlState())
        }
        
        //is dislike
        if media.pIsDisliked == 1
        {
            cell.btnDislike.setImage(Utilities().themedImage(img_dislikeHighlighted), for: UIControlState())
        }
        else
        {
            cell.btnDislike.setImage(Utilities().themedImage(img_disliked), for: UIControlState())
        }
        
        //comment icon highlighted
        
        if media.pCountComments > 0
        {
            cell.btnComments.setImage(Utilities().themedImage(img_commentHighlighted), for: UIControlState())
        }
        else
        {
            cell.btnComments.setImage(Utilities().themedImage(img_commentsIcon), for: UIControlState())
        }
        
        //comment count
        
        var strComments = ""
        if media.pCountComments == 1
        {
            strComments = "comment"
        }
        else
        {
            strComments = "comments"
        }
        
        cell.btnCommentCount.setTitle(String.init(format:"%d %@",media.pCountComments, strComments), for: UIControlState())
        
        //like count
        
        var strLikes = ""
        
        if media.pCountLikes == 1 || media.pCountLikes == 0
        {
            strLikes = "like"
        }
        else
        {
            strLikes = "likes"
        }
        
        cell.btnLikesCount.setTitle(String.init(format:"%d %@",media.pCountLikes, strLikes), for: UIControlState())
        
        
        //dislike count
        
        var strDislikes = ""
        
        if media.pCountDislikes == 1 || media.pCountDislikes == 0
        {
            strDislikes = "dislike"
        }
        else
        {
            strDislikes = "dislikes"
        }
        
        
        cell.btnDislikeCount.setTitle(String.init(format:"%d %@",media.pCountDislikes, strDislikes), for: UIControlState())
        
        
        //expiry time
        cell.lblExpiryTime.font = ProximaNovaRegular(14)
        if media.pExpiryDate.isEmpty == false
        {
            cell.lblExpiryTime.text = Utilities().getFutureDateAgo(media.pExpiryDate)
            
        }
        else
        {
            cell.lblExpiryTime.text = "NA"
        }
        
        //button tags
        cell.btnPlay.tag = indexPath.section
        cell.btnLike.tag = indexPath.section
        cell.btnDislike.tag = indexPath.section
        cell.btnComments.tag = indexPath.section
        cell.btnShare.tag = indexPath.section
        cell.btnLikesCount.tag = indexPath.section
        cell.btnDislikeCount.tag = indexPath.section
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let obj = arrMedia[indexPath.section]
        universalDeleg!.moveToNextScreen("SingleDetailVc", WithData: obj)
    }
    
    var heightForIndexPath = [IndexPath: CGFloat]()
    var averageRowHeight: CGFloat = 465 //your best estimate
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        heightForIndexPath[indexPath] = cell.frame.height
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return heightForIndexPath[indexPath] ?? averageRowHeight
    }
}
