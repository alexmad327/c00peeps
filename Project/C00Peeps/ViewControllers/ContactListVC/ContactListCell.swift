//
//  ContactListCell.swift
//  C00Peeps
//
//  Created by SOTSYS011 on 3/18/16.
//  Copyright © 2016 SOTSYS011. All rights reserved.
//

import UIKit

class ContactListCell: UITableViewCell
{

    @IBOutlet var lblName: UILabel!
    @IBOutlet weak var btnSelect: UIButton?
    @IBOutlet weak var widthConstraintBtnSelect: NSLayoutConstraint!
}
