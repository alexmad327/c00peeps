//
//  ContactListData.swift
//  C00Peeps
//
//  Created by Anubha on 26/07/16.
//  Copyright © 2016 SOTSYS011. All rights reserved.
//

import UIKit

class ContactListData {
    var id : String? = ""
    var userId : String? = ""
    var name : String? = ""
    var image : UIImage? = nil
    var phone : String? = ""
    var status : String? = ""
    var profileURL: Data?
    var selected : Bool? = false
    
    init(id : String? = "",userId : String? = "", name : String?, image : UIImage?, url: String, phone : String?,status : String? = "", selected : Bool?) {
        self.id = id
        self.userId = userId
        self.image = image
        self.name = name
        self.phone = phone
        self.status = status
        self.selected = selected
        let pictureURL = URL(string: url)
        if pictureURL != nil{
        profileURL = try? Data(contentsOf: pictureURL!)
    }
    
}
    init () {
        
    }
    
}
