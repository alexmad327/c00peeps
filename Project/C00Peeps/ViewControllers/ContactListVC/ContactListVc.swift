//
//  ContactListVc.swift
//  C00Peeps
//
//  Created by SOTSYS011 on 3/16/16.
//  Copyright © 2016 SOTSYS011. All rights reserved.
//

import UIKit
import ContactsUI
import AddressBook
import Contacts
import SwiftyJSON
import TwitterKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


@available(iOS 9.0, *)
class ContactListVc: BaseVC ,UITableViewDelegate,UITableViewDataSource
{
    @IBOutlet weak var tblSelection: UITableView!
    @IBOutlet weak var btnSelect: UIBarButtonItem!
    @IBOutlet weak var bottomConstraintTblSelection: NSLayoutConstraint!
    @IBOutlet weak var heightConstraintBtnInvite: NSLayoutConstraint!
    @IBOutlet weak var searchBarContacts: UISearchBar!
    @IBOutlet weak var btnInvite: UIButton!
    @IBOutlet weak var btnSkip: UIButton!
    
    let contactStore = CNContactStore()
    var contacts : Array<ContactListData>? = []
    var filteredContacts : Array<ContactListData>?
    var username:String!
    var addContactType : AddContactType?
    var sessionUserID:String?
    var imageCheckBox:UIImage!
    var imageCheckedCheckBox:UIImage!
    var isSearchOn:Bool = false
    var isComingFromSettings:Bool = false
    var syncType:SyncType?

    override func viewDidLoad()
    {
        super.viewDidLoad()
        switch addContactType!.rawValue {
        case 1:
            if self.isComingFromSettings {
                self.title = "Find Contacts"
            }
            else {
                self.title = "Phone Book"
            }
            ApplicationDelegate.showLoader(loaderTitle_FetchingContacts)
            
            DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async {
                self.fetchContacts()
            }
        case 3:
            self.title = "Twitter Friends"
            self.fetchTwitterFriends()
            
        default:
            printCustom("Facebook friends list can't be shown as facebook doesnot allow, facebook app invite feature integrated instead on previous screen")
        }
        
        // Update View According To Selected Theme
        self.updateViewAccordingToTheme()
        
        if self.isComingFromSettings {
            bottomConstraintTblSelection.constant = 0
            self.navigationItem.rightBarButtonItem = nil //Hide "Select All" Button
            self.btnInvite.isHidden = true
            self.btnSkip.isHidden = true
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // Add Observer for keyboard show/hide notifications.
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        // Remove Observers
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: - Update View According To Selected Theme
    func updateViewAccordingToTheme() {
        btnInvite.setBackgroundImage(Utilities().themedImage(img_invite), for: UIControlState())
        btnSkip.setBackgroundImage(Utilities().themedImage(img_skip), for: UIControlState())
        
        // Used in table view.
        imageCheckBox = Utilities().themedImage(img_radio)
        imageCheckedCheckBox = Utilities().themedImage(img_radioSelected)
    }
    
    // MARK: - Keyboard Show/Hide Notification
    
    func keyboardWillShow(_ notification: Foundation.Notification) {
        // Move table view up.
        let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
        bottomConstraintTblSelection.constant = keyboardSize!.size.height
        
        // Update constraints with animation.
        self.view.setNeedsUpdateConstraints()
        self.tblSelection.setNeedsUpdateConstraints()
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.tblSelection.layoutIfNeeded()
            self.view.layoutIfNeeded()
        })
        
        searchBarContacts.showsCancelButton = true
    }
    
    func keyboardWillHide(_ notification: Foundation.Notification) {
        // Move table view down.
        if self.isComingFromSettings {
            bottomConstraintTblSelection.constant = 0
        }
        else {
            bottomConstraintTblSelection.constant = heightConstraintBtnInvite.constant
        }
        
        // Update constraints with animation.
        self.view.setNeedsUpdateConstraints()
        self.tblSelection.setNeedsUpdateConstraints()
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.tblSelection.layoutIfNeeded()
            self.view.layoutIfNeeded()
        })
        
        searchBarContacts.showsCancelButton = false
    }
    
    //MARK: - UITableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        // Return the number of rows
        if isSearchOn {// Search ON
            // Return number of filtered contacts
            return filteredContacts!.count
        }
        else {// Search OFF
            return contacts!.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var contact:ContactListData!
        if isSearchOn {// Search ON
            contact = filteredContacts![indexPath.row]
        }
        else {// Search OFF
            contact = contacts![indexPath.row]
        }
        
        if addContactType!.rawValue == 1 {
            let cell : ContactListCell! = tblSelection.dequeueReusableCell(withIdentifier: "ContactListCell",for: indexPath) as! ContactListCell
            if contact.name == "" {
                cell.lblName.text = contact.phone
            }
            else {
                cell.lblName.text = contact.name
            }
            if isComingFromSettings {//Find Contacts
                printCustom("contact.status!:\(contact.status!)")
                switch contact.status! {
                case "Pending":
                    cell.btnSelect!.setImage(Utilities().themedImage(img_pending), for: UIControlState())
                    cell.btnSelect!.setImage(Utilities().themedImage(img_pendingSelected), for: .highlighted)

                case "Approved":
                    cell.btnSelect!.setImage(Utilities().themedImage(img_following), for: UIControlState())
                    cell.btnSelect!.setImage(Utilities().themedImage(img_followingSelected), for: .highlighted)

                default://Unknown
                    cell.btnSelect!.setImage(Utilities().themedImage(img_followSelected), for: UIControlState())
                    cell.btnSelect!.setImage(Utilities().themedImage(img_follow), for: .highlighted)
                }
               
                cell.widthConstraintBtnSelect.constant = 81
            }
            else {//Invite Friends - doesn't matter coming from settings or not
                if contact.selected == true {
                    cell.btnSelect!.setImage(imageCheckedCheckBox, for: UIControlState())
                }
                else {
                    cell.btnSelect!.setImage(imageCheckBox, for: UIControlState())
                }
            }
            
            return cell
        }
        else {
            let cell : SociaContactCell! = tblSelection.dequeueReusableCell(withIdentifier: "SociaCell",for: indexPath) as! SociaContactCell
            cell.profileImage?.image = UIImage(data: contact.profileURL!)
            cell.lblName?.text = contact.name
            cell.profileImage.layer.masksToBounds = false
            cell.profileImage.layer.cornerRadius = cell.profileImage.frame.height/2
            cell.profileImage.clipsToBounds = true
            
            if isComingFromSettings {//Follow People
                switch contact.status! {
                case "Pending":
                    cell.btnSelect!.setImage(Utilities().themedImage(img_pending), for: UIControlState())
                    cell.btnSelect!.setImage(Utilities().themedImage(img_pendingSelected), for: .highlighted)
                    
                case "Approved":
                    cell.btnSelect!.setImage(Utilities().themedImage(img_following), for: UIControlState())
                    cell.btnSelect!.setImage(Utilities().themedImage(img_followingSelected), for: .highlighted)
                    
                default://Unknown
                    cell.btnSelect!.setImage(Utilities().themedImage(img_followSelected), for: UIControlState())
                    cell.btnSelect!.setImage(Utilities().themedImage(img_follow), for: .highlighted)
                }

                cell.widthConstraintBtnSelect.constant = 81
            }
            else {//Invite Friends
                if contact.selected == true {
                    cell.btnSelect!.setImage(imageCheckedCheckBox, for: UIControlState())
                }
                else {
                    cell.btnSelect!.setImage(imageCheckBox, for: UIControlState())
                }
            }
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if !isComingFromSettings {
            var contact:ContactListData!
            if isSearchOn {// Search ON
                contact = filteredContacts![indexPath.row]
            }
            else {// Search OFF
                contact = contacts![indexPath.row]
            }
            if contact.selected == true {
                contact.selected = false
            }
            else {
                contact.selected = true
            }
            
            tblSelection.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if addContactType!.rawValue == 1 {
            return 44
        }
        else {
            return 73
        }
    }
    
    func animateTable() {
        tblSelection.reloadData()
        
        let cells = tblSelection.visibleCells
        let tableHeight: CGFloat = tblSelection.bounds.size.height
        
        for i in cells {
            let cell: UITableViewCell = i as UITableViewCell
            cell.transform = CGAffineTransform(translationX: 0, y: tableHeight)
        }
        
        var index = 0
        
        for a in cells {
            let cell: UITableViewCell = a as UITableViewCell
            UIView.animate(withDuration: 1.5, delay: 0.05 * Double(index), usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: [], animations: {
                cell.transform = CGAffineTransform(translationX: 0, y: 0);
                }, completion: nil)
            
            index += 1
        }
    }
    //MARK: - IBActions
    
    @IBAction func btnSkipClicked(_ sender: AnyObject) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSelectSingleContactClicked(_ sender: AnyObject) {
        let button = sender as! UIButton
        let view = button.superview!
        
        var indexPath: IndexPath?
        if addContactType?.rawValue == 1{
            let cell = view.superview as! ContactListCell
            indexPath = tblSelection.indexPath(for: cell)
        }
        else {
            let cell = view.superview as! SociaContactCell
            indexPath = tblSelection.indexPath(for: cell)
        }
        var contact:ContactListData!
        if self.filteredContacts?.count > 0 {// Search ON
            contact = filteredContacts![indexPath!.row]
        }
        else {// Search OFF
            contact = contacts![indexPath!.row]
        }
        if self.syncType == SyncType.notFollowingUsers {
            if reachability?.currentReachabilityStatus == .notReachable {
                self.showCommonAlert(alertMsg_NoInternetConnection)
            }
            else {
            //If user is not already followed
            switch contact.status! {
            case "Pending":
                //Create the AlertController
                let actionSheetController: UIAlertController = UIAlertController(title: "Follow Request", message: "", preferredStyle: .actionSheet)
                
                //Create and add the Cancel action
                let cancelAction: UIAlertAction = UIAlertAction(title: alertBtnTitle_Cancel, style: .cancel) { action -> Void in
                    //Just dismiss the action sheet
                }
                actionSheetController.addAction(cancelAction)
                
                //Create and add first option action
                let unfollowAction: UIAlertAction = UIAlertAction(title: "Delete Follow Request", style: .destructive) { action -> Void in
                    contact.status = "Unknown"
                    
                    //Refreshing tble cell
                    self.tblSelection.reloadRows(at: [indexPath!], with: UITableViewRowAnimation.fade)

                    //Calling API
                    self.unfollowPeople(Int(contact.userId!)!)
                }
                actionSheetController.addAction(unfollowAction)
                
                //Present the AlertController
                self.present(actionSheetController, animated: true, completion: nil)
                
            default://Unknown
                contact.status = "Pending"

                //Refreshing tble cell
                self.tblSelection.reloadRows(at: [indexPath!], with: UITableViewRowAnimation.fade)
                
                //Calling API
                self.followPeople(Int(contact.userId!)!)
            }
            }
        }
        else {
            if contact.selected == true {
                contact.selected = false
            }
            else {
                contact.selected = true
            }
            tblSelection.reloadRows(at: [IndexPath(item: indexPath!.row, section: 0)], with: UITableViewRowAnimation.none)
        }
    }
    
    @IBAction func btnSelectAll_Clciked(_ sender: AnyObject)
    {
        if btnSelect.title == "Select all"
        {
            self.selectAll()
        }
        else
        {
            self.unSelectAll()
        }
    }
    
    @IBAction func btnInvitationClick(_ sender: UIButton) {
        let selectedContacts = contacts?.filter({ ($0.selected == true)})
        if selectedContacts!.count > 0 {
            self.showAlertAskToSendInvite(selectedContacts)
        }
        else {
            var errorMsg = ""
            if self.syncType == SyncType.notPecXUsers {
                errorMsg = alertMsg_SelectUsersToInvite
            }
            else {
                errorMsg = alertMsg_SelectUsersToFollow
            }
            
            let alert:UIAlertController = UIAlertController(title: "", message: errorMsg, preferredStyle: .alert)
            let alertAction = UIAlertAction(title: alertBtnTitle_OK, style: .default, handler: nil)
            alert.addAction(alertAction)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    // MARK: - Search Bar Delegates
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        isSearchOn = false
        
        // Resign keyboard.
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        let text:NSString = searchBar.text! as NSString
        if text.length == 0 {//If no text searched, then empty filtered contacts to continue with contacts.
            self.filteredContacts = []
            isSearchOn = false
        }
        else {//Filter contacts with searched text.
            filterContactsForEnteredText(searchBar.text!)
            isSearchOn = true
        }
        self.tblSelection.reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        isSearchOn = false
        
        // On clicking cancel button on search bar, empty filtered contacts to continue with contacts.
        self.filteredContacts = []
        self.tblSelection.reloadData()
        
        // Clear search bar text and resign keyboard.
        searchBar.text = ""
        searchBar.resignFirstResponder()
    }
    
    
    // MARK: - Filter Contacts
    func filterContactsForEnteredText(_ searchText: String) {
        // Fetch contacts that match searched text content.
        self.filteredContacts = self.contacts!.filter({
            let contactName: NSString = $0.name! as NSString
            return (contactName.range(of: searchText, options: NSString.CompareOptions.caseInsensitive).location) != NSNotFound
        })
    }
    
    //MARK: - Select/Unselect all Contacts
    func selectAll() {
        self.filteredContacts = []
        for index in 0  ..< self.contacts!.count {
            let contact:ContactListData = self.contacts![index]
            contact.selected = true
        }
        btnSelect.title = "Unselect all"
        tblSelection.reloadData()
    }
    
    func unSelectAll() {
        self.filteredContacts = []
        for index in 0  ..< self.contacts!.count {
            let contact:ContactListData = self.contacts![index]
            contact.selected = false
        }
        btnSelect.title = "Select all"
        tblSelection.reloadData()
    }
    
    //MARK: - Fetch Phonebook Contacts/Facebook Friends/Twitter Friends
    func fetchContacts() {
        DispatchQueue.main.async {
            self.contacts = []
            
            let toFetch = [CNContactGivenNameKey, CNContactImageDataKey, CNContactFamilyNameKey, CNContactImageDataAvailableKey, CNContactPhoneNumbersKey]
            let request = CNContactFetchRequest(keysToFetch: toFetch as [CNKeyDescriptor])
            
            do{
                try self.contactStore.enumerateContacts(with: request) {
                    contact, stop in
                    
                    var userPhoneNumber:CNPhoneNumber!
                    if contact.phoneNumbers.count > 0 {
                        userPhoneNumber = contact.phoneNumbers[0].value 
                    }
                  
                    if userPhoneNumber != nil {
                        let strPhoneNo = userPhoneNumber.value(forKey: "digits") as? String
                        let data = ContactListData(id:"", name: contact.givenName, image: nil, url: "", phone : Utilities().truncatePhoneNo(strPhoneNo!), selected: false)
                        self.contacts!.append(data)
                    }
                }
                
                DispatchQueue.main.async {
                    self.initiateCallSyncAPI()
                }
                
            } catch let err{
                printCustom("err:\(err)")
                DispatchQueue.main.async {
                    ApplicationDelegate.hideLoader()
                }
            }
        }
    }
    
    func fetchTwitterFriends() {
        ApplicationDelegate.showLoader(loaderTitle_FetchingFriends)
        
        let client = TWTRAPIClient()
        let friendListEndpoint = FETCH_TWITTER_FOLLOWERS_URL
        let params = ["user_id": sessionUserID!]
        var clientError : NSError?
        
        let request = client.urlRequest(withMethod: "GET", urlString: friendListEndpoint, parameters: params, error: &clientError)
        
        client.sendTwitterRequest(request) { (response, data, connectionError) -> Void in
            if connectionError != nil {
                let error = connectionError! as NSError
                printCustom("Error: \(error)")
                DispatchQueue.main.async {
                    ApplicationDelegate.hideLoader ()
                    self.showCommonAlert(error.description)
                }
            }
            else {
                do {
                    let json = try JSONSerialization.jsonObject(with: data!, options: []) as! [String: Any]
                    printCustom("json: \(json)")
                    printCustom("json[users]: \(json["users"])")

                    
                    if let users = json["users"] as? NSArray {
                        printCustom("json[users`]: \(json["users"])")

                        for user in users {
                            let userDict = user as! [String: Any]
                            let follower = ContactListData(id: userDict["id_str"] as? String, name: userDict["name"] as? String, image: nil, url: userDict["profile_image_url"] as! String, phone: "", selected: false)//TwitterFollower(name: user["name"] as! String, url: user["profile_image_url"] as! String)
                            self.contacts?.append(follower)
                        }
                    }
                    DispatchQueue.main.async {
                        ApplicationDelegate.hideLoader ()
                        self.initiateCallSyncAPI()
                        //self.tblSelection.reloadData()
                        //                        if self.contacts!.count != 0 {
                        //                            self.animateTable()
                        //                        }
                    }
                } catch let jsonError as NSError {
                    printCustom("json error: \(jsonError.localizedDescription)")
                }
                
            }
        }
    }
    
    // MARK: - Send Invite to Contacts/Twitter Friends
    func sendInviteToSelectedContacts () {
        self.callAPISendInviteToContacts()
    }
    
    func sendInviteToSelectedTwitterFriends () {
        let selectedContacts = contacts?.filter({ ($0.selected == true)})
        
        ApplicationDelegate.showLoader(loaderTitle_SendingInvitation)
        
        for contactIndex in 0...selectedContacts!.count - 1 {
            let client = TWTRAPIClient(userID: sessionUserID)
            let directMsgEndpoint = SEND_DIRECT_MSG_TO_TWITTER_FOLLOWER_URL
            let params = ["user_id": selectedContacts![contactIndex].id!, "text" : socialMediaText + " " + socialMediaLink]
            var clientError : NSError?
            
            let request = client.urlRequest(withMethod: "POST", urlString: directMsgEndpoint, parameters: params, error: &clientError)
            
            client.sendTwitterRequest(request) { (response, data, connectionError) -> Void in
                if connectionError != nil {
                    let error = connectionError as! NSError
                    printCustom("Error: \(error)")
                    DispatchQueue.main.async {
                        ApplicationDelegate.hideLoader ()
                        self.showCommonAlert(error.description)
                    }
                }
                else {
                    do {
                        let json = try JSONSerialization.jsonObject(with: data!, options: []) as! [String: Any]
                        printCustom("json: \(json)")
                        let recipient = json["recipient"] as! [String: Any]
                        printCustom("json id_str: \(recipient["id_str"])")
                        let lastContact = selectedContacts![selectedContacts!.count - 1]
                        if lastContact.id! == recipient["id_str"] as! String {
                            DispatchQueue.main.async {
                                ApplicationDelegate.hideLoader ()
                                self.showInvitesSentAlert(alertMsg_InvitesSent)
                            }
                        }
                    } catch let jsonError as NSError {
                        printCustom("json error: \(jsonError.localizedDescription)")
                    }
                }
            }
        }
    }
    
    // MARK: - Show Alert
    func showAlertAskToSendInvite(_ selectedContacts: Array<ContactListData>?) {
        let strMsg : String!
        if selectedContacts!.count == self.contacts!.count {
            strMsg = alertMsg_AskSendInviteToAll
        }
        else {
            strMsg = String(format: alertMsg_AskSendInviteToSelected, selectedContacts!.count)
        }
        
        let alert = UIAlertController(title: "", message: strMsg, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: alertBtnTitle_Yes, style: UIAlertActionStyle.default, handler: { (UIAlertAction) -> Void in
            if self.addContactType?.rawValue == 1 {
                self.sendInviteToSelectedContacts()
            }
            else {
                self.sendInviteToSelectedTwitterFriends()
            }
        }))
        alert.addAction(UIAlertAction(title: alertBtnTitle_Cancel, style: UIAlertActionStyle.cancel, handler:nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK: - Call API
    func initiateCallSyncAPI() {
        if self.contacts?.count > 0 {
            if self.syncType == SyncType.notFollowingUsers {
                //http://coopeeps.trantorinc.com/coopeeps/users/checknotfollowingpecx
                self.callAPISyncNotFollowingUsers(self.addContactType!)
            }
            else if self.syncType == SyncType.notPecXUsers {
                //http://coopeeps.trantorinc.com/coopeeps/users/synctwitter
                //http://coopeeps.trantorinc.com/coopeeps/users/syncphone
                self.callAPISyncNotPecXUsers()
            }
        }
        else {
            ApplicationDelegate.hideLoader()
            
            var errorMsg = ""
            if self.syncType == SyncType.notPecXUsers {
                errorMsg = alertMsg_NoUsersToInvite
            }
            else {
                errorMsg = alertMsg_NoUsersToFollow
            }
            
            let alert:UIAlertController = UIAlertController(title: "", message: errorMsg, preferredStyle: .alert)
            let alertAction = UIAlertAction(title: alertBtnTitle_OK, style: .default, handler: { (UIAlertAction) -> Void in
                self.navigationController?.popViewController(animated: true)
            })
            alert.addAction(alertAction)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func callAPISyncNotPecXUsers() {
        ApplicationDelegate.showLoader(loaderTitle_Syncing)
        var parameters = [String: AnyObject]()
        if addContactType == AddContactType.twitter {
            let arrContacts:[String] = self.contacts!.map { $0.id! }
            parameters["twitter_ids"] = arrContacts.joined(separator: ",") as AnyObject
        }
        if addContactType == AddContactType.phoneBook {
            let arrContacts:[String] = self.contacts!.map { $0.phone! }
            parameters["phone"] = arrContacts.joined(separator: ",") as AnyObject
        }
        
        APIManager.sharedInstance.fetchNotRegisteredPecXUsers(parameters, Target: self)
    }
    
    func callAPISyncNotFollowingUsers(_ type:AddContactType) {
        ApplicationDelegate.showLoader(loaderTitle_Syncing)
        var parameters = [String: AnyObject]()
        var arrContacts:[String] = []
        parameters["user_id"] = Utilities.getUserDetails().id as AnyObject
        if type == AddContactType.twitter {
            parameters["type"] = 1 as AnyObject
            arrContacts = self.contacts!.map { $0.id! }
        }
        else {
            parameters["type"] = 2 as AnyObject
            arrContacts = self.contacts!.map { $0.phone! }
            
        }
        parameters["id"] = arrContacts.joined(separator: ",") as AnyObject
        
        APIManager.sharedInstance.fetchNotFollowingPecXUsers(parameters, Target: self)
    }
    
    func callAPISendInviteToContacts() {
        ApplicationDelegate.showLoader(loaderTitle_SendingInvitation)
        var parameters = [String: AnyObject]()
        let selectedContacts: Array<ContactListData> = (contacts?.filter({ ($0.selected == true)}))!
        let arrContacts:[String] = selectedContacts.map { $0.phone! }
            
        parameters["phone"] = arrContacts.joined(separator: ",") as AnyObject
        
        APIManager.sharedInstance.sendInviteToContacts(parameters, Target: self)
    }
    
    func followPeople(_ userId:Int){
        
        //{1=Approved, 2=Pending, 3=Blocked, 4=Unknown}
            var parameters = [String: AnyObject]()
            parameters["requester_id"] = Utilities.getUserDetails().id as AnyObject
            parameters["responder_id"] = userId as AnyObject
            parameters["status_id"] = 2 as AnyObject //Pending Status
            
            print(parameters)
            
            APIManager.sharedInstance.requestFollow(parameters, Target: self)
    }
    
    func unfollowPeople(_ userId:Int){
            var parameters = [String: AnyObject]()
            parameters["requester_id"] = Utilities.getUserDetails().id as AnyObject
            parameters["responder_id"] = userId as AnyObject
            
            APIManager.sharedInstance.requestUnFollow(parameters, Target: self)
    }
    
    //MARK:- API Response
    func responseFetchNotRegisteredPecXUsers(_ notify: Foundation.Notification) {
        ApplicationDelegate.hideLoader()
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFICATION_FETCHNOTREGISTEREDPECXUSERS), object: nil)
        
        let swiftyJsonVar = JSON(notify.object!)
        let message = swiftyJsonVar["message"].stringValue
        let status = swiftyJsonVar["status"].intValue
        
        if swiftyJsonVar [ERROR_KEY].exists() {
            self.showCommonAlert(swiftyJsonVar[ERROR_KEY].stringValue)
        }
        else if (status == 1) {
            let responsePhone = swiftyJsonVar["response"]["phone"]
            let responseTwitterIds = swiftyJsonVar["response"]["twitter_ids"]
            if responsePhone != nil {
                printCustom("response:\(responsePhone.stringValue)")

                //Remove phone contacts which are registered with PecX
                let arrPhones = responsePhone.stringValue.components(separatedBy: ",")
                self.contacts! = self.contacts!.filter { arrPhones.contains($0.phone!) }
            }
            if responseTwitterIds != nil {
                printCustom("response:\(responseTwitterIds.stringValue)")

                //Remove twitter contacts which are registered with PecX
                let arrTwitterIds = responseTwitterIds.stringValue.components(separatedBy: ",")
                self.contacts! = self.contacts!.filter { arrTwitterIds.contains($0.id!) }
            }
            DispatchQueue.main.async {

            self.animateTable()
            }
        }
        else if (status == 0) { // Error response
            let responseErrorMessage = swiftyJsonVar["response"].stringValue
            self.showCommonAlert(responseErrorMessage)
        }
        else {
            self.showCommonAlert(message)
        }
    }
    
    func responseFetchNotFollowingPecXUsers(_ notify: Foundation.Notification) {
        ApplicationDelegate.hideLoader()
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFICATION_FETCHNOTFOLLOWINGPECXUSERS), object: nil)
        
        let swiftyJsonVar = JSON(notify.object!)
        let message = swiftyJsonVar["message"].stringValue
        let status = swiftyJsonVar["status"].intValue
        
        if swiftyJsonVar [ERROR_KEY].exists() {
            self.showCommonAlert(swiftyJsonVar[ERROR_KEY].stringValue)
        }
        else if (status == 1) {
            /*
             id =         (
             {
             "data_id" = 5555228243;
             "user_id" = 154;
             }
             );

             */
            let responseIds = swiftyJsonVar["response"]["id"].arrayObject
            let responseType = swiftyJsonVar["response"]["type"]
            let responseIdsStr = swiftyJsonVar["response"]["id_string"].stringValue
            
           // let responseDataIds:[String:String] = responseIds["data_id"] as! [String:String]

            if responseIds != nil {
                let arrIds = responseIdsStr.components(separatedBy: ",")
                if responseType == 1 {//Twitter
                    //Remove twitter friends which are following PecX
                    self.contacts! = self.contacts!.filter { arrIds.contains($0.id!) }
                }
                else {//Phone Book
                    //Remove phone contacts which are following PecX
                    self.contacts! = self.contacts!.filter { arrIds.contains($0.phone!) }
                }
                
                if responseIds!.count > 0 {
                    for count in 0...responseIds!.count - 1 {
                        let id:[String:String] = responseIds![count] as! [String:String]
                        printCustom("id dict:\(id)")
                        let responseDataId = id["data_id"]
                        let responseUserId = id["user_id"]
                        let responseUserStatus = id["status"]
                        
                        for contact in self.contacts! {
                            printCustom("responseDataId.stringValue:\(responseDataId)")
                            
                            var idToCompare = ""
                            if responseType == 1 {
                                idToCompare = contact.id!
                            }
                            else {
                                idToCompare = contact.phone!
                            }
                            printCustom("contact.idToCompare:\(idToCompare)")

                            if responseDataId == idToCompare {
                                contact.userId = responseUserId
                                contact.status = responseUserStatus
                                
                                printCustom("contact.userId:\(contact.userId)")
                                printCustom("contact.status:\(contact.status)")
                            }
                        }
                    }
                }
            }
            DispatchQueue.main.async {
            self.animateTable()
            }
        }
        else if (status == 0) { // Error response
            let responseErrorMessage = swiftyJsonVar["response"].stringValue
            self.showCommonAlert(responseErrorMessage)
        }
        else {
            self.showCommonAlert(message)
        }
    }
    
    func responseSendInviteToContacts(_ notify: Foundation.Notification) {
        ApplicationDelegate.hideLoader()
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFICATION_SENDINVITETOCONTACTS), object: nil)
        
        let swiftyJsonVar = JSON(notify.object!)
        let message = swiftyJsonVar["message"].stringValue
        let status = swiftyJsonVar["status"].intValue
        
        if swiftyJsonVar [ERROR_KEY].exists() {
            self.showCommonAlert(swiftyJsonVar[ERROR_KEY].stringValue)
        }
        else if (status == 1) {
            let response = swiftyJsonVar["response"]
            DispatchQueue.main.async {
                ApplicationDelegate.hideLoader ()
                self.showInvitesSentAlert(response.stringValue)
            }
        }
        else if (status == 0) { // Error response
            let responseErrorMessage = swiftyJsonVar["response"].stringValue
            self.showCommonAlert(responseErrorMessage)
        }
        else {
            self.showCommonAlert(message)
        }
    }
    
    func responseFollowPeople (_ notify: Foundation.Notification)
    {
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFIACTION_FOLLOWPEOPLE), object: nil)
        
        let swiftyJsonVar = JSON(notify.object!)
        
        let message = swiftyJsonVar["message"].stringValue
        let status = swiftyJsonVar["status"].intValue
        
        if swiftyJsonVar [ERROR_KEY].exists()
        {
            self.showCommonAlert(swiftyJsonVar[ERROR_KEY].stringValue)
        }
        else if (status == 1) //success response
        {
            //List item status already changed before hitting api.
        }
        else if (status == 0) //error response
        {
            let responseErrorMessage = swiftyJsonVar["response"].stringValue
            self.showCommonAlert(responseErrorMessage)
        }
        else
        {
            self.showCommonAlert(message)
        }
    }
    
    func responseUnFollowPeople (_ notify: Foundation.Notification)
    {
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFIACTION_UNFOLLOWPEOPLE), object: nil)
        
        let swiftyJsonVar = JSON(notify.object!)
        
        let message = swiftyJsonVar["message"].stringValue
        let status = swiftyJsonVar["status"].intValue
        
        if swiftyJsonVar [ERROR_KEY].exists()
        {
            self.showCommonAlert(swiftyJsonVar[ERROR_KEY].stringValue)
        }
        else if (status == 1) //success response
        {
           //List item status already changed before hitting api.
        }
        else if (status == 0) //error response
        {
            let responseErrorMessage = swiftyJsonVar["response"].stringValue
            self.showCommonAlert(responseErrorMessage)
        }
        else
        {
            self.showCommonAlert(message)
        }
    }

    
    // MARK: - Show Alert
    func showInvitesSentAlert(_ msg:String) {
        let alert:UIAlertController = UIAlertController(title: "", message: msg, preferredStyle: .alert)
        let alertAction = UIAlertAction(title: alertBtnTitle_OK, style: .default, handler: { (UIAlertAction) -> Void in
            self.navigationController?.popViewController(animated: true)
        })
        alert.addAction(alertAction)
        self.present(alert, animated: true, completion: nil)
    }
    
}

