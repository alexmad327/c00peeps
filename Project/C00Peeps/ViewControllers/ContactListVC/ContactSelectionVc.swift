//
//  ContactSelectionVc.swift
//  C00Peeps
//
//  Created by SOTSYS011 on 3/16/16.
//  Copyright © 2016 SOTSYS011. All rights reserved.
//

import UIKit
import ContactsUI
import AddressBook
import Contacts

class ContactSelectionVc: BaseVC {
    let contactStore = CNContactStore()

    @IBOutlet weak var imgVwBg: UIImageView!
    @IBOutlet weak var imgVwLogo: UIImageView!
    @IBOutlet weak var btnPhoneBook: UIButton!
    @IBOutlet weak var btnSocialMedia: UIButton!
    var isComingFromSettings:Bool = false
    var syncType:SyncType?

    override func viewDidLoad()
    {
        super.viewDidLoad()
        

        if self.isComingFromSettings {
            self.title = "Invite Friends"
        }
        else {
            self.title = "Add Contacts"
        }

        self.navigationController?.isNavigationBarHidden = false

        if self.isComingFromSettings {
            self.navigationItem.rightBarButtonItem = nil
        }
        else {
            self.navigationItem.setHidesBackButton(true, animated: false)

            let userId: String? = Utilities.getUserDetails().id
            if userId == nil || userId == "" {
                Utilities.setSignUpState(SignUpState.inviteFriends)
            }
        }
        
        // Update View According To Selected Theme
        self.updateViewAccordingToTheme()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Hide tab bar for this controller.
        self.tabBarController?.tabBar.isHidden = true
    }
    
    // MARK: - Update View According To Selected Theme
    func updateViewAccordingToTheme() {
        imgVwBg.image = Utilities().themedImage(img_bg)
        imgVwLogo.image = Utilities().themedImage(img_logo)
       
        btnPhoneBook.setImage(Utilities().themedImage(img_phonebook), for: UIControlState())
        btnPhoneBook.setImage(Utilities().themedImage(img_phonebookSelected), for: .highlighted)
        
        btnSocialMedia.setImage(Utilities().themedImage(img_socialIcon), for: UIControlState())
        btnSocialMedia.setImage(Utilities().themedImage(img_socialIconSelected), for: .highlighted)
    }
        
    // MARK: - Button Actions
    @IBAction func btnSkipClicked(_ sender: AnyObject) {
        
        checkAccountActivatedFromAddContacts = true
        
        if Utilities.getUserDetails().social_account_type == "0" // Normal Signup
        {
            self.callAPI_SignIn(Utilities.getUserDetails().username, password:Utilities.getUserDetails().password)
        }
        else
        {
            if Utilities.getUserDetails().social_account_type == "1" //Facebook
            {
                self.callAPI_CheckSocialUserExists(AccountType.facebook, socialMediaID: Utilities.getUserDetails().social_unique_id)
            }
            else if Utilities.getUserDetails().social_account_type == "2" //Twitter
            {
                self.callAPI_CheckSocialUserExists(AccountType.twitter, socialMediaID: Utilities.getUserDetails().social_unique_id)
            }
            else if Utilities.getUserDetails().social_account_type == "3" //Instagram
            {
                self.callAPI_CheckSocialUserExists(AccountType.instagram, socialMediaID: Utilities.getUserDetails().social_unique_id)
            }
        }
    }
    
    @IBAction func btnContact_Clciked(_ sender: AnyObject)
    {
        checkIfUserAccessGranted()
    }
    
    
    func checkIfUserAccessGranted()
    {
        requestForAccess { (accessGranted) -> Void in
            if accessGranted {
                 DispatchQueue.main.async(execute: { () -> Void in
                let controller = self.storyboard?.instantiateViewController(withIdentifier: "ContactListVc") as? ContactListVc
                controller!.addContactType = AddContactType.phoneBook
                controller!.syncType = self.syncType
                self.navigationController?.pushViewController(controller!, animated: true)
                })
                
            }else{
            }
        }
    }
    
    func requestForAccess(_ completionHandler: @escaping (_ accessGranted: Bool) -> Void) {
            let authorizationStatus = CNContactStore.authorizationStatus(for: CNEntityType.contacts)
            switch authorizationStatus {
            case .authorized:
                completionHandler(true)
                
            case .denied, .notDetermined:
                self.contactStore.requestAccess(for: CNEntityType.contacts, completionHandler: { (access, accessError) -> Void in
                        if access {
                            completionHandler(access)
                        }
                        else {
                            if authorizationStatus == CNAuthorizationStatus.denied {
                                DispatchQueue.main.async(execute: { () -> Void in
                                    let message = "\(accessError!.localizedDescription)\n\n" + alertMsg_AllowAppToAccessContacts
                                    self.showCommonAlert(message)
                                })
                            }
                        }
                    })
                
                
            default:
                completionHandler(false)
            }
    }
    
    @IBAction func btnSocial_Clicked(_ sender: AnyObject)
    {
        let ContactListObject = self.storyboard?.instantiateViewController(withIdentifier: "SocialMediaVc") as? SocialMediaVc
        ContactListObject?.isComingFromSettings = isComingFromSettings
        ContactListObject!.syncType = self.syncType
        self.navigationController?.pushViewController(ContactListObject!, animated: true)
    }
    @IBAction func btnBack_Clciked(_ sender: AnyObject)
    {
     self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnNext_Clicked(_ sender: AnyObject) {
//        let ContactListObject = self.storyboard?.instantiateViewControllerWithIdentifier("ContactListVc") as? ContactListVc
//        self.navigationController?.pushViewController(ContactListObject!, animated: true)
    }
}
