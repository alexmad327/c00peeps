
import UIKit
import SwiftyJSON

class BaseVC: UIViewController
{

    var btnBack = UIButton(type: UIButtonType.system) as UIButton
    var btnProfile = UIButton(type: UIButtonType.system) as UIButton
    var btnNotification = UIButton(type: UIButtonType.system) as UIButton
    
    var lblTitle = UILabel()
    var headerView = UIView()
    var topImage = UIImageView()
    var eventBase = Event()

    //declare this property where it won't go out of scope relative to your listener
    var reachability: Reachability?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        
        /* Commented by Anubha
        This will be implemented if we use NavigationBar at place of custom View
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        self.navigationController?.navigationBar.barTintColor = ColorWithRGB (86, green: 253, blue: 233,alpha: 1.0)
        self.navigationController?.navigationBar.backgroundColor = ColorWithRGB (86, green: 253, blue: 233,alpha: 1.0)
        let color : UIColor =  ColorWithRGB (68, green: 151, blue: 213,alpha: 1.0)
        let titleFont : UIFont = UIFont(name: "ProximaNova-Semibold",size: 22)!
       
        let attributes = [
            NSForegroundColorAttributeName : color,
            NSFontAttributeName : titleFont
        ]
        self.navigationController?.navigationBar.titleTextAttributes = attributes
        */
       
        /*
         Commented by Arshdeep
         self.navigationController?.navigationBarHidden = true
        self.createHeader()
        */
        // --- Added by Arshdeep
       
        // Remove "Back" text from back button.
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.plain, target: self, action: #selector(BaseVC.backButtonAction(_:)))
        
        // Remove separator between navigation bar and view.
        
        /*
        Commented by Gagandeep on 30 Sep 2016, as getting crash on unniversalviewcontroller
 
        for parent in self.navigationController!.navigationBar.subviews {
            for childView in parent.subviews {
                if(childView is UIImageView) {
                    childView.removeFromSuperview()
                }
            }
        }*/
                
        checkNetworkConnection()
        
    }
  
    override func viewWillDisappear(_ animated: Bool) {
        
        super.viewWillDisappear(animated)
        reachability!.stopNotifier()
        NotificationCenter.default.removeObserver(self,
                                                            name: ReachabilityChangedNotification,
                                                            object: reachability)
    }
    
    func showAlertPaymentSuccessful(_ msg:String) {
        let alert:UIAlertController = UIAlertController(title: "", message: msg, preferredStyle: .alert)
        let alertAction = UIAlertAction(title: alertBtnTitle_OK, style: .default, handler: { (UIAlertAction) -> Void in
            
            if self.eventBase.isSurveyAttached == 1
            {
                //self.moveBackToNotifications()
                
                //self.navigationController?.popToRootViewControllerAnimated(true)
                
                //If survey attached then move to Survey Detail screen
                
                let st = UIStoryboard(name: "Events", bundle:nil)
                let surveyDetailVc = st.instantiateViewController(withIdentifier: "SurveyDetailVc") as! SurveyDetailVc
                
                surveyDetailVc.event = self.eventBase
                surveyDetailVc.isComingFromPaymentSuccess = true
                surveyDetailVc.isShowSurvey = true
                self.navigationController?.pushViewController(surveyDetailVc, animated: true)
            }
            else
            {
                // If event media type is Video, then show video in survey detail screen by hiding survey button.
                if self.eventBase.mediaType == 3 {
                    //If survey attached then move to Survey Detail screen
                    let st = UIStoryboard(name: "Events", bundle:nil)
                    let surveyDetailVc = st.instantiateViewController(withIdentifier: "SurveyDetailVc") as! SurveyDetailVc
                    
                    surveyDetailVc.event = self.eventBase
                    surveyDetailVc.isComingFromPaymentSuccess = true
                    surveyDetailVc.isShowSurvey = false
                    self.navigationController?.pushViewController(surveyDetailVc, animated: true)
                }
                else {
                    // move back to home.
                    self.navigationController?.popToRootViewController(animated: true)
                }
            }
        })
        alert.addAction(alertAction)
        self.present(alert, animated: true, completion: nil)
        
    }
    
    
    // MARK: - Change textfield placeholder color
    func changeTextfieldPlaceholderColor(_ textField:UITextField, placeholderText:String, color:UIColor) {
        let strPlaceholder = NSAttributedString(string: placeholderText, attributes: [kCTForegroundColorAttributeName as NSAttributedStringKey:color])
        textField.attributedPlaceholder = strPlaceholder
    }
    
    func changeButtonColor(_ button:UIButton, title:String, color:UIColor) {
        let strTitle = NSAttributedString(string: title, attributes: [kCTForegroundColorAttributeName as NSAttributedStringKey:color])
        button.setAttributedTitle(strTitle, for: UIControlState())
    }
    
    // MARK: - Check Network Connection
    func checkNetworkConnection(){
        
        //declare this inside of viewWillAppear
        
        reachability = Reachability()
        
        NotificationCenter.default.addObserver(self, selector: #selector(BaseVC.reachabilityChanged(_:)), name: ReachabilityChangedNotification, object: reachability)
        do{
            try reachability?.startNotifier()
        }catch{
            printCustom("could not start reachability notifier")
        }
    }
    
    @objc func reachabilityChanged(_ note: Foundation.Notification) {
        
        let reachability = note.object as! Reachability
        if reachability.isReachable {
            if reachability.isReachableViaWiFi {
                printCustom("Reachable via WiFi")
            } else {
                printCustom("Reachable via Cellular")
            }
        } else {
            printCustom("Network not reachable")
        }
    }
    
    
    // MARK: - Show Alert
    func showCommonAlert(_ msg:String) {
        let alert:UIAlertController = UIAlertController(title: "", message: msg, preferredStyle: .alert)
        let alertAction = UIAlertAction(title: alertBtnTitle_OK, style: .default, handler: { (UIAlertAction) -> Void in
            if checkAccountActivatedFromAddContacts {
                checkAccountActivatedFromAddContacts = false
                // ---- Move back to initial screen ----
                self.navigationController?.popToRootViewController(animated: true)
            }
        })
        alert.addAction(alertAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func showAlertActivateAccount() {
        let alert:UIAlertController = UIAlertController(title: "", message: alertMsg_ActivateAccountToLogin, preferredStyle: .alert)
        let alertAction = UIAlertAction(title: alertBtnTitle_OK, style: .default, handler: { (UIAlertAction) -> Void in
            // ---- Move back to login ----
            // Check if login screeen exists in navigation hierarchy.
            for viewController in self.navigationController!.viewControllers {
                if viewController.isKind(of: SigninVc.self) {
                    // Move back to login screen.
                    self.navigationController?.popToViewController(viewController, animated: true)
                    break
                }
            }
        })
        alert.addAction(alertAction)
        self.present(alert, animated: true, completion: nil)

    }
    
    func showAlertPaymentSuccessful(msg:String) {
        let alert:UIAlertController = UIAlertController(title: "", message: msg, preferredStyle: .alert)
        let alertAction = UIAlertAction(title: alertBtnTitle_OK, style: .default, handler: { (UIAlertAction) -> Void in

            if self.eventBase.isSurveyAttached == 1
            {
                //self.moveBackToNotifications()
                
                //self.navigationController?.popToRootViewControllerAnimated(true)
                
                //If survey attached then move to Survey Detail screen
                
                let st = UIStoryboard(name: "Events", bundle:nil)
                let surveyDetailVc = st.instantiateViewController(withIdentifier: "SurveyDetailVc") as! SurveyDetailVc
                
                surveyDetailVc.event = self.eventBase
                surveyDetailVc.isComingFromPaymentSuccess = true
                self.navigationController?.pushViewController(surveyDetailVc, animated: true)
            }
            else
            {
                // move back to home.
                self.navigationController?.popToRootViewController(animated: true)
            }
        })
        alert.addAction(alertAction)
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func showDodoMessage(_ error:String, messageType:MessageType){
        let alert:UIAlertController = UIAlertController(title: "", message: error, preferredStyle: .alert)
         var alertAction:UIAlertAction!
        if error == "Account Created Successfully" {
            alertAction = UIAlertAction(title: "OK", style: .default, handler: { (UIAlertAction) -> Void in
                self.navigationController?.popViewController(animated: true)
            })
        }
        else {
            alertAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        }
        alert.addAction(alertAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func getHeader()->UIView{
        return headerView
    }
    
    @objc func backButtonAction(_ sender:UIButton!)
    {
        if let navController = self.navigationController {
            navController.popViewController(animated: true)
        }
    }
    
    func hideBackButton()
    {
        for vi in self.headerView.subviews
        {
            if vi.isKind(of: UIButton.self)
            {
                let btn = vi as! UIButton
                
                if btn.tag == 10
                {
                    btn.removeFromSuperview()
                }
            }
        }
    }
    
    func hideProfileNotificationButtons()
    {
        for vi in self.headerView.subviews
        {
            if vi.isKind(of: UIButton.self)
            {
                let btn = vi as! UIButton
                
                if btn.tag == 11 || btn.tag == 12
                {
                    btn.removeFromSuperview()
                }
            }
        }
    }
    
    func btnNotificationClick(_ sender: AnyObject)
    {
        let controller1 : NotificationViewController = UIStoryboard(name: "Notification", bundle: nil).instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
        self.navigationController?.pushViewController(controller1, animated: true)
    }
       
    
    // MARK: - Call API
    func callAPI_SignIn(_ username:String, password:String) {
        ApplicationDelegate.showLoader(loaderTitle_SigningIn)
        var parameters = [String: AnyObject]()
        parameters["user_name"] = username as AnyObject
        parameters["password"] = password as AnyObject
        parameters["device_token"] = Utilities.getDeviceToken() as AnyObject//"67c5aaa7de122bc5e8a2726c081be79204faede76829e617b625c1"
        parameters["device_type"] = "iPhone" as AnyObject
        parameters["public_key"] = "publicKey" as AnyObject
        APIManager.sharedInstance.requestLogin(parameters,Target:self)
    }
    
    func callAPI_CheckSocialUserExists(_ accountType: AccountType, socialMediaID: String) {
        var parameters = [String: AnyObject]()
        
        parameters["social_account_type"] = accountType.rawValue as AnyObject
        if accountType.rawValue != 0 {
            parameters["social_unique_id"] = socialMediaID as AnyObject
        }
        
        printCustom("check social user exists parameters:\(parameters)")
        
        ApplicationDelegate.showLoader(loaderTitle_SigningIn)
        APIManager.sharedInstance.requestCheckSocialUserExists(parameters, Target: self)
    }
    
    func callLogout() {
        
        ApplicationDelegate.showLoader(loaderTitle_LoggingOut)
        
        var parameters = [String: AnyObject]()
        //Utilities.getDeviceToken()
        parameters["device_token"] = Utilities.getDeviceToken() as AnyObject//"67c5aaa7de122bc5e8a2726c081be79204faede76829e617b625c1"
        parameters["device_type"] = "iPhone" as AnyObject
        parameters["user_id"] = Utilities.getUserDetails().id as AnyObject
        APIManager.sharedInstance.requestLogout(parameters, Target: self)
    }
    
   
    func processPayment(_ id:Int, amount:String, nonce:String, token:String, storeInVault:String) {
        ApplicationDelegate.showLoader(loaderTitle_ProcessingPayment)
        
        var parameters = [String: AnyObject]()
        parameters["user_id"] = Utilities.getUserDetails().id as AnyObject
        parameters["event_id"] = id as AnyObject
        parameters["amount"] = amount as AnyObject
        parameters["nonce"] = nonce as AnyObject
        parameters["storeInVault"] = storeInVault as AnyObject
        parameters["token"] = token as AnyObject
        APIManager.sharedInstance.requestProcessBraintreePayment(parameters, Target: self)
    }
   
    //MARK: - API Response
    func responseSignIn (_ notify: Foundation.Notification)
    {
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFIACTION_LOGIN), object: nil)
        
        ApplicationDelegate.hideLoader ()
        let swiftyJsonVar = JSON(notify.object!)
        let message = swiftyJsonVar["message"].stringValue
        let status = swiftyJsonVar["status"].intValue
        
        if (swiftyJsonVar [ERROR_KEY] != nil) {
            self.showCommonAlert(swiftyJsonVar[ERROR_KEY].stringValue)
        }
        else if (status == 1) {
            let response = swiftyJsonVar["response"]
            printCustom("login response:\(response)")
            printCustom("login basic id:\(response["basic"]["id"])")
            
            let responseUserDetail:[String:AnyObject] = response.dictionaryObject! as [String : AnyObject]
                let userDetail:UserDetail = UserDetail()
                Parser.parseUserDetails(responseUserDetail, userDetail: userDetail)
                
                UINavigationBar.appearance().setBackgroundImage(Utilities().themedImage(img_header), for: .default)
                
                let st = UIStoryboard(name: "Main", bundle:nil)
                let homeScreen = st.instantiateViewController(withIdentifier: "tabBarcontroller") as! UITabBarController
                let nav = UINavigationController.init(rootViewController: homeScreen)
                nav.isNavigationBarHidden = true
                ApplicationDelegate.window?.rootViewController = nav
        }
        else if (status == 0 || status == -1) {//-1 = 'Your account is not activated yet. Please activate the account from your registered email.'
            let responseErrorMessage = swiftyJsonVar["response"].stringValue
            self.showCommonAlert(responseErrorMessage)
        }
        else {
            self.showCommonAlert(message)
        }
    }
    
    
    func responseCheckSocialUserExists (_ notify: Foundation.Notification) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFICATION_CHECKSOCIALUSEREXISTS), object: nil)
        
        ApplicationDelegate.hideLoader ()
        
        let swiftyJsonVar = JSON(notify.object!)
        let message = swiftyJsonVar["message"].stringValue
        let status = swiftyJsonVar["status"].intValue
        
        if swiftyJsonVar [ERROR_KEY].exists() {
            self.showCommonAlert(swiftyJsonVar[ERROR_KEY].stringValue)
        }
        else if (status == 1) {
            let response = swiftyJsonVar["response"]
            let responseUserDetail:[String:AnyObject] = response.dictionaryObject! as [String : AnyObject]
            let userDetail:UserDetail = UserDetail()
            Parser.parseUserDetails(responseUserDetail, userDetail: userDetail)
            
            self.navigationController?.isNavigationBarHidden = true
            let homeScreen = self.storyboard?.instantiateViewController(withIdentifier: "tabBarcontroller") as? UITabBarController
            self.navigationController?.pushViewController(homeScreen!, animated: true)
        }
        else if (status == 0) {
            let responseErrorMessage = swiftyJsonVar["response"].stringValue
            self.showCommonAlert(responseErrorMessage)
        }
        else if status == -1 {//'Your account is not activated yet. Please activate the account from your registered email.'
            let responseErrorMessage = swiftyJsonVar["response"].stringValue
            self.showCommonAlert(responseErrorMessage)
        }
        else if status == -2 {//'Seems like you are not a PECx user. Please Sign up with your Social Account first.'
            self.navigateToSignUpScreen()
        }
        else {
            self.showCommonAlert(message)
        }
    }
    
    func responseLogOut (_ notify: Foundation.Notification) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFIACTION_LOGOUT), object: nil)
        ApplicationDelegate.hideLoader ()
        let swiftyJsonVar = JSON(notify.object!)
        let message = swiftyJsonVar["message"].stringValue
        let status = swiftyJsonVar["status"].intValue
        
        if (swiftyJsonVar [ERROR_KEY] != nil) {
            self.showCommonAlert(swiftyJsonVar[ERROR_KEY].stringValue)
        }
        else if (status == 1) {
            let currentThemeId = Utilities.getUserDetails().theme_id
            
            // Remove old user data except theme.
            Utilities.removeUserDetails()
            
            // Save theme.
            let userDetail:UserDetail = UserDetail()
            userDetail.theme_id = currentThemeId
            Utilities.setUserDetails(userDetail)
            
            //Unlink all social accounts.
            Utilities().linkSocialAccount(false, accountType: AccountType.all)
            
            //Move back to login.
            self.moveToLogin()
        }
        else if (status == 0) {
            let responseErrorMessage = swiftyJsonVar["response"].stringValue
            self.showCommonAlert(responseErrorMessage)
        }
        else {
            self.showCommonAlert(message)
        }
    }
   
    

    func navigateToSignUpScreen() {
        printCustom("base vc navigateToSignUpScreen")
    }
    
    //MARK: - API Response
    func responseProcessPayment(_ notify: Foundation.Notification) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFICATION_PROCESSBRAINTREEPAYMENT), object: nil)
        
        ApplicationDelegate.hideLoader ()
        let swiftyJsonVar = JSON(notify.object!)
        let message = swiftyJsonVar["message"].stringValue
        let status = swiftyJsonVar["status"].intValue
        
        if swiftyJsonVar [ERROR_KEY].exists() {
            self.showCommonAlert(swiftyJsonVar[ERROR_KEY].stringValue)
        }
        else if (status == 1) {
            printCustom("response:\(swiftyJsonVar["response"])")
            /*
             {
             "error_code" = 0;
             message = Success;
             response = "Payment Successful";
             status = 1;
             }
             */
            
            self.showAlertPaymentSuccessful(swiftyJsonVar["response"].stringValue)
        }
        else if (status == 0) { // Error response
            /*
             81703 - Credit card type is not accepted by this merchant account. (This card type is not supported. Please enter another card number.)
             81707 - CVV must be 4 digits for American Express and 3 digits for other card types. (correct)
             81715 - Credit card number is invalid.(correct) - The credit card number must pass a Luhn-10 check.
             81716 - Credit card number must be 12-19 digits.(correct)
             81717 - Credit card number is not an accepted test number.(correct)
             81724 - Duplicate card exists in the vault. - (This card already exists in your saved cards. Please select this card and initiate payment.)
             91726 - Credit card type is not accepted by this merchant account.(This card type is not supported. Please enter another card number.)
             91734 - Credit card type is not accepted by this merchant account.(This card type is not supported. Please enter another card number.)
             81736 - CVV verification failed. - CVV was incorrect or not supplied.(CVV is incorrect.)
             81737 - Postal code verification failed. - Postal code was incorrect or not supplied.(Postal code is incorrect.)
             {
             "error_code" = 81715;
             message = Error;
             response = "payment_method_nonce does not contain a valid payment instrument type.\nCredit card number is invalid.";
             status = 0;
             }
             */
            
            var responseErrorMessage = swiftyJsonVar["response"].stringValue
            let responseErrorCode = swiftyJsonVar["error_code"].stringValue
            if responseErrorCode != "0" {
                switch responseErrorCode {
                case "81715":
                    responseErrorMessage = alertMsg_InvalidCardNo
                case "81724":
                    responseErrorMessage = alertMsg_CardAlreadyExists
                case "81736":
                    responseErrorMessage = alertMsg_IncorrectCVV
                case "81737":
                    responseErrorMessage = alertMsg_IncorrectPostalCode
                default:
                    if responseErrorCode == "81703" || responseErrorCode == "91726" || responseErrorCode == "91734" {
                        responseErrorMessage = alertMsg_UnsupportedCardType
                    }
                    else {
                        //No change in error message
                    }
                }
            }
            self.showCommonAlert(responseErrorMessage)
        }
        else {
            self.showCommonAlert(message)
        }
    }
    
    //MARK: - Navigate
    func moveToLogin() {
        // Check if login screeen exists in navigation hierarchy.
        var loginViewExists = false
        for viewController in self.navigationController!.viewControllers {
            if viewController.isKind(of: SigninVc.self) {
                // Move back to login screen.
                self.navigationController?.popToViewController(viewController, animated: true)
                loginViewExists = true
                break
            }
        }
        
        if !loginViewExists {
            // Set Initial Screen as root.
            let st = UIStoryboard(name: "Main", bundle:nil)
            let initialVc = st.instantiateViewController(withIdentifier: "InitialVc") as! InitialVc
            let nav = UINavigationController.init(rootViewController: initialVc)
            nav.isNavigationBarHidden = false
            ApplicationDelegate.window?.rootViewController = nav
        }
    }
    
    func moveBackToNotifications() {
        for viewController in self.navigationController!.viewControllers {
            if viewController.isKind(of: NotificationViewController.self) {
                // Move back to notifications screen.
                self.navigationController?.popToViewController(viewController, animated: true)
                break
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
