//
//  CustomizeScreenVc.swift
//  C00Peeps
//
//  Created by SOTSYS011 on 3/12/16.
//  Copyright © 2016 SOTSYS011. All rights reserved.
//

import UIKit
import SwiftyJSON
import Zip

let CellSpace:CGFloat = 30.0
class CustomizeScreenVc: BaseVC, UICollectionViewDataSource, UICollectionViewDelegate {
    
    @IBOutlet weak var collectionVw: UICollectionView!
    @IBOutlet weak var imgVwHeader: UIImageView!
    
    var isComingFromSettings:Bool = false
    var selectedThemeId: String = "1"
    let reuseIdentifier = "ThemeCell" // also enter this string as the cell identifier in the storyboard
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        if self.isComingFromSettings {
            self.navigationItem.rightBarButtonItem = nil
            self.title = "Change Screen Colors"
        }
        else {
            self.title = "Customize Theme"
            self.navigationItem.setHidesBackButton(true, animated: false)
        }
       
        let userId: String? = Utilities.getUserDetails().id
        if userId == nil || userId == "" {
            Utilities.setSignUpState(SignUpState.setTheme)
        }
        
        // Update View According To Selected Theme
        self.updateViewAccordingToTheme()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Hide tab bar for this controller.
        self.tabBarController?.tabBar.isHidden = true
    }

    // MARK: - Update View According To Selected Theme
    func updateViewAccordingToTheme() {
        imgVwHeader.image = Utilities().themedImage(img_headerCustomize)
    }
    
    // MARK: - Button Actions
    @IBAction func skipAction(){
        if currentThemeId != "1" {
            // Call API to update theme.
            self.updateTheme(currentThemeId)
        }
        else {
            self.navigateToContactSelection()
        }
    }
    
    // MARK: - UICollectionViewDataSource protocol
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return 6
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! ThemeCollectionViewCell
        cell.themeImage.image = UIImage(named: String(format: "%@%d%@", "Theme", indexPath.row+1, "_t1") )
        if currentThemeId == String(indexPath.row + 1) {
            cell.alpha = 0.6
            cell.isUserInteractionEnabled = false
        }
        else {
            cell.isUserInteractionEnabled = true
            cell.alpha = 1.0
        }
        cell.backgroundColor = UIColor.clear
        return cell
    }
    
    // MARK: - UICollectionViewDelegate protocol
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        // Download theme only if not available in documents directory
        
        if self.isThemeImagesFolderAlreadyExists() {
            self.initiateThemeChange(collectionView, indexPath: indexPath)
        }
        else {
            let alert:UIAlertController = UIAlertController(title: "", message: alertMsg_CustomizeScreenConfirmation, preferredStyle: .alert)
            let alertActionYes:UIAlertAction = UIAlertAction(title: "Yes", style: .default, handler: { (UIAlertAction) -> Void in
                self.initiateThemeChange(collectionView, indexPath: indexPath)
            })
            alert.addAction(alertActionYes)
            
            let alertActionNo:UIAlertAction = UIAlertAction(title: "No", style: .cancel, handler: nil)
            alert.addAction(alertActionNo)
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                               sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        let cellSize:CGSize = CGSize(width: (collectionView.frame.size.width)/3, height: (collectionView.frame.size.width)/3)
        return cellSize
    }
    
    //MARK: - Initiate Theme Change
    func initiateThemeChange(_ collectionView: UICollectionView, indexPath: IndexPath) {
        let _selectedAssetArray = collectionView.visibleCells
        for i in 0..._selectedAssetArray.count - 1 {
            
            let index:IndexPath = IndexPath.init(row: i, section: 0)
            let cell = collectionView.cellForItem(at: index)!
            cell.isUserInteractionEnabled = true
            cell.alpha = 1.0;
            
        }
        let newCell = collectionView.cellForItem(at: indexPath)!
        newCell.isUserInteractionEnabled = false
        newCell.alpha = 0.6;
        
        // Call API to update theme.
        self.updateTheme(String (indexPath.row+1))
    }
    
    //MARK: - Check if theme images folder already exists
    func isThemeImagesFolderAlreadyExists() ->  Bool {
        // Download theme only if not available in documents directory
        let paths = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.allDomainsMask, true)
        let documentsDirectory:String = paths.first!
        //printCustom("documentsDirectory:\(documentsDirectory)")
        let themeName = "theme" + selectedThemeId
        let fileName = URL(fileURLWithPath:documentsDirectory).appendingPathComponent(themeName)
        //printCustom("fileName:\(fileName!.path!)")
        let fileManager = FileManager()
        if (fileManager.fileExists(atPath: "\(fileName.path)")) {
            printCustom("Already download theme.")
            return true
        }
        else {
            return false
        }
    }
    
    //MARK: - Save Zip File in Docs Directory
    func saveThemeZipFileInDocDir(_ data:Data?) {
        //save data to document directory
        let paths = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.allDomainsMask, true)
        let documentsDirectory:String = paths.first!
        printCustom("documentsDirectory:\(documentsDirectory)")
        let themeName = "theme" + selectedThemeId + ".zip"
        let fileName = URL(fileURLWithPath:documentsDirectory).appendingPathComponent(themeName)
        printCustom("fileName:\(fileName)")
        try? data!.write(to: fileName, options: [.atomic])
        
        self.unzipFileAndSaveToDocDir(fileName)
    }
    
    //MARK: - Unzip File And Save to Docs Directory
    func unzipFileAndSaveToDocDir(_ pathURL:URL) {
        do {
            try Zip.quickUnzipFile(pathURL)
            self.removeZipFileFromDocDir(pathURL)
         
        } catch {
            print("ERROR in unzipping file")
        }
    }
    
    //MARK: - Remove Zip File from Docs Directory
    func removeZipFileFromDocDir(_ pathURL:URL) {
        let fileManager = FileManager()
        do {
            try fileManager.removeItem(atPath: pathURL.path)
            self.navigateWithChangedTheme()
        }
        catch{
            printCustom("Could not delete file: \(error)")
        }
    }
    
    //MARK: - Call API
    func updateTheme(_ themeId: String){
        selectedThemeId = themeId
        ApplicationDelegate.showLoader(loaderTitle_SavingTheme)
        var parameters = [String: AnyObject]()
        parameters["theme_id"] = themeId as AnyObject
        parameters["user_id"] = Utilities.getUserDetails().id as AnyObject
        APIManager.sharedInstance.requestThemeType(parameters, Target: self)
    }
    
    func fetchThemeImagesFolderUrl(_ themeId: String){
        selectedThemeId = themeId
        ApplicationDelegate.showLoader(loaderTitle_SavingTheme)
        var parameters = [String: AnyObject]()
        parameters["theme_id"] = themeId as AnyObject
        APIManager.sharedInstance.requestFetchThemeImagesFolderUrl(parameters, Target: self)
    }
    
    func downloadThemeImagesFile(_ folderUrl:String) {
        ApplicationDelegate.showLoader(loaderTitle_DownloadingTheme)
        APIManager.sharedInstance.requestDownloadThemeImagesFile(folderUrl, Target: self)
    }
  
    //MARK: - API Response
    func responseThemeType (_ notify: Foundation.Notification)
    {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFIACTION_THEMETYPE), object: nil)
        ApplicationDelegate.hideLoader ()
        let swiftyJsonVar = JSON(notify.object!)
        let message = swiftyJsonVar["message"].stringValue
        let status = swiftyJsonVar["status"].intValue
        
        if swiftyJsonVar [ERROR_KEY].exists()
        {
            self.showCommonAlert(swiftyJsonVar[ERROR_KEY].stringValue)
        }
        else if (status == 1)
        {
           //self.saveUpdatedThemeId()
            
            // Theme changes testing pending
            if selectedThemeId != "1" {//Theme 1 images are already in bundle, so dont need to download them.
                // Fetch Updated Theme Images URL
                self.fetchThemeImagesFolderUrl(selectedThemeId)
            }
            else {
                self.navigateWithChangedTheme()
            }
        }
        else if (status == 0) //error response
        {
            let responseErrorMessage = swiftyJsonVar["response"].stringValue
            self.showCommonAlert(responseErrorMessage)
        }
        else {
            self.showCommonAlert(message)
        }
    }
    
    func saveUpdatedThemeId() {
        // Update Theme ID.
        let userDetail:UserDetail = Utilities.getUserDetails()
        userDetail.theme_id = selectedThemeId
        Utilities.setUserDetails(userDetail)
        printCustom("updated user details theme_id: \(Utilities.getUserDetails().theme_id)")
    }
    
    func responseFetchThemeImagesFolderUrl (_ notify: Foundation.Notification) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFICATION_FETCHTHEMEIMAGESFOLDERURL), object: nil)
        ApplicationDelegate.hideLoader ()
        let swiftyJsonVar = JSON(notify.object!)
        let message = swiftyJsonVar["message"].stringValue
        let status = swiftyJsonVar["status"].intValue
        
        if swiftyJsonVar [ERROR_KEY].exists()
        {
            self.showCommonAlert(swiftyJsonVar[ERROR_KEY].stringValue)
        }
        else if (status == 1)
        {
            let folderUrl = swiftyJsonVar["response"]["folder"].stringValue
            printCustom("folderUrl:\(folderUrl)")
            
            // Download theme only if not available in documents directory
            if self.isThemeImagesFolderAlreadyExists() {
                self.navigateWithChangedTheme()
            }
            else {
                self.downloadThemeImagesFile(folderUrl)
            }
        }
        else if (status == 0) //error response
        {
            let responseErrorMessage = swiftyJsonVar["response"].stringValue
            self.showCommonAlert(responseErrorMessage)
        }
        else {
            self.showCommonAlert(message)
        }
    }
    
    func responseDownloadThemeImagesFile (_ notify: Foundation.Notification) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFICATION_DOWNLOADTHEMEIMAGESFILE), object: nil)
        DispatchQueue.main.async {

        ApplicationDelegate.hideLoader ()
        let data:Data? = notify.object! as? Data
            if data != nil {
                self.saveThemeZipFileInDocDir(data)
            }
        }
    }
    
    //MARK: - Navigate
    func navigateWithChangedTheme() {
//        // Update View According To Selected Theme
//        self.updateViewAccordingToTheme()
        
        if self.isComingFromSettings {
            Utilities().setClearThemeDataSetting(false)

            let alert:UIAlertController = UIAlertController(title: "", message: alertMsg_ReLoginToUpdateTheme, preferredStyle: .alert)
            let alertAction = UIAlertAction(title: alertBtnTitle_Logout, style: .default, handler: { (UIAlertAction) -> Void in
                self.callLogout()
                self.saveUpdatedThemeId()
            })
            alert.addAction(alertAction)
            self.present(alert, animated: true, completion: nil)
            //self.navigationController?.popViewControllerAnimated(true)
        }
        else {
            self.saveUpdatedThemeId()
            
            UINavigationBar.appearance().setBackgroundImage(Utilities().themedImage(img_header), for: .default)
            
            self.navigateToContactSelection()
        }
    }
    
    func navigateToContactSelection() {
        if Utilities.getSignUpState() != SignUpState.home {
            Utilities.setSignUpState(SignUpState.inviteFriends)
        }
        
        let ContactSectionObject = self.storyboard?.instantiateViewController(withIdentifier: "ContactSelectionVc") as? ContactSelectionVc
        ContactSectionObject?.syncType = SyncType.notPecXUsers
        self.navigationController?.navigationBar.setBackgroundImage(Utilities().themedImage(img_header), for: .default)
        self.navigationController?.pushViewController(ContactSectionObject!, animated: true)
    }
  
}
