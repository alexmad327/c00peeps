//
//  ThemeCollectionViewCell.swift
//  C00Peeps
//
//  Created by Anubha on 19/07/16.
//  Copyright © 2016 SOTSYS011. All rights reserved.
//

import UIKit

class ThemeCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var themeImage: UIImageView!

}
