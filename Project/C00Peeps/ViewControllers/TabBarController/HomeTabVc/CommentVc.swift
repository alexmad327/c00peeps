//
//  CommentVc.swift
//  C00Peeps
//
//  Created by OSX on 15/07/16.
//  Copyright © 2016 SOTSYS011. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol SingleDetailToCommentProtocol {
    
    func updateCommentCount(_ commentCount:Int)
}

class CommentVc: BaseVC {
    
    var md  = Media()
    var arrComments = [Comment]()
    var offset = 0
    var page = 1
    var totalRecordCount = 0
    var imageCache = [String:UIImage]()
    var refreshControl: UIRefreshControl!
    
    var singleObject:SingleDetailToCommentProtocol?
    
    @IBOutlet weak var lblNoComments: UILabel!
    @IBOutlet weak var btnAddComment: UIButton!
    @IBOutlet var txtComments: UITextField!
    @IBOutlet weak var tblComments: UITableView!
    
    override func viewDidAppear(_ animated: Bool) {
    
        super.viewDidAppear(animated)
    
        tblComments.isHidden = true
        lblNoComments.isHidden = false
        lblNoComments.text = alertMsg_FetchingComments
        getCommentsData()
    }
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.navigationItem.title = "Comments"
        
        tblComments.rowHeight = UITableViewAutomaticDimension
        tblComments.estimatedRowHeight = 80
        //ADDED BY ANUBHA
        tblComments.setNeedsLayout()
        tblComments.layoutIfNeeded()
        addRefreshControl()
        
    }
    
    func addRefreshControl(){
        
        refreshControl = UIRefreshControl()
        refreshControl.tintColor = UIColor.darkGray
        refreshControl.attributedTitle = NSAttributedString(string: loaderTitle_PullToRefresh)
        refreshControl.addTarget(self, action: #selector(CommentVc.handleRefresh(_:)), for: UIControlEvents.valueChanged)
        tblComments.addSubview(refreshControl)
        
    }
    
    func handleRefresh(_ refreshControl: UIRefreshControl) {
        
        imageCache.removeAll()
        page = 1
        arrComments.removeAll()
        offset = 0
        
        getCommentsData()

    }
    
    @IBAction func addComment(_ sender: AnyObject) {
        
        if(!txtComments.text!.isEmpty)
        {
            sendComment()
        }
        else
        {
            //Show an alert to write text
            let alert = UIAlertController(title: "", message: alertMsg_Comment, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: alertBtnTitle_OK, style: UIAlertActionStyle.default, handler:nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    //MARK: - Get Comments Data
    func getCommentsData(){
        
        self.imageCache.removeAll()

        var parameters = [String: AnyObject]()
        parameters["user_id"] = Utilities.getUserDetails().id as AnyObject
        parameters["limit"] = Limit as AnyObject
        parameters["offset"] = offset as AnyObject
        parameters["media_id"] = md.pId as AnyObject
        
        APIManager.sharedInstance.requestGetComments(parameters, Target: self)
    }
    
    //MARK: - Get Comments Response
    func responseGetComments (_ notify: Foundation.Notification)
    {
         refreshControl.endRefreshing()
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFIACTION_GETCOMMENTS), object: nil)
        
        let swiftyJsonVar = JSON(notify.object!)
        
        let message = swiftyJsonVar["message"].stringValue
        let status = swiftyJsonVar["status"].intValue
        totalRecordCount = swiftyJsonVar["response"]["totalRecordCount"].intValue
        
        singleObject?.updateCommentCount(totalRecordCount)
        
        if(swiftyJsonVar[ERROR_KEY] != nil)
        {
            tblComments.isHidden = true
            
            lblNoComments.text = swiftyJsonVar[ERROR_KEY].stringValue
            lblNoComments.isHidden = false
            tblComments.isHidden = true

            
            showDodoMessage(swiftyJsonVar[ERROR_KEY].stringValue, messageType: MessageType.error)
        }
        else if (status == 1) //success response
        {
            if offset == 0
            {
                arrComments.removeAll()
            }
            
            var cmts = [Comment]()
            cmts = Parser.getParsedCommentsArrayFromData(swiftyJsonVar["response"]["comments"])
            
            arrComments.append(contentsOf: cmts)
            
            if arrComments.count>0
            {
                //load table
                lblNoComments.isHidden = true
                
                tblComments.isHidden = false
                tblComments.reloadData()
               
    
            }
            else
            {
                tblComments.isHidden = true
                
                lblNoComments.text = alertMsg_NoComments
                lblNoComments.isHidden = false
                tblComments.isHidden = true
            }
        }
        else if (status == 0) //error response
        {
            let responseErrorMessage = swiftyJsonVar["response"].stringValue
            showDodoMessage(responseErrorMessage, messageType: MessageType.error)
        }
        else
        {
            showDodoMessage(message, messageType:
                MessageType.error)
        }
    }
    
    //MARK: - Add Comment
    func sendComment(){
        
        ApplicationDelegate.showLoader(alertMsg_AddingComments)
        
        var parameters = [String: AnyObject]()
        parameters["user_id"] = md.pUserId as AnyObject
        parameters["commenter_id"] = Utilities.getUserDetails().id as AnyObject
        parameters["commenter_details"] = self.txtComments.text as AnyObject
        parameters["media_id"] = md.pId as AnyObject
        
        APIManager.sharedInstance.requestAddComment(parameters, Target: self)
    }
    
    //MARK: - Add Comment Response
    func responseAddComment (_ notify: Foundation.Notification)
    {
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFIACTION_ADDCOMMENT)
            , object: nil)
        
        ApplicationDelegate.hideLoader ()
        
        txtComments.text = ""
        txtComments.resignFirstResponder()
        
        let swiftyJsonVar = JSON(notify.object!)
        
        let message = swiftyJsonVar["message"].stringValue
        let status = swiftyJsonVar["status"].intValue
        
        if swiftyJsonVar [ERROR_KEY].exists()
        {
            self.showCommonAlert(swiftyJsonVar[ERROR_KEY].stringValue)
        }
        else if (status == 1) //success response
        {
            offset = 0
            page = 1
            
            //reload data
            getCommentsData()
        }
        else if (status == 0) //error response
        {
            let responseErrorMessage = swiftyJsonVar["response"].stringValue
            showDodoMessage(responseErrorMessage, messageType: MessageType.error)
        }
        else
        {
            showDodoMessage(message, messageType: MessageType.error)
        }
    }
    
    // MARK: - Table view data source
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrComments.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell
    {
        //Load More
        if (arrComments.count >= Limit) && (indexPath.row + 1  == arrComments.count) {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "LoadMoreCell")
            
            if let lbl5176 =  cell?.viewWithTag(5176) as? UILabel
            {
                lbl5176.text = loading_Title
                
                if (totalRecordCount) > (page) * Limit {
                    
                    offset = (page) * Limit
                    page += 1
                    
                    getCommentsData()
                }
                else
                {
                    lbl5176.isHidden = true
                    
                    lbl5176.text = loadingNoMoreData
                    
                    return refreshCellForRow(indexPath)
                }
            }
            
            return cell!
        }
            
        else{
            
            
            return refreshCellForRow(indexPath)
        }
        
    }
    
    func refreshCellForRow(_ indexPath:IndexPath)->CommentCell
    {
        let cell = self.tblComments.dequeueReusableCell(withIdentifier: "CommentCell1") as! CommentCell
        
        let commentVal:Comment = arrComments[indexPath.row]
        cell.lblUserName.font = ProximaNovaRegular(15)
        cell.lblUserName.text = commentVal.pUserName
        cell.lblComment.font = ProximaNovaRegular(15)
        cell.lblComment.text = commentVal.pComment
        /*Added for test Purpose
        cell.lblComment.text = "jewhuewroiworuoiwuriouwioruiowuriouwioruiowuriouwioruiouwriourwiouwiouriowuriowuriuwioruiowuriowuriourwiouiowruiowuriowuroiuwoiruiowuriowurioruwoiuwoiruiowuriowuriouwioruiowuriowuriouwioruiowuriouwioruiowruoiwuriouwoiwruiwuriowioruiowuroiuiowruoiruwoiwurjewhuewroiworuoiwuriouwioruiowuriouwioruiowuriouwioruiouwriourwiouwiouriowuriowuriuwioruiowuriowuriourwiouiowruiowuriowuroiuwoiruiowuriowurioruwoiuwoiruiowuriowuriouwioruiowuriowuriouwioruiowuriouwioruiowruoiwuriouwoiwruiwuriowioruiowuroiuiowruoiruwoiwurAnubha"*/
  /*     COMMENTED BY ANUBHA   cell.lblComment.preferredMaxLayoutWidth =  CGRectGetWidth(cell.lblComment.frame)
        cell.lblComment.numberOfLines = 0;*/
        
        //creation date
        cell.lblCreateDate.font = ProximaNovaRegular(15)
        if commentVal.pCreatedDate.isEmpty == false
        {
            cell.lblCreateDate.text = Utilities().getDateAgo(commentVal.pCreatedDate)
        }
        else
        {
            cell.lblCreateDate.text = "NA"
        }
        
        //Loading User profile image
        cell.imgUser.image = Utilities().themedImage(img_defaultUser)
        cell.imgUser!.contentMode = .scaleAspectFit
        cell.imgUser.layer.masksToBounds = false
        cell.imgUser.layer.cornerRadius = cell.imgUser.frame.height/2
        cell.imgUser.clipsToBounds = true
        
        if commentVal.pUserImage.isEmpty == true
        {
            return cell
        }
        
        if let img = imageCache[commentVal.pUserImage] {
            cell.imgUser!.image = img
        }
        else if imageCache[commentVal.pUserImage] == nil
        {
            //Downloading user's image
            URLSession.shared.dataTask(with: URL(string: commentVal.pUserImage)!, completionHandler: { (data, response, error) -> Void in
                
                if error != nil {
                    
                    cell.imgUser!.image = Utilities().themedImage(img_defaultUser)
                    
                    print(error)
                    return
                }
                
                DispatchQueue.main.async(execute: { () -> Void in
                    let image = UIImage(data: data!)
                    
                    if (image == nil)
                    {
                        cell.imgUser!.image = Utilities().themedImage(img_defaultUser)
                    }
                    else
                    {
                        cell.imgUser!.image = image
                        self.imageCache[commentVal.pUserImage] = cell.imgUser!.image
                    }
                    
                })
            }).resume()
        }
        
        return cell
    }
}
