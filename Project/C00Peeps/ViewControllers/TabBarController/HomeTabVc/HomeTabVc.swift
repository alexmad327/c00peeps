//
//  HomeTabVc.swift
//  C00Peeps
//
//  Created by SOTSYS011 on 3/18/16.
//  Copyright © 2016 SOTSYS011. All rights reserved.
//

import UIKit
import SwiftyJSON

extension UIImage {
    func makeImageWithColorAndSize(_ color: UIColor, size: CGSize) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        color.setFill()
        UIRectFill(CGRect(x: 0, y: 0, width: size.width, height: size.height))
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
}
class HomeTabVc: BaseVC, UniversalDelegate {
    

    @IBOutlet weak var lblNoRecordsFound: UILabel!
    @IBOutlet weak var btnRefresh: UIButton!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var viewNoFeed: UIView!
    @IBOutlet weak var btnFollowPeople: UIButton!
    
    var offset = 0
    var totalRecords = 0
    var pageNumber = 1
    var feedsArray = [Media]()
    var delegate:UniversalVc?
    var controller : UniversalVc = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "UniversalVc") as! UniversalVc
    
    override func viewWillAppear(_ animated: Bool) {
    
        self.tabBarController?.tabBar.isHidden = false
        
        if viewNoFeed.isHidden == false{
            
            // Show Loader.
            loadingIndicator.startAnimating()
            self.perform(#selector(HomeTabVc.getFeedsData), with: nil, afterDelay: 0.2)
        }
        
        //Get unread notification count
        getUnreadNotificationCount()
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        viewNoFeed.isHidden = true
        
        printCustom("Utilities.getUserDetails().mediaTypeId:\(Utilities.getUserDetails().filmMediaTypeId)")
        self.navigationItem.title = "Home"
    
      
        var imgNotif = Utilities().themedImage(img_notification)
        imgNotif = imgNotif.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        
        
        let customView = UIView.init(frame: CGRect(x: 0, y: 25, width: 110, height: 30))
        
        let imgview = UIImageView.init(image: imgNotif)
        imgview.frame = CGRect(x: 60, y: 2, width: 25, height: 23)
        customView.addSubview(imgview)
        
        let btn = UIButton(frame: CGRect(x: 60, y: 0, width: 50, height: 30))
        btn.addTarget(self, action:#selector(HomeTabVc.moveToNotification), for: UIControlEvents.touchUpInside)
        customView.addSubview(btn)
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: customView)
        
        //self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: imgNotif, style:UIBarButtonItemStyle.Plain, target: self, action: #selector(HomeTabVc.moveToNotification))

        self.tabBarController?.tabBar.selectionIndicatorImage = UIImage().makeImageWithColorAndSize(Utilities().themedMultipleColor()[1], size: CGSize(width: self.tabBarController!.tabBar.frame.width/5, height: self.tabBarController!.tabBar.frame.height))//Utilities().themedImage(img_selectedBtm)

        Utilities.setSignUpState(SignUpState.home)
        
        //testDecrypt()
        
        // Show Loader.
        loadingIndicator.startAnimating()
        self.perform(#selector(HomeTabVc.getFeedsData), with: nil, afterDelay: 0.2)
    }
    
    func drawCircle(_ unreadCount:Int) {
        
        var imgNotif = Utilities().themedImage(img_notification)
        imgNotif = imgNotif.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        
        if unreadCount > 0
        {
            let customView = UIView.init(frame: CGRect(x: 0, y: 25, width: 110, height: 30))
            //customView.backgroundColor = UIColor.greenColor()
            
            let countLabel = UILabel.init(frame: CGRect(x: 80, y: 0, width: 25, height: 15))
            
            countLabel.layer.backgroundColor  = UIColor.red.cgColor
            
            countLabel.layer.cornerRadius = 8
            
            countLabel.textAlignment = NSTextAlignment.center
            countLabel.font = UIFont(name: "Arial", size: 10.0)
            countLabel.textColor = UIColor.white
            countLabel.text = String(unreadCount)
            customView.addSubview(countLabel)
            
            
            let imgview = UIImageView.init(image: imgNotif)
            imgview.frame = CGRect(x: 60, y: 2, width: 25, height: 23)
            customView.addSubview(imgview)
            
            let btn = UIButton(frame: CGRect(x: 60, y: 0, width: 50, height: 30))
            btn.addTarget(self, action:#selector(HomeTabVc.moveToNotification), for: UIControlEvents.touchUpInside)
            customView.addSubview(btn)
            
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: customView)
        }
        else
        {
            let customView = UIView.init(frame: CGRect(x: 0, y: 25, width: 110, height: 30))
            
            let imgview = UIImageView.init(image: imgNotif)
            imgview.frame = CGRect(x: 60, y: 2, width: 25, height: 23)
            customView.addSubview(imgview)
            
            let btn = UIButton(frame: CGRect(x: 60, y: 0, width: 50, height: 30))
            btn.addTarget(self, action:#selector(HomeTabVc.moveToNotification), for: UIControlEvents.touchUpInside)
            customView.addSubview(btn)
            
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: customView)
            
            //self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: imgNotif, style:UIBarButtonItemStyle.Plain, target: self, action: #selector(HomeTabVc.moveToNotification))
        }
    }
    
    /*
    func testDecrypt(){
        
        
        guard let fileURL = Bundle(for: type(of: self)).url(forResource: "Movie", withExtension:".mp4") else {
            
            fatalError("File not found")
            
        }
        print(fileURL)
        
        do {
            
            let data =  try Data.init(contentsOf: fileURL)
            
            print("original data count: " + (String(data.count)))
            
            //let publicKey = Encryption.sharedInstance.createRSAPublicKey("Arsh")
            
            //Ashish's public key
            let ecryptedData = Encryption.sharedInstance.encryptData(data)
            
            //Decrypt data
            let decryptedData = Encryption.sharedInstance.decryptData(ecryptedData)
            
            //Save media locally
            let fileManager = FileManager.default
            let urls = fileManager.urls(for: .documentDirectory, in: .userDomainMask)
            let documentDirectory = urls[0] as URL
            
            let dataPath = documentDirectory.appendingPathComponent("Movie555.mp4")
            
            print(dataPath.path)
            
            try decryptedData.write(to: URL.init(fileURLWithPath: dataPath.path))
            
        } catch let jsonError as NSError {
            
            print("Error: ",(jsonError.description))
        }   
    }*/
 
    //MARK:- Follow people, if no feed found
    @IBAction func btnFollowPeople(_ sender: AnyObject){
        
        let st = UIStoryboard(name: "Gallery", bundle:nil)
        let likersVc = st.instantiateViewController(withIdentifier: "LikersViewController") as! LikersViewController
        likersVc.screenType = .topFiveUsers
        
        self.navigationController?.pushViewController(likersVc, animated: true)
    }
    
    func moveToNotification(){
        
        let st = UIStoryboard(name: "Notification", bundle:nil)
        let obj = st.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    fileprivate func showUniversalView(){
        
        if delegate != nil
        {
            delegate?.tblTimeline.isHidden = false
            delegate?.arrMedia = feedsArray
            delegate?.totalRecordCount = totalRecords
            delegate?.offset = offset
            delegate?.page = pageNumber
            delegate?.reloadTable()
        }
        else
        {
            controller = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "UniversalVc") as! UniversalVc
            
            controller.arrMedia = feedsArray
            controller.totalRecordCount = totalRecords
            controller.universalDeleg = self;
            controller.offset = offset
            controller.page = pageNumber
            
            let fr =  CGRect(x: 0, y: heightNavigationBar, width: controller.view.frame.size.width, height: self.view.frame.height-heightNavigationBar)
            
            controller.view.frame = fr;
            controller.willMove(toParentViewController: self)
            self.view.addSubview(controller.view)
            self.addChildViewController(controller)
            controller.didMove(toParentViewController: self)
        }
    }
    
    //MARK: - Button Actions
    @IBAction func btnRefreshClicked(_ sender: AnyObject) {
        self.lblNoRecordsFound.isHidden = true
        self.btnRefresh.isHidden = true
        self.loadingIndicator.startAnimating()
        self.getFeedsData()
    }
    
    //MARK: - Get Feeds Data
    func getFeedsData(){
        var parameters = [String: AnyObject]()
        parameters["user_id"] = Utilities.getUserDetails().id as AnyObject
        parameters["limit"] = Limit as AnyObject
        parameters["offset"] = offset as AnyObject
        
        APIManager.sharedInstance.requestGetFeeds(parameters, Target: self)
    }
    
    //MARK: - Get Feeds Response
    func responseGetFeeds (_ notify: Foundation.Notification)
    {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFIACTION_GETFEEDS), object: nil)
        
        if loadingIndicator.isAnimating {
            loadingIndicator.stopAnimating()
        }
        
        let swiftyJsonVar = JSON(notify.object!)
        
        let message = swiftyJsonVar["message"].stringValue
        let status = swiftyJsonVar["status"].intValue
        
        if swiftyJsonVar [ERROR_KEY].exists()
        {
            self.showErrorMessage(swiftyJsonVar[ERROR_KEY].stringValue)
        }
        else if (status == 1) //success response
        {
            if offset == 0
            {
                feedsArray.removeAll()
            }
            
            totalRecords = swiftyJsonVar["response"]["count"].intValue
            var arrFeeds = [Media]()
            arrFeeds = Parser.getParsedMediaArrayFromData(swiftyJsonVar["response"]["media"])
            
            feedsArray.append(contentsOf: arrFeeds)
            
            if feedsArray.count>0
            {
                self.hideErrorMessage()
                
                viewNoFeed.isHidden = true
                showUniversalView()
            }
            else
            {
                self.showErrorMessage("No record exist")
                
            }
        }
        else if (status == 0) //error response
        {
            let responseErrorMessage = swiftyJsonVar["response"].stringValue
            
            if responseErrorMessage == "No feeds found"
            {
                viewNoFeed.isHidden = false
                
                if delegate != nil
                {    
                    delegate?.willMove(toParentViewController: nil)
                    delegate?.view.removeFromSuperview()
                    delegate?.removeFromParentViewController()
                    
                    delegate = nil
                }
            }
            else
            {
                self.showErrorMessage(responseErrorMessage)
            }
        }
        else
        {
            self.showErrorMessage(message)
        }
    }
    
        
    //MARK: - Read Notification
    func getUnreadNotificationCount(){
        
        loadingIndicator.startAnimating()
        
        var parameters = [String: AnyObject]()
        parameters["user_id"] = Utilities.getUserDetails().id as AnyObject
        
        APIManager.sharedInstance.getUnreadNotificationCount(parameters, Target: self)
    }
    
    //MARK: - Read Notification Response
    func responseGetUnreadNotificationCount (_ notify: Foundation.Notification)
    {
        if loadingIndicator.isAnimating {
            loadingIndicator.stopAnimating()
        }
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFICATION_GETUNREADNOTIFICATIONCOUNT), object: nil)
        
        let swiftyJsonVar = JSON(notify.object!)
        
        let message = swiftyJsonVar["message"].stringValue
        let status = swiftyJsonVar["status"].intValue
        
        if swiftyJsonVar [ERROR_KEY].exists()
        {
            self.showErrorMessage(swiftyJsonVar[ERROR_KEY].stringValue)
        }
        else if (status == 1) //success response
        {
            let unreadCount = swiftyJsonVar["response"].intValue
            UIApplication.shared.applicationIconBadgeNumber = unreadCount
             drawCircle(unreadCount)
        }
        else if (status == 0) //error response
        {
            let responseErrorMessage = swiftyJsonVar["response"].stringValue
            self.showErrorMessage(responseErrorMessage)
        }
        else
        {
            self.showErrorMessage(message)
        }
    }
    
    // MARK: - Show/Hide Error Message
    func showErrorMessage(_ message:String) {
        self.btnRefresh.isHidden = false
        self.lblNoRecordsFound.isHidden = false
        self.lblNoRecordsFound.text = message
    }
    
    func hideErrorMessage() {
        self.btnRefresh.isHidden = true
        self.lblNoRecordsFound.isHidden = true
    }

    
    //MARK: - MoveToNext Screen
    func moveToNextScreen(_ ScreenName:String, WithData:AnyObject)
    {
        
        if(ScreenName == "SingleDetailVc")
        {
            let singleDetailVc = self.storyboard?.instantiateViewController(withIdentifier: "SingleDetailVc") as? SingleDetailVc
            
            var arr = [Media]()
            arr.append(WithData as! Media)
            singleDetailVc!.arrMedia = arr
            self.navigationController?.pushViewController(singleDetailVc!, animated: true)
        }
        else if(ScreenName == "CommentVc")
        {
            let commentObject = self.storyboard?.instantiateViewController(withIdentifier: "CommentVc") as? CommentVc
            commentObject?.md = WithData as! Media
            self.navigationController?.pushViewController(commentObject!, animated: true)
        }
        else if(ScreenName == "GalleryMainViewController")
        {
            let st = UIStoryboard(name: "Gallery", bundle:nil)
            let galleryObject = st.instantiateViewController(withIdentifier: "GalleryMainViewController") as? GalleryMainViewController
            let md = WithData as! Media
            galleryObject?.otherUserId = md.pUserId
            self.navigationController?.pushViewController(galleryObject!, animated: true)
        }
        else if (ScreenName == "ForwardMedia")
        {
            let st = UIStoryboard(name: "Digital", bundle:nil)
            let obj = st.instantiateViewController(withIdentifier: "ContactsScreen") as! ContactsScreen
            let md = WithData as! Media
            obj.fwdMediaId = md.pId
            obj.isComingFromForwardMedia = true
            self.navigationController?.pushViewController(obj, animated: true)
        }
    }
    
    //MARK: - Reload Media (or pull to refresh)
    func reloadMedia(_ obj:UniversalVc)
    {
        offset = 0
        pageNumber = 1
        delegate = obj
        
        getFeedsData()
    }
    
    
    //MARK: - Load More Media
    func loadMoreMedia(_ page:Int, offset offset1:Int, obj del:UniversalVc)
    {
        offset = offset1
        pageNumber = page
        delegate = del
        
        getFeedsData()
    }
}
