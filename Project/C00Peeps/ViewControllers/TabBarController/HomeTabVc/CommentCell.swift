//
//  CommentCell.swift
//  C00Peeps
//
//  Created by OSX on 27/07/16.
//  Copyright © 2016 SOTSYS011. All rights reserved.
//

import Foundation
import UIKit

class CommentCell: UITableViewCell {
    
    @IBOutlet weak var lblComment: UILabel!
    @IBOutlet weak var imgUser: UIImageView!
    
    @IBOutlet weak var lblCreateDate: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        // Initialization code
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.layoutIfNeeded()
        imgUser!.layer.cornerRadius = (imgUser!.frame.size.width)/2
        imgUser!.clipsToBounds = true
    }
    
}