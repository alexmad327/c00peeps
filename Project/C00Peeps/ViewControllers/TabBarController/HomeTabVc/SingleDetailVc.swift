//
//  SingleDetailVc.swift
//  C00Peeps
//
//  Created by OSX on 15/07/16.
//  Copyright © 2016 SOTSYS011. All rights reserved.
//

import Foundation

class SingleDetailVc: BaseVC, UniversalDelegate, SingleDetailToCommentProtocol {
    
    var arrMedia = [Media]()
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let controller : UniversalVc = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "UniversalVc") as! UniversalVc
        
        controller.bComingFromSingleMediaScreen = true;
        let fr =  CGRect(x: 0, y: heightNavigationBar, width: controller.view.frame.size.width, height: self.view.frame.height-heightNavigationBar)
        controller.view.frame = fr;
        controller.willMove(toParentViewController: self)
        
        controller.arrMedia = arrMedia
        controller.universalDeleg = self
        
        printCustom("Single Media :\(arrMedia)")
        self.view.addSubview(controller.view)
        self.addChildViewController(controller)
        controller.didMove(toParentViewController: self)
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.title = "Single Media"

    }
    
    //MARK: - MoveToNext Screen
    func moveToNextScreen(_ ScreenName:String, WithData:AnyObject)
    {
        if(ScreenName == "CommentVc")
        {
            let st = UIStoryboard(name: "Home", bundle:nil)
            let commentObject = st.instantiateViewController(withIdentifier: "CommentVc") as! CommentVc
            
            commentObject.md = WithData as! Media
            commentObject.singleObject = self
            self.navigationController?.pushViewController(commentObject, animated: true)
        }
        else if(ScreenName == "GalleryMainViewController")
        {
            let st = UIStoryboard(name: "Gallery", bundle:nil)
            let galleryObject = st.instantiateViewController(withIdentifier: "GalleryMainViewController") as? GalleryMainViewController
            let md = WithData as! Media
            galleryObject?.otherUserId = md.pUserId
            self.navigationController?.pushViewController(galleryObject!, animated: true)
        }
        else if (ScreenName == "ForwardMedia")
        {
            let st = UIStoryboard(name: "Digital", bundle:nil)
            let obj = st.instantiateViewController(withIdentifier: "ContactsScreen") as! ContactsScreen
            let md = WithData as! Media
            obj.fwdMediaId = md.pId
            obj.isComingFromForwardMedia = true
            self.navigationController?.pushViewController(obj, animated: true)
        }
    }
    
    //MARK: - Update Comment Count
    func updateCommentCount(_ commentCount:Int){
        
        let media = arrMedia[0]
        
        media.pCountComments = commentCount
        
    }
    
}
