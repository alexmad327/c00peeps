//
//  GalleryMainViewController.swift
//  C00Peeps
//
//  Created by SOTSYS027 on 3/16/16.
//  Copyright © 2016 SOTSYS011. All rights reserved.
//

import UIKit
import SwiftyJSON

//private let reuseIdentifier = "Cell"

class GalleryMainViewController: UICollectionViewController, UpdateContent {
    
    @IBOutlet weak var lblNoData: UILabel!
    @IBOutlet weak var btnPhotos: UIButton!
    @IBOutlet weak var btnMovies: UIButton!
    @IBOutlet weak var btnMusic: UIButton!
    @IBOutlet weak var btnVideoGames: UIButton!
    @IBOutlet weak var btnPeepText: UIButton!
    var leadingConstraintSelectionView:NSLayoutConstraint!
    
    var topViewTopSpaceWithMyGallery:NSLayoutConstraint!
    var topViewTopSpaceWithAllGallery:NSLayoutConstraint!
    var topViewTopSpaceWithGridButton:NSLayoutConstraint!
    var topViewTopSpaceWithListButton:NSLayoutConstraint!
    
    var vwSelection:UIView!
  
    var universalCollectionViewMovies : UniversalCollectionView!
    var universalCollectionViewVideoGames : UniversalCollectionView!
    var universalCollectionViewMusic : UniversalCollectionView!
    var universalCollectionViewPhotos : UniversalCollectionView!
    var universalCollectionViewPeepText:UniversalCollectionView!
    
    var headerView: UICollectionReusableView!
    
    var image_user: UIImageView!
    var lblPostcount: UIButton!
    var lblFollowerscount: UIButton!
    var lblFollowingscount: UIButton!
    var label_user_name: UILabel!
    var lblTagLine: UILabel!
    
    var btnGridView: UIButton!
    var btnListView: UIButton!
    var btnAllGallery: UIButton!
    var btnMyGallery: UIButton!
   
    
    var arrayMedia = [Media]()
    var arrayMusic = [Media]()
    var arrayVideoGames = [Media]()
    var arrayPhotos = [Media]()
    var arrayPeepText = [Media]()
    
    //Movies
    var offsetMovies = 0
    var currentPageMovies = 1
    var totalRecordsMovies = 0
    
    //Music
    var offsetMusic = 0
    var currentPageMusic = 1
    var totalRecordsMusic = 0
    
    //Video Games
    var offsetVideoGames = 0
    var currentPageVideoGames = 1
    var totalRecordsVideoGames = 0
    
    //Photos
    var offsetPhotos = 0
    var currentPagePhotos = 1
    var totalRecordsPhotos = 0
    
    //Peep Text
    var offsetPeepText = 0
    var currentPagePeepText = 1
    var totalRecordsPeepText = 0
    
    var catIdMusic = 1
    var catIdMovies = 2
    var catIdPhotos = 3
    var catIdVideoGames = 4
    var catIdPeepText = -1
    
    var galleryType = GalleryType.myGallery //My Gallery
    var likerMediaType = LikerMediaType.gallery // Gallery

    
    var isSecondTabLoaded: Bool!
    var isThirdTabLoaded: Bool!
    var isFourthTabLoaded: Bool!
    var isFifthTabLoaded: Bool!
    var selectedIndex = 11
    var displayListView = false
    
    var otherUserId = -1
    
    override func viewWillAppear(_ animated: Bool) {
    
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = false
        self.navigationItem.title = "Gallery"
        
        self.collectionView?.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.updateViewAccordingToTheme()
        
        self.collectionView!.register(UniversalCollectionViewCell.self, forCellWithReuseIdentifier: "universal_cell")
        self.collectionView!.register(UINib(nibName: "UniversalCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "universal_cell")

        self.automaticallyAdjustsScrollViewInsets = false
        self.collectionView?.backgroundColor = UIColor.clear
        self.view.backgroundColor = UIColor.white
        
        self.collectionView?.delegate = self
        self.collectionView?.dataSource = self
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Update View According To Selected Theme
    func updateViewAccordingToTheme() {
        
        let imgSettings = Utilities().themedImage(img_setting)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: imgSettings, style:UIBarButtonItemStyle.plain, target: self, action: #selector(moveToProfileScreen))
    }

    //MARK: - Move to profile screen
    func moveToProfileScreen(){
        //Fetch user details in background
        self.getUserDetails()
        
        // Move to Settings
        self.navigationItem.title = ""
        let storyboard = UIStoryboard(name: "ProfileStoryBoard", bundle:nil)
        let ContactListObject = storyboard.instantiateViewController(withIdentifier: "SettingMainViewController") as? SettingMainViewController
        self.navigationController?.pushViewController(ContactListObject!, animated: true)
    }
    
    //MARK: - Get User Details
    func getUserDetails() {
        APIManager.sharedInstance.cancelAllRequests()

        var parameters = [String: AnyObject]()
        
        if otherUserId == -1
        {
            parameters["user_id"] = Utilities.getUserDetails().id as AnyObject
        }
        else
        {
            parameters["user_id"] = String(otherUserId) as AnyObject
        }
        
        APIManager.sharedInstance.requestGetUserDetails(parameters, Target: self)
    }
    
    //MARK: - Response Get User Details
    func responseGetUserDetails (_ notify: Foundation.Notification) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFIACTION_GETUSERDETAILS), object: nil)
        
        let swiftyJsonVar = JSON(notify.object!)
        let status = swiftyJsonVar["status"].intValue
        if (status == 1) {
            let response = swiftyJsonVar["response"]
            printCustom("get user details response:\(response)")
            printCustom("get user details  id:\(response["basic"]["id"])")
            
            let responseUserDetail:[String:AnyObject] = response.dictionaryObject! as [String : AnyObject]
            let userDetail:UserDetail = UserDetail()
            Parser.parseUserDetails(responseUserDetail, userDetail: userDetail)
            printCustom("Utilities.getUserDetails().mediaTypeId:\(Utilities.getUserDetails().filmMediaTypeId)")

        }
    }
    
    func showCommonAlert(_ msg:String) {
        let alert:UIAlertController = UIAlertController(title: "", message: msg, preferredStyle: .alert)
        let alertAction = UIAlertAction(title: alertBtnTitle_OK, style: .default, handler: nil)
        alert.addAction(alertAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK: - Get Gallery media
    func getGalleryMedia(_ categoryId:Int){
        
        //Category Id (1 Music 2 Movie 3 Photo 4 Video Game
        var parameters = [String: AnyObject]()
        
        parameters["type"] = galleryType.rawValue as AnyObject
        parameters["category_id"] = categoryId as AnyObject
        parameters["limit"] = SearchLimit as AnyObject
        
        if otherUserId == -1 || otherUserId == Int(Utilities.getUserDetails().id)
        {
            btnAllGallery.isHidden = false
            btnMyGallery.isHidden = false

            parameters["user_id"] = Utilities.getUserDetails().id as AnyObject
        }
        else
        {
            btnAllGallery.isHidden = true
            btnMyGallery.isHidden = true
            
            parameters["user_id"] = String(otherUserId) as AnyObject
        }
        
        APIManager.sharedInstance.cancelAllRequests()
        
        if categoryId == catIdMovies //Movies
        {
            if arrayMedia.count == 0 {
                universalCollectionViewMovies.arrayCollection = []
                universalCollectionViewMovies.showLoadingIndicator = true
                universalCollectionViewMovies.displayRetryOption = true
                universalCollectionViewMovies.reloadData()
            }
            parameters["offset"] = offsetMovies as AnyObject
            
            APIManager.sharedInstance.requestGalleryMovies(parameters, Target: self)
        }
        else if categoryId == catIdMusic //Music
        {
            if arrayMusic.count == 0 {
                universalCollectionViewMusic.arrayCollection = []
                universalCollectionViewMusic.showLoadingIndicator = true
                universalCollectionViewMusic.displayRetryOption = true
                universalCollectionViewMusic.reloadData()
            }
            parameters["offset"] = offsetMusic as AnyObject
            
            APIManager.sharedInstance.requestGalleryMusic(parameters, Target: self)
        }
        else if categoryId == catIdVideoGames //Video Games
        {
            if arrayVideoGames.count == 0 {
                universalCollectionViewVideoGames.arrayCollection = []
                universalCollectionViewVideoGames.showLoadingIndicator = true
                universalCollectionViewVideoGames.displayRetryOption = true
                universalCollectionViewVideoGames.reloadData()
            }
            parameters["offset"] = offsetVideoGames as AnyObject
            APIManager.sharedInstance.requestGalleryVideoGames(parameters, Target: self)
        }
        else if categoryId == catIdPhotos //Photos
        {
            if arrayPhotos.count == 0 {
                universalCollectionViewPhotos.arrayCollection = []
                universalCollectionViewPhotos.showLoadingIndicator = true
                universalCollectionViewPhotos.displayRetryOption = true
                universalCollectionViewPhotos.reloadData()
            }
            parameters["offset"] = offsetPhotos as AnyObject
            APIManager.sharedInstance.requestGalleryPhotos(parameters, Target: self)
        }
        else if categoryId == catIdPeepText //PeepText
        {
            if arrayPeepText.count == 0 {
                universalCollectionViewPeepText.arrayCollection = []
                universalCollectionViewPeepText.showLoadingIndicator = true
                universalCollectionViewPeepText.displayRetryOption = true
                universalCollectionViewPeepText.reloadData()
            }
            parameters["offset"] = offsetPeepText as AnyObject
            
            print(parameters)
            APIManager.sharedInstance.requestGalleryPeepText(parameters, Target: self)
        }
    }
    
    //MARK: - Get Movies Response
    func responseGalleryMovies (_ notify: Foundation.Notification)
    {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFIACTION_GETGALLERYMOVIES), object: nil)
        
        let swiftyJsonVar = JSON(notify.object!)
        
        let message = swiftyJsonVar["message"].stringValue
        let status = swiftyJsonVar["status"].intValue
        
        if swiftyJsonVar [ERROR_KEY].exists()
        {
            universalCollectionViewMovies.arrayCollection = []
            universalCollectionViewMovies.showLoadingIndicator = false
            universalCollectionViewMovies.displayRetryOption = true
            universalCollectionViewMovies.noRecordsFoundTitle = swiftyJsonVar[ERROR_KEY].stringValue
            universalCollectionViewMovies.reloadData()
        }
        else if (status == 1) //success response
        {
            totalRecordsMovies = swiftyJsonVar["response"]["count"].intValue
            
            let userDetail = swiftyJsonVar["response"]["user_details"]
            refreshHeaderWithValues(userDetail)
            
            if totalRecordsMovies > 0
            {
                var arrFeeds = [Media]()
                arrFeeds = Parser.getParsedMediaArrayFromData(swiftyJsonVar["response"]["media"])
               
                
                if offsetMovies == 0
                {
                    arrayMedia.removeAll()
                }
                
                arrayMedia.append(contentsOf: arrFeeds)
                
                if arrayMedia.count>0
                {
                    universalCollectionViewMovies.totalRecordCount = totalRecordsMovies
                    universalCollectionViewMovies.searchCategoryId = catIdMovies
                    universalCollectionViewMovies.currentPage = currentPageMovies
                    universalCollectionViewMovies.offset = offsetMovies
                    universalCollectionViewMovies.arrayCollection = self.arrayMedia
                    universalCollectionViewMovies.showLoadingIndicator = false
                    universalCollectionViewMovies.displayRetryOption = true
                    universalCollectionViewMovies.reloadData()
                    
                }
                else
                {
                    universalCollectionViewMovies.arrayCollection = self.arrayMedia
                    universalCollectionViewMovies.showLoadingIndicator = false
                    universalCollectionViewMovies.displayRetryOption = true
                    universalCollectionViewMovies.noRecordsFoundTitle = alertMsg_NoData
                    universalCollectionViewMovies.reloadData()
                }
            }
            else
            {
                universalCollectionViewMovies.arrayCollection = self.arrayMedia
                universalCollectionViewMovies.showLoadingIndicator = false
                universalCollectionViewMovies.displayRetryOption = true
                universalCollectionViewMovies.noRecordsFoundTitle = alertMsg_NoData
                universalCollectionViewMovies.reloadData()
            }
        }
        else if (status == 0) //error response
        {
            universalCollectionViewMovies.arrayCollection = []
            universalCollectionViewMovies.showLoadingIndicator = false
            universalCollectionViewMovies.displayRetryOption = true
            universalCollectionViewMovies.noRecordsFoundTitle = swiftyJsonVar["response"].stringValue
            universalCollectionViewMovies.reloadData()
        }
        else
        {
            universalCollectionViewMovies.arrayCollection = []
            universalCollectionViewMovies.showLoadingIndicator = false
            universalCollectionViewMovies.displayRetryOption = true
            universalCollectionViewMovies.noRecordsFoundTitle = message
            universalCollectionViewMovies.reloadData()
        }
    }
    
    
    //MARK: - Get Movies Response
    func responseGalleryMusic (_ notify: Foundation.Notification)
    {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFIACTION_GETGALLERYMUSIC), object: nil)
        
        let swiftyJsonVar = JSON(notify.object!)
        
        let message = swiftyJsonVar["message"].stringValue
        let status = swiftyJsonVar["status"].intValue
        
        if swiftyJsonVar [ERROR_KEY].exists()
        {
            universalCollectionViewMusic.arrayCollection = []
            universalCollectionViewMusic.showLoadingIndicator = false
            universalCollectionViewMusic.displayRetryOption = true
            universalCollectionViewMusic.noRecordsFoundTitle = swiftyJsonVar[ERROR_KEY].stringValue
            universalCollectionViewMusic.reloadData()
        }
        else if (status == 1) //success response
        {
            totalRecordsMusic = swiftyJsonVar["response"]["count"].intValue
            
            let userDetail = swiftyJsonVar["response"]["user_details"]
            refreshHeaderWithValues(userDetail)

            
            if totalRecordsMusic > 0
            {
                var arrFeeds = [Media]()
                arrFeeds = Parser.getParsedMediaArrayFromData(swiftyJsonVar["response"]["media"])
                
                if offsetMusic == 0
                {
                    arrayMusic.removeAll()
                }
                
                arrayMusic.append(contentsOf: arrFeeds)
                
                if arrayMusic.count>0
                {
                    universalCollectionViewMusic.totalRecordCount = totalRecordsMusic
                    universalCollectionViewMusic.searchCategoryId = catIdMusic
                    universalCollectionViewMusic.currentPage = currentPageMusic
                    universalCollectionViewMusic.offset = offsetMusic
                    universalCollectionViewMusic.arrayCollection = self.arrayMusic
                    universalCollectionViewMusic.showLoadingIndicator = false
                    universalCollectionViewMusic.displayRetryOption = true
                    universalCollectionViewMusic.reloadData()
                    
                }
                else
                {
                    universalCollectionViewMusic.arrayCollection = self.arrayMusic
                    universalCollectionViewMusic.showLoadingIndicator = false
                    universalCollectionViewMusic.displayRetryOption = true
                    universalCollectionViewMusic.noRecordsFoundTitle = alertMsg_NoData
                    universalCollectionViewMusic.reloadData()
                }
            }
            else
            {
                universalCollectionViewMusic.arrayCollection = self.arrayMusic
                universalCollectionViewMusic.showLoadingIndicator = false
                universalCollectionViewMusic.displayRetryOption = true
                universalCollectionViewMusic.noRecordsFoundTitle = alertMsg_NoData
                universalCollectionViewMusic.reloadData()
            }
        }
        else if (status == 0) //error response
        {
            universalCollectionViewMusic.arrayCollection = []
            universalCollectionViewMusic.showLoadingIndicator = false
            universalCollectionViewMusic.displayRetryOption = true
            universalCollectionViewMusic.noRecordsFoundTitle = swiftyJsonVar["response"].stringValue
            universalCollectionViewMusic.reloadData()
        }
        else
        {
            universalCollectionViewMusic.arrayCollection = []
            universalCollectionViewMusic.showLoadingIndicator = false
            universalCollectionViewMusic.displayRetryOption = true
            universalCollectionViewMusic.noRecordsFoundTitle = message
            universalCollectionViewMusic.reloadData()
        }
    }
    
    //MARK: - Get Video Games Response
    func responseGalleryVideoGames (_ notify: Foundation.Notification)
    {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFIACTION_GETGALLERYVIDEOGAMES), object: nil)
        
        let swiftyJsonVar = JSON(notify.object!)
        
        let message = swiftyJsonVar["message"].stringValue
        let status = swiftyJsonVar["status"].intValue
        
        if swiftyJsonVar [ERROR_KEY].exists()
        {
            universalCollectionViewVideoGames.arrayCollection = []
            universalCollectionViewVideoGames.showLoadingIndicator = false
            universalCollectionViewVideoGames.displayRetryOption = true
            universalCollectionViewVideoGames.noRecordsFoundTitle = swiftyJsonVar[ERROR_KEY].stringValue
            universalCollectionViewVideoGames.reloadData()
        }
        else if (status == 1) //success response
        {
            totalRecordsVideoGames = swiftyJsonVar["response"]["count"].intValue
            
            let userDetail = swiftyJsonVar["response"]["user_details"]
            refreshHeaderWithValues(userDetail)
            
            if totalRecordsVideoGames > 0
            {
                var arrFeeds = [Media]()
                arrFeeds = Parser.getParsedMediaArrayFromData(swiftyJsonVar["response"]["media"])
               
                
                if offsetVideoGames == 0
                {
                    arrayVideoGames.removeAll()
                }
                
                arrayVideoGames.append(contentsOf: arrFeeds)
                
                if arrayVideoGames.count>0
                {
                    universalCollectionViewVideoGames.totalRecordCount = totalRecordsVideoGames
                    universalCollectionViewVideoGames.searchCategoryId = catIdVideoGames
                    universalCollectionViewVideoGames.currentPage = currentPageVideoGames
                    universalCollectionViewVideoGames.offset = offsetVideoGames
                    universalCollectionViewVideoGames.arrayCollection = self.arrayVideoGames
                    universalCollectionViewVideoGames.showLoadingIndicator = false
                    universalCollectionViewVideoGames.displayRetryOption = true
                    universalCollectionViewVideoGames.reloadData()
                   
                }
                else
                {
                    universalCollectionViewVideoGames.arrayCollection = self.arrayVideoGames
                    universalCollectionViewVideoGames.showLoadingIndicator = false
                    universalCollectionViewVideoGames.noRecordsFoundTitle = alertMsg_NoData
                    universalCollectionViewVideoGames.displayRetryOption = true
                    universalCollectionViewVideoGames.reloadData()
                }
            }
            else
            {
                universalCollectionViewVideoGames.arrayCollection = self.arrayVideoGames
                universalCollectionViewVideoGames.showLoadingIndicator = false
                universalCollectionViewVideoGames.noRecordsFoundTitle = alertMsg_NoData
                universalCollectionViewVideoGames.displayRetryOption = true
                universalCollectionViewVideoGames.reloadData()
            }
        }
        else if (status == 0) //error response
        {
            universalCollectionViewVideoGames.arrayCollection = []
            universalCollectionViewVideoGames.showLoadingIndicator = false
            universalCollectionViewVideoGames.displayRetryOption = true
            universalCollectionViewVideoGames.noRecordsFoundTitle = swiftyJsonVar["response"].stringValue
            universalCollectionViewVideoGames.reloadData()
        }
        else
        {
            universalCollectionViewVideoGames.arrayCollection = []
            universalCollectionViewVideoGames.showLoadingIndicator = false
            universalCollectionViewVideoGames.displayRetryOption = true
            universalCollectionViewVideoGames.noRecordsFoundTitle = message
            universalCollectionViewVideoGames.reloadData()
        }
    }
    
    //MARK: - Get Photos Response
    func responseGalleryPhotos (_ notify: Foundation.Notification)
    {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFIACTION_GETGALLERYPHOTOS), object: nil)
        
        let swiftyJsonVar = JSON(notify.object!)
        
        let message = swiftyJsonVar["message"].stringValue
        let status = swiftyJsonVar["status"].intValue
        
        if swiftyJsonVar [ERROR_KEY].exists()
        {
            universalCollectionViewPhotos.arrayCollection = []
            universalCollectionViewPhotos.showLoadingIndicator = false
            universalCollectionViewPhotos.displayRetryOption = true
            universalCollectionViewPhotos.noRecordsFoundTitle = swiftyJsonVar[ERROR_KEY].stringValue
            universalCollectionViewPhotos.reloadData()
        }
        else if (status == 1) //success response
        {
            totalRecordsPhotos = swiftyJsonVar["response"]["count"].intValue
            
            let userDetail = swiftyJsonVar["response"]["user_details"]
            refreshHeaderWithValues(userDetail)
            
            if totalRecordsPhotos > 0
            {
                var arrFeeds = [Media]()
                arrFeeds = Parser.getParsedMediaArrayFromData(swiftyJsonVar["response"]["media"])
               
                
                if offsetPhotos == 0
                {
                    arrayPhotos.removeAll()
                }
                
                arrayPhotos.append(contentsOf: arrFeeds)
                
                if arrayPhotos.count>0
                {
                    universalCollectionViewPhotos.totalRecordCount = totalRecordsPhotos
                    universalCollectionViewPhotos.searchCategoryId = catIdPhotos
                    universalCollectionViewPhotos.currentPage = currentPagePhotos
                    universalCollectionViewPhotos.offset = offsetPhotos
                    universalCollectionViewPhotos.arrayCollection = self.arrayPhotos
                    universalCollectionViewPhotos.showLoadingIndicator = false
                    universalCollectionViewPhotos.displayRetryOption = true
                    universalCollectionViewPhotos.reloadData()
                    
                }
                else
                {
                    universalCollectionViewPhotos.arrayCollection = self.arrayPhotos
                    universalCollectionViewPhotos.showLoadingIndicator = false
                    universalCollectionViewPhotos.displayRetryOption = true
                    universalCollectionViewPhotos.noRecordsFoundTitle = alertMsg_NoData
                    universalCollectionViewPhotos.reloadData()
                }
            }
            else
            {
                universalCollectionViewPhotos.arrayCollection = self.arrayPhotos
                universalCollectionViewPhotos.showLoadingIndicator = false
                universalCollectionViewPhotos.displayRetryOption = true
                universalCollectionViewPhotos.noRecordsFoundTitle = alertMsg_NoData
                universalCollectionViewPhotos.reloadData()
            }
        }
        else if (status == 0) //error response
        {
            universalCollectionViewPhotos.arrayCollection = []
            universalCollectionViewPhotos.showLoadingIndicator = false
            universalCollectionViewPhotos.displayRetryOption = true
            universalCollectionViewPhotos.noRecordsFoundTitle = swiftyJsonVar["response"].stringValue
            universalCollectionViewPhotos.reloadData()
        }
        else
        {
            universalCollectionViewPhotos.arrayCollection = []
            universalCollectionViewPhotos.showLoadingIndicator = false
            universalCollectionViewPhotos.displayRetryOption = true
            universalCollectionViewPhotos.noRecordsFoundTitle = message
            universalCollectionViewPhotos.reloadData()
        }
    }
    
    //MARK: - Get PeepText Response
    func responseGalleryPeepText (_ notify: Foundation.Notification)
    {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFIACTION_GETGALLERYPEEPTEXT), object: nil)
        
        let swiftyJsonVar = JSON(notify.object!)
        
        let message = swiftyJsonVar["message"].stringValue
        let status = swiftyJsonVar["status"].intValue
        
        printCustom("swiftyJsonVar [ERROR_KEY]:\(swiftyJsonVar [ERROR_KEY])")
        
        if swiftyJsonVar [ERROR_KEY].exists()
        {
            universalCollectionViewPeepText.arrayCollection = []
            universalCollectionViewPeepText.showLoadingIndicator = false
            universalCollectionViewPeepText.displayRetryOption = true
            universalCollectionViewPeepText.noRecordsFoundTitle = swiftyJsonVar[ERROR_KEY].stringValue
            universalCollectionViewPeepText.reloadData()
        }
        else if (status == 1) //success response
        {
            totalRecordsPeepText = swiftyJsonVar["response"]["count"].intValue
            
            let userDetail = swiftyJsonVar["response"]["user_details"]
            refreshHeaderWithValues(userDetail)
            
            if totalRecordsPeepText > 0
            {
                var arrFeeds = [Media]()
                arrFeeds = Parser.getParsedMediaArrayFromData(swiftyJsonVar["response"]["media"])
              
                
                if offsetPeepText == 0
                {
                    arrayPeepText.removeAll()
                }
                
                arrayPeepText.append(contentsOf: arrFeeds)
                
                if arrayPeepText.count>0
                {
                    universalCollectionViewPeepText.totalRecordCount = totalRecordsPeepText
                    universalCollectionViewPeepText.searchCategoryId = catIdPeepText
                    universalCollectionViewPeepText.currentPage = currentPagePeepText
                    universalCollectionViewPeepText.offset = offsetPeepText
                    universalCollectionViewPeepText.arrayCollection = self.arrayPeepText
                    universalCollectionViewPeepText.showLoadingIndicator = false
                    universalCollectionViewPeepText.displayRetryOption = true
                    universalCollectionViewPeepText.galleryType = galleryType
                    universalCollectionViewPeepText.reloadData()
                    
                }
                else
                {
                    universalCollectionViewPeepText.arrayCollection = self.arrayPeepText
                    universalCollectionViewPeepText.showLoadingIndicator = false
                    universalCollectionViewPeepText.displayRetryOption = true
                    universalCollectionViewPeepText.noRecordsFoundTitle = alertMsg_NoData
                    universalCollectionViewPeepText.reloadData()
                }
            }
            else
            {
                universalCollectionViewPeepText.arrayCollection = self.arrayPeepText
                universalCollectionViewPeepText.showLoadingIndicator = false
                universalCollectionViewPeepText.displayRetryOption = true
                universalCollectionViewPeepText.noRecordsFoundTitle = alertMsg_NoData
                universalCollectionViewPeepText.reloadData()
            }
        }
        else if (status == 0) //error response
        {
            universalCollectionViewPeepText.arrayCollection = []
            universalCollectionViewPeepText.showLoadingIndicator = false
            universalCollectionViewPeepText.displayRetryOption = true
            universalCollectionViewPeepText.noRecordsFoundTitle = swiftyJsonVar["response"].stringValue
            universalCollectionViewPeepText.reloadData()
        }
        else
        {
            universalCollectionViewPeepText.arrayCollection = []
            universalCollectionViewPeepText.showLoadingIndicator = false
            universalCollectionViewPeepText.displayRetryOption = true
            universalCollectionViewPeepText.noRecordsFoundTitle = message
            universalCollectionViewPeepText.reloadData()
        }
    }
    
    
    // MARK:- UpdateContent delegate
   
    func reloadMediaGallery(_ categoryId:Int) {
        
        //Category Id (1 Music 2 Movie 3 Photo 4 Video Game

        if categoryId == 1 //Music
        {
            self.catIdMusic = categoryId
            self.currentPageMusic = 1
            self.offsetMusic = 0
        }
        else if categoryId == 2 //Movie
        {
            self.catIdMovies = categoryId
            self.currentPageMovies = 1
            self.offsetMovies = 0
        }
        else if categoryId == 3 //Photos
        {
            self.catIdPhotos = categoryId
            self.currentPagePhotos = 1
            self.offsetPhotos = 0
        }
        else if categoryId == 4 //Video Games
        {
            self.catIdVideoGames = categoryId
            self.currentPageVideoGames = 1
            self.offsetVideoGames = 0
        }
        else if categoryId == -1 //PeepText
        {
            //self.catIdPeepText = categoryId
            self.currentPagePeepText = 1
            self.offsetPeepText = 0

        }
    
        self.getGalleryMedia(categoryId)
    }
    
    func loadMoreMediaGallery(_ page:NSInteger,offset:Int, categoryId:Int){
        
        //Category Id (1 Music 2 Movie 3 Photo 4 Video Game
        
        if categoryId == 1 //Music
        {
            self.catIdMusic = categoryId
            self.currentPageMusic = page
            self.offsetMusic = offset
        }
        else if categoryId == 2 //Movie
        {
            self.catIdMovies = categoryId
            self.currentPageMovies = page
            self.offsetMovies = offset
        }
        else if categoryId == 3 //Photos
        {
            self.catIdPhotos = categoryId
            self.currentPagePhotos = page
            self.offsetPhotos = offset
        }
        else if categoryId == 4 //Video Games
        {
            self.catIdVideoGames = categoryId
            self.currentPageVideoGames = page
            self.offsetVideoGames = offset
        }
        else if categoryId == 5 //PeepText
        {
            self.catIdPeepText = categoryId
            self.currentPagePeepText = page
            self.offsetPeepText = offset
        }
        
        self.getGalleryMedia(categoryId)
    }
    
    //MARK:- Move to Single detail or comments screen
    func moveToNextScreenGallery(_ ScreenName:String, WithData:AnyObject)
    {
        
        if(ScreenName == "SingleDetailVc")
        {
            self.navigationItem.title = ""
            let st = UIStoryboard(name: "Home", bundle:nil)
            let singleDetailVc = st.instantiateViewController(withIdentifier: "SingleDetailVc") as! SingleDetailVc
            
            var arr = [Media]()
            arr.append(WithData as! Media)
            singleDetailVc.arrMedia = arr
            self.navigationController?.pushViewController(singleDetailVc, animated: true)
        }
        else if(ScreenName == "PeepTextDetailCollectionViewController")
        {
            self.navigationItem.title = ""
            let st = UIStoryboard(name: "Gallery", bundle:nil)
            let peepTextDetailVc = st.instantiateViewController(withIdentifier: "PeepTextDetailCollectionViewController") as! PeepTextDetailCollectionViewController
            let media = (WithData as! Media)
            if galleryType.rawValue == 1
            {
                peepTextDetailVc.otherUserId = media.pSentUserId
                peepTextDetailVc.otherUserName = media.pSentUserName

            }
            else{
                peepTextDetailVc.otherUserId = media.pUserId
                peepTextDetailVc.otherUserName = media.pUserName

            }
            peepTextDetailVc.galleryType = .peepDetailGallery
            self.navigationController?.pushViewController(peepTextDetailVc, animated: true)
        }
            
        else if(ScreenName == "CommentVc")
        {
            self.navigationItem.title = ""
            let st = UIStoryboard(name: "Home", bundle:nil)
            let commentObject = st.instantiateViewController(withIdentifier: "CommentVc") as! CommentVc
            
            commentObject.md = WithData as! Media
            self.navigationController?.pushViewController(commentObject, animated: true)
        }
        
        else if(ScreenName == "GalleryMainViewController")
        {
             self.navigationItem.title = ""
            let st = UIStoryboard(name: "Gallery", bundle:nil)
            let galleryObject = st.instantiateViewController(withIdentifier: "GalleryMainViewController") as? GalleryMainViewController
            let md = WithData as! Media
            galleryObject?.otherUserId = md.pUserId
            self.navigationController?.pushViewController(galleryObject!, animated: true)
        }
        
        else if(ScreenName == "LikersViewController")
        {
            self.navigationItem.title = ""
            let st = UIStoryboard(name: "Gallery", bundle:nil)
            let likerObject = st.instantiateViewController(withIdentifier: "LikersViewController") as! LikersViewController
            let peopleObj = WithData as! Media
            likerObject.mediaId = peopleObj.pId
            likerObject.likerMediaType = likerMediaType
            
            likerObject.screenType = .likersList
            
            self.navigationController?.pushViewController(likerObject, animated: true)

        }
        else if(ScreenName == "DisLikersViewController")
        {
            self.navigationItem.title = ""
            let st = UIStoryboard(name: "Gallery", bundle:nil)
            let likerObject = st.instantiateViewController(withIdentifier: "LikersViewController") as! LikersViewController
            let peopleObj = WithData as! Media
            likerObject.mediaId = peopleObj.pId
            likerObject.likerMediaType = likerMediaType
            likerObject.screenType = .dislikersList
            
            self.navigationController?.pushViewController(likerObject, animated: true)
            
        }
        else if (ScreenName == "ForwardMedia")
        {
            let st = UIStoryboard(name: "Digital", bundle:nil)
            let obj = st.instantiateViewController(withIdentifier: "ContactsScreen") as! ContactsScreen
            let md = WithData as! Media
            obj.fwdMediaId = md.pId
            obj.isComingFromForwardMedia = true
            self.navigationController?.pushViewController(obj, animated: true)
        }
        
    }
    
    //MARK:- Button Actions
    @IBAction func selectButton(_ sender: UIButton)
    {
        let btn = sender
        
        selectedIndex = btn.tag
        
        reloadSegment()
    }
    
    //MARK:- Reload Segment
    func reloadSegment()
    {
        likerMediaType = LikerMediaType.gallery
        
        var index = -1

        if selectedIndex == 11 //Movies
        {
            offsetMovies = 0
            currentPageMovies = 1
            index = 0
            
            btnPhotos.setImage(Utilities().themedImage(img_gallerySearchGallery), for: UIControlState())
            btnMusic.setImage(Utilities().themedImage(img_musicSearchGallery), for: UIControlState())
            btnMovies.setImage(Utilities().themedImage(img_filmSelectedSearchGallery), for: UIControlState())
            btnVideoGames.setImage(Utilities().themedImage(img_gamesSearchGallery), for: UIControlState())
            btnPeepText.setImage(Utilities().themedImage(img_peepTextSearchGallery), for: UIControlState())
            
            if(universalCollectionViewPhotos != nil)
            {
                universalCollectionViewPhotos.isHidden = true
            }
            
            if(universalCollectionViewVideoGames != nil)
            {
                universalCollectionViewVideoGames.isHidden = true
            }
            
            if universalCollectionViewMusic != nil
            {
                universalCollectionViewMusic.isHidden = true
            }
            
            if(universalCollectionViewPeepText != nil)
            {
                universalCollectionViewPeepText.isHidden = true
            }
            
            if universalCollectionViewMovies == nil || isSecondTabLoaded == false
            {
                let collectinLayout = UICollectionViewFlowLayout()
                universalCollectionViewMovies = UniversalCollectionView.init(frame: self.view.frame, collectionViewLayout: collectinLayout, catId: catIdMovies, del: self, array: arrayMedia, header: headerView, displayType:displayListView, galleryType:galleryType)
                universalCollectionViewMovies.displayRetryOption = true
                self.collectionView = universalCollectionViewMovies
                isSecondTabLoaded = true
                
                self.reloadMediaGallery(catIdMovies)
            }
            else
            {
                self.universalCollectionViewMovies.setContentOffset(CGPoint.zero, animated: true)
                universalCollectionViewMovies.isHidden = false
                self.collectionView = universalCollectionViewMovies
                self.universalCollectionViewMovies.headerCollection = headerView
                self.universalCollectionViewMovies.arrayCollection = arrayMedia
                self.universalCollectionViewMovies.displayListView = displayListView
                self.universalCollectionViewMovies.displayRetryOption = true
                self.universalCollectionViewMovies.reloadData()
                
                if arrayMedia.count <= 0
                {
                    self.reloadMediaGallery(catIdMovies)
                }
            }
                    }
        else if selectedIndex == 12  //Music
        {
            offsetMusic = 0
            currentPageMusic = 1
            index = 1
            
            btnPhotos.setImage(Utilities().themedImage(img_gallerySearchGallery), for: UIControlState())
            btnMusic.setImage(Utilities().themedImage(img_musicSelectedSearchGallery), for: UIControlState())
            btnMovies.setImage(Utilities().themedImage(img_filmSearchGallery), for: UIControlState())
            btnVideoGames.setImage(Utilities().themedImage(img_gamesSearchGallery), for: UIControlState())
            btnPeepText.setImage(Utilities().themedImage(img_peepTextSearchGallery), for: UIControlState())
            
            
            if(universalCollectionViewPhotos != nil)
            {
                universalCollectionViewPhotos.isHidden = true
            }
            
            if(universalCollectionViewMovies != nil)
            {
                universalCollectionViewMovies.isHidden = true
            }
            
            if(universalCollectionViewVideoGames != nil)
            {
                universalCollectionViewVideoGames.isHidden = true
            }
            
            if(universalCollectionViewPeepText != nil)
            {
                universalCollectionViewPeepText.isHidden = true
            }
            
            if universalCollectionViewMusic == nil || isFourthTabLoaded == false
            {
                let collectinLayout = UICollectionViewFlowLayout()
                universalCollectionViewMusic = UniversalCollectionView.init(frame: self.view.frame, collectionViewLayout: collectinLayout, catId: catIdMusic, del: self, array: arrayMusic, header: headerView,displayType:displayListView, galleryType:galleryType)
                //send value to indicate music tab selected
                universalCollectionViewMusic.displayRetryOption = true
                universalCollectionViewMusic.isMusicTabSelected = true
                self.collectionView = universalCollectionViewMusic
                isFourthTabLoaded = true
                
                self.reloadMediaGallery(catIdMusic)
            }
            else
            {
                self.universalCollectionViewMusic.setContentOffset(CGPoint.zero, animated: true)
                universalCollectionViewMusic.isHidden = false
                self.collectionView = universalCollectionViewMusic
                self.universalCollectionViewMusic.headerCollection = headerView
                self.universalCollectionViewMusic.arrayCollection = arrayMusic
                self.universalCollectionViewMusic.displayListView = displayListView
                self.universalCollectionViewMusic.displayRetryOption = true
                self.universalCollectionViewMusic.reloadData()
                
                if arrayMusic.count <= 0
                {
                    self.reloadMediaGallery(catIdMusic)
                }
            }
        }
        else if selectedIndex == 13  //Video Games
        {
            offsetVideoGames = 0
            currentPageVideoGames = 1
            index = 2
            
            btnPhotos.setImage(Utilities().themedImage(img_gallerySearchGallery), for: UIControlState())
            btnMusic.setImage(Utilities().themedImage(img_musicSearchGallery), for: UIControlState())
            btnMovies.setImage(Utilities().themedImage(img_filmSearchGallery), for: UIControlState())
            btnVideoGames.setImage(Utilities().themedImage(img_gamesSelectedSearchGallery), for: UIControlState())
            btnPeepText.setImage(Utilities().themedImage(img_peepTextSearchGallery), for: UIControlState())
            
            if(universalCollectionViewPhotos != nil)
            {
                universalCollectionViewPhotos.isHidden = true
            }
            
            if(universalCollectionViewMovies != nil)
            {
                universalCollectionViewMovies.isHidden = true
            }
            
            if universalCollectionViewMusic != nil
            {
                universalCollectionViewMusic.isHidden = true
            }
            
            if(universalCollectionViewPeepText != nil)
            {
                universalCollectionViewPeepText.isHidden = true
            }
            
            if universalCollectionViewVideoGames == nil || isThirdTabLoaded == false
            {
                let collectinLayout = UICollectionViewFlowLayout()
                universalCollectionViewVideoGames = UniversalCollectionView.init(frame: self.view.frame, collectionViewLayout: collectinLayout, catId: catIdVideoGames, del: self, array: arrayVideoGames, header: headerView, displayType:displayListView, galleryType:galleryType)
                universalCollectionViewVideoGames.displayRetryOption = true
                self.collectionView = universalCollectionViewVideoGames
                isThirdTabLoaded = true
                
                self.reloadMediaGallery(catIdVideoGames)
            }
            else
            {
                self.universalCollectionViewVideoGames.setContentOffset(CGPoint.zero, animated: true)
                self.universalCollectionViewVideoGames.isHidden = false
                self.collectionView = universalCollectionViewVideoGames
                self.universalCollectionViewVideoGames.headerCollection = headerView
                self.universalCollectionViewVideoGames.arrayCollection = arrayVideoGames
                self.universalCollectionViewVideoGames.displayListView = displayListView
                self.universalCollectionViewVideoGames.displayRetryOption = true
                self.universalCollectionViewVideoGames.reloadData()
                
                if arrayVideoGames.count <= 0
                {
                    self.reloadMediaGallery(catIdVideoGames)
                }

            }
        }
        else if selectedIndex == 14  //Photos
        {
            offsetPhotos = 0
            currentPagePhotos = 1
            index = 3
            
            btnPhotos.setImage(Utilities().themedImage(img_gallerySelectedSearchGallery), for: UIControlState())
            btnMusic.setImage(Utilities().themedImage(img_musicSearchGallery), for: UIControlState())
            btnMovies.setImage(Utilities().themedImage(img_filmSearchGallery), for: UIControlState())
            btnVideoGames.setImage(Utilities().themedImage(img_gamesSearchGallery), for: UIControlState())
            btnPeepText.setImage(Utilities().themedImage(img_peepTextSearchGallery), for: UIControlState())
            
            if(universalCollectionViewVideoGames != nil)
            {
                universalCollectionViewVideoGames.isHidden = true
            }
            
            if universalCollectionViewMusic != nil
            {
                universalCollectionViewMusic.isHidden = true
            }
            
            if universalCollectionViewMovies != nil
            {
                universalCollectionViewMovies.isHidden = true
            }
            
            if universalCollectionViewPeepText != nil
            {
                universalCollectionViewPeepText.isHidden = true
            }
            
            if universalCollectionViewPhotos == nil
            {
                let collectinLayout = UICollectionViewFlowLayout()
                universalCollectionViewPhotos = UniversalCollectionView.init(frame: self.view.frame, collectionViewLayout: collectinLayout, catId: catIdPhotos, del: self, array: arrayPhotos, header: headerView, displayType:displayListView, galleryType:galleryType)
                universalCollectionViewPhotos.displayRetryOption = true
                self.collectionView = universalCollectionViewPhotos
                
                self.reloadMediaGallery(catIdPhotos)
            }
            else
            {
                self.universalCollectionViewPhotos.setContentOffset(CGPoint.zero, animated: true)
                self.universalCollectionViewPhotos.isHidden = false
                self.collectionView = universalCollectionViewPhotos
                self.universalCollectionViewPhotos.headerCollection = headerView
                self.universalCollectionViewPhotos.arrayCollection = arrayPhotos
                self.universalCollectionViewPhotos.displayListView = displayListView
                self.universalCollectionViewPhotos.displayRetryOption = true
                self.universalCollectionViewPhotos.reloadData()
                
                if arrayPhotos.count <= 0
                {
                    self.reloadMediaGallery(catIdPhotos)
                }
            }
        }
        else if selectedIndex == 15 //Peep Text
        {
            likerMediaType = LikerMediaType.peepText
            offsetPeepText = 0
            currentPagePeepText = 1
            index = 4
            
            btnPhotos.setImage(Utilities().themedImage(img_gallerySearchGallery), for: UIControlState())
            btnMusic.setImage(Utilities().themedImage(img_musicSearchGallery), for: UIControlState())
            btnMovies.setImage(Utilities().themedImage(img_filmSearchGallery), for: UIControlState())
            btnVideoGames.setImage(Utilities().themedImage(img_gamesSearchGallery), for: UIControlState())
            btnPeepText.setImage(Utilities().themedImage(img_peepTextSelectedSearchGallery), for: UIControlState())
            
            if(universalCollectionViewPhotos != nil)
            {
                universalCollectionViewPhotos.isHidden = true
            }
            
            if(universalCollectionViewMovies != nil)
            {
                universalCollectionViewMovies.isHidden = true
            }
            
            if(universalCollectionViewVideoGames != nil)
            {
                universalCollectionViewVideoGames.isHidden = true
            }
            
            if(universalCollectionViewMusic != nil)
            {
                universalCollectionViewMusic.isHidden = true
            }
            
            if universalCollectionViewPeepText == nil || isFifthTabLoaded == false
            {
                let collectinLayout = UICollectionViewFlowLayout()
                self.universalCollectionViewPeepText = UniversalCollectionView.init(frame: self.view.frame, collectionViewLayout: collectinLayout, catId: catIdPeepText, del: self, array: arrayPeepText, header: headerView,displayType:displayListView, galleryType:galleryType)
                self.universalCollectionViewPeepText.displayPeepText = true
                self.universalCollectionViewPeepText.displayRetryOption = true
                self.collectionView = universalCollectionViewPeepText
                isFifthTabLoaded = true
                self.reloadMediaGallery(catIdPeepText)
            }
            else
            {
                
                self.universalCollectionViewPeepText.setContentOffset(CGPoint.zero, animated: true)
                self.universalCollectionViewPeepText.isHidden = false
                self.collectionView = universalCollectionViewPeepText
                self.universalCollectionViewPeepText.headerCollection = headerView
                self.universalCollectionViewPeepText.arrayCollection = arrayPeepText
                self.universalCollectionViewPeepText.displayListView = displayListView
                self.universalCollectionViewPeepText.displayPeepText = true
                self.universalCollectionViewPeepText.displayRetryOption = true
                self.universalCollectionViewPeepText.reloadData()
                
                if arrayPeepText.count <= 0
                {
                    self.reloadMediaGallery(catIdPeepText)
                }
            }
        }
        
        if leadingConstraintSelectionView != nil {
            leadingConstraintSelectionView.constant = (btnMovies.frame.size.width * (CGFloat(index))) + CGFloat(index)
        }

    }
    
    // MARK:- UICollectionViewDataSource
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "collectionHeader", for: indexPath)
        image_user = headerView.viewWithTag(1) as! UIImageView
        lblPostcount = headerView.viewWithTag(2) as! UIButton!
        lblFollowerscount = headerView.viewWithTag(3) as! UIButton!
        lblFollowingscount = headerView.viewWithTag(4) as! UIButton!
        label_user_name = headerView.viewWithTag(5) as! UILabel!
        lblTagLine = headerView.viewWithTag(6) as! UILabel!
        btnGridView = headerView.viewWithTag(7) as! UIButton!
        btnListView = headerView.viewWithTag(8) as! UIButton!
        btnMyGallery = headerView.viewWithTag(9) as! UIButton!
        btnAllGallery = headerView.viewWithTag(10) as! UIButton!
        
        btnMovies = headerView.viewWithTag(11) as! UIButton!
        btnMusic = headerView.viewWithTag(12) as! UIButton!
        btnVideoGames = headerView.viewWithTag(13) as! UIButton!
        btnPhotos = headerView.viewWithTag(14) as! UIButton!
        btnPeepText = headerView.viewWithTag(15) as! UIButton!
        vwSelection = headerView.viewWithTag(101) as UIView!

        let topView = headerView.viewWithTag(100) as UIView!
        
        for constraint in topView?.constraints as! [NSLayoutConstraint] {
            
            if constraint.identifier == "LeadingConstraintSelectionViewId" {
                leadingConstraintSelectionView = constraint
            }
        }
        
        printCustom("topView.constraints:\(self.view.constraints)")
        
        for constraint in headerView.constraints as [NSLayoutConstraint] {
            
            if constraint.identifier == "topViewTopSpaceWithMyGallery" {
                topViewTopSpaceWithMyGallery = constraint
            }
            
            if constraint.identifier == "topViewTopSpaceWithAllGallery" {
                topViewTopSpaceWithAllGallery = constraint
            }
            
            if constraint.identifier == "topViewTopSpaceWithGridButton" {
                topViewTopSpaceWithGridButton = constraint
            }
            
            if constraint.identifier == "topViewTopSpaceWithListButton" {
                topViewTopSpaceWithListButton = constraint
            }
            
            
            //If My profile/Galley is open
            if otherUserId == -1 || otherUserId == Int(Utilities.getUserDetails().id)
            {
                btnAllGallery.isHidden = false
                btnMyGallery.isHidden = false
                
                if topViewTopSpaceWithMyGallery != nil
                {
                    topViewTopSpaceWithMyGallery.priority = UILayoutPriority(rawValue: 999)
                }
                
                if topViewTopSpaceWithAllGallery != nil
                {
                    topViewTopSpaceWithAllGallery.priority = UILayoutPriority(rawValue: 999)
                }
                
                if topViewTopSpaceWithGridButton != nil
                {
                    topViewTopSpaceWithGridButton.priority = UILayoutPriority(rawValue: 900)
                }
                
                if topViewTopSpaceWithListButton != nil
                {
                    topViewTopSpaceWithListButton.priority = UILayoutPriority(rawValue: 900)
                }
            }
            else //If Other user profile/Galley is open
            {
                btnAllGallery.isHidden = true
                btnMyGallery.isHidden = true
                
                if topViewTopSpaceWithMyGallery != nil
                {
                    topViewTopSpaceWithMyGallery.priority = UILayoutPriority(rawValue: 900)
                }
                
                if topViewTopSpaceWithAllGallery != nil
                {
                    topViewTopSpaceWithAllGallery.priority = UILayoutPriority(rawValue: 900)
                }
                
                if topViewTopSpaceWithGridButton != nil
                {
                    topViewTopSpaceWithGridButton.priority = UILayoutPriority(rawValue: 999)
                }
                
                if topViewTopSpaceWithListButton != nil
                {
                    topViewTopSpaceWithListButton.priority = UILayoutPriority(rawValue: 999)
                }
            }
        }
        
        vwSelection.backgroundColor = Utilities().themedMultipleColor()[0]
        btnMyGallery.backgroundColor = Utilities().themedMultipleColor()[1]//UIColor(red: 128.0/255.0, green: 0.0/255.0, blue: 128.0/255.0, alpha: 1.0)
        lblPostcount.setBackgroundImage(Utilities().themedImage(img_galleryBtnGallery), for: UIControlState())
        lblFollowerscount.setBackgroundImage(Utilities().themedImage(img_galleryBtnGallery), for: UIControlState())
        lblFollowingscount.setBackgroundImage(Utilities().themedImage(img_galleryBtnGallery), for: UIControlState())

        image_user.layer.masksToBounds = false
        image_user.layer.cornerRadius = image_user.frame.height/2
        image_user.clipsToBounds = true
        
        btnGridView.setImage(Utilities().themedImage(img_gridSelected), for: UIControlState())
        btnListView.setImage(Utilities().themedImage(img_list), for: UIControlState())
        
        lblPostcount.addTarget(self, action: #selector(self.navigateToMyPostsScreen(_:)), for: .touchUpInside)
        lblFollowerscount.addTarget(self, action: #selector(self.navigateToFollowerListScreen(_:)), for: .touchUpInside)
        lblFollowingscount.addTarget(self, action: #selector(self.navigateToFollowingListScreen(_:)), for: .touchUpInside)
        
        btnMovies.addTarget(self, action: #selector(self.selectButton(_:)), for: .touchUpInside)
        btnMusic.addTarget(self, action: #selector(self.selectButton(_:)), for: .touchUpInside)
        btnVideoGames.addTarget(self, action: #selector(self.selectButton(_:)), for: .touchUpInside)
        btnPhotos.addTarget(self, action: #selector(self.selectButton(_:)), for: .touchUpInside)
        btnPeepText.addTarget(self, action: #selector(self.selectButton(_:)), for: .touchUpInside)
        
        
        btnGridView.addTarget(self, action: #selector(self.headerbuttonTapped(_:)), for: .touchUpInside)
        btnListView.addTarget(self, action: #selector(self.headerbuttonTapped(_:)), for: .touchUpInside)
        btnMyGallery.addTarget(self, action: #selector(self.headerbuttonTapped(_:)), for: .touchUpInside)
        btnAllGallery.addTarget(self, action: #selector(self.headerbuttonTapped(_:)), for: .touchUpInside)

        //if gallery screen is opened for other user then reduce collection header height
        //as we are hiding My Gallery & All Gallery buttons
        if otherUserId != -1 && otherUserId != Int(Utilities.getUserDetails().id)
        {
            let hh = btnMyGallery.frame.size.height
            
            let frm = CGRect(x: headerView.frame.origin.x, y: headerView.frame.origin.y, width: headerView.frame.size.width,height: headerView.frame.size.height-hh)
            
            headerView.frame = frm
        }
        
        self.reloadSegment()
        
        return headerView
    }
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if headerView != nil
        {
            return arrayMedia.count
        }
        else
        {
            return 0
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            let cell:UICollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "universal_cell", for: indexPath)
            return cell
    }
    
    //MARK:- navigate to my posts screen
    func navigateToMyPostsScreen(_ sender: UIButton){
     
         self.navigationItem.title = ""
        
        let editProfileViewController = UIStoryboard(name: "ProfileStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "PostsLikedId") as? PostsLikedViewController
        
        editProfileViewController!.myposts = 1
        
        if otherUserId == -1 || otherUserId == Int(Utilities.getUserDetails().id)!
        {
            editProfileViewController!.userId = Int(Utilities.getUserDetails().id)!
        }
        else
        {
            editProfileViewController!.userId = otherUserId
        }

        
        self.navigationController?.pushViewController(editProfileViewController!, animated: true)
        
}
    
    //MARK:- navigate to followers list screen
    func navigateToFollowerListScreen(_ sender: UIButton){
    
        self.navigationItem.title = ""
        
        let st = UIStoryboard(name: "Gallery", bundle:nil)
        let likersVc = st.instantiateViewController(withIdentifier: "LikersViewController") as! LikersViewController
        likersVc.screenType = .followersList
        
        if otherUserId == -1 || otherUserId == Int(Utilities.getUserDetails().id)!
        {
            likersVc.userId = Int(Utilities.getUserDetails().id)!
        }
        else
        {
            likersVc.userId = otherUserId
        }
        
        self.navigationController?.pushViewController(likersVc, animated: true)
           
    }
    
    
    //MARK:- navigate to following users list screen
    func navigateToFollowingListScreen(_ sender: UIButton){
        
        self.navigationItem.title = ""
        
        let st = UIStoryboard(name: "Gallery", bundle:nil)
        let likersVc = st.instantiateViewController(withIdentifier: "LikersViewController") as! LikersViewController
        likersVc.screenType = .followingsList
        
        if otherUserId == -1 || otherUserId == Int(Utilities.getUserDetails().id)!
        {
            likersVc.userId = Int(Utilities.getUserDetails().id)!
        }
        else
        {
            likersVc.userId = otherUserId
        }
        
        self.navigationController?.pushViewController(likersVc, animated: true)
        
    }
    
    //MARK:- Refresh Collection Header
    func refreshHeaderWithValues(_ userDetail:JSON){
        
        //username
        label_user_name.text = userDetail["username"].stringValue
        label_user_name.font = ProximaNovaRegular(16)
        
        //tagline
        if (userDetail["tagline"].stringValue.isEmpty == false){
            lblTagLine.text = userDetail["tagline"].stringValue
        }
        else{
            lblTagLine.text = "Hey i am using PECX!"
        }
        lblTagLine.font = ProximaNovaRegular(16)
        
        //post count
        var pCount = ""
        
        if userDetail["posts_count"].intValue == 1 || userDetail["posts_count"].intValue == 0
        {
           pCount = String.init(format: "%@ Post", userDetail["posts_count"].stringValue)
        }
        else
        {
            pCount = String.init(format: "%@ Posts", userDetail["posts_count"].stringValue)
        }
        
        lblPostcount.titleLabel?.font = ProximaNovaRegular(12)
        lblPostcount.setTitle(pCount, for: UIControlState())
        
        //followers count
        
        var pFollowerCount = ""
        
        if userDetail["followers_count"].intValue == 1 || userDetail["followers_count"].intValue == 0
        {
            pFollowerCount = String.init(format: "%@ Follower", userDetail["followers_count"].stringValue)
        }
        else
        {
            pFollowerCount = String.init(format: "%@ Followers", userDetail["followers_count"].stringValue)
        }

        lblFollowerscount.titleLabel?.font = ProximaNovaRegular(12)
        lblFollowerscount.setTitle(pFollowerCount, for: UIControlState())
        
        
        //following count
        
        var pFollowingCount = ""
        
        pFollowingCount = String.init(format: "%@ Following", userDetail["followings_count"].stringValue)
        
        /*
        if userDetail["followings_count"].intValue == 1 || userDetail["followings_count"].intValue == 0
        {
            pFollowingCount = String.init(format: "%@ Following", userDetail["followings_count"].stringValue)
        }
        else
        {
            pFollowingCount = String.init(format: "%@ Followings", userDetail["followings_count"].stringValue)
        }*/

        lblFollowingscount.titleLabel?.font = ProximaNovaRegular(12)
        lblFollowingscount.setTitle(pFollowingCount, for: UIControlState())
        
        let imgURL  = userDetail["user_image"].stringValue
        
        image_user!.image = Utilities().themedImage(img_defaultUser)
        
        if imgURL.isEmpty == false{
            
            //Downloading user's image
            URLSession.shared.dataTask(with: URL(string: imgURL)!, completionHandler: { (data, response, error) -> Void in
                
                if error != nil {
                    
                   self.image_user!.image = Utilities().themedImage(img_defaultUser)
                    
                    print(error)
                    return
                }
                
                DispatchQueue.main.async(execute: { () -> Void in
                    let image = UIImage(data: data!)
                    if (image == nil)
                    {
                        self.image_user!.image = Utilities().themedImage(img_defaultUser)
                    }
                    else
                    {
                        self.image_user!.image = image
                    }
                })
            }).resume()
        }

    }
    
    //MARK:- Header Buttons Click
    func headerbuttonTapped(_ sender: AnyObject)
    {
        let button = sender as! UIButton
    
        if button.tag == 7
        {
            //Grid View
            displayListView = false
            
            btnGridView.setImage(Utilities().themedImage(img_gridSelected), for: UIControlState())
            btnListView.setImage(Utilities().themedImage(img_list), for: UIControlState())
            
            reloadSegment()
        }
        else if button.tag == 8
        {
            //List View
            displayListView = true
            
            btnGridView.setImage(Utilities().themedImage(img_grid), for: UIControlState())
            btnListView.setImage(Utilities().themedImage(img_listSelected), for: UIControlState())
            
            reloadSegment()
        }
        else if button.tag == 9
        {
            //My Gallery
            
            btnMyGallery.setTitleColor(UIColor.white, for: UIControlState())
            btnMyGallery.backgroundColor = Utilities().themedMultipleColor()[1]//UIColor(red: 128.0/255.0, green: 0.0/255.0, blue: 128.0/255.0, alpha: 1.0)
            
            btnAllGallery.setTitleColor(UIColor.darkGray, for: UIControlState())
            btnAllGallery.backgroundColor = UIColor(red: 228.0/255.0, green: 228.0/255.0, blue: 228.0/255.0, alpha: 1.0)
            
            galleryType = .myGallery
            
            if selectedIndex == 11 //Movies
            {
                arrayMedia.removeAll()
                self.reloadMediaGallery(catIdMovies)
            }
            else if selectedIndex == 12 //Music
            {
                arrayMusic.removeAll()
                self.reloadMediaGallery(catIdMusic)
            }
            else if selectedIndex == 13 //Video Games
            {
                arrayVideoGames.removeAll()
                self.reloadMediaGallery(catIdVideoGames)
            }
            else if selectedIndex == 14 //Photos
            {
                arrayPhotos.removeAll()
                self.reloadMediaGallery(catIdPhotos)
            }
            else if selectedIndex == 15 //PeepText
            {
                arrayPeepText.removeAll()
                self.reloadMediaGallery(catIdPeepText)
            }
            
        }
        else if button.tag == 10
        {
            //All Gallery
            
            btnMyGallery.setTitleColor(UIColor.darkGray, for: UIControlState())
            btnMyGallery.backgroundColor = UIColor(red: 228.0/255.0, green: 228.0/255.0, blue: 228.0/255.0, alpha: 1.0)
            
            btnAllGallery.setTitleColor(UIColor.white, for: UIControlState())
            btnAllGallery.backgroundColor = Utilities().themedMultipleColor()[1]//UIColor(red: 128.0/255.0, green: 0.0/255.0, blue: 128.0/255.0, alpha: 1.0)
            
            galleryType = .allGallery
            
            if selectedIndex == 11 //Movies
            {
                arrayMedia.removeAll()
                self.reloadMediaGallery(catIdMovies)
            }
            else if selectedIndex == 12 //Music
            {
                arrayMusic.removeAll()
                self.reloadMediaGallery(catIdMusic)
            }
            else if selectedIndex == 13 //Video Games
            {
                arrayVideoGames.removeAll()
                self.reloadMediaGallery(catIdVideoGames)
            }
            else if selectedIndex == 14 //Photos
            {
                arrayPhotos.removeAll()
                self.reloadMediaGallery(catIdPhotos)
            }
            else if selectedIndex == 15 //PeepText
            {
                arrayPeepText.removeAll()
                self.reloadMediaGallery(catIdPeepText)
            }
        }
    }
}
