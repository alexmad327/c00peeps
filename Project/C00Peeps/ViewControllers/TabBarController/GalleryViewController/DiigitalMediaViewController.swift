//
//  ViewController.swift
//  PhotoVideoFilter
//
//  Created by Mac on 5/09/16.
//  Copyright © 2016 Gagandeep Bawa. All rights reserved.
//

import UIKit

class DiigitalMediaViewController: BaseVC, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    
    @IBOutlet weak var imgBackground: UIImageView!
    @IBOutlet weak var imgLogo: UIImageView!
   
     @IBOutlet weak var customView: UIView!
    
    @IBOutlet weak var btnCaptureVideo: UIButton!
    @IBOutlet weak var btnCapturePhoto: UIButton!
    @IBOutlet weak var btnGallery: UIButton!
    @IBOutlet weak var btnPeepText: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.navigationItem.title = "Digital Media"
        
        updateViewAccordingToTheme()
        
    }

    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        self.tabBarController?.tabBar.isHidden = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
        //Test Commit
    }

    // MARK: - Update View According To Selected Theme
    
    func updateViewAccordingToTheme() {
        
        imgBackground.image = Utilities().themedImage(img_bg)
        imgLogo.image = Utilities().themedImage(img_logo)
        
        btnCaptureVideo.setImage(Utilities().themedImage(img_video), for: UIControlState())
        btnCaptureVideo.setImage(Utilities().themedImage(img_videoSelected), for: .highlighted)
        btnCaptureVideo.setImage(Utilities().themedImage(img_videoSelected), for: .selected)
        
        btnCapturePhoto.setImage(Utilities().themedImage(img_camera), for: UIControlState())
        btnCapturePhoto.setImage(Utilities().themedImage(img_cameraSelected), for: .highlighted)
        btnCapturePhoto.setImage(Utilities().themedImage(img_cameraSelected), for: .selected)
        
        btnGallery.setImage(Utilities().themedImage(img_photo), for: UIControlState())
        btnGallery.setImage(Utilities().themedImage(img_photoSelected), for: .highlighted)
        btnGallery.setImage(Utilities().themedImage(img_photoSelected), for: .selected)
        
        btnPeepText.setImage(Utilities().themedImage(img_peepText), for: UIControlState())
        btnPeepText.setImage(Utilities().themedImage(img_peepTextSelected), for: .highlighted)
        btnPeepText.setImage(Utilities().themedImage(img_peepTextSelected), for: .selected)
    }
    
    //MARK:- Move To Photo
    @IBAction func moveToPhoto(_ sender: AnyObject) {
        
        let st = UIStoryboard(name: "Digital", bundle:nil)
        let obj = st.instantiateViewController(withIdentifier: "CapturePhoto") as! CapturePhoto
        obj.isComingFromPeepTextFlow = false
        obj.isComingFromEventsFlow = false
        mediaType = MediaType.photo
        mediaAttachment = MediaAttachment.photo//like event screen
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    //MARK:- Move to Video
    @IBAction func moveToVideo(_ sender: AnyObject) {
        
        let st = UIStoryboard(name: "Digital", bundle:nil)
        let obj = st.instantiateViewController(withIdentifier: "CaptureVideo") as! CaptureVideo
        obj.isComingFromPeepTextFlow = false
        obj.isComingFromEventsFlow = false
        mediaType = MediaType.video
        mediaAttachment = MediaAttachment.video//like event screen
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    //MARK:- Move to Library
    @IBAction func moveToLibrary(_ sender: AnyObject) {
        isGalleryMediaSent = false

        let fusuma = FusumaViewController()
        fusuma.isComingFromDigitalTab = true
        
        //Using presentview as getting tabbar hide issues in fusuma library with push navigation
        let nav = UINavigationController.init(rootViewController: fusuma)
        self.present(nav, animated: true, completion: nil)
    }
    
    //MARK:- Move to Peep Text
    @IBAction func moveToPeepText(_ sender: AnyObject) {
        
        let st = UIStoryboard(name: "Digital", bundle:nil)
        let obj = st.instantiateViewController(withIdentifier: "AddPeepText") as! AddPeepText
        isComingFromChatFlow = false

        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    //MARK:- Resizing image
    
    func imageResize(_ imageObj:UIImage, sizeChange:CGSize)-> UIImage {
        
        let hasAlpha = false
        let scale: CGFloat = 0.0 // Automatically use scale factor of main screen
        
        UIGraphicsBeginImageContextWithOptions(sizeChange, !hasAlpha, scale)
        imageObj.draw(in: CGRect(origin: CGPoint.zero, size: sizeChange))
        
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext() // !!!
        return scaledImage!
    }
}

