//
//  LikersViewController.swift
//  C00Peeps
//
//  Created by Anubha on 24/11/16.
//  Copyright © 2016 SOTSYS011. All rights reserved.
//

import UIKit
import SwiftyJSON
//MARK: Message Type
public enum LikerMediaType:Int {
    
    case gallery = 1, peepText
    
}

public enum ScreenType:Int {
    
    case likersList = 1, followersList, followingsList, dislikersList, topFiveUsers
}

class LikersViewController: BaseVC, UITableViewDelegate, UITableViewDataSource  {
    
    @IBOutlet weak var tableSearch:UITableView!
    @IBOutlet weak var lblNoRecordsFound: UILabel!
    @IBOutlet weak var btnRefresh: UIButton!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    var refreshControl: UIRefreshControl!
    var imageCache = [String:UIImage]()
    var arrayPeople = [People]()
    var offsetPeople = 0
    var currentPagePeople = 1
    var totalRecordsPeople = 0
    var mediaId = 0
    var userId = 0 // require to get follower or following users list
    var likerMediaType = LikerMediaType.gallery //My Gallery
    var screenType = ScreenType.likersList //Likers List

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Liker People"
        addRefreshControl()
        getLikerPeoples()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Pull to refresh for people table view
    func addRefreshControl(){
        
        refreshControl = UIRefreshControl()
        refreshControl.tintColor = UIColor.darkGray
        refreshControl.attributedTitle = NSAttributedString(string: loaderTitle_PullToRefresh)
        refreshControl.addTarget(self, action: #selector(SearchViewController.handleRefresh(_:)), for: UIControlEvents.valueChanged)
        self.tableSearch.addSubview(refreshControl)
        
    }
    
    func handleRefresh(_ refreshControl: UIRefreshControl) {
        
        offsetPeople = 0
        currentPagePeople = 1
        imageCache.removeAll()
        getLikerPeoples()
    }
    
    //MARK: - Get People Data
    func getLikerPeoples(){
        if arrayPeople.count == 0 {
            loadingIndicator.startAnimating()
            self.hideErrorMessage()
        }
        
        //Fetch follower users list
        if screenType == ScreenType.followersList
        {
            self.navigationItem.title = "Followers"
            
            var parameters = [String: AnyObject]()
            parameters["user_id"] = userId as AnyObject
            parameters["limit"] = Limit as AnyObject
            parameters["offset"] = offsetPeople as AnyObject
           
            //cancel all requests
            APIManager.sharedInstance.cancelAllRequests()
            print(parameters)
            APIManager.sharedInstance.requestGetFollowerPeople(parameters, Target: self)
        }
        //Fetch following users list
        else if screenType == ScreenType.followingsList
        {
            self.navigationItem.title = "Following"
            
            var parameters = [String: AnyObject]()
            parameters["user_id"] = userId as AnyObject
            parameters["limit"] = Limit as AnyObject
            parameters["offset"] = offsetPeople as AnyObject
            
            //cancel all requests
            APIManager.sharedInstance.cancelAllRequests()
            print(parameters)
            APIManager.sharedInstance.requestGetFollowingPeople(parameters, Target: self)
        }
        else if screenType == ScreenType.dislikersList
        {
            self.navigationItem.title = "Dislikes"
            
            var parameters = [String: AnyObject]()
            parameters["user_id"] = Utilities.getUserDetails().id as AnyObject
            parameters["limit"] = Limit as AnyObject
            parameters["offset"] = offsetPeople as AnyObject
            parameters["type"] = likerMediaType.rawValue as AnyObject
            parameters["media_id"] = mediaId as AnyObject
            
            //cancel all requests
            APIManager.sharedInstance.cancelAllRequests()
            APIManager.sharedInstance.requestGetDisLikerPeople(parameters, Target: self)
        }
        else if screenType == ScreenType.likersList
        {
            self.navigationItem.title = "Likes"
            
            var parameters = [String: AnyObject]()
            parameters["user_id"] = Utilities.getUserDetails().id as AnyObject
            parameters["limit"] = Limit as AnyObject
            parameters["offset"] = offsetPeople as AnyObject
            parameters["type"] = likerMediaType.rawValue as AnyObject
            parameters["media_id"] = mediaId as AnyObject
            //cancel all requests
            APIManager.sharedInstance.cancelAllRequests()
            APIManager.sharedInstance.requestGetLikerPeople(parameters, Target: self)
        }
        else if screenType == ScreenType.topFiveUsers
        {
            self.navigationItem.title = "People"
            
            var parameters = [String: AnyObject]()
            parameters["user_id"] = Utilities.getUserDetails().id as AnyObject
            
            print(parameters)
            APIManager.sharedInstance.requestGetTopFiveUsers(parameters, Target: self)
        }
        
    }
    
    //MARK: - Get Likers Response
    func responseGetLikerPeoples (_ notify: Foundation.Notification)
    {
        if loadingIndicator.isAnimating {
            loadingIndicator.stopAnimating()
        }
        
        refreshControl.endRefreshing()
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFIACTION_GETLIKERPEOPLE)
            
            
            
            , object: nil)
        
        let swiftyJsonVar = JSON(notify.object!)
        
        let message = swiftyJsonVar["message"].stringValue
        let status = swiftyJsonVar["status"].intValue
        
        if swiftyJsonVar [ERROR_KEY].exists()
        {
            self.showErrorMessage(swiftyJsonVar[ERROR_KEY].stringValue)
        }
        else if (status == 1) //success response
        {
            if offsetPeople == 0
            {
                arrayPeople.removeAll()
            }
            
            totalRecordsPeople = swiftyJsonVar["response"]["count"].intValue
            
            if totalRecordsPeople > 0
            {
                var arrFeeds = [People]()
                arrFeeds = Parser.getParsedPeopleArrayFromData(swiftyJsonVar["response"]["list"])
                
                arrayPeople.append(contentsOf: arrFeeds)
                
                if arrayPeople.count>0
                {
                    self.hideErrorMessage()
                    tableSearch.reloadData()
                }
            }
            else
            {
                tableSearch.reloadData()
                self.showErrorMessage(alertMsg_NoUsers)
            }
        }
        else if (status == 0) //error response
        {
            let responseErrorMessage = swiftyJsonVar["response"].stringValue
            self.showErrorMessage(responseErrorMessage)
        }
        else
        {
            self.showErrorMessage(message)
        }
    }

    //MARK: - Get Likers Response
    func responseGetDisLikerPeoples (_ notify: Foundation.Notification)
    {
        if loadingIndicator.isAnimating {
            loadingIndicator.stopAnimating()
        }
        
        refreshControl.endRefreshing()
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFIACTION_GETDISLIKERPEOPLE), object: nil)
        
        let swiftyJsonVar = JSON(notify.object!)
        
        let message = swiftyJsonVar["message"].stringValue
        let status = swiftyJsonVar["status"].intValue
        
        if swiftyJsonVar [ERROR_KEY].exists()
        {
            self.showErrorMessage(swiftyJsonVar[ERROR_KEY].stringValue)
        }
        else if (status == 1) //success response
        {
            if offsetPeople == 0
            {
                arrayPeople.removeAll()
            }
            
            totalRecordsPeople = swiftyJsonVar["response"]["count"].intValue
            
            if totalRecordsPeople > 0
            {
                var arrFeeds = [People]()
                arrFeeds = Parser.getParsedPeopleArrayFromData(swiftyJsonVar["response"]["list"])
                
                arrayPeople.append(contentsOf: arrFeeds)
                
                if arrayPeople.count>0
                {
                    self.hideErrorMessage()
                    tableSearch.reloadData()
                }
            }
            else
            {
                tableSearch.reloadData()
                self.showErrorMessage(alertMsg_NoUsers)
            }
        }
        else if (status == 0) //error response
        {
            let responseErrorMessage = swiftyJsonVar["response"].stringValue
            self.showErrorMessage(responseErrorMessage)
        }
        else
        {
            self.showErrorMessage(message)
        }
    }
    
    //MARK: - Get Following People Response
    func responseGetFollowingPeoples (_ notify: Foundation.Notification)
    {
        if loadingIndicator.isAnimating {
            loadingIndicator.stopAnimating()
        }
        
        refreshControl.endRefreshing()
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFIACTION_GETFOLLOWINGPEOPLE), object: nil)
        
        let swiftyJsonVar = JSON(notify.object!)
        
        let message = swiftyJsonVar["message"].stringValue
        let status = swiftyJsonVar["status"].intValue
        
        if swiftyJsonVar [ERROR_KEY].exists()
        {
            self.showErrorMessage(swiftyJsonVar[ERROR_KEY].stringValue)
        }
        else if (status == 1) //success response
        {
            if offsetPeople == 0
            {
                arrayPeople.removeAll()
            }
            
            totalRecordsPeople = swiftyJsonVar["response"]["count"].intValue
            
            if totalRecordsPeople > 0
            {
                var arrFeeds = [People]()
                arrFeeds = Parser.getParsedPeopleArrayFromData(swiftyJsonVar["response"]["list"])
                
                arrayPeople.append(contentsOf: arrFeeds)
                
                if arrayPeople.count>0
                {
                    self.hideErrorMessage()
                    tableSearch.reloadData()
                }
            }
            else
            {
                tableSearch.reloadData()
                self.showErrorMessage(alertMsg_NoUsers)
            }
        }
        else if (status == 0) //error response
        {
            let responseErrorMessage = swiftyJsonVar["response"].stringValue
            self.showErrorMessage(responseErrorMessage)
        }
        else
        {
            self.showErrorMessage(message)
        }
    }

    
    
    //MARK: - Get Followers People Response
    func responseGetFollowerPeoples (_ notify: Foundation.Notification)
    {
        if loadingIndicator.isAnimating {
            loadingIndicator.stopAnimating()
        }
        
        refreshControl.endRefreshing()
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFIACTION_GETFOLLOWERPEOPLE), object: nil)
        
        let swiftyJsonVar = JSON(notify.object!)
        
        let message = swiftyJsonVar["message"].stringValue
        let status = swiftyJsonVar["status"].intValue
        
        if swiftyJsonVar [ERROR_KEY].exists()
        {
            self.showErrorMessage(swiftyJsonVar[ERROR_KEY].stringValue)
        }
        else if (status == 1) //success response
        {
            if offsetPeople == 0
            {
                arrayPeople.removeAll()
            }
            
            totalRecordsPeople = swiftyJsonVar["response"]["count"].intValue
            
            if totalRecordsPeople > 0
            {
                var arrFeeds = [People]()
                arrFeeds = Parser.getParsedPeopleArrayFromData(swiftyJsonVar["response"]["list"])
                
                arrayPeople.append(contentsOf: arrFeeds)
                
                if arrayPeople.count>0
                {
                    self.hideErrorMessage()
                    tableSearch.reloadData()
                }
            }
            else
            {
                tableSearch.reloadData()
                self.showErrorMessage(alertMsg_NoUsers)
            }
        }
        else if (status == 0) //error response
        {
            let responseErrorMessage = swiftyJsonVar["response"].stringValue
            self.showErrorMessage(responseErrorMessage)
        }
        else
        {
            self.showErrorMessage(message)
        }
    }
    
    //MARK: - Get Top Five Users Response
    func responseGetTopFiveUsers(_ notify: Foundation.Notification)
    {
        if loadingIndicator.isAnimating {
            loadingIndicator.stopAnimating()
        }
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFIACTION_GETTOPFIVEUSERS), object: nil)
        
        let swiftyJsonVar = JSON(notify.object!)
        
        let message = swiftyJsonVar["message"].stringValue
        let status = swiftyJsonVar["status"].intValue
        
        if swiftyJsonVar [ERROR_KEY].exists()
        {
            self.showErrorMessage(swiftyJsonVar[ERROR_KEY].stringValue)
        }
        else if (status == 1) //success response
        {
            var arrFeeds = [People]()
            arrFeeds = Parser.getParsedPeopleArrayFromData(swiftyJsonVar["response"])
            
            arrayPeople.append(contentsOf: arrFeeds)
            
            if arrayPeople.count>0
            {
                self.hideErrorMessage()
                tableSearch.reloadData()
            }
        }
        else if (status == 0) //error response
        {
            let responseErrorMessage = swiftyJsonVar["response"].stringValue
            self.showErrorMessage(responseErrorMessage)
        }
        else
        {
            self.showErrorMessage(message)
        }
    }

    
    // MARK:- UITableViewDataSource delegate
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        
        return 80.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
            return arrayPeople.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //Load More
        if (arrayPeople.count >= Limit) && (indexPath.row + 1  == arrayPeople.count) {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "LoadMoreCell")
            
            if let lbl5176 =  cell?.viewWithTag(5176) as? UILabel
            {
                lbl5176.text = loading_Title
                
                if (totalRecordsPeople) > (currentPagePeople) * Limit {
                    
                    offsetPeople = (currentPagePeople) * Limit
                    currentPagePeople += 1
                    getLikerPeoples()
                    
                }
                else
                {
                    lbl5176.text = loadingNoMoreData
                    
                    lbl5176.isHidden = true
                    
                    return refreshCellForRow(indexPath)
                }
            }
            
            return cell!
        }
            
        else{
            
            return refreshCellForRow(indexPath)
        }
    }
    
    func refreshCellForRow(_ indexPath:IndexPath)->SearchTableCell
    {
        let cell = self.tableSearch.dequeueReusableCell( withIdentifier: "SearchCell", for: indexPath) as! SearchTableCell
        
        cell.backgroundColor = UIColor.clear
        cell.selectionStyle = .none
        cell.tag = indexPath.row
        
        let people: People!
        if arrayPeople.count <= indexPath.row
        {
            return cell
        }
        people = arrayPeople[(indexPath as IndexPath).row]
        cell.imageView_user!.image = Utilities().themedImage(img_defaultUser)
        cell.imageView_user!.contentMode = .scaleAspectFit
        if let img = imageCache[people.pUserImage] {
            cell.imageView_user!.image = img
        }
        else if imageCache[people.pUserImage] == nil && people.pUserImage != ""
        {
            //Downloading user's image
            URLSession.shared.dataTask(with: URL(string: people.pUserImage)!, completionHandler: { (data, response, error) -> Void in
                
                if error != nil {
                    
                    cell.imageView_user!.image = Utilities().themedImage(img_defaultUser)
                    
                    print(error)
                    return
                }
                
                DispatchQueue.main.async(execute: { () -> Void in
                    let image = UIImage(data: data!)
                    if (image == nil)
                    {
                        cell.imageView_user!.image = Utilities().themedImage(img_defaultUser)
                    }
                    else
                    {
                        cell.imageView_user!.image = image
                        self.imageCache[people.pUserImage] = cell.imageView_user!.image
                    }
                })
            }).resume()
        }
        
        cell.labelName.font = ProximaNovaRegular(15)
        cell.labelName!.text = people.pUserName
        
        cell.labelPost.font = ProximaNovaRegular(14)
        if people.pPost > 1{
            cell.labelPost!.text = NSString.init(format:"%d Posts", people.pPost) as String
        }
        else{
            cell.labelPost!.text = NSString.init(format:"%d Post", people.pPost) as String
        }
        
        cell.buttonFollow.tag = indexPath.row
        cell.buttonFollow.addTarget(self, action: #selector(SearchViewController.addbuttonTapped(_:)), for: .touchUpInside)
        
        if people.pFriendStatus == "Unknown"
        {
            cell.buttonFollow.setImage(Utilities().themedImage(img_followSelected), for: UIControlState())
            cell.buttonFollow.setImage(Utilities().themedImage(img_follow), for: .highlighted)
        }
        else if people.pFriendStatus == "Approved"
        {
            cell.buttonFollow.setImage(Utilities().themedImage(img_following), for: UIControlState())
            cell.buttonFollow.setImage(Utilities().themedImage(img_followingSelected), for: .highlighted)
        }
        else if people.pFriendStatus == "Pending"
        {
            cell.buttonFollow.setImage(Utilities().themedImage(img_pending), for: UIControlState())
            cell.buttonFollow.setImage(Utilities().themedImage(img_pendingSelected), for: .highlighted)
        }
        
        if people.pId == Int(Utilities.getUserDetails().id) {
            cell.buttonFollow.isHidden = true
        }
        else{
            cell.buttonFollow.isHidden = false

        }
        
        //TODO:- Need to remove this condition later when follower/following API complete
        if screenType == ScreenType.followersList || screenType == ScreenType.followingsList{
            
            cell.buttonFollow.isHidden = true
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    // MARK:- Follow Button Click
    
    func addbuttonTapped(_ sender: AnyObject)
    {
        let button = sender as! UIButton
        let people: People!
        people = arrayPeople[button.tag]
        
        //If user is not already followed
        if people.pFriendStatus == "Unknown"
        {
            
            if screenType == ScreenType.topFiveUsers
            {
                people.pFriendStatus = "Approved"
            }
            else
            {
                people.pFriendStatus = "Pending"
            }
            
            //Refreshing tble cell
            let buttonPosition = sender.convert(CGPoint.zero, to: tableSearch)
            let indexPath = tableSearch.indexPathForRow(at: buttonPosition)! as IndexPath
            tableSearch.reloadRows(at: [indexPath], with: UITableViewRowAnimation.fade)
            
            //Calling API
            followPeople(people.pId)
        }
        else if people.pFriendStatus == "Approved"
        {
            
            //Create the AlertController
            let actionSheetController: UIAlertController = UIAlertController(title: "Unfollow " + people.pUserName + "?", message: "", preferredStyle: .actionSheet)
            
            //Create and add the Cancel action
            let cancelAction: UIAlertAction = UIAlertAction(title: alertBtnTitle_Cancel, style: .cancel) { action -> Void in
                //Just dismiss the action sheet
            }
            actionSheetController.addAction(cancelAction)
            
            //Create and add first option action
            let unfollowAction: UIAlertAction = UIAlertAction(title: "Unfollow", style: .destructive) { action -> Void in
                
                people.pFriendStatus = "Unknown"
                
                //Refreshing tble cell
                let buttonPosition = sender.convert(CGPoint.zero, to: self.tableSearch)
                let indexPath = self.tableSearch.indexPathForRow(at: buttonPosition)! as IndexPath
                self.tableSearch.reloadRows(at: [indexPath], with: UITableViewRowAnimation.fade)
                
                //Calling API
                self.unfollowPeople(people.pId)
            }
            actionSheetController.addAction(unfollowAction)
            
            
            //Present the AlertController
            self.present(actionSheetController, animated: true, completion: nil)
        }
        else if people.pFriendStatus == "Pending"
        {
            //Create the AlertController
            let actionSheetController: UIAlertController = UIAlertController(title: "Follow Request", message: "", preferredStyle: .actionSheet)
            
            //Create and add the Cancel action
            let cancelAction: UIAlertAction = UIAlertAction(title: alertBtnTitle_Cancel, style: .cancel) { action -> Void in
                //Just dismiss the action sheet
            }
            actionSheetController.addAction(cancelAction)
            
            //Create and add first option action
            let unfollowAction: UIAlertAction = UIAlertAction(title: "Delete Follow Request", style: .destructive) { action -> Void in
                
                people.pFriendStatus = "Unknown"
                
                //Refreshing tble cell
                let buttonPosition = sender.convert(CGPoint.zero, to: self.tableSearch)
                let indexPath = self.tableSearch.indexPathForRow(at: buttonPosition)! as IndexPath
                self.tableSearch.reloadRows(at: [indexPath], with: UITableViewRowAnimation.fade)
                
                //Calling API
                
                self.unfollowPeople(people.pId)
            }
            actionSheetController.addAction(unfollowAction)
            
            
            //Present the AlertController
            self.present(actionSheetController, animated: true, completion: nil)
        }
    }
    
    // MARK: - Show/Hide Error Message
    func showErrorMessage(_ message:String) {
        self.btnRefresh.isHidden = false
        self.lblNoRecordsFound.isHidden = false
        self.lblNoRecordsFound.text = message
        tableSearch.isHidden = true
        
    }
    
    func hideErrorMessage() {
        self.btnRefresh.isHidden = true
        self.lblNoRecordsFound.isHidden = true
        
            tableSearch.isHidden = false    
    }
    
    //MARK: - Follow People
    func followPeople(_ userId:Int){
        
        //{1=Approved, 2=Pending, 3=Blocked, 4=Unknown}
        if reachability?.currentReachabilityStatus == .notReachable {
            self.showCommonAlert(alertMsg_NoInternetConnection)
        }
        else
        {
            var parameters = [String: AnyObject]()
            parameters["requester_id"] = Utilities.getUserDetails().id as AnyObject
            parameters["responder_id"] = userId as AnyObject
            
            if screenType == ScreenType.topFiveUsers
            {
                parameters["status_id"] = 1 as AnyObject //Direct follow
            }
            else
            {
                parameters["status_id"] = 2 as AnyObject //Pending Status
            }
            
            print(parameters)
            
            APIManager.sharedInstance.requestFollow(parameters, Target: self)

        }
    }
    
    //MARK: - Follow People Response
    func responseFollowPeople (_ notify: Foundation.Notification)
    {
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFIACTION_FOLLOWPEOPLE), object: nil)
        
        let swiftyJsonVar = JSON(notify.object!)
        
        let message = swiftyJsonVar["message"].stringValue
        let status = swiftyJsonVar["status"].intValue
        
        if swiftyJsonVar [ERROR_KEY].exists()
        {
            self.showCommonAlert(swiftyJsonVar[ERROR_KEY].stringValue)
        }
        else if (status == 1) //success response
        {
            
        }
        else if (status == 0) //error response
        {
            let responseErrorMessage = swiftyJsonVar["response"].stringValue
            self.showCommonAlert(responseErrorMessage)
        }
        else
        {
            self.showCommonAlert(message)
        }
    }
    
    //MARK: - Unfollow People
    func unfollowPeople(_ userId:Int){
        if reachability?.currentReachabilityStatus == .notReachable {
            self.showCommonAlert(alertMsg_NoInternetConnection)
        }
        else {
        var parameters = [String: AnyObject]()
        parameters["requester_id"] = Utilities.getUserDetails().id as AnyObject
        parameters["responder_id"] = userId as AnyObject
        
        APIManager.sharedInstance.requestUnFollow(parameters, Target: self)
        }
    }
    
    //MARK: - Unfollow People Response
    func responseUnFollowPeople (_ notify: Foundation.Notification)
    {
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFIACTION_UNFOLLOWPEOPLE), object: nil)
        
        let swiftyJsonVar = JSON(notify.object!)
        
        let message = swiftyJsonVar["message"].stringValue
        let status = swiftyJsonVar["status"].intValue
        
        if swiftyJsonVar [ERROR_KEY].exists()
        {
            self.showCommonAlert(swiftyJsonVar[ERROR_KEY].stringValue)
        }
        else if (status == 1) //success response
        {
            
        }
        else if (status == 0) //error response
        {
            let responseErrorMessage = swiftyJsonVar["response"].stringValue
            self.showCommonAlert(responseErrorMessage)
        }
        else
        {
            self.showCommonAlert(message)
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
