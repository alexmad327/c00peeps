//
//  PeepTextDetailCollectionViewController.swift
//  C00Peeps
//
//  Created by Anubha on 17/11/16.
//  Copyright © 2016 SOTSYS011. All rights reserved.
//

import UIKit
import SwiftyJSON

private let reuseIdentifier = "Cell"

class PeepTextDetailCollectionViewController: UICollectionViewController,UpdateContent {

    
    //Peep Text
    var offsetPeepText = 0
    var currentPagePeepText = 1
    var totalRecordsPeepText = 0
    var arrayPeepText = [Media]()
    var catIdPeepText = -1
    var otherUserId = 0
    var otherUserName = ""

    var galleryType = GalleryType.peepDetailGallery //My Gallery
    var height:CGFloat = 0.0
    var width:CGFloat = 0.0
    
    var universalCollectionViewPeepText:UniversalCollectionView!

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.title = "Peep Conversation"
        requestPeepTextConverstaion()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.automaticallyAdjustsScrollViewInsets = false
        self.collectionView?.backgroundColor = UIColor.clear
        self.view.backgroundColor = UIColor.white
        
        let collectinLayout = UICollectionViewFlowLayout()
        self.universalCollectionViewPeepText = UniversalCollectionView.init(frame: self.view.frame, collectionViewLayout: collectinLayout, catId: catIdPeepText, del: self, array: arrayPeepText, header: nil, displayType:true, galleryType:galleryType)
        self.universalCollectionViewPeepText.displayPeepText = true
        self.universalCollectionViewPeepText.displayRetryOption = true
        self.universalCollectionViewPeepText.classType = .peepDetail
        self.collectionView = universalCollectionViewPeepText
        
         height = CGFloat(UIScreen.main.bounds.height - (self.navigationController?.navigationBar.frame.size.height)! - (self.tabBarController?.tabBar.frame.size.height)! - UIApplication.shared.statusBarFrame.size.height)
         width = CGFloat(UIScreen.main.bounds.width)
        
        var imageViewObject :UIImageView
        imageViewObject = UIImageView(frame:CGRect(x: 0, y: (height-50)+1, width: width, height: 1));
        imageViewObject.backgroundColor = ColorWithRGB(127, green: 15, blue: 126, alpha: 1.0)
        self.view.addSubview(imageViewObject)
        
        let button = UIButton(frame: CGRect(x: 14, y: (height-50) + 7, width: width-28, height: 37))
        button.setTitle("Send Peep Text", for: UIControlState())
        button.setBackgroundImage(Utilities().themedImage(img_btnSelected), for: UIControlState())//UIImage.init(named: "btnSelected_t1"
        button.addTarget(self, action: #selector(replyClicked), for: .touchUpInside)
        button.setTitleColor(UIColor.white, for: UIControlState())
        self.view.addSubview(button)
        
    }
    
        
        // Do any additional setup after loading the view.

    override func viewDidLayoutSubviews() {
        
        self.collectionView?.frame = CGRect(x: 0, y: 0, width: width, height: height-50);

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func requestPeepTextConverstaion(){
        
        var parameters = [String: AnyObject]()
        parameters["other_user_id"] = otherUserId as AnyObject
        parameters["limit"] = SearchLimit as AnyObject
        parameters["user_id"] = Utilities.getUserDetails().id as AnyObject
        parameters["offset"] = offsetPeepText as AnyObject
        APIManager.sharedInstance.requestPeepTextConversation(parameters, Target: self)
    }
    func replyClicked () {
        self.navigationItem.title = ""
        let st = UIStoryboard(name: "Digital", bundle:nil)
        let obj = st.instantiateViewController(withIdentifier: "AddPeepText") as! AddPeepText
       
        // Save contact to reply.
        let contact:Contact = Contact()
        contact.pId = otherUserId
        contact.pUserName = otherUserName
        sendPeepTextToContact = contact
        
        Utilities().setPeepTextSentUser(otherUserId, sentUserName: otherUserName)
        self.navigationController?.pushViewController(obj, animated: true)
    }
    //MARK: - Get PeepText Response
    func responsePeepTextConversation (_ notify: Foundation.Notification)
    {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFIACTION_GETGALLERYPEEPTEXT), object: nil)
        
        let swiftyJsonVar = JSON(notify.object!)
        
        let message = swiftyJsonVar["message"].stringValue
        let status = swiftyJsonVar["status"].intValue
        if swiftyJsonVar [ERROR_KEY].exists()
        {
            universalCollectionViewPeepText.arrayCollection = []
            universalCollectionViewPeepText.showLoadingIndicator = false
            universalCollectionViewPeepText.displayRetryOption = true
            universalCollectionViewPeepText.noRecordsFoundTitle = swiftyJsonVar[ERROR_KEY].stringValue
            universalCollectionViewPeepText.reloadData()
        }
        else if (status == 1) //success response
        {
            totalRecordsPeepText = swiftyJsonVar["response"]["count"].intValue
            if totalRecordsPeepText > 0
            {
                var arrFeeds = [Media]()
                arrFeeds = Parser.getParsedMediaArrayFromData(swiftyJsonVar["response"]["message_list"])
                if offsetPeepText == 0
                {
                    arrayPeepText.removeAll()
                }
                
                arrayPeepText.append(contentsOf: arrFeeds)
                
                if arrayPeepText.count>0
                {
                    universalCollectionViewPeepText.totalRecordCount = totalRecordsPeepText
                    universalCollectionViewPeepText.searchCategoryId = catIdPeepText
                    universalCollectionViewPeepText.currentPage = currentPagePeepText
                    universalCollectionViewPeepText.offset = offsetPeepText
                    universalCollectionViewPeepText.arrayCollection = self.arrayPeepText
                    universalCollectionViewPeepText.showLoadingIndicator = false
                    universalCollectionViewPeepText.displayRetryOption = true
                    universalCollectionViewPeepText.galleryType = galleryType
                    universalCollectionViewPeepText.reloadData()
                    
                }
            }
            else
            {
                universalCollectionViewPeepText.arrayCollection = self.arrayPeepText
                universalCollectionViewPeepText.showLoadingIndicator = false
                universalCollectionViewPeepText.displayRetryOption = true
                universalCollectionViewPeepText.noRecordsFoundTitle = alertMsg_NoData
                universalCollectionViewPeepText.reloadData()
            }
        }
        else if (status == 0) //error response
        {
            universalCollectionViewPeepText.arrayCollection = []
            universalCollectionViewPeepText.showLoadingIndicator = false
            universalCollectionViewPeepText.displayRetryOption = true
            universalCollectionViewPeepText.noRecordsFoundTitle = swiftyJsonVar["response"].stringValue
            universalCollectionViewPeepText.reloadData()
        }
        else
        {
            universalCollectionViewPeepText.arrayCollection = []
            universalCollectionViewPeepText.showLoadingIndicator = false
            universalCollectionViewPeepText.displayRetryOption = true
            universalCollectionViewPeepText.noRecordsFoundTitle = message
            universalCollectionViewPeepText.reloadData()
        }
    }
    
    //Reload Media (or pull to refresh)
    func reloadMediaGallery(_ categoryId:Int) {
        self.currentPagePeepText = 1
        self.offsetPeepText = 0
        self.requestPeepTextConverstaion()
  
    }

    func loadMoreMediaGallery(_ page:NSInteger,offset:Int, categoryId:Int){
        
        //Category Id (1 Music 2 Movie 3 Photo 4 Video Game
            self.currentPagePeepText = page
            self.offsetPeepText = offset
            self.requestPeepTextConverstaion()
    }
    
    //MARK:- Move to Single detail or comments screen
    func moveToNextScreenGallery(_ ScreenName:String, WithData:AnyObject)
    {
    
        if(ScreenName == "LikersViewController")
        {
            self.navigationItem.title = ""
            let st = UIStoryboard(name: "Gallery", bundle:nil)
            let likerObject = st.instantiateViewController(withIdentifier: "LikersViewController") as! LikersViewController
            let peopleObj = WithData as! Media
            likerObject.mediaId = peopleObj.pId
            likerObject.likerMediaType = .peepText
            likerObject.screenType = .likersList
            self.navigationController?.pushViewController(likerObject, animated: true)
            
        }
        else  if(ScreenName == "DisLikersViewController")
        {
            self.navigationItem.title = ""
            let st = UIStoryboard(name: "Gallery", bundle:nil)
            let likerObject = st.instantiateViewController(withIdentifier: "LikersViewController") as! LikersViewController
            let peopleObj = WithData as! Media
            likerObject.mediaId = peopleObj.pId
            likerObject.likerMediaType = .peepText
            likerObject.screenType = .dislikersList
            self.navigationController?.pushViewController(likerObject, animated: true)
            
        }
        else if(ScreenName == "GalleryMainViewController")
        {
             self.navigationItem.title = ""
            let st = UIStoryboard(name: "Gallery", bundle:nil)
            let galleryObject = st.instantiateViewController(withIdentifier: "GalleryMainViewController") as? GalleryMainViewController
            let md = WithData as! Media
            galleryObject?.otherUserId = md.pUserId
            self.navigationController?.pushViewController(galleryObject!, animated: true)
        }
        else if (ScreenName == "ForwardMedia")
        {
            let st = UIStoryboard(name: "Digital", bundle:nil)
            let obj = st.instantiateViewController(withIdentifier: "ContactsScreen") as! ContactsScreen
            let md = WithData as! Media
            obj.fwdMediaId = md.pId
            obj.isComingFromForwardMedia = true
            self.navigationController?.pushViewController(obj, animated: true)
        }
        
    }
}
