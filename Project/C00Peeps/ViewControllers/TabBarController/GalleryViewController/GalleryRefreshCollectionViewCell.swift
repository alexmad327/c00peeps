//
//  GalleryRefreshCollectionViewCell.swift
//  C00Peeps
//
//  Created by Arshdeep on 03/11/16.
//  Copyright © 2016 SOTSYS011. All rights reserved.
//

import UIKit

class GalleryRefreshCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var lblNoRecordsFound: UILabel!
    @IBOutlet weak var btnRefresh: UIButton!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
