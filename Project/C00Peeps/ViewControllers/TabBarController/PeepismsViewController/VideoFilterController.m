//
//  VideoFilterController.m
//  Speazie
//
//  Created by OSX on 28/07/16.
//
//

#import "VideoFilterController.h"
#import "CollectionViewCell.h"
#import "VideoPublishController.h"
#import "C00Peeps-Swift.h"

#define DegreesToRadians(x) ((x) * M_PI / 180.0)

@interface VideoFilterController (){
    
    NSMutableArray *arrFilters; //Array for video presets
    NSMutableArray *arrImages;
    NSString *selFilterName;
    
    bool bWatermarkAdded;
    bool bWritingFile;
    bool bEditMode;
    
    UIButton *btnNext;
    LBVideoOrientation orientation;
    GPUImagePicture *gpuImage;
    
    UIImage *imgEditBtm;
    UIImage *imgEditBtmSelected;
    UIImage *imgSaturation;
    UIImage *imgSaturationSelected;
    UIImage *imgBrighness;
    UIImage *imgBrighnessSelected;
    UIImage *imgContrast;
    UIImage *imgContrastSelected;
    UIImage *imgFilterBtm;
    UIImage *imgFilterBtmSelected;
    UIImage *imgBackArrow;
    UIImage *imgCancelBtm;
    UIImage *imgSaveBtm;

}

@end

@implementation VideoFilterController

@synthesize collPresets, filter;

- (void)viewDidLoad {
    
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    brightnessVal = 0.0;
    contrastVal = 1.0;
    saturationVal = 1.0;
    
    self.title = @"Filter";
    
    bWritingFile = false;
    
    arrImages = [[NSMutableArray alloc] init];
    
    arrFilters = [[NSMutableArray alloc] init];
    
    [arrFilters addObject:@"GPUIMAGE_ORIGINAL"];
    [arrFilters addObject:@"GPUIMAGE_SEPIA"];
    [arrFilters addObject:@"GPUIMAGE_GRAYSCALE"];
    [arrFilters addObject:@"GPUIMAGE_COLORINVERT"];
    [arrFilters addObject:@"GPUIMAGE_VIGNETTE"];
    
    //new ones
    [arrFilters addObject:@"GPUIMAGE_GAMMA"];
    [arrFilters addObject:@"GPUIMAGE_HUE"];
    [arrFilters addObject:@"GPUIMAGE_WHITEBALANCE"];
    [arrFilters addObject:@"GPUIMAGE_LUMINANCE_RANGE"];
    [arrFilters addObject:@"GPUIMAGE_HALFTONE"];
    
    //TODO:- Enable this flag when need to add filter on video
    //bWatermarkAdded = true;
    
    Utilities *utilities = [[Utilities alloc] init];
    imgEditBtm = [utilities themedImage:[ImageConstants img_editBtmObjc]];
    imgEditBtmSelected = [utilities themedImage:[ImageConstants img_editBtmSelectedObjc]];
    imgSaturation = [utilities themedImage:[ImageConstants img_saturationObjc]];
    imgSaturationSelected = [utilities themedImage:[ImageConstants img_saturationSelectedObjc]];
    imgBrighness = [utilities themedImage:[ImageConstants img_brighnessObjc]];
    imgBrighnessSelected = [utilities themedImage:[ImageConstants img_brighnessSelectedObjc]];
    imgContrast = [utilities themedImage:[ImageConstants img_contrastObjc]];
    imgContrastSelected = [utilities themedImage:[ImageConstants img_contrastSelectedObjc]];
    imgFilterBtm = [utilities themedImage:[ImageConstants img_filterBtmObjc]];
    imgFilterBtmSelected = [utilities themedImage:[ImageConstants img_filterBtmSelectedObjc]];
    imgBackArrow = [utilities themedImage:[ImageConstants img_backArrowObjc]];
    imgCancelBtm = [utilities themedImage:[ImageConstants img_cancelBtmObjc]];
    imgSaveBtm = [utilities themedImage:[ImageConstants img_saveBtmObjc]];

    //_vwWaitVideoProcessing.backgroundColor = [[utilities themedMultipleColor] firstObject];
    
    [self setConstraint];
    
    [self updateViewAccordingToTheme];
    
    gpuImage = [[GPUImagePicture alloc] initWithImage:self.updatedPhoto];
    
    [self processFilterImage];
    
}

- (void)loadViews
{
    CGRect cameraFrame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.width);
    
    UIView* viewCamera = [[UIView alloc] initWithFrame:cameraFrame];
    viewCamera.backgroundColor = [UIColor redColor];
    
    filterView = [[GPUImageView alloc] initWithFrame:cameraFrame];
    filterView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
    [filterView setFillMode:kGPUImageFillModePreserveAspectRatioAndFill];
    
    viewCamera = filterView;
    
    [self.customView addSubview:viewCamera];
    
    //filterView1
    
    filterView1 = [[GPUImageView alloc] initWithFrame:cameraFrame];
    filterView1.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
    [filterView1 setFillMode:kGPUImageFillModePreserveAspectRatioAndFill];
    
    viewCamera = filterView1;
    
    [self.customView addSubview:viewCamera];
    
    //filterView2
    
    filterView2 = [[GPUImageView alloc] initWithFrame:cameraFrame];
    filterView2.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
    [filterView2 setFillMode:kGPUImageFillModePreserveAspectRatioAndFill];
    
    viewCamera = filterView2;
    
    [self.customView addSubview:viewCamera];
    
    //filterView3
    
    filterView3 = [[GPUImageView alloc] initWithFrame:cameraFrame];
    filterView3.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
    [filterView3 setFillMode:kGPUImageFillModePreserveAspectRatioAndFill];
    
    viewCamera = filterView3;
    
    [self.customView addSubview:viewCamera];
    
    //filterView4
    
    filterView4 = [[GPUImageView alloc] initWithFrame:cameraFrame];
    filterView4.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
    [filterView4 setFillMode:kGPUImageFillModePreserveAspectRatioAndFill];
    
    viewCamera = filterView4;
    
    [self.customView addSubview:viewCamera];
    
    filter = nil;
    
    [self applyFilter];
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:TRUE];
    
    brightnessVal = 0.0;
    contrastVal = 1.0;
    saturationVal = 1.0;
    
    bWritingFile = false;
    
    UIBarButtonItem *btn = [[UIBarButtonItem alloc] initWithImage:imgBackArrow style:UIBarButtonItemStylePlain target:self action:@selector(backButtonAction)];
    
    self.navigationItem.leftBarButtonItem = btn;
    
    NSString *title = @"Next";
    if (self.isComingFromEventsFlow == true) {
        title = @"Done";
    }
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:title style:UIBarButtonItemStylePlain target:self action:@selector(next)];
    
    self.tabBarController.tabBar.hidden = true;
    
    self.hidesBottomBarWhenPushed = false;
    
    [self setExtendedLayoutIncludesOpaqueBars:YES];
    
    bEditVideo = false;
    btnCrop.hidden = false;
    btnRotate.hidden = false;
    btnBrightness.hidden = false;
    self.collPresets.hidden = false;
    
    [self performSelector:@selector(loadViews) withObject:nil afterDelay:0.2];
}

// MARK: - Update View According To Selected Theme

-(void) updateViewAccordingToTheme {
    [btnCrop setImage:imgContrastSelected forState:UIControlStateNormal];//[UIImage imageNamed:@"contrastSelected_t1"]
    [btnRotate setImage:imgSaturation forState:UIControlStateNormal];//[UIImage imageNamed:@"saturation_t1"]
    [btnBrightness setImage:imgBrighness forState:UIControlStateNormal];//[UIImage imageNamed:@"brighness_t1"]
    [btnFilter setImage:imgFilterBtmSelected forState:UIControlStateNormal];//[UIImage imageNamed:@"filterBtmSelected_t1"]
    [btnEdit setImage:imgEditBtm forState:UIControlStateNormal];//[UIImage imageNamed:@"editBtm_t1"]
    
    Utilities *utilities = [[Utilities alloc] init];
    slider.minimumTrackTintColor = [utilities themedMultipleColor][0];//[UIColor purpleColor];//Utilities().themedMultipleColor().0
    slider.maximumTrackTintColor = [UIColor grayColor];
    
    lblContrast.hidden = true;
    lblBrightness.hidden = true;
    lblSaturation.hidden = true;
}


-(void)stopAndGoBackAfterAppResigns{
    
    //clean all target
    //[self cleanTargets];
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(void) backButtonAction{
    
    //clean all target
    [self cleanTargets];
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)next{
    
    NSLog(@"Start Saving Video .......");
    
    //This is for speazie project
    //[self createThumbnailWithFilters];
    
    //clean all target
    [self cleanTargets];
    
    if(filter)
    {
        //filterView3
        
        CGRect cameraFrame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.width);
        
        UIView* viewCamera = [[UIView alloc] initWithFrame:cameraFrame];
        viewCamera.backgroundColor = [UIColor redColor];
        
        filterView3 = [[GPUImageView alloc] initWithFrame:cameraFrame];
        filterView3.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
        [filterView3 setFillMode:kGPUImageFillModePreserveAspectRatioAndFill];
        
        viewCamera = filterView3;
        
        [self.customView addSubview:viewCamera];
        
        [filter forceProcessingAtSize:filterView3.sizeInPixels];
    }
    //Show loader - Saving Filtered Video
    //AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [self.loaderWaitVideoProcessing startAnimating];
    [self.vwWaitVideoProcessing setHidden:NO];
    self.navigationItem.leftBarButtonItem.enabled = NO;
    self.navigationItem.rightBarButtonItem.enabled = NO;

    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(queue, ^{
        //Do stuff
        //[appDelegate showLoader:[AlertConstants loaderTitle_SavingFilteredVideoObjc]];
       

        [self saveFilterVideo];
        
    });

    /*if (_isComingFromEventsFlow) {
        
//        indicator.hidesWhenStopped = true;
//        indicator.hidden = false;
//        [indicator startAnimating];
        
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        dispatch_async(queue, ^{
            //Do stuff
            
            [self saveFilterVideo];
            
        });
        
        [self moveToEventDetailScreen];
    }
    else {
        
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        dispatch_async(queue, ^{
            //Do stuff
            
            [self saveFilterVideo];
        });
        
        //Moving to Video Publish screen
        
        VideoPublishController *obj = [[VideoPublishController alloc] initWithNibName:@"VideoPublishController" bundle:[NSBundle mainBundle]];
        obj.sampleURL = _sampleURL;
        obj.filterValue = slider.value;
        obj.typeFilter = filterType;
        obj.updatedPhoto = _updatedPhoto;
        obj.strPeepText = _strPeepText;
        obj.bWatermarkAdded = bWatermarkAdded;
        obj.brightnessVal = brightnessVal;
        obj.contrastVal = contrastVal;
        obj.saturationVal = saturationVal;
        obj.isComingFromPeepTextFlow = _isComingFromPeepTextFlow;
        obj.isComingFromEventsFlow = _isComingFromEventsFlow;
        [self.navigationController pushViewController:obj animated:YES];
    }*/
}

- (void)moveToNextScreen {
    uint64_t fileSize = [[[NSFileManager defaultManager] attributesOfItemAtPath:_movieURL.path error:nil] fileSize];

    if ([[NSFileManager defaultManager] fileExistsAtPath:_movieURL.path] && fileSize > 0) {
        if (self.isComingFromEventsFlow) {
            [self moveToEventDetailScreen];
        }
        else {
            //Moving to Video Publish screen
            [self moveToVideoPublishScreen];
        }
    }
    else {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:[AlertConstants alertMsg_prepareVideoErrorObjc] preferredStyle:UIAlertControllerStyleAlert];
        
        [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            if (self.isComingFromEventsFlow) {
                for (UIViewController *view in self.navigationController.viewControllers) {
                    if ([view isKindOfClass:[CreateEventSecondViewController class]]) {
                        [self.navigationController popToViewController:view animated:YES];
                    }
                }
            }
            else {
                for (UIViewController *view in self.navigationController.viewControllers) {
                    if ([view isKindOfClass:[CaptureVideo class]]) {
                        [self.navigationController popToViewController:view animated:YES];
                    }
                }
            }
        }]];
        
       [self presentViewController:alertController animated:YES completion:nil];
    }
}

- (void)moveToEventDetailScreen{
    
    for (UIViewController *view in self.navigationController.viewControllers) {
        if ([view isKindOfClass:[CreateEventSecondViewController class]]) {
            [self.navigationController popToViewController:view animated:YES];
        }
    }
    
    if (self.delegate != nil) {
        
        if (filterType == GPUIMAGE_NONE)
        {
            [self.delegate filterSelected:@"FALSE"];
           
        }
        else
        {
            [self.delegate filterSelected:@"TRUE"];
        }
        
         [self.delegate mediaSelected:_updatedPhoto mediaUrl:_sampleURL];
    }
}

- (void)moveToVideoPublishScreen {
    VideoPublishController *obj = [[VideoPublishController alloc] initWithNibName:@"VideoPublishController" bundle:[NSBundle mainBundle]];
    obj.sampleURL = _sampleURL;
    obj.filterValue = slider.value;
    obj.typeFilter = filterType;
    obj.updatedPhoto = _updatedPhoto;
    obj.strPeepText = _strPeepText;
    obj.bWatermarkAdded = bWatermarkAdded;
    obj.brightnessVal = brightnessVal;
    obj.contrastVal = contrastVal;
    obj.saturationVal = saturationVal;
    obj.isComingFromPeepTextFlow = _isComingFromPeepTextFlow;
    obj.isComingFromEventsFlow = _isComingFromEventsFlow;
    [self.navigationController pushViewController:obj animated:YES];
}

-(void)viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:animated];
    
    [self cleanTargets];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


#pragma mark -


- (void)retrievingProgress
{
    //self.progressLabel.text = [NSString stringWithFormat:@"%d%%", (int)(movieFile.progress * 100)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Collection View Methods

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return [arrFilters count];
}

// The cell that is returned must be retrieved from a call to
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    @try {
        
        CollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"filterCell1" forIndexPath:indexPath];
        
        NSString *value = [self filterTitle:indexPath.row];
        
        cell.lblFilterName.text = value;
        
        GPUImageView *imgView = [arrImages objectAtIndex:indexPath.row];
        
        [cell.imgFilter addSubview:imgView];

        return cell;
        
    }
    @catch (NSException *exception) {
        
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    brightnessVal = 0.0;
    contrastVal = 1.0;
    saturationVal = 1.0;
    
    NSString *strFilter = [arrFilters objectAtIndex:indexPath.row];
    
    if([strFilter isEqualToString:@"GPUIMAGE_BRIGHTNESS"])
    {
        filterType = GPUIMAGE_BRIGHTNESS;
        [self setApplyFilter:filterType];
    }
    else if([strFilter isEqualToString:@"GPUIMAGE_GRAYSCALE"])
    {
        filterType = GPUIMAGE_GRAYSCALE;
        [self setApplyFilter:filterType];
    }
    else if([strFilter isEqualToString:@"GPUIMAGE_COLORINVERT"])
    {
        filterType = GPUIMAGE_COLORINVERT;
        [self setApplyFilter:filterType];
    }
    else if([strFilter isEqualToString:@"GPUIMAGE_SEPIA"])
    {
        filterType = GPUIMAGE_SEPIA;
        [self setApplyFilter:filterType];
    }
    else if([strFilter isEqualToString:@"GPUIMAGE_VIGNETTE"])
    {
        filterType = GPUIMAGE_VIGNETTE;
        [self setApplyFilter:filterType];
    }
    else if([strFilter isEqualToString:@"GPUIMAGE_ORIGINAL"])
    {
        filterType = GPUIMAGE_ORIGINAL;
        [self setApplyFilter:filterType];
    }
    else if([strFilter isEqualToString:@"GPUIMAGE_GAMMA"])
    {
        filterType = GPUIMAGE_GAMMA;
        [self setApplyFilter:filterType];
    }
    else if([strFilter isEqualToString:@"GPUIMAGE_HUE"])
    {
        filterType = GPUIMAGE_HUE;
        [self setApplyFilter:filterType];
    }
    else if([strFilter isEqualToString:@"GPUIMAGE_OPACITY"])
    {
        filterType = GPUIMAGE_OPACITY;
        [self setApplyFilter:filterType];
    }
    else if([strFilter isEqualToString:@"GPUIMAGE_WHITEBALANCE"])
    {
        filterType = GPUIMAGE_WHITEBALANCE;
        [self setApplyFilter:filterType];
    }
    else if([strFilter isEqualToString:@"GPUIMAGE_LUMINANCE_RANGE"])
    {
        filterType = GPUIMAGE_LUMINANCE_RANGE;
        [self setApplyFilter:filterType];
    }
    else if([strFilter isEqualToString:@"GPUIMAGE_HALFTONE"])
    {
        filterType = GPUIMAGE_HALFTONE;
        [self setApplyFilter:filterType];
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    return CGSizeMake(100.0, 100.0);
}


 #pragma mark- Processing original image with all filters
 
- (void) processFilterImage{
    
    [arrImages removeAllObjects];
    
    arrImages = [[NSMutableArray alloc] init];
    
    for (int i = 0; i< [arrFilters count]; i++)
    {
        [filter removeAllTargets];
        filter = nil;
        [gpuImage removeAllTargets];
        
        GPUImageView *imgView = [[GPUImageView alloc] initWithFrame:CGRectMake(0, 0, 80, 80)];
        
        
        filter = [[GPUImageSepiaFilter alloc] init];
        
        switch (i) {
                
            case 0:{
                
                GPUImageBrightnessFilter *brightFilter = [[GPUImageBrightnessFilter alloc] init];
                brightFilter.brightness = 0.0;
                filter = brightFilter;
                brightFilter = nil;
                
            }
                break;
                
            case 1:
            {
                filter = [[GPUImageSepiaFilter alloc] init];
                
            }
                break;
            case 2:
            {
                filter = [[GPUImageGrayscaleFilter alloc] init];
                
            }
                break;
                
            case 3:
            {
                filter = [[GPUImageColorInvertFilter alloc] init];
                
            }
                break;
            case 4:
            {
                filter = [[GPUImageVignetteFilter alloc] init];
                
                
            }
                break;
            case 5:
            {
                GPUImageGammaFilter *gammaFilter = [[GPUImageGammaFilter alloc] init];
                gammaFilter.gamma = 2.0;
                filter = gammaFilter;
                gammaFilter = nil;
                
                
            }
                break;
            case 6:
            {
                GPUImageHueFilter *hueFilter = [[GPUImageHueFilter alloc] init];
                hueFilter.hue = 20;
                filter = hueFilter;
                hueFilter = nil;
                
            }
                break;
            case 7:
            {
                GPUImageWhiteBalanceFilter *whiteBalanceFilter = [[GPUImageWhiteBalanceFilter alloc] init];
                whiteBalanceFilter.temperature = 7000;
                whiteBalanceFilter.tint = 0;
                filter = whiteBalanceFilter;
                whiteBalanceFilter = nil;
            }
                break;
            case 8:
            {
                GPUImageLuminanceThresholdFilter *luminanceFilter = [[GPUImageLuminanceThresholdFilter alloc] init];
                filter = luminanceFilter;
                luminanceFilter = nil;
            }
                break;
            case 9:
            {
                filter = [[GPUImageHalftoneFilter alloc] init];
                
            }
                break;
        }
        
        [filter addTarget:imgView];
        [gpuImage addTarget:filter];
        [gpuImage processImage];
        
        
        //added original image
        [arrImages addObject:imgView];
    }
    
//    UICollectionViewFlowLayout *flowLayout =  (UICollectionViewFlowLayout*)self.collPresets.collectionViewLayout;
//    
//    [flowLayout setItemSize:CGSizeMake(80.0, 80.0)];
    
    self.collPresets.hidden = false;
    self.collPresets.delegate = self;
    self.collPresets.dataSource = self;
    
    [self.collPresets reloadData];
    
}


/*
 #pragma mark- Applying Filter
 
 -(UIImage*) applyCoreImageFilter:(int)index image:(UIImage*)img{
 
 UIImageOrientation imgOrientation = img.imageOrientation;
 
 CGImageRef cgimg = img.CGImage;
 
 CIImage *coreImage = [[CIImage alloc] initWithCGImage:cgimg];
 
 CIImage *Output = [self filtername:index coreImg:coreImage];
 
 CIContext *context = [CIContext contextWithOptions:nil];
 
 CGImageRef output = [context createCGImage:Output fromRect:Output.extent];
 
 UIImage* result = [[UIImage alloc] initWithCGImage:output scale:1.0 orientation:imgOrientation];
 
 result = [self imageResize:result change:CGSizeMake([UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.width )];
 
 return result;
 
 }
 
 - (CIImage*) filtername: (int)index coreImg:(CIImage*)coreImage{
 
 //return coreImage;
 
 switch (index) {
 
 case 1:{
 
 CIFilter *filter1 = [CIFilter filterWithName:@"CISepiaTone"];
 [filter1 setValue:coreImage forKey:kCIInputImageKey];
 [filter1 setValue:@1.0 forKey:@"inputIntensity"];
 CIImage *result = [filter1 valueForKey:kCIOutputImageKey];
 return result;
 }
 break;
 
 case 2:
 {
 CIFilter *filter1 = [CIFilter filterWithName:@"CIPhotoEffectMono"];
 [filter1 setValue:coreImage forKey:kCIInputImageKey];
 CIImage *result = [filter1 valueForKey:kCIOutputImageKey];
 return result;
 }
 break;
 
 case 3:
 {
 CIFilter *filter1 = [CIFilter filterWithName:@"CIColorInvert"];
 [filter1 setValue:coreImage forKey:kCIInputImageKey];
 CIImage *result = [filter1 valueForKey:kCIOutputImageKey];
 return result;
 }
 break;
 case 4:
 {
 CIFilter *filter1 = [CIFilter filterWithName:@"CIVignette"];
 [filter1 setValue:coreImage forKey:kCIInputImageKey];
 [filter1 setValue:@2.0 forKey:@"inputIntensity"];
 [filter1 setValue:@30.0 forKey:@"inputRadius"];
 
 CIImage *result = [filter1 valueForKey:kCIOutputImageKey];
 return result;
 }
 break;
 case 5:
 {
 
 CIFilter *filter1 = [CIFilter filterWithName:@"CIGammaAdjust"];
 [filter1 setValue:coreImage forKey:kCIInputImageKey];
 [filter1 setValue:@2.0 forKey:@"inputPower"];
 CIImage *result = [filter1 valueForKey:kCIOutputImageKey];
 return result;
 }
 break;
 case 6:
 {
 
 CIFilter *filter1 = [CIFilter filterWithName:@"CIHueAdjust"];
 [filter1 setValue:coreImage forKey:kCIInputImageKey];
 [filter1 setValue:@20.0 forKey:@"inputAngle"];
 CIImage *result = [filter1 valueForKey:kCIOutputImageKey];
 return result;
 }
 break;
 case 7:
 {
 
 CIFilter *filter1 = [CIFilter filterWithName:@"CIWhitePointAdjust"];
 [filter1 setValue:coreImage forKey:kCIInputImageKey];
 //[filter1 setValue:@1.0 forKey:@"inputColor"]; //needs to be update the color we want to do, any CIColor
 CIImage *result = [filter1 valueForKey:kCIOutputImageKey];
 return result;
 }
 break;
 case 8:
 {
 
 CIFilter *filter1 = [CIFilter filterWithName:@"CISharpenLuminance"];
 [filter1 setValue:coreImage forKey:kCIInputImageKey];
 [filter1 setValue:@0.30 forKey:@"inputSharpness"];
 CIImage *result = [filter1 valueForKey:kCIOutputImageKey];
 return result;
 }
 break;
 case 9:
 {
 
 CIFilter *filter1 = [CIFilter filterWithName:@"CICMYKHalftone"];
 [filter1 setValue:coreImage forKey:kCIInputImageKey];
 CIImage *result = [filter1 valueForKey:kCIOutputImageKey];
 return result;
 }
 break;
 }
 
 CIFilter *filter1 = [CIFilter filterWithName:@"CIColorControls"];
 [filter1 setValue:coreImage forKey:kCIInputImageKey];
 [filter1 setValue:@0.0 forKey:kCIInputBrightnessKey];
 CIImage *result = [filter1 valueForKey:kCIOutputImageKey];
 return result;
 
 }
 */

-(NSString*)filterTitle:(NSInteger)index
{
    switch (index) {
        case 0:
            return @"Original";
        case 1:
            return @"Sepia";
        case 2:
            return @"GrayScale";
        case 3:
            return @"Invert";
        case 4:
            return @"Vignette";
        case 5:
            return @"Gamma";
        case 6:
            return @"Hue";
        case 7:
            return @"WhiteBalace";
        case 8:
            return @"Luminance";
        case 9:
            return @"Halftone";
        default:
            return @"";
    }
}


#pragma mark- Resizing image

- (UIImage*) imageResize : (UIImage*)imageObj change:(CGSize)sizeChange {
    
    bool hasAlpha = false;
    CGFloat scale = 0.0;
    
    UIGraphicsBeginImageContextWithOptions(sizeChange, !hasAlpha, scale);
    [imageObj drawInRect:CGRectMake(0.0, 0.0, sizeChange.width, sizeChange.height)];
    UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return scaledImage;
}

#pragma mark - Set Apply Filter

- (void)setApplyFilter:(GPUImageShowcaseFilterType) filterTypes
{
    
    //remove exiting attached filter
    [self removeFilter];
    
    //set new filter
    if (filterTypes != GPUIMAGE_ORIGINAL)
        [self setFilterMethod:filterTypes];
    
    [self addRemoveWatermarkFilter];
}

- (void) addRemoveWatermarkFilter{
    
    if(bWatermarkAdded)
    {
        [self addMultipleFiltersChain];
        
        if(bWritingFile)
        {
            [blendFilter addTarget:movieWriter];
        }
    }
    else
    {
        [self addMultipleFiltersChain];
        
        if(bWritingFile)
        {
            if(filter)
                [filter addTarget:movieWriter];
            else
                [saturationFilter addTarget:movieWriter];
        }
    }
}

#pragma mark- Apply Filter

- (void) applyFilter{
    
    if(movieWriter)
    {
        [movieWriter cancelRecording];
        [movieWriter endProcessing];
    }
    
    AVAsset *asset = [AVAsset assetWithURL: _sampleURL];
    orientation = [asset videoOrientation];
    
    movieFile = [[GPUImageMovie alloc] initWithURL:_sampleURL];
    
    movieFile.runBenchmark = YES;
    movieFile.playAtActualSpeed = YES;
    movieFile.shouldRepeat = YES;
    movieFile.playSound = YES;
    
    filterType = GPUIMAGE_NONE;
    
    [self addRemoveWatermarkFilter];
    
    //movieFile
    [movieFile  startProcessing];
}

- (void) correctVideoOrientation{
    
    if (orientation == LBVideoOrientationUp)
    {
        NSLog(@"Up"); //this is the orientation after recording the video
        
        [brightnessFilter setInputRotation:kGPUImageRotateRight atIndex:0];
    }
    else if (orientation == LBVideoOrientationDown)
    {
        NSLog(@"Down");
    }
    else if (orientation == LBVideoOrientationLeft)
    {
        NSLog(@"Left");
    }
    else if (orientation == LBVideoOrientationRight)
    {
        NSLog(@"Right"); //this is the orientation after picking existing video from photos
    }
}

//MARK:- Create Thumbnail With Filter Values
- (void) createThumbnailWithFilters{
    
    [self cleanTargets];
    
    for (UIView *v in self.customView.subviews)
    {
        [v removeFromSuperview];
    }
    
    brightnessFilter = [[GPUImageBrightnessFilter alloc] init]; //2
    contrastFilter = [[GPUImageContrastFilter alloc] init]; //3
    saturationFilter = [[GPUImageSaturationFilter alloc] init]; //4
    
    [(GPUImageBrightnessFilter*)brightnessFilter setBrightness:brightnessVal]; // change value between -1.00 to 1.00
    [(GPUImageContrastFilter*)contrastFilter setContrast:contrastVal];     // change value between 0.00 to 4.00
    [(GPUImageSaturationFilter*)saturationFilter setSaturation:saturationVal];   //change value between 0.00 to 2.00
    
    GPUImagePicture *gpuImage = [[GPUImagePicture alloc] initWithImage:_updatedPhoto];
    
    GPUImageView *imgView = [[GPUImageView alloc] initWithFrame:_customView.bounds];
    [_customView addSubview:imgView];
    
    [gpuImage addTarget:brightnessFilter];
    [brightnessFilter addTarget:contrastFilter];
    [contrastFilter addTarget:saturationFilter];
    
    if(filter)
    {
        [saturationFilter addTarget:filter];
        [filter addTarget:imgView];
    }
    else
    {
        [saturationFilter addTarget:imgView];
    }
    
    [gpuImage processImage];
}

- (void) addMultipleFiltersChainForEventDetailFlow:(GPUImageOutput<GPUImageInput> *)filter1{
    
    groupFilter = [[GPUImageFilterGroup alloc]init]; //1
    
    brightnessFilter = [[GPUImageBrightnessFilter alloc] init]; //2
    contrastFilter = [[GPUImageContrastFilter alloc] init]; //3
    saturationFilter = [[GPUImageSaturationFilter alloc] init]; //4
    
    [(GPUImageBrightnessFilter*)brightnessFilter setBrightness:brightnessVal]; // change value between -1.00 to 1.00
    [(GPUImageContrastFilter*)contrastFilter setContrast:contrastVal];     // change value between 0.00 to 4.00
    [(GPUImageSaturationFilter*)saturationFilter setSaturation:saturationVal];   //change value between 0.00 to 2.00
    
    
    [self correctVideoOrientation];
    
    
    [(GPUImageFilterGroup *)groupFilter addFilter:brightnessFilter];
    [(GPUImageFilterGroup *)groupFilter addFilter:contrastFilter];
    [(GPUImageFilterGroup *)groupFilter addFilter:saturationFilter];
    
    if(filter1)
        [(GPUImageFilterGroup *)groupFilter addFilter:filter1];
    
    [movieFile addTarget:brightnessFilter];
    [brightnessFilter addTarget:contrastFilter];
    [contrastFilter addTarget:saturationFilter];
    
    if(filter1)
        [saturationFilter addTarget:filter1];
    
    [(GPUImageFilterGroup *) groupFilter setInitialFilters:[NSArray arrayWithObject: brightnessFilter]];
    
    if(filter1)
        [(GPUImageFilterGroup *) groupFilter setTerminalFilter:filter1];
    else
        [(GPUImageFilterGroup *) groupFilter setTerminalFilter:saturationFilter];
    
    
    [brightnessFilter forceProcessingAtSize:filterView.sizeInPixels];
    [contrastFilter forceProcessingAtSize:filterView1.sizeInPixels];
    [saturationFilter forceProcessingAtSize:filterView2.sizeInPixels];
    
    if(filter1 && !bWritingFile)
    {
        //filterView3
        
        CGRect cameraFrame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.width);
        
        UIView* viewCamera = [[UIView alloc] initWithFrame:cameraFrame];
        viewCamera.backgroundColor = [UIColor redColor];
        
        filterView3 = [[GPUImageView alloc] initWithFrame:cameraFrame];
        filterView3.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
        [filterView3 setFillMode:kGPUImageFillModePreserveAspectRatioAndFill];
        
        viewCamera = filterView3;
        
        [self.customView addSubview:viewCamera];
        
        [filter forceProcessingAtSize:filterView3.sizeInPixels];
        
    }
    
    [brightnessFilter addTarget:filterView];
    [contrastFilter addTarget:filterView1];
    [saturationFilter addTarget:filterView2];
    if(filter1)
        [filter1 addTarget:filterView3];
    
    if(bWatermarkAdded)
    {
        blendFilter = [[GPUImageAlphaBlendFilter alloc] init];
        blendFilter.mix = 1.0;
        [(GPUImageFilterGroup *)groupFilter addFilter:blendFilter];
        [(GPUImageFilterGroup *) groupFilter setTerminalFilter:blendFilter];
        
        //filterView4
        
        CGRect cameraFrame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.width);
        
        UIView* viewCamera = [[UIView alloc] initWithFrame:cameraFrame];
        viewCamera.backgroundColor = [UIColor redColor];
        
        filterView4 = [[GPUImageView alloc] initWithFrame:cameraFrame];
        filterView4.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
        [filterView4 setFillMode:kGPUImageFillModePreserveAspectRatioAndFill];
        
        viewCamera = filterView4;
        
        [self.customView addSubview:viewCamera];
        
        [blendFilter forceProcessingAtSize:filterView4.sizeInPixels];
        
        //add blend filter
        [blendFilter addTarget:filterView4];
        
        CGRect cameraFrame1 = _customView.frame;
        UIView *contentView = [[UIView alloc] initWithFrame:cameraFrame1];
        contentView.backgroundColor = [UIColor clearColor];
        
        UIImageView *ivTemp = [[UIImageView alloc] initWithFrame:CGRectMake(20, 20, 40, 40)];
        ivTemp.image = imgBrighnessSelected; // self.selectedWatermarkImage; [UIImage imageNamed:@"brighnessSelected_t1"]
        [contentView addSubview:ivTemp];
        
        uiElementInput = [[GPUImageUIElement alloc] initWithView:contentView];
        [uiElementInput setShouldSmoothlyScaleOutput:YES];
        
        if(filter1)
        {
            [filter1 addTarget:blendFilter];
            [uiElementInput addTarget:blendFilter];
            
            __weak GPUImageUIElement *weakElement = uiElementInput;
            [filter1 setFrameProcessingCompletionBlock:^(GPUImageOutput * filter, CMTime frameTime){
                
                [weakElement update];
            }];
        }
        else
        {
            [saturationFilter addTarget:blendFilter];
            [uiElementInput addTarget:blendFilter];
            
            
            __weak GPUImageUIElement *weakElement = uiElementInput;
            [saturationFilter setFrameProcessingCompletionBlock:^(GPUImageOutput * filter, CMTime frameTime){
                
                [weakElement update];
            }];
        }
    }

}

- (void) addMultipleFiltersChain{
    
    groupFilter = [[GPUImageFilterGroup alloc]init]; //1
    
    brightnessFilter = [[GPUImageBrightnessFilter alloc] init]; //2
    contrastFilter = [[GPUImageContrastFilter alloc] init]; //3
    saturationFilter = [[GPUImageSaturationFilter alloc] init]; //4
    
    [(GPUImageBrightnessFilter*)brightnessFilter setBrightness:brightnessVal]; // change value between -1.00 to 1.00
    [(GPUImageContrastFilter*)contrastFilter setContrast:contrastVal];     // change value between 0.00 to 4.00
    [(GPUImageSaturationFilter*)saturationFilter setSaturation:saturationVal];   //change value between 0.00 to 2.00
    
    
    [self correctVideoOrientation];
    
    
    [(GPUImageFilterGroup *)groupFilter addFilter:brightnessFilter];
    [(GPUImageFilterGroup *)groupFilter addFilter:contrastFilter];
    [(GPUImageFilterGroup *)groupFilter addFilter:saturationFilter];
    
    if(filter)
        [(GPUImageFilterGroup *)groupFilter addFilter:filter];
    
    [movieFile addTarget:brightnessFilter];
    [brightnessFilter addTarget:contrastFilter];
    [contrastFilter addTarget:saturationFilter];
    
    if(filter)
        [saturationFilter addTarget:filter];
    
    [(GPUImageFilterGroup *) groupFilter setInitialFilters:[NSArray arrayWithObject: brightnessFilter]];
    
    if(filter)
        [(GPUImageFilterGroup *) groupFilter setTerminalFilter:filter];
    else
        [(GPUImageFilterGroup *) groupFilter setTerminalFilter:saturationFilter];
    
    
    [brightnessFilter forceProcessingAtSize:filterView.sizeInPixels];
    [contrastFilter forceProcessingAtSize:filterView1.sizeInPixels];
    [saturationFilter forceProcessingAtSize:filterView2.sizeInPixels];
    
    if(filter && !bWritingFile)
    {
        //filterView3
        
        CGRect cameraFrame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.width);
        
        UIView* viewCamera = [[UIView alloc] initWithFrame:cameraFrame];
        viewCamera.backgroundColor = [UIColor redColor];
        
        filterView3 = [[GPUImageView alloc] initWithFrame:cameraFrame];
        filterView3.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
        [filterView3 setFillMode:kGPUImageFillModePreserveAspectRatioAndFill];
        
        viewCamera = filterView3;
        
        [self.customView addSubview:viewCamera];
        
        [filter forceProcessingAtSize:filterView3.sizeInPixels];
        
    }
    
    [brightnessFilter addTarget:filterView];
    [contrastFilter addTarget:filterView1];
    [saturationFilter addTarget:filterView2];
    if(filter)
        [filter addTarget:filterView3];
    
    if(bWatermarkAdded)
    {
        blendFilter = [[GPUImageAlphaBlendFilter alloc] init];
        blendFilter.mix = 1.0;
        [(GPUImageFilterGroup *)groupFilter addFilter:blendFilter];
        [(GPUImageFilterGroup *) groupFilter setTerminalFilter:blendFilter];
        
        //filterView4
        
        CGRect cameraFrame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.width);
        
        UIView* viewCamera = [[UIView alloc] initWithFrame:cameraFrame];
        viewCamera.backgroundColor = [UIColor redColor];
        
        filterView4 = [[GPUImageView alloc] initWithFrame:cameraFrame];
        filterView4.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
        [filterView4 setFillMode:kGPUImageFillModePreserveAspectRatioAndFill];
        
        viewCamera = filterView4;
        
        [self.customView addSubview:viewCamera];
        
        [blendFilter forceProcessingAtSize:filterView4.sizeInPixels];
        
        //add blend filter
        [blendFilter addTarget:filterView4];
        
        CGRect cameraFrame1 = _customView.frame;
        UIView *contentView = [[UIView alloc] initWithFrame:cameraFrame1];
        contentView.backgroundColor = [UIColor clearColor];
       
        UIImageView *ivTemp = [[UIImageView alloc] initWithFrame:CGRectMake(20, 20, 40, 40)];
        ivTemp.image = imgBrighnessSelected; // self.selectedWatermarkImage;
        [contentView addSubview:ivTemp];
        
        uiElementInput = [[GPUImageUIElement alloc] initWithView:contentView];
        [uiElementInput setShouldSmoothlyScaleOutput:YES];
        
        if(filter)
        {
            [filter addTarget:blendFilter];
            [uiElementInput addTarget:blendFilter];
            
            __weak GPUImageUIElement *weakElement = uiElementInput;
            [filter setFrameProcessingCompletionBlock:^(GPUImageOutput * filter, CMTime frameTime){
                
                [weakElement update];
            }];
        }
        else
        {
            [saturationFilter addTarget:blendFilter];
            [uiElementInput addTarget:blendFilter];
            
            
            __weak GPUImageUIElement *weakElement = uiElementInput;
            [saturationFilter setFrameProcessingCompletionBlock:^(GPUImageOutput * filter, CMTime frameTime){
                
                [weakElement update];
            }];
        }
    }
}

#pragma mark- Clean Targets

- (void) saveFilterVideo{
    
    if (filterType != GPUIMAGE_NONE)
    {
        NSLog(@"setting the filter ...");
        [self setFilterMethod:filterType];
    }

    GPUImageOutput<GPUImageInput> *filterNew;
    
    if (filter)
    {
        NSLog(@"found the filter ...");
        
        filterNew = filter;
    }
    else
    {
        NSLog(@"Did not found the filter ...");
    }
    
    bWritingFile = true;
    
    _pathToMovie = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/Movie.mp4"];
    unlink([_pathToMovie UTF8String]); // If a file already exists, AVAssetWriter won't let you record new frames, so delete the old movie
    _movieURL = [NSURL fileURLWithPath:_pathToMovie];
    
    NSMutableDictionary *outputSettings = [[NSMutableDictionary alloc] init];
    [outputSettings setObject:AVVideoCodecH264 forKey:AVVideoCodecKey];
    [outputSettings setObject:[NSNumber numberWithInt:320] forKey:AVVideoWidthKey];
    [outputSettings setObject:[NSNumber numberWithInt:320] forKey:AVVideoHeightKey];
    
    NSDictionary *videoCleanApertureSettings = [NSDictionary dictionaryWithObjectsAndKeys:
                                                [NSNumber numberWithInt:320], AVVideoCleanApertureWidthKey,
                                                [NSNumber numberWithInt:320], AVVideoCleanApertureHeightKey,
                                                [NSNumber numberWithInt:0], AVVideoCleanApertureHorizontalOffsetKey,
                                                [NSNumber numberWithInt:0], AVVideoCleanApertureVerticalOffsetKey,
                                                nil];
    NSDictionary *videoAspectRatioSettings = [NSDictionary dictionaryWithObjectsAndKeys:
                                              [NSNumber numberWithInt:3], AVVideoPixelAspectRatioHorizontalSpacingKey,
                                              [NSNumber numberWithInt:3], AVVideoPixelAspectRatioVerticalSpacingKey,
                                              nil];
    
    NSMutableDictionary * compressionProperties = [[NSMutableDictionary alloc] init];
    [compressionProperties setObject:videoCleanApertureSettings forKey:AVVideoCleanApertureKey];
    [compressionProperties setObject:videoAspectRatioSettings forKey:AVVideoPixelAspectRatioKey];
    [compressionProperties setObject:[NSNumber numberWithInt: 960000] forKey:AVVideoAverageBitRateKey];
    [compressionProperties setObject:[NSNumber numberWithInt: 30] forKey:AVVideoMaxKeyFrameIntervalKey];
    [compressionProperties setObject:AVVideoProfileLevelH264Main31 forKey:AVVideoProfileLevelKey];
    
    [outputSettings setObject:compressionProperties forKey:AVVideoCompressionPropertiesKey];
    
    movieWriter = [[GPUImageMovieWriter alloc] initWithMovieURL:_movieURL size:CGSizeMake(320.0, 320.0) fileType:AVFileTypeQuickTimeMovie outputSettings:outputSettings];
    
    movieFile = [[GPUImageMovie alloc] initWithURL:_sampleURL];
    movieFile.runBenchmark = YES;
    movieFile.playAtActualSpeed = NO;
    
    if(bWatermarkAdded)
    {
        [self addMultipleFiltersChainForEventDetailFlow:filterNew];
        
        if(bWritingFile)
        {
            [blendFilter addTarget:movieWriter];
        }
    }
    else
    {
        [self addMultipleFiltersChainForEventDetailFlow:filterNew];
        
        if(bWritingFile)
        {
            if(filterNew)
                [filterNew addTarget:movieWriter];
            else
                [saturationFilter addTarget:movieWriter];
        }
    }
    
    filterView.hidden = NO;
    
    // Configure this for video from the movie file, where we want to preserve all video frames and audio samples
    movieWriter.shouldPassthroughAudio = YES;
    movieFile.audioEncodingTarget = movieWriter;
    [movieFile enableSynchronizedEncodingUsingMovieWriter:movieWriter];
    
    //Start processing
    
    [movieWriter startRecording];
    [movieFile  startProcessing];
    
    __weak GPUImageMovieWriter *weakMovieWriter = movieWriter;
    __weak GPUImageMovie *weakmovieFile = movieFile;
    __weak typeof(self) weakSelf = self;
    //AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

    [movieWriter setCompletionBlock:^{
        
        [weakMovieWriter finishRecording];
        
        bWritingFile = false;
        
        weakmovieFile.audioEncodingTarget = nil;
        
        dispatch_async(dispatch_get_main_queue(), ^{
           // [appDelegate hideLoader];
            [weakSelf.loaderWaitVideoProcessing stopAnimating];
            [weakSelf.vwWaitVideoProcessing setHidden:YES];
            weakSelf.navigationItem.leftBarButtonItem.enabled = YES;
            weakSelf.navigationItem.rightBarButtonItem.enabled = YES;
            [weakSelf moveToNextScreen];
        });
       
        
//        if (_isComingFromEventsFlow)
//        {
//            [indicator stopAnimating];
//            indicator.hidden = true;
//            
//            [self moveToEventDetailScreen];
//        }
        
    }]; //movieWriter ends here
}

- (void) removeFilter{
    
    if(movieFile)
    {
        [movieFile removeAllTargets];
        
        if(blendFilter)
        {
            [filterView4 removeFromSuperview];
            filterView4 = nil;
            
            [blendFilter removeAllTargets];
            blendFilter = nil;
            
            [uiElementInput removeAllTargets];
            uiElementInput = nil;
        }
        
        if(filter)
        {
            [filterView3 removeFromSuperview];
            filterView3 = nil;
            
            [filter removeAllTargets];
            filter = nil;
        }
        
        [groupFilter removeAllTargets];
        groupFilter = nil;
        
        [brightnessFilter removeAllTargets];
        brightnessFilter = nil;
        
        [saturationFilter removeAllTargets];
        saturationFilter = nil;
        
        [contrastFilter removeAllTargets];
        contrastFilter = nil;
    }
}

- (void) cleanTargets{
    
    if(blendFilter)
    {
        [blendFilter removeTarget:filterView];
        [blendFilter removeAllTargets];
        blendFilter = nil;
        
        [uiElementInput removeAllTargets];
        uiElementInput = nil;
    }
    
    if(filter)
    {
        [filter removeAllTargets];
        filter = nil;
    }
    
    [groupFilter removeAllTargets];
    groupFilter = nil;
    
    [brightnessFilter removeAllTargets];
    brightnessFilter = nil;
    
    [saturationFilter removeAllTargets];
    saturationFilter = nil;
    
    [contrastFilter removeAllTargets];
    contrastFilter = nil;
    
    [movieFile removeAllTargets];
    [movieFile endProcessing];
    movieFile = nil;
}

#pragma mark- Set Filter

- (void) setFilterMethod:(GPUImageShowcaseFilterType)type{
    
    filterType = type;
    
    switch (type)
    {
            
        case GPUIMAGE_SEPIA:
        {
            filter = [[GPUImageSepiaFilter alloc] init];
        }; break;
        case GPUIMAGE_GRAYSCALE:
        {
            filter = [[GPUImageGrayscaleFilter alloc] init];
        }; break;
        case GPUIMAGE_COLORINVERT:
        {
            filter = [[GPUImageColorInvertFilter alloc] init];
        }; break;
        case GPUIMAGE_BRIGHTNESS:
        {
            GPUImageBrightnessFilter *brightFilter = [[GPUImageBrightnessFilter alloc] init];
            brightFilter.brightness = 0.0;
            filter = brightFilter;
            brightFilter = nil;
        }break;
            
        case GPUIMAGE_SHARPEN:
        {
            filter = [[GPUImageSharpenFilter alloc] init];
        }   break;
            
        case GPUIMAGE_VIGNETTE:
        {
            filter = [[GPUImageVignetteFilter alloc] init];
        }   break;
            
        case GPUIMAGE_SATURATION:
        {
            filter = [[GPUImageSaturationFilter alloc] init];
        }   break;
            
        case GPUIMAGE_CONTRAST:
        {
            filter = [[GPUImageContrastFilter alloc] init];
        }   break;
        case GPUIMAGE_GAMMA:
        {
            GPUImageGammaFilter *gammaFilter = [[GPUImageGammaFilter alloc] init];
            gammaFilter.gamma = 2.0;
            filter = gammaFilter;
            gammaFilter = nil;
            
        }   break;
        case GPUIMAGE_HUE:
        {
            GPUImageHueFilter *hueFilter = [[GPUImageHueFilter alloc] init];
            hueFilter.hue = 20;
            filter = hueFilter;
            hueFilter = nil;
            
        }   break;
        case GPUIMAGE_OPACITY:
        {
            GPUImageOpacityFilter *opacityFilter = [[GPUImageOpacityFilter alloc] init];
            opacityFilter.opacity = 0.8;
            filter = opacityFilter;
            opacityFilter = nil;
            
        }   break;
        case GPUIMAGE_WHITEBALANCE:
        {
            GPUImageWhiteBalanceFilter *whiteBalanceFilter = [[GPUImageWhiteBalanceFilter alloc] init];
            whiteBalanceFilter.temperature = 7000;
            whiteBalanceFilter.tint = 0;
            filter = whiteBalanceFilter;
            whiteBalanceFilter = nil;
            
        }   break;
        case GPUIMAGE_LUMINANCE_RANGE:
        {
            GPUImageLuminanceThresholdFilter *luminanceFilter = [[GPUImageLuminanceThresholdFilter alloc] init];
            filter = luminanceFilter;
            luminanceFilter = nil;
        }   break;
        case GPUIMAGE_HALFTONE:
        {
            filter = [[GPUImageHalftoneFilter alloc] init];
        }   break;
            
    }
    
}
//MARK:- Set Constraints

- (void) setConstraint{
    
    NSLog(@"Width: %f, Height: %f", [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
    
    CGFloat bb = [UIScreen mainScreen].bounds.size.width - [UIScreen mainScreen].bounds.size.height/2;
    
    CGFloat kk = ([UIScreen mainScreen].bounds.size.height-40 - [UIScreen mainScreen].bounds.size.width)/2;

    CGFloat cc = 64 + bb + kk;
    
    sliderY.constant = cc - 15;
    cropY.constant = cc - 35;
    brightnessY.constant = cc - 35;
    rotateY.constant = cc - 35;

    colletionY.constant = cc-50;
    
}

- (IBAction)clickAddFilter:(id)sender {
    
    if(bEditVideo)
    {
        //user click on cancel button
        [btnFilter setImage:imgFilterBtmSelected forState:UIControlStateNormal];//[UIImage imageNamed:@"filterBtmSelected_t1"]
        [btnEdit setImage:imgEditBtm forState:UIControlStateNormal];//[UIImage imageNamed:@"editBtm_t1"]
        
        bEditVideo = false;
        
        slider.hidden = true;
        btnCrop.hidden = false;
        btnRotate.hidden = false;
        btnBrightness.hidden = false;
        lblContrast.hidden = false;
        lblBrightness.hidden = false;
        lblSaturation.hidden = false;
        collPresets.hidden = true;
        
        switch (baseFilterType)
        {
                
            case GPUIMAGE_BRIGHTNESS:
            {
                [(GPUImageBrightnessFilter *)brightnessFilter setBrightness:brightnessVal];
                
            }break;
                
            case GPUIMAGE_CONTRAST:
            {
                [(GPUImageContrastFilter *)contrastFilter setContrast:contrastVal];
                
            }   break;
                
            case GPUIMAGE_SATURATION:
            {
                [(GPUImageSaturationFilter *)saturationFilter setSaturation:saturationVal];
                
            }   break;
            default:
            {
                [(GPUImageBrightnessFilter *)brightnessFilter setBrightness:brightnessVal];
            }break;
        }
    }
    else
    {
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Next" style:UIBarButtonItemStylePlain target:self action:@selector(next)];
        [btnFilter setImage:imgFilterBtmSelected forState:UIControlStateNormal];
        [btnEdit setImage:imgEditBtm forState:UIControlStateNormal];
        
        btnNext.hidden = false;
        
        btnCrop.hidden = true;
        btnRotate.hidden = true;
        btnBrightness.hidden = true;
        lblContrast.hidden = true;
        lblBrightness.hidden = true;
        lblSaturation.hidden = true;
        self.collPresets.hidden = false;
        slider.hidden = true;
    }
}

- (IBAction)sliderValueChanged:(id)sender {
    
    switch (baseFilterType)
    {
        case GPUIMAGE_BRIGHTNESS:
        {
            [(GPUImageBrightnessFilter *)brightnessFilter setBrightness:[(UISlider *)sender value]];
            
            
        }break;
            
        case GPUIMAGE_CONTRAST:
        {
            [(GPUImageContrastFilter *)contrastFilter setContrast:[(UISlider *)sender value]];
            
            
        }   break;
            
        case GPUIMAGE_SATURATION:
        {
            [(GPUImageSaturationFilter *)saturationFilter setSaturation:[(UISlider *)sender value]];
            
            
        }   break;
        default:
        {
            [(GPUImageBrightnessFilter *)brightnessFilter setBrightness:[(UISlider *)sender value]];
            
        }break;
    }
}

- (IBAction)clickEditVideo:(id)sender {
    
    if(bEditVideo)
    {
        //user has click on ok button
        
        switch (baseFilterType)
        {
            case GPUIMAGE_BRIGHTNESS:
            {
                brightnessVal = slider.value;
                
            }break;
                
            case GPUIMAGE_CONTRAST:
            {
                contrastVal = slider.value;
                
                
            }   break;
                
            case GPUIMAGE_SATURATION:
            {
                saturationVal = slider.value;
                
                
            }   break;
            default:
            {
                brightnessVal = slider.value;
                
            }break;
        }
        
        bEditVideo = false;
        
        slider.hidden = true;
        btnCrop.hidden = false;
        btnRotate.hidden = false;
        btnBrightness.hidden = false;
        lblContrast.hidden = false;
        lblBrightness.hidden = false;
        lblSaturation.hidden = false;
        collPresets.hidden = true;
        
        [btnFilter setImage:imgFilterBtm forState:UIControlStateNormal];
        [btnEdit setImage:imgEditBtmSelected forState:UIControlStateNormal];
        
    }
    else
    {
        self.navigationItem.rightBarButtonItem = nil;
        
        [btnFilter setImage:imgFilterBtm forState:UIControlStateNormal];
        [btnEdit setImage:imgEditBtmSelected forState:UIControlStateNormal];
        
        [btnCrop setImage:imgContrast forState:UIControlStateNormal];
        [btnRotate setImage:imgSaturation forState:UIControlStateNormal];
        [btnBrightness setImage:imgBrighness forState:UIControlStateNormal];
        
        slider.hidden = true;
        btnCrop.hidden = false;
        btnRotate.hidden = false;
        btnBrightness.hidden = false;
        lblContrast.hidden = false;
        lblBrightness.hidden = false;
        lblSaturation.hidden = false;
        collPresets.hidden = true;
    }
}

#pragma mark-  Set Contrast

- (IBAction)clickCrop:(id)sender {
    
    [btnCrop setImage:imgContrastSelected forState:UIControlStateNormal];
    [btnRotate setImage:imgSaturation forState:UIControlStateNormal];
    [btnBrightness setImage:imgBrighness forState:UIControlStateNormal];
    
    bEditVideo = true;
    
    [btnFilter setImage:imgCancelBtm forState:UIControlStateNormal];
    [btnEdit setImage:imgSaveBtm forState:UIControlStateNormal];
    
    slider.hidden = false;
    slider.minimumValue = 0.0;
    slider.maximumValue = 4.0;
    slider.value = contrastVal;
    
    baseFilterType = GPUIMAGE_CONTRAST;
    
    btnCrop.hidden = true;
    btnRotate.hidden = true;
    btnBrightness.hidden = true;
    lblContrast.hidden = true;
    lblBrightness.hidden = true;
    lblSaturation.hidden = true;
}

#pragma mark-  Set Brightness

- (IBAction)clickBrightness:(id)sender {
    
    [btnCrop setImage:imgContrast forState:UIControlStateNormal];
    [btnRotate setImage:imgSaturation forState:UIControlStateNormal];
    [btnBrightness setImage:imgBrighnessSelected forState:UIControlStateNormal];
    
    bEditVideo = true;
    
    [btnFilter setImage:imgCancelBtm forState:UIControlStateNormal];
    [btnEdit setImage:imgSaveBtm forState:UIControlStateNormal];
    
    slider.hidden = false;
    
    //// Brightness ranges from -1.0 to 1.0, with 0.0 as the normal level
    slider.minimumValue = -1.0;
    slider.maximumValue = 1.0;
    slider.value = brightnessVal;
    
    baseFilterType = GPUIMAGE_BRIGHTNESS;
    
    btnCrop.hidden = true;
    btnRotate.hidden = true;
    btnBrightness.hidden = true;
    lblContrast.hidden = true;
    lblBrightness.hidden = true;
    lblSaturation.hidden = true;
}

#pragma mark-  Set Saturation
- (IBAction)clickRotate:(id)sender {
    
    [btnCrop setImage:imgContrast forState:UIControlStateNormal];
    [btnRotate setImage:imgSaturationSelected forState:UIControlStateNormal];
    [btnBrightness setImage:imgBrighness forState:UIControlStateNormal];
    
    bEditVideo = true;
    
    [btnFilter setImage:imgCancelBtm forState:UIControlStateNormal];
    [btnEdit setImage:imgSaveBtm forState:UIControlStateNormal];
    
    slider.hidden = false;
    
    ///** Saturation ranges from 0.0 (fully desaturated) to 2.0 (max saturation), with 1.0 as the normal level
    slider.minimumValue = 0.0;
    slider.maximumValue = 2.0;
    slider.value = saturationVal;
    
    baseFilterType = GPUIMAGE_SATURATION;
    
    btnCrop.hidden = true;
    btnRotate.hidden = true;
    btnBrightness.hidden = true;
    lblContrast.hidden = true;
    lblBrightness.hidden = true;
    lblSaturation.hidden = true;
}

@end
