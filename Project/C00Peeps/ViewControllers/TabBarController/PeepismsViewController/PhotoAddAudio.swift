//
//  PhotoAddAudio.swift
//  PhotoVideoFilter
//
//  Created by Mac on 5/09/16.
//  Copyright © 2016 Gagandeep Bawa. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation

class CustomSlide: UISlider {
    
    @IBInspectable var trackHeight: CGFloat = 15
    
    override func trackRect(forBounds bounds: CGRect) -> CGRect {
        //set your bounds here
        return CGRect(origin: bounds.origin, size: CGSize(width: bounds.width, height: trackHeight))
    }
}

class PhotoAddAudio: BaseVC, AVAudioPlayerDelegate, AVAudioRecorderDelegate {

    @IBOutlet weak var photoView: UIImageView!
    var imagePhoto: UIImage!
    
    @IBOutlet weak var btnCaptureAudio: UIButton!
    @IBOutlet weak var btnOk: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var sliderRecording: CustomSlide!
    @IBOutlet weak var photoviewBottom: NSLayoutConstraint!
    @IBOutlet weak var sliderBaseline: NSLayoutConstraint!
    @IBOutlet weak var recordingBaseline: NSLayoutConstraint!
    
    
    var audioPlayer: AVAudioPlayer?
    var audioRecorder: AVAudioRecorder!
    var isRecordingDone:Bool?
    
    var timer:Timer?
    let maxAudioRecordingTime:Float = 60.0
    var currentRecordingTime:Float = 0.0
    var recordingDuration:Float = 0.0
    var dataPath:String?
    var isComingFromPeepTextFlow:Bool?
    var isComingFromEventsFlow:Bool?
    
    var delegate: CreateEventSecondControllerDelegate?
    
    let recordSettings = [AVSampleRateKey : NSNumber(value: Float(44100.0) as Float),
                          AVFormatIDKey : NSNumber(value: Int32(kAudioFormatMPEG4AAC) as Int32),
                          AVNumberOfChannelsKey : NSNumber(value: 1 as Int32),
                          AVEncoderAudioQualityKey : NSNumber(value: Int32(AVAudioQuality.min.rawValue) as Int32)]
    
    override func viewWillDisappear(_ animated: Bool) {
        
        super.viewWillDisappear(animated)
        
        stopAudioPlaying()
        stopAudioRecording()
        
        do{
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
        }
        catch{
            
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
         self.tabBarController?.tabBar.isHidden = true
        
        isRecordingDone = false
        
        sliderRecording.minimumTrackTintColor = Utilities().themedMultipleColor()[0]
        sliderRecording.maximumTrackTintColor = UIColor.gray
        sliderRecording.setThumbImage(nil, for: UIControlState())
        sliderRecording.maximumValue =  maxAudioRecordingTime
        sliderRecording.minimumValue =  currentRecordingTime
        
        sliderRecording.setThumbImage(UIImage(), for: UIControlState())
        
        lblTime.text =  NSString(format: "00:00/00:%d", Int(maxAudioRecordingTime)) as String
        
        dataPath = self.directoryURL()?.path
        
        let audioSession = AVAudioSession.sharedInstance()
        do {
            try audioSession.setCategory(AVAudioSessionCategoryPlayAndRecord)
            try audioRecorder = AVAudioRecorder(url: self.directoryURL()!,
                                                settings: recordSettings)
            audioRecorder!.prepareToRecord()
        } catch {
        }
    }
    
    func directoryURL() -> URL? {
        let fileManager = FileManager.default
        let urls = fileManager.urls(for: .documentDirectory, in: .userDomainMask)
        let documentDirectory = urls[0] as URL
        let soundURL = documentDirectory.appendingPathComponent("audio.m4a")
        return soundURL
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        updateViewAccordingToTheme()
        
        setConstraint()
        
        //Show Skip button when user is not coming from Peep Text flow
        if isComingFromPeepTextFlow == false
        {
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Skip", style: UIBarButtonItemStyle.plain, target: self, action: #selector(PhotoAddAudio.skip))
        }
        
        self.title = "Audio"

        
        photoView.image = imagePhoto

        
        
    }
    
    func checkMicrophoneAuthorization(){
        
        let authorizationStatus = AVCaptureDevice.authorizationStatus(for: AVMediaType.audio)
        switch authorizationStatus {
        case .notDetermined:
            // permission dialog not yet presented, request authorization
            AVCaptureDevice.requestAccess(for: AVMediaType.audio,
                                                      completionHandler: { (granted:Bool) -> Void in
                                                        if (granted == false) {
                                                            
                                                            //show alert
                                                            let alert = UIAlertController(title: "", message: alertMsg_AudioAccess, preferredStyle: UIAlertControllerStyle.alert)
                                                            alert.addAction(UIAlertAction(title: alertBtnTitle_OK, style: UIAlertActionStyle.default, handler:{ (UIAlertAction) -> Void in
                                                                
                                                                if let appSettings = URL(string: UIApplicationOpenSettingsURLString) {
                                                                    UIApplication.shared.openURL(appSettings)
                                                                }
                                                            }))
                                                            
                                                            self.present(alert, animated: true, completion: nil)
                                                        }
                                                        else
                                                        {
                                                            self.startAudioRecording()
                                                        }
            })
        case .authorized:
            // go ahead
            
            self.startAudioRecording()
            
            break
        case .denied, .restricted:
            
            print("user denied access of camera")
            
            //show alert
            let alert = UIAlertController(title: "", message: alertMsg_AudioAccess, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: alertBtnTitle_OK, style: UIAlertActionStyle.default, handler:{ (UIAlertAction) -> Void in
                
                if let appSettings = URL(string: UIApplicationOpenSettingsURLString) {
                    UIApplication.shared.openURL(appSettings)
                }
            }))
            
            self.present(alert, animated: true, completion: nil)
            
            break
            // the user explicitly denied camera usage or is not allowed to access the camera devices
        }
    }
    
     //MARK:- Skip Recording
    
    func skip(){
    
        stopAudioPlaying()
        stopAudioRecording()
        
        do{
            
            let path = dataPath!
            try FileManager.default.removeItem(atPath: path)
            
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
            
            let st = UIStoryboard(name: "Digital", bundle:nil)
            let obj = st.instantiateViewController(withIdentifier: "PhotoFilterEdit") as! PhotoFilterEdit
            obj.imagePhoto = imagePhoto
            obj.dataPath = dataPath!
            obj.isComingFromPeepTextFlow = isComingFromPeepTextFlow
            obj.isComingFromEventsFlow = isComingFromEventsFlow
            obj.delegate = self.delegate
            self.navigationController?.pushViewController(obj, animated: true)
        }
        catch{
            
        }
    }
    
    // MARK: - Update View According To Selected Theme
    
    func updateViewAccordingToTheme() {
        
        btnCaptureAudio.setImage(Utilities().themedImage(img_audio), for: UIControlState())
        btnCaptureAudio.setImage(Utilities().themedImage(img_audioStop), for: .highlighted)
        btnCaptureAudio.setImage(Utilities().themedImage(img_audioStop), for: .selected)
        
        btnOk.setImage(Utilities().themedImage(img_saveBtm), for: UIControlState())
        btnOk.setImage(Utilities().themedImage(img_saveBtmSelected), for: .highlighted)
        btnOk.setImage(Utilities().themedImage(img_saveBtmSelected), for: .selected)
        
        btnCancel.setImage(Utilities().themedImage(img_cancelBtm), for: UIControlState())
        btnCancel.setImage(Utilities().themedImage(img_cancelBtmSelected), for: .highlighted)
        btnCancel.setImage(Utilities().themedImage(img_cancelBtmSelected), for: .selected)
    }

    //MARK:- Move Next
    
    @IBAction func moveToNextScreen(_ sender: AnyObject) {
        
        if isRecordingDone == true
        {
            stopAudioPlaying()
            stopAudioRecording()
            
            do{
                try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
                
                //This condition will work for event & digital tab
                if isComingFromPeepTextFlow == false
                {
                    let st = UIStoryboard(name: "Digital", bundle:nil)
                    let obj = st.instantiateViewController(withIdentifier: "PhotoFilterEdit") as! PhotoFilterEdit
                    obj.imagePhoto = imagePhoto
                    obj.dataPath = dataPath!
                    obj.isComingFromPeepTextFlow = isComingFromPeepTextFlow
                    mediaAttachment = MediaAttachment.audio
                    obj.isComingFromEventsFlow = isComingFromEventsFlow
                    obj.delegate = self.delegate
                    self.navigationController?.pushViewController(obj, animated: true)
                }
                else //This condition will work for peep text flow
                {
                    let st = UIStoryboard(name: "Digital", bundle:nil)
                    let obj = st.instantiateViewController(withIdentifier: "PublishPhotoAudio") as! PublishPhotoAudio
                    obj.imagePhoto = photoView.image
                    obj.dataPath = dataPath;
                    obj.isComingFromPeepTextFlow = isComingFromPeepTextFlow
                    
                    self.navigationController?.hidesBottomBarWhenPushed = false
                    self.navigationController?.pushViewController(obj, animated: true)
                }
            }
            catch{
                
            }
        }
        else
        {
            let alert = UIAlertController(title: "", message: alertMsg_RecordAudio, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: alertBtnTitle_OK, style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
        }
    }
    
    //MARK:- Redo Recording
    
    @IBAction func redoRecording(_ sender: AnyObject) {
        
        stopAudioPlaying()
        stopAudioRecording()
        
        isRecordingDone = false
        
        sliderRecording.maximumValue =  maxAudioRecordingTime
        sliderRecording.minimumValue =  0.0
        
        btnCaptureAudio.setImage(Utilities().themedImage(img_audio), for: UIControlState())
    }
    
    //MARK:- Set Constraints
    
    func setConstraint(){
        
        let bb = UIScreen.main.bounds.width - UIScreen.main.bounds.height/2
        
        var aa = (UIScreen.main.bounds.height-40 - UIScreen.main.bounds.width)/2
        aa = aa + bb
        
        let constraints = self.view.constraints
        let count = constraints.count
        var index = 0;
        
        let ph = UIScreen.main.bounds.height-40 - UIScreen.main.bounds.width - 64
        
        photoviewBottom.constant = ph
        sliderBaseline.constant = ph - 15
        recordingBaseline.constant = ph + 20
        
        while (index < count)
        {
            let constraint = constraints[index]
            
            if constraint.identifier == "captureAudioPosition" {
        
                let height = btnCaptureAudio.frame.size.width
                
                constraint.constant = aa + height/2
            }
            
            index += 1;
        }
        
    }
    
    //MARK:- Record Audio
    
    @IBAction func captureAudio(_ sender: AnyObject) {
        
        if isRecordingDone == true
        {
            if audioPlayer?.isPlaying == true
            {
                stopAudioPlaying()
            }
            else
            {
                do{
                    
                    try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
                    
                     btnCaptureAudio.setImage(Utilities().themedImage(img_audioStop), for: UIControlState())
                    
                    audioPlayer = try AVAudioPlayer(contentsOf: (audioRecorder?.url)!)
                    
                    audioPlayer?.delegate = self
                    
                    audioPlayer?.play()
                    
                    sliderRecording.maximumValue =  recordingDuration
                    sliderRecording.minimumValue =  currentRecordingTime
                    sliderRecording.value = currentRecordingTime
                    
                    var strCurrent = String()
                    if recordingDuration < 10
                    {
                        strCurrent = NSString(format: "00:00/00:0%d", Int(recordingDuration)) as String
                    }
                    else
                    {
                        strCurrent = NSString(format: "00:00/00:%d", Int(recordingDuration)) as String
                    }

                    
                    lblTime.text = strCurrent
                    
                    timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(PhotoAddAudio.playingProgress), userInfo: nil, repeats: true)
                    
                }
                catch
                {
                    
                }
            }
        }
        else
        {
            //Start Audio Recording
            if !audioRecorder.isRecording{
                
                checkMicrophoneAuthorization()

            }
            else
            {
                //Stop Audio Recording, if recording start
                stopAudioRecording()
            }
        }
       
    }
    
    func startAudioRecording(){
        
        do{
            
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryRecord)
            
            audioRecorder?.record()
            
            btnCaptureAudio.setImage(Utilities().themedImage(img_audioStop), for: UIControlState())
            
            sliderRecording.value = currentRecordingTime
            
            lblTime.text =  NSString(format: "00:00/00:%d", Int(maxAudioRecordingTime)) as String
            
            timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(PhotoAddAudio.recordingProgress), userInfo: nil, repeats: true)
        }
        catch
        {
            
        }
    }
    
    //MARK:- Stop Record
    
    func stopAudioRecording(){
        
        isRecordingDone = true
        
        timer?.invalidate()
        
        audioRecorder?.stop()
        
        btnCaptureAudio.setImage(Utilities().themedImage(img_videoPlay), for: UIControlState())
        
        sliderRecording.value = currentRecordingTime
        
        recordingDuration = currentRecordingTime
        
        var strCurrent = String()
        
        let current = Int(currentRecordingTime)
        
        if current < 10
        {
            strCurrent = NSString(format: "00:0%d/00:%d", current, Int(maxAudioRecordingTime)) as String
        }
        else
        {
            strCurrent = NSString(format: "00:%d/00:%d", current, Int(maxAudioRecordingTime)) as String
        }
        
        lblTime.text = strCurrent
        
        currentRecordingTime = 0.0
    }
    
    //MARK:- Stop Playing
    
    func stopAudioPlaying(){
        
        
        btnCaptureAudio.setImage(Utilities().themedImage(img_videoPlay), for: UIControlState())
        
        timer?.invalidate()
        
        audioPlayer?.stop()
        
        sliderRecording.value = currentRecordingTime
        
        var strCurrent = String()
        let current = Int(currentRecordingTime)
        
        //if current < Int(maxAudioRecordingTime)
            
        if current < 10
        {
            if recordingDuration < 10
            {
                strCurrent = NSString(format: "00:0%d/00:0%d", current, Int(recordingDuration)) as String
            }
            else
            {
                strCurrent = NSString(format: "00:0%d/00:%d", current, Int(recordingDuration)) as String
            }
            
        }
        else
        {
            if recordingDuration < 10
            {
                strCurrent = NSString(format: "00:%d/00:0%d", current, Int(recordingDuration)) as String
            }
            else
            {
                strCurrent = NSString(format: "00:%d/00:%d", current, Int(recordingDuration)) as String
            }
        }
        
        lblTime.text = strCurrent
        
        currentRecordingTime = 0.0
    }
    
    //MARK:- Show playing progress
    
    func playingProgress(){
        
        currentRecordingTime = currentRecordingTime + 1
        
        if(currentRecordingTime == recordingDuration)
        {
            stopAudioPlaying()
        }
        else
        {
            var strCurrent = String()
            let current = Int(currentRecordingTime)
            
            if current < 10
            {
                if recordingDuration < 10
                {
                    strCurrent = NSString(format: "00:0%d/00:0%d", current, Int(recordingDuration)) as String
                }
                else
                {
                    strCurrent = NSString(format: "00:0%d/00:%d", current, Int(recordingDuration)) as String
                }
                
            }
            else
            {
                if recordingDuration < 10
                {
                    strCurrent = NSString(format: "00:%d/00:0%d", current, Int(recordingDuration)) as String
                }
                else
                {
                    strCurrent = NSString(format: "00:%d/00:%d", current, Int(recordingDuration)) as String
                }
                
            }
            
            lblTime.text = strCurrent
            
            sliderRecording.value = currentRecordingTime
        }
    }
    
    //MARK:- Show recording progress
    
    func recordingProgress(){
        
        currentRecordingTime = currentRecordingTime + 1
        
        if(currentRecordingTime == maxAudioRecordingTime)
        {
            stopAudioRecording()
        }
        else
        {
           
            
            var strCurrent = String()
            let current = Int(currentRecordingTime)
            
            if current < 10
            {
                strCurrent = NSString(format: "00:0%d/00:%d", current, Int(maxAudioRecordingTime)) as String
            }
            else
            {
                strCurrent = NSString(format: "00:%d/00:%d", current, Int(maxAudioRecordingTime)) as String
            }
            
            lblTime.text = strCurrent
            
            sliderRecording.value = currentRecordingTime
        }
    }


    //MARK:- Audio player delegate method
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool)
    {
         stopAudioPlaying()
    }
    

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.

    }
}
