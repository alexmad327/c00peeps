//
//  ContactsScreen.swift
//  PhotoVideoFilter
//
//  Created by OSX on 09/09/16.
//  Copyright © 2016 Gagandeep Bawa. All rights reserved.
//

import Foundation
import UIKit
import AWSS3
import AWSCore
import SwiftyJSON
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}

var isComingFromChatFlow = false

protocol LockUnlockProtocol {
    // protocol definition goes here
    
    func startSendingData()
    func cancelPopup()
}

@objc class ContactsScreen: BaseVC, LockUnlockProtocol, UISearchBarDelegate{
    
    @objc var imageData:Data?
    @objc var dataPath:String?
    @objc var isComingFromPeepTextFlow = false
    @objc var mediacaption:String?

    //Event
    var event:Event?
    @objc var isComingFromEventsFlow = false
    var isComingFromCreateSubmerchantFlow = false
    var isComingFromForwardMedia = false
    var fwdMediaId = 0
    var filterApplied = false;
    
    //Setting this only when coming from VideoPublish screen
    @objc var mediaCategoryId = -1
    
    var arrContacts = [Contact]()
    var arrFilterContacts = [Contact]()
    
    var isAllContactSelected:Bool?
    var isContactSearching:Bool?
    var isPublishedAllFollowers = false
    
    var statusLabel: UILabel?
    var bIsPopupShown:Bool?
    
    var mediaFile:String?
    var mediaFileAttachment:String?
    var bUploadingAttachment:Bool?
    var contactIds:String?
    var selectedContactPublicKey:String?
    var uploadingType:String?
    
    var videoPreparingDelay = 0
    var offset = 0
    var page = 1
    var pageSize = 10
    var totalRecordCount = 0
    var sendMediaLocked = 0
    var refreshControl: UIRefreshControl!
    
    @IBOutlet weak var tableWithBottomView: NSLayoutConstraint!
    @IBOutlet weak var tableWithSelectAllButton: NSLayoutConstraint!
    @IBOutlet weak var searchbarTopWithMainView: NSLayoutConstraint!
    @IBOutlet weak var searchbarTopWithAllFollowings: NSLayoutConstraint!
    @IBOutlet weak var tableTopWithSearchBar: NSLayoutConstraint!
    @IBOutlet weak var tableTopWithAllFollowers: NSLayoutConstraint!
    
    @IBOutlet weak var lblNoContact: UILabel!
    @IBOutlet weak var btnRefresh: UIButton!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tblContacts: UITableView!
    @IBOutlet weak var btnAllFollowings: UIButton!
    @IBOutlet weak var btnAllFollowers: UIButton!
    @IBOutlet weak var btnSelectAll: UIButton!
    
    let customerView  = LockUnlockView.init(frame: CGRect(x: (UIScreen.main.bounds.width - 250.0)/2, y: (UIScreen.main.bounds.height-300)/2, width: 250, height: 300))
    
    var backgroundView : UIView = UIView(frame: CGRect(x: 0,y: 0, width: UIScreen.main.bounds.width,height: UIScreen.main.bounds.height))
    
    let cancelBtn = UIButton.init(frame: CGRect(x: (UIScreen.main.bounds.width - 250.0)/2+250-25,y: (UIScreen.main.bounds.height - 300.0)/2-25,width: 50,height: 50))
    
    var completionHandler: AWSS3TransferUtilityUploadCompletionHandlerBlock?
    var progressBlock: AWSS3TransferUtilityProgressBlock?
    
    
    
    //MARK:- viewWillAppear
    
    override func viewWillAppear(_ animated: Bool) {
        isGalleryMediaSent = false

        super.viewWillAppear(animated)
        
        self.tabBarController?.tabBar.isHidden = true
        
    }

    //MARK:- viewDidLoad
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        bIsPopupShown = false
        
        contactIds = ""
        
        //If coming from video publish screen
        if mediaCategoryId != -1
        {
            mediaAttachment = MediaAttachment.video
        }
        addRefreshControl()
        
        if  isComingFromChatFlow == true{
            arrContacts = [sendPeepTextToContact]
            printCustom("contactid:\(arrContacts.first?.pId)")
            printCustom("contactname:\(arrContacts.first?.pUserName)")
            //arrContacts = Utilities().getPeepTextSentUser()
            lblNoContact.isHidden = true
            tblContacts.isHidden = false
            //load table
            tblContacts.reloadData()
        }
        else{
            // Show Loader.
            loadingIndicator.startAnimating()
            
            //Fetch All Contacts
            getContacts()
        }
        isAllContactSelected = false
        isContactSearching = false
        bUploadingAttachment = false
        
        self.navigationItem.title = "Contacts"
        
        let userDetail:UserDetail = Utilities.getUserDetails()
    
        //Show only All following users, If user is sending peep text or he is private user (2) or anonymous user (4)
        if isComingFromPeepTextFlow == true || isComingFromEventsFlow == true || isComingFromForwardMedia == true || userDetail.privacy_level_id == "2" || userDetail.privacy_level_id == "4"
        {
            isPublishedAllFollowers = false
            updateConstraint()
        }
        
        let leftBtn = UIBarButtonItem(image: Utilities().themedImage(img_backArrow), style: UIBarButtonItemStyle.plain, target: self, action: #selector(ContactsScreen.back))
        self.navigationItem.leftBarButtonItem = leftBtn
            
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Send", style: UIBarButtonItemStyle.plain, target: self, action: #selector(ContactsScreen.done))
        
        statusLabel = UILabel(frame: CGRect(x: 60, y: 2, width: UIScreen.main.bounds.width-120, height: 40))
        statusLabel?.textColor = UIColor.white
        statusLabel?.textAlignment = NSTextAlignment.center
        statusLabel?.font = ProximaNovaRegular(17)
        self.navigationController?.navigationBar.addSubview(statusLabel!)
        
        setImagesAccordingToTheme(1)
    }
    
    func addRefreshControl(){
        
        refreshControl = UIRefreshControl()
        refreshControl.tintColor = UIColor.darkGray
        refreshControl.attributedTitle = NSAttributedString(string: loaderTitle_PullToRefresh)
        refreshControl.addTarget(self, action: #selector(ContactsScreen.handleRefresh(_:)), for: UIControlEvents.valueChanged)
        tblContacts.addSubview(refreshControl)
    }
    
    func handleRefresh(_ refreshControl: UIRefreshControl) {
        
        page = 1
        arrContacts.removeAll()
        offset = 0
        
        if isPublishedAllFollowers == false
        {
            getContacts()
        }
        else
        {
            getFollowers()
        }
    }
    
    func updateConstraint(){
        
        btnAllFollowings.isHidden = true
        btnAllFollowers.isHidden = true
        searchbarTopWithAllFollowings.priority = UILayoutPriority(rawValue: 900.0)
        searchbarTopWithMainView.priority = UILayoutPriority(rawValue: 1000.0)
    }
    
    //MARK:- viewWillDisAppear
    
    override func viewWillDisappear(_ animated: Bool) {
        
        super.viewWillDisappear(animated)
        
        self.statusLabel?.removeFromSuperview()
        self.navigationItem.title = "Contacts"
        
    }
    
    //MARK:- Set images according to selected theme
    
    func setImagesAccordingToTheme(_ selectedThemeId:Int){
        
        tblContacts.isHidden = false
        searchBar.isHidden = false
        btnSelectAll.isHidden = false
        
         btnAllFollowings.setTitleColor(UIColor.white, for: UIControlState())
        btnAllFollowers.setTitleColor(UIColor.black, for: UIControlState())
        
        btnAllFollowings.setBackgroundImage(Utilities().themedImage(img_contactbtnSelected), for: UIControlState())
        btnAllFollowers.setBackgroundImage(Utilities().themedImage(img_contactbtn), for: UIControlState())
        btnSelectAll.setBackgroundImage(Utilities().themedImage(img_selectAllBtn), for: UIControlState())
      
    }
    
    // MARK: - Button Actions
    @IBAction func btnRefreshClicked(_ sender: AnyObject) {
        
        self.tblContacts.isHidden = true
        self.lblNoContact.isHidden = true
        self.btnRefresh.isHidden = true
        self.loadingIndicator.startAnimating()
        
        if isPublishedAllFollowers == false
        {
            getContacts()
        }
        else
        {
            getFollowers()
        }
    }
    
    // MARK: - Done
    
    func done(){
        
        searchBar.resignFirstResponder()
        
        if isComingFromForwardMedia  // If coming to forward any existing media
        {
            //configure selected contact ids
            contactIds = ""//empty previous ids before setting new ids.
            setupSelectedContactIds()
            
            if contactIds?.isEmpty == true
            {
                let ac = UIAlertController(title: "", message: alertMsg_selectContact, preferredStyle: .alert)
                ac.addAction(UIAlertAction(title: alertBtnTitle_OK, style: .default, handler: nil))
                self.present(ac, animated: true, completion: nil)
            }
            else
            {
                self.navigationItem.title = ""
                forwardMedia(fwdMediaId)
            }
        }
        else if isComingFromPeepTextFlow // If coming from peep text section.
        {
             //If there is no attachment in peep text then directly call API
            if mediaAttachment == MediaAttachment.none
            {
                //configure selected contact ids
                contactIds = ""//empty previous ids before setting new ids.
                setupSelectedContactIds()
                
                if contactIds?.isEmpty == true
                {
                    let ac = UIAlertController(title: "", message: alertMsg_selectContact, preferredStyle: .alert)
                    ac.addAction(UIAlertAction(title: alertBtnTitle_OK, style: .default, handler: nil))
                    self.present(ac, animated: true, completion: nil)
                }
                else
                {
                    
                    self.navigationItem.title = ""
                    
                    sendMediaLocked = 0

                    sendPeepText()
                }
            }
            else
            {
                showLockUnlockPopup()
            }
        }
        else if isComingFromEventsFlow {// If coming from events section.
            contactIds = ""//empty previous ids before setting new ids.
            setupSelectedContactIds()
            
            if contactIds?.isEmpty == true
            {
                let ac = UIAlertController(title: "", message: alertMsg_selectContact, preferredStyle: .alert)
                ac.addAction(UIAlertAction(title: alertBtnTitle_OK, style: .default, handler: nil))
                self.present(ac, animated: true, completion: nil)
            }
            else
            {
                if (mediaAttachment == MediaAttachment.none) ||  (mediaAttachment != MediaAttachment.none && !isEditingEventMedia && isEditingEvent)
                {
                    // If no media, then directly send event.
                    if isEditingEvent && !isEditingEventMedia
                    {
                        sendMediaLocked = event!.isMediaLocked
                    }
                    
                    sendEvent()
                }
                else
                {
                    // If media exists and event is private/public, then show lock/unlock popup.
                    showLockUnlockPopup()
                }
            }
        }
        //Don't show lock/unlock popup, if sending to All Followers
        else if isPublishedAllFollowers == true
        {
            sendMediaLocked = 0
            
            backgroundView.backgroundColor = UIColor(red: 0.5, green: 0.5, blue: 0.5, alpha: 1.0)
            backgroundView.alpha = 0.5
            self.view.addSubview(backgroundView)
            
            displayUploadProgress()
            
            startUploading()
            
        }
        //Show lock/unlock popup, if sending to All Followings
        else
        {
            showLockUnlockPopup()
        }
    }
    
    
    // MARK: - Show Lock/Unlock UI
    
    func showLockUnlockPopup(){
        
        if bIsPopupShown == true
        {
            
        }
        else
        {
            if isComingFromEventsFlow
            {
                customerView.refreshDefaultLockUnlockViewForEventFlow(event!)
            }
            else
            {
                customerView.refreshDefaultLockUnlockView()
            }
            
            customerView.delegate = self
            bIsPopupShown = true
            backgroundView.backgroundColor = UIColor(red: 0.5, green: 0.5, blue: 0.5, alpha: 1.0)
            backgroundView.alpha=0.5
            self.view.addSubview(backgroundView)
            self.view.addSubview(customerView)
            cancelBtn.setImage(Utilities().themedImage(img_cancelIcon), for: UIControlState())
            cancelBtn.addTarget(self, action: #selector(cancelPopup), for: .touchUpInside)
            self.view.addSubview(cancelBtn)
        }
    }
    
    // MARK: - Selected contact Id
    
    func setupSelectedContactIds(){
        printCustom("setupSelectedContactIds arrContacts:\(arrContacts)")

        if isContactSearching == false
        {
            if arrContacts.count > 0 {
                for i in 0...arrContacts.count-1
                {
                    let contact = arrContacts[i]
                    
                    if contact.pSelected == true
                    {
                        selectedContactPublicKey = contact.pPublicKey
                        contactIds = (contactIds)! + (String.init(format: "%d", contact.pId) + ",")
                    }
                }
            }
        }
        else
        {
            if arrFilterContacts.count >  0 {
                
                for i in 0...arrFilterContacts.count-1
                {
                    let contact = arrFilterContacts[i]
                    
                    if contact.pSelected == true
                    {
                        contactIds = (contactIds)! + (String.init(format: "%d", contact.pId) + ",")
                    }
                }
            }
            
        }
        
        
        contactIds = contactIds?.trimmingCharacters(in: CharacterSet.init(charactersIn: ","))
    }
    
    // MARK: - LockUnlock Protocol methods
    
    func startSendingData()
    {
        self.navigationItem.title = ""
        //If sending to following users, this will work in case of Peep Text & Event flow, as well as for digital media
        if isPublishedAllFollowers == false
        {
            //configure selected contact ids
            contactIds = ""//empty previous ids before setting new ids.
            setupSelectedContactIds()
            
            if contactIds?.isEmpty == true
            {
                let ac = UIAlertController(title: "", message: alertMsg_selectContact, preferredStyle: .alert)
                ac.addAction(UIAlertAction(title: alertBtnTitle_OK, style: .default, handler: nil))
                self.present(ac, animated: true, completion: nil)
            }
            else
            {
                if customerView.isLocked == false{
                    
                    sendMediaLocked = 0
                }
                else
                {
                    sendMediaLocked = 1
                }
                
                //If media file already exists in case of editing event, but event media not edited, so media file & media attachment file can be directly sent without uplaoding again.
                if mediaFile != nil && isComingFromEventsFlow && isEditingEvent {
                    
                    //hide lock/unlock popup
                    hidelockUnlockPopup()
                    sendEvent()
                }
                else
                {
                    displayUploadProgress()
                    
                    // If there is no media file uploaded then upload media files
                    if mediaFile?.isEmpty == true || mediaFile == nil {
                        startUploading()
                    }
                    // If media file already uploaded then call send media API
                    else
                    {
                        self.bUploadingAttachment = true
                        self.uploadFinished()
                    }
                }
            }
        }
        else //If sending to follower users
        {
            sendMediaLocked = 0
            
            displayUploadProgress()
            
            if mediaFile?.isEmpty == true || mediaFile == nil {
                startUploading()
            }
            else {
                self.bUploadingAttachment = true
                self.uploadFinished()
            }
        }
    }
    
    func hidelockUnlockPopup(){
        
        DispatchQueue.main.async(execute: {
            
            self.cancelBtn.removeFromSuperview()
            
            self.bIsPopupShown = false
            
            self.customerView.removeFromSuperview()
            
            self.navigationItem.hidesBackButton = true
            
            self.navigationItem.leftBarButtonItem = nil
            
            self.navigationItem.rightBarButtonItem = nil
        })
    }
    
    func cancelPopup(){
        
        bIsPopupShown = false
        
        cancelBtn.removeFromSuperview()
        backgroundView.removeFromSuperview()
        customerView.removeFromSuperview()
    }
    
    // MARK: - Back
    
    func back(){
        
        if bIsPopupShown == false
        {
            if self.isComingFromCreateSubmerchantFlow && Utilities.getUserDetails().submerchantExists == "1" {
                for viewController in self.navigationController!.viewControllers {
                    if viewController.isKind(of: EventDetailsVc.self) {
                        // Move back to event details to edit event, but not to "create submerchant screen" as submerchant is created before coming to contacts screen.
                        self.navigationController?.popToViewController(viewController, animated: true)
                        break
                    }
                }
            }
            else {
                self.navigationController?.popViewController(animated: true)
            }
        }
        
    }
    
    
    // MARK: - Select All Followers
    @IBAction func clickAllFollowers(_ sender: AnyObject) {
        
        btnSelectAll.isHidden = true
        
        tableTopWithSearchBar.priority = UILayoutPriority(rawValue: 900.0)
        tableTopWithAllFollowers.priority = UILayoutPriority(rawValue: 999.0)
        
        tableWithSelectAllButton.priority = UILayoutPriority(rawValue: 900.0)
        tableWithBottomView.priority = UILayoutPriority(rawValue: 999.0)
        
        isPublishedAllFollowers = true
        
        btnAllFollowings.setBackgroundImage(Utilities().themedImage(img_contactbtn), for: UIControlState())
        btnAllFollowings.setTitleColor(UIColor.black, for: UIControlState())
        
        btnAllFollowers.setBackgroundImage(Utilities().themedImage(img_contactbtnSelected), for: UIControlState())
        btnAllFollowers.setTitleColor(UIColor.white, for: UIControlState())
        
        tblContacts.isHidden = true
        searchBar.isHidden = true
        
        
        offset = 0
        page = 1
        
        //get followers from API
        loadingIndicator.startAnimating()
        getFollowers()
    }
    
    // MARK: - Select All Followings
    @IBAction func clickAllFollowings(_ sender: AnyObject) {
        
        tableTopWithSearchBar.priority = UILayoutPriority(rawValue: 999.0)
        tableTopWithAllFollowers.priority = UILayoutPriority(rawValue: 900.0)
        
        tableWithSelectAllButton.priority = UILayoutPriority(rawValue: 999.0)
        tableWithBottomView.priority = UILayoutPriority(rawValue: 900.0)
        
        isPublishedAllFollowers = false
        
        btnAllFollowings.setBackgroundImage(Utilities().themedImage(img_contactbtnSelected), for: UIControlState())
        btnAllFollowings.setTitleColor(UIColor.white, for: UIControlState())
        
        btnAllFollowers.setBackgroundImage(Utilities().themedImage(img_contactbtn), for: UIControlState())
        btnAllFollowers.setTitleColor(UIColor.black, for: UIControlState())
        
        tblContacts.isHidden = true
        searchBar.isHidden = false
        btnSelectAll.isHidden = false
        
        offset = 0
        page = 1
        
        //get followings from API
        loadingIndicator.startAnimating()
        getContacts()
    }
    
    // MARK: - Select All
    @IBAction func clickSelectAll(_ sender: AnyObject) {
        
        if self.isContactSearching == false
        {
            if self.isAllContactSelected == true && arrContacts.count > 0
            {
                for i in 0...self.arrContacts.count-1
                {
                    self.arrContacts[i].pSelected = false
                }
                
                self.isAllContactSelected = false
                
                self.btnSelectAll.setTitle("Select All", for: UIControlState())
                
                self.tblContacts.reloadData()
            }
            else
            {
                if arrContacts.count > 0
                {
                    for i in 0...self.arrContacts.count-1
                    {
                        self.arrContacts[i].pSelected = true
                        
                    }
                    
                    self.isAllContactSelected = true
                    
                    self.btnSelectAll.setTitle("Unselect All", for: UIControlState())
                    
                    self.tblContacts.reloadData()
                }
            }
        }
        else
        {
            if self.isAllContactSelected == true
            {
                for i in 0...self.arrFilterContacts.count-1
                {
                    self.arrFilterContacts[i].pSelected = false
                }
                
                self.isAllContactSelected = false
                
                self.btnSelectAll.setTitle("Select All", for: UIControlState())
                
                self.tblContacts.reloadData()
            }
            else
            {
                for i in 0...self.arrFilterContacts.count-1
                {
                    self.arrFilterContacts[i].pSelected = true
                    
                }
                
                self.isAllContactSelected = true
                
                self.btnSelectAll.setTitle("Unselect All", for: UIControlState())
                
                self.tblContacts.reloadData()
                
            }
        }
    }
    
    
    // MARK: -  Actions methods
    
    @IBAction func btnContactSelect(_ sender: AnyObject){
        
        //Refreshing tble cell
        let buttonPosition = sender.convert(CGPoint.zero, to: tblContacts)
        let indexPath = tblContacts.indexPathForRow(at: buttonPosition)! as IndexPath
        
        selectContact(indexPath)
    }
    
    // MARK: - Table view data source
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
         if isContactSearching == false
         {
            return arrContacts.count
         }
        
        else
         {
            return arrFilterContacts.count
         }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell
    {
        var arrTemp = [Contact]()
        
        //Load More
        if isContactSearching == false
        {
            arrTemp = arrContacts
        }
        else
        {
            arrTemp = arrFilterContacts
        }
        
        if (arrTemp.count >= Limit) && (indexPath.row + 1  == arrTemp.count) {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "LoadMoreCell")
            
            if let lbl5176 =  cell?.viewWithTag(5176) as? UILabel
            {
                lbl5176.text = loading_Title
                
                if (totalRecordCount) > (page) * Limit {
                    
                    offset = (page) * Limit
                    page += 1
                    
                    if isPublishedAllFollowers == false
                    {
                        getContacts()
                    }
                    else
                    {
                        getFollowers()
                    }
                }
                else
                {
                    lbl5176.text = loadingNoMoreData
                    lbl5176.isHidden = true
                    return refreshCellForRow(indexPath)!
                }
            }
            
            return cell!
        }

        else{
            return refreshCellForRow(indexPath)!
        }
    }
    
    func refreshCellForRow (_ indexPath:IndexPath)->UITableViewCell?
    {
        let cell = self.tblContacts.dequeueReusableCell(withIdentifier: "Cell")
        var contact = Contact()
        if isContactSearching == false
        {
            if indexPath.row < arrContacts.count
            {
                contact = arrContacts[indexPath.row]
            }
        }
        else
        {
            if indexPath.row < arrFilterContacts.count
            {
                contact = arrFilterContacts[indexPath.row]
            }
        }
        
        let lblUserName =  cell!.viewWithTag(1) as! UILabel
        
        //if user is anonymous
        if contact.pPrivacyLevelId == 4
        {
            lblUserName.text = anonymousUsername + String(contact.pId)
        }
        else
        {
            lblUserName.text = contact.pUserName
        }
        
        lblUserName.font = ProximaNovaRegular(15)
        
        let  btnSelect = cell!.viewWithTag(2) as! UIButton
        if contact.pSelected == true
        {
            btnSelect.setImage(Utilities().themedImage(img_radioSelected), for: UIControlState())
        }
        else
        {
            btnSelect.setImage(Utilities().themedImage(img_radio), for: UIControlState())
        }
         return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAtIndexPath indexPath: IndexPath)
    {
        selectContact(indexPath)
    }
    
    func selectContact(_ indexPath: IndexPath){
        
        if isPublishedAllFollowers == false
        {
            let cell = self.tblContacts.cellForRow(at: indexPath)
            
            let  btnSelect = cell!.viewWithTag(2) as! UIButton
            
            let contact:Contact?
            
            if isContactSearching == false
            {
                contact = arrContacts[indexPath.row]
                
                if contact?.pSelected == true
                {
                    btnSelect.setImage(Utilities().themedImage(img_radio), for: UIControlState())
                    contact?.pSelected = false
                }
                else
                {
                    btnSelect.setImage(Utilities().themedImage(img_radioSelected), for: UIControlState())
                    contact?.pSelected = true
                }
            }
            else
            {
                contact = arrFilterContacts[indexPath.row]
                
                if contact?.pSelected == true
                {
                    btnSelect.setImage(Utilities().themedImage(img_radio), for: UIControlState())
                    contact?.pSelected = false
                }
                else
                {
                    btnSelect.setImage(Utilities().themedImage(img_radioSelected), for: UIControlState())
                    contact?.pSelected = true
                }
            }
        }
    }
    
    //MARK:- Searchbar delegate methods
    
    // called when cancel button is clicked
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
        APIManager.sharedInstance.cancelAllRequests()
        
        searchBar.text = ""
        searchBar.resignFirstResponder()
        
        arrFilterContacts.removeAll()
        
        isAllContactSelected = false
        btnSelectAll.setTitle("Select All", for: UIControlState())
        
        isContactSearching = false
        tblContacts.reloadData()
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar){
        
        isContactSearching = true
        
        if arrContacts.count > 0 {
            for i in 0...arrContacts.count-1
            {
                let contact = arrContacts[i]
                contact.pSelected = false
            }
        }
        contactIds = ""
        
        tblContacts.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar){
        
        if searchBar.text?.characters.count > 0
        {
            arrFilterContacts.removeAll()
            
            page = 1
            offset = 0
            
            //Calling Search Contacts API
            searchContacts(searchBar.text!)
        }
        else
        {
            self.showCommonAlert(alertMsg_Comment)
        }
        
    }
    
    //MARK:- Add uploading label on navigation header
    
    func displayUploadProgress(){
        
        self.progressBlock = {(task, progress) in
            DispatchQueue.main.async(execute: {
                
                let val =  Float(progress.fractionCompleted)
                let percent = Int(val*100.0)
                self.statusLabel?.text = String.init(format: "Uploading...%d", percent)
                self.statusLabel?.text = (self.statusLabel?.text)! + "%" + " (" + self.uploadingType! + ")"

            })
        }
        
        self.completionHandler = { (task, error) -> Void in
            DispatchQueue.main.async(execute: {
                
                if ((error) != nil){
                    
                    //NSLog("Error: %@",error!);
                    
                    self.statusLabel!.text = "Uploading Failed"
                    
                    let alert = UIAlertController(title: "Error", message: "Uploading Failed", preferredStyle: UIAlertControllerStyle.alert)
                    
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (UIAlertAction) -> Void in
                        
                        self.clearUploadingState()
                    }))
                    
                    self.present(alert, animated: true, completion: nil)
                }
                else
                {
                    self.uploadFinished()
                }
            })
        }
        
    }
    
    func uploadFinished() {
        NSLog("Upload Done!")
        if self.isComingFromEventsFlow == true || self.isComingFromPeepTextFlow == true
        {
            
            if mediaAttachment == MediaAttachment.video || mediaAttachment == MediaAttachment.audio
            {
                if self.bUploadingAttachment == false
                {
                    self.bUploadingAttachment = true
                    self.startUploading()
                }
                else
                {
                    self.statusLabel!.text = "Uploading...100%"
                    
                    self.bUploadingAttachment = false
                    
                    if self.isComingFromEventsFlow == true
                    {
                        // Call Create Event API
                        self.sendEvent()
                    }
                    else if self.isComingFromPeepTextFlow == true
                    {
                        
                        //Call Upload Media API
                        self.sendPeepText()
                    }
                }
            }
            else //This will work for only PHOTO UPLOAD
            {
                self.statusLabel!.text = "Uploading...100% (Photo)"
                
                self.bUploadingAttachment = false
                
                if self.isComingFromEventsFlow == true
                {
                    // Call Create Event API
                    self.sendEvent()
                }
                else if self.isComingFromPeepTextFlow == true
                {
                    
                    //Call Upload Media API
                    self.sendPeepText()
                }
            }
        }
        else
        {
            if mediaAttachment == MediaAttachment.audio
            {
                if self.bUploadingAttachment == false
                {
                    self.bUploadingAttachment = true
                    self.startUploading()
                }
                else
                {
                    self.statusLabel!.text = "Uploading...100% (Audio)"
                    
                    self.bUploadingAttachment = false
                    
                    //Call Upload Media API
                    self.uploadDigitalMedia()
                }
                
            }
            else if mediaAttachment == MediaAttachment.video
            {
                if self.bUploadingAttachment == false
                {
                    self.bUploadingAttachment = true
                    self.startUploading()
                }
                else
                {
                    self.statusLabel!.text = "Uploading...100% (Video)"
                    
                    self.bUploadingAttachment = false
                    
                    //Call Upload Media API
                    self.uploadDigitalMedia()
                }
                
            }
            else if self.bUploadingAttachment == false
            {
                self.statusLabel!.text = "Uploading...100% (Photo)"
                
                //Call Upload Media API
                self.uploadDigitalMedia()
            }
        }
    }
    
    //MARK:- Uploading digital media file on Amazon bucket with progress
    
    func startUploading() {
        
//        self.navigationItem.title = ""
        
        statusLabel?.removeFromSuperview()
        statusLabel = UILabel(frame: CGRect(x: 60, y: 2, width: UIScreen.main.bounds.width-120, height: 40))
        statusLabel?.textColor = UIColor.white
        statusLabel?.textAlignment = NSTextAlignment.center
        statusLabel?.font = ProximaNovaRegular(17)
        self.navigationController?.navigationBar.addSubview(statusLabel!)
        
        self.statusLabel!.text = "Uploading..."
        
        DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async {
            
            if self.isComingFromPeepTextFlow == true || self.isComingFromEventsFlow == true {
                printCustom("startUploading")
                if mediaAttachment == MediaAttachment.photo
                {
                    self.uploadingType = "Photo"
                    self.uploadData(self.imageData!, contentType:ContentType_Image)
                }
                else if mediaAttachment == MediaAttachment.audio
                {
                    if self.bUploadingAttachment == true
                    {
                        self.uploadingType = "Audio"
                        let audioData = try? Data.init(contentsOf: URL(fileURLWithPath: self.dataPath!))
                        self.uploadData(audioData!, contentType:ContentType_Audio)
                    }
                    else
                    {
                        self.uploadingType = "Photo"
                        self.uploadData(self.imageData!, contentType:ContentType_Image)
                    }
                   
                }
                else if mediaAttachment == MediaAttachment.video
                {
                    self.uploadVideo()
                }
            }
            else
            {
                //if audio attachment is added with picture
                if self.bUploadingAttachment == true && mediaAttachment == MediaAttachment.audio
                {
                    self.uploadingType = "Audio"
                    let audioData = try? Data.init(contentsOf: URL(fileURLWithPath: self.dataPath!))
                    self.uploadData(audioData!, contentType:ContentType_Audio)
                }
                //if video is uploading
                else if mediaAttachment == MediaAttachment.video
                {
                    self.uploadVideo()
                }
                //if simple picture/image is uploading
                else
                {
                    self.uploadingType = "Photo"
                    self.uploadData(self.imageData!, contentType:ContentType_Image)
                }
            }
        }
    }
    
    func thumbnailVideo(_ VideoURL:URL)->Data{
        
        var uiImage = UIImage()
        var imageData1 = Data()
        
        do {
            let asset = AVURLAsset(url: VideoURL, options: nil)
            let imgGenerator = AVAssetImageGenerator(asset: asset)
            imgGenerator.appliesPreferredTrackTransform = true
            let cgImage = try imgGenerator.copyCGImage(at: CMTimeMake(0, 1), actualTime: nil)
            uiImage = UIImage(cgImage: cgImage)
            
            printCustom("width: \(uiImage.size.width) height \(uiImage.size.height)")
            
            imageData1 = UIImageJPEGRepresentation(uiImage, 0.5)!
            
            // lay out this image view, or if it already exists, set its image property to uiImage
        } catch let error as NSError {
            printCustom("Error generating thumbnail: \(error)")
        }
        
        return imageData1
    }
    
    func uploadVideo()
    {
        switch (self.mediaCategoryId)
        {
        case 1:
            mediaCategory = MediaCategory.music
            break;
        case 2:
            mediaCategory = MediaCategory.movie
            break;
        case 3:
            mediaCategory = MediaCategory.photo
            break;
        case 4:
            mediaCategory = MediaCategory.videoGame
            break;
        default:
            mediaCategory = MediaCategory.movie
            break;
        }
        
       
        let fileManager = FileManager.default
        let urls = fileManager.urls(for: .documentDirectory, in: .userDomainMask)
        let documentDirectory = urls[0] as URL
        var VideoURL = documentDirectory.appendingPathComponent("Movie.mp4")
        
        
        if (filterApplied == true)
        {
            //if filter has applied on video in VideoFilterController.m then we will upload movie file (Movie.mp4) from Document folder
            VideoURL = documentDirectory.appendingPathComponent("Movie.mp4")
            
            let videoData:Data = try! Data.init(contentsOf: URL(fileURLWithPath: VideoURL.path))
            if videoData.count <= 0
            {
                let ac = UIAlertController(title: "", message: alert_filtererror, preferredStyle: .alert)
                ac.addAction(UIAlertAction(title: alertBtnTitle_OK, style: UIAlertActionStyle.default, handler: { (UIAlertAction) -> Void in
                    
                    return
                }))
                //ac.addAction(UIAlertAction(title: alertBtnTitle_OK, style: .default, handler: nil))
            }
        }
        else
        {
            let videoData:Data = try! Data.init(contentsOf: URL(fileURLWithPath: VideoURL.path))
            if videoData.count <= 0
            {
                let ac = UIAlertController(title: "", message: alert_videoerror, preferredStyle: .alert)
                ac.addAction(UIAlertAction(title: alertBtnTitle_OK, style: UIAlertActionStyle.default, handler: { (UIAlertAction) -> Void in
                    
                    return
                }))
                //ac.addAction(UIAlertAction(title: alertBtnTitle_OK, style: .default, handler: nil))
            }
            
            //if no filter has applied on video in VideoFilterController.m then we will upload movie file (tempMovie.mp4) from tmp folder
            VideoURL = URL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent("tempMovie").appendingPathExtension("mp4")
        }
        
        printCustom("Video URL Path: ", function: VideoURL.path)
        
        if FileManager.default.fileExists(atPath: VideoURL.path) {
            
            //Uploading video thumbnail
            if self.bUploadingAttachment == false
            {
                self.uploadingType = "Photo"
                let thumbnailData = thumbnailVideo(VideoURL)
                 self.uploadData(thumbnailData, contentType:ContentType_Image)
            }
            //Uploading video file data
            else
            {
                self.uploadingType = "Video"
                let videoData:Data = try! Data.init(contentsOf: URL(fileURLWithPath: VideoURL.path))
                self.uploadData(videoData, contentType:ContentType_Movie)
            }
        }
        else
        {
            self.clearUploadingState()

            let alert = UIAlertController(title: "", message: alertMsg_prepareVideoError, preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (UIAlertAction) -> Void in
                // Start capturing video again from start.
               // self.navigationController?.popToRootViewController(animated: true)
                if (self.isComingFromEventsFlow) {
                    if self.navigationController != nil {
                        for viewController in self.navigationController!.viewControllers {
                            if viewController.isKind(of: CreateEventSecondViewController.self) {
                                self.navigationController?.popToViewController(viewController, animated: true)
                                break
                            }
                        }
                    }
                }
                else {
                    if self.navigationController != nil {
                        for viewController in self.navigationController!.viewControllers {
                            if viewController.isKind(of: CaptureVideo.self) {
                                self.navigationController?.popToViewController(viewController, animated: true)
                                break
                            }
                        }
                    }
                }
            }))
            
            self.present(alert, animated: true, completion: nil)
            /*//Wait for video saving till 32 seconds
            if( self.videoPreparingDelay > 32)
            {
                //break uploading state
                self.clearUploadingState()
            }
            else
            {
                //wait for video saving
                
                DispatchQueue.main.async(execute: {
                    
                    self.videoPreparingDelay += 1
                    
                    self.statusLabel!.text = "Preparing Video..."
                    
                    self.perform(#selector(ContactsScreen.startUploading), with: nil, afterDelay: 1.0)
                })
                
            }*/
        }
    }
    
    func uploadData(_ data: Data, contentType:String ) {
        
         self.navigationItem.title = ""
        
        var dataMedia = Data()
        
        let expression = AWSS3TransferUtilityUploadExpression()
         
        expression.progressBlock = progressBlock
        
        let transferUtility = AWSS3TransferUtility.default()
        var S3UploadKeyName1 = ""
        
        if sendMediaLocked == 1{
            
            //dataMedia = Encryption.sharedInstance.encryptData(data, RSAPublicKey: selectedContactPublicKey!)
            
            dataMedia = Encryption.sharedInstance.encryptData(data)
            
            S3UploadKeyName1 = String.init(format: "%@.dat", Utilities().getUUIDForObjectUpload())
        }
        else if contentType == ContentType_Image
        {
            dataMedia = data
            
            S3UploadKeyName1 = String.init(format: "%@.png", Utilities().getUUIDForObjectUpload())
        }
        else if contentType == ContentType_Movie
        {
            dataMedia = data
            
            S3UploadKeyName1 = String.init(format: "%@.mp4", Utilities().getUUIDForObjectUpload())
        }
        else if contentType == ContentType_Audio
        {
            dataMedia = data
            
            S3UploadKeyName1 = String.init(format: "%@.m4a", Utilities().getUUIDForObjectUpload())
        }
        
        
        if self.isComingFromPeepTextFlow == true || self.isComingFromEventsFlow == true
        {
            if mediaAttachment == MediaAttachment.photo
            {
                 mediaFile = S3UploadKeyName1
            }
            else if mediaAttachment == MediaAttachment.audio || mediaAttachment == MediaAttachment.video
            {
                if bUploadingAttachment == true
                {
                    mediaFileAttachment = S3UploadKeyName1
                }
                else
                {
                    mediaFile = S3UploadKeyName1
                }
            }
        }
        else if bUploadingAttachment == true
        {
            mediaFileAttachment = S3UploadKeyName1
        }
        else
        {
            mediaFile = S3UploadKeyName1
        }
        
        //hide lock/unlock popup
        hidelockUnlockPopup()
        
        printCustom("S3UploadKeyName1: \(S3UploadKeyName1)")
        
        //Save locked media locally in sent folder
        if sendMediaLocked == 1
        {
            var mediaName = ""
            
            //Decrypt data
            let decryptedData = Encryption.sharedInstance.decryptData(dataMedia)
            
           if contentType == ContentType_Movie
            {
                mediaName = S3UploadKeyName1.replacingOccurrences(of: ".dat", with: ".mp4")
            }
            else if contentType == ContentType_Audio
            {
                mediaName = S3UploadKeyName1.replacingOccurrences(of: ".dat", with: ".m4a")
            }
            else if contentType == ContentType_Image
            {
                mediaName = S3UploadKeyName1.replacingOccurrences(of: ".dat", with: ".png")
            }
            
            Utilities().saveMediaLocally(mediaName, mediaData: decryptedData, folderName: localSentFolderName)
        }
        
        
        transferUtility.uploadData(
            dataMedia,
            bucket: S3BucketName,
            key: S3UploadKeyName1,
            contentType: contentType,
            expression: expression,
            completionHandler: completionHandler).continueWith { (task) -> AnyObject! in
                if let error = task.error {
                    
                    NSLog("Error: %@",error.localizedDescription);
                    
                    let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                    
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (UIAlertAction) -> Void in
                        
                        self.clearUploadingState()
                    }))
                   
                    self.present(alert, animated: true, completion: nil)

                }
                if task.isFaulted {
                    
                    //NSLog("Exception: %@",exception.description);
                    
                    let alert = UIAlertController(title: "Error", message: "Some error occured", preferredStyle: UIAlertControllerStyle.alert)
                    
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (UIAlertAction) -> Void in
                        
                        self.clearUploadingState()
                    }))
                    
                    self.present(alert, animated: true, completion: nil)
                    
                }
                if let _ = task.result {
                    
                    NSLog("Upload Starting!")
                    
                    
                    self.statusLabel!.text = "Uploading Start"
                }
                
                return nil;
        }
    }
    
    func clearUploadingState(){
        
         DispatchQueue.main.async(execute: {
            self.mediaFile = ""//empty media file if not uploaded.
            self.mediaFileAttachment = ""//empty media file attachment if not uploaded.

            self.backgroundView.removeFromSuperview()
            
            self.statusLabel?.removeFromSuperview()
            self.navigationItem.title = "Contacts"
            
            self.navigationItem.hidesBackButton = false
            
            let leftBtn = UIBarButtonItem(image: Utilities().themedImage(img_backArrow), style: UIBarButtonItemStyle.plain, target: self, action: #selector(ContactsScreen.back))
            self.navigationItem.leftBarButtonItem = leftBtn
            
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Send", style: UIBarButtonItemStyle.plain, target: self, action: #selector(ContactsScreen.done))
        })
    }

    //MARK:- Get Hash tags fom caption
    
    func gethashTags(_ mystr:String)->String
    {
        let searchstr = "#"
        let ranges: [NSRange]
        var hashTags = ""
        
        do {
            // Create the regular expression.
            let regex = try NSRegularExpression(pattern: searchstr, options: [])
            
            // Use the regular expression to get an array of NSTextCheckingResult.
            // Use map to extract the range from each result.
            ranges = regex.matches(in: mystr, options: [], range: NSMakeRange(0, mystr.characters.count)).map {$0.range}
            
            //print(ranges)
            
            var arr = [String]()
            
            for rg in ranges{
                
                let index1 = mystr.characters.index(mystr.startIndex, offsetBy: rg.location)
                var substring1 = mystr.substring(from: index1)
                
                if let x:Range<String.Index> = substring1.range(of: " ")
                {
                    substring1 = substring1.substring(to: x.lowerBound)
                    arr.append(substring1)
                }
                else
                {
                    arr.append(substring1)
                }
            }
            
            print(arr)
            
            if arr.count > 0
            {
                for hash in arr
                {
                    hashTags = hashTags + hash + ","
                }
                
                hashTags = hashTags.trimmingCharacters(in: CharacterSet.init(charactersIn: ","))
            }
            
            
        }
        catch {
            // There was a problem creating the regular expression
            ranges = []
        }
        
        return hashTags
    }
    
    //MARK:- Uploading Digital Media
    
    func uploadDigitalMedia()
    {
    
        ApplicationDelegate.showLoader("Uploading Media...")
        var parameters = [String: AnyObject]()
        
        parameters["user_id"] = Utilities.getUserDetails().id as AnyObject
        
        if mediaType == MediaType.photo
        {
            parameters["media_type"] = 1 as AnyObject
        }
        else if mediaType == MediaType.audio
        {
            parameters["media_type"] = 2 as AnyObject
        }
        else if mediaType == MediaType.video
        {
            parameters["media_type"] = 3 as AnyObject
        }
        
        parameters["media_file"] =  mediaFile as AnyObject
        
        if mediaAttachment == MediaAttachment.audio
        {
            parameters["media_attachment"] = mediaFileAttachment as AnyObject
            parameters["media_attachment_type"] = 2 as AnyObject //Audio file
        }
        else if mediaAttachment == MediaAttachment.video
        {
            parameters["media_attachment"] = mediaFileAttachment as AnyObject
            parameters["media_attachment_type"] = 3 as AnyObject //Video file
        }
        
        parameters["media_caption"] = mediacaption as AnyObject
        
        //If All Followers(1), otherwise (0)
        if isPublishedAllFollowers == true
        {
            parameters["is_publishedAll"] = 1 as AnyObject
        }
        else
        {
            parameters["is_publishedAll"] = 0 as AnyObject
        }
        
        if mediacaption?.isEmpty == false
        {
            let hashTags = gethashTags(mediacaption!)
            
            if hashTags.isEmpty == false
            {
                parameters["hash_tags"] =  hashTags as AnyObject
            }
        }
        
        if mediaCategory == MediaCategory.music
        {
            parameters["category_id"] = 1 as AnyObject
        }
        else if mediaCategory == MediaCategory.movie
        {
            parameters["category_id"] = 2 as AnyObject
        }
        else if mediaCategory == MediaCategory.photo
        {
            parameters["category_id"] = 3 as AnyObject
        }
        else if mediaCategory == MediaCategory.videoGame
        {
            parameters["category_id"] = 4 as AnyObject
        }
        
        //This will work when send to Followings user
        
        if isPublishedAllFollowers == false
        {
            parameters["sent_to"] = contactIds as AnyObject
        }
    
        //If locked (1), else (0)
        parameters["is_locked"] = sendMediaLocked as AnyObject
        
        self.statusLabel!.text = "Publishing..."
        
        APIManager.sharedInstance.requestUploadDigitalMedia(parameters,Target:self)
    }
    
    //MARK:- Response Upload Digital Media
    
    func responseUploadDigitalMedia (_ notify: Foundation.Notification)
    {
        clearUploadingState()
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFIACTION_POSTUPLOADDIGITALMEDIA), object: nil)
        
        ApplicationDelegate.hideLoader ()
        let swiftyJsonVar = JSON(notify.object!)
        let message = swiftyJsonVar["message"].stringValue
        let status = swiftyJsonVar["status"].intValue
        
        if swiftyJsonVar [ERROR_KEY].exists()
        {
            self.showCommonAlert(swiftyJsonVar[ERROR_KEY].stringValue)
        }
        else if (status == 1)
        {
            //let response = swiftyJsonVar["response"]
            
            let alert = UIAlertController(title: "", message: alertMsg_sendMedia, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: alertBtnTitle_OK, style: UIAlertActionStyle.default, handler:{ (UIAlertAction) -> Void in
                isGalleryMediaSent = true
                self.navigationController?.popToRootViewController(animated: false)
            }))
            
            self.present(alert, animated: true, completion: nil)
        }
        else if (status == 0) //error response
        {
            let responseErrorMessage = swiftyJsonVar["response"].stringValue
            self.showCommonAlert(responseErrorMessage)
        }
        else{
            self.showCommonAlert(message)
        }
    }
       
    //MARK:- Sending Event
    func sendEvent() {
//        self.showCommonAlert(alertMsg_ApiInProgress)
//        
//        return
        
        ApplicationDelegate.showLoader(loaderTitle_SendingInvitation)
    
        var parameters = [String: AnyObject]()
        parameters["creator_id"] = Utilities.getUserDetails().id as AnyObject
        parameters["title"] = self.event?.title as AnyObject
        parameters["description"] = self.event?.desc as AnyObject
        parameters["location"] = self.event?.location as AnyObject
        parameters["start_date"] = Utilities().convertDateToAnotherFormat(self.event!.fromDate, fromFormat: dateFormatLocal, toFormat: dateFormatServer) as AnyObject//self.event?.fromDate//2016-10-27
        parameters["end_date"] = Utilities().convertDateToAnotherFormat(self.event!.toDate, fromFormat: dateFormatLocal, toFormat: dateFormatServer) as AnyObject
        parameters["start_time"] = Utilities().convertDateToAnotherFormat(self.event!.fromTime, fromFormat: timeFormatLocal, toFormat: timeFormatServer) as AnyObject//self.event?.fromTime//03:27:00
        parameters["end_time"] = Utilities().convertDateToAnotherFormat(self.event!.toTime, fromFormat: timeFormatLocal, toFormat: timeFormatServer) as AnyObject
        parameters["expiration_date"] = Utilities().convertDateToAnotherFormat(self.event!.expirationDate, fromFormat: dateFormatLocal, toFormat: dateFormatServer) as AnyObject
        parameters["category_id"] = self.event?.categoryId as AnyObject
        if event?.ticketCost != "0" {
            parameters["ticket_type"] = "1" as AnyObject//Paid
            parameters["ticket_cost"] = self.event?.ticketCost as AnyObject
        }
        else {
            parameters["ticket_type"] = "2" as AnyObject//Free
            parameters["ticket_cost"] = "0" as AnyObject
        }
        
        if let ticket = self.event?.totalTickets
        {
            if Int(ticket) > 0
            {
                parameters["total_tickets"] = self.event?.totalTickets as AnyObject
            }
            else
            {
                parameters["total_tickets"] = "0" as AnyObject
            }
        }
        else
        {
            parameters["total_tickets"] = "0" as AnyObject
        }
        
        //Survey Attached
        if event?.isSurveyAttached == 1
        {
            parameters["survey_add"] = 1 as AnyObject
        }
        else
        {
            parameters["survey_add"] = 0 as AnyObject
        }
        
        parameters["event_type_id"] = self.event?.eventType as AnyObject//Private = 1 / Public = 2
        if mediaAttachment != MediaAttachment.none {
            parameters["is_locked"] = sendMediaLocked as AnyObject
            if mediaFile != nil {
                if mediaFile!.contains(BASE_CLOUD_FRONT_URL) {
                    mediaFile = mediaFile?.replacingOccurrences(of: BASE_CLOUD_FRONT_URL, with: "")
                }
                parameters["media_file"] = mediaFile as AnyObject
            }
        
            var type = 0
            switch mediaAttachment {
            case .photo:
                type = 1
            case .audio:
                type = 2
            default://Video
                type = 3
            }
            parameters["media_type"] = type as AnyObject//1 = Photo,2 = Audio, 3 = Video
            
            if mediaFileAttachment != nil {
                if mediaFileAttachment!.contains(BASE_CLOUD_FRONT_URL) {
                    mediaFileAttachment = mediaFileAttachment?.replacingOccurrences(of: BASE_CLOUD_FRONT_URL, with: "")
                }
                parameters["media_attachment_url"] = mediaFileAttachment as AnyObject
            }
        }
        parameters["participants"] = contactIds as AnyObject
        
        if isEditingEvent {
            parameters["id"] = self.event?.id as AnyObject
            APIManager.sharedInstance.editEvent(parameters, Target: self)
        }
        else {
            APIManager.sharedInstance.requestSendEventInvitation(parameters, Target: self)
        }
    }
    
    //MARK:- Sending Peep Text
    
    func sendPeepText()
    {
        ApplicationDelegate.showLoader(loaderTitle_sendPeepText)
         var parameters = [String: AnyObject]()
        parameters["user_id"] = Utilities.getUserDetails().id as AnyObject
    
        parameters["media_file"] =  mediaFile as AnyObject
        
        if mediaAttachment == MediaAttachment.photo
        {
            parameters["media_attachment_type"] = 1 as AnyObject //Pic file
        }
        else if mediaAttachment == MediaAttachment.audio
        {
            parameters["media_attachment"] = mediaFileAttachment as AnyObject
            parameters["media_attachment_type"] = 2 as AnyObject //Audio file
        }
        else if mediaAttachment == MediaAttachment.video
        {
            parameters["media_attachment"] = mediaFileAttachment as AnyObject
            parameters["media_attachment_type"] = 3 as AnyObject //Video file
        }
        
        parameters["peep_text"] = peepTextConstant as AnyObject
        
        //This will work when send to Followings user
        
        if isPublishedAllFollowers == false
        {
            if contactIds?.isEmpty == true {
                self.setupSelectedContactIds()
            }
            parameters["sent_to"] = contactIds as AnyObject
        }
    
        //If locked (1), else (0)
        parameters["is_locked"] = sendMediaLocked as AnyObject
        
        self.statusLabel!.text = "Publishing..."
        
        print(parameters)
        
        APIManager.sharedInstance.requestSendPeepText(parameters,Target:self)
    }
  
    
    //MARK:- Response Send Event
    
    func responseSendEvent (_ notify: Foundation.Notification) {
        clearUploadingState()
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFICATION_SENDEVENT), object: nil)
        
        ApplicationDelegate.hideLoader ()
        let swiftyJsonVar = JSON(notify.object!)
        let message = swiftyJsonVar["message"].stringValue
        let status = swiftyJsonVar["status"].intValue
        
        if swiftyJsonVar [ERROR_KEY].exists()
        {
            self.showCommonAlert(swiftyJsonVar[ERROR_KEY].stringValue)
        }
        else if (status == 1)
        {
            //let response = swiftyJsonVar["response"]
            
            let alert = UIAlertController(title: "", message: alertMsg_EventInvitationSent, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: alertBtnTitle_OK, style: UIAlertActionStyle.default, handler:{ (UIAlertAction) -> Void in
                NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: NOTIFICATION_RELOADCREATEDEVENT), object: nil)

                self.navigationController?.popToRootViewController(animated: true)
            }))
            
            self.present(alert, animated: true, completion: nil)
            
        }
        else if (status == 0) //error response
        {
            let responseErrorMessage = swiftyJsonVar["response"].stringValue
            self.showCommonAlert(responseErrorMessage)
        }
        else
        {
            self.showCommonAlert(message)
        }
        
    }
    
    func responseEditEvent (_ notify: Foundation.Notification) {
        clearUploadingState()
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFICATION_EDITEVENT), object: nil)
        
        ApplicationDelegate.hideLoader ()
        let swiftyJsonVar = JSON(notify.object!)
        let message = swiftyJsonVar["message"].stringValue
        let status = swiftyJsonVar["status"].intValue
        
        if swiftyJsonVar [ERROR_KEY].exists()
        {
            self.showCommonAlert(swiftyJsonVar[ERROR_KEY].stringValue)
        }
        else if (status == 1)
        {
            let response = swiftyJsonVar["response"]["event_details"]
            printCustom("response:\(response)")
            var event = Event()
            event = Parser.getParsedEventsData(response)
            NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: NOTIFICATION_RELOADEDITEDEVENT), object: event)
            self.navigationController?.popToRootViewController(animated: true)
        }
        else if (status == 0) //error response
        {
            let responseErrorMessage = swiftyJsonVar["response"].stringValue
            self.showCommonAlert(responseErrorMessage)
        }
        else
        {
            self.showCommonAlert(message)
        }
        
    }
    

    //MARK:- Response Send Peep Text
    
    func responseSendPeepText (_ notify: Foundation.Notification)
    {
        clearUploadingState()
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFIACTION_POSTSENDPEEPTEXT), object: nil)
        
        ApplicationDelegate.hideLoader ()
        let swiftyJsonVar = JSON(notify.object!)
        let message = swiftyJsonVar["message"].stringValue
        let status = swiftyJsonVar["status"].intValue
        
        if swiftyJsonVar [ERROR_KEY].exists()
        {
            self.showCommonAlert(swiftyJsonVar[ERROR_KEY].stringValue)
        }
        else if (status == 1)
        {
            //let response = swiftyJsonVar["response"]
            
            let alert = UIAlertController(title: "", message: alertMsg_sendPeepText, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: alertBtnTitle_OK, style: UIAlertActionStyle.default, handler:{ (UIAlertAction) -> Void in
                
                self.navigationController?.popToRootViewController(animated: true)
            }))
            
            self.present(alert, animated: true, completion: nil)
            
        }
        else if (status == 0) //error response
        {
            let responseErrorMessage = swiftyJsonVar["response"].stringValue
            self.showCommonAlert(responseErrorMessage)
        }
        else
        {
            self.showCommonAlert(message)
        }
    }
    
    
    //MARK:- Get Contacts
    
    func getContacts()
    {
        searchBar.isUserInteractionEnabled = true
        btnSelectAll.isUserInteractionEnabled = true
        isContactSearching = false
        
        var parameters = [String: AnyObject]()
        parameters["user_id"] = Utilities.getUserDetails().id as AnyObject
        parameters["limit"] = pageSize as AnyObject
        parameters["offset"] = offset as AnyObject
        
        APIManager.sharedInstance.requestGetContacts(parameters,Target:self)
    }
    
    //MARK:- Response Get Contacts
    
    func responseGetContacts (_ notify: Foundation.Notification)
    {
        refreshControl.endRefreshing()
        
        if loadingIndicator.isAnimating {
            loadingIndicator.stopAnimating()
        }
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFIACTION_GETCONTACTS), object: nil)
        
        let swiftyJsonVar = JSON(notify.object!)
        let message = swiftyJsonVar["message"].stringValue
        let status = swiftyJsonVar["status"].intValue
        
        if swiftyJsonVar [ERROR_KEY].exists()
        {
            self.showErrorMessage(swiftyJsonVar[ERROR_KEY].stringValue)
        }
        else if (status == 1)
        {
            let response = swiftyJsonVar["response"]["list"]
            totalRecordCount = swiftyJsonVar["response"]["count"].intValue
            
            if response.count < 1
            {
                btnSelectAll.isHidden = true
                
                self.navigationItem.rightBarButtonItem = nil
                
                self.showErrorMessage(loaderTitle_noContact)
            }
            else
            {
                btnSelectAll.isHidden = false
                
                self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Send", style: UIBarButtonItemStyle.plain, target: self, action: #selector(ContactsScreen.done))
                
                if offset == 0
                {
                    arrContacts.removeAll()
                }
                
                var cnts = [Contact]()
                cnts = Parser.getParsedContactArrayFromData(response)
                arrContacts.append(contentsOf: cnts)
                printCustom("arrContacts:\(arrContacts)")
                if arrContacts.count>0
                {
                    self.hideErrorMessage()
                }
                else
                {
                     self.showErrorMessage(loaderTitle_noContact)
                }
            }
            
        }
        else if (status == 0) //error response
        {
            btnSelectAll.isHidden = true
            self.navigationItem.rightBarButtonItem = nil
            
            let responseErrorMessage = swiftyJsonVar["response"].stringValue
            self.showErrorMessage(responseErrorMessage)
        }
        else
        {
            self.showErrorMessage(message)
        }
    }
    

    //MARK:- Get Followers
    
    func getFollowers()
    {
        searchBar.isUserInteractionEnabled = false
        btnSelectAll.isUserInteractionEnabled = false
        
        isContactSearching = false
        
        var parameters = [String: AnyObject]()
        parameters["user_id"] = Utilities.getUserDetails().id as AnyObject
        parameters["limit"] = pageSize as AnyObject
        parameters["offset"] = offset as AnyObject
        
        APIManager.sharedInstance.requestGetFollowers(parameters,Target:self)
    }
    
    //MARK:- Response Get Followers
    
    func responseGetFollowers (_ notify: Foundation.Notification)
    {
        refreshControl.endRefreshing()
        
        if loadingIndicator.isAnimating {
            loadingIndicator.stopAnimating()
        }
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFIACTION_GETFOLLOWERS), object: nil)
        
        //ApplicationDelegate.hideLoader ()
        let swiftyJsonVar = JSON(notify.object!)
        let message = swiftyJsonVar["message"].stringValue
        let status = swiftyJsonVar["status"].intValue
        
        if swiftyJsonVar [ERROR_KEY].exists()
        {
            self.showErrorMessage(swiftyJsonVar[ERROR_KEY].stringValue)
        }
        else if (status == 1)
        {
            let response = swiftyJsonVar["response"]["list"]
            totalRecordCount = swiftyJsonVar["response"]["count"].intValue
            
            if response.count < 1
            {
                self.navigationItem.rightBarButtonItem = nil
                self.showErrorMessage(loaderTitle_noFollowers)
            }
            else
            {
                self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Send", style: UIBarButtonItemStyle.plain, target: self, action: #selector(ContactsScreen.done))
                
                if offset == 0
                {
                    arrContacts.removeAll()
                }
                
                var cnts = [Contact]()
                cnts = Parser.getParsedContactArrayFromData(response)
                
                arrContacts.append(contentsOf: cnts)
                printCustom("arrContacts:\(arrContacts)")

                if arrContacts.count>0
                {
                    for i in 0...arrContacts.count-1
                    {
                        arrContacts[i].pSelected = true
                    }
                    self.hideErrorMessage()
                }
                else
                {
                    self.showErrorMessage(loaderTitle_noFollowers)
                }
            }
            
        }
        else if (status == 0) //error response
        {
            
             self.navigationItem.rightBarButtonItem = nil
            
            let responseErrorMessage = swiftyJsonVar["response"].stringValue
            self.showErrorMessage(responseErrorMessage)
        }
        else
        {
            self.showErrorMessage(message)
        }
    }

    
    //MARK:- Search Contacts
    
    func searchContacts(_ text:String)
    {
        isContactSearching = true
        
        ApplicationDelegate.showLoader(loaderTitle_searchingContacts)
        
        var parameters = [String: AnyObject]()
        parameters["user_id"] = Utilities.getUserDetails().id as AnyObject
        parameters["limit"] = pageSize as AnyObject
        parameters["offset"] = offset as AnyObject
        parameters["keyword"] = text as AnyObject
        
        APIManager.sharedInstance.cancelRequest(RequestType.request_SEARCHCONTACTS)
        
        APIManager.sharedInstance.requestSearchContacts(parameters,Target:self)
    }
    
    //MARK:- Response Search Contacts
    
    func responseSearchContacts (_ notify: Foundation.Notification)
    {
        
        searchBar.resignFirstResponder()
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFIACTION_SEARCHCONTACTS), object: nil)
        
        ApplicationDelegate.hideLoader ()
        let swiftyJsonVar = JSON(notify.object!)
        let message = swiftyJsonVar["message"].stringValue
        let status = swiftyJsonVar["status"].intValue
        
        if swiftyJsonVar [ERROR_KEY].exists()
        {
            self.showCommonAlert(swiftyJsonVar[ERROR_KEY].stringValue)
        }
        else if (status == 1)
        {
            let response = swiftyJsonVar["response"]["list"]
            totalRecordCount = swiftyJsonVar["response"]["count"].intValue
            
            if response.count < 1
            {
                tblContacts.isHidden = true
                lblNoContact.isHidden = false
                lblNoContact.text = loaderTitle_noContact
            }
            else
            {
                var cnts = [Contact]()
                cnts = Parser.getParsedContactArrayFromData(response)
                
                arrFilterContacts.append(contentsOf: cnts)
                
                if arrFilterContacts.count>0
                {
                    lblNoContact.isHidden = true
                    tblContacts.isHidden = false
                    
                    //load table
                    tblContacts.reloadData()
                }
                else
                {
                    tblContacts.isHidden = true
                    lblNoContact.isHidden = false
                    lblNoContact.text = loaderTitle_noContact
                }
            }
            
        }
        else if (status == 0) //error response
        {
            let responseErrorMessage = swiftyJsonVar["response"].stringValue
            self.showCommonAlert(responseErrorMessage)
        }
        else
        {
            self.showCommonAlert(message)
        }
    }
    
    
    //MARK:- Forward Media
    func forwardMedia(_ fwdMediaId:Int)
    {
        var parameters = [String: AnyObject]()
        parameters["user_id"] = Utilities.getUserDetails().id as AnyObject
        parameters["parent_id"] = fwdMediaId as AnyObject
        parameters["sent_to"] = contactIds as AnyObject
        
        self.statusLabel!.text = "Forwarding Media..."
        
        print(parameters)
        
        APIManager.sharedInstance.requestForwardMedia(parameters,Target:self)
    }
    
    //MARK:- Response Forward Media
    func responseForwardMedia (_ notify: Foundation.Notification) {
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFIACTION_FORWARDMEDIA), object: nil)
        
        let swiftyJsonVar = JSON(notify.object!)
        
        let message = swiftyJsonVar["message"].stringValue
        let status = swiftyJsonVar["status"].intValue
        let response = swiftyJsonVar["response"].stringValue
        
        if swiftyJsonVar [ERROR_KEY].exists() {
            printCustom("response:\(swiftyJsonVar[ERROR_KEY].stringValue)")
        }
        else if (status == 0) //API message response
        {
            printCustom("response:\(message)")
        }
        else if (status == 1) //success response
        {
            printCustom("response:\(response)")
            
            let alert = UIAlertController(title: "", message: alertMsg_sendMedia, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: alertBtnTitle_OK, style: UIAlertActionStyle.default, handler:{ (UIAlertAction) -> Void in
                
                self.navigationController?.popToRootViewController(animated: true)
            }))
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    // MARK: - Show/Hide Error Message
    func showErrorMessage(_ message:String) {
        self.tblContacts.isHidden = true
        self.btnRefresh.isHidden = false
        self.lblNoContact.isHidden = false
        self.lblNoContact.text = message
    }
    
    func hideErrorMessage() {
        self.btnRefresh.isHidden = true
        self.lblNoContact.isHidden = true
        self.tblContacts.isHidden = false
        self.tblContacts.reloadData()
    }

}
