//
//  AddPeepText.swift
//  C00Peeps
//
//  Created by OSX on 26/09/16.
//  Copyright © 2016 SOTSYS011. All rights reserved.
//

import UIKit

class AddPeepText:BaseVC {
    
    @IBOutlet weak var btnSelectAttachment: UIButton!
    @IBOutlet weak var txtViewCaption: UITextView!
    let defaultText = alertMsg_PeepTextDefaultText
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.navigationItem.title = "Peep Text"
        txtViewCaption.text = defaultText
        
         self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Next", style: UIBarButtonItemStyle.plain, target: self, action: #selector(AddPeepText.nextFunc))
        
        self.updateViewAccordingToTheme()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        self.tabBarController?.tabBar.isHidden = false
    }
    
    // MARK: - Update View According To Selected Theme
    func updateViewAccordingToTheme() {
        btnSelectAttachment.setImage(Utilities().themedImage(img_attachIconSelected), for: .highlighted)
        btnSelectAttachment.setImage(Utilities().themedImage(img_attachIconSelected), for: .selected)
    }
    
    //Move to contact screen, without selecting any attachment
    func nextFunc(){
        
        if self.emptyText() == false
        {
            let st = UIStoryboard(name: "Digital", bundle:nil)
            let obj = st.instantiateViewController(withIdentifier: "ContactsScreen") as! ContactsScreen
            obj.isComingFromPeepTextFlow = true
            //obj.arrContacts = [sendPeepTextToContact]
            mediaAttachment = MediaAttachment.none
            peepTextConstant = self.txtViewCaption.text
            self.navigationController?.pushViewController(obj, animated: true)
        }
    }
    
    func emptyText()->Bool{
        
        txtViewCaption.text = txtViewCaption.text.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        
        if txtViewCaption.text == defaultText || txtViewCaption.text.isEmpty == true
        {
            let alert = UIAlertController(title: "", message: alertMsg_enterPeepText, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: alertBtnTitle_OK, style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
            return true
        }
        
        return false
    }
    //MARK:- Select Attachment Type
    
    @IBAction func selectAttachment(_ sender: AnyObject) {
        
        //Create the AlertController
        let actionSheetController: UIAlertController = UIAlertController(title: alertMsg_SelectAttachment, message: "", preferredStyle: .actionSheet)
        
        //Create and add the Cancel action
        let cancelAction: UIAlertAction = UIAlertAction(title: alertBtnTitle_Cancel, style: .cancel) { action -> Void in
            //Just dismiss the action sheet
        }
        actionSheetController.addAction(cancelAction)
        //Create and add first option action
        let attachAudioAction: UIAlertAction = UIAlertAction(title: alertMsg_AttachAudio, style: .default) { action -> Void in
            //Code for launching the camera goes here
            
            if self.emptyText() == false
            {
                let st = UIStoryboard(name: "Digital", bundle:nil)
                let obj = st.instantiateViewController(withIdentifier: "PhotoAddAudio") as! PhotoAddAudio
                obj.imagePhoto = Utilities().themedImage(img_audioDefault)
                obj.isComingFromPeepTextFlow = true
                peepTextConstant = self.txtViewCaption.text
                mediaAttachment = MediaAttachment.audio
                self.navigationController?.pushViewController(obj, animated: true)
            }
            
        }
        actionSheetController.addAction(attachAudioAction)
        
        //Create and add a second option action
        let attachPictureAction: UIAlertAction = UIAlertAction(title: alertMsg_AttachPhoto, style: .default) { action -> Void in
            //Code for picking from camera roll goes here
            
            if self.emptyText() == false
            {
                let st = UIStoryboard(name: "Digital", bundle:nil)
                let obj = st.instantiateViewController(withIdentifier: "CapturePhoto") as! CapturePhoto
                obj.isComingFromPeepTextFlow = true
                obj.isComingFromEventsFlow = false
                peepTextConstant = self.txtViewCaption.text
                mediaAttachment = MediaAttachment.photo
                self.navigationController?.pushViewController(obj, animated: true)
            }
            
        }
        actionSheetController.addAction(attachPictureAction)
        
        //Create and add a third option action
        let attachVideo: UIAlertAction = UIAlertAction(title: alertMsg_AttachVideo, style: .default) { action -> Void in
            //Code for picking from camera roll goes here
            
            if self.emptyText() == false
            {
                let st = UIStoryboard(name: "Digital", bundle:nil)
                let obj = st.instantiateViewController(withIdentifier: "CaptureVideo") as! CaptureVideo
                obj.isComingFromPeepTextFlow = true
                obj.isComingFromEventsFlow = false
                peepTextConstant = self.txtViewCaption.text
                mediaAttachment = MediaAttachment.video
                self.navigationController?.pushViewController(obj, animated: true)
            }
            
        }
        actionSheetController.addAction(attachVideo)
        
        //Create and add a fourth option action
        let chooseFromGallery: UIAlertAction = UIAlertAction(title: alertMsg_AttachGallery, style: .default) { action -> Void in
            if self.emptyText() == false
            {
                // Show Fusuma Gallery
                let fusuma = FusumaViewController()
                fusuma.isComingFromPeepTextFlow = true
                peepTextConstant = self.txtViewCaption.text
                self.navigationController?.pushViewController(fusuma, animated: true)
            }
        }
        actionSheetController.addAction(chooseFromGallery)
        
        //Present the AlertController
        self.present(actionSheetController, animated: true, completion: nil)
    }
    
    // MARK: - Media selected from Gallery
    func mediaSelected(_ mediaImage:UIImage?, mediaUrl:URL?) {
        printCustom("mediaSelected")
        
       /* if mediaImage != nil {
            self.isMediaAttached = true
            
            self.showDigitalMediaSelection()
            
            event?.mediaImage = mediaImage
            event?.mediaUrl = mediaUrl
        }
        else {
            self.isMediaAttached = false
            
            self.hideDigitalMediaSelection()
            
            event?.mediaImage = nil
            event?.mediaUrl = nil
        }*/
    }

    
    //MARK:- UITextView delegate methods
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool{
        
        if txtViewCaption.text == alertMsg_PeepTextDefaultText
        {
            txtViewCaption.text = ""
        }
        
        
        return true
    }
    
    func textView(_ textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        
        let numberOfChars = newText.characters.count // for Swift use count(newText)
        
        return numberOfChars < 140;
    }
}
