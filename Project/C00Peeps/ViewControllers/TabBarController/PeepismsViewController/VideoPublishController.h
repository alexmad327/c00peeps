//
//  VideoPublishController.h
//  C00Peeps
//
//  Created by OSX on 14/09/16.
//  Copyright © 2016 SOTSYS011. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import <GPUImage/GPUImage.h>
#import "GPUImage.h"
#import "VideoFilterController.h"
#import "AVAsset+VideoOrientation.h"

@class VideoFilterController;

@interface VideoPublishController : UIViewController<UITextViewDelegate>{
    
    GPUImageMovie *movieFile;
    GPUImageUIElement *uiElementInput;
    GPUImageFilterGroup *groupFilter;
    GPUImageOutput<GPUImageInput> *brightnessFilter;
    GPUImageOutput<GPUImageInput> *contrastFilter;
    GPUImageOutput<GPUImageInput> *saturationFilter;
    GPUImageOutput<GPUImageInput> *filter;
    GPUImageAlphaBlendFilter *blendFilter;
    GPUImageMovieWriter *movieWriter;
    NSTimer * timer;
    int filterType;
    GPUImageView *filterView ;
    GPUImageView *filterView1 ;
    GPUImageView *filterView2 ;
    GPUImageView *filterView3 ;
    GPUImageView *filterView4 ;

    UIView* coverView;

    __weak IBOutlet UILabel *lblCategory;
    __weak IBOutlet UIButton *btnCategory;
    __weak IBOutlet NSLayoutConstraint *layoutTxtViewToCategoryLabel;
    __weak IBOutlet NSLayoutConstraint *layoutTxtViewToCustomView;
    IBOutlet UINavigationBar *headerBar;
}

@property (strong, nonatomic) NSString *strPeepText;
@property (strong, nonatomic) IBOutlet UITextView *txtView;
@property (strong, nonatomic) IBOutlet UIView *customView;
@property (strong, nonatomic) IBOutlet UIView *containerView;
@property (strong, nonatomic) IBOutlet UIScrollView *scroll;
@property (strong, nonatomic) UIImage *updatedPhoto;
@property (retain, nonatomic) NSURL *sampleURL ;
@property GPUImageShowcaseFilterType typeFilter;
@property CGFloat filterValue;
@property int mediaCategoryId;
@property bool isComingFromPeepTextFlow;
@property bool isComingFromEventsFlow;
@property bool bWatermarkAdded;
@property float brightnessVal;
@property float saturationVal;
@property float contrastVal;

@end
