
//
//  CameraEngine.swift
//  naruhodo
//
//  Created by FUJIKI TAKESHI on 2014/11/10.
//  Copyright (c) 2014年 Takeshi Fujiki. All rights reserved.
//

import Foundation
import AVFoundation
import AssetsLibrary

class CameraEngine : NSObject, AVCaptureVideoDataOutputSampleBufferDelegate, AVCaptureAudioDataOutputSampleBufferDelegate{

    let captureSession = AVCaptureSession()
    let videoDevice = AVCaptureDevice.default(for: .video)
    let audioDevice = AVCaptureDevice.default(for: .audio)
    var videoWriter : VideoWriter?

    var height:Int?
    var width:Int?
    
    var isUsingFrontFacingCamera = false
    var isCapturing = false
    var isPaused = false
    var isDiscontinue = false
    var fileIndex = 0
    
    var timeOffset = CMTimeMake(0, 0)
    var lastAudioPts: CMTime?

    let lockQueue = DispatchQueue(label: "com.takecian.LockQueue", attributes: [])
    let recordingQueue = DispatchQueue(label: "com.takecian.RecordingQueue", attributes: [])
    
    var captureVideo:CaptureVideo?
    
    func startup(){
        
        // video input
        videoDevice?.activeVideoMinFrameDuration = CMTimeMake(1, 30)
        
      
        do
        {
            let videoInput = try AVCaptureDeviceInput(device: videoDevice!) as AVCaptureDeviceInput
            captureSession.addInput(videoInput)
        }
        catch let error as NSError {
            Logger.log(error.localizedDescription)
        }

        do
        {
            let audioInput = try AVCaptureDeviceInput(device: audioDevice!) as AVCaptureDeviceInput
            captureSession.addInput(audioInput)
        }
        catch let error as NSError {
            Logger.log(error.localizedDescription)
        }
        
        
        // video output
        let videoDataOutput = AVCaptureVideoDataOutput()
        videoDataOutput.setSampleBufferDelegate(self, queue: recordingQueue)
        videoDataOutput.alwaysDiscardsLateVideoFrames = true
        videoDataOutput.videoSettings = [
            
            kCVPixelBufferPixelFormatTypeKey as AnyHashable : Int(kCVPixelFormatType_32BGRA)
            ] as! [String : Any]
        captureSession.addOutput(videoDataOutput)
        if let connection = videoDataOutput.connection(with: AVMediaType.video) {
            if connection.isVideoOrientationSupported {
                // Force recording to portrait
                connection.videoOrientation = AVCaptureVideoOrientation.portrait
            }
        }
        
        height = Int(UIScreen.main.bounds.width)
        width = Int(UIScreen.main.bounds.width)
        
        if (width! % 2) != 0{
            
            width = width! + 1
            height = height! + 1
        }
        
        // audio output
        let audioDataOutput = AVCaptureAudioDataOutput()
        audioDataOutput.setSampleBufferDelegate(self, queue: recordingQueue)
        captureSession.addOutput(audioDataOutput)
        
        captureSession.sessionPreset = AVCaptureSession.Preset.medium
        captureSession.startRunning()
    }
    
    func shutdown(){
        captureSession.stopRunning()
    }

    func start(){
        lockQueue.sync {
            if !self.isCapturing{
                Logger.log("in")
                
                self.videoWriter = VideoWriter(
                    fileUrl: self.filePathUrl(),
                    height: self.height!, width: self.width!,
                    channels: 1,
                    samples: 44100
                )
                
                self.isPaused = false
                self.isDiscontinue = false
                self.isCapturing = true
                self.timeOffset = CMTimeMake(0, 0)
            }
        }
    }
    
    func stop(){
        self.lockQueue.sync {
            if self.isCapturing{
                self.isCapturing = false
                DispatchQueue.main.async(execute: { () -> Void in
                    Logger.log("in")
                    self.videoWriter!.finish { () -> Void in
                        Logger.log("Recording finished.")
                        self.videoWriter = nil
                        
                        self.createThumbnailFromVideo()
                        
                        self.captureVideo?.processThumbnail()
                        
//                        let assetsLib = ALAssetsLibrary()
//                        assetsLib.writeVideoAtPathToSavedPhotosAlbum(self.filePathUrl(), completionBlock: {
//                            (nsurl, error) -> Void in
//                            Logger.log("Transfer video to library finished.")
//                            self.fileIndex++
//                        })
                    }
                })
            }
        }
    }
    
    //MARK:- Thumbnail video
    
    func createThumbnailFromVideo(){
        
        var uiImage = UIImage()
        
        do {
            let asset = AVURLAsset(url: self.filePathUrl(), options: nil)
            let imgGenerator = AVAssetImageGenerator(asset: asset)
            imgGenerator.appliesPreferredTrackTransform = true
            let cgImage = try imgGenerator.copyCGImage(at: CMTimeMake(0, 1), actualTime: nil)
            uiImage = UIImage(cgImage: cgImage)
            
            uiImage = Utilities().cropToBounds(uiImage, cgwidth: UIScreen.main.bounds.width, cgheight: UIScreen.main.bounds.width)
            
            let imageData1 = UIImagePNGRepresentation(uiImage) //UIImageJPEGRepresentation(uiImage, 0.6)
            
            let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
            let documentsDirectory = paths.first! as String
            let dataPath1 = documentsDirectory + "/thumbnailVideo.png"
            printCustom(dataPath1)
            try? imageData1!.write(to: URL(fileURLWithPath: dataPath1), options: [.atomic])
            
        } catch let error as NSError {
            printCustom("Error generating thumbnail: \(error)")
        }
    }

    
    func pause(){
        self.lockQueue.sync {
            if self.isCapturing{
                Logger.log("in")
                self.isPaused = true
                self.isDiscontinue = true
            }
        }
    }
    
    func resume(){
        self.lockQueue.sync {
            if self.isCapturing{
                Logger.log("in")
                self.isPaused = false
            }
        }
    }
    
    func captureOutput(_ captureOutput: AVCaptureOutput!, didOutputSampleBuffer sampleBuffer: CMSampleBuffer!, from connection: AVCaptureConnection!){
        self.lockQueue.sync {
            if !self.isCapturing || self.isPaused {
                return
            }
            
            let isVideo = captureOutput is AVCaptureVideoDataOutput
            
            /*
            if self.videoWriter == nil && !isVideo {
                
                let fmt = CMSampleBufferGetFormatDescription(sampleBuffer)
                let asbd = CMAudioFormatDescriptionGetStreamBasicDescription(fmt!)
                
                self.videoWriter = VideoWriter(
                    fileUrl: self.filePathUrl(),
                    height: self.height!, width: self.width!,
                    channels: Int(asbd.memory.mChannelsPerFrame),
                    samples: asbd.memory.mSampleRate
                )
            }
            
            
            if self.isDiscontinue {
                if isVideo {
                    return
                }

                var pts = CMSampleBufferGetPresentationTimeStamp(sampleBuffer)

                let isAudioPtsValid = self.lastAudioPts!.flags.intersect(CMTimeFlags.Valid)
                if isAudioPtsValid.rawValue != 0 {
                    Logger.log("isAudioPtsValid is valid")
                    let isTimeOffsetPtsValid = self.timeOffset.flags.intersect(CMTimeFlags.Valid)
                    if isTimeOffsetPtsValid.rawValue != 0 {
                        Logger.log("isTimeOffsetPtsValid is valid")
                        pts = CMTimeSubtract(pts, self.timeOffset);
                    }
                    let offset = CMTimeSubtract(pts, self.lastAudioPts!);

                    if (self.timeOffset.value == 0)
                    {
                        Logger.log("timeOffset is \(self.timeOffset.value)")
                        self.timeOffset = offset;
                    }
                    else
                    {
                        Logger.log("timeOffset is \(self.timeOffset.value)")
                        self.timeOffset = CMTimeAdd(self.timeOffset, offset);
                    }
                }
                self.lastAudioPts!.flags = CMTimeFlags()
                self.isDiscontinue = false
            }
 
            
            var buffer = sampleBuffer
            if self.timeOffset.value > 0 {
                buffer = self.ajustTimeStamp(sampleBuffer, offset: self.timeOffset)
            }

            if !isVideo {
                var pts = CMSampleBufferGetPresentationTimeStamp(buffer)
                let dur = CMSampleBufferGetDuration(buffer)
                if (dur.value > 0)
                {
                    pts = CMTimeAdd(pts, dur)
                }
                self.lastAudioPts = pts
            }*/
            
            self.videoWriter?.write(sampleBuffer, isVideo: isVideo)
        }
    }
    
    func filePath() -> String {
        
        let tempPath = URL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent("tempMovie").appendingPathExtension("mp4").absoluteString
        if FileManager.default.fileExists(atPath: tempPath) {
            do {
                try FileManager.default.removeItem(atPath: tempPath)
            } catch { }
        }
        return tempPath
    }
    
    func filePathUrl() -> URL! {
        
        let tempPath = URL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent("tempMovie").appendingPathExtension("mp4").absoluteString
        if FileManager.default.fileExists(atPath: tempPath) {
            do {
                try FileManager.default.removeItem(atPath: tempPath)
            } catch { }
        }
        return URL(string: tempPath)!
    }
    
    func ajustTimeStamp(_ sample: CMSampleBuffer, offset: CMTime) -> CMSampleBuffer {
        var count: CMItemCount = 0
        CMSampleBufferGetSampleTimingInfoArray(sample, 0, nil, &count);
        var info = [CMSampleTimingInfo](repeating: CMSampleTimingInfo(duration: CMTimeMake(0, 0), presentationTimeStamp: CMTimeMake(0, 0), decodeTimeStamp: CMTimeMake(0, 0)), count: count)
        CMSampleBufferGetSampleTimingInfoArray(sample, count, &info, &count);

        for i in 0..<count {
            info[i].decodeTimeStamp = CMTimeSubtract(info[i].decodeTimeStamp, offset);
            info[i].presentationTimeStamp = CMTimeSubtract(info[i].presentationTimeStamp, offset);
        }

        var out: CMSampleBuffer?
        CMSampleBufferCreateCopyWithNewTiming(nil, sample, count, &info, &out);
        return out!
    }
    
    func switchCamera() {
        
        if AVCaptureDevice.devices(for: AVMediaType.video).count > 1 {
            
            let desiredPosition:AVCaptureDevice.Position?
            
            if isUsingFrontFacingCamera == true {
                
                desiredPosition = AVCaptureDevice.Position.back
            }
            else
            {
                desiredPosition = AVCaptureDevice.Position.front;
            }
            
            for d in AVCaptureDevice.devices(for: AVMediaType.video) {
                
                
                if (d as AnyObject).position == desiredPosition {
                    
                    captureSession.beginConfiguration()
                    
                    do {
                        
                        let input = try AVCaptureDeviceInput(device: d as! AVCaptureDevice)
                        
                        for oldInput in captureSession.inputs {
                            
                            captureSession.removeInput(oldInput as! AVCaptureInput)
                        }
                        
                        //Adding video input type in session
                        captureSession.addInput(input)
                        
                        //Adding audio input type in session
                        //let audioCaptureDevice = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeAudio)
                        let audioCaptureDevice = AVCaptureDevice.default(for: AVMediaType.audio)
                        do {
                            let audioInput = try AVCaptureDeviceInput.init(device: audioCaptureDevice!)
                            
                            captureSession.addInput(audioInput)
                            
                            captureSession.commitConfiguration()
                            
                            break;
                        }
                        catch let error as NSError {
                            printCustom("error: \(error)")
                        }
                        
                    } catch let error as NSError {
                        printCustom("error: \(error)")
                    }
                }
            }
            
            if isUsingFrontFacingCamera == true {
                
                isUsingFrontFacingCamera = false
            }
            else{
                
                isUsingFrontFacingCamera = true
            }
        }
    }
}
