//
//  CollectionViewCell.h
//  Speazie
//
//  Created by Mac on 13/08/16.
//
//

#import <UIKit/UIKit.h>

@interface CollectionViewCell : UICollectionViewCell{
    
    
}

@property(strong, nonatomic) IBOutlet UILabel *lblFilterName;
@property(strong, nonatomic) IBOutlet UIImageView *imgFilter;

@end
