//
//  CollectionViewCell.m
//  Speazie
//
//  Created by Mac on 13/08/16.
//
//

#import "CollectionViewCell.h"

@implementation CollectionViewCell

@synthesize lblFilterName, imgFilter;

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.imgFilter = [[UIImageView alloc] initWithFrame:CGRectMake(5, 20, 80, 80)];
        [self.contentView addSubview:self.imgFilter];
        
        self.lblFilterName = [[UILabel alloc] initWithFrame:CGRectMake(5, 2, 80, 15)];
        self.lblFilterName.textColor = [UIColor blackColor];
        self.lblFilterName.textAlignment = NSTextAlignmentCenter;
        self.lblFilterName.font = [UIFont fontWithName:@"ProximaNova-Regular" size:16];
        [self.contentView addSubview:self.lblFilterName];
    }
    return self;
}

@end
