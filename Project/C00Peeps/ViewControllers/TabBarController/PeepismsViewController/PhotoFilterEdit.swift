//
//  PhotoFilterEdit.swift
//  PhotoVideoFilter
//
//  Created by Mac on 5/09/16.
//  Copyright © 2016 Gagandeep Bawa. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation
import Photos
import ALCameraViewController

class PhotoFilterEdit: BaseVC, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, AVAudioPlayerDelegate {
    
    @IBOutlet weak var photoCollectionView: UICollectionView!
    @IBOutlet weak var photoView: UIImageView!
    
    @IBOutlet weak var btnPlayAudio: UIButton!
    @IBOutlet weak var btnCrop: UIButton!
    @IBOutlet weak var btnBrightness: UIButton!
    @IBOutlet weak var btnRotate: UIButton!
    @IBOutlet weak var sliderBrightness: UISlider!
    @IBOutlet weak var btnAddFilter: UIButton!
    @IBOutlet weak var btnEditPhoto: UIButton!
    @IBOutlet weak var lblCrop: UILabel!
    @IBOutlet weak var lblBrightness: UILabel!
    @IBOutlet weak var lblRotate: UILabel!
    @IBOutlet weak var photoviewBottom: NSLayoutConstraint!
    @IBOutlet weak var photocollectionviewCenter: NSLayoutConstraint!
    
    //var next = UIButton(type: UIButtonType.System) as UIButton
    let filterCount = 10
    var imgOrientation:UIImageOrientation?
    var coreImage:CIImage?
    
    var imagePhoto: UIImage!
    var updatedPhoto: UIImage!
    var brightPhoto: UIImage!
    var arrImages = [UIImage]()
    
    var selectedIndexPath: Int?
    var audioPlayer: AVAudioPlayer?
    var isBrightnessSelected:Bool?
    var dataPath:String?
    var isComingFromPeepTextFlow:Bool?
    var isComingFromEventsFlow:Bool?
    
    var delegate: CreateEventSecondControllerDelegate?
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.title = "Filter"
        var rightBtnTitle = "Next"
        if self.isComingFromEventsFlow == true {
            rightBtnTitle = "Done"
        }
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: rightBtnTitle, style: UIBarButtonItemStyle.plain, target: self, action: #selector(PhotoFilterEdit.moveNext))
        
        updateViewAccordingToTheme()
        
        setConstraint()
        
        photoView.image = imagePhoto
        
        //Brightness Slider
        isBrightnessSelected = false
        sliderBrightness.minimumTrackTintColor = Utilities().themedMultipleColor()[0]
        sliderBrightness.maximumTrackTintColor = UIColor.gray
        sliderBrightness.maximumValue =  1
        sliderBrightness.minimumValue =  0
        
        //lblTime.text =  NSString(format: "00:00/00:%d", Int(maxAudioRecordingTime)) as String
        
        btnCrop.isHidden = true
        btnRotate.isHidden = true
        btnBrightness.isHidden = true
        photoCollectionView.isHidden = false
        
        updatedPhoto = imagePhoto
        
        processFilterImage()
        
        if isComingFromPeepTextFlow == true
        {
            btnPlayAudio.isHidden = true
        }
        else
        {
            //Show hide audio buttons
            if FileManager.default.fileExists(atPath: dataPath!) == false{
                
                btnPlayAudio.isHidden = true
            }
            else
            {
                btnPlayAudio.isHidden = false
            }
        }
        
    }
    
    //MARK:- Play audio file
    
    func playAudioRecording(){
        
        if audioPlayer?.isPlaying == true
        {
            //change image of play button
            
            btnPlayAudio.setImage(Utilities().themedImage(img_play), for: .normal)
            
            audioPlayer?.stop()
        }
        else
        {
            //change image of play button
            
            btnPlayAudio.setImage(Utilities().themedImage(img_playSelected), for: .normal)
            
            let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
            let documentsDirectory = paths.first! as String
            let dataPath = documentsDirectory + "/audio.m4a"
            
            let url = URL(fileURLWithPath: dataPath)
            
            do{
                audioPlayer = try AVAudioPlayer(contentsOf: url)
                
                audioPlayer?.delegate = self
                
                audioPlayer?.play()
            }
            catch{
                
                
            }
        }
        
    }
    
    //MARK:- Audio Play Finish delegate
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool)
    {
        //change image of play button
        
        btnPlayAudio.setImage(Utilities().themedImage(img_play), for: .normal)
    }
    
    //MARK:-
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    
    //MARK:- Move Next
    
    func moveNext(){
        if self.isComingFromEventsFlow == true {
            if self.navigationController != nil {
                for viewController in self.navigationController!.viewControllers {
                    if viewController.isKind(of: CreateEventSecondViewController.self) {
                        self.navigationController?.popToViewController(viewController, animated: true)
                        break
                    }
                }
            }
            
            if mediaAttachment == MediaAttachment.audio {
                let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
                let documentsDirectory = paths.first! as String
                let dataPath = documentsDirectory + "/audio.m4a"
                
                let url = URL(fileURLWithPath: dataPath)
                
                do{
                    self.delegate?.mediaSelected(photoView.image!, mediaUrl: url)
                    
                }
            }
            else {
                self.delegate?.mediaSelected(photoView.image!, mediaUrl: nil)
            }
        }
        else {
            let st = UIStoryboard(name: "Digital", bundle:nil)
            let obj = st.instantiateViewController(withIdentifier: "PublishPhotoAudio") as! PublishPhotoAudio
            obj.imagePhoto = photoView.image
            obj.dataPath = dataPath;
            obj.isComingFromPeepTextFlow = isComingFromPeepTextFlow
            self.navigationController?.hidesBottomBarWhenPushed = false
            self.navigationController?.pushViewController(obj, animated: true)
        }
        
    }
    
    //MARK:- Play Audio
    
    @IBAction func playAudio(_ sender: AnyObject) {
        
        playAudioRecording()
    }
    
    
    // MARK: - Update View According To Selected Theme
    
    func updateViewAccordingToTheme() {
        
        btnPlayAudio.setImage(Utilities().themedImage(img_play), for: UIControlState())
        btnPlayAudio.setImage(Utilities().themedImage(img_playSelected), for: .highlighted)
        btnPlayAudio.setImage(Utilities().themedImage(img_playSelected), for: .selected)
        
        btnCrop.setImage(Utilities().themedImage(img_crop), for: .normal)
        btnCrop.setImage(Utilities().themedImage(img_cropSelected), for: .highlighted)
        btnCrop.setImage(Utilities().themedImage(img_cropSelected), for: .selected)
        
        btnRotate.setImage(Utilities().themedImage(img_rotate), for: .normal)
        btnRotate.setImage(Utilities().themedImage(img_rotateSelected), for: .highlighted)
        btnRotate.setImage(Utilities().themedImage(img_rotateSelected), for: .selected)
        
        
        btnBrightness.setImage(Utilities().themedImage(img_brighness), for: .normal)
        btnBrightness.setImage(Utilities().themedImage(img_brighnessSelected), for: .highlighted)
        btnBrightness.setImage(Utilities().themedImage(img_brighnessSelected), for: .selected)
        
        btnAddFilter.setImage(Utilities().themedImage(img_filterBtm), for: .normal)
        btnAddFilter.setImage(Utilities().themedImage(img_filterBtmSelected), for: .highlighted)
        btnAddFilter.setImage(Utilities().themedImage(img_filterBtmSelected), for: .selected)
        
        btnEditPhoto.setImage(Utilities().themedImage(img_editBtm), for: .normal)
        btnEditPhoto.setImage(Utilities().themedImage(img_editBtmSelected), for: .highlighted)
        btnEditPhoto.setImage(Utilities().themedImage(img_editBtmSelected), for: .selected)
        
        lblCrop.isHidden = true
        lblBrightness.isHidden = true
        lblRotate.isHidden = true
    }
    
    
    //MARK:- Set Constraints
    
    func setConstraint(){
        
        let bb = UIScreen.main.bounds.width - UIScreen.main.bounds.height/2
        
        let aa = (UIScreen.main.bounds.height-40 - UIScreen.main.bounds.width)-40
        
        var cc = (UIScreen.main.bounds.height-40 - UIScreen.main.bounds.width)/2
        
        cc = cc + bb
        
        
        let ph = UIScreen.main.bounds.height-40 - UIScreen.main.bounds.width - 64
        
        photoviewBottom.constant = ph
        
        photocollectionviewCenter.constant = cc - 50 + 64
        
        var constraints = photoCollectionView.constraints
        
        var count = constraints.count
        var index = 0;
        var found = false;
        
        while (!found && index < count) {
            let constraint = constraints[index]
            if constraint.identifier == "photoCollectionHeight" {
                //save the reference to constraint
                found = true;
                
                constraint.constant = aa + 44
            }
            
            index += 1;
        }
        
        
        constraints = self.view.constraints
        count = constraints.count
        index = 0;
        
        while (index < count) {
            let constraint = constraints[index]
            if constraint.identifier == "photoCollectionPosition" {
                
                constraint.constant = bb + 20 + 44
            }
            else if constraint.identifier == "cropY" {
                
                constraint.constant = cc + 44 - 20
            }
            else if constraint.identifier == "rotateY" {
                
                constraint.constant = cc + 44 - 20
            }
            else if constraint.identifier == "brightnessY" {
                
                constraint.constant = cc + 44 - 20
            }
            else if constraint.identifier == "brightnessSliderY" {
                
                constraint.constant = cc + 44
            }
            index += 1;
        }
        
    }
    
    //MARK:- Collection View Delegate Methods
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionCell", for: indexPath as IndexPath) as! CollectionCell
        
        // add a border
        cell.layer.borderColor = Utilities().themedMultipleColor()[0].cgColor//UIColor.purpleColor().CGColor
        cell.layer.borderWidth = 4
        
        photoView.image = applyCoreImageFilter(indexPath.row,img: updatedPhoto)
        
        selectedIndexPath = indexPath.row
        
        photoCollectionView.reloadData()
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeSelectItemAtIndexPath indexPath: IndexPath){
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionCell", for: indexPath as IndexPath) as! CollectionCell
        
        cell.layer.borderWidth = 0
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        
        return filterCount;
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        
        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionCell", for: indexPath as IndexPath) as! CollectionCell
        
        //        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("collectionCell", forIndexPath: indexPath) as! CollectionCell
        
        if indexPath.row < arrImages.count
        {
            let img = arrImages[indexPath.row]
            
            cell.imgView.image = img
            
            cell.lblFilterName.text = filterTitle(indexPath.row)
        }
        
        var borderColor: CGColor! = UIColor.clear.cgColor
        var borderWidth: CGFloat = 0
        
        if indexPath.row == selectedIndexPath{
            //borderColor = UIColor.purpleColor().CGColor
            borderColor = UIColor.clear.cgColor
            borderWidth = 4 //or whatever you please
        }else{
            borderColor = UIColor.clear.cgColor
            borderWidth = 0
        }
        
        cell.imgView.layer.borderWidth = borderWidth
        cell.imgView.layer.borderColor = borderColor
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        
        //let hh = photoCollectionView.frame.size.height
        
        
        return CGSize(width: 100.0,height: 100.0)
    }
    
    //MARK:- Applying Filter
    
    func applyCoreImageFilter(_ index:Int, img:UIImage)->UIImage{
        
        let imgOrientation = img.imageOrientation
        
        let cgimg = img.cgImage
        
        let coreImage = CIImage(cgImage: cgimg!)
        
        
        let Output = filtername(index, coreImage: coreImage)
        
        let context = CIContext(options: nil)
        let output = context.createCGImage(Output, from: Output.extent)
        var result = UIImage(cgImage: output!, scale: 1.0, orientation: imgOrientation)
        
        result = Utilities().imageResize(result,sizeChange: CGSize(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.width ))
        
        return result
    }
    
    func filterTitle(_ index:Int)->String{
        
        switch index {
            
            //        case 1:
            //
            //            return "Original"
            
        case 0:
            
            return "SepiaTone"
            
        case 1:
            
            return "Tonal"
            
            
        case 2:
            
            return "Process"
            
        case 3:
            
            return "Vignette"
            
        case 4:
            
            return "Transfer"
            
        case 5:
            
            return "Noir"
            
            
        case 6:
            
            return "Mono"
            
        case 7:
            
            return "Instant"
            
            
        case 8:
            
            return "Fade"
            
            
        case 9:
            
            return "Invert"
            
            
        default:
            
            return "SepiaTone"
        }
        
    }
    
    func filtername(_ index:Int, coreImage:CIImage)->CIImage{
        
        switch index {
            
            //        case 0:
            //
            //            return coreImage
            
        case 0:
            
            let sepiaFilter = CIFilter(name: "CISepiaTone")
            sepiaFilter?.setValue(coreImage, forKey: kCIInputImageKey)
            sepiaFilter?.setValue(1, forKey: kCIInputIntensityKey)
            let sepiaOutput = sepiaFilter?.value(forKey: kCIOutputImageKey) as? CIImage
            return sepiaOutput!
            
        case 1:
            let photoEffectTonal = CIFilter(name:"CIPhotoEffectTonal")
            photoEffectTonal!.setValue(coreImage, forKey:kCIInputImageKey)
            let photoEffectTonalOutput = photoEffectTonal?.value(forKey: kCIOutputImageKey) as? CIImage
            return photoEffectTonalOutput!
            
        case 2:
            let photoEffectProcess = CIFilter(name:"CIPhotoEffectProcess")
            photoEffectProcess!.setValue(coreImage, forKey:kCIInputImageKey)
            let photoEffectProcessOutput = photoEffectProcess?.value(forKey: kCIOutputImageKey) as? CIImage
            return photoEffectProcessOutput!
            
        case 3:
            
            let vignette = CIFilter(name:"CIVignette")
            vignette!.setValue(coreImage, forKey:kCIInputImageKey)
            vignette!.setValue(1 * 2, forKey:"inputIntensity")
            vignette!.setValue(1 * 30, forKey:"inputRadius")
            let vignetteOutput = vignette?.value(forKey: kCIOutputImageKey) as? CIImage
            return vignetteOutput!
            
        case 4:
            
            let photoEffectTransfer = CIFilter(name:"CIPhotoEffectTransfer")
            photoEffectTransfer!.setValue(coreImage, forKey:kCIInputImageKey)
            let photoEffectTransferOutput = photoEffectTransfer?.value(forKey: kCIOutputImageKey) as? CIImage
            return photoEffectTransferOutput!
            
        case 5:
            
            let photoEffectNoir = CIFilter(name:"CIPhotoEffectNoir")
            photoEffectNoir!.setValue(coreImage, forKey:kCIInputImageKey)
            let photoEffectNoirOutput = photoEffectNoir?.value(forKey: kCIOutputImageKey) as? CIImage
            return photoEffectNoirOutput!
            
        case 6:
            let photoEffectMono = CIFilter(name:"CIPhotoEffectMono")
            photoEffectMono!.setValue(coreImage, forKey:kCIInputImageKey)
            let photoEffectMonoOutput = photoEffectMono?.value(forKey: kCIOutputImageKey) as? CIImage
            return photoEffectMonoOutput!
            
        case 7:
            
            let photoEffectInstant = CIFilter(name:"CIPhotoEffectInstant")
            photoEffectInstant!.setValue(coreImage, forKey:kCIInputImageKey)
            let photoEffectInstantOutput = photoEffectInstant?.value(forKey:
                kCIOutputImageKey) as? CIImage
            return photoEffectInstantOutput!
            
        case 8:
            let photoEffectFade = CIFilter(name:"CIPhotoEffectFade")
            photoEffectFade!.setValue(coreImage, forKey:kCIInputImageKey)
            let photoEffectFadeOutput = photoEffectFade?.value(forKey: kCIOutputImageKey) as? CIImage
            return photoEffectFadeOutput!
            
        case 9:
            let photoEffectInvert = CIFilter(name:"CIColorInvert")
            photoEffectInvert!.setValue(coreImage, forKey:kCIInputImageKey)
            let photoEffectInvertOutput = photoEffectInvert?.value(forKey: kCIOutputImageKey) as? CIImage
            return photoEffectInvertOutput!
            
        default:
            let sepiaFilter = CIFilter(name: "CISepiaTone")
            sepiaFilter?.setValue(coreImage, forKey: kCIInputImageKey)
            sepiaFilter?.setValue(1, forKey: kCIInputIntensityKey)
            let sepiaOutput = sepiaFilter?.value(forKey: kCIOutputImageKey) as? CIImage
            return sepiaOutput!
        }
        
    }
    
    //MARK:- Processing original image with all filters
    
    func processFilterImage(){
        
        
        DispatchQueue.global().async {
            // background thread code
            
            let img = Utilities().imageResize(self.updatedPhoto!, sizeChange: CGSize(width: 80, height: 80))
            
            self.arrImages.removeAll()
            
            self.arrImages = [UIImage]()
            
            for i in 0...self.filterCount-1
            {
                let imag = self.applyCoreImageFilter(i,img: img)
                
                self.arrImages.append(imag)
            }
            
            DispatchQueue.main.async {
                self.photoCollectionView.reloadData()
                
            }
        }
    }
    
    
    //MARK:- Edit Photo
    
    @IBAction func editPhoto(_ sender: AnyObject) {
        
        //Using this button as a Cancel button when brightnesws control selected
        if isBrightnessSelected == true
        {
            //unhide next button
            for vi in self.headerView.subviews
            {
                if vi.isKind(of:UIButton.self)
                {
                    let btn = vi as! UIButton
                    
                    if btn.tag == 1001
                    {
                        btn.isHidden = false
                    }
                }
            }
            
            btnAddFilter.setImage(Utilities().themedImage(img_filterBtm), for: .normal)
            btnEditPhoto.setImage(Utilities().themedImage(img_editBtm), for: .normal)
            
            isBrightnessSelected = false
            
            btnCrop.isHidden = false
            btnRotate.isHidden = false
            btnBrightness.isHidden = false
            lblCrop.isHidden = false
            lblBrightness.isHidden = false
            lblRotate.isHidden = false
            
            sliderBrightness.isHidden = true
            sliderBrightness.value = 0
            photoView.image = updatedPhoto
        }
        else
        {
            btnAddFilter.setImage(Utilities().themedImage(img_filterBtm), for: .normal)
            btnEditPhoto.setImage(Utilities().themedImage(img_editBtmSelected), for: .normal)
            
            btnCrop.setImage(Utilities().themedImage(img_crop), for: .normal)
            btnRotate.setImage(Utilities().themedImage(img_rotate), for: .normal)
            btnBrightness.setImage(Utilities().themedImage(img_brighness), for: .normal)
            
            btnCrop.isHidden = false
            btnRotate.isHidden = false
            btnBrightness.isHidden = false
            lblCrop.isHidden = false
            lblBrightness.isHidden = false
            lblRotate.isHidden = false
            
            
            photoCollectionView.isHidden = true
            sliderBrightness.isHidden = true
            
            updatedPhoto = photoView.image
        }
    }
    
    //MARK:- Show Filter
    
    @IBAction func addFilter(_ sender: AnyObject) {
        
        //Using this button as a Ok button when brightnesws control selected
        if isBrightnessSelected == true
        {
            //unhide next button
            for vi in self.headerView.subviews
            {
                if vi.isKind(of: UIButton.self)
                {
                    let btn = vi as! UIButton
                    
                    if btn.tag == 1001
                    {
                        btn.isHidden = false
                    }
                }
            }
            
            btnAddFilter.setImage(Utilities().themedImage(img_filterBtmSelected), for: .normal)
            btnEditPhoto.setImage(Utilities().themedImage(img_editBtm), for: .normal)
            
            isBrightnessSelected = false
            
            btnCrop.isHidden = false
            btnRotate.isHidden = false
            btnBrightness.isHidden = false
            lblCrop.isHidden = false
            lblBrightness.isHidden = false
            lblRotate.isHidden = false
            
            sliderBrightness.isHidden = true
            
            photoView.image = brightPhoto
            
            updatedPhoto = brightPhoto
        }
        else
        {
            
            btnAddFilter.setImage(Utilities().themedImage(img_filterBtmSelected), for: .normal)
            btnEditPhoto.setImage(Utilities().themedImage(img_editBtm), for: .normal)
            
            btnCrop.isHidden = true
            btnRotate.isHidden = true
            btnBrightness.isHidden = true
            lblCrop.isHidden = true
            lblBrightness.isHidden = true
            lblRotate.isHidden = true
            
            photoCollectionView.isHidden = false
            
            self.photoView.image = updatedPhoto
            
            processFilterImage()
        }
    }
    
    //MARK:- Rotate Photo
    
    @IBAction func rotatePhoto(_ sender: AnyObject) {
        
        btnCrop.setImage(Utilities().themedImage(img_crop), for: .normal)
        btnRotate.setImage(Utilities().themedImage(img_rotateSelected), for: .normal)
        btnBrightness.setImage(Utilities().themedImage(img_brighness), for: .normal)
        
        if let originalImage = self.updatedPhoto {
            
            let rotateSize = CGSize(width: originalImage.size.height, height: originalImage.size.width)
            let scale: CGFloat = 0.0 // Automatically use scale factor of main screen
            
            UIGraphicsBeginImageContextWithOptions(rotateSize, true, scale)
            if let context = UIGraphicsGetCurrentContext() {
                
                context.rotate(by: 90.0 * CGFloat(Double.pi) / 180.0)
                //CGContextRotateCTM(context, 90.0 * CGFloat(M_PI) / 180.0)
                
                context.translateBy(x: 0, y: -originalImage.size.height)
                // CGContextTranslateCTM(context, 0, -originalImage.size.height)
                
                
                originalImage.draw(in: CGRect(x:0, y: 0, width: originalImage.size.width, height: originalImage.size.height))
                
                //                originalImage.drawInRect(CGRectMake(0, 0, originalImage.size.width, originalImage.size.height))
                
                self.updatedPhoto = UIGraphicsGetImageFromCurrentImageContext()
                UIGraphicsEndImageContext()
            }
            
            photoView.image = self.updatedPhoto
        }
    }
    
    //MARK:- Brightness controls
    @IBAction func brightnessPhoto(_ sender: AnyObject) {
        
        btnCrop.setImage(Utilities().themedImage(img_crop), for: .normal)
        btnRotate.setImage(Utilities().themedImage(img_rotate), for: .normal)
        btnBrightness.setImage(Utilities().themedImage(img_brighnessSelected), for: .normal)
        
        btnAddFilter.setImage(Utilities().themedImage(img_saveBtm), for: .normal)
        btnEditPhoto.setImage(Utilities().themedImage(img_cancelBtm), for: .normal)
        
        //hide next button
        for vi in self.headerView.subviews
        {
            if vi.isKind(of: UIButton.self)
            {
                let btn = vi as! UIButton
                
                if btn.tag == 1001
                {
                    btn.isHidden = true
                }
            }
        }
        
        isBrightnessSelected = true
        
        btnCrop.isHidden = true
        btnRotate.isHidden = true
        btnBrightness.isHidden = true
        lblCrop.isHidden = true
        lblBrightness.isHidden = true
        lblRotate.isHidden = true
        
        sliderBrightness.isHidden = false
        
        imgOrientation = updatedPhoto.imageOrientation
        
        let cgimg = updatedPhoto.cgImage
        
        coreImage = CIImage(cgImage: cgimg!)
    }
    
    @IBAction func brightnessValueChanged(_ sender: AnyObject) {
        
        let intensity = sliderBrightness.value
        
        let lighten = CIFilter(name: "CIColorControls")
        lighten?.setValue(coreImage, forKey: kCIInputImageKey)
        lighten?.setValue(intensity, forKey: kCIInputBrightnessKey)
        
        let Output = lighten?.value(forKey:kCIOutputImageKey) as? CIImage
        
        let context = CIContext(options: nil)
        let output = context.createCGImage(Output!, from: Output!.extent)
        
        brightPhoto = UIImage(cgImage: output!, scale: 1.0, orientation: imgOrientation!)
        
        photoView.image = brightPhoto
    }
    
    
    //MARK:- Apply Crop
    
    var onCompletion: CameraViewCompletion?
    
    @IBAction func cropPhoto(_ sender: AnyObject) {
        
        
        btnCrop.setImage(Utilities().themedImage(img_cropSelected), for: .normal)
        btnRotate.setImage(Utilities().themedImage(img_rotate), for: .normal)
        btnBrightness.setImage(Utilities().themedImage(img_brighness), for: .normal)
        
        SingleImageSaver()
            .setImage(updatedPhoto!)
            .onSuccess { asset in
                
                self.startConfirmController(asset)
                
            }
            .onFailure { error in
                
            }
            .save()
    }
    
    fileprivate func startConfirmController(_ asset: PHAsset) {
        //let confirmViewController = ConfirmViewController(asset: asset, allowsCropping: true)
        let confirmViewController = ConfirmViewController.init(asset: asset, croppingParameters: CroppingParameters.init(isEnabled: true, allowResizing: true, allowMoving: true, minimumSize: CGSize.init(width: 512, height: 512)))
        
        confirmViewController.onComplete = { image, asset in
            if let image = image, let asset = asset {
                self.onCompletion?(image, asset)
                self.photoView.image = image
                self.updatedPhoto = image
                self.dismiss(animated: true, completion: nil)
            } else {
                self.dismiss(animated: true, completion: nil)
            }
        }
        confirmViewController.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        present(confirmViewController, animated: true, completion: nil)
    }
    
}
