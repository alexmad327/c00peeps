//
//  VideoWriter.swift
//  naruhodo
//
//  Created by FUJIKI TAKESHI on 2014/11/11.
//  Copyright (c) 2014年 Takeshi Fujiki. All rights reserved.
//

import Foundation
import AVFoundation
import AssetsLibrary

class VideoWriter : NSObject{
    var fileWriter: AVAssetWriter!
    var videoInput: AVAssetWriterInput!
    var audioInput: AVAssetWriterInput!
    
    init(fileUrl:URL!, height:Int, width:Int, channels:Int, samples:Float64){
        
        fileWriter = try? AVAssetWriter(outputURL: fileUrl, fileType: AVFileType.mov)
        
        let videoCleanApertureSettings = [AVVideoCleanApertureHeightKey: height,
                                          AVVideoCleanApertureWidthKey: width,
                                          AVVideoCleanApertureHorizontalOffsetKey: 2,
                                          AVVideoCleanApertureVerticalOffsetKey: 2
        ]
        
        let videoAspectRatioSettings = [AVVideoPixelAspectRatioHorizontalSpacingKey: 3,
                                          AVVideoPixelAspectRatioVerticalSpacingKey: 3
        ]
        //AVVideoMaxKeyFrameIntervalKey : 90,
        let codecSettings  = [AVVideoAverageBitRateKey: 960000,
                              AVVideoCleanApertureKey: videoCleanApertureSettings,
                              AVVideoProfileLevelKey : AVVideoProfileLevelH264Baseline41,
                              AVVideoPixelAspectRatioKey:videoAspectRatioSettings
        ] as [String : Any]
        
        let videoOutputSettings: Dictionary<String, AnyObject> = [
            AVVideoCodecKey : AVVideoCodecH264 as AnyObject,
            AVVideoWidthKey : width as AnyObject,
            AVVideoHeightKey : height as AnyObject,
            AVVideoCompressionPropertiesKey : codecSettings as AnyObject,
            AVVideoScalingModeKey: AVVideoScalingModeResizeAspectFill as AnyObject
        ]
        
        videoInput = AVAssetWriterInput(mediaType: .video, outputSettings: videoOutputSettings)
        videoInput.expectsMediaDataInRealTime = true
        fileWriter.add(videoInput)
        
        let audioOutputSettings: Dictionary<String, AnyObject> = [
            AVFormatIDKey : Int(kAudioFormatMPEG4AAC) as AnyObject,
            AVNumberOfChannelsKey : channels as AnyObject,
            AVSampleRateKey : samples as AnyObject,
            AVEncoderBitRateKey : 128000 as AnyObject
        ]
        audioInput = AVAssetWriterInput(mediaType: .audio, outputSettings: audioOutputSettings)
        audioInput.expectsMediaDataInRealTime = true
        fileWriter.add(audioInput)
    }
    
    func write(_ sample: CMSampleBuffer, isVideo: Bool){
        if CMSampleBufferDataIsReady(sample) {
            if fileWriter.status == AVAssetWriterStatus.unknown {
                
                if isVideo {
                
                    Logger.log("Start writing, isVideo = \(isVideo), status = \(fileWriter.status.rawValue)")
                    let startTime = CMSampleBufferGetPresentationTimeStamp(sample)
                    fileWriter.startWriting()
                    fileWriter.startSession(atSourceTime: startTime)
                }
            }
            if fileWriter.status == AVAssetWriterStatus.failed {
                Logger.log("Error occured, isVideo = \(isVideo), status = \(fileWriter.status.rawValue), \(fileWriter.error!.localizedDescription)")
                return
            }
            if isVideo {
                if videoInput.isReadyForMoreMediaData {
                    videoInput.append(sample)
                }
            }else{
                if audioInput.isReadyForMoreMediaData {
                    audioInput.append(sample)
                }
            }
        }
    }
    
    func finish(_ callback: @escaping () -> Void){
        fileWriter.finishWriting(completionHandler: callback)
    }
}
