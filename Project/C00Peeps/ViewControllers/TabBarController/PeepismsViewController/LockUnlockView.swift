//
//  LockUnlockView.swift
//  PhotoVideoFilter
//
//  Created by OSX on 09/09/16.
//  Copyright © 2016 Gagandeep Bawa. All rights reserved.
//

import Foundation
import UIKit

class LockUnlockView: UIView {

    @IBOutlet weak var btnLockUnlock: UIButton!
    @IBOutlet weak var btnSend: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var bgImageView: UIImageView!
    
    @IBOutlet weak var lblLockUnlock: UILabel!
    @IBOutlet weak var lblTitle: UILabel!

    var isLocked:Bool?
    var view: UIView!
    var delegate:LockUnlockProtocol?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        xibSetup()
    }

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        xibSetup()
    }
    
    func xibSetup() {
        view = loadViewFromNib()
        
        // use bounds not frame or it'll be offset
        view.frame = bounds
        
        // Make the view stretch with containing view
        view.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(view)
        
        setImagesAccordingToTheme()
        
       
    }
    
    func refreshDefaultLockUnlockView(){
        
        if Utilities.getUserDetails().privacy_level_id == "2" // private user
        {
            isLocked = true
            lblLockUnlock.text = "Locked"
            btnLockUnlock.setImage(Utilities().themedImage(img_lockIcon), for: UIControlState())
        }
        else
        {
            isLocked = false
            lblLockUnlock.text = "Unlocked"
            btnLockUnlock.setImage(Utilities().themedImage(img_unlockIcon), for: UIControlState())
        }
    }
   
    func refreshDefaultLockUnlockViewForEventFlow(_ event:Event){
        
        if event.eventType == 1 // private user
        {
            isLocked = true
            lblLockUnlock.text = "Locked"
            btnLockUnlock.setImage(Utilities().themedImage(img_lockIcon), for: UIControlState())
        }
        else if event.eventType == 2 // public user
        {
            isLocked = false
            lblLockUnlock.text = "Unlocked"
            btnLockUnlock.setImage(Utilities().themedImage(img_unlockIcon), for: UIControlState())
        }
    }
    
    func loadViewFromNib() -> UIView {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "LockUnlock", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        
        return view
    }
    
    //MARK:- Set images according to selected theme
    
    func setImagesAccordingToTheme(){
        
        isLocked = true
        
        bgImageView.image = Utilities().themedImage(img_popupBg)
        btnSend.setTitleColor(UIColor.white, for: UIControlState())
        btnSend.setTitle("Send", for: UIControlState())
        lblLockUnlock.textColor = UIColor.white
        lblLockUnlock.text = "Locked"
        
        btnLockUnlock.setImage(Utilities().themedImage(img_lockIcon), for: UIControlState())
        btnSend.setBackgroundImage(Utilities().themedImage(img_sendBtn), for: UIControlState())
        
    }
    
    
    @IBAction func LockUnlockMedia(_ sender: AnyObject) {
        
        if isLocked == true
        {
            isLocked = false
            lblLockUnlock.text = "Unlocked"
            btnLockUnlock.setImage(Utilities().themedImage(img_unlockIcon), for: UIControlState())
        }
        else
        {
            isLocked = true
            lblLockUnlock.text = "Locked"
            btnLockUnlock.setImage(Utilities().themedImage(img_lockIcon), for: UIControlState())
        }
    }
    
    @IBAction func sendMedia(_ sender: AnyObject) {
        
        delegate?.startSendingData()
    }
    
    @IBAction func cancel(_ sender: AnyObject) {
        
        delegate?.cancelPopup()
    }
}
