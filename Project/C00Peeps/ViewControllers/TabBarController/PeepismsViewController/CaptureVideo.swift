//
//  CaptureVideo.swift
//  PhotoVideoFilter
//
//  Created by Mac on 5/09/16.
//  Copyright © 2016 Gagandeep Bawa. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation
import AssetsLibrary

class CaptureVideo: BaseVC {

    
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var btnCaptureVideo: UIButton!
    @IBOutlet weak var btnCameraToggle: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnRedo: UIButton!
    @IBOutlet weak var sliderRecording: UISlider!
    

    var timer:Timer?
    var currentRecordingTime:Float = 0.0
    var videoThumbnail = UIImage()
    var isComingFromPeepTextFlow:Bool?
    var isComingFromEventsFlow:Bool?

    var isRecording = false
    let cameraEngine = CameraEngine()
    
    var delegate: CreateEventSecondControllerDelegate?

    lazy var frontCameraDevice: AVCaptureDevice? = {
        let devices = AVCaptureDevice.devices(for: AVMediaType.video) as! [AVCaptureDevice]
        return devices.filter{$0.position == .back}.first
    }()
    
    lazy var micDevice: AVCaptureDevice? = {
        //return AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeAudio)
        return AVCaptureDevice.default(for: AVMediaType.audio)
    }()
    
    
    fileprivate var tempFilePath: URL = {
       // let tempPath = URL(fileURLWithPath: NSTemporaryDirectory().appending("tempMovie").appending(.))
        let tempPath = URL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent("tempMovie").appendingPathExtension("mp4").absoluteString
        if FileManager.default.fileExists(atPath: tempPath) {
            do {
                try FileManager.default.removeItem(atPath: tempPath)
            } catch { }
        }
        return URL(string: tempPath
            )!
    }()

    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        btnRedo.isEnabled = false
        
        lblTime.text =  NSString(format: "00:00/00:%d", Int(maxVideoRecordingTime)) as String
        
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        super.viewWillDisappear(animated)

    }
    
    //MARK:- ViewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Video"
        
        btnRedo.isEnabled = false
        updateViewAccordingToTheme()
        
        setConstraint()
        
        self.navigationItem.hidesBackButton = true
        
        sliderRecording.minimumTrackTintColor = Utilities().themedMultipleColor()[0]
        sliderRecording.maximumTrackTintColor = UIColor.gray
        sliderRecording.setThumbImage(nil, for: UIControlState())
        sliderRecording.maximumValue =  Float(maxVideoRecordingTime)
        sliderRecording.minimumValue =  currentRecordingTime
        sliderRecording.setThumbImage(UIImage(), for: UIControlState())
        
        lblTime.text =  NSString(format: "00:00/00:%d", Int(maxVideoRecordingTime)) as String
        
        checkMicrophoneAuthorization()
        
        checkCameraAuthorization()
        
        
    }
    
    //MARK:- Camera Authorization
    func checkCameraAuthorization(){
        
        let authorizationStatus = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
        switch authorizationStatus {
        case .notDetermined:
            // permission dialog not yet presented, request authorization
            AVCaptureDevice.requestAccess(for: AVMediaType.video,
                                                      completionHandler: { (granted:Bool) -> Void in
                                                        if (granted == false) {
                                                            
                                                            //show alert
                                                            let alert = UIAlertController(title: "", message: alertMsg_CameraAccess, preferredStyle: UIAlertControllerStyle.alert)
                                                            alert.addAction(UIAlertAction(title: alertBtnTitle_OK, style: UIAlertActionStyle.default, handler:{ (UIAlertAction) -> Void in
                                                                
                                                                if let appSettings = URL(string: UIApplicationOpenSettingsURLString) {
                                                                    UIApplication.shared.openURL(appSettings)
                                                                }
                                                            }))
                                                            
                                                            self.present(alert, animated: true, completion: nil)
                                                        }
                                                        else
                                                        {
                                                             DispatchQueue.main.async(execute: { () -> Void in
                                                                
                                                                self.startCamera()
                                                            })
                                                        }
            })
        case .authorized:
            // go ahead
            self.startCamera()
            
            break
        case .denied, .restricted:
            
            print("user denied access of camera")
            
            //show alert
            let alert = UIAlertController(title: "", message: alertMsg_CameraAccess, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: alertBtnTitle_OK, style: UIAlertActionStyle.default, handler:{ (UIAlertAction) -> Void in
                
                if let appSettings = URL(string: UIApplicationOpenSettingsURLString) {
                    UIApplication.shared.openURL(appSettings)
                }
            }))
            
            self.present(alert, animated: true, completion: nil)
            
            break
            // the user explicitly denied camera usage or is not allowed to access the camera devices
        }
    }
    
    //MARK:- Audio Authorization
    func checkMicrophoneAuthorization(){
        
        let authorizationStatus = AVCaptureDevice.authorizationStatus(for: AVMediaType.audio)
        switch authorizationStatus {
        case .notDetermined:
            // permission dialog not yet presented, request authorization
            AVCaptureDevice.requestAccess(for: AVMediaType.audio,
                                                      completionHandler: { (granted:Bool) -> Void in
                                                        if (granted == false) {
                                                            
                                                            //show alert
                                                            let alert = UIAlertController(title: "", message: alertMsg_AudioAccess, preferredStyle: UIAlertControllerStyle.alert)
                                                            alert.addAction(UIAlertAction(title: alertBtnTitle_OK, style: UIAlertActionStyle.default, handler:{ (UIAlertAction) -> Void in
                                                                
                                                                if let appSettings = URL(string: UIApplicationOpenSettingsURLString) {
                                                                    UIApplication.shared.openURL(appSettings)
                                                                }
                                                            }))
                                                            
                                                            self.present(alert, animated: true, completion: nil)
                                                        }
                                                        else
                                                        {
                                                            
                                                        }
            })
        case .authorized:
            // go ahead
            
            
            break
        case .denied, .restricted:
            
            print("user denied access of camera")
            
            //show alert
            let alert = UIAlertController(title: "", message: alertMsg_AudioAccess, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: alertBtnTitle_OK, style: UIAlertActionStyle.default, handler:{ (UIAlertAction) -> Void in
                
                if let appSettings = URL(string: UIApplicationOpenSettingsURLString) {
                    UIApplication.shared.openURL(appSettings)
                }
            }))
            
            self.present(alert, animated: true, completion: nil)
            
            break
            // the user explicitly denied camera usage or is not allowed to access the camera devices
        }
    }
    
    func startCamera(){
        
        
        let cameraToggle = view.viewWithTag(1)
        let slider = view.viewWithTag(2)
        let duration = view.viewWithTag(3)
        
        cameraEngine.startup()
        
        let bb = CGRect(x: 0.0, y: 64.0,width: UIScreen.main.bounds.width,height: UIScreen.main.bounds.width)
        let videoLayer = AVCaptureVideoPreviewLayer(session: cameraEngine.captureSession)
        videoLayer.frame = bb
        videoLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        view.layer.addSublayer(videoLayer)
        
        view.addSubview(cameraToggle!)
        view.addSubview(slider!)
        view.addSubview(duration!)
    }
    
    fileprivate func deviceInputFromDevice(_ device: AVCaptureDevice?) -> AVCaptureDeviceInput? {
        guard let validDevice = device else { return nil }
        do {
            return try AVCaptureDeviceInput(device: validDevice)
        } catch let outError {
            printCustom("Device setup error occured \(outError)")
            return nil
        }
    }

    // MARK: - Update View According To Selected Theme
    
    func updateViewAccordingToTheme() {
        
        btnCaptureVideo.setImage(Utilities().themedImage(img_videoPlay), for: UIControlState())
        
        btnCancel.setImage(Utilities().themedImage(img_cancelBtm), for: UIControlState())
        btnCancel.setImage(Utilities().themedImage(img_cancelBtmSelected), for: .highlighted)
        btnCancel.setImage(Utilities().themedImage(img_cancelBtmSelected), for: .selected)
        
        btnRedo.setImage(Utilities().themedImage(img_saveBtm), for: UIControlState())
        btnRedo.setImage(Utilities().themedImage(img_saveBtmSelected), for: .highlighted)
        btnRedo.setImage(Utilities().themedImage(img_saveBtmSelected), for: .selected)
        
        
        sliderRecording.minimumTrackTintColor = Utilities().themedMultipleColor()[0]
        sliderRecording.maximumTrackTintColor = UIColor.gray
    }
    
    //MARK:- Capture video
    
    @IBAction func captureVideo(_ sender: AnyObject) {
        
        if isRecording == true
        {
            stopVideo()
        }
        else
        {
            isRecording = true
            
            sliderRecording.value = currentRecordingTime
            
            btnCaptureVideo.setImage(Utilities().themedImage(img_audioStop), for: UIControlState())
            
            lblTime.text =  NSString(format: "00:00/00:%d", Int(maxVideoRecordingTime)) as String
            
            timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(CaptureVideo.recordingProgress), userInfo: nil, repeats: true)
            
            if !cameraEngine.isCapturing {
                
                if FileManager.default.fileExists(atPath: tempFilePath.path) {
                    do {
                        try FileManager.default.removeItem(atPath: tempFilePath.path)
                    } catch { }
                }
                
                btnRedo.isEnabled = false
                cameraEngine.start()
            }
        }
    }
    
    func recordingProgress(){
        
        currentRecordingTime = currentRecordingTime + 1
        
        if(currentRecordingTime == Float(maxVideoRecordingTime))
        {
            stopVideo()
        }
        else
        {
            var strCurrent = String()
            let current = Int(currentRecordingTime)
            if current < 10
            {
                strCurrent = NSString(format: "00:0%d/00:%d", current, Int(maxVideoRecordingTime)) as String
            }
            else
            {
                strCurrent = NSString(format: "00:%d/00:%d", current, Int(maxVideoRecordingTime)) as String
            }
            
            lblTime.text = strCurrent
            
            sliderRecording.value = currentRecordingTime
        }
    }
    
    //MARK:- Stop capturing video
    
    func stopVideo(){
        
        if cameraEngine.isCapturing {
            
            btnRedo.isEnabled = false
            cameraEngine.captureVideo = self
            cameraEngine.stop()
        }
        
        timer?.invalidate()
        
        btnCaptureVideo.setImage(Utilities().themedImage(img_videoPlay), for: UIControlState())
        
        isRecording = false
    
        sliderRecording.value = currentRecordingTime
        
        var strCurrent = String()
        let current = Int(currentRecordingTime)
        if current < Int(maxVideoRecordingTime)
        {
            if current < 10
            {
                strCurrent = NSString(format: "00:0%d/00:%d", current, Int(maxVideoRecordingTime)) as String
            }
            else
            {
                strCurrent = NSString(format: "00:%d/00:%d", current, Int(maxVideoRecordingTime)) as String
            }
        }
        else
        {
            strCurrent = NSString(format: "00:%d/00:%d", current, Int(maxVideoRecordingTime)) as String
        }
        
        lblTime.text = strCurrent
        
        currentRecordingTime = 0.0
        
    }
    
    //This is callback method getting called from cameraEngine class
    func processThumbnail(){
        
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
        let documentsDirectory = paths.first! as String
        let dataPath1 = documentsDirectory + "/thumbnailVideo.png"
        
        DispatchQueue.main.async(execute: { () -> Void in
            
            if FileManager().fileExists(atPath: dataPath1)
            {
                self.btnRedo.isEnabled = true
                self.videoThumbnail = UIImage(contentsOfFile: dataPath1)!
            }
            else
            {
                let alert = UIAlertController(title: "", message: String(format: "Error in generating video thumbnail. Please record again."), preferredStyle: UIAlertControllerStyle.alert)
                
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            }
            
        })
    }
    
    //MARK:- Toggle camera
    
    @IBAction func cameraToggle(_ sender: AnyObject) {
        
        cameraEngine.switchCamera()
    }

    @IBAction func cancelClick(_ sender: AnyObject) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- OK button will navigate to next screen
    @IBAction func redoClick(_ sender: AnyObject){
        
        if cameraEngine.isCapturing {
            cameraEngine.stop()
        }
        
        timer?.invalidate()
        
        isRecording = false
        
        currentRecordingTime = 0.0
        
        sliderRecording.value = currentRecordingTime
        
        btnCaptureVideo.setImage(Utilities().themedImage(img_videoPlay), for: UIControlState())
        
        lblTime.text =  NSString(format: "00:00/00:%d", Int(maxVideoRecordingTime)) as String
        
        if FileManager.default.fileExists(atPath: tempFilePath.path) {
         
            let st = UIStoryboard(name: "Digital", bundle:nil)
            let obj = st.instantiateViewController(withIdentifier: "VideoFilterController") as! VideoFilterController
            
            obj.sampleURL = tempFilePath
            
            obj.navigationController?.navigationBar.isHidden = false
            obj.updatedPhoto = videoThumbnail
            obj.isComingFromPeepTextFlow = isComingFromPeepTextFlow!
            obj.isComingFromEventsFlow = isComingFromEventsFlow!
            obj.strPeepText = peepTextConstant
            obj.delegate = delegate
            self.navigationController?.pushViewController(obj, animated: true)
        }
        else{
            
            let alert = UIAlertController(title: "", message: String(format: "Please record video..."), preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }

    //MARK:- Set constraints
    
    func setConstraint(){
        
        let bb = UIScreen.main.bounds.width - UIScreen.main.bounds.height/2
        
        var aa = (UIScreen.main.bounds.height-40 - UIScreen.main.bounds.width)/2
        aa = aa + bb
        
        let constraints = self.view.constraints
        let count = constraints.count
        var index = 0;
        
        while (index < count)
        {
            let constraint = constraints[index]
            
            if constraint.identifier == "captureVideo" {
                
                let height = btnCaptureVideo.frame.size.width
                
                constraint.constant = aa + height/2
            }
            
            if constraint.identifier == "sliderY" {
                
                constraint.constant = bb + 64
            }
            
            if constraint.identifier == "lblY" {
                
                constraint.constant = bb + 64 - 20
            }
            
            index += 1;
        }
    }
}
