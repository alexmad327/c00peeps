//
//  VideoPublishController.m
//  C00Peeps
//
//  Created by OSX on 14/09/16.
//  Copyright © 2016 SOTSYS011. All rights reserved.
//

#import "VideoPublishController.h"
#import "C00Peeps-Swift.h"

@interface VideoPublishController (){
    
    NSString *selFilterName;
    NSString *strDefaultText;
    NSInteger lastSelIndexPath;
    bool bWritingFile;
    bool bKeyboardUp;
    UIButton *btnNext;
    LBVideoOrientation orientation;
    UIImage *imgBrighnessSelected;
    UIImage *imgBackArrow;
}

@end


@implementation VideoPublishController

@synthesize typeFilter;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    strDefaultText = @"Write a caption ...100 characters limit";
    
    bWritingFile = false;
    
    if(self.isComingFromPeepTextFlow)
    {
        _txtView.text = _strPeepText;
        _txtView.userInteractionEnabled = false;
        lblCategory.hidden = TRUE;
        btnCategory.hidden = TRUE;
        layoutTxtViewToCategoryLabel.priority = 900;
        layoutTxtViewToCustomView.priority = 1000;
    }
    else
    {
        _txtView.text = strDefaultText;
        _txtView.userInteractionEnabled = true;
        lblCategory.hidden = FALSE;
        btnCategory.hidden = FALSE;
        layoutTxtViewToCategoryLabel.priority = 1000;
        layoutTxtViewToCustomView.priority = 900;
    }
    
    Utilities *utilities = [[Utilities alloc] init];
    imgBrighnessSelected = [utilities themedImage:[ImageConstants img_brighnessSelectedObjc]];
    imgBackArrow = [utilities themedImage:[ImageConstants img_backArrowObjc]];
    
    
}

- (void) viewDidUnload{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)loadViews
{
    
    CGRect cameraFrame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.width);
    
    UIView* viewCamera = [[UIView alloc] initWithFrame:cameraFrame];
    viewCamera.backgroundColor = [UIColor redColor];
    
    filterView = [[GPUImageView alloc] initWithFrame:cameraFrame];
    filterView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
    [filterView setFillMode:kGPUImageFillModePreserveAspectRatioAndFill];
    
    viewCamera = filterView;
    
    [self.customView addSubview:viewCamera];
    
    //filterView1
    
    filterView1 = [[GPUImageView alloc] initWithFrame:cameraFrame];
    filterView1.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
    [filterView1 setFillMode:kGPUImageFillModePreserveAspectRatioAndFill];
    
    viewCamera = filterView1;
    
    [self.customView addSubview:viewCamera];
    
    //filterView2
    
    filterView2 = [[GPUImageView alloc] initWithFrame:cameraFrame];
    filterView2.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
    [filterView2 setFillMode:kGPUImageFillModePreserveAspectRatioAndFill];
    
    viewCamera = filterView2;
    
    [self.customView addSubview:viewCamera];
    
    //filterView3
    
    filterView3 = [[GPUImageView alloc] initWithFrame:cameraFrame];
    filterView3.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
    [filterView3 setFillMode:kGPUImageFillModePreserveAspectRatioAndFill];
    
    viewCamera = filterView3;
    
    [self.customView addSubview:viewCamera];
    
    //filterView4
    
    filterView4 = [[GPUImageView alloc] initWithFrame:cameraFrame];
    filterView4.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
    [filterView4 setFillMode:kGPUImageFillModePreserveAspectRatioAndFill];
    
    viewCamera = filterView4;
    
    [self.customView addSubview:viewCamera];
    
    //Apply filter
    [self setFilter:self.typeFilter];
    
    [self applyFilter];
    
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:TRUE];
    
    UIBarButtonItem *btn = [[UIBarButtonItem alloc] initWithImage:imgBackArrow style:UIBarButtonItemStylePlain target:self action:@selector(backButtonAction)];
    self.navigationItem.leftBarButtonItem = btn;
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Next" style:UIBarButtonItemStylePlain target:self action:@selector(next)];
    
    self.tabBarController.tabBar.hidden = false;
    
    [self setExtendedLayoutIncludesOpaqueBars:YES];
    
    lastSelIndexPath = 0;
    
    self.title = @"Video";
    
    [self performSelector:@selector(loadViews) withObject:nil afterDelay:0.2];
}

-(void)next{
    
    if(bKeyboardUp)
    {
        [_txtView resignFirstResponder];
    
    }
    else
    {
        if(!_isComingFromPeepTextFlow && [lblCategory.text isEqualToString: @"Choose a Category"])
        {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Please Select Category" preferredStyle:UIAlertControllerStyleAlert];
            
            [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
            }]];
            
            [self presentViewController:alertController animated:YES completion:nil];
        }
        else
        {
            if ([_txtView.text isEqualToString:strDefaultText])
            {
                _txtView.text = @"";
            }
            
            UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Digital" bundle:[NSBundle mainBundle]];
            ContactsScreen *obj = (ContactsScreen*)[sb instantiateViewControllerWithIdentifier:@"ContactsScreen"];
            obj.isComingFromPeepTextFlow = self.isComingFromPeepTextFlow;
            obj.dataPath = [_sampleURL path];
            obj.mediacaption = _txtView.text;
            obj.mediaCategoryId = self.mediaCategoryId;
            
            //if no filter has applied in VideoFilterController.m then we will upload movie file (tempMovie.mp4) from tmp folder
            if (typeFilter == GPUIMAGE_NONE)
            {
                obj.filterApplied = false;
            }
            else
            {
                obj.filterApplied = true;
            }
            
            NSData *data = UIImageJPEGRepresentation(_updatedPhoto, 0.5);
            obj.imageData = data;
            
            [self.navigationController pushViewController:obj animated:YES];
        }
    }
}

-(void) backButtonAction{
    
    //clean all target
    [self cleanTargets];
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark- Apply Filter

- (void) applyFilter{
    
    AVAsset *asset = [AVAsset assetWithURL:_sampleURL];
    orientation = [asset videoOrientation];
    
    movieFile = [[GPUImageMovie alloc] initWithURL:_sampleURL];
    
    movieFile.runBenchmark = YES;
    movieFile.playAtActualSpeed = YES;
    movieFile.shouldRepeat = YES;
    movieFile.playSound = YES;
    
    [self addRemoveWatermarkFilter];
    
    [movieFile  startProcessing];
}

- (void) addRemoveWatermarkFilter{
    
    if(_bWatermarkAdded)
    {
        [self addMultipleFiltersChain];
        
        if(bWritingFile)
        {
            [blendFilter addTarget:movieWriter];
        }
    }
    else
    {
        [self addMultipleFiltersChain];
        
        if(bWritingFile)
        {
            if(filter)
                [filter addTarget:movieWriter];
            else
                [saturationFilter addTarget:movieWriter];
        }
    }
}

- (void) correctVideoOrientation{
    
    if (orientation == LBVideoOrientationUp)
    {
        NSLog(@"Up"); //this is the orientation after recording the video
        
        [brightnessFilter setInputRotation:kGPUImageRotateRight atIndex:0];
    }
    else if (orientation == LBVideoOrientationDown)
    {
        NSLog(@"Down");
    }
    else if (orientation == LBVideoOrientationLeft)
    {
        NSLog(@"Left");
    }
    else if (orientation == LBVideoOrientationRight)
    {
        NSLog(@"Right"); //this is the orientation after picking existing video from photos
    }
}

- (void) addMultipleFiltersChain{
    
    groupFilter = [[GPUImageFilterGroup alloc]init]; //1
    
    brightnessFilter = [[GPUImageBrightnessFilter alloc] init]; //2
    contrastFilter = [[GPUImageContrastFilter alloc] init]; //3
    saturationFilter = [[GPUImageSaturationFilter alloc] init]; //4
    
    [(GPUImageBrightnessFilter*)brightnessFilter setBrightness:_brightnessVal]; // change value between -1.00 to 1.00
    [(GPUImageContrastFilter*)contrastFilter setContrast:_contrastVal];     // change value between 0.00 to 4.00
    [(GPUImageSaturationFilter*)saturationFilter setSaturation:_saturationVal];   //change value between 0.00 to 2.00
    
    
    [self correctVideoOrientation];
    
    
    [(GPUImageFilterGroup *)groupFilter addFilter:brightnessFilter];
    [(GPUImageFilterGroup *)groupFilter addFilter:contrastFilter];
    [(GPUImageFilterGroup *)groupFilter addFilter:saturationFilter];
    
    if(filter)
        [(GPUImageFilterGroup *)groupFilter addFilter:filter];
    
    [movieFile addTarget:brightnessFilter];
    [brightnessFilter addTarget:contrastFilter];
    [contrastFilter addTarget:saturationFilter];
    
    if(filter)
        [saturationFilter addTarget:filter];
    
    [(GPUImageFilterGroup *) groupFilter setInitialFilters:[NSArray arrayWithObject: brightnessFilter]];
    
    if(filter)
        [(GPUImageFilterGroup *) groupFilter setTerminalFilter:filter];
    else
        [(GPUImageFilterGroup *) groupFilter setTerminalFilter:saturationFilter];
    
    
    [brightnessFilter forceProcessingAtSize:filterView.sizeInPixels];
    [contrastFilter forceProcessingAtSize:filterView1.sizeInPixels];
    [saturationFilter forceProcessingAtSize:filterView2.sizeInPixels];
    if(filter)
    {
        //filterView3
        
        CGRect cameraFrame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.width);
        
        UIView* viewCamera = [[UIView alloc] initWithFrame:cameraFrame];
        viewCamera.backgroundColor = [UIColor redColor];
        
        filterView3 = [[GPUImageView alloc] initWithFrame:cameraFrame];
        filterView3.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
        [filterView3 setFillMode:kGPUImageFillModePreserveAspectRatioAndFill];
        
        viewCamera = filterView3;
        
        [self.customView addSubview:viewCamera];
        
        [filter forceProcessingAtSize:filterView3.sizeInPixels];
        
    }
    
    [brightnessFilter addTarget:filterView];
    [contrastFilter addTarget:filterView1];
    [saturationFilter addTarget:filterView2];
    if(filter)
        [filter addTarget:filterView3];
    
    if(_bWatermarkAdded)
    {
        blendFilter = [[GPUImageAlphaBlendFilter alloc] init];
        blendFilter.mix = 1.0;
        [(GPUImageFilterGroup *)groupFilter addFilter:blendFilter];
        [(GPUImageFilterGroup *) groupFilter setTerminalFilter:blendFilter];
        
        //filterView3
        
        CGRect cameraFrame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.width);
        
        UIView* viewCamera = [[UIView alloc] initWithFrame:cameraFrame];
        viewCamera.backgroundColor = [UIColor redColor];
        
        filterView4 = [[GPUImageView alloc] initWithFrame:cameraFrame];
        filterView4.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
        [filterView4 setFillMode:kGPUImageFillModePreserveAspectRatioAndFill];
        
        viewCamera = filterView4;
        
        [self.customView addSubview:viewCamera];
        
        [blendFilter forceProcessingAtSize:filterView4.sizeInPixels];
        
        //add blend filter
        [blendFilter addTarget:filterView4];
        
        CGRect cameraFrame1 = _customView.frame;
        UIView *contentView = [[UIView alloc] initWithFrame:cameraFrame1];
        contentView.backgroundColor = [UIColor clearColor];
        
        UIImageView *ivTemp = [[UIImageView alloc] initWithFrame:CGRectMake(20, 20, 40, 40)];
        ivTemp.image = imgBrighnessSelected; // self.selectedWatermarkImage;
        [contentView addSubview:ivTemp];
        
        uiElementInput = [[GPUImageUIElement alloc] initWithView:contentView];
        [uiElementInput setShouldSmoothlyScaleOutput:YES];
        
        if(filter)
        {
            [filter addTarget:blendFilter];
            [uiElementInput addTarget:blendFilter];
            
            __weak GPUImageUIElement *weakElement = uiElementInput;
            [filter setFrameProcessingCompletionBlock:^(GPUImageOutput * filter, CMTime frameTime){
                
                [weakElement update];
            }];
        }
        else
        {
            [saturationFilter addTarget:blendFilter];
            [uiElementInput addTarget:blendFilter];
            
            
            __weak GPUImageUIElement *weakElement = uiElementInput;
            [saturationFilter setFrameProcessingCompletionBlock:^(GPUImageOutput * filter, CMTime frameTime){
                
                [weakElement update];
            }];
        }
    }
}

#pragma mark- Clean Target

- (void) cleanTargets{
    
    if(blendFilter)
    {
        [blendFilter removeTarget:filterView];
        [blendFilter removeAllTargets];
        blendFilter = nil;
        
        [uiElementInput removeAllTargets];
        uiElementInput = nil;
    }
    
    if(filter)
    {
        [filter removeAllTargets];
        filter = nil;
    }
    
    [groupFilter removeAllTargets];
    groupFilter = nil;
    
    [brightnessFilter removeAllTargets];
    brightnessFilter = nil;
    
    [saturationFilter removeAllTargets];
    saturationFilter = nil;
    
    [contrastFilter removeAllTargets];
    contrastFilter = nil;
    
    [movieFile removeAllTargets];
    [movieFile endProcessing];
    movieFile = nil;
}

#pragma mark- Set Filter

- (void) setFilter:(GPUImageShowcaseFilterType)type{
    
    filterType = type;
    
    switch (type)
    {
            
        case GPUIMAGE_SEPIA:
        {
            filter = [[GPUImageSepiaFilter alloc] init];
        }; break;
        case GPUIMAGE_GRAYSCALE:
        {
            filter = [[GPUImageGrayscaleFilter alloc] init];
        }; break;
        case GPUIMAGE_COLORINVERT:
        {
            filter = [[GPUImageColorInvertFilter alloc] init];
        }; break;
        case GPUIMAGE_BRIGHTNESS:
        {
            GPUImageBrightnessFilter *brightFilter = [[GPUImageBrightnessFilter alloc] init];
            brightFilter.brightness = 0.0;
            filter = brightFilter;
            brightFilter = nil;
        }break;
            
        case GPUIMAGE_SHARPEN:
        {
            filter = [[GPUImageSharpenFilter alloc] init];
        }   break;
            
        case GPUIMAGE_VIGNETTE:
        {
            filter = [[GPUImageVignetteFilter alloc] init];
        }   break;
            
        case GPUIMAGE_SATURATION:
        {
            filter = [[GPUImageSaturationFilter alloc] init];
        }   break;
            
        case GPUIMAGE_CONTRAST:
        {
            filter = [[GPUImageContrastFilter alloc] init];
        }   break;
        case GPUIMAGE_GAMMA:
        {
            GPUImageGammaFilter *gammaFilter = [[GPUImageGammaFilter alloc] init];
            gammaFilter.gamma = 2.0;
            filter = gammaFilter;
            gammaFilter = nil;
            
        }   break;
        case GPUIMAGE_HUE:
        {
            GPUImageHueFilter *hueFilter = [[GPUImageHueFilter alloc] init];
            hueFilter.hue = 20;
            filter = hueFilter;
            hueFilter = nil;
            
        }   break;
        case GPUIMAGE_OPACITY:
        {
            GPUImageOpacityFilter *opacityFilter = [[GPUImageOpacityFilter alloc] init];
            opacityFilter.opacity = 0.8;
            filter = opacityFilter;
            opacityFilter = nil;
            
        }   break;
        case GPUIMAGE_WHITEBALANCE:
        {
            GPUImageWhiteBalanceFilter *whiteBalanceFilter = [[GPUImageWhiteBalanceFilter alloc] init];
            whiteBalanceFilter.temperature = 7000;
            whiteBalanceFilter.tint = 0;
            filter = whiteBalanceFilter;
            whiteBalanceFilter = nil;
            
        }   break;
        case GPUIMAGE_LUMINANCE_RANGE:
        {
            GPUImageLuminanceThresholdFilter *luminanceFilter = [[GPUImageLuminanceThresholdFilter alloc] init];
            //luminanceFilter.threshold = 0.5;
            filter = luminanceFilter;
            luminanceFilter = nil;

        }   break;
        case GPUIMAGE_HALFTONE:
        {
            filter = [[GPUImageHalftoneFilter alloc] init];
        }   break;
            
    }
    
}

- (void) setImagesAsPerSelectedTheme{
    
}

#pragma mark- Select Category

-(IBAction)selectCategory:(id)sender{
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        // Cancel button tappped do nothing.
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Movie" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        
        lblCategory.text = @"Movie";
        self.mediaCategoryId = 2;
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Music" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        
        lblCategory.text = @"Music";
        self.mediaCategoryId = 1;
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Video Games" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        
        lblCategory.text = @"Video Games";
        self.mediaCategoryId = 4;
        
    }]];
    
    //in case of video or music need to hide photo category
    /*[actionSheet addAction:[UIAlertAction actionWithTitle:@"Photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        lblCategory.text = @"Photo";
        self.mediaCategoryId = 3;
        
    }]];*/
    
    // Present action sheet.
    [self presentViewController:actionSheet animated:YES completion:nil];
}

-(void)viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:animated];
    
    [self cleanTargets];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark- TextView Delegate Methods

- (void)textViewDidBeginEditing:(UITextView *)textView{

    if ([_txtView.text isEqualToString:strDefaultText])
    {
        _txtView.text = @"";
    }
    
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    
    if ([textView.text length] > 99)
    {
        return false;
    }
    
    return true;
}

@end
