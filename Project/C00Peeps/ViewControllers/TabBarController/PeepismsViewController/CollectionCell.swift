//
//  CollectionCell.swift
//  PhotoVideoFilter
//
//  Created by Mac on 5/09/16.
//  Copyright © 2016 Gagandeep Bawa. All rights reserved.
//

import UIKit

class CollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblFilterName: UILabel!
    
}
