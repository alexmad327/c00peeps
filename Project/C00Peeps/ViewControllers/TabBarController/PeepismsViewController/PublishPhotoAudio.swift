//
//  PublishPhotoAudio.swift
//  PhotoVideoFilter
//
//  Created by Mac on 5/09/16.
//  Copyright © 2016 Gagandeep Bawa. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation


class PublishPhotoAudio: BaseVC, AVAudioPlayerDelegate, UITextViewDelegate {
    
    @IBOutlet weak var photoView: UIImageView!
    @IBOutlet weak var btnPlayAudio: UIButton!
    @IBOutlet weak var btnSelectCategory: UIButton!
    @IBOutlet weak var lblCategoryName: UILabel!
    @IBOutlet weak var txtViewCaption: UITextView!
    @IBOutlet weak var scroll:UIScrollView?
    
    @IBOutlet weak var captionTop: NSLayoutConstraint!
    @IBOutlet weak var captioTopFromPhotoView: NSLayoutConstraint!
    
    var imagePhoto: UIImage!
    var audioPlayer: AVAudioPlayer?
    var btn:UIBarButtonItem?
    var isKeyboardShown:Bool?
    var dataPath:String?
    var isComingFromPeepTextFlow:Bool?
    
    override func viewDidAppear(_ animated: Bool) {
        
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        self.tabBarController?.tabBar.isHidden = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "Publish"
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Next", style: UIBarButtonItemStyle.plain, target: self, action: #selector(PublishPhotoAudio.nextPhoto))
   
        isKeyboardShown = false
        
        photoView.image = imagePhoto
        
        txtViewCaption.text = alertMsg_PublishMediaDefaultText
        
        if isComingFromPeepTextFlow == true{
            
            updateConstraint()
        }
        
        if FileManager.default.fileExists(atPath: dataPath!) == false{
            
            btnPlayAudio.isHidden = true
        }
        else
        {
            btnPlayAudio.isHidden = false
        }
        
        mediaCategory = MediaCategory.none
        
        // Update View According To Selected Theme
        self.updateViewAccordingToTheme()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Update View According To Selected Theme
    func updateViewAccordingToTheme() {
        btnPlayAudio.setImage(Utilities().themedImage(img_videoPlay), for: UIControlState())
    }
    
    func updateConstraint(){
        
        lblCategoryName.isHidden = true
        btnSelectCategory.isHidden = true
        
        captioTopFromPhotoView.priority = UILayoutPriority(rawValue: 1000.0)
        captionTop.priority = UILayoutPriority(rawValue: 900.0)
        
        txtViewCaption.text = peepTextConstant
        txtViewCaption.isUserInteractionEnabled = false
    }
    
    //MARK:- Select Category
    
    @IBAction func selectCategory(_ sender: AnyObject) {
        
        //Create the AlertController
        let actionSheetController: UIAlertController = UIAlertController(title: alertMsg_SelectCategory, message: "", preferredStyle: .actionSheet)
        
        //Create and add the Cancel action
        let cancelAction: UIAlertAction = UIAlertAction(title: alertBtnTitle_Cancel, style: .cancel) { action -> Void in
            //Just dismiss the action sheet
        }
        actionSheetController.addAction(cancelAction)
        
        /*//Create and add first option action - only required when sending video.
         let takePictureAction: UIAlertAction = UIAlertAction(title: alertMsg_Movie, style: .Default) { action -> Void in
         
         mediaCategory = MediaCategory.Movie
         self.lblCategoryName.text = alertMsg_Movie
         }
         actionSheetController.addAction(takePictureAction)*/
        
        if FileManager.default.fileExists(atPath: dataPath!) == false {//Audio does not exist - show photo category
            //Create and add a fourth option action
            let choosePicture: UIAlertAction = UIAlertAction(title: alertMsg_Photo, style: .default) { action -> Void in
                
                mediaCategory = MediaCategory.photo
                self.lblCategoryName.text = alertMsg_Photo
            }
            actionSheetController.addAction(choosePicture)
        }
        else {//Audio exists - show music and video categories
            //Create and add a second option action
            let choosePictureAction: UIAlertAction = UIAlertAction(title: alertMsg_Music, style: .default) { action -> Void in
                
                mediaCategory = MediaCategory.music
                self.lblCategoryName.text = alertMsg_Music
            }
            actionSheetController.addAction(choosePictureAction)
            
            /*//Create and add a third option action
            let chooseVideoGames: UIAlertAction = UIAlertAction(title: alertMsg_VideoGames, style: .Default) { action -> Void in
                mediaCategory = MediaCategory.VideoGame
                self.lblCategoryName.text = alertMsg_VideoGames
            }
            actionSheetController.addAction(chooseVideoGames)*/
        }
        
        //Present the AlertController
        self.present(actionSheetController, animated: true, completion: nil)
    }
    
    //MARK:- Move Next
    
    func nextPhoto(){
        
        //Using this button as Done button to hide keyboard
        if isKeyboardShown == true{
           
            isKeyboardShown = false
            
            txtViewCaption.resignFirstResponder()
            
            for vi in self.headerView.subviews
            {
                if vi.isKind(of: UIButton.self)
                {
                    let btn = vi as! UIButton
                    
                    if btn.tag == 201
                    {
                        let multipleAttributes = [
                            NSAttributedStringKey.foregroundColor: UIColor.white,
                            NSAttributedStringKey.font: ProximaNovaSemibold(15)]
                        let myAttrString = NSAttributedString(string: "Next", attributes: multipleAttributes)
                        btn.setAttributedTitle(myAttrString, for: UIControlState())
                        //btn.removeFromSuperview()
                    }
                }
            }
        }
        else
        {
            
            if isComingFromPeepTextFlow == false && mediaCategory == MediaCategory.none
            {
                let ac = UIAlertController(title: "", message: alertMsg_SelectCategory, preferredStyle: .alert)
                ac.addAction(UIAlertAction(title: alertBtnTitle_OK, style: .default, handler: nil))
                self.present(ac, animated: true, completion: nil)
            }
            else
            {
                if txtViewCaption.text == alertMsg_PublishMediaDefaultText
                {
                    txtViewCaption.text = ""
                }
                
                //Move to Contacts screen
                
                let img = Utilities().cropToBounds(photoView.image!, cgwidth: UIScreen.main.bounds.width, cgheight: UIScreen.main.bounds.width)
                
                printCustom("width: \(img.size.width) height \(img.size.height)")
                
                let data: Data = UIImageJPEGRepresentation(img, 0.5)!
                
                let st = UIStoryboard(name: "Digital", bundle:nil)
                let obj = st.instantiateViewController(withIdentifier: "ContactsScreen") as! ContactsScreen
                
                //image data
                obj.imageData = data
                
                obj.isComingFromPeepTextFlow = isComingFromPeepTextFlow!
                
                //audio file path
                obj.dataPath = dataPath
                obj.mediacaption = txtViewCaption.text
                self.navigationController?.pushViewController(obj, animated: true)

            }
        }
    }
    
    func cancelView(){
        
        //lockUnlockView!.removeFromSuperview()
    }
    
    //MARK:- Play audio file
    
    func playAudioRecording(){
        
        if audioPlayer?.isPlaying == true
        {
            //change image of play button
            
            btnPlayAudio.setImage(Utilities().themedImage(img_videoPlay), for: UIControlState())
            
            audioPlayer?.stop()
        }
        else
        {
            //change image of play button
            
            btnPlayAudio.setImage(Utilities().themedImage(img_audioPlay), for: UIControlState())
            
            let url = URL(fileURLWithPath: dataPath!)
            
            do{
                audioPlayer = try AVAudioPlayer(contentsOf: url)
                
                audioPlayer?.delegate = self
                
                audioPlayer?.play()
            }
            catch{
                
                
            }
        }
        
    }
    
    @IBAction func playAudio(_ sender: AnyObject) {
        
        playAudioRecording()
    }
    
    //MARK:- Audio Play Finish delegate
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool)
    {
        //change image of play button
        
        btnPlayAudio.setImage(Utilities().themedImage(img_videoPlay), for: UIControlState())
    }
    
    //MARK:- UITextView delegate methods
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool{
        
        if (txtViewCaption.text == alertMsg_PublishMediaDefaultText)
        {
            txtViewCaption.text = ""
        }
        
        return true
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        
        let numberOfChars = newText.characters.count // for Swift use count(newText)
        
        return numberOfChars < 100;
    }
}
