//
//  VideoFilterController.h
//  Speazie
//
//  Created by OSX on 28/07/16.
//
//

#import <UIKit/UIKit.h>
//#import <GPUImage/GPUImage.h>
#import "GPUImage.h"
#import <AssetsLibrary/ALAssetsLibrary.h>
#import <CoreImage/CoreImage.h>
#import <CoreImage/CIFilter.h>
#import "AVAsset+VideoOrientation.h"

@protocol CreateEventSecondControllerDelegate;

typedef enum {
    
    GPUIMAGE_NONE = 0,
    GPUIMAGE_BRIGHTNESS = 1,
    GPUIMAGE_GRAYSCALE = 2,
    GPUIMAGE_COLORINVERT = 3,
    GPUIMAGE_SHARPEN = 5,
    GPUIMAGE_SATURATION = 6,
    GPUIMAGE_SEPIA = 7,
    GPUIMAGE_VIGNETTE = 8,
    GPUIMAGE_CONTRAST = 9,
    GPUIMAGE_ORIGINAL = 10,
    GPUIMAGE_GAMMA = 11,
    GPUIMAGE_HUE = 12,
    GPUIMAGE_OPACITY = 13,
    GPUIMAGE_WHITEBALANCE = 14,
    GPUIMAGE_LUMINANCE_RANGE = 15,
    GPUIMAGE_HALFTONE = 16
    
} GPUImageShowcaseFilterType;

@interface VideoFilterController  : UIViewController <UICollectionViewDataSource,UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UIGestureRecognizerDelegate> {
    
    GPUImageMovie *movieFile;
    GPUImageUIElement *uiElementInput;
   
    GPUImageFilterGroup *groupFilter;
    GPUImageOutput<GPUImageInput> *brightnessFilter;
    GPUImageOutput<GPUImageInput> *contrastFilter;
    GPUImageOutput<GPUImageInput> *saturationFilter;
    GPUImageAlphaBlendFilter *blendFilter;
    GPUImageMovieWriter *movieWriter;
    NSTimer * timer;
    GPUImageShowcaseFilterType filterType;
    GPUImageShowcaseFilterType baseFilterType;
    float brightnessVal;
    float saturationVal;
    float contrastVal;
    GPUImageView *filterView ;
    GPUImageView *filterView1 ;
    GPUImageView *filterView2 ;
    GPUImageView *filterView3 ;
    GPUImageView *filterView4 ;
    UIView* coverView;
    
    __weak IBOutlet NSLayoutConstraint *colletionY;
    __weak IBOutlet NSLayoutConstraint *sliderY;
    __weak IBOutlet NSLayoutConstraint *cropY;
    __weak IBOutlet NSLayoutConstraint *brightnessY;
    __weak IBOutlet NSLayoutConstraint *rotateY;
    
    __weak IBOutlet UIActivityIndicatorView *indicator;

    bool bEditVideo;
    
    IBOutlet UINavigationBar *headerBar;
    __weak IBOutlet UIButton *btnCrop;
    __weak IBOutlet UIButton *btnBrightness;
    __weak IBOutlet UIButton *btnRotate;
    __weak IBOutlet UIButton *btnFilter;
    __weak IBOutlet UIButton *btnEdit;
    __weak IBOutlet UISlider *slider;
    __weak IBOutlet UILabel *lblContrast;
    __weak IBOutlet UILabel *lblBrightness;
    __weak IBOutlet UILabel *lblSaturation;
     //__weak IBOutlet UIButton *btnPlay;
}

@property (strong, nonatomic)  GPUImageOutput<GPUImageInput> *filter;
@property (strong, nonatomic) NSString *strPeepText;
@property (strong, nonatomic) UIImage *selectedWatermarkImage;
@property (strong, nonatomic) UIImageView *watermarkImageView;

@property (strong, nonatomic) IBOutlet UICollectionView *collPresets;
@property (strong, nonatomic) IBOutlet UIView *customView;
@property (retain, nonatomic) IBOutlet UIButton *btnSaveVideo;
@property (retain, nonatomic) IBOutlet UILabel *progressLabel;
@property (weak, nonatomic) IBOutlet UIView *vwWaitVideoProcessing;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loaderWaitVideoProcessing;
@property (retain, nonatomic)  NSURL *movieURL ;
@property (retain, nonatomic) NSURL *sampleURL ; 
@property (strong, nonatomic) NSString *pathToMovie;
@property (strong, nonatomic) UIImage *updatedPhoto;
@property (nonatomic) bool isComingFromPeepTextFlow;
@property (nonatomic) bool isComingFromEventsFlow;
@property (nonatomic, weak) id <CreateEventSecondControllerDelegate>delegate;
-(void)stopAndGoBackAfterAppResigns;

@end
