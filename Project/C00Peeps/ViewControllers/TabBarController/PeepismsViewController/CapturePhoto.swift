//
//  CapturePhoto.swift
//  PhotoVideoFilter
//
//  Created by Mac on 5/09/16.
//  Copyright © 2016 Gagandeep Bawa. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation

class CapturePhoto: BaseVC, AVCapturePhotoCaptureDelegate {
    
    let captureSession = AVCaptureSession()
    //let stillImageOutput = AVCaptureStillImageOutput()
    let stillImageOutput = AVCapturePhotoOutput()
    var error: NSError?
    var isUsingFrontFacingCamera:Bool?
    var previewLayer:AVCaptureVideoPreviewLayer? = nil
    var cameraPreview:UIView?
    var isComingFromPeepTextFlow:Bool?
    var isComingFromEventsFlow:Bool?

    @IBOutlet weak var camView: UIImageView!
    @IBOutlet weak var btnCapturePhoto: UIButton!
    @IBOutlet weak var btnFocus: UIButton!
    @IBOutlet weak var btnToggle: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var cameraviewBottom: NSLayoutConstraint!
    @IBOutlet weak var toggleViewBottom: NSLayoutConstraint!
    
    var delegate: CreateEventSecondControllerDelegate?

    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
         self.tabBarController?.tabBar.isHidden = true
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
     
        super.viewWillDisappear(animated)
    }
    
    //MARK:- ViewDidLoad
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.title = "Photo"

        self.navigationItem.hidesBackButton = true
        
        updateViewAccordingToTheme()
        
        setConstraint()
        
        let authorizationStatus = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
        switch authorizationStatus {
        case .notDetermined:
            // permission dialog not yet presented, request authorization
            AVCaptureDevice.requestAccess(for: .video,
                                                      completionHandler: { (granted:Bool) -> Void in
                                                        if (granted == false) {
                                                        
                                                            //show alert
                                                            let alert = UIAlertController(title: "", message: alertMsg_CameraAccess, preferredStyle: UIAlertControllerStyle.alert)
                                                            alert.addAction(UIAlertAction(title: alertBtnTitle_OK, style: UIAlertActionStyle.default, handler:{ (UIAlertAction) -> Void in
                                                                
                                                                if let appSettings = URL(string: UIApplicationOpenSettingsURLString) {
                                                                    UIApplication.shared.openURL(appSettings)
                                                                }
                                                            }))
                                                            
                                                            self.present(alert, animated: true, completion: nil)
                                                        }
                                                        else
                                                        {
                                                            DispatchQueue.main.async(execute: { () -> Void in
                                                                
                                                                self.startCamera()
                                                            })
                                                        }
            })
        case .authorized:
        // go ahead
            self.startCamera()
            
            break
        case .denied, .restricted:
            
            print("user denied access of camera")
            
            //show alert
            let alert = UIAlertController(title: "", message: alertMsg_CameraAccess, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: alertBtnTitle_OK, style: UIAlertActionStyle.default, handler:{ (UIAlertAction) -> Void in
                
                if let appSettings = URL(string: UIApplicationOpenSettingsURLString) {
                    UIApplication.shared.openURL(appSettings)
                }
            }))
            
            self.present(alert, animated: true, completion: nil)
            
            break
            // the user explicitly denied camera usage or is not allowed to access the camera devices
        }
    }
    
    //MARK:- Start Camera
    func startCamera(){
        
        let devices = AVCaptureDevice.devices().filter{ ($0 as AnyObject).hasMediaType(AVMediaType.video) && ($0 as AnyObject).position == AVCaptureDevice.Position.back }
        
        if let captureDevice = devices.first  {
            
            do {
                
                try captureSession.addInput(AVCaptureDeviceInput(device: captureDevice))
                
            } catch let error as NSError {
                printCustom("error:\(error)")
            }
            
            let bb = CGRect(x: 0,y: 0,width: UIScreen.main.bounds.width,height: UIScreen.main.bounds.width)
            captureSession.sessionPreset = AVCaptureSession.Preset.photo
            captureSession.startRunning()
            
//            stillImageOutput.outputSettings = [AVVideoCodecKey:AVVideoCodecJPEG]
//            stillImageOutput.isHighResolutionStillImageOutputEnabled = false
//            if captureSession.canAddOutput(stillImageOutput) {
//                captureSession.addOutput(stillImageOutput)
//            }
  
            if captureSession.canAddOutput(stillImageOutput) {
                captureSession.addOutput(stillImageOutput)
            }
            
            previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
            
            previewLayer!.bounds = bb
            
            previewLayer!.position = CGPoint(x: bb.midX, y: bb.midY)
            previewLayer!.videoGravity = AVLayerVideoGravity.resizeAspectFill
            
            cameraPreview = UIView(frame: CGRect(x: 0.0, y: 64.0, width: bb.size.width, height: bb.size.height))
            cameraPreview!.layer.addSublayer(previewLayer!)
            
             let cameraBaselineView = view.viewWithTag(11)
            
            view.addSubview(cameraPreview!)
            view.addSubview(cameraBaselineView!)
        }
    }
    
    // MARK: - Update View According To Selected Theme
    
    func updateViewAccordingToTheme() {
        
        btnCapturePhoto.setImage(Utilities().themedImage(img_clickphoto), for: UIControlState())
        //btnCapturePhoto.setImage(Utilities().themedImage(img_clickphoto), forState: .Highlighted)
        //btnCapturePhoto.setImage(Utilities().themedImage(img_clickphoto), forState: .Selected)
        
        btnFocus.setImage(Utilities().themedImage(img_noflash), for: UIControlState())
        btnFocus.setImage(Utilities().themedImage(img_flash), for: .highlighted)
        btnFocus.setImage(Utilities().themedImage(img_flash), for: .selected)
        
        btnToggle.setImage(Utilities().themedImage(img_flipBtm), for: UIControlState())
        //btnToggle.setImage(Utilities().themedImage(img_flipBtm), forState: .Highlighted)
        //btnToggle.setImage(Utilities().themedImage(img_flipBtm), forState: .Selected)
        
        btnCancel.setImage(Utilities().themedImage(img_cancelBtm), for: UIControlState())
        btnCancel.setImage(Utilities().themedImage(img_cancelBtmSelected), for: .highlighted)
        btnCancel.setImage(Utilities().themedImage(img_cancelBtmSelected), for: .selected)
    }
    
    //MARK:- Set Constraints
    
    func setConstraint(){
        
        let bb = UIScreen.main.bounds.width - UIScreen.main.bounds.height/2
        
        var aa = (UIScreen.main.bounds.height-40 - UIScreen.main.bounds.width)/2
        aa = aa + bb
        
        let ph = UIScreen.main.bounds.height-40 - UIScreen.main.bounds.width - 64
        
        cameraviewBottom.constant = ph
        toggleViewBottom.constant = ph
        
        let constraints = self.view.constraints
        let count = constraints.count
        var index = 0;
        
        while (index < count) {
            
            let constraint = constraints[index]
            
            if constraint.identifier == "capturePhotoPosition" {
                
                let height = btnCapturePhoto.frame.size.width
                
                constraint.constant = aa + height/2

            }
            
            index += 1;
        }
        
    }
    
    @IBAction func capturePhoto(_ sender: AnyObject) {
        
        let settings = AVCapturePhotoSettings()
        
        // doesn't show in the simulator
        #if (!arch(x86_64))

            let previewPixelType = settings.__availablePreviewPhotoPixelFormatTypes.first!
            let previewFormat = [
                kCVPixelBufferPixelFormatTypeKey as String: previewPixelType,
                kCVPixelBufferWidthKey as String: 160,
                kCVPixelBufferHeightKey as String: 160
            ]
            settings.previewPhotoFormat = previewFormat
            stillImageOutput.capturePhoto(with: settings, delegate: self)
            
//            guard let preview = settings.previewPhotoFormat?.first
//            else
//            {
//                //return
//
//            }
//            settings.previewPhotoFormat = [kCVPixelBufferPixelFormatTypeKey as String: preview]
//            stillImageOutput.capturePhoto(with: settings, delegate: self)
        #endif
        
        //if let videoConnection = stillImageOutput.connection(withMediaType: AVMediaTypeVideo) {
            /*
            stillImageOutput.captureStillImageAsynchronously(from: videoConnection) {
                (imageDataSampleBuffer, error) -> Void in
                
                let imageData = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(imageDataSampleBuffer)
                
                var img = UIImage.init(data: imageData!)
                
                img = Utilities().imageResize(img!,sizeChange: CGSize(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.width * (4/3)))
                
                img = Utilities().cropToBounds(img!, cgwidth: UIScreen.main.bounds.width, cgheight: UIScreen.main.bounds.width)
                
                printCustom("width: \(img!.size.width) height \(img!.size.height)")
                
                //Save original photo if setting is ON
                if Utilities().saveOriginalPhoto()
                {
                    UIImageWriteToSavedPhotosAlbum(img!, nil, nil, nil);
                }

                //Navigate to Filter screen when coming from Peep Text Flow
                if self.isComingFromPeepTextFlow == true
                {
                    let dataPath = ""
                    
                    let st = UIStoryboard(name: "Digital", bundle:nil)
                    let obj = st.instantiateViewController(withIdentifier: "PhotoFilterEdit") as! PhotoFilterEdit
                    obj.imagePhoto = img
                    obj.dataPath = dataPath
                    obj.isComingFromPeepTextFlow = self.isComingFromPeepTextFlow
                    self.navigationController?.pushViewController(obj, animated: true)
                }
                else
                {
                    let st = UIStoryboard(name: "Digital", bundle:nil)
                    let obj = st.instantiateViewController(withIdentifier: "PhotoAddAudio") as! PhotoAddAudio
                    obj.imagePhoto = img
                    obj.isComingFromPeepTextFlow = self.isComingFromPeepTextFlow!
                    if self.isComingFromEventsFlow == true {
                        obj.isComingFromEventsFlow = self.isComingFromEventsFlow
                        obj.delegate = self.delegate
                    }
                    else {
                        obj.isComingFromEventsFlow = false
                    }
                    self.navigationController?.pushViewController(obj, animated: true)
                }
            }*/
            
        //}
    }
    
    
    // callBack from take picture
    func photoOutput(_ captureOutput: AVCapturePhotoOutput,  didFinishProcessingPhoto photoSampleBuffer: CMSampleBuffer?,  previewPhoto previewPhotoSampleBuffer: CMSampleBuffer?, resolvedSettings:  AVCaptureResolvedPhotoSettings, bracketSettings:   AVCaptureBracketedStillImageSettings?, error: Error?) {
        
        if let error = error {
            print("error occure : \(error.localizedDescription)")
        }
        
        if  let sampleBuffer = photoSampleBuffer,
            let previewBuffer = previewPhotoSampleBuffer,
            let dataImage =  AVCapturePhotoOutput.jpegPhotoDataRepresentation(forJPEGSampleBuffer:  sampleBuffer, previewPhotoSampleBuffer: previewBuffer) {
            print(UIImage(data: dataImage)?.size as Any)
            
            let dataProvider = CGDataProvider(data: dataImage as CFData)
            let cgImageRef: CGImage! = CGImage(jpegDataProviderSource: dataProvider!, decode: nil, shouldInterpolate: true, intent: .defaultIntent)
            var img = UIImage(cgImage: cgImageRef, scale: 1.0, orientation: UIImageOrientation.right)

            img = Utilities().imageResize(img,sizeChange: CGSize(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.width * (4/3)))
            
            img = Utilities().cropToBounds(img, cgwidth: UIScreen.main.bounds.width, cgheight: UIScreen.main.bounds.width)
            
            printCustom("width: \(img.size.width) height \(img.size.height)")
            
            //Save original photo if setting is ON
            if Utilities().saveOriginalPhoto()
            {
                UIImageWriteToSavedPhotosAlbum(img, nil, nil, nil);
            }
            
            //Navigate to Filter screen when coming from Peep Text Flow
            if self.isComingFromPeepTextFlow == true
            {
                let dataPath = ""
                
                let st = UIStoryboard(name: "Digital", bundle:nil)
                let obj = st.instantiateViewController(withIdentifier: "PhotoFilterEdit") as! PhotoFilterEdit
                obj.imagePhoto = img
                obj.dataPath = dataPath
                obj.isComingFromPeepTextFlow = self.isComingFromPeepTextFlow
                self.navigationController?.pushViewController(obj, animated: true)
            }
            else
            {
                let st = UIStoryboard(name: "Digital", bundle:nil)
                let obj = st.instantiateViewController(withIdentifier: "PhotoAddAudio") as! PhotoAddAudio
                obj.imagePhoto = img
                obj.isComingFromPeepTextFlow = self.isComingFromPeepTextFlow!
                if self.isComingFromEventsFlow == true {
                    obj.isComingFromEventsFlow = self.isComingFromEventsFlow
                    obj.delegate = self.delegate
                }
                else {
                    obj.isComingFromEventsFlow = false
                }
                self.navigationController?.pushViewController(obj, animated: true)
            }

        } else {
            print("some error here")
        }
    }
    
    // This method you can use somewhere you need to know camera permission   state
    func askPermission() {
        print("here")
        let cameraPermissionStatus =  AVCaptureDevice.authorizationStatus(for: .video)
        
        switch cameraPermissionStatus {
        case .authorized:
            print("Already Authorized")
        case .denied:
            print("denied")
            
            let alert = UIAlertController(title: "Sorry :(" , message: "But  could you please grant permission for camera within device settings",  preferredStyle: .alert)
            let action = UIAlertAction(title: "Ok", style: .cancel,  handler: nil)
            alert.addAction(action)
            present(alert, animated: true, completion: nil)
            
        case .restricted:
            print("restricted")
        default:
            AVCaptureDevice.requestAccess(for: AVMediaType.video, completionHandler: {
                [weak self]
                (granted :Bool) -> Void in
                
                if granted == true {
                    // User granted
                    print("User granted")
                    DispatchQueue.main.async(){
                        //Do smth that you need in main thread
                    }
                }
                else {
                    // User Rejected
                    print("User Rejected")
                    
                    DispatchQueue.main.async(){
                        let alert = UIAlertController(title: "WHY?" , message:  "Camera it is the main feature of our application", preferredStyle: .alert)
                        let action = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
                        alert.addAction(action)
                        self?.present(alert, animated: true, completion: nil)
                    }
                }
            });
        }
    }
    
    
    @IBAction func focusCamera(_ sender: AnyObject) {
        
        //let avDevice = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo)
        
        let avDevice = AVCaptureDevice.default(for: .video)
        
        // check if the device has torch
        if (avDevice?.hasTorch)! {
            // lock your device for configuration
            do {
                try avDevice?.lockForConfiguration()
                
            }
            catch {
                printCustom("")
            }
            
            // check if your torchMode is on or off. If on turns it off otherwise turns it on
            if (avDevice?.isTorchActive)! {
                
                btnFocus.setImage(Utilities().themedImage(img_noflash), for: UIControlState())
                
                avDevice?.torchMode = AVCaptureDevice.TorchMode.off
            } else {
                // sets the torch intensity to 100%
                do {
                    btnFocus.setImage(Utilities().themedImage(img_flash), for: UIControlState())
                    try avDevice?.setTorchModeOn(level: 1.0)
                }
                catch {
                    printCustom("")
                }
            }
            // unlock your device
            avDevice?.unlockForConfiguration()
        }
    }

    @IBAction func toggleCamera(_ sender: AnyObject) {
        
//        AVCaptureDeviceDiscoverySession *captureDeviceDiscoverySession = [AVCaptureDeviceDiscoverySession discoverySessionWithDeviceTypes:@[AVCaptureDeviceTypeBuiltInWideAngleCamera]
//            mediaType:AVMediaTypeVideo
//            position: AVCaptureDevicePositionUnspecified]; // here you pass AVCaptureDevicePositionUnspecified to find all capture devices
//        
//        NSArray *captureDevices = [captureDeviceDiscoverySession devices];
        
        if AVCaptureDevice.devices(for: .video).count > 1 {
            
            let desiredPosition:AVCaptureDevice.Position?
            
            if isUsingFrontFacingCamera == true {
                
                btnFocus.isHidden = false
                desiredPosition = AVCaptureDevice.Position.back
            }
            else
            {
                btnFocus.isHidden = true
                desiredPosition = AVCaptureDevice.Position.front;
            }
            
            for d in AVCaptureDevice.devices (for: AVMediaType.video) {
                
                
                if (d as AnyObject).position == desiredPosition {
                    
                    captureSession.beginConfiguration()
                    
                    do {
                        
                        let input = try AVCaptureDeviceInput(device: d as! AVCaptureDevice)
                        
                        for oldInput in captureSession.inputs {
                            
                            captureSession.removeInput(oldInput as! AVCaptureInput)
                        }
                        
                        //Adding video input type in session
                        captureSession.addInput(input)
                        
                        //Adding audio input type in session
                        //let audioCaptureDevice = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeAudio)
                        
                        let audioCaptureDevice = AVCaptureDevice.default(for: .audio)
                        
                        do {
                            let audioInput = try AVCaptureDeviceInput.init(device: audioCaptureDevice!)
                            
                            captureSession.addInput(audioInput)
                            
                            captureSession.commitConfiguration()
                            
                            break;
                        }
                        catch let error as NSError {
                            printCustom("error:\(error)")
                        }
                        
                    } catch let error as NSError {
                        printCustom("error:\(error)")
                    }
                }
            }
            
            if isUsingFrontFacingCamera == true {
                
                isUsingFrontFacingCamera = false
            }
            else{
                
                isUsingFrontFacingCamera = true
            }
        }
    }
    
    @IBAction func cancelCapturePhoto(_ sender: AnyObject) {
        
        //self.view.insertSubview(self.camView, atIndex: 0)
        self.navigationController?.popViewController(animated: true)
    }
    
//    @IBAction func moveNext(sender: AnyObject) {
//        
//        let st = UIStoryboard(name: "Main", bundle:nil)
//        let obj = st.instantiateViewControllerWithIdentifier("PhotoAddAudio") as! PhotoAddAudio
//        self.navigationController?.pushViewController(obj, animated: true)
//        
//    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}
