//
//  UniversalCollectionViewPeepTextListCell.swift
//  C00Peeps
//
//  Created by Mac on 31/10/16.
//  Copyright © 2016 SOTSYS011. All rights reserved.
//

import UIKit

class UniversalCollectionViewPeepTextListCell: UICollectionViewCell {

    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var btnUserImage: UIButton!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblPeepText: UILabel!
    @IBOutlet weak var lblPostedDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
