//
//  SearchViewController.swift
//  C00Peeps
//
//  Created by SOTSYS027 on 3/16/16.
//  Copyright © 2016 SOTSYS011. All rights reserved.
//

import UIKit
import SwiftyJSON
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class SearchViewController: BaseVC, UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource, UpdateContent {
    
    var searchController: UISearchController!
    
    @IBOutlet weak var tableSearch:UITableView!
    @IBOutlet weak var btnPeople: UIButton!
    @IBOutlet weak var btnMovies: UIButton!
    @IBOutlet weak var btnMusic: UIButton!
    @IBOutlet weak var btnVideoGames: UIButton!
    @IBOutlet weak var lblNoRecordsFound: UILabel!
    @IBOutlet weak var btnRefresh: UIButton!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var vwSelection: UIView!
    @IBOutlet weak var leadingConstraintSelectionView: NSLayoutConstraint!
    
    var universalCollectionViewMovies : UniversalCollectionView!
    var universalCollectionViewVideoGames : UniversalCollectionView!
    var universalCollectionViewMusic : UniversalCollectionView!
    
    var refreshControl: UIRefreshControl!
    
    var arrayPeople = [People]()
    var filterArrayPeople = [People]()
    var imageCache = [String:UIImage]()
   
    var offsetPeople = 0
    var currentPagePeople = 1
    var totalRecordsPeople = 0
    
    var offsetMovies = 0
    var currentPageMovies = 1
    var totalRecordsMovies = 0
    var catIdMovie = 2
    
    var offsetMusic = 0
    var currentPageMusic = 1
    var totalRecordsMusic = 0
    var catIdMusic = 1
    
    var offsetVideoGames = 0
    var currentPageVideoGames = 1
    var totalRecordsVideoGames = 0
    var catIdVideoGames = 4
    
    var arrayMovies = [Media]()
    var arrayMusic = [Media]()
    var arrayVideoGames = [Media]()
    var filterArrayMedia = [Media]()
    
    var isSecondTabLoaded: Bool!
    var isThirdTabLoaded: Bool!
    var isFourthTabLoaded: Bool!
    var selectedIndex = 0
    
    
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
        
        if searchController.isActive == true{
            
            searchController.isActive = false
            searchCancel()
        }
        
    }
    
     // MARK:- ViewDid Load
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        updateViewAccordingToTheme()
        
        addRefreshControl()
        
//        //Fetching all public users
//        getPeoples()
//        
        isSecondTabLoaded = false
        isThirdTabLoaded = false
        isFourthTabLoaded = false
        
        self.searchController = ({
            let controller = UISearchController(searchResultsController: nil)
            controller.searchBar.delegate = self
            controller.dimsBackgroundDuringPresentation = false
            controller.hidesNavigationBarDuringPresentation = false
            return controller
        })()
        
        definesPresentationContext = true
        searchController.searchBar.delegate = self
       
        // Change search bar field appearance.
        let textFieldInsideSearchBar = searchController.searchBar.value(forKey: "searchField") as? UITextField
        textFieldInsideSearchBar!.backgroundColor = UIColor.black.withAlphaComponent(0.2)
        textFieldInsideSearchBar!.textColor = UIColor.white
        textFieldInsideSearchBar!.layer.cornerRadius = 14
        self.changeTextfieldPlaceholderColor(textFieldInsideSearchBar!, placeholderText: textFieldInsideSearchBar!.placeholder!, color: UIColor.white)
        searchController.searchBar.setImage(Utilities().themedImage(img_searchIcon), for: .search, state: UIControlState())

        self.navigationItem.titleView = searchController.searchBar
        
        tableSearch.register(UITableViewCell.self, forCellReuseIdentifier: "LoadingCell")
        tableSearch.estimatedRowHeight = 50
        tableSearch.separatorColor = UIColor.clear
        self.automaticallyAdjustsScrollViewInsets = false

        self.reloadSegment()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Update View According To Selected Theme
    
    func updateViewAccordingToTheme() {
        vwSelection.backgroundColor = Utilities().themedMultipleColor()[0]
    }
    
    //MARK:- Pull to refresh for people table view
    func addRefreshControl(){
        
        refreshControl = UIRefreshControl()
        refreshControl.tintColor = UIColor.darkGray
        refreshControl.attributedTitle = NSAttributedString(string: loaderTitle_PullToRefresh)
        refreshControl.addTarget(self, action: #selector(SearchViewController.handleRefresh(_:)), for: UIControlEvents.valueChanged)
        self.tableSearch.addSubview(refreshControl)
        
    }
    
    func handleRefresh(_ refreshControl: UIRefreshControl) {
        
        offsetPeople = 0
        currentPagePeople = 1
        
        imageCache.removeAll()
        
        getPeoples()
    }
    
    //MARK: - Get People Data
    func getPeoples(){
        if arrayPeople.count == 0 {
            loadingIndicator.startAnimating()
            self.hideErrorMessage()
        }
        
        var parameters = [String: AnyObject]()
        parameters["user_id"] = Utilities.getUserDetails().id as AnyObject
        parameters["limit"] = Limit as AnyObject
        parameters["offset"] = offsetPeople as AnyObject
        
        //cancel all requests
        APIManager.sharedInstance.cancelAllRequests()
        
        APIManager.sharedInstance.requestGetPeople(parameters, Target: self)
    }
    
    //MARK: - Get People Response
    func responseGetPeoples (_ notify: Foundation.Notification)
    {
        if loadingIndicator.isAnimating {
            loadingIndicator.stopAnimating()
        }
        
        refreshControl.endRefreshing()
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFIACTION_GETPEOPLE), object: nil)
        
        let swiftyJsonVar = JSON(notify.object!)
        
        let message = swiftyJsonVar["message"].stringValue
        let status = swiftyJsonVar["status"].intValue
        
        if swiftyJsonVar [ERROR_KEY].exists()
        {
            self.showErrorMessage(swiftyJsonVar[ERROR_KEY].stringValue)
        }
        else if (status == 1) //success response
        {
            if offsetPeople == 0
            {
                arrayPeople.removeAll()
            }
            
            totalRecordsPeople = swiftyJsonVar["response"]["count"].intValue
            
            if totalRecordsPeople > 0
            {
                var arrFeeds = [People]()
                arrFeeds = Parser.getParsedPeopleArrayFromData(swiftyJsonVar["response"]["list"])
                
                arrayPeople.append(contentsOf: arrFeeds)
                
                if arrayPeople.count>0
                {
                    self.hideErrorMessage()
                    tableSearch.reloadData()
                }
            }
            else
            {
                tableSearch.reloadData()
              
                self.showErrorMessage(alertMsg_NoUsers)
            }
        }
        else if (status == 0) //error response
        {
            let responseErrorMessage = swiftyJsonVar["response"].stringValue
            self.showErrorMessage(responseErrorMessage)
        }
        else
        {
            self.showErrorMessage(message)
        }
    }
    
    //MARK: - Search People Data
    func searchPeoples(){
        if filterArrayPeople.count == 0 {
            loadingIndicator.startAnimating()
            self.hideErrorMessage()
        }
        
        if self.searchController.searchBar.text!.characters.count > 0
        {
            var parameters = [String: AnyObject]()
            parameters["user_id"] = Utilities.getUserDetails().id as AnyObject
            parameters["limit"] = Limit as AnyObject
            parameters["offset"] = offsetPeople as AnyObject
            parameters["keyword"] = searchController.searchBar.text as AnyObject
            
            //cancel all requests
            APIManager.sharedInstance.cancelAllRequests()
            
            APIManager.sharedInstance.requestSearchPeople(parameters, Target: self)
        }
    }
    
    //MARK: - Search People Response
    func responseSearchPeoples (_ notify: Foundation.Notification)
    {
        if loadingIndicator.isAnimating {
            loadingIndicator.stopAnimating()
        }
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFIACTION_SEARCHPEOPLE), object: nil)
        
        let swiftyJsonVar = JSON(notify.object!)
        
        let message = swiftyJsonVar["message"].stringValue
        let status = swiftyJsonVar["status"].intValue
        
        if swiftyJsonVar [ERROR_KEY].exists()
        {
            self.showErrorMessage(swiftyJsonVar[ERROR_KEY].stringValue)
        }
        else if (status == 1) //success response
        {
            if offsetPeople == 0
            {
                filterArrayPeople.removeAll()
            }
            
            let totalCount = swiftyJsonVar["response"]["count"].intValue
            if totalCount > 0
            {
                var arrFeeds = [People]()
                arrFeeds = Parser.getParsedPeopleArrayFromData(swiftyJsonVar["response"]["list"])
                
                filterArrayPeople.append(contentsOf: arrFeeds)
                
                if filterArrayPeople.count>0
                {
                    
                    self.hideErrorMessage()
                    tableSearch.reloadData()
                }
            }
            else
            {
                tableSearch.reloadData()
                
                self.showErrorMessage(alertMsg_NoUsers)
            }
        }
        else if (status == 0) //error response
        {
            let responseErrorMessage = swiftyJsonVar["response"].stringValue
            self.showErrorMessage(responseErrorMessage)
        }
        else
        {
            self.showErrorMessage(message)
        }
    }
    
    //MARK: - Get public media
    func getPublicMedia(_ categoryId:Int)
    {
       
        //Category Id (1 Music 2 Movie 3 Photo 4 Video Game
        var parameters = [String: AnyObject]()
        parameters["category_id"] = categoryId as AnyObject
        parameters["limit"] = SearchLimit as AnyObject
        parameters["user_id"] = Utilities.getUserDetails().id as AnyObject
    
        //cancel all requests
        APIManager.sharedInstance.cancelAllRequests()
        
        if categoryId == catIdMovie //Movies
        {
            if arrayMovies.count == 0 {
                loadingIndicator.startAnimating()
                self.hideErrorMessage()
            }

            parameters["offset"] = offsetMovies as AnyObject
            APIManager.sharedInstance.getPublicMovieMedia(parameters, Target: self)
        }
         else if categoryId == catIdMusic //Music
        {
            if arrayMusic.count == 0 {
                loadingIndicator.startAnimating()
                self.hideErrorMessage()
            }

            parameters["offset"] = offsetMusic as AnyObject
            APIManager.sharedInstance.getPublicMusicMedia(parameters, Target: self)
        }
        else if categoryId == catIdVideoGames //Video Games
        {
            if arrayVideoGames.count == 0 {
                loadingIndicator.startAnimating()
                self.hideErrorMessage()
            }
            
            parameters["offset"] = offsetVideoGames as AnyObject
            APIManager.sharedInstance.getPublicVideoGamesMedia(parameters, Target: self)
        }
    }
    
    //MARK: - Get public Movie media Response
    func responseGetPublicMovieMedia (_ notify: Foundation.Notification)
    {
        if loadingIndicator.isAnimating {
            loadingIndicator.stopAnimating()
        }
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFIACTION_GETPUBLICMOVIEMEDIA), object: nil)
        
        let swiftyJsonVar = JSON(notify.object!)
        
        let message = swiftyJsonVar["message"].stringValue
        let status = swiftyJsonVar["status"].intValue
        
        if swiftyJsonVar [ERROR_KEY].exists()
        {
            self.showErrorMessage(swiftyJsonVar[ERROR_KEY].stringValue)
        }
        else if (status == 1) //success response
        {
            totalRecordsMovies = swiftyJsonVar["response"]["count"].intValue
            
            if totalRecordsMovies > 0
            {
                self.hideErrorMessage()
                var arrFeeds = [Media]()
                arrFeeds = Parser.getParsedMediaArrayFromData(swiftyJsonVar["response"]["media"])
                
                if offsetMovies == 0
                {
                    arrayMovies.removeAll()
                }
                
                arrayMovies.append(contentsOf: arrFeeds)
                
                if arrayMovies.count>0
                {
                    universalCollectionViewMovies.totalRecordCount = self.totalRecordsMovies
                    universalCollectionViewMovies.searchCategoryId = self.catIdMovie
                    universalCollectionViewMovies.currentPage = self.currentPageMovies
                    universalCollectionViewMovies.offset = self.offsetMovies
                    universalCollectionViewMovies.arrayCollection = self.arrayMovies
                    universalCollectionViewMovies.reloadData()
                }
            }
            else
            {
                self.showErrorMessage(alertMsg_NoData)
            }
        }
        else if (status == 0) //error response
        {
            let responseErrorMessage = swiftyJsonVar["response"].stringValue
            self.showErrorMessage(responseErrorMessage)
        }
        else
        {
            self.showErrorMessage(message)
        }
    }
    
    //MARK: - Get public Music media Response
    func responseGetPublicMusicMedia (_ notify: Foundation.Notification)
    {
        if loadingIndicator.isAnimating {
            loadingIndicator.stopAnimating()
        }
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFIACTION_GETPUBLICMUSICMEDIA), object: nil)
        
        let swiftyJsonVar = JSON(notify.object!)
        
        let message = swiftyJsonVar["message"].stringValue
        let status = swiftyJsonVar["status"].intValue
        
        if swiftyJsonVar [ERROR_KEY].exists()
        {
            self.showErrorMessage(swiftyJsonVar[ERROR_KEY].stringValue)
        }
        else if (status == 1) //success response
        {
            totalRecordsMusic = swiftyJsonVar["response"]["count"].intValue
            
            if totalRecordsMusic > 0
            {
                self.hideErrorMessage()
                
                var arrFeeds = [Media]()
                arrFeeds = Parser.getParsedMediaArrayFromData(swiftyJsonVar["response"]["media"])
                
                if offsetMusic == 0
                {
                    arrayMusic.removeAll()
                }
                
                arrayMusic.append(contentsOf: arrFeeds)
                
                if arrayMusic.count>0
                {
                    universalCollectionViewMusic.totalRecordCount = self.totalRecordsMusic
                    universalCollectionViewMusic.searchCategoryId = self.catIdMusic
                    universalCollectionViewMusic.currentPage = self.currentPageMusic
                    universalCollectionViewMusic.offset = self.offsetMusic
                    universalCollectionViewMusic.arrayCollection = self.arrayMusic
                    universalCollectionViewMusic.reloadData()
                }
            }
            else
            {
                self.showErrorMessage(alertMsg_NoData)
            }
        }
        else if (status == 0) //error response
        {
            let responseErrorMessage = swiftyJsonVar["response"].stringValue
            self.showErrorMessage(responseErrorMessage)
        }
        else
        {
            self.showErrorMessage(message)
        }
    }
    
    //MARK: - Get public Video Games media Response
    func responseGetPublicVideoGamesMedia (_ notify: Foundation.Notification)
    {
        if loadingIndicator.isAnimating {
            loadingIndicator.stopAnimating()
        }
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFIACTION_GETPUBLICVIDEOGAMESMEDIA), object: nil)
        
        let swiftyJsonVar = JSON(notify.object!)
        
        let message = swiftyJsonVar["message"].stringValue
        let status = swiftyJsonVar["status"].intValue
        
        if swiftyJsonVar [ERROR_KEY].exists()
        {
            self.showErrorMessage(swiftyJsonVar[ERROR_KEY].stringValue)
        }
        else if (status == 1) //success response
        {
            totalRecordsVideoGames = swiftyJsonVar["response"]["count"].intValue
            
            if totalRecordsVideoGames > 0
            {
                self.hideErrorMessage()
                
                var arrFeeds = [Media]()
                arrFeeds = Parser.getParsedMediaArrayFromData(swiftyJsonVar["response"]["media"])
                
                if offsetVideoGames == 0
                {
                    arrayVideoGames.removeAll()
                }
                
                arrayVideoGames.append(contentsOf: arrFeeds)
                
                if arrayVideoGames.count>0
                {
                    universalCollectionViewVideoGames.totalRecordCount = self.totalRecordsVideoGames
                    universalCollectionViewVideoGames.searchCategoryId = self.catIdVideoGames
                    universalCollectionViewVideoGames.currentPage = self.currentPageVideoGames
                    universalCollectionViewVideoGames.offset = self.offsetVideoGames
                    universalCollectionViewVideoGames.arrayCollection = self.arrayVideoGames
                    universalCollectionViewVideoGames.reloadData()
                }
            }
            else
            {
                self.showErrorMessage(alertMsg_NoData)
            }
        }
        else if (status == 0) //error response
        {
            let responseErrorMessage = swiftyJsonVar["response"].stringValue
            self.showErrorMessage(responseErrorMessage)
        }
        else
        {
            self.showErrorMessage(message)
        }
    }
    
    //MARK: - Search public media
    func searchPublicMedia(_ categoryId:Int){
      
        if self.searchController.searchBar.text!.characters.count > 0
        {
            if filterArrayMedia.count == 0 {
                loadingIndicator.startAnimating()
                self.hideErrorMessage()
            }
            
            var parameters = [String: AnyObject]()
            parameters["category_id"] = categoryId as AnyObject
            parameters["limit"] = SearchLimit as AnyObject
            parameters["keyword"] = searchController.searchBar.text as AnyObject
            parameters["user_id"] = Utilities.getUserDetails().id as AnyObject
            
            if categoryId == catIdMovie //Movies
            {
                parameters["offset"] = offsetMovies as AnyObject
            }
            else if categoryId == catIdMusic //Music
            {
                parameters["offset"] = offsetMusic as AnyObject
                
            }
            else if categoryId == catIdVideoGames //Video Games
            {
                parameters["offset"] = offsetVideoGames as AnyObject

            }
            
            //cancel all requests
            APIManager.sharedInstance.cancelAllRequests()
            
            APIManager.sharedInstance.searchPublicMedia(parameters, Target: self)
        }
    }
    
    //MARK: - Search public media Response
    func responseSearchPublicMedia (_ notify: Foundation.Notification)
    {
        if loadingIndicator.isAnimating {
            loadingIndicator.stopAnimating()
        }
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFIACTION_SEARCHPUBLICMEDIA), object: nil)
        
        let swiftyJsonVar = JSON(notify.object!)
        
        let message = swiftyJsonVar["message"].stringValue
        let status = swiftyJsonVar["status"].intValue
        
        if swiftyJsonVar [ERROR_KEY].exists()
        {
            self.showErrorMessage(swiftyJsonVar[ERROR_KEY].stringValue)
        }
        else if (status == 1) //success response
        {
            if selectedIndex == 1 // Movie
            {
                totalRecordsMovies = swiftyJsonVar["response"]["count"].intValue
                
                if totalRecordsMovies > 0
                {
                    self.hideErrorMessage()
                    
                    var arrFeeds = [Media]()
                    arrFeeds = Parser.getParsedMediaArrayFromData(swiftyJsonVar["response"]["media"])
                    
                    if offsetMovies == 0
                    {
                        filterArrayMedia.removeAll()
                    }
                    
                    filterArrayMedia.append(contentsOf: arrFeeds)
                    
                    if filterArrayMedia.count>0
                    {
                        universalCollectionViewMovies.totalRecordCount = self.totalRecordsMovies
                        universalCollectionViewMovies.searchCategoryId = self.catIdMovie
                        universalCollectionViewMovies.currentPage = self.currentPageMovies
                        universalCollectionViewMovies.offset = self.offsetMovies
                        universalCollectionViewMovies.arrayCollection = self.filterArrayMedia
                        universalCollectionViewMovies.reloadData()
                    }
                }
                else
                {
                    self.showErrorMessage(alertMsg_NoData)
                }

            }
            else if selectedIndex == 2 // Music
            {
                totalRecordsMusic = swiftyJsonVar["response"]["count"].intValue
                
                if totalRecordsMusic > 0
                {
                    self.hideErrorMessage()

                    var arrFeeds = [Media]()
                    arrFeeds = Parser.getParsedMediaArrayFromData(swiftyJsonVar["response"]["media"])
                    
                    if offsetMusic == 0
                    {
                        filterArrayMedia.removeAll()
                    }
                    
                    filterArrayMedia.append(contentsOf: arrFeeds)
                    
                    if filterArrayMedia.count>0
                    {
                        universalCollectionViewMusic.totalRecordCount = self.totalRecordsMusic
                        universalCollectionViewMusic.searchCategoryId = self.catIdMusic
                        universalCollectionViewMusic.currentPage = self.currentPageMusic
                        universalCollectionViewMusic.offset = self.offsetMusic
                        universalCollectionViewMusic.arrayCollection = self.filterArrayMedia
                        universalCollectionViewMusic.reloadData()
                    }
                }
                else
                {
                    self.showErrorMessage(alertMsg_NoData)
                }
            }
            else if selectedIndex == 3 // Video Games
            {
                totalRecordsVideoGames = swiftyJsonVar["response"]["count"].intValue
                
                if totalRecordsVideoGames > 0
                {
                    self.hideErrorMessage()

                    var arrFeeds = [Media]()
                    arrFeeds = Parser.getParsedMediaArrayFromData(swiftyJsonVar["response"]["media"])
                    
                    if offsetVideoGames == 0
                    {
                        filterArrayMedia.removeAll()
                    }
                    
                    filterArrayMedia.append(contentsOf: arrFeeds)
                    
                    if filterArrayMedia.count>0
                    {
                        universalCollectionViewVideoGames.totalRecordCount = self.totalRecordsVideoGames
                        universalCollectionViewVideoGames.searchCategoryId = self.catIdVideoGames
                        universalCollectionViewVideoGames.currentPage = self.currentPageVideoGames
                        universalCollectionViewVideoGames.offset = self.offsetVideoGames
                        universalCollectionViewVideoGames.arrayCollection = self.filterArrayMedia
                        universalCollectionViewVideoGames.reloadData()
                    }
                }
                else
                {
                    self.showErrorMessage(alertMsg_NoData)
                }
            }
        }
        else if (status == 0) //error response
        {
            let responseErrorMessage = swiftyJsonVar["response"].stringValue
            self.showErrorMessage(responseErrorMessage)
        }
        else
        {
            self.showErrorMessage(message)
        }
    }
    
    
    //MARK: - Follow People
    func followPeople(_ userId:Int){
        
        //{1=Approved, 2=Pending, 3=Blocked, 4=Unknown}
        var parameters = [String: AnyObject]()
        parameters["requester_id"] = Utilities.getUserDetails().id as AnyObject
        parameters["responder_id"] = userId as AnyObject
        parameters["status_id"] = 2 as AnyObject //Pending Status
        
         print(parameters)
        
        APIManager.sharedInstance.requestFollow(parameters, Target: self)
    }
    
    //MARK: - Follow People Response
    func responseFollowPeople (_ notify: Foundation.Notification)
    {
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFIACTION_FOLLOWPEOPLE), object: nil)
        
        let swiftyJsonVar = JSON(notify.object!)
        
        let message = swiftyJsonVar["message"].stringValue
        let status = swiftyJsonVar["status"].intValue
        
        if swiftyJsonVar [ERROR_KEY].exists()
        {
            self.showCommonAlert(swiftyJsonVar[ERROR_KEY].stringValue)
        }
        else if (status == 1) //success response
        {
            
        }
        else if (status == 0) //error response
        {
            let responseErrorMessage = swiftyJsonVar["response"].stringValue
            self.showCommonAlert(responseErrorMessage)
        }
        else
        {
            self.showCommonAlert(message)
        }
    }
    
    //MARK: - Unfollow People
    func unfollowPeople(_ userId:Int){
        var parameters = [String: AnyObject]()
        parameters["requester_id"] = Utilities.getUserDetails().id as AnyObject
        parameters["responder_id"] = userId as AnyObject
        
        APIManager.sharedInstance.requestUnFollow(parameters, Target: self)
    }
    
    //MARK: - Unfollow People Response
    func responseUnFollowPeople (_ notify: Foundation.Notification)
    {
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFIACTION_UNFOLLOWPEOPLE), object: nil)
        
        let swiftyJsonVar = JSON(notify.object!)
        
        let message = swiftyJsonVar["message"].stringValue
        let status = swiftyJsonVar["status"].intValue
        
        if swiftyJsonVar [ERROR_KEY].exists()
        {
            self.showCommonAlert(swiftyJsonVar[ERROR_KEY].stringValue)
        }
        else if (status == 1) //success response
        {
            
        }
        else if (status == 0) //error response
        {
            let responseErrorMessage = swiftyJsonVar["response"].stringValue
            self.showCommonAlert(responseErrorMessage)
        }
        else
        {
            self.showCommonAlert(message)
        }
    }

    
    // MARK: - Show/Hide Error Message
    func showErrorMessage(_ message:String) {
        self.btnRefresh.isHidden = false
        self.lblNoRecordsFound.isHidden = false
        self.lblNoRecordsFound.text = message
        
        if selectedIndex == 0 { //People
            tableSearch.isHidden = true
        }
        else if selectedIndex == 1 { //Movies
            if(universalCollectionViewMovies != nil) {
                universalCollectionViewMovies.isHidden = true
            }
        }
        else if selectedIndex == 2 { //Music
            if universalCollectionViewMusic != nil {
                universalCollectionViewMusic.isHidden = true
            }
        }
        else if selectedIndex == 3 { //Video Games
            if universalCollectionViewVideoGames != nil {
                universalCollectionViewVideoGames.isHidden = true
            }
        }
    }
    
    func hideErrorMessage() {
        self.btnRefresh.isHidden = true
        self.lblNoRecordsFound.isHidden = true
        
        if selectedIndex == 0 { //People
            tableSearch.isHidden = false
        }
        else if selectedIndex == 1 { //Movies
            if(universalCollectionViewMovies != nil) {
                universalCollectionViewMovies.isHidden = false
            }
        }
        else if selectedIndex == 2 { //Music
            if universalCollectionViewMusic != nil {
                universalCollectionViewMusic.isHidden = false
            }
        }
        else if selectedIndex == 3 { //Video Games
            if universalCollectionViewVideoGames != nil {
                universalCollectionViewVideoGames.isHidden = false
            }
        }
    }
    
    //MARK: - Button Actions
    @IBAction func btnRefreshClicked(_ sender: AnyObject) {
        self.lblNoRecordsFound.isHidden = true
        self.btnRefresh.isHidden = true
        self.loadingIndicator.startAnimating()
        
        if selectedIndex == 0 { //People
            if self.searchController.searchBar.text!.characters.count > 0 {
                filterArrayPeople.removeAll(keepingCapacity: false)
                offsetPeople = 0
                currentPagePeople = 1
                self.searchPeoples()
            }
            else {
                self.getPeoples()
            }
        }
        else if selectedIndex == 1 { //Movies
            if self.searchController.searchBar.text!.characters.count > 0 {
                filterArrayMedia.removeAll(keepingCapacity: false)
                offsetMovies = 0
                currentPageMovies = 1
                self.searchPublicMedia(catIdMovie)
            }
            else {
                self.reloadMediaGallery(catIdMovie)
            }
        }
        else if selectedIndex == 2 { //Music
            if self.searchController.searchBar.text!.characters.count > 0 {
                filterArrayMedia.removeAll(keepingCapacity: false)
                offsetMusic = 0
                currentPageMusic = 1
                self.searchPublicMedia(catIdMusic)
            }
            else {
                self.reloadMediaGallery(catIdMusic)
            }
        }
        else if selectedIndex == 3 { //Video Games
            if self.searchController.searchBar.text!.characters.count > 0 {
                filterArrayMedia.removeAll(keepingCapacity: false)
                offsetVideoGames = 0
                currentPageVideoGames = 1
                self.searchPublicMedia(catIdVideoGames)
            }
            else {
                self.reloadMediaGallery(catIdVideoGames)
            }
        }
    }

    @IBAction func selectButton(_ sender: UIButton)
    {
        let btn = sender 
        
        selectedIndex = btn.tag
        
        reloadSegment()
    }
    
    //MARK:- Reload Data
    
    func reloadSegment()
    {
        self.leadingConstraintSelectionView.constant = btnPeople.frame.size.width * (CGFloat(selectedIndex))
        
        if selectedIndex == 0 //People
        {
        
            btnPeople.setImage(Utilities().themedImage(img_usersSelectedSearchGallery), for: UIControlState())
            btnMusic.setImage(Utilities().themedImage(img_musicSearchGallery), for: UIControlState())
            btnMovies.setImage(Utilities().themedImage(img_filmSearchGallery), for: UIControlState())
            btnVideoGames.setImage(Utilities().themedImage(img_gamesSearchGallery), for: UIControlState())
            
            
            tableSearch.isHidden = false

            if universalCollectionViewMovies != nil
            {
                universalCollectionViewMovies.isHidden = true
            }
            
            if universalCollectionViewVideoGames != nil
            {
                universalCollectionViewVideoGames.isHidden = true
            }
            if universalCollectionViewMusic != nil
            {
                universalCollectionViewMusic.isHidden = true
            }
            
            //If search is active then perform search
            if self.searchController!.isActive &&  searchController.searchBar.text?.characters.count > 0
            {
                
                performSearch()
                return
            }
            else
            {
                searchController.searchBar.text = ""
                searchController.searchBar.resignFirstResponder()
                searchController.isActive = false
            }
            
            if arrayPeople.count == 0 {
                offsetMovies = 0
                currentPageMovies = 1
                self.getPeoples()
            }
            else {
                if self.loadingIndicator.isAnimating {
                    self.loadingIndicator.stopAnimating()
                }
                tableSearch.reloadData()
                self.hideErrorMessage()
            }
        
           
        }
        else if selectedIndex == 1 //Movies
        {
            offsetMovies = 0
            currentPageMovies = 1
            
            btnPeople.setImage(Utilities().themedImage(img_usersSearchGallery), for: UIControlState())
            btnMusic.setImage(Utilities().themedImage(img_musicSearchGallery), for: UIControlState())
            btnMovies.setImage(Utilities().themedImage(img_filmSelectedSearchGallery), for: UIControlState())
            btnVideoGames.setImage(Utilities().themedImage(img_gamesSearchGallery), for: UIControlState())
           
            tableSearch.isHidden = true
            if(universalCollectionViewVideoGames != nil)
            {
                universalCollectionViewVideoGames.isHidden = true
            }
            
            if universalCollectionViewMusic != nil
            {
                universalCollectionViewMusic.isHidden = true
            }
            
            if universalCollectionViewMovies == nil || isSecondTabLoaded == false
            {
                let collectinLayout = UICollectionViewFlowLayout()
                universalCollectionViewMovies = UniversalCollectionView.init(frame: tableSearch.frame, collectionViewLayout: collectinLayout, catId: catIdMovie, del: self, array: arrayMovies, header: nil, displayType:false, galleryType:.myGallery)
                self.view.addSubview(universalCollectionViewMovies)
                isSecondTabLoaded = true
                
                //If search is active then perform search
                if self.searchController!.isActive &&  searchController.searchBar.text?.characters.count > 0
                {
                    
                    performSearch()
                    return
                }
                else
                {
                    searchController.searchBar.text = ""
                    searchController.searchBar.resignFirstResponder()
                    searchController.isActive = false
                }
                
                self.reloadMediaGallery(catIdMovie) //Movies
            }
            else
            {
                //If search is active then perform search
                if self.searchController!.isActive &&  searchController.searchBar.text?.characters.count > 0
                {
                    performSearch()
                    return
                }
                else{
                    
                    searchController.searchBar.text = ""
                    searchController.searchBar.resignFirstResponder()
                    searchController.isActive = false
                }
                
                self.universalCollectionViewMovies.setContentOffset(CGPoint.zero, animated: true)
                self.universalCollectionViewMovies.isHidden = false
                self.universalCollectionViewMovies.arrayCollection = arrayMovies
                self.universalCollectionViewMovies.reloadData()
               
                if arrayMovies.count <= 0
                {
                    self.reloadMediaGallery(catIdMovie)
                }
                else {
                    self.hideErrorMessage()
                }
            }
        }
        else if selectedIndex == 2 //Music
        {
            offsetMusic = 0
            currentPageMusic = 1
            
            btnPeople.setImage(Utilities().themedImage(img_usersSearchGallery), for: UIControlState())
            btnMusic.setImage(Utilities().themedImage(img_musicSelectedSearchGallery), for: UIControlState())
            btnMovies.setImage(Utilities().themedImage(img_filmSearchGallery), for: UIControlState())
            btnVideoGames.setImage(Utilities().themedImage(img_gamesSearchGallery), for: UIControlState())
            

            tableSearch.isHidden = true
            
            if(universalCollectionViewMovies != nil)
            {
                universalCollectionViewMovies.isHidden = true
            }
            
            if(universalCollectionViewVideoGames != nil)
            {
                universalCollectionViewVideoGames.isHidden = true
            }
            
            if universalCollectionViewMusic == nil || isFourthTabLoaded == false
            {
                let collectinLayout = UICollectionViewFlowLayout()
                universalCollectionViewMusic = UniversalCollectionView.init(frame: tableSearch.frame, collectionViewLayout: collectinLayout, catId: catIdMusic, del: self, array: arrayMusic, header: nil, displayType:false, galleryType:.myGallery)
                self.view.addSubview(universalCollectionViewMusic)
                isFourthTabLoaded = true
                universalCollectionViewMusic.isMusicTabSelected = true
                //If search is active then perform search
                if self.searchController!.isActive &&  searchController.searchBar.text?.characters.count > 0
                {
                    
                    performSearch()
                    return
                }
                else
                {
                    searchController.searchBar.text = ""
                    searchController.searchBar.resignFirstResponder()
                    searchController.isActive = false
                }
                
                self.reloadMediaGallery(catIdMusic) //Music
            }
            else
            {
                //If search is active then perform search
               if self.searchController!.isActive &&  searchController.searchBar.text?.characters.count > 0
               {
                
                    performSearch()
                    return
                }
                else
                {
                    searchController.searchBar.text = ""
                    searchController.searchBar.resignFirstResponder()
                    searchController.isActive = false
                }
                
                universalCollectionViewMusic.isMusicTabSelected = true
                universalCollectionViewMusic.isHidden = false
                self.universalCollectionViewMusic.arrayCollection = arrayMusic
                self.universalCollectionViewMusic.reloadData()
                
                if arrayMusic.count <= 0
                {
                    self.reloadMediaGallery(catIdMusic)
                }
                else {
                    self.hideErrorMessage()
                }
            }
        }
        else if selectedIndex == 3 //Video Games
        {
            offsetVideoGames = 0
            currentPageVideoGames = 1
            
            btnPeople.setImage(Utilities().themedImage(img_usersSearchGallery), for: UIControlState())
            btnMusic.setImage(Utilities().themedImage(img_musicSearchGallery), for: UIControlState())
            btnMovies.setImage(Utilities().themedImage(img_filmSearchGallery), for: UIControlState())
            btnVideoGames.setImage(Utilities().themedImage(img_gamesSelectedSearchGallery), for: UIControlState())
            

            tableSearch.isHidden = true
            
            if(universalCollectionViewMovies != nil)
            {
                universalCollectionViewMovies.isHidden = true
            }
            
            if universalCollectionViewMusic != nil
            {
                universalCollectionViewMusic.isHidden = true
            }
            
            if universalCollectionViewVideoGames == nil || isThirdTabLoaded == false
            {
                let collectinLayout = UICollectionViewFlowLayout()
                universalCollectionViewVideoGames = UniversalCollectionView.init(frame: tableSearch.frame, collectionViewLayout: collectinLayout, catId: catIdVideoGames, del: self, array: arrayVideoGames, header: nil, displayType:false, galleryType:.myGallery)
                self.view.addSubview(universalCollectionViewVideoGames)
                isThirdTabLoaded = true
                
                //If search is active then perform search
                if self.searchController!.isActive &&  searchController.searchBar.text?.characters.count > 0
                {
                    
                    performSearch()
                    return
                }
                else
                {
                    searchController.searchBar.text = ""
                    searchController.searchBar.resignFirstResponder()
                    searchController.isActive = false
                }
                
                self.reloadMediaGallery(catIdVideoGames) //Video Games
            }
            else
            {
                //If search is active then perform search
                if self.searchController!.isActive &&  searchController.searchBar.text?.characters.count > 0
                {
                    
                    performSearch()
                    return
                }
                else
                {
                    searchController.searchBar.text = ""
                    searchController.searchBar.resignFirstResponder()
                    searchController.isActive = false
                }
                
                universalCollectionViewVideoGames.isHidden = false
                self.universalCollectionViewVideoGames.arrayCollection = arrayVideoGames
                self.universalCollectionViewVideoGames.reloadData()
             
                if arrayVideoGames.count <= 0
                {
                    self.reloadMediaGallery(catIdVideoGames)
                }
                else {
                    self.hideErrorMessage()
                }
            }
        }
    }
    
    //Reload Media (or pull to refresh)
    func reloadMediaGallery(_ categoryId:Int) {
        
        //Category Id (1 Music 2 Movie 3 Photo 4 Video Game
        
        if categoryId == 1 //Music
        {
            self.catIdMusic = categoryId
            self.currentPageMusic = 1
            self.offsetMusic = 0
        }
        else if categoryId == 2 //Movie
        {
            self.catIdMovie = categoryId
            self.currentPageMovies = 1
            self.offsetMovies = 0
        }
        else if categoryId == 4 //Video Games
        {
            self.catIdVideoGames = categoryId
            self.currentPageVideoGames = 1
            self.offsetVideoGames = 0
        }
        
        self.getPublicMedia(categoryId)
    }
    
    func loadMoreMediaGallery(_ page:NSInteger,offset:Int, categoryId:Int){
        
        //Category Id (1 Music 2 Movie 3 Photo 4 Video Game
        
        if categoryId == 1 //Music
        {
            self.catIdMusic = categoryId
            self.currentPageMusic = page
            self.offsetMusic = offset
        }
        else if categoryId == 2 //Movie
        {
            self.catIdMovie = categoryId
            self.currentPageMovies = page
            self.offsetMovies = offset
        }
        else if categoryId == 4 //Video Games
        {
            self.catIdVideoGames = categoryId
            self.currentPageVideoGames = page
            self.offsetVideoGames = offset
        }

       self.getPublicMedia(categoryId)
    }
    
    func moveToNextScreenGallery(_ ScreenName:String, WithData:AnyObject)
    {
        if(ScreenName == "SingleDetailVc")
        {
            
            let st = UIStoryboard(name: "Home", bundle:nil)
            let singleDetailVc = st.instantiateViewController(withIdentifier: "SingleDetailVc") as! SingleDetailVc
            
            var arr = [Media]()
            arr.append(WithData as! Media)
            singleDetailVc.arrMedia = arr
            self.navigationController?.pushViewController(singleDetailVc, animated: true)
        }
    }
    
    // MARK:- UISearchBar delegate
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar)
    {
        searchCancel()
    }
    
    func searchCancel(){
        
        APIManager.sharedInstance.cancelAllRequests()
        
        searchController.searchBar.text = ""
        searchController.searchBar.resignFirstResponder()
        searchController.isActive = false
        
        if selectedIndex == 0
        {
            filterArrayPeople.removeAll()
            self.tableSearch.reloadData()
        }
        else if selectedIndex == 1 //Movies
        {
            filterArrayMedia.removeAll()
            self.universalCollectionViewMovies.arrayCollection = arrayMovies
            self.universalCollectionViewMovies.reloadData()
        }
        else if selectedIndex == 2  //Music
        {
            filterArrayMedia.removeAll()
            self.universalCollectionViewMusic.arrayCollection = arrayMusic
            self.universalCollectionViewMusic.reloadData()
        }
        else if selectedIndex == 3  //Video Games
        {
            filterArrayMedia.removeAll()
            self.universalCollectionViewVideoGames.arrayCollection = arrayVideoGames
            self.universalCollectionViewVideoGames.reloadData()
        }
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar){
        
        
        searchController.searchBar.text = ""
        searchController.isActive = true
        
        if selectedIndex == 0
        {
            filterArrayPeople.removeAll()
            self.tableSearch.reloadData()
        }
        else if selectedIndex == 1 //Movies
        {
            filterArrayMedia.removeAll()
            self.universalCollectionViewMovies.arrayCollection = filterArrayMedia
            self.universalCollectionViewMovies.reloadData()
        }
        else if selectedIndex == 2  //Music
        {
            filterArrayMedia.removeAll()
            self.universalCollectionViewMusic.arrayCollection = filterArrayMedia
            self.universalCollectionViewMusic.reloadData()
        }
        else if selectedIndex == 3  //Video Games
        {
            filterArrayMedia.removeAll()
            self.universalCollectionViewVideoGames.arrayCollection = filterArrayMedia
            self.universalCollectionViewVideoGames.reloadData()
        }
    }
    
    // MARK:- UISearchResultsUpdating delegate
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar){
        
        performSearch()
    }
    
    func performSearch(){
        
        if searchController.searchBar.text?.characters.count > 0
        {
            if selectedIndex == 0 //People
            {
                filterArrayPeople.removeAll(keepingCapacity: false)
                if self.searchController.searchBar.text!.characters.count > 0
                {
                    offsetPeople = 0
                    currentPagePeople = 1
                    
                    searchPeoples()
                }
                else
                {
                    filterArrayPeople = arrayPeople
                    
                    self.tableSearch.reloadData()
                }
            }
            else if selectedIndex == 1  //Movies
            {
                
                filterArrayMedia.removeAll(keepingCapacity: false)
                if self.searchController.searchBar.text!.characters.count > 0
                {
                    offsetMovies = 0
                    currentPageMovies = 1
                    
                    searchPublicMedia(catIdMovie) //Movies
                }
                else
                {
                    filterArrayMedia = arrayMovies
                }
                
                self.universalCollectionViewMovies?.reloadUniversalCollectionView(filterArrayMedia)
            }
            else if selectedIndex == 2  //Music
            {
                
                filterArrayMedia.removeAll(keepingCapacity: false)
                if self.searchController.searchBar.text!.characters.count > 0
                {
                    offsetMusic = 0
                    currentPageMusic = 1
                    
                    searchPublicMedia(catIdMusic) //Music
                }
                else
                {
                    filterArrayMedia = arrayMovies
                }
                
                self.universalCollectionViewVideoGames?.reloadUniversalCollectionView(filterArrayMedia)
            }
            else if selectedIndex == 3 //Video Games
            {
                
                filterArrayMedia.removeAll(keepingCapacity: false)
                if self.searchController.searchBar.text!.characters.count > 0
                {
                    offsetVideoGames = 0
                    currentPageVideoGames = 1
                    
                    searchPublicMedia(catIdVideoGames) //Video Games
                }
                else
                {
                    filterArrayMedia = arrayMovies
                }
                
                self.universalCollectionViewMusic?.reloadUniversalCollectionView(filterArrayMedia)
            }
        }
        else
        {
            self.showCommonAlert(alertMsg_Comment)
        }
    }
    
    //MARK:- UserGallery Navigation Action
    @IBAction func btnUserGalleryNavigation(_ sender: AnyObject){
        
         let btn = sender as! UIButton
        
        let st = UIStoryboard(name: "Gallery", bundle:nil)
        let galleryObject = st.instantiateViewController(withIdentifier: "GalleryMainViewController") as? GalleryMainViewController
        galleryObject?.otherUserId = btn.tag
        self.navigationController?.pushViewController(galleryObject!, animated: true)
    }
    
    // MARK:- UITableViewDataSource delegate
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
   
        return 80.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchController.isActive
        {
            return filterArrayPeople.count
        }
        else
        {
            return arrayPeople.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //Load More
        if (arrayPeople.count >= Limit) && (indexPath.row + 1  == arrayPeople.count) {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "LoadMoreCell")
            
            if let lbl5176 =  cell?.viewWithTag(5176) as? UILabel
            {
                lbl5176.text = loading_Title
                
                if (totalRecordsPeople) > (currentPagePeople) * Limit {
                    
                    offsetPeople = (currentPagePeople) * Limit
                    currentPagePeople += 1
                    getPeoples()
                    
                }
                else
                {
                    lbl5176.text = loadingNoMoreData
                    
                    lbl5176.isHidden = true
                    
                    return refreshCellForRow(indexPath)
                }
            }
            
            return cell!
        }
            
        else{
            
            return refreshCellForRow(indexPath)
        }
    }
    
    func refreshCellForRow(_ indexPath:IndexPath)->SearchTableCell
    {
        let cell = self.tableSearch.dequeueReusableCell( withIdentifier: "SearchCell", for: indexPath) as! SearchTableCell
        
        cell.backgroundColor = UIColor.clear
        cell.selectionStyle = .none
        cell.tag = indexPath.row
        
        let people: People!
        if searchController.isActive
        {
            if filterArrayPeople.count <= indexPath.row
            {
                return cell
            }
            
            people = filterArrayPeople[(indexPath as IndexPath).row]
        }
        else
        {
            if arrayPeople.count <= indexPath.row
            {
                return cell
            }
            
            people = arrayPeople[(indexPath as IndexPath).row]
        }
        
        cell.imageView_user!.image = Utilities().themedImage(img_defaultUserBig)
        cell.imageView_user!.contentMode = .scaleAspectFit
        if let img = imageCache[people.pUserImage] {
            cell.imageView_user!.image = img
        }
        else if imageCache[people.pUserImage] == nil && people.pUserImage != ""
        {
            //Downloading user's image
            URLSession.shared.dataTask(with: URL(string: people.pUserImage)!, completionHandler: { (data, response, error) -> Void in
                
                if error != nil {
                    
                    cell.imageView_user!.image = Utilities().themedImage(img_defaultUserBig)

                    print(error)
                    return
                }
                
                DispatchQueue.main.async(execute: { () -> Void in
                    let image = UIImage(data: data!)
                    if (image == nil)
                    {
                        cell.imageView_user!.image = Utilities().themedImage(img_defaultUserBig)
                    }
                    else
                    {
                        cell.imageView_user!.image = image
                        self.imageCache[people.pUserImage] = cell.imageView_user!.image
                    }
                })
            }).resume()
        }
        
        cell.labelName.font = ProximaNovaRegular(15)
        cell.labelName!.text = people.pUserName
        
        cell.labelPost.font = ProximaNovaRegular(14)
        if people.pPost > 1{
            cell.labelPost!.text = NSString.init(format:"%d Posts", people.pPost) as String
        }
        else{
            cell.labelPost!.text = NSString.init(format:"%d Post", people.pPost) as String
        }
        
        cell.buttonFollow.tag = indexPath.row
        cell.buttonFollow.addTarget(self, action: #selector(SearchViewController.addbuttonTapped(_:)), for: .touchUpInside)
        
        if people.pFriendStatus == "Unknown"
        {
            cell.buttonFollow.setImage(Utilities().themedImage(img_followSelected), for: UIControlState())
            cell.buttonFollow.setImage(Utilities().themedImage(img_follow), for: .highlighted)
        }
        else if people.pFriendStatus == "Approved"
        {
            cell.buttonFollow.setImage(Utilities().themedImage(img_following), for: UIControlState())
            cell.buttonFollow.setImage(Utilities().themedImage(img_followingSelected), for: .highlighted)
        }
        else if people.pFriendStatus == "Pending"
        {
            cell.buttonFollow.setImage(Utilities().themedImage(img_pending), for: UIControlState())
            cell.buttonFollow.setImage(Utilities().themedImage(img_pendingSelected), for: .highlighted)
        }
        
        
        if people.pId ==  Int(Utilities.getUserDetails().id)
        {
            cell.buttonFollow.isHidden = true
        }
        else
        {
            cell.buttonFollow.isHidden = false
        }
        
        
        cell.btnUserImage.addTarget(self, action: #selector(btnUserGalleryNavigation), for: .touchUpInside)
        cell.btnUserImage.tag = people.pId //set userid as button tag
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    
    var heightForIndexPath = [IndexPath: CGFloat]()
    var averageRowHeight: CGFloat = 465 //your best estimate
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        heightForIndexPath[indexPath] = cell.frame.height
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return heightForIndexPath[indexPath] ?? averageRowHeight
    }
    
     // MARK:- Follow Button Click
    
    func addbuttonTapped(_ sender: AnyObject)
    {
        if reachability?.currentReachabilityStatus == .notReachable {
            self.showCommonAlert(alertMsg_NoInternetConnection)
        }
        else {
        let button = sender as! UIButton
        
        let people: People!
        if searchController.isActive
        {
            people = filterArrayPeople[button.tag]
        }
        else
        {
            people = arrayPeople[button.tag]
        }
        
        //If user is not already followed
        if people.pFriendStatus == "Unknown"
        {
            people.pFriendStatus = "Pending"
            
            //Refreshing tble cell
            let buttonPosition = sender.convert(CGPoint.zero, to: tableSearch)
            let indexPath = tableSearch.indexPathForRow(at: buttonPosition)! as IndexPath
            tableSearch.reloadRows(at: [indexPath], with: UITableViewRowAnimation.fade)
        
            //Calling API
            followPeople(people.pId)
        }
        else if people.pFriendStatus == "Approved"
        {

            //Create the AlertController
            let actionSheetController: UIAlertController = UIAlertController(title: "Unfollow " + people.pUserName + "?", message: "", preferredStyle: .actionSheet)
            
            //Create and add the Cancel action
            let cancelAction: UIAlertAction = UIAlertAction(title: alertBtnTitle_Cancel, style: .cancel) { action -> Void in
                //Just dismiss the action sheet
            }
            actionSheetController.addAction(cancelAction)
            
            //Create and add first option action
            let unfollowAction: UIAlertAction = UIAlertAction(title: "Unfollow", style: .destructive) { action -> Void in
                
                people.pFriendStatus = "Unknown"
                
                //Refreshing tble cell
                let buttonPosition = sender.convert(CGPoint.zero, to: self.tableSearch)
                let indexPath = self.tableSearch.indexPathForRow(at: buttonPosition)! as IndexPath
                self.tableSearch.reloadRows(at: [indexPath], with: UITableViewRowAnimation.fade)
                
                //Calling API
                self.unfollowPeople(people.pId)
            }
            actionSheetController.addAction(unfollowAction)
            
            
            //Present the AlertController
            self.present(actionSheetController, animated: true, completion: nil)
        }
        else if people.pFriendStatus == "Pending"
        {
            //Create the AlertController
            let actionSheetController: UIAlertController = UIAlertController(title: "Follow Request", message: "", preferredStyle: .actionSheet)
            
            //Create and add the Cancel action
            let cancelAction: UIAlertAction = UIAlertAction(title: alertBtnTitle_Cancel, style: .cancel) { action -> Void in
                //Just dismiss the action sheet
            }
            actionSheetController.addAction(cancelAction)
            
            //Create and add first option action
            let unfollowAction: UIAlertAction = UIAlertAction(title: "Delete Follow Request", style: .destructive) { action -> Void in
                
                people.pFriendStatus = "Unknown"
                
                //Refreshing tble cell
                let buttonPosition = sender.convert(CGPoint.zero, to: self.tableSearch)
                let indexPath = self.tableSearch.indexPathForRow(at: buttonPosition)! as IndexPath
                self.tableSearch.reloadRows(at: [indexPath], with: UITableViewRowAnimation.fade)
                
                //Calling API
                
                self.unfollowPeople(people.pId)
            }
            actionSheetController.addAction(unfollowAction)
            
            
            //Present the AlertController
            self.present(actionSheetController, animated: true, completion: nil)
        }
    }
    }
}

