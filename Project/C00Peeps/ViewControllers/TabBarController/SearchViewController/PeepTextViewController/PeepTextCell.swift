//
//  PeepTextCell.swift
//  C00Peeps
//
//  Created by SOTSYS027 on 3/16/16.
//  Copyright © 2016 SOTSYS011. All rights reserved.
//

import UIKit

class PeepTextCell: UITableViewCell
{
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblSubtitle: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }

}
