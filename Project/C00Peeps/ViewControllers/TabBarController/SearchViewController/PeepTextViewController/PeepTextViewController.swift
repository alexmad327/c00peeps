//
//  PeepTextViewController.swift
//  C00Peeps
//
//  Created by SOTSYS027 on 3/16/16.
//  Copyright © 2016 SOTSYS011. All rights reserved.
//

import UIKit

class PeepTextViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    var namesArray : [String] = ["Lauren Richard", "Nicholas Ray", "Kim White", "Charles Gray", "Timothy Jones", "Sarah Underwood", "William Pearl", "Juan Rodriguez", "Anna Hunt", "Marie Turner", "George Porter", "Zachary Hecker", "David Fletcher"]
    var subTitleArray : [String] = ["The best and most beautiful things in the world cannot be seen or even touched - they must be felt with the heart",
        "The best and most beautiful things in the world cannot be seen or even touched - they must be felt with the heart",
        "The best and most beautiful things in the world cannot be seen or even touched - they must be felt with the heart",
        "The best and most beautiful things in the world cannot be seen or even touched - they must be felt with the heart",
        "The best and most beautiful things in the world cannot be seen or even touched - they must be felt with the heart",
        "The best and most beautiful things in the world cannot be seen or even touched - they must be felt with the heart",
        "The best and most beautiful things in the world cannot be seen or even touched - they must be felt with the heart",
        "The best and most beautiful things in the world cannot be seen or even touched - they must be felt with the heart",
        "The best and most beautiful things in the world cannot be seen or even touched - they must be felt with the heart",
        "The best and most beautiful things in the world cannot be seen or even touched - they must be felt with the heart",
        "The best and most beautiful things in the world cannot be seen or even touched - they must be felt with the heart",
        "The best and most beautiful things in the world cannot be seen or even touched - they must be felt with the heart",
        "The best and most beautiful things in the world cannot be seen or even touched - they must be felt with the heart"]
    
    var photoNameArray : [String] = ["woman5.jpg", "man1.jpg", "woman1.jpg", "man2.jpg", "man3.jpg", "woman2.jpg", "man4.jpg", "man5.jpg", "woman3.jpg", "woman4.jpg", "man6.jpg", "man7.jpg", "man8.jpg"]
    
    @IBOutlet weak var tblView: UITableView!
    
    override func viewDidLoad()
    {
        self.navigationController?.isNavigationBarHidden = true

        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return namesArray .count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PeepCustomCell", for: indexPath) as! PeepTextCell
        cell.lblName.text = namesArray[indexPath.row]
        cell.lblSubtitle.text = subTitleArray[indexPath.row]
        cell.profileImage.image = UIImage(named: photoNameArray[indexPath.row])
        cell.profileImage.layer.masksToBounds = false
        cell.profileImage.layer.cornerRadius = cell.profileImage.frame.height/2
        cell.profileImage.clipsToBounds = true
        
        
        // Configure the cell...
        
        
        return cell;
    }
    
    
}
