//
//  ReloadContent.swift
//  C00Peeps
//
//  Created by Mac on 06/10/16.
//  Copyright © 2016 SOTSYS011. All rights reserved.
//

import Foundation

protocol UpdateContent {
    
    func moveToNextScreenGallery(_ ScreenName:String, WithData:AnyObject)
    func reloadMediaGallery(_ categoryId:Int)
    func loadMoreMediaGallery(_ page:Int, offset:Int, categoryId:Int)

}
