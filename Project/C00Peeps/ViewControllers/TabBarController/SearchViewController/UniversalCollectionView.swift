//
//  UniversalCollectionView.swift
//  C00Peeps
//
//  Created by Mac on 06/10/16.
//  Copyright © 2016 SOTSYS011. All rights reserved.
//
import UIKit
import AVKit
import AVFoundation
import SwiftyJSON
import FBSDKLoginKit
import FBSDKShareKit
import TwitterKit

//MARK: Message Type
public enum ClassType {
    
    case gallery
    case peepDetail
    
}
public enum GalleryType: Int {
    
    case myGallery = 1,allGallery,peepDetailGallery
    
}

let peepTextCellDefaultHeight:CGFloat = 80
let peepTextLabelDefaultHeight:CGFloat = 28
let peepTextMargins:CGFloat = 86

let universalCellDefaultHeight:CGFloat = 545
let universalCellWidthSubtract:CGFloat = 320
let peepTextLabelWithMediaDefaultHeight:CGFloat = 24
let peepTextSideMarginsWithMedia:CGFloat = 20

let widthSubtractConstant:CGFloat = 8
var reachability: Reachability?

class UniversalCollectionView: UICollectionView, UICollectionViewDelegate, UICollectionViewDataSource, PlayMediaDelegate, FBSDKSharingDelegate, DownloadLockedMediaDelegate {
    
    var mediaType:String!
    var currentPage = 1
    var offset = 0
    var searchCategoryId = 1
    var totalRecordCount = 0
    var imageCache = [String:UIImage]()
    var updateDelegate: UpdateContent?
    var arrayCollection = [Media]()
    var headerCollection: UICollectionReusableView?
    var displayListView = false
    var displayPeepText = false
    var displayRetryOption = false
    var indexPathlastPlayed:IndexPath?
    var refreshControlUniversalView: UIRefreshControl!
    var noRecordsFoundTitle = ""
    var showLoadingIndicator = false
    var galleryType = GalleryType.myGallery
    var classType = ClassType.gallery
    var isMusicTabSelected = false
    
    var mediaSharedId: Int = 0
    var mediaSharedOwnerId: Int = 0
    
     let objDownloadLockedMedia = DownloadLockedMedia()
    
    init(frame: CGRect, collectionViewLayout layout: UICollectionViewLayout, catId:Int, del:UpdateContent, array:[Media], header: UICollectionReusableView?, displayType:Bool, galleryType:GalleryType)
    {
        super.init(frame: frame, collectionViewLayout: layout)
        self.frame = frame
        if header != nil
        {
            let layout = self.collectionViewLayout as! UICollectionViewFlowLayout
            //layout.headerReferenceSize = CGSizeMake(0, 242-30);
            layout.headerReferenceSize = CGSize(width: 0, height: (header?.frame.height)!);
            self.headerCollection = header
        }
        
        self.alwaysBounceVertical = true
        addRefreshControl()
        displayListView = displayType
        self.arrayCollection = array
        self.searchCategoryId = catId
        self.updateDelegate = del
        self.galleryType = galleryType
        self.backgroundColor = UIColor.clear
        
        let layout = self.collectionViewLayout as! UICollectionViewFlowLayout
        layout.minimumLineSpacing = 2.0
        layout.minimumInteritemSpacing = 2.0
        layout.sectionInset = UIEdgeInsets(top: 0, left: 2, bottom: 0, right: 2)
        
        //register collection view cell to display Movies, Music, Photos & Video games
        //in grid view on Gallery, Search & post liked Screen (under settings)
        self.register(UniversalCollectionViewCell.self, forCellWithReuseIdentifier: "universal_cell")
        self.register(UINib(nibName: "UniversalCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "universal_cell")
        

        //register list view cell to display Movies, Music, Photos & Video games
        // in expanded/list view on Gallery Screen
        self.register(UniversalCollectionViewCell.self, forCellWithReuseIdentifier: "universal_tablecell")
        self.register(UINib(nibName: "UniversalCollectionViewTableCell", bundle: nil), forCellWithReuseIdentifier:"universal_tablecell")
        
        //register load more cell
        self.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "LoadMoreCell")
        self.register(UINib(nibName: "UniversalCollectionViewLoadMoreCell", bundle: nil), forCellWithReuseIdentifier:"LoadMoreCell")
        
        //register peep text cell to display peep text in grid view on Gallery screen
        self.register(UniversalCollectionViewPeepTextListCell.self, forCellWithReuseIdentifier: "PeepTextListCell")
        self.register(UINib(nibName: "UniversalCollectionViewPeepTextListCell", bundle: nil), forCellWithReuseIdentifier:"PeepTextListCell")
        
        //register peep text cell to display peep text in expanded view on Gallery & Single Peep Text Detail screen
        self.register(UniversalCollectionViewPeepTextExpandedCell.self, forCellWithReuseIdentifier: "PeepTextExpandedCell")
        self.register(UINib(nibName: "UniversalCollectionViewPeepTextExpandedCell", bundle: nil), forCellWithReuseIdentifier:"PeepTextExpandedCell")
 
        //register refresh cell
        self.register(GalleryRefreshCollectionViewCell.self, forCellWithReuseIdentifier: "refreshGalleryCell")
        self.register(UINib(nibName: "GalleryRefreshCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "refreshGalleryCell")

        
        self.delegate  = self
        self.dataSource = self
        
    }
    
    func addRefreshControl(){
        
        refreshControlUniversalView = UIRefreshControl()
        refreshControlUniversalView.tintColor = UIColor.darkGray
        refreshControlUniversalView.attributedTitle = NSAttributedString(string: loaderTitle_PullToRefresh)
        refreshControlUniversalView.addTarget(self, action: #selector(UniversalCollectionView.handleRefresh(_:)), for: UIControlEvents.valueChanged)
        self.addSubview(refreshControlUniversalView)
        
    }
    
    func handleRefresh(_ refreshControlUniversalView: UIRefreshControl) {
        
        refreshControlUniversalView.endRefreshing()
        
        self.updateDelegate?.reloadMediaGallery(self.searchCategoryId)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("This class does not support NSCoding")
    }
    
    func reloadUniversalCollectionView(_ array: [Media])
    {
        arrayCollection = array
        self.reloadData()
    }
    
    // MARK: -  Actions methods
    
    func btnRefreshGalleryClicked() {
        self.updateDelegate?.reloadMediaGallery(self.searchCategoryId)
    }
    
    func btnPlayAction(_ sender: AnyObject){
        
        //Refreshing tble cell
        let buttonPosition = sender.convert(CGPoint.zero, to: self)
        let indexPath = self.indexPathForItem(at: buttonPosition)! as IndexPath
        
        var cell = UniversalCollectionViewTableCell()
        var cellPeepTextDetail = UniversalCollectionViewPeepTextExpandedCell()
        
        if displayPeepText == true {
            
            cellPeepTextDetail = self.cellForItem(at: indexPath) as! UniversalCollectionViewPeepTextExpandedCell
            cellPeepTextDetail.delegate = self
            
        }
        else
        {
            cell = self.cellForItem(at: indexPath) as! UniversalCollectionViewTableCell
            cell.delegate = self
            
        }
        
        
        let btn = sender as! UIButton
        
        //get media obj from index
        let md = arrayCollection[btn.tag]
        
        //stop previous video item if any
        stopPreviousPlayedVideo(indexPath)
        
        indexPathlastPlayed = indexPath
        
        //If audio attachment exist
        if md.pMediaAttachmentType == 2
        {
            if displayPeepText == true
            {
                cellPeepTextDetail.cellTag = btn.tag
            }
            else
            {
                cell.cellTag = btn.tag
            }
            
            
            if md.pIsPlaying == 0
            {
                if md.pIsLocked == 1
                {
                    var localPath = ""
                    
                    //if media is sent media
                    if String(md.pUserId) == Utilities.getUserDetails().id
                    {
                        localPath = Utilities().getLocalMediaPath(md.pMediaAttachment, mediaAttachmentType: 2, folderName:localSentFolderName)
                    }
                        
                        //if media is received media
                    else
                    {
                        localPath = Utilities().getLocalMediaPath(md.pMediaAttachment, mediaAttachmentType: 2, folderName: localReceivedFolderName)
                    }
                    
                    if localPath.characters.count > 0
                    {
                        if displayPeepText == true
                        {
                            cellPeepTextDetail.loadVideo(URL.init(fileURLWithPath: localPath))
                            
                        }
                        else
                        {
                            cell.loadVideo(URL.init(fileURLWithPath: localPath))
                        }
                    }
                    else
                    {
                        
                       
                    }
                }
                else
                {
                    let soundURL = URL.init(string: md.pMediaAttachment)
                    
                    if displayPeepText == true
                    {
                        cellPeepTextDetail.loadVideo(soundURL!)
                    }
                    else
                    {
                        cell.loadVideo(soundURL!)
                    }
                }
            }
            else
            {
                if displayPeepText == true
                {
                    cellPeepTextDetail.stopVideo()
                }
                else
                {
                    cell.stopVideo()
                }
            }
            
        }
            //If video attachment exist
        else if md.pMediaAttachmentType == 3
        {
            
            if displayPeepText == true
            {
                cellPeepTextDetail.cellTag = btn.tag
            }
            else
            {
                cell.cellTag = btn.tag
            }
            
            if md.pIsPlaying == 0
            {
                if md.pIsLocked == 1
                {
                    var localPath = ""
                    
                    //if media is sent media
                    if String(md.pUserId) == Utilities.getUserDetails().id
                    {
                        localPath = Utilities().getLocalMediaPath(md.pMediaAttachment, mediaAttachmentType: 3, folderName:localSentFolderName)
                    }
                        
                        //if media is received media
                    else
                    {
                        localPath = Utilities().getLocalMediaPath(md.pMediaAttachment, mediaAttachmentType: 3, folderName: localReceivedFolderName)
                    }
                    
                    if localPath.characters.count > 0
                    {
                        if displayPeepText == true
                        {
                            cellPeepTextDetail.loadVideo(URL.init(fileURLWithPath: localPath))
                            
                        }
                        else
                        {
                            cell.loadVideo(URL.init(fileURLWithPath: localPath))
                        }
                    }
                    else
                    {
                    
                    }
                }
                else
                {
                    let videoURL = URL.init(string: md.pMediaAttachment)
                    
                    if displayPeepText == true
                    {
                        cellPeepTextDetail.loadVideo(videoURL!)
                    }
                    else
                    {
                        cell.loadVideo(videoURL!)
                    }
                }
            }
            else
            {
                if displayPeepText == true
                {
                    cellPeepTextDetail.stopVideo()
                }
                else
                {
                    cell.stopVideo()
                }
            }
        }
    }
    
    func playFinished(_ cellTag:Int)
    {
        let md = arrayCollection[cellTag]
        md.pIsPlaying = 0
        indexPathlastPlayed = nil
    }
    
    func playStart(_ cellTag:Int)
    {
        let md = arrayCollection[cellTag]
        md.pIsPlaying = 1
    }
  
    //MARK:- Like Action
    func btnLikeAction(_ sender: AnyObject){
        if reachability?.currentReachabilityStatus == .notReachable {
            self.showCommonAlert(alertMsg_NoInternetConnection)
        }
        else {
        
            let btn = sender as! UIButton
            
            //get media obj from index
            let md = arrayCollection[btn.tag]
            
            //If media is not already liked
            if md.pIsLiked == 0
            {
                if(md.pCountDislikes != 0 && md.pIsDisliked == 1)
                {
                    md.pCountDislikes -= 1
                }
                
                md.pIsLiked = 1
                md.pIsDisliked = 0
                md.pCountLikes += 1
                
                
            }
            else //If media is already liked
            {
                md.pIsLiked = 0
                
                if(md.pCountLikes != 0)
                {
                    md.pCountLikes -= 1
                }
            }
            
            //Refreshing tble cell
            let buttonPosition = sender.convert(CGPoint.zero, to: self)
            let indexPath = self.indexPathForItem(at: buttonPosition)! as IndexPath
            
            var arrIndexPath = [IndexPath]()
            arrIndexPath.append(indexPath)
            self.reloadItems(at: arrIndexPath)
            
            //Calling API
            likeMediaRequest(md.pId,likerId: md.pUserId)
            
        }
    }
    
    //MARK:- Like Count Action
    func btnLikeCountAction(_ sender: AnyObject){
        
        let btn = sender as! UIButton
        //get media obj from index
        let md = arrayCollection[btn.tag]
        if (md.pCountLikes != 0) {
            let button = sender as! UIButton
            let obj = arrayCollection[button.tag]
            if classType == .gallery {
            self.updateDelegate!.moveToNextScreenGallery("LikersViewController", WithData: obj)
            }
            if classType == .peepDetail {
                self.updateDelegate!.moveToNextScreenGallery("LikersViewController", WithData: obj)
            }
        }
        else{
            let alert = UIAlertController(title: "Alert", message: "No like exist.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            
            if classType == .gallery {
                let objView = self.updateDelegate as! GalleryMainViewController
                objView.present(alert, animated: true, completion: nil)
                
            }
            else if classType == .peepDetail {
                let objView = self.updateDelegate as! PeepTextDetailCollectionViewController
                objView.present(alert, animated: true, completion: nil)
                
            }
        }
    }
    
    //MARK:- Dislike Count Action
    func btnDisLikeCountAction(_ sender: AnyObject){
        
        let btn = sender as! UIButton
        //get media obj from index
        let md = arrayCollection[btn.tag]
        if (md.pCountDislikes != 0) {
            let button = sender as! UIButton
            let obj = arrayCollection[button.tag]
            if classType == .gallery {
                self.updateDelegate!.moveToNextScreenGallery("DisLikersViewController", WithData: obj)
            }
            if classType == .peepDetail {
                self.updateDelegate!.moveToNextScreenGallery("DisLikersViewController", WithData: obj)
            }
        }
        else{
            let alert = UIAlertController(title: "Alert", message: "No dislikes exist.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            
            if classType == .gallery {
                let objView = self.updateDelegate as! GalleryMainViewController
                objView.present(alert, animated: true, completion: nil)
                
            }
            else if classType == .peepDetail {
                let objView = self.updateDelegate as! PeepTextDetailCollectionViewController
                objView.present(alert, animated: true, completion: nil)
                
            }
        }
    }
    
    //MARK:- Dislike Action
    func btnDislikeAction(_ sender: AnyObject){
        if reachability?.currentReachabilityStatus == .notReachable {
            self.showCommonAlert(alertMsg_NoInternetConnection)
        }
        else {
        
            let btn = sender as! UIButton
            
            //get media obj from index
            let md = arrayCollection[btn.tag]
            
            //If media was not already disliked
            if md.pIsDisliked == 0
            {
                
                if(md.pCountLikes != 0 && md.pIsLiked == 1)
                {
                    md.pCountLikes -= 1
                }
                
                md.pCountDislikes += 1
                md.pIsDisliked = 1
                md.pIsLiked = 0
                
                
            }
            else  //If media was already disliked
            {
                md.pIsDisliked = 0
                
                if(md.pCountDislikes != 0)
                {
                    md.pCountDislikes -= 1
                }
            }

            //Refreshing tble cell
            let buttonPosition = sender.convert(CGPoint.zero, to: self)
            let indexPath = self.indexPathForItem(at: buttonPosition)! as IndexPath
            
            var arrIndexPath = [IndexPath]()
            arrIndexPath.append(indexPath)
            self.reloadItems(at: arrIndexPath)
            
            //Calling API
            dislikeMediaRequest(md.pId,likerId: md.pUserId)
        }
    }
    
    //MARK:- Comment Action
    func btnCommentAction(_ sender: AnyObject){
        
        //get media obj from index
        let buttonPosition = sender.convert(CGPoint.zero, to: self)
        let indexPath = self.indexPathForItem(at: buttonPosition)! as IndexPath
        let obj = arrayCollection[indexPath.row]
        
        if displayPeepText == true && classType == .gallery  {
            self.updateDelegate!.moveToNextScreenGallery("PeepTextDetailCollectionViewController", WithData: obj)
            
        }
        else if classType == .gallery{
            self.updateDelegate!.moveToNextScreenGallery("CommentVc", WithData: obj)
        }
    }
    
    //MARK:- Share Action
    func btnShareAction(_ sender: AnyObject){
        
        //get media obj from index
        let buttonPosition = sender.convert(CGPoint.zero, to: self)
        let indexPath = self.indexPathForItem(at: buttonPosition)! as IndexPath
        
        let obj = arrayCollection[indexPath.row]
        
        //can't share this media if this is locked or belong to private user
        if obj.pIsLocked == 1 || obj.pPrivacyLevelId == 2
        {
            let alert = UIAlertController(title: "Alert", message: "Can not share this media", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            if classType == .gallery {
                let objView = self.updateDelegate as! GalleryMainViewController
                objView.present(alert, animated: true, completion: nil)
                
            }
            else if classType == .peepDetail {
                let objView = self.updateDelegate as! PeepTextDetailCollectionViewController
                objView.present(alert, animated: true, completion: nil)
                
            }
        }
        else  if obj.pIsLocked == 0 && obj.pPrivacyLevelId == 3
        {
            //can share this medis if this is unlocked and belong to public user
            
            //Redirect to Contact screen for forwarding purpose
            self.updateDelegate!.moveToNextScreenGallery("ForwardMedia", WithData: obj)
        }
    }
    
    //MARK:- UserGallery Navigation Action
    @IBAction func btnUserGalleryNavigation(_ sender: AnyObject){
        
        let buttonPosition = sender.convert(CGPoint.zero, to: self)
        let indexPath = self.indexPathForItem(at: buttonPosition)! as IndexPath
        let obj = arrayCollection[indexPath.row]

        self.updateDelegate!.moveToNextScreenGallery("GalleryMainViewController", WithData: obj)
    }
    
    //MARK:- Report Media Action
    func btnReportMedia(_ sender: AnyObject){
        
        let btn = sender as! UIButton
        let md = arrayCollection[btn.tag]
        
        //Create the AlertController
        let actionSheetController: UIAlertController = UIAlertController(title: alertMsg_SelectCategory, message: "", preferredStyle: .actionSheet)
        
        //Create and add the Cancel action
        let cancelAction: UIAlertAction = UIAlertAction(title: alertBtnTitle_Cancel, style: .cancel) { action -> Void in
            //Just dismiss the action sheet
        }
        actionSheetController.addAction(cancelAction)
        
        //Add report media button for other user's post
        if md.pUserId != Int(Utilities.getUserDetails().id)
        {
            //Create and add a fourth option action
            let report: UIAlertAction = UIAlertAction(title: alertMsg_Report, style: .destructive) { action -> Void in
                
                self.reportMediaRequest(md.pId, ownerId: md.pUserId)
                
            }
            actionSheetController.addAction(report)
        }
        
        self.mediaSharedId = md.pId
        self.mediaSharedOwnerId = md.pUserId
        
        //Create and add first option action
        let shareFacebook: UIAlertAction = UIAlertAction(title: alertMsg_ShareToFacebook, style: .default) { action -> Void in
            self.shareLinkToFacebook(md.pSlug)
            
        }
        actionSheetController.addAction(shareFacebook)
        
        
        //Create and add a second option action
        let tweet: UIAlertAction = UIAlertAction(title: alertMsg_Tweet, style: .default) { action -> Void in
            self.tweetLinkOnTwitter(md.pSlug)
            
        }
        actionSheetController.addAction(tweet)
        
        //Create and add a third option action
        let copyShareURL: UIAlertAction = UIAlertAction(title: alertMsg_CopyShareURL, style: .default) { action -> Void in
            self.copyLink(md.pSlug)
        }
        actionSheetController.addAction(copyShareURL)
        if classType == .gallery {
            let objView = self.updateDelegate as! GalleryMainViewController
            objView.present(actionSheetController, animated: true, completion: nil)
            
        }
        else if classType == .peepDetail {
            let objView = self.updateDelegate as! PeepTextDetailCollectionViewController
            objView.present(actionSheetController, animated: true, completion: nil)
            
        }
    }
    
    // MARK: - Share/Tweet/Copy Link
    func shareLinkToFacebook(_ link:String) {
        self.shareSocialMediaLinkOnFB(link)
       /* let facebookURL: NSURL = NSURL(string: "fb://")!
        if UIApplication.sharedApplication().canOpenURL(facebookURL) {
            if FBSDKAccessToken.currentAccessToken() != nil {
                self.shareSocialMediaLinkOnFB(link)
            }
            else {
                let facebookLogin = FBSDKLoginManager()
                let objView = self.updateDelegate as! GalleryMainViewController
                facebookLogin.logInWithReadPermissions(["email"],  fromViewController: objView) { (facebookResult: FBSDKLoginManagerLoginResult!, facebookError: NSError!) -> Void in
                    if !facebookResult.isCancelled {
                        self.shareSocialMediaLinkOnFB(link)
                    }
                }
            }
        }
        else {
            let alert:UIAlertController = UIAlertController(title: "", message: alertMsg_DownloadFacebookToShareLink, preferredStyle: .Alert)
            let alertAction = UIAlertAction(title: alertBtnTitle_OK, style: .Default, handler: { (UIAlertAction) -> Void in
                // open app store.
                self.openFacebookOnAppStore()
            })
            
            alert.addAction(alertAction)
            if classType == .Gallery {
                let objView = self.updateDelegate as! GalleryMainViewController
                objView.presentViewController(alert, animated: true, completion: nil)
                
            }
            else if classType == .PeepDetail {
                let objView = self.updateDelegate as! PeepTextDetailCollectionViewController
                objView.presentViewController(alert, animated: true, completion: nil)
                
            }
        }*/
    }
    
    func tweetLinkOnTwitter(_ link:String) {
        let composer = TWTRComposer()
        composer.setURL(URL(string: link))
        var objView:UIViewController!
        
        if classType == .gallery {
            objView = self.updateDelegate as! GalleryMainViewController
            
        }
        else if classType == .peepDetail {
            objView = self.updateDelegate as! PeepTextDetailCollectionViewController
            
        }        // Called from a UIViewController
        composer.show(from: objView) { result in
            if (result == TWTRComposerResult.cancelled) {
                printCustom("Tweet composition cancelled")
            }
            else {
                printCustom("Sending tweet!")
                self.mediaShared(self.mediaSharedId, mediaOwnerId: self.mediaSharedOwnerId, platform: AccountType.twitter)
            }
        }
    }
    
    func copyLink(_ link:String) {
        UIPasteboard.general.string = link
    }
    
    //MARK: - Share On FB/ Open AppStore
    func shareSocialMediaLinkOnFB(_ link:String) {
        let url:URL = URL(string: link)!
        var objView:UIViewController!
        let facebookShareDialog: FBSDKShareDialog = Utilities().getShareDialogWithContentURL(url)
        if classType == .gallery {
            objView = self.updateDelegate as! GalleryMainViewController
            
        }
        else if classType == .peepDetail {
            objView = self.updateDelegate as! PeepTextDetailCollectionViewController
            
        }
        if facebookShareDialog.canShow() {
            FBSDKShareDialog.show(from: objView.parent, with: Utilities().getShareLinkContentWithContentURL(url), delegate: self)
        }
        else {
            
            let alert = UIAlertController(title: "Alert", message:alertMsg_InvalidFBShareableLink, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            
            if classType == .gallery {
                let objView = self.updateDelegate as! GalleryMainViewController
                objView.present(alert, animated: true, completion: nil)
                
            }
            else if classType == .peepDetail {
                let objView = self.updateDelegate as! PeepTextDetailCollectionViewController
                objView.present(alert, animated: true, completion: nil)
                
            }
            
        }
    }
    
    /*func openFacebookOnAppStore() {
        // open app store.
        let facebookAppStoreURL = "https://itunes.apple.com/in/app/facebook/id284882215?mt=8"
        let appStoreURL:NSURL = NSURL(string: facebookAppStoreURL)!
        var objView:UIViewController!
        
        if classType == .Gallery {
            objView = self.updateDelegate as! GalleryMainViewController
            
        }
        else if classType == .PeepDetail {
            objView = self.updateDelegate as! PeepTextDetailCollectionViewController
            
        }
        if UIApplication.sharedApplication().canOpenURL(appStoreURL) {
            UIApplication.sharedApplication().openURL(appStoreURL)
        }
        else {
            
            let alert = UIAlertController(title: "Alert", message:alertMsg_DownloadFacebookToSendAppInvites, preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
            
            objView.presentViewController(alert, animated: true, completion: nil)
            
        }
    }*/
    
    // MARK: - FBSDKSharing Delegate
    func sharer(_ sharer: FBSDKSharing!, didCompleteWithResults results: [AnyHashable: Any]!) {
        printCustom("didCompleteWithResults results:\(results)")
        if results.count > 0 {
            self.mediaShared(self.mediaSharedId, mediaOwnerId: self.mediaSharedOwnerId, platform: AccountType.facebook)
        }
    }
    
    func sharer(_ sharer: FBSDKSharing!, didFailWithError error: Error!) {
        //printCustom("didFailWithError error:\(error.description)")
    }
    
    func sharerDidCancel(_ sharer: FBSDKSharing!) {
        printCustom("sharerDidCancel")
    }
    
    // MARK: - Media Shared
    func mediaShared(_ mediaId:Int, mediaOwnerId:Int, platform:AccountType){
        var parameters = [String: AnyObject]()
        parameters["media_id"] = mediaId as AnyObject
        parameters["sent_to"] = mediaOwnerId as AnyObject
        parameters["sent_by"] = Utilities.getUserDetails().id as AnyObject
        parameters["shared_on"] = platform.rawValue as AnyObject
        
        APIManager.sharedInstance.shareMedia(parameters, Target: self)
    }
    
    // MARK: - Media Shared Response
    func mediaSharedResponse (_ notify: Foundation.Notification) {
        //NSNotificationCenter.defaultCenter().removeObserver(self, name: NOTIFICATION_SHAREMEDIA, object: nil)
        
        let swiftyJsonVar = JSON(notify.object!)
        
        let message = swiftyJsonVar["message"].stringValue
        let status = swiftyJsonVar["status"].intValue
        let response = swiftyJsonVar["response"].stringValue
        
        if swiftyJsonVar [ERROR_KEY].exists() {
            printCustom("response:\(swiftyJsonVar[ERROR_KEY].stringValue)")
        }
        else if (status == 0) //API message response
        {
            printCustom("response:\(message)")
        }
        else if (status == 1) //success response
        {
            printCustom("response:\(response)")
        }
    }
    
    // MARK: - Like Media
    func likeMediaRequest(_ mediaId:Int, likerId:Int){
        //ApplicationDelegate.showLoader(loaderTitle_LikeMedia)
        var parameters = [String: AnyObject]()
        
        //User Id(Owner of the feed)
        parameters["user_id"] = likerId as AnyObject
        
        //mediaID
        parameters["media_id"] = mediaId as AnyObject
        
        //User Id who wants to like/dislike a feed
        parameters["liker_id"] = Utilities.getUserDetails().id as AnyObject
        
        //1-like, 2-dislike
        parameters["like_status"] = 1 as AnyObject
        if displayPeepText == true{
            APIManager.sharedInstance.requestLikePeepText(parameters, Target: self)
        }
        else{
            APIManager.sharedInstance.requestLike(parameters, Target: self)
        }
    }
    
    // MARK: - Like Media Response
    func likeMediaResponse (_ notify: Foundation.Notification)
    {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFIACTION_LIKE), object: nil)
        
        let swiftyJsonVar = JSON(notify.object!)
        
        let message = swiftyJsonVar["message"].stringValue
        let status = swiftyJsonVar["status"].intValue
        
        if swiftyJsonVar [ERROR_KEY].exists()
        {
            
            let alert = UIAlertController(title: "Alert", message:swiftyJsonVar[ERROR_KEY].stringValue, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            
            if classType == .gallery {
                let objView = self.updateDelegate as! GalleryMainViewController
                objView.present(alert, animated: true, completion: nil)
                
            }
            else if classType == .peepDetail {
                let objView = self.updateDelegate as! PeepTextDetailCollectionViewController
                objView.present(alert, animated: true, completion: nil)
                
            }
            
        }
        else if (status == 0) //API message response
        {
            let alert = UIAlertController(title: "Alert", message:message, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            
            if classType == .gallery {
                let objView = self.updateDelegate as! GalleryMainViewController
                objView.present(alert, animated: true, completion: nil)
                
            }
            else if classType == .peepDetail {
                let objView = self.updateDelegate as! PeepTextDetailCollectionViewController
                objView.present(alert, animated: true, completion: nil)
                
            }
            
        }
        else if (status == 1) //success response
        {
            
        }
    }
    
    // MARK: - Like Peep Text Response
    func likePeepTextResponse (_ notify: Foundation.Notification)
    {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFIACTION_LIKE_PEEPTEXT), object: nil)
        
        //ApplicationDelegate.hideLoader ()
        
        let swiftyJsonVar = JSON(notify.object!)
        
        let message = swiftyJsonVar["message"].stringValue
        let status = swiftyJsonVar["status"].intValue
        
        if swiftyJsonVar [ERROR_KEY].exists()
        {
            
            let alert = UIAlertController(title: "Alert", message:swiftyJsonVar[ERROR_KEY].stringValue, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            
            if classType == .gallery {
                let objView = self.updateDelegate as! GalleryMainViewController
                objView.present(alert, animated: true, completion: nil)
                
            }
            else if classType == .peepDetail {
                let objView = self.updateDelegate as! PeepTextDetailCollectionViewController
                objView.present(alert, animated: true, completion: nil)
                
            }
            
        }
        else if (status == 0) //API message response
        {
            let alert = UIAlertController(title: "Alert", message:message, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            
            if classType == .gallery {
                let objView = self.updateDelegate as! GalleryMainViewController
                objView.present(alert, animated: true, completion: nil)
                
            }
            else if classType == .peepDetail {
                let objView = self.updateDelegate as! PeepTextDetailCollectionViewController
                objView.present(alert, animated: true, completion: nil)
                
            }
            
        }
        else if (status == 1) //success response
        {
            
        }
    }
    
    // MARK: - Dislike Media
    func dislikeMediaRequest(_ mediaId:Int, likerId:Int){
        //ApplicationDelegate.showLoader(loaderTitle_DislikeMedia)
        var parameters = [String: AnyObject]()
        
        //User Id(Owner of the feed)
        parameters["user_id"] = likerId as AnyObject
        
        ////mediaID
        parameters["media_id"] = mediaId as AnyObject
        
        //User Id who wants to like/dislike a feed
        parameters["liker_id"] = Utilities.getUserDetails().id as AnyObject
        
        //1-like, 2-dislike
        parameters["like_status"] = 2 as AnyObject
        if displayPeepText == true{
            APIManager.sharedInstance.requestDislikePeepText(parameters, Target: self)
        }
        else{
            APIManager.sharedInstance.requestDislike(parameters, Target: self)
        }
    }
    
    // MARK: - Dislike Media Response
    func dislikeMediaResponse (_ notify: Foundation.Notification)
    {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFIACTION_DISLIKE), object: nil)
        
        //ApplicationDelegate.hideLoader ()
        
        let swiftyJsonVar = JSON(notify.object!)
        
        let message = swiftyJsonVar["message"].stringValue
        let status = swiftyJsonVar["status"].intValue
        
        if swiftyJsonVar [ERROR_KEY].exists()
        {
            let alert = UIAlertController(title: "Alert", message:swiftyJsonVar[ERROR_KEY].stringValue, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            if classType == .gallery {
                let objView = self.updateDelegate as! GalleryMainViewController
                objView.present(alert, animated: true, completion: nil)
                
            }
            else if classType == .peepDetail {
                let objView = self.updateDelegate as! PeepTextDetailCollectionViewController
                objView.present(alert, animated: true, completion: nil)
                
            }
        }
        else if (status == 0) //API message response
        {
            let alert = UIAlertController(title: "Alert", message:message, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            if classType == .gallery {
                let objView = self.updateDelegate as! GalleryMainViewController
                objView.present(alert, animated: true, completion: nil)
                
            }
            else if classType == .peepDetail {
                let objView = self.updateDelegate as! PeepTextDetailCollectionViewController
                objView.present(alert, animated: true, completion: nil)
                
            }
        }
        else if (status == 1) //success response
        {
            
        }
    }
    
    // MARK: - Dislike PeepText Response
    func dislikePeepTextResponse (_ notify: Foundation.Notification)
    {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFIACTION_DISLIKE_PEEPTEXT), object: nil)
        
        //ApplicationDelegate.hideLoader ()
        
        let swiftyJsonVar = JSON(notify.object!)
        
        let message = swiftyJsonVar["message"].stringValue
        let status = swiftyJsonVar["status"].intValue
        
        if swiftyJsonVar [ERROR_KEY].exists()
        {
            let alert = UIAlertController(title: "Alert", message:swiftyJsonVar[ERROR_KEY].stringValue, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            if classType == .gallery {
                let objView = self.updateDelegate as! GalleryMainViewController
                objView.present(alert, animated: true, completion: nil)
                
            }
            else if classType == .peepDetail {
                let objView = self.updateDelegate as! PeepTextDetailCollectionViewController
                objView.present(alert, animated: true, completion: nil)
                
            }
        }
        else if (status == 0) //API message response
        {
            let alert = UIAlertController(title: "Alert", message:message, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            if classType == .gallery {
                let objView = self.updateDelegate as! GalleryMainViewController
                objView.present(alert, animated: true, completion: nil)
                
            }
            else if classType == .peepDetail {
                let objView = self.updateDelegate as! PeepTextDetailCollectionViewController
                objView.present(alert, animated: true, completion: nil)
                
            }
        }
        else if (status == 1) //success response
        {
            
        }
    }
    
    // MARK: - Report Media
    func reportMediaRequest(_ mediaId:Int, ownerId:Int){
        
        //ApplicationDelegate.showLoader(loaderTitle_ReportinMedia)
        var parameters = [String: AnyObject]()
        parameters["user_id"] = Utilities.getUserDetails().id as AnyObject
        parameters["media_id"] = mediaId as AnyObject
        parameters["owner_id"] = ownerId as AnyObject
        
        APIManager.sharedInstance.reportMedia(parameters, Target: self)
    }
    
    // MARK: - Report Media Response
    func reportMediaResponse (_ notify: Foundation.Notification)
    {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFIACTION_REPORTMEDIA), object: nil)
        
        //ApplicationDelegate.hideLoader ()
        
        let swiftyJsonVar = JSON(notify.object!)
        
        let message = swiftyJsonVar["message"].stringValue
        let status = swiftyJsonVar["status"].intValue
        
        if swiftyJsonVar [ERROR_KEY].exists()
        {
            let alert = UIAlertController(title: "Alert", message:swiftyJsonVar[ERROR_KEY].stringValue, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            
            if classType == .gallery {
                let objView = self.updateDelegate as! GalleryMainViewController
                objView.present(alert, animated: true, completion: nil)
                
            }
            else if classType == .peepDetail {
                let objView = self.updateDelegate as! PeepTextDetailCollectionViewController
                objView.present(alert, animated: true, completion: nil)
                
            }
        }
        else if (status == 0) //API message response
        {
            let alert = UIAlertController(title: "Alert", message:message, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            
            if classType == .gallery {
                let objView = self.updateDelegate as! GalleryMainViewController
                objView.present(alert, animated: true, completion: nil)
                
            }
            else if classType == .peepDetail {
                let objView = self.updateDelegate as! PeepTextDetailCollectionViewController
                objView.present(alert, animated: true, completion: nil)
                
            }        }
        else if (status == 1) //success response
        {
            let alert = UIAlertController(title: "", message: swiftyJsonVar["response"].stringValue, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler:nil))
            
            let objView = self.updateDelegate as! GalleryMainViewController
            objView.present(alert, animated: true, completion: nil)
        }
    }
    
    func stopPreviousPlayedVideo(_ indexP:IndexPath)
    {
        if (indexPathlastPlayed != nil && indexPathlastPlayed != indexP) {
            
            if displayPeepText == true {
                
                let cellPeepTextDetail = self.cellForItem(at: self.indexPathlastPlayed!) as! UniversalCollectionViewPeepTextExpandedCell
                cellPeepTextDetail.stopVideo()
                
            }
            else
            {
                let cellSel = self.cellForItem(at: self.indexPathlastPlayed!) as! UniversalCollectionViewTableCell
                cellSel.stopVideo()
            }
            
            
            indexPathlastPlayed = nil;
        }
    }
    
    //MARK:- Configure Cell Action Methods
    
    func configureCellActionEvents(_ cell1:AnyObject){
        
        if cell1.isKind(of: UniversalCollectionViewTableCell.self)
        {
            let cell = cell1 as! UniversalCollectionViewTableCell
            
            cell.btnLike.addTarget(self, action: #selector(btnLikeAction), for: .touchUpInside)
            cell.btnLikesCount.addTarget(self, action: #selector(btnLikeCountAction), for: .touchUpInside)
            cell.btnDislikeCount.addTarget(self, action: #selector(btnDisLikeCountAction), for: .touchUpInside)
            cell.btnDislike.addTarget(self, action: #selector(btnDislikeAction), for: .touchUpInside)
            cell.btnComments.addTarget(self, action: #selector(btnCommentAction), for: .touchUpInside)
            cell.btnCommentCount.addTarget(self, action: #selector(btnCommentAction), for: .touchUpInside)
            cell.btnShare.addTarget(self, action: #selector(btnShareAction), for: .touchUpInside)
            cell.btnPlay.addTarget(self, action: #selector(btnPlayAction), for: .touchUpInside)
            cell.btnReportMedia.addTarget(self, action: #selector(btnReportMedia), for: .touchUpInside)
            cell.btnUserImage.addTarget(self, action: #selector(btnUserGalleryNavigation), for: .touchUpInside)
        }
        else if cell1.isKind(of: UniversalCollectionViewPeepTextExpandedCell.self)
        {
            let cell = cell1 as! UniversalCollectionViewPeepTextExpandedCell
            
            cell.btnLike.addTarget(self, action: #selector(btnLikeAction), for: .touchUpInside)
            cell.btnDislike.addTarget(self, action: #selector(btnDislikeAction), for: .touchUpInside)
            
            cell.btnLikesCount.addTarget(self, action: #selector(btnLikeCountAction), for: .touchUpInside)
            cell.btnDislikeCount.addTarget(self, action: #selector(btnDisLikeCountAction), for: .touchUpInside)
            
            cell.btnShare.addTarget(self, action: #selector(btnShareAction), for: .touchUpInside)
            cell.btnPlay.addTarget(self, action: #selector(btnPlayAction), for: .touchUpInside)
            cell.btnReportMedia.addTarget(self, action: #selector(btnReportMedia), for: .touchUpInside)
            cell.btnUserImage.addTarget(self, action: #selector(btnUserGalleryNavigation), for: .touchUpInside)
        }
    }
    
    
    
    
    //MARK:- Displaying list/expanded view cell for PeepText categories
    
    func refreshCellForPeepTextExpandedRow(_ indexPath:IndexPath)->UniversalCollectionViewPeepTextExpandedCell
    {
        let cell = self.dequeueReusableCell(withReuseIdentifier: "PeepTextExpandedCell", for: indexPath) as! UniversalCollectionViewPeepTextExpandedCell
        
        //Configure Cell Action Methods
        configureCellActionEvents(cell)
        
        let media = arrayCollection[(indexPath as IndexPath).row]
        
        //Loading User profile image
        cell.imgProfile.image = Utilities().themedImage(img_defaultUserBig)
        cell.imgProfile!.contentMode = .scaleAspectFit
        cell.imgProfile.layer.masksToBounds = false
        cell.imgProfile.layer.cornerRadius = cell.imgProfile.frame.height/2
        cell.imgProfile.clipsToBounds = true
        
        if media.pUserImage.isEmpty == true
        {
            cell.imgProfile!.image = UIImage.init(named: img_defaultUserBig)
        }
        if let img = imageCache[media.pUserImage] {
            cell.imgProfile!.image = img
        }
        else if imageCache[media.pUserImage] == nil && media.pUserImage != ""
        {
            //Downloading user's image
            URLSession.shared.dataTask(with: URL(string: media.pUserImage)!, completionHandler: { (data, response, error) -> Void in
                
                if error != nil {
                    
                    cell.imgProfile!.image = Utilities().themedImage(img_defaultUserBig)
                    
                    print(error)
                    return
                }
                
                DispatchQueue.main.async(execute: { () -> Void in
                    let image = UIImage(data: data!)
                    
                    if (image == nil)
                    {
                        cell.imgProfile!.image = Utilities().themedImage(img_defaultUserBig)
                    }
                    else
                    {
                        cell.imgProfile!.image = image
                        self.imageCache[media.pUserImage] = cell.imgProfile!.image
                    }
                })
            }).resume()
        }
        
        //No attachment only peep text
        if media.pMediaAttachmentType == 0
        {
            cell.likecountViewTopWithPeepTextViewBottom.priority = UILayoutPriority(rawValue: 999.0)
            cell.layoutLikecountViewTopWithMediaImgBottom.priority = UILayoutPriority(rawValue: 900.0)
            cell.imgMedia.isHidden = true
            cell.btnPlay.isHidden = true
        }
            //play button enable when a peep text has an video or audio attachment
        else if media.pMediaAttachmentType == 3 || media.pMediaAttachmentType == 2
        {
            cell.likecountViewTopWithPeepTextViewBottom.priority = UILayoutPriority(rawValue: 900.0)
            cell.layoutLikecountViewTopWithMediaImgBottom.priority = UILayoutPriority(rawValue: 999.0)
            
            cell.imgMedia.isHidden = false
            cell.btnPlay.isHidden = false
            cell.btnPlay.setImage(Utilities().themedImage(img_videoPlay), for: UIControlState())
        }
            //only photo as an attachment
        else if media.pMediaAttachmentType == 1
        {
            cell.likecountViewTopWithPeepTextViewBottom.priority = UILayoutPriority(rawValue: 900.0)
            cell.layoutLikecountViewTopWithMediaImgBottom.priority = UILayoutPriority(rawValue: 999.0)
            
            cell.imgMedia.isHidden = false
            cell.btnPlay.isHidden = true
        }
        
        if media.pIsLocked == 1
        {
            var localPath = ""
            
            //if media is sent media
            if String(media.pUserId) == Utilities.getUserDetails().id
            {
                localPath = Utilities().getLocalMediaPath(media.pMediaFile, mediaAttachmentType: 0, folderName:localSentFolderName)
            }
                
                //if media is received media
            else
            {
                localPath = Utilities().getLocalMediaPath(media.pMediaFile, mediaAttachmentType: 0, folderName: localReceivedFolderName)
            }
            
            if localPath.characters.count > 0
            {
                let img = UIImage.init(contentsOfFile: localPath)
                
                if img != nil
                {
                    cell.imgMedia.image = img
                }
                else
                {
                     cell.btnPlay.isHidden = true
                    cell.imgMedia.image = Utilities().themedImage(img_unlockMedia)
                }
            }
            else
            {
                 cell.btnPlay.isHidden = true
                cell.imgMedia.image = Utilities().themedImage(img_unlockMedia)
            }

        }
        else
        {
            //Loading Media image
            cell.imgMedia.image = Utilities().themedImage(img_movieImage)
            cell.imgMedia!.contentMode = .scaleAspectFit
            
            if let img = imageCache[media.pMediaFile] {
                cell.imgMedia!.image = img
            }
            else if imageCache[media.pMediaFile] == nil && media.pMediaFile != ""
            {
                //Downloading media image
                URLSession.shared.dataTask(with: URL(string: media.pMediaFile)!, completionHandler: { (data, response, error) -> Void in
                    
                    if error != nil {
                        
                        cell.imgMedia!.image = Utilities().themedImage(img_movieImage)
                        
                        print(error)
                        return
                    }
                    
                    DispatchQueue.main.async(execute: { () -> Void in
                        let image = UIImage(data: data!)
                        
                        if image == nil
                        {
                            cell.imgMedia!.image = Utilities().themedImage(img_movieImage)
                        }
                        else
                        {
                            cell.imgMedia!.image = image
                            self.imageCache[media.pMediaFile] = cell.imgMedia!.image
                        }
                        
                    })
                }).resume()
            }
        }
        
        
        // peep text
        cell.lblPeepText.font = ProximaNovaRegular(14)
        if media.pPeepText.isEmpty == false
        {
            cell.lblPeepText.text = media.pPeepText
        }
        else
        {
            cell.lblPeepText.text = "NA"
        }
        
        
        //isliked
        if media.pIsLiked == 1
        {
            cell.btnLike.setImage(Utilities().themedImage(img_likeHighlighted), for: UIControlState())
        }
        else
        {
            cell.btnLike.setImage(Utilities().themedImage(img_liked), for: UIControlState())
        }
        
        //is dislike
        if media.pIsDisliked == 1
        {
            cell.btnDislike.setImage(Utilities().themedImage(img_dislikeHighlighted), for: UIControlState())
        }
        else
        {
            cell.btnDislike.setImage(Utilities().themedImage(img_disliked), for: UIControlState())
        }
        
        //like count
        
        var strLikes = ""
        if media.pCountLikes == 1 || media.pCountLikes == 0
        {
            strLikes = "like"
        }
        else
        {
            strLikes = "likes"
        }
        
        cell.btnLikesCount.setTitle(String.init(format:"%d %@",media.pCountLikes, strLikes), for: UIControlState())
        
        //dislike count
        
        var strDislikes = ""
        if media.pCountDislikes == 1 || media.pCountDislikes == 0
        {
            strDislikes = "dislike"
        }
        else
        {
            strDislikes = "dislikes"
        }

        
        cell.btnDislikeCount.setTitle(String.init(format:"%d %@",media.pCountDislikes, strDislikes), for: UIControlState())
        
        //username
        
        if(self.galleryType == .myGallery)
        {
            var username = ""
            
            //if user is anonymous
            if media.pSentToPrivacyLevelId == 4
            {
                username = anonymousUsername + String(media.pSentUserId)
            }
            else
            {
                username = media.pSentUserName
            }
            
            cell.lblUserName.text = "You" + " Vs " + username
            
        }
        else  if(self.galleryType == .allGallery)
        {
            var username = ""
            
            //if user is anonymous
            if media.pPrivacyLevelId == 4
            {
                username = anonymousUsername + String(media.pUserId)
            }
            else
            {
                username = media.pUserName
            }
            
            cell.lblUserName.text = username + " Vs " + "You"
            
        }
        else  if(self.galleryType == .peepDetailGallery)
        {
            var username = ""
            
            //if user is anonymous
            if media.pPrivacyLevelId == 4
            {
                username = anonymousUsername + String(media.pUserId)
            }
            else
            {
                username = media.pUserName
            }
            
            cell.lblUserName.text = username
            
        }
        
        cell.lblUserName.font = ProximaNovaRegular(15)
        
        //creation date
        cell.lblPostedDate.font = ProximaNovaRegular(14)
        
        if media.pCreatedDate.isEmpty == false
        {
            cell.lblPostedDate.text = Utilities().getDateAgo(media.pCreatedDate)
        }
        else
        {
            cell.lblPostedDate.text = "NA"
        }
        
        //button tags
        cell.btnPlay.tag = indexPath.row
        cell.btnLike.tag = indexPath.row
        cell.btnDislike.tag = indexPath.row
        
        cell.btnReportMedia.tag = indexPath.row
        cell.btnShare.tag = indexPath.row
        cell.btnLikesCount.tag = indexPath.row
        cell.btnDislikeCount.tag = indexPath.row
        
        return cell
    }
    
    //MARK:- Displaying grid view cell for PeepText categories
    
    func refreshCellForPeepTextGridRow(_ indexPath:IndexPath)->UniversalCollectionViewPeepTextListCell
    {
    
        let cell = self.dequeueReusableCell(withReuseIdentifier: "PeepTextListCell", for: indexPath) as! UniversalCollectionViewPeepTextListCell
        
        let media = arrayCollection[(indexPath as IndexPath).row]
        
        //Loading User profile image
        cell.imgProfile.image = Utilities().themedImage(img_defaultUserBig)
        cell.imgProfile!.contentMode = .scaleAspectFit
        cell.imgProfile.layer.masksToBounds = false
        cell.imgProfile.layer.cornerRadius = cell.imgProfile.frame.height/2
        cell.imgProfile.clipsToBounds = true
        
         if media.pUserImage.isEmpty == true
        {
            cell.imgProfile!.image = UIImage.init(named: img_defaultUserBig)
        }
        else if let img = imageCache[media.pUserImage] {
            cell.imgProfile!.image = img
        }
        else if imageCache[media.pUserImage] == nil
        {
            //Downloading user's image
            URLSession.shared.dataTask(with: URL(string: media.pUserImage)!, completionHandler: { (data, response, error) -> Void in
                
                if error != nil {
                    
                    cell.imgProfile!.image = Utilities().themedImage(img_defaultUserBig)
                    
                    print(error)
                    return
                }
                
                DispatchQueue.main.async(execute: { () -> Void in
                    let image = UIImage(data: data!)
                    
                    if (image == nil)
                    {
                        cell.imgProfile!.image = Utilities().themedImage(img_defaultUserBig)
                    }
                    else
                    {
                        cell.imgProfile!.image = image
                        self.imageCache[media.pUserImage] = cell.imgProfile!.image
                    }
                })
            }).resume()
        }
        
        //username
        
        if(self.galleryType == .myGallery)
        {
            var username = ""
            
            //if user is anonymous
            if media.pSentToPrivacyLevelId == 4
            {
                username = anonymousUsername + String(media.pSentUserId)
            }
            else
            {
                username = media.pSentUserName
            }

            cell.lblUserName.text = "You" + " Vs " + username
        }
        else if(self.galleryType == .allGallery){
            
            var username = ""
            
            //if user is anonymous
            if media.pPrivacyLevelId == 4
            {
                username = anonymousUsername + String(media.pUserId)
            }
            else
            {
                username = media.pUserName
            }
            
            cell.lblUserName.text = username + " Vs " + "You"
            
        }
        else if(self.galleryType == .peepDetailGallery){
                        
            var username = ""
            
            //if user is anonymous
            if media.pPrivacyLevelId == 4
            {
                username = anonymousUsername + String(media.pUserId)
            }
            else
            {
                username = media.pUserName
            }
            
            cell.lblUserName.text = username
            
        }
        cell.lblUserName.font = ProximaNovaRegular(15)
        
        //PeepText
        cell.lblPeepText.font = ProximaNovaRegular(14)
        if media.pPeepText.isEmpty == false
        {
            cell.lblPeepText.text =  media.pPeepText
        }
        else
        {
            cell.lblPeepText.text = "NA"
        }
        
        //creation date
        cell.lblPostedDate.font = ProximaNovaRegular(14)
        if media.pCreatedDate.isEmpty == false
        {
            cell.lblPostedDate.text = Utilities().getDateAgo(media.pCreatedDate)
        }
        else
        {
            cell.lblPostedDate.text = "NA"
        }
        
        return cell
    }
    
    //MARK:- Displaying list/expanded view cell of Movie, Photo, Music, Video games
    
    func refreshCellForListRow(_ indexPath:IndexPath)->UniversalCollectionViewTableCell
    {
        let cell = self.dequeueReusableCell(withReuseIdentifier: "universal_tablecell", for: indexPath) as! UniversalCollectionViewTableCell
        
        //Configure Cell Action Methods
        configureCellActionEvents(cell)
        
        let media = arrayCollection[(indexPath as IndexPath).row]
        
        //Loading User profile image
        cell.imgProfile.image = Utilities().themedImage(img_defaultUserBig)
        cell.imgProfile!.contentMode = .scaleAspectFit
        cell.imgProfile.layer.masksToBounds = false
        cell.imgProfile.layer.cornerRadius = cell.imgProfile.frame.height/2
        cell.imgProfile.clipsToBounds = true
        
        if media.pUserImage.isEmpty == true
        {
            cell.imgProfile!.image = UIImage.init(named: img_defaultUserBig)
        }
        if let img = imageCache[media.pUserImage] {
            cell.imgProfile!.image = img
        }
        else if imageCache[media.pUserImage] == nil
        {
            //Downloading user's image
            URLSession.shared.dataTask(with: URL(string: media.pUserImage)!, completionHandler: { (data, response, error) -> Void in
                
                if error != nil {
                    
                    cell.imgProfile!.image = Utilities().themedImage(img_defaultUserBig)
                    
                    print(error)
                    return
                }
                
                DispatchQueue.main.async(execute: { () -> Void in
                    let image = UIImage(data: data!)
                    
                    if (image == nil)
                    {
                        cell.imgProfile!.image = Utilities().themedImage(img_defaultUserBig)
                    }
                    else
                    {
                        cell.imgProfile!.image = image
                        self.imageCache[media.pUserImage] = cell.imgProfile!.image
                    }
                })
            }).resume()
        }
        
        if media.pIsLocked == 1
        {
            var localPath = ""
            
            //if media is sent media
            if String(media.pUserId) == Utilities.getUserDetails().id
            {
                localPath = Utilities().getLocalMediaPath(media.pMediaFile, mediaAttachmentType: 0, folderName:localSentFolderName)
            }
            
            //if media is received media
            else
            {
                localPath = Utilities().getLocalMediaPath(media.pMediaFile, mediaAttachmentType: 0, folderName: localReceivedFolderName)
            }
            
            if localPath.characters.count > 0
            {
                let img = UIImage.init(contentsOfFile: localPath)
                
                if img != nil
                {
                    cell.imgMedia.image = img
                    
                    if media.pMediaAttachmentType == 3 || media.pMediaAttachmentType == 2
                    {
                        cell.btnPlay.isHidden = false
                        cell.btnPlay.setImage(Utilities().themedImage(img_videoPlay), for: UIControlState())
                    }
                }
                else
                {
                    cell.btnPlay.isHidden = true
                    cell.imgMedia.image = Utilities().themedImage(img_unlockMedia)
                }
            }
            else
            {
                cell.btnPlay.isHidden = true
                cell.imgMedia.image = Utilities().themedImage(img_unlockMedia)
            }
        }
        else
        {
            //play button enable when a photo has an video or audio attachment
            if media.pMediaAttachmentType == 3 || media.pMediaAttachmentType == 2
            {
                cell.btnPlay.isHidden = false
                cell.btnPlay.setImage(Utilities().themedImage(img_videoPlay), for: UIControlState())
            }
            else
            {
                cell.btnPlay.isHidden = true
            }
            
            //Loading Media image
            if self.isMusicTabSelected {
                cell.imgMedia.image = Utilities().themedImage(img_musicImage)
            }
            else {
                cell.imgMedia.image = Utilities().themedImage(img_movieImage)
            }
            cell.imgMedia!.contentMode = .scaleAspectFit
            
            if let img = imageCache[media.pMediaFile] {
                cell.imgMedia!.image = img
            }
            else if imageCache[media.pMediaFile] == nil
            {
                //Downloading media image
                URLSession.shared.dataTask(with: URL(string: media.pMediaFile)!, completionHandler: { (data, response, error) -> Void in
                    
                    if error != nil {
                        
                        //Loading Media image
                        if self.isMusicTabSelected {
                            cell.imgMedia.image = Utilities().themedImage(img_musicImage)
                        }
                        else {
                            cell.imgMedia.image = Utilities().themedImage(img_movieImage)
                        }
                        
                        print(error)
                        return
                    }
                    
                    DispatchQueue.main.async(execute: { () -> Void in
                        let image = UIImage(data: data!)
                        
                        if image == nil
                        {
                            //Loading Media image
                            if self.isMusicTabSelected {
                                cell.imgMedia.image = Utilities().themedImage(img_musicImage)
                            }
                            else {
                                cell.imgMedia.image = Utilities().themedImage(img_movieImage)
                            }
                        }
                        else
                        {
                            cell.imgMedia!.image = image
                            self.imageCache[media.pMediaFile] = cell.imgMedia!.image
                        }
                        
                    })
                }).resume()
            }
        }
        
        
        if displayPeepText == true{
            
            //media peep text
            cell.lblMediaCaption.font = ProximaNovaRegular(15)
            if media.pPeepText.isEmpty == false
            {
                cell.lblMediaCaption.text = media.pPeepText
            }
            else
            {
                cell.lblMediaCaption.text = "NA"
            }
        }
        else{
            
            //media caption
            cell.lblMediaCaption.font = ProximaNovaRegular(15)
            if media.pMediaCaption.isEmpty == false
            {
                cell.lblMediaCaption.text = media.pMediaCaption
            }
            else
            {
                cell.lblMediaCaption.text = "NA"
            }
        }
        
        
        //isliked
        if media.pIsLiked == 1
        {
            cell.btnLike.setImage(Utilities().themedImage(img_likeHighlighted), for: UIControlState())
        }
        else
        {
            cell.btnLike.setImage(Utilities().themedImage(img_liked), for: UIControlState())
        }
        
        //is dislike
        if media.pIsDisliked == 1
        {
            cell.btnDislike.setImage(Utilities().themedImage(img_dislikeHighlighted), for: UIControlState())
        }
        else
        {
            cell.btnDislike.setImage(Utilities().themedImage(img_disliked), for: UIControlState())
        }
        
        
        //comment icon highlighted
        
        if media.pCountComments > 0
        {
            cell.btnComments.setImage(Utilities().themedImage(img_commentHighlighted), for: UIControlState())
        }
        else
        {
            cell.btnComments.setImage(Utilities().themedImage(img_commentsIcon), for: UIControlState())
        }
        
        //comment & label count
        
        var strComments = ""
        if media.pCountComments == 1
        {
            strComments = "comment"
        }
        else
        {
            strComments = "comments"
        }
        
        cell.btnCommentCount.setTitle(String.init(format:"%d %@",media.pCountComments, strComments), for: UIControlState())
        
        //like count
        var strLikes = ""
        if media.pCountLikes == 1 || media.pCountLikes == 0
        {
            strLikes = "like"
        }
        else
        {
            strLikes = "likes"
        }
        
        cell.btnLikesCount.setTitle(String.init(format:"%d %@",media.pCountLikes, strLikes), for: UIControlState())
        
        //dislike count
        var strDisLikes = ""
        if media.pCountDislikes == 1 || media.pCountDislikes == 0
        {
            strDisLikes = "dislike"
        }
        else
        {
            strDisLikes = "dislikes"
        }
    
        cell.btnDislikeCount.setTitle(String.init(format:"%d %@",media.pCountDislikes, strDisLikes), for: UIControlState())
        
        
        //username
        var username = ""
        
        //if user is anonymous
        if media.pPrivacyLevelId == 4
        {
            username = anonymousUsername + String(media.pUserId)
        }
        else
        {
            username = media.pUserName
        }
        cell.lblUserName.text = username
        cell.lblUserName.font = ProximaNovaRegular(15)
        
        //expiry time
        cell.lblExpiryTime.font = ProximaNovaRegular(14)
        if media.pExpiryDate.isEmpty == false
        {
            cell.lblExpiryTime.text = Utilities().getFutureDateAgo(media.pExpiryDate)
        }
        else
        {
            cell.lblExpiryTime.text = "NA"
        }
        
        //creation date
        cell.lblPostedDate.font = ProximaNovaRegular(14)
        if media.pCreatedDate.isEmpty == false
        {
            cell.lblPostedDate.text = Utilities().getDateAgo(media.pCreatedDate)
        }
        else
        {
            cell.lblPostedDate.text = "NA"
        }
        
        //button tags
        cell.btnPlay.tag = indexPath.row
        cell.btnLike.tag = indexPath.row
        cell.btnDislike.tag = indexPath.row
        cell.btnComments.tag = indexPath.row
        cell.btnReportMedia.tag = indexPath.row
        cell.btnShare.tag = indexPath.row
        cell.btnLikesCount.tag = indexPath.row
        cell.btnDislikeCount.tag = indexPath.row

        return cell
    }
    
    //MARK:- Displaying grid view cell of Movie, Photo, Music & Video games categories
    
    func refreshCellForCollectionRow(_ indexPath:IndexPath)->UniversalCollectionViewCell
    {
        if let cell = self.dequeueReusableCell(withReuseIdentifier: "universal_cell", for: indexPath) as? UniversalCollectionViewCell
        {
            cell.tag = indexPath.row
            
            let media = arrayCollection[(indexPath as IndexPath).row]
            cell.backgroundColor = UIColor.clear
            
            cell.buttonSource.tag = indexPath.row
            
            //play button enable when a photo has a video attachment
            if media.pMediaAttachmentType == 3
            {
                cell.imageType.isHidden = false
                cell.imageType.image = Utilities().themedImage(img_videoIcon)
            }
                //play button enable when a photo has an audio attachment
            else if media.pMediaAttachmentType == 2
            {
                cell.imageType.isHidden = false
                cell.imageType.image = Utilities().themedImage(img_audioIcon)
            }
            else
            {
                cell.imageType.isHidden = true
            }
            
            if media.pIsLocked == 1
            {
                var localPath = ""
                
                //if media is sent media
                if String(media.pUserId) == Utilities.getUserDetails().id
                {
                    localPath = Utilities().getLocalMediaPath(media.pMediaFile, mediaAttachmentType: 0, folderName:localSentFolderName)
                }
                    
                    //if media is received media
                else
                {
                    localPath = Utilities().getLocalMediaPath(media.pMediaFile, mediaAttachmentType: 0, folderName: localReceivedFolderName)
                }
                
                //found media downloaded locally in document directory
                if localPath.characters.count > 0
                {
                    let img = UIImage.init(contentsOfFile: localPath)
                    
                    if img != nil
                    {
                        cell.buttonSource.image = img
                    }
                    else
                    {
                        cell.imageType.isHidden = true
                        cell.buttonSource.image = Utilities().themedImage(img_unlockMediaThumbnail)
                    }
                }
                else
                {
                    cell.imageType.isHidden = true
                    cell.buttonSource.image = Utilities().themedImage(img_unlockMediaThumbnail)
                }
                
            }
            else
            {
                //Loading Media image
                if self.isMusicTabSelected {
                    cell.buttonSource.image = Utilities().themedImage(img_musicThumbnail)
                }
                else {
                    cell.buttonSource.image = Utilities().themedImage(img_movieThumbnail)
                }
                cell.buttonSource!.contentMode = .scaleAspectFit
                
                if media.pMediaFile.contains(".png")
                {
                    if let img = imageCache[media.pMediaFile] {
                        cell.buttonSource!.image = img
                    }
                    else if imageCache[media.pMediaFile] == nil
                    {
                        //Downloading media image
                        URLSession.shared.dataTask(with: URL(string: media.pMediaFile)!, completionHandler: { (data, response, error) -> Void in
                            
                            if error != nil {
                                
                                if self.isMusicTabSelected {
                                    cell.buttonSource.image = Utilities().themedImage(img_musicThumbnail)
                                }
                                else {
                                    cell.buttonSource.image = Utilities().themedImage(img_movieThumbnail)
                                }
                                
                                print(error)
                                return
                            }
                            
                            DispatchQueue.main.async(execute: { () -> Void in
                                let image = UIImage(data: data!)
                                
                                if image == nil
                                {
                                    if self.isMusicTabSelected {
                                        cell.buttonSource.image = Utilities().themedImage(img_musicThumbnail)
                                    }
                                    else {
                                        cell.buttonSource.image = Utilities().themedImage(img_movieThumbnail)
                                    }
                                }
                                else
                                {
                                    cell.buttonSource!.image = image
                                    self.imageCache[media.pMediaFile] = image
                                }
                            })
                        }).resume()
                    }
                }
            }
            
            return cell

        }
        else
        {
            return UniversalCollectionViewCell()
        }
    }

    // MARK: - UICollectionViewDelegate protocol
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                               sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        
        collectionView.collectionViewLayout.invalidateLayout()
        if displayRetryOption == true && arrayCollection.count == 0 {
            if ((headerCollection) != nil) {
                let width: CGFloat = UIScreen.main.bounds.size.width-widthSubtractConstant
                let height = self.frame.size.height - headerCollection!.frame.size.height
                return CGSize(width: width, height: height)
            }
            else{
                let width: CGFloat = UIScreen.main.bounds.size.width-widthSubtractConstant
                let height = self.frame.size.height
                return CGSize(width: width, height: height)
            }
            
            
        }
        else if displayListView == true || arrayCollection.count == 0
        {
            let width: CGFloat = UIScreen.main.bounds.size.width-widthSubtractConstant
            var returnHeight:CGFloat
            //This condition will work if arrayCollection exists
            if (arrayCollection.count != 0)
            {
                let media = arrayCollection[(indexPath as IndexPath).row]
                //If condition will work for Peep text
                if (!media.pPeepText.isEmpty)
                {
                    let calculatedHeightPeepText:CGFloat = self.heightForLabel(media.pPeepText, font: ProximaNovaRegular(14.0), width: width-peepTextSideMarginsWithMedia)
                    //If attachment is missing
                    if (media.pMediaAttachmentType == 0)
                    {
                        //If height of peep text less than default height of peep text
                        if  (calculatedHeightPeepText < peepTextLabelWithMediaDefaultHeight)
                        {
                            //Here we are not adding media height
                            returnHeight = universalCellDefaultHeight - universalCellWidthSubtract
                        }
                            //If height of peep text greater than default height of peep text
                        else{
                            //Here we are not adding media height
                            returnHeight = (universalCellDefaultHeight - universalCellWidthSubtract)  + (calculatedHeightPeepText - peepTextLabelWithMediaDefaultHeight)
                        }
                        return CGSize(width: width, height: returnHeight)
                    }
                        //If attachment found with peep text
                    else{
                        //If height of peep text less than default height of peep text
                        if  (calculatedHeightPeepText < peepTextLabelWithMediaDefaultHeight)
                        {
                            //Here we are adding media height
                            returnHeight = universalCellDefaultHeight - universalCellWidthSubtract  + width
                        }
                            //If height of peep text greater than default height of peep text
                        else{
                            //Here we are adding media height
                            returnHeight = (universalCellDefaultHeight - universalCellWidthSubtract  + width)  + (calculatedHeightPeepText - peepTextLabelWithMediaDefaultHeight)
                        }
                        return CGSize(width: width, height: returnHeight)
                    }
                }
                    //else condition will work without Peep text
                else{
                    returnHeight = universalCellDefaultHeight - universalCellWidthSubtract  + width
                    return CGSize(width: width, height: returnHeight)
                }
            }
                //This condition will work if arrayCollection is nil
            else{
                returnHeight = universalCellDefaultHeight - universalCellWidthSubtract  + width
                return CGSize(width: width, height: returnHeight)
            }
        }
        else if displayPeepText == true
        {
            let media = arrayCollection[(indexPath as IndexPath).row]
            let width: CGFloat = UIScreen.main.bounds.size.width-widthSubtractConstant
            let calculatedHeight:CGFloat = self.heightForLabel(media.pPeepText, font: ProximaNovaRegular(14.0), width: width-peepTextMargins)
            var returnHeight:CGFloat
            if  (calculatedHeight < peepTextLabelDefaultHeight)
            {
                returnHeight = peepTextCellDefaultHeight
            }
            else{
                returnHeight = peepTextCellDefaultHeight  + (calculatedHeight - peepTextLabelDefaultHeight)
            }
            return CGSize(width: width, height: returnHeight)
        }
        else
        {
            let width: CGFloat = (UIScreen.main.bounds.size.width - widthSubtractConstant)/3
            return CGSize(width: width, height: width)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        if headerCollection != nil
        {
            return headerCollection!
        }
        
        return UICollectionReusableView(frame: CGRect.zero)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        DispatchQueue.main.async{
            
            if (self.indexPathlastPlayed != nil) {
                
                if self.displayPeepText == true {
                    
                    let cellPeepTextDetail = self.cellForItem(at: self.indexPathlastPlayed!) as! UniversalCollectionViewPeepTextExpandedCell
                    cellPeepTextDetail.stopVideo()
                    
                }
                else
                {
                    let cellSel = self.cellForItem(at: self.indexPathlastPlayed!) as! UniversalCollectionViewTableCell
                    cellSel.stopVideo()
                }
            }
        }
        
        if arrayCollection.count > 0 {
            
            let media = arrayCollection[(indexPath as IndexPath).row]
            
            //if media is locked then initiate download and unlock process
            if media.pIsLocked == 1{
                
                var localFolderName = ""
                
                //if media is sent media
                if String(media.pUserId) == Utilities.getUserDetails().id
                {
                     localFolderName = localSentFolderName
                }
                else
                {
                    localFolderName = localReceivedFolderName
                }
                
                //if displaying peep text in collapsed/grid mode
                if displayPeepText == true && classType == .gallery && displayListView == false
                {
                    let obj = arrayCollection[indexPath.row]
                    self.updateDelegate!.moveToNextScreenGallery("PeepTextDetailCollectionViewController", WithData: obj)
                }
                //if displaying peep text in expanded mode on gallery screen or in peeptextdetail screen
                else if displayPeepText == true && (classType == .peepDetail || classType == .gallery)
                {
                    //If media is downloaded locally and unlocked
                    if Utilities().getLocalMediaPath(media.pMediaFile, mediaAttachmentType: 0, folderName:localFolderName).characters.count > 0
                    {
                        let obj = arrayCollection[indexPath.row]
                       self.updateDelegate!.moveToNextScreenGallery("PeepTextDetailCollectionViewController", WithData: obj)
                    }
                    else  //If media is not downloaded locally
                    {
                        //Start media download & unlock process
                        objDownloadLockedMedia.indexPathGlobal = indexPath
                        objDownloadLockedMedia.mediaURL = media.pMediaFile
                        objDownloadLockedMedia.mediaAttachmentURL = media.pMediaAttachment
                        objDownloadLockedMedia.mediaAttachmentType = media.pMediaAttachmentType
                        objDownloadLockedMedia.downloadLockDeleg = self
                        objDownloadLockedMedia.flderName = localFolderName
                        
                        if classType == .peepDetail
                        {
                            objDownloadLockedMedia.className = "UniversalCollectionViewPeepText"
                        }
                        else if classType == .gallery
                        {
                            objDownloadLockedMedia.className = "UniversalCollectionView"
                        }
                        
                        objDownloadLockedMedia.unloackMedia()
                    }
                }
                else
                {
                    //If media is downloaded locally and unlocked
                    if Utilities().getLocalMediaPath(media.pMediaFile, mediaAttachmentType: 0, folderName:localFolderName).characters.count > 0
                    {
                        let obj = arrayCollection[indexPath.row]
                       updateDelegate!.moveToNextScreenGallery("SingleDetailVc", WithData: obj)
                    }
                    else  //If media is not downloaded locally
                    {
                        //Start media download & unlock process
                        objDownloadLockedMedia.indexPathGlobal = indexPath
                        objDownloadLockedMedia.mediaURL = media.pMediaFile
                        objDownloadLockedMedia.mediaAttachmentURL = media.pMediaAttachment
                        objDownloadLockedMedia.mediaAttachmentType = media.pMediaAttachmentType
                        objDownloadLockedMedia.downloadLockDeleg = self
                        objDownloadLockedMedia.flderName = localFolderName
                        objDownloadLockedMedia.className = "UniversalCollectionView"
                        objDownloadLockedMedia.unloackMedia()
                    }
                }
            }
            else
            {
                let obj = arrayCollection[indexPath.row]
                if displayPeepText == true && classType == .gallery
                {
                    self.updateDelegate!.moveToNextScreenGallery("PeepTextDetailCollectionViewController", WithData: obj)
                }
                else if classType == .gallery
                {
                    updateDelegate!.moveToNextScreenGallery("SingleDetailVc", WithData: obj)
                }
            }
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath){
        
        if ((self.indexPathlastPlayed) != nil) {
            
            if displayPeepText == true
            {
                let cell = self.dequeueReusableCell(withReuseIdentifier: "PeepTextExpandedCell", for: indexPath) as! UniversalCollectionViewPeepTextExpandedCell
                if cell.cellTag != -1
                {
                    cell.stopVideo()
                }
            }
            else
            {
                let cell = self.dequeueReusableCell(withReuseIdentifier: "universal_tablecell", for: indexPath) as! UniversalCollectionViewTableCell
                if cell.cellTag != -1
                {
                    cell.stopVideo()
                }
            }
            
        }
    }
    
    //MARK: - Download Locked Media Delegate Methods
    
    func showMessage(_ messge:String){

        let ac = UIAlertController(title: "", message: messge, preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        
        if classType == .gallery {
            let objView = self.updateDelegate as! GalleryMainViewController
            objView.present(ac, animated: true, completion: nil)
            
        }
        else if classType == .peepDetail {
            let objView = self.updateDelegate as! PeepTextDetailCollectionViewController
            objView.present(ac, animated: true, completion: nil)
            
        }
    }
    
    func refreshUI(_ indexPathGlobal:IndexPath){
        
        self.reloadData()
        
//        var arrIndexPath = [NSIndexPath]()
//        arrIndexPath.append(indexPathGlobal)
//        self.reloadItemsAtIndexPaths(arrIndexPath)
        
    }
    
    //MARK: - Response peep code verification
    func responsepeepCodeVerification(_ notify: Foundation.Notification)
    {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFIACTION_PEEPCODEVERIFICATION), object: nil)
        
        let swiftyJsonVar = JSON(notify.object!)
        
        objDownloadLockedMedia.responsepeepCode(swiftyJsonVar)
    }
    
    // MARK: - UICollectionViewDataSource protocol
    
    // tell the collection view how many cells to make
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if arrayCollection.count > 0 {
            return arrayCollection.count
        }
        else {
            if displayRetryOption {
                return 1
            }
            else {
                return 0
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        //Load More
        
        
        if arrayCollection.count > 0 {
            if (arrayCollection.count >= SearchLimit) && (indexPath.row + 1  == arrayCollection.count) {
                
                if let cell = self.dequeueReusableCell(withReuseIdentifier: "LoadMoreCell", for: indexPath) as? UICollectionViewCell
                {
                    if let lbl5176 =  cell.viewWithTag(5176) as? UILabel
                    {
                        lbl5176.text = loading_Title
                        
                        if (totalRecordCount) > (currentPage) * SearchLimit {
                            
                            offset = (currentPage) * SearchLimit
                            currentPage += 1
                            
                            self.updateDelegate?.loadMoreMediaGallery(self.currentPage, offset:self.offset, categoryId:self.searchCategoryId)
                        }
                        else
                        {
                            lbl5176.isHidden = true
                            
                            lbl5176.text = loadingNoMoreData
                            
                            //Display Movies, Music, Photos & VideoGames media in list/expanded view on gallery screen
                            if displayListView == true &&  displayPeepText == false
                            {
                                return refreshCellForListRow(indexPath)
                            }
                                //Display Peep Text in grid view on gallery screen
                            else if displayListView == false && displayPeepText == true
                            {
                                return refreshCellForPeepTextGridRow(indexPath)
                            }
                                //Display Peep Text in list/expanded view on gallery screen
                            else if displayListView == true && displayPeepText == true
                            {
                                return refreshCellForPeepTextExpandedRow(indexPath)
                            }
                                //Display Movies, Music, Photos & VideoGames media in grid view on gallery screen
                            else
                            {
                                return refreshCellForCollectionRow(indexPath)
                            }
                        }
                    }
                    
                    return cell
                }
            }
                
            else{
                
                //Display Movies, Music, Photos & VideoGames media in list/expanded view on gallery screen
                if displayListView == true &&  displayPeepText == false
                {
                    return refreshCellForListRow(indexPath)
                }
                    //Display Peep Text in grid view on gallery screen
                else if displayListView == false && displayPeepText == true
                {
                    return refreshCellForPeepTextGridRow(indexPath)
                }
                    //Display Peep Text in list/expanded view on gallery screen
                else if displayListView == true && displayPeepText == true
                {
                    return refreshCellForPeepTextExpandedRow(indexPath)
                }
                    //Display Movies, Music, Photos & VideoGames media in grid view on gallery screen
                else
                {
                    return refreshCellForCollectionRow(indexPath)
                }
                
            }
        }
        else {
            let cell:GalleryRefreshCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "refreshGalleryCell", for: indexPath) as! GalleryRefreshCollectionViewCell
            
            cell.lblNoRecordsFound.text = noRecordsFoundTitle
            
            if cell.lblNoRecordsFound.text == "" {
                cell.btnRefresh.isHidden = true
                cell.lblNoRecordsFound.isHidden = true
            }
            else {
                cell.btnRefresh.isHidden = false
                cell.lblNoRecordsFound.isHidden = false
            }
            
            cell.btnRefresh.addTarget(self, action: #selector(btnRefreshGalleryClicked), for: .touchUpInside)
            
            if showLoadingIndicator {
                cell.loadingIndicator.startAnimating()
                cell.btnRefresh.isHidden = true
                cell.lblNoRecordsFound.isHidden = true
            }
            else {
                if cell.loadingIndicator.isAnimating {
                    cell.loadingIndicator.stopAnimating()
                }
                cell.btnRefresh.isHidden = false
                cell.lblNoRecordsFound.isHidden = false
            }
            
            return cell
        }
    }
    // MARK:- Dynamic height calculation
    
    func heightForLabel(_ text:String, font:UIFont, width:CGFloat) -> CGFloat
    {
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        label.sizeToFit()
        return label.frame.height
        
    }
    
    // MARK:- END
    
    func imageTapped(_ sender:AnyObject)
    {
        let button = sender as! UIButton
        let obj = arrayCollection[button.tag]
        if displayPeepText == true && classType == .gallery {
        self.updateDelegate!.moveToNextScreenGallery("PeepTextDetailCollectionViewController", WithData: obj)
            
        }
        else if classType == .gallery{
            updateDelegate!.moveToNextScreenGallery("SingleDetailVc", WithData: obj)
        }
    }
    
    // MARK: - Show Alert
    func showCommonAlert(_ msg:String) {
        let alert:UIAlertController = UIAlertController(title: "", message: msg, preferredStyle: .alert)
        let alertAction = UIAlertAction(title: alertBtnTitle_OK, style: .default, handler: nil)
        alert.addAction(alertAction)
        if classType == .gallery {
            let objView = self.updateDelegate as! GalleryMainViewController
            objView.present(alert, animated: true, completion: nil)
            
        }
        else if classType == .peepDetail {
            let objView = self.updateDelegate as! PeepTextDetailCollectionViewController
            objView.present(alert, animated: true, completion: nil)
            
        }
    }
}
