//
//  SearchTableCell.swift
//  C00Peeps
//
//  Created by Mac on 06/10/16.
//  Copyright © 2016 SOTSYS011. All rights reserved.
//

import UIKit

class SearchTableCell: UITableViewCell {
    
    @IBOutlet weak var imageView_user: UIImageView!
    @IBOutlet weak var btnUserImage: UIButton!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelPost: UILabel!
    @IBOutlet weak var buttonFollow: UIButton!
    @IBOutlet weak var activityIndicatorImage: UIActivityIndicatorView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.layoutIfNeeded()
        imageView_user!.layer.cornerRadius = (imageView_user!.frame.size.width)/2
        imageView_user!.clipsToBounds = true
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
