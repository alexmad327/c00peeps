//
//  UniversalCollectionViewCell.swift
//  C00Peeps
//
//  Created by Mac on 06/10/16.
//  Copyright © 2016 SOTSYS011. All rights reserved.
//

import UIKit

class UniversalCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var buttonSource: UIImageView!
    @IBOutlet var imageType: UIImageView!
    //@IBOutlet var activityIndicator_image: UIActivityIndicatorView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
}
