//
//  UniversalCollectionViewTableCell.swift
//  C00Peeps
//
//  Created by Mac on 22/10/16.
//  Copyright © 2016 SOTSYS011. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit

class UniversalCollectionViewTableCell: UICollectionViewCell {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var btnUserImage: UIButton!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblExpiryTime: UILabel!
    @IBOutlet weak var imgMedia: UIImageView!
    @IBOutlet weak var btnLike: UIButton!
    @IBOutlet weak var btnDislike: UIButton!
    @IBOutlet weak var btnComments: UIButton!
    @IBOutlet weak var lblPostedDate: UILabel!
    @IBOutlet weak var btnReportMedia: UIButton!
    @IBOutlet weak var btnShare: UIButton!
    @IBOutlet weak var btnPlay: UIButton!
    @IBOutlet weak var lblMediaCaption: UILabel!
    @IBOutlet weak var activity: UIActivityIndicatorView!
    @IBOutlet weak var btnLikesCount: UIButton!
    @IBOutlet weak var btnDislikeCount: UIButton!
    @IBOutlet weak var btnCommentCount: UIButton!

    var videoPlayer:AVPlayer!
    var videoItem:AVPlayerItem!
    var playerLayer:AVPlayerLayer!
    
    var delegate:PlayMediaDelegate!
    var cellTag:Int = -1
    
    fileprivate let ObservatingKeyPath = "currentItem.status"
    
    fileprivate let PlayerStatusObservingContext = UnsafeMutablePointer<Int>(bitPattern: 1)
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    
    //MARK:- This method will play video & audio both
    
    func loadVideo(_ videoURL:URL)
    {
        videoItem = AVPlayerItem.init(url: videoURL)
        videoPlayer = AVPlayer.init(playerItem: videoItem)
        
        playerLayer = AVPlayerLayer(player: videoPlayer)
        
        DispatchQueue.main.async {
            
            self.playerLayer.frame = CGRect(x: self.contentView.frame.origin.x,y: self.contentView.frame.origin.y+80, width: self.contentView.frame.size.width, height: self.contentView.frame.size.width)
            
            self.playerLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
            self.playerLayer.backgroundColor = UIColor.clear.cgColor
            self.contentView.layer.addSublayer(self.playerLayer)
            self.contentView.bringSubview(toFront: self.btnPlay)
            self.contentView.bringSubview(toFront: self.activity)
            
        }
        videoPlayer.volume = 0.75
        videoPlayer.play()
        
        activity.startAnimating()
        activity.isHidden = false
        
        videoPlayer.addObserver(self, forKeyPath: "status", options: NSKeyValueObservingOptions(), context: nil)
        
        NotificationCenter.default.addObserver(
            forName: NSNotification.Name.AVPlayerItemFailedToPlayToEndTime,
            object: nil,
            queue: nil,
            using: { notification in
                
                if self.videoPlayer != nil {
                    
                    self.removeObserversForPlayer()
                    self.resetPlayerVariables()
                    self.btnPlay.setImage(Utilities().themedImage(img_videoPlay), for: UIControlState())
                    self.delegate.playFinished(self.cellTag)
                    self.cellTag = -1
                }
        })
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil, queue: nil) { notification in
            
            if self.videoPlayer != nil {
                
                self.removeObserversForPlayer()
                self.resetPlayerVariables()
                self.btnPlay.setImage(Utilities().themedImage(img_videoPlay), for: UIControlState())
                self.delegate.playFinished(self.cellTag)
                self.cellTag = -1
            }
            
            
        }
    }
    
    func stopVideo()
    {
        if videoPlayer != nil {
            
            videoPlayer.pause()
            
            removeObserversForPlayer()
            resetPlayerVariables()
            
            btnPlay.setImage(Utilities().themedImage(img_videoPlay), for: UIControlState())
            
            
            self.delegate.playFinished(self.cellTag)
            cellTag = -1
        }
    }
    
    // catch changes to status
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if (keyPath == "rate") {
            
        }
        if (keyPath == "status") {
            
            activity.stopAnimating()
            activity.isHidden = true
            
            delegate.playStart(cellTag)
            btnPlay.setImage(Utilities().themedImage(img_audioPlay), for: UIControlState())
        }
    }
    
    func removeObserversForPlayer(){
        
        if ((videoPlayer) != nil) {
            
            videoPlayer.removeObserver(self, forKeyPath: "status")
        }
    }
    
    func resetPlayerVariables()
    {
        if videoPlayer != nil {
            videoPlayer = nil
        }
        
        if videoItem != nil {
            videoItem = nil
        }
        
        if playerLayer != nil{
            
            playerLayer.removeFromSuperlayer()
        }
    }
}
