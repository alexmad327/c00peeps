//
//  GenrateCodeVc.swift
//  C00Peeps
//
//  Created by SOTSYS011 on 3/10/16.
//  Copyright © 2016 SOTSYS011. All rights reserved.
//

import UIKit
import SwiftyJSON
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}

class GenrateCodeVc: BaseVC
{
    @IBOutlet var NumberScrollView: JTNumberScrollAnimatedView!
    @IBOutlet weak var txtCode: UITextField!
    @IBOutlet weak var imgDivider: UIImageView!
    @IBOutlet weak var staticLabel: UILabel!
    @IBOutlet weak var btnUserGenerated: UIButton!
    @IBOutlet weak var btnSystemGenerated: UIButton!
    @IBOutlet weak var btnDone: UIButton!
    @IBOutlet weak var imgVwHeader: UIImageView!
    
    var isComingFromSettings:Bool = false

    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        if self.isComingFromSettings {
            self.title = "Change PECx Code"
        }
        else {
            self.title = "Register PECx Code"
            self.navigationItem.setHidesBackButton(true, animated: false)
        }

        NotificationCenter.default.addObserver(self, selector: #selector(GenrateCodeVc.limitTextField), name: NSNotification.Name(rawValue: "UITextFieldTextDidChangeNotification"), object: txtCode)
        
        let attributedString = NSMutableAttributedString (string:staticLabel.text! as String)
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 5 // Whatever line spacing you want in points
        attributedString.addAttribute(kCTParagraphStyleAttributeName as NSAttributedStringKey, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))
        staticLabel.attributedText = attributedString;
        txtCode.isHidden = true
        NumberScrollView.isHidden = true
        
        self.NumberScrollView.textColor = UIColor.darkGray
        self.NumberScrollView.font = ProximaNovaSemibold(16)
        self.NumberScrollView.minLength = 9
        
        // Update View According To Selected Theme
        self.updateViewAccordingToTheme()
        
        self.btnUsergenerate_Clicked(btnUserGenerated)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Hide tab bar for this controller.
        self.tabBarController?.tabBar.isHidden = true
    }
    
    // MARK: - Update View According To Selected Theme
    func updateViewAccordingToTheme() {
        imgVwHeader.image = Utilities().themedImage(img_headerTop)
        btnDone.setBackgroundImage(Utilities().themedImage(img_btnSelected), for: UIControlState())
        btnDone.setBackgroundImage(Utilities().themedImage(img_btnSelected), for: .highlighted)

        btnUserGenerated.setImage(Utilities().themedImage(img_userGenerated), for: UIControlState())
        btnUserGenerated.setImage(Utilities().themedImage(img_userGeneratedSelected), for: .highlighted)
        btnUserGenerated.setImage(Utilities().themedImage(img_userGeneratedSelected), for: .selected)

        btnSystemGenerated.setImage(Utilities().themedImage(img_systemGenerated), for: UIControlState())
        btnSystemGenerated.setImage(Utilities().themedImage(img_systemGeneratedSelected), for: .highlighted)
        btnSystemGenerated.setImage(Utilities().themedImage(img_systemGeneratedSelected), for: .selected)
    }
    
    //MARK: - IBAction
    
    @IBAction func btnUsergenerate_Clicked(_ sender: UIButton)
    {
        sender.isSelected = true
        btnSystemGenerated.isSelected = false
        txtCode.text = ""
        txtCode.isHidden = false
        NumberScrollView.isHidden = true
        txtCode.becomeFirstResponder()
    }
    
    @IBAction func btnSystemGenrated_Clicked(_ sender: UIButton)
    {
        sender.isSelected = true
        btnUserGenerated.isSelected = false
        txtCode.isHidden = true
        NumberScrollView.isHidden = false
        self.NumberScrollView.value = Int((arc4random() % 500000000)) as NSNumber//9 digits
        self.NumberScrollView.startAnimation()
        txtCode.resignFirstResponder()

        let bounds = self.btnSystemGenerated.bounds
        UIView.animate(withDuration: 1.0, delay: 0.0, usingSpringWithDamping: 0.2, initialSpringVelocity: 10, options: [], animations: {
            self.btnSystemGenerated.bounds = CGRect(x: bounds.origin.x - 20, y: bounds.origin.y, width: bounds.size.width , height: bounds.size.height)
            }, completion: nil)
        
    }
    
    
    @IBAction  func btnDone_Clciked(_ sender: AnyObject)
    {
        if txtCode.text!.isEmpty && self.txtCode.isHidden == false {
            self.showCommonAlert(alertMsg_EmptyCode)
            return
        }
        if self.NumberScrollView.isHidden == true && self.txtCode.isHidden == true {
            self.showCommonAlert(alertMsg_EmptyCode)
            return
        }
        else if !txtCode.text!.isEmpty && self.txtCode.isHidden == false && (txtCode.text?.characters.count < 5 || txtCode.text?.characters.count > 9)
        {
            self.showCommonAlert(alertMsg_CodeRange)
            return
        }
        
        ApplicationDelegate.showLoader(loaderTitle_Submitting)
        
        var parameters = [String: AnyObject]()
        if txtCode.text!.isEmpty {
            parameters["peepcode"] = self.NumberScrollView.value.stringValue as AnyObject
            
        }
        else{
            parameters["peepcode"] = txtCode.text as AnyObject
            
        }
        parameters["user_id"] = Utilities.getUserDetails().id as AnyObject
        APIManager.sharedInstance.requestSetPeepCode(parameters, Target: self)
    }
    //MARK: - PeepCode Response
    func responsePeepCode (_ notify: Foundation.Notification)
    {
    
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFIACTION_SETPEEPCODE), object: nil)
        
        ApplicationDelegate.hideLoader ()
        
        let swiftyJsonVar = JSON(notify.object!)
        let message = swiftyJsonVar["message"].stringValue
        let status = swiftyJsonVar["status"].intValue
        
        if swiftyJsonVar [ERROR_KEY].exists()
        {
            self.showCommonAlert(swiftyJsonVar[ERROR_KEY].stringValue)
        }
        else if (status == 1)
        {
            // Update Peep Code.
            let userDetail:UserDetail = Utilities.getUserDetails()
            if txtCode.text!.isEmpty {
               userDetail.peepcode = self.NumberScrollView.value.stringValue
            }
            else{
                userDetail.peepcode = self.txtCode.text!
            }
            Utilities.setUserDetails(userDetail)
            printCustom("updated user details peepcode: \(Utilities.getUserDetails().peepcode)")
            printCustom("updated user details mediaTypeId: \(Utilities.getUserDetails().filmMediaTypeId)")

            if self.isComingFromSettings {
                self.navigationController?.popViewController(animated: true)
            }
            else {
                if Utilities.getSignUpState() != SignUpState.home {
                    Utilities.setSignUpState(SignUpState.setPrivacyLevel)
                }

                let AuthenticationObject = self.storyboard?.instantiateViewController(withIdentifier: "AccountAuthenticationVc") as? AccountAuthenticationVc
                self.navigationController?.pushViewController(AuthenticationObject!, animated: true)
            }
            
        }
        else if (status == 0) //error response
        {
            let responseErrorMessage = swiftyJsonVar["response"].stringValue
            self.showCommonAlert(responseErrorMessage)
        }
        else{
            self.showCommonAlert(message)
        }
    }
    //MARK: - UITextField Delegates
    
    func textField(_ textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool
    {
        if string.isEmpty{
            return true
        }
        
        if  textField.text?.characters.count > 8 {
            txtCode.resignFirstResponder()
            let bounds = self.btnUserGenerated.bounds
            UIView.animate(withDuration: 1.0, delay: 0.0, usingSpringWithDamping: 0.2, initialSpringVelocity: 10, options: [], animations: {
                self.btnUserGenerated.bounds = CGRect(x: bounds.origin.x - 20, y: bounds.origin.y, width: bounds.size.width  , height: bounds.size.height)
                }, completion: nil)
            return false
        }
        else {
            return true
        }
    }
    // Do any additional setup after loading the view.
    
    func limitTextField(_ Notif:Foundation.Notification) {
        
        let attributedString = NSMutableAttributedString(string: txtCode.text!)
        attributedString.addAttribute(kCTKernAttributeName as NSAttributedStringKey, value: 15, range: NSMakeRange(0, (txtCode.text?.characters.count)!))
        txtCode.attributedText = attributedString
        
    }
}

/* Commented by Anubha due to extra code written by previous developer
 func stopEditing()
 {
 self.view .endEditing(true)
 }
 func dismissAlert() {
 
 let bounds = view.bounds
 let smallFrame = CGRectInset(view.frame, view.frame.size.width / 4, view.frame.size.height / 4)
 let finalFrame = CGRectOffset(smallFrame, 0, bounds.size.height)
 
 UIView.animateKeyframesWithDuration(4, delay: 0, options: .CalculationModeCubic, animations: {
 
 UIView.addKeyframeWithRelativeStartTime(0.0, relativeDuration: 0.5) {
 self.view.frame = smallFrame
 }
 
 UIView.addKeyframeWithRelativeStartTime(0.5, relativeDuration: 0.5) {
 self.view.frame = finalFrame
 }
 let TouchVcObject = self.storyboard?.instantiateViewControllerWithIdentifier("AccountAuthenticationVc") as? AccountAuthenticationVc
 self.navigationController?.pushViewController(TouchVcObject!, animated: true)
 
 }, completion: nil)
 
 }
 
 func randomStringWithLength (len : Int) -> NSString {
 
 let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
 
 let randomString : NSMutableString = NSMutableString(capacity: len)
 
 for (var i=0; i < len; i++){
 let length = UInt32 (letters.length)
 let rand = arc4random_uniform(length)
 randomString.appendFormat("%C", letters.characterAtIndex(Int(rand)))
 }
 
 return randomString
 }
 func stopEditing()
 {
 self.view .endEditing(true)
 }
 func randomStringWithLength (len : Int) -> NSString {
 
 let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
 
 let randomString : NSMutableString = NSMutableString(capacity: len)
 
 for (var i=0; i < len; i++){
 let length = UInt32 (letters.length)
 let rand = arc4random_uniform(length)
 randomString.appendFormat("%C", letters.characterAtIndex(Int(rand)))
 }
 
 return randomString
 }
 */


