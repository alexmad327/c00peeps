
//
//  AppDelegate.swift
//  C00Peeps
//
//  Created by SOTSYS011 on 2/25/16.
//  Copyright © 2016 SOTSYS011. All rights reserved.
//

import UIKit
import Alamofire
import FBSDKCoreKit
import Fabric
import TwitterKit
import AWSS3
import AWSCore
import IQKeyboardManagerSwift
import Braintree
import SwiftyJSON
import Crashlytics
 
let loaderSpinnerMarginSide : CGFloat = 35.0
let loaderSpinnerMarginTop : CGFloat = 20.0
let loaderTitleMargin : CGFloat = 5.0

 //var videoFilterView : VideoFilterController?
 
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    //MARK: - App Launched
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        // Amazon Cognito Setup
        setTheCognitoPoolId()
        
        // Flurry Setup
        flurryIntegration()
        
        // Status bar white
        application.isStatusBarHidden = false
        application.statusBarStyle = .lightContent
        
        // Navigation bar customizations
        // Set Navigation Bar and Button Title Color and Font.
        UINavigationBar.appearance().tintColor = UIColor.white
        UINavigationBar.appearance().titleTextAttributes = [kCTForegroundColorAttributeName:UIColor.white] as [NSAttributedStringKey : Any]
       // UIBarButtonItem.appearance().setTitleTextAttributes([ NSForegroundColorAttributeName:UIColor.white], for: UIControlState())
        UINavigationBar.appearance().titleTextAttributes = [kCTFontAttributeName:ProximaNovaRegular(22), NSAttributedStringKey.foregroundColor: UIColor.white] as! [NSAttributedStringKey : Any]
        UINavigationBar.appearance().setBackgroundImage(Utilities().themedImage(img_header), for: .default)
        UINavigationBar.appearance().isTranslucent = false
        
        //Register push notification
        let settings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
        UIApplication.shared.registerUserNotificationSettings(settings)
        UIApplication.shared.registerForRemoteNotifications()
        
        
        // If your app wasn’t running and the user launches it by tapping the push notification, the push notification is passed to your app in the launchOptions
        if let notification = launchOptions?[UIApplicationLaunchOptionsKey.remoteNotification] as? [String: AnyObject] {
            let aps = notification["aps"] as! [String: AnyObject]
            
        }
        
        let userId: String? = Utilities.getUserDetails().id
        if userId == nil || userId == "" {
        
            Utilities().setSaveOriginalPhoto(true)
        }
        
        // Handle next/previous/done on textfield automatically
//        IQKeyboardManager.sharedManager().enable = true
        IQKeyboardManager.shared.enable = true
        
        // Braintree URL Scheme
        BTAppSwitch.setReturnURLScheme(appBundleId + ".payments")
        
        // Call API to receive braintree token from server.
        self.createBraintreeToken()
        
        printCustom("Utilities.getIsFailedToUpgradePackage():\(Utilities.getIsFailedToUpgradePackage())")

        // Call API to update package if was not updated earlier.
        if Utilities.getIsFailedToUpgradePackage() == "1" {
            self.updatePackageAPI(Int(Utilities.getUserDetails().package_id)!)
        }
        
        // Twitter Initialization
//        Fabric.with([Twitter.self])
//        Twitter.sharedInstance().start(withConsumerKey: twitterConsumerKey, consumerSecret: twitterConsumerSecret)
       
        Fabric.with([Crashlytics.self])
        TWTRTwitter.sharedInstance().start(withConsumerKey: twitterConsumerKey, consumerSecret: twitterConsumerSecret)
        
        // Facebook Initialization
        FBSDKProfile.enableUpdates(onAccessTokenChange: true)
        return FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
    }
    
    //MARK:- Push Notification Methods
    
    func application(_ application: UIApplication,didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        //send this device token to server
        
        let tokenChars = (deviceToken as NSData).bytes.bindMemory(to: CChar.self, capacity: deviceToken.count)
        var tokenString = ""
        
        for i in 0..<deviceToken.count {
            tokenString += String(format: "%02.2hhx", arguments: [tokenChars[i]])
        }
        
        Utilities.setDeviceToken(tokenString)
        print("Device Token:", tokenString)
    }
    
    //Called if unable to register for APNS.
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {

        print("Failed to register:", error)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        
        print("Recived: \(userInfo)")
        
        //let aps = userInfo["aps"] as! [String: AnyObject]
        
        //var temp : NSDictionary = userInfo
        if let info = userInfo["aps"] as? Dictionary<String, AnyObject>
        {
            //UIApplication.sharedApplication().applicationIconBadgeNumber = 1
            //var badge = info["badge"] as! String
            var alertMsg = info["alert"] as! String
            var alert: UIAlertView!
            alert = UIAlertView(title: "", message: alertMsg, delegate: nil, cancelButtonTitle: "OK")
            alert.show()
        }
    }
    
    //MARK: - Handle Open URL
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any]) -> Bool {
        if url.scheme?.localizedCaseInsensitiveCompare(appBundleId + ".payments") == .orderedSame {
            return BTAppSwitch.handleOpen(url, options: options)
        }
        
        // Twitter
        if TWTRTwitter.sharedInstance().application(app, open:url, options: options) {
            return true
        }
        
        // Facebook
        let sourceApplication: String? = options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String
        return FBSDKApplicationDelegate.sharedInstance().application(app, open: url, sourceApplication: sourceApplication, annotation: nil)
    }

    //MARK: - Handle Events
    func application(_ application: UIApplication, handleEventsForBackgroundURLSession identifier: String, completionHandler: @escaping () -> Void) {
        /*
         Store the completion handler.
         */
        AWSS3TransferUtility.interceptApplication(application, handleEventsForBackgroundURLSession: identifier, completionHandler: completionHandler)
    }
    
    //MARK: - Direct Upload Methods - for AWS - AWSS3TransferManager
    func setTheCognitoPoolId() {
        
        // Initialize the Amazon Cognito credentials provider
        
        let strPoolId = CognitoPoolId
        let credentialsProvider = AWSCognitoCredentialsProvider(regionType: AWSRegionType.USEast1, identityPoolId: strPoolId)
        let configuration = AWSServiceConfiguration(region: AWSRegionType.USEast1, credentialsProvider: credentialsProvider)
        AWSServiceManager.default().defaultServiceConfiguration = configuration
    }
  
    //MARK: - Flurry Integration
    func flurryIntegration() {
        Flurry.startSession(FLURRY_APIKEY_STAGING)
        FlurrySessionBuilder().withCrashReporting(true)
        
        var dic = [String:String]()
        dic["UserId"] = "49"
        dic["Username"] = "ashishkhurana"
        dic["MediaId"] = "11111"
        
        /*
         
         FlurryEventFailed = 0,
         FlurryEventRecorded,
         FlurryEventUniqueCountExceeded,
         FlurryEventParamsCountExceeded,
         FlurryEventLogCountExceeded,
         FlurryEventLoggingDelayed,
         FlurryEventAnalyticsDisabled
         
         */
        
        let status = Flurry.logEvent("test", withParameters:dic)
        
        if status == FlurryEventRecorded {
        }
        
        //let er:NSError!
        //Flurry.logError("", message:"", error:er)
    }
   
    
    //MARK: - Show/Hide Loader
    func showLoader(_ title: String) {
        if !window!.isUserInteractionEnabled {
            self.hideLoader()
        }
        
        window?.isUserInteractionEnabled = false
        let vwLoadingIndicator: UIView = UIView()
        vwLoadingIndicator.frame = CGRect(x: self.window!.frame.size.width/2 - 150/2, y: self.window!.frame.size.height/2 - 100/2, width: 150, height: 100)
        vwLoadingIndicator.center = self.window!.center
        vwLoadingIndicator.backgroundColor = Utilities().themedMultipleColor()[1]
        vwLoadingIndicator.layer.cornerRadius = 10
        vwLoadingIndicator.layer.shadowColor = UIColor.black.cgColor
        vwLoadingIndicator.layer.shadowOpacity = 0.2
        vwLoadingIndicator.layer.shadowOffset = CGSize(width: 5, height: 5)
        vwLoadingIndicator.accessibilityIdentifier = "Loader_View"
        self.window!.addSubview(vwLoadingIndicator)
        
        let loadingIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
        loadingIndicator.frame = CGRect(x: vwLoadingIndicator.frame.size.width/2 - 40/2, y: 15.0, width: 40.0, height: 40.0)
        loadingIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.whiteLarge
        loadingIndicator.accessibilityIdentifier = "Loader_Indicator"
        vwLoadingIndicator.addSubview(loadingIndicator)
        loadingIndicator.startAnimating()
        
        var lblLoadingIndicator: UILabel! = UILabel()
        let titleSize = CGRect(x: 10, y: loadingIndicator.frame.origin.y + loadingIndicator.frame.size.height, width: vwLoadingIndicator.frame.size.width - 20, height: 30)
        lblLoadingIndicator = UILabel()
        lblLoadingIndicator.frame = titleSize
        lblLoadingIndicator.text = title
        lblLoadingIndicator.font = UIFont.systemFont(ofSize: 17)
        lblLoadingIndicator.textColor = UIColor.white
        lblLoadingIndicator.textAlignment = NSTextAlignment.center
        lblLoadingIndicator.numberOfLines = 1
        lblLoadingIndicator.adjustsFontSizeToFitWidth = true
        vwLoadingIndicator.addSubview(lblLoadingIndicator)
    }
    
    func hideLoader() {
        window?.isUserInteractionEnabled = true
        
        for vw in window!.subviews {
            if vw.accessibilityIdentifier == "Loader_View" {
                for actInd in vw.subviews {
                    if actInd .isKind(of: UIActivityIndicatorView.self) {
                        if actInd.accessibilityIdentifier == "Loader_Indicator" {
                            actInd.removeFromSuperview()
                        }
                    }
                }
                vw.removeFromSuperview()
            }
        }
    }
    
    //MARK:- UIApplication other delegates
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
        
        
//        if videoFilterView!.isKindOfClass(VideoFilterController) && videoFilterView != nil
//        {
//            //pause the video if its playing
//            videoFilterView!.stopAndGoBackAfterAppResigns()
//        }
        
        
        /*
        let st = UIStoryboard(name: "Main", bundle:nil)
        let controller = st.instantiateViewControllerWithIdentifier("tabBarcontroller") as! UITabBarController
        
//        let nav = UINavigationController.init(rootViewController: homeScreen)
//        nav.navigationBarHidden = true
//        ApplicationDelegate.window?.rootViewController = nav
        
        //let controller = UIApplication.sharedApplication().keyWindow?.rootViewController as! TabBarController
        
        if controller.isKindOfClass(TabBarController) {
            
            let presentViewC:UIViewController? = visibleViewControllerInTabBar(controller)
            
            
        }*/
       
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
        
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
      
    }
    
    func visibleViewControllerInTabBar(_ rootViewController:UITabBarController)->UIViewController
    {
    
        if rootViewController.isKind(of: UITabBarController.self)
        {
            let tabCntrl = rootViewController
            
            if tabCntrl.viewControllers![3].isKind(of: UINavigationController.self)
            {
                let selectedViewController = tabCntrl.viewControllers![3] as! UINavigationController
                
                print(selectedViewController)
                
                let selCon = selectedViewController.visibleViewController
                
                print(selCon)
                
                return selCon!
            }
        }
        
        return rootViewController
    }
    
    //MARK: - Call API
    func createBraintreeToken() {
        APIManager.sharedInstance.requestCreateBraintreeToken(nil, Target: self)
    }

    func updatePackageAPI(_ packageType:Int) {
        var parameters = [String: AnyObject]()
        parameters["user_id"] = Utilities.getUserDetails().id as AnyObject
        parameters["package_id"] = packageType as AnyObject
        APIManager.sharedInstance.requestUpgradePackage(parameters, Target: self)
    }
    
     //MARK: - API Response
    func responseCreateBraintreeToken (_ notify: Foundation.Notification) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFICATION_CREATEBRAINTREETOKEN), object: nil)
        
        let swiftyJsonVar = JSON(notify.object!)
        let status = swiftyJsonVar["status"].intValue
        
        printCustom("swiftyJsonVar:\(swiftyJsonVar)")
        if (status == 1) {
            let response = swiftyJsonVar["response"]
            Utilities.setBraintreeToken(response.stringValue)
            
            printCustom("BraintreeToken\(response.stringValue)")
        }

        NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: NOTIFICATION_BRAINTREETOKENRECEIVED), object: nil)
    }
    
    func responseUpdatePackage (_ notify: Foundation.Notification) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFICATION_UPDATEPACKAGE), object: nil)
        
        ApplicationDelegate.hideLoader ()
        
        let swiftyJsonVar = JSON(notify.object!)
        let status = swiftyJsonVar["status"].intValue
        printCustom("swiftyJsonVar:\(swiftyJsonVar)")

        if swiftyJsonVar [ERROR_KEY].exists() {
            Utilities.setIsFailedToUpgradePackage("1")
        }
        else if (status == 1) {
            Utilities.setIsFailedToUpgradePackage("0")
        }
        else {
            Utilities.setIsFailedToUpgradePackage("1")
        }
    }

}

