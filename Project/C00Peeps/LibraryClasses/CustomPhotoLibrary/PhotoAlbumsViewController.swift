//
//  PhotoAlbumsViewController.swift
//  FusumaExample
//
//  Created by Arshdeep on 13/10/16.
//  Copyright © 2016 ytakzk. All rights reserved.
//

import UIKit
import Photos

class PhotoAlbumsViewController: UIViewController {

    @IBOutlet weak var tblVwAlbums: UITableView!
    
    
    var allAlbums: [AnyObject] = []
    var imageManager: PHCachingImageManager?
    var delegate:FusumaViewController? = nil
    var albumTitle: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.

        tblVwAlbums.register(UINib(nibName: "PhotoAlbumTableViewCell", bundle: nil), forCellReuseIdentifier: "photoAlbumId")
  
        imageManager = PHCachingImageManager()
 
        // Save Smart Albums which has media inside it.
        let smartAlbums: PHFetchResult = PHAssetCollection.fetchAssetCollections(with: .smartAlbum, subtype: .any, options: nil)
        saveAlbum(smartAlbums as! PHFetchResult<AnyObject>)

        // Save Albums which has media inside it.
        let albums: PHFetchResult = PHAssetCollection.fetchAssetCollections(with: .album, subtype: .any, options: nil)
        saveAlbum(albums as! PHFetchResult<AnyObject>)
    }
    
    //MARK: - Save Album
    func saveAlbum(_ albums: PHFetchResult<AnyObject>) {// Save album collection which has assets.
        if albums.count > 0 {
            for albumIndex in 0...albums.count - 1 {
                let collection:PHAssetCollection = albums.object(at: albumIndex) as! PHAssetCollection
                let assets: PHFetchResult = PHAsset.fetchAssets(in: collection, options: nil)
                if assets.count > 0 {
                    if collection.localizedTitle == "Camera Roll" {//Keeping Camera Roll as first album.
                        allAlbums.insert(collection, at: 0)
                    }
                    else {
                        allAlbums.append(collection)
                    }
                }
            }
        }
    }

    //MARK: - Button Actions
    @IBAction func titleButtonPressed(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
    }
      
    //MARK: - Table View Datasource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Return the number of rows
        return allAlbums.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        let cell : PhotoAlbumTableViewCell = tblVwAlbums.dequeueReusableCell(withIdentifier: "photoAlbumId",for: indexPath) as! PhotoAlbumTableViewCell
        
        // Album
        let collection = allAlbums[indexPath.row] as! PHAssetCollection
        
        // Album name
        cell.lblAlbumName.text = collection.localizedTitle
        
        // Album Assets
        let assets: PHFetchResult = PHAsset.fetchAssets(in: collection, options: nil)
        
        // Album Image Count
        cell.lblAlbumImagesCount.text = String(assets.count)
        
        // Album Last Image
        let asset: PHAsset = assets.lastObject as! PHAsset
        let options = PHImageRequestOptions()
        options.isNetworkAccessAllowed = true
        self.imageManager?.requestImage(for: asset,
                                                targetSize: CGSize(width: asset.pixelWidth, height: asset.pixelHeight),
                                                contentMode: .aspectFill,
                                                options: options) {
                                                    result, info in
                                                    cell.imgVwAlbum.image = result
        }
        
        if collection.localizedTitle == "Videos" {
            cell.imgVwVideoIcon.isHidden = false
        }
        else {
            cell.imgVwVideoIcon.isHidden = true
        }
        
        return cell
    }
   
    func tableView(_ tableView: UITableView, didSelectRowAtIndexPath indexPath: IndexPath) {
        let collection = allAlbums[indexPath.row] as! PHAssetCollection
        let sortOptions = PHFetchOptions()
        sortOptions.sortDescriptors = [
            NSSortDescriptor(key: "modificationDate", ascending: false)
        ]
        let assets: PHFetchResult = PHAsset.fetchAssets(in: collection, options: sortOptions)

        delegate!.albumChanged(assets as! PHFetchResult<AnyObject>, albumTitle: collection.localizedTitle!)
        
        self.dismiss(animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
