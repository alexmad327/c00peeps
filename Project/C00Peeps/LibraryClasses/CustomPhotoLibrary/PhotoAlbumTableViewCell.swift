//
//  PhotoAlbumTableViewCell.swift
//  FusumaExample
//
//  Created by Arshdeep on 13/10/16.
//  Copyright © 2016 ytakzk. All rights reserved.
//

import UIKit

class PhotoAlbumTableViewCell: UITableViewCell {

    @IBOutlet weak var imgVwAlbum: UIImageView!
    @IBOutlet weak var lblAlbumName: UILabel!
    @IBOutlet weak var lblAlbumImagesCount: UILabel!
    @IBOutlet weak var imgVwVideoIcon: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
