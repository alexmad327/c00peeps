//
//  FSAlbumView.swift
//  Fusuma
//
//  Created by Yuta Akizuki on 2015/11/14.
//  Copyright © 2015年 ytakzk. All rights reserved.
//

import UIKit
import Photos

@objc public protocol FSAlbumViewDelegate: class {
    
    func albumViewCameraRollUnauthorized()
}

final class FSAlbumView: UIView, UICollectionViewDataSource, UICollectionViewDelegate, PHPhotoLibraryChangeObserver, UIGestureRecognizerDelegate {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var imageCropView: FSImageCropView!
    @IBOutlet weak var imageCropViewContainer: UIView!
    
    @IBOutlet weak var collectionViewConstraintHeight: NSLayoutConstraint!
    @IBOutlet weak var imageCropViewConstraintTop: NSLayoutConstraint!
    
    weak var delegate: FSAlbumViewDelegate? = nil
    
    var images: PHFetchResult<PHAsset>!
    var imageManager: PHCachingImageManager?
    var previousPreheatRect: CGRect = CGRect.zero
    let cellSize = CGSize(width: 100, height: 100)
    var phAsset: PHAsset!
    var selectedAsset: PHAsset!
    
    // Variables for calculating the position
    enum Direction {
        case scroll
        case stop
        case up
        case down
    }
    let imageCropViewOriginalConstraintTop: CGFloat = 0
    let imageCropViewMinimalVisibleHeight: CGFloat  = 40//100
    var dragDirection = Direction.up
    var imaginaryCollectionViewOffsetStartPosY: CGFloat = 0.0
    
    var cropBottomY: CGFloat  = 0.0
    var dragStartPos: CGPoint = CGPoint.zero
    let dragDiff: CGFloat     = 20.0
    
    //Play Video
    
    var audioPlayer:AVAudioPlayer!
    var videoPlayer:AVPlayer!
    var videoItem:AVPlayerItem!
    var playerLayer:AVPlayerLayer!
    var btnPlayVideo: UIButton!
    var shouldPlayVideo: Bool = false
        
    static func instance() -> FSAlbumView {
        
        return UINib(nibName: "FSAlbumView", bundle: Bundle(for: self.classForCoder())).instantiate(withOwner: self, options: nil)[0] as! FSAlbumView
    }
    
    //MARK: - Initialize
    func initialize() {
        if images != nil {
            
            return
        }
		
		self.isHidden = false
        
        let panGesture      = UIPanGestureRecognizer(target: self, action: #selector(FSAlbumView.panned(_:)))
        panGesture.delegate = self
        self.addGestureRecognizer(panGesture)
        
        collectionViewConstraintHeight.constant = self.frame.height - imageCropView.frame.height - imageCropViewOriginalConstraintTop
        imageCropViewConstraintTop.constant = 0//50
        dragDirection = Direction.up
        
        imageCropViewContainer.layer.shadowColor   = UIColor.black.cgColor
        imageCropViewContainer.layer.shadowRadius  = 30.0
        imageCropViewContainer.layer.shadowOpacity = 0.9
        imageCropViewContainer.layer.shadowOffset  = CGSize.zero
        
        collectionView.register(UINib(nibName: "FSAlbumViewCell", bundle: Bundle(for: self.classForCoder)), forCellWithReuseIdentifier: "FSAlbumViewCell")
		collectionView.backgroundColor = UIColor.lightGray
     
        // Never load photos Unless the user allows to access to photo album
        checkPhotoAuth()
        
        // Sorting condition
        let options = PHFetchOptions()
        options.sortDescriptors = [
            NSSortDescriptor(key: "modificationDate", ascending: false)
        ]        
        images = PHAsset.fetchAssets(with: options)
       
        
        if images.count > 0 {
            selectedAsset = images.object(at: 0) as! PHAsset

            shouldPlayVideo = false
            changeAsset(images[0] as! PHAsset)
            collectionView.reloadData()
            collectionView.selectItem(at: IndexPath(row: 0, section: 0), animated: false, scrollPosition: UICollectionViewScrollPosition())
        }
        
        PHPhotoLibrary.shared().register(self)
        
    }
   
    deinit {
        printCustom("deinit")
        if PHPhotoLibrary.authorizationStatus() == PHAuthorizationStatus.authorized {
            
            PHPhotoLibrary.shared().unregisterChangeObserver(self)
        }
        self.resetPlayerVariables()
    }
    
    //MARK: - Gesture Recognizer

    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        
        return true
    }
    
    func panned(_ sender: UITapGestureRecognizer) {
        
        if sender.state == UIGestureRecognizerState.began {
            
            let view    = sender.view
            let loc     = sender.location(in: view)
            let subview = view?.hitTest(loc, with: nil)
            
            if subview == imageCropView && imageCropViewConstraintTop.constant == imageCropViewOriginalConstraintTop {
                
                return
            }
            
            dragStartPos = sender.location(in: self)
            
            cropBottomY = self.imageCropViewContainer.frame.origin.y + self.imageCropViewContainer.frame.height
            
            // Move
            if dragDirection == Direction.stop {
                
                dragDirection = (imageCropViewConstraintTop.constant == imageCropViewOriginalConstraintTop) ? Direction.up : Direction.down
            }
            
            // Scroll event of CollectionView is preferred.
            if (dragDirection == Direction.up   && dragStartPos.y < cropBottomY + dragDiff) ||
                (dragDirection == Direction.down && dragStartPos.y > cropBottomY) {
                    
                    dragDirection = Direction.stop
                    
                    imageCropView.changeScrollable(false)
                    
            } else {
                
                imageCropView.changeScrollable(true)
            }
            
        } else if sender.state == UIGestureRecognizerState.changed {
            
            let currentPos = sender.location(in: self)
            
            if dragDirection == Direction.up && currentPos.y < cropBottomY - dragDiff {
                
                imageCropViewConstraintTop.constant = max(imageCropViewMinimalVisibleHeight - self.imageCropViewContainer.frame.height, currentPos.y + dragDiff - imageCropViewContainer.frame.height)
                
                collectionViewConstraintHeight.constant = min(self.frame.height - imageCropViewMinimalVisibleHeight, self.frame.height - imageCropViewConstraintTop.constant - imageCropViewContainer.frame.height)
                
            } else if dragDirection == Direction.down && currentPos.y > cropBottomY {
                
                imageCropViewConstraintTop.constant = min(imageCropViewOriginalConstraintTop, currentPos.y - imageCropViewContainer.frame.height)
                
                collectionViewConstraintHeight.constant = max(self.frame.height - imageCropViewOriginalConstraintTop - imageCropViewContainer.frame.height, self.frame.height - imageCropViewConstraintTop.constant - imageCropViewContainer.frame.height)
                
            } else if dragDirection == Direction.stop && collectionView.contentOffset.y < 0 {
                
                dragDirection = Direction.scroll
                imaginaryCollectionViewOffsetStartPosY = currentPos.y
                
            } else if dragDirection == Direction.scroll {
                
                imageCropViewConstraintTop.constant = imageCropViewMinimalVisibleHeight - self.imageCropViewContainer.frame.height + currentPos.y - imaginaryCollectionViewOffsetStartPosY
                
                collectionViewConstraintHeight.constant = max(self.frame.height - imageCropViewOriginalConstraintTop - imageCropViewContainer.frame.height, self.frame.height - imageCropViewConstraintTop.constant - imageCropViewContainer.frame.height)
                
            }
            
        } else {
            
            imaginaryCollectionViewOffsetStartPosY = 0.0
            
            if sender.state == UIGestureRecognizerState.ended && dragDirection == Direction.stop {
                
                imageCropView.changeScrollable(true)
                return
            }
            
            let currentPos = sender.location(in: self)
            
            if currentPos.y < cropBottomY - dragDiff && imageCropViewConstraintTop.constant != imageCropViewOriginalConstraintTop {
                
                // The largest movement
                imageCropView.changeScrollable(false)
                
                imageCropViewConstraintTop.constant = imageCropViewMinimalVisibleHeight - self.imageCropViewContainer.frame.height
                
                collectionViewConstraintHeight.constant = self.frame.height - imageCropViewMinimalVisibleHeight
                
                UIView.animate(withDuration: 0.3, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: {
                    
                    self.layoutIfNeeded()
                    
                    }, completion: nil)
                
                dragDirection = Direction.down
                
            } else {
                
                // Get back to the original position
                imageCropView.changeScrollable(true)
                
                imageCropViewConstraintTop.constant = imageCropViewOriginalConstraintTop
                collectionViewConstraintHeight.constant = self.frame.height - imageCropViewOriginalConstraintTop - imageCropViewContainer.frame.height
                
                UIView.animate(withDuration: 0.3, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: {
                    
                    self.layoutIfNeeded()
                    
                    }, completion: nil)
                
                dragDirection = Direction.up
                
            }
        }
    }
    
    //MARK: - Format Time
    func timeFormatted(_ totalSeconds: Double) -> String {
        let sec:Int = Int(totalSeconds)
        let seconds: Int = sec % 60
        let minutes: Int = (sec / 60) % 60
        let hours: Int = sec / 3600
        if hours == 0 {
            return String(format: "%2d:%02d", minutes, seconds)
        }
        else {
            return String(format: "%2d:%2d", hours, minutes)
        }
    }
    
    // MARK: - Album Changed
    func albumChanged(_ albumImages:PHFetchResult<PHAsset>, albumTitle:String) {
        images = albumImages
        
        self.stopVideo(true)
        
        if images.count > 0 {
            selectedAsset = images.object(at: 0) as! PHAsset
            shouldPlayVideo = false
            changeAsset(images[0] as! PHAsset)
            collectionView.reloadData()
            collectionView.selectItem(at: IndexPath(row: 0, section: 0), animated: false, scrollPosition: UICollectionViewScrollPosition())
        }
    }
    
    // MARK: - Play Video
    @IBAction func videoPlayerTapped(_ sender: AnyObject) {
        printCustom("videoPlayerTapped")
        //self.stopVideo(true)
        
        if self.btnPlayVideo != nil {
            if !self.btnPlayVideo.isHidden {
                pauseOrResumeVideo(true)
            }
            else {
                pauseOrResumeVideo(false)
            }
        }
    }
    
    func btnPlayVideoPressed() {
        if !self.btnPlayVideo.isHidden {
            pauseOrResumeVideo(true)
        }
        else {
            pauseOrResumeVideo(false)
        }
    }
    
    // MARK: - Request Video
    func fetchVideoUrlAndPlayVideo(_ asset:PHAsset) {
        self.imageManager?.requestAVAsset(forVideo: asset, options: nil, resultHandler: { (asset, audioMix, info) in
            printCustom("video asset:\(asset)")
            
            if let urlAsset: AVURLAsset? = asset as? AVURLAsset {
                let localVideoUrl : URL = urlAsset!.url
                self.loadVideo(localVideoUrl)
            }
        })
    }
    
    // MARK: - Load Video
    func loadVideo(_ videoURL:URL) {
        videoItem = AVPlayerItem.init(url: videoURL)
        videoPlayer = AVPlayer.init(playerItem: videoItem)
        playerLayer = AVPlayerLayer(player: videoPlayer)
        
        DispatchQueue.main.async {
            self.playerLayer.frame = CGRect(x: 0, y: 0, width: self.imageCropView.frame.size.width, height: self.imageCropView.frame.size.width)
            self.playerLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
            self.playerLayer.backgroundColor = UIColor.clear.cgColor
            self.imageCropViewContainer.layer.addSublayer(self.playerLayer)
            
            self.btnPlayVideo = UIButton(frame: CGRect(x: self.imageCropView.frame.size.width/2 - 33/2,y: (self.imageCropView.frame.size.height/2 - 36/2), width: 33, height: 36))
            self.btnPlayVideo.setImage(Utilities().themedImage(img_playIcon), for: UIControlState())
            self.btnPlayVideo.addTarget(self, action: #selector(self.btnPlayVideoPressed), for: .touchUpInside)
            self.imageCropViewContainer.addSubview(self.btnPlayVideo)
            if self.shouldPlayVideo {
                self.btnPlayVideo.isHidden = true
            }
            else {
                self.btnPlayVideo.isHidden = false
            }
        }

        if shouldPlayVideo {
            videoPlayer.play()
        }
      
        //videoPlayer.addObserver(self, forKeyPath: "status", options: NSKeyValueObservingOptions(), context: nil)
        
       /* NSNotificationCenter.defaultCenter().addObserverForName(
            AVPlayerItemFailedToPlayToEndTimeNotification,
            object: nil,
            queue: nil,
            usingBlock: { notification in
                if self.videoPlayer != nil {
                    self.resetPlayerVariables()
                    self.btnPlayVideo.hidden = false
                }
        })*/
        
        NotificationCenter.default.addObserver(self, selector: #selector(itemFailedToPlayToEnd), name: NSNotification.Name.AVPlayerItemFailedToPlayToEndTime, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(itemDidPlayToEnd), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil)//addObserverForName(AVPlayerItemDidPlayToEndTimeNotification, object: nil, queue: nil) { notification in
//            if self.videoPlayer != nil {
//                self.resetPlayerVariables()
//                self.shouldPlayVideo = false
//                self.fetchVideoUrlAndPlayVideo(self.selectedAsset)
//            }
//        }
    }
    
    func itemDidPlayToEnd() {
        if self.videoPlayer != nil {
            self.resetPlayerVariables()
            self.shouldPlayVideo = false
            self.fetchVideoUrlAndPlayVideo(self.selectedAsset)
        }
    }
    
    func itemFailedToPlayToEnd() {
        if self.videoPlayer != nil {
            self.resetPlayerVariables()
            self.btnPlayVideo.isHidden = false
        }
    }

    // MARK: - Pause/Resume/Stop Video
    
    func pauseOrResumeVideo(_ resume:Bool) {
        if videoPlayer != nil {
            if selectedAsset.mediaType == .video {
                if resume {
                    self.btnPlayVideo.isHidden = true
                    videoPlayer.play()
                }
                else {
                    self.btnPlayVideo.isHidden = false
                    videoPlayer.pause()
                }
            }
        }
        else {
            self.fetchVideoUrlAndPlayVideo(selectedAsset)
        }
    }
    
    func stopVideo(_ showPlayIcon:Bool) {
        if videoPlayer != nil {
            videoPlayer.pause()
            if showPlayIcon {
                self.btnPlayVideo.isHidden = false
            }
            else {
                self.btnPlayVideo.isHidden = true
            }
            resetPlayerVariables()
        }
    }
    
    // MARK: - Reset Video Player
    func resetPlayerVariables() {
        if videoPlayer != nil {
            videoPlayer = nil
        }
        
        if videoItem != nil {
            videoItem = nil
        }
        
        if playerLayer != nil{
            playerLayer.removeFromSuperlayer()
        }
        
        if self.btnPlayVideo != nil {
            self.btnPlayVideo.removeFromSuperview()
        }
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.AVPlayerItemFailedToPlayToEndTime, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil)
    }
    
    // MARK: - UICollectionViewDelegate Protocol
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FSAlbumViewCell", for: indexPath) as! FSAlbumViewCell
        
        let currentTag = cell.tag + 1
        cell.tag = currentTag
        
        let asset = self.images[indexPath.item] as! PHAsset
        
        if asset.mediaType == .video {
            self.imageManager?.requestAVAsset(forVideo: asset, options: nil, resultHandler: { (asset, audioMix, info) in
                printCustom("video asset:\(asset)")
                
                if let urlAsset: AVURLAsset? = asset as? AVURLAsset {
                    let localVideoUrl : URL = urlAsset!.url
                    let sourceAsset:AVURLAsset = AVURLAsset(url: localVideoUrl, options: nil)
                    let duration:CMTime = sourceAsset.duration
                    let formattedDuration = self.timeFormatted(CMTimeGetSeconds(duration))
                    printCustom("video duration:\(formattedDuration)")
                    cell.duration = formattedDuration
                }

            })
        }
        else {
            cell.duration = ""
        }
        
        self.imageManager?.requestImage(for: asset,
                                                targetSize: cellSize,
                                                contentMode: .aspectFill,
                                                options: nil) {
                                                    result, info in
                                                    
                                                    if cell.tag == currentTag {
                                                        cell.image = result
                                                    }
                                                    
        }
        
         
      
        return cell
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return images == nil ? 0 : images.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        
        let width = (collectionView.frame.width - 3) / 3
        return CGSize(width: width, height: width)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedAsset = images[indexPath.row] as! PHAsset
        shouldPlayVideo = true
        changeAsset(images[indexPath.row] as! PHAsset)
        
        imageCropView.changeScrollable(true)
        
        imageCropViewConstraintTop.constant = imageCropViewOriginalConstraintTop
        collectionViewConstraintHeight.constant = self.frame.height - imageCropViewOriginalConstraintTop - imageCropViewContainer.frame.height
        
        UIView.animate(withDuration: 0.2, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: {
            
            self.layoutIfNeeded()
            
            }, completion: nil)
        
        dragDirection = Direction.up
        collectionView.scrollToItem(at: indexPath, at: .top, animated: true)
    }

    // MARK: - ScrollViewDelegate
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if scrollView == collectionView {
            self.updateCachedAssets()
        }
    }
    
    
    //MARK: - PHPhotoLibraryChangeObserver
    func photoLibraryDidChange(_ changeInstance: PHChange) {
        
        DispatchQueue.main.async {
            
            let collectionChanges = changeInstance.changeDetails(for: self.images)
            if collectionChanges != nil {
                
                self.images = collectionChanges!.fetchResultAfterChanges
                
                let collectionView = self.collectionView!
                
                if !collectionChanges!.hasIncrementalChanges || collectionChanges!.hasMoves {
                    
                    collectionView.reloadData()
                    
                } else {
                    
                    collectionView.performBatchUpdates({
                        let removedIndexes = collectionChanges!.removedIndexes
                        if (removedIndexes?.count ?? 0) != 0 {
                            collectionView.deleteItems(at: removedIndexes!.aapl_indexPathsFromIndexesWithSection(0))
                        }
                        let insertedIndexes = collectionChanges!.insertedIndexes
                        if (insertedIndexes?.count ?? 0) != 0 {
                            collectionView.insertItems(at: insertedIndexes!.aapl_indexPathsFromIndexesWithSection(0))
                        }
                        let changedIndexes = collectionChanges!.changedIndexes
                        if (changedIndexes?.count ?? 0) != 0 {
                            collectionView.reloadItems(at: changedIndexes!.aapl_indexPathsFromIndexesWithSection(0))
                        }
                        }, completion: nil)
                }
                
                self.resetCachedAssets()
            }
        }
    }
}

internal extension UICollectionView {
    
    func aapl_indexPathsForElementsInRect(_ rect: CGRect) -> [IndexPath] {
        let allLayoutAttributes = self.collectionViewLayout.layoutAttributesForElements(in: rect)
        if (allLayoutAttributes?.count ?? 0) == 0 {return []}
        var indexPaths: [IndexPath] = []
        indexPaths.reserveCapacity(allLayoutAttributes!.count)
        for layoutAttributes in allLayoutAttributes! {
            let indexPath = layoutAttributes.indexPath
            indexPaths.append(indexPath)
        }
        return indexPaths
    }
}

internal extension IndexSet {
    
    func aapl_indexPathsFromIndexesWithSection(_ section: Int) -> [IndexPath] {
        var indexPaths: [IndexPath] = []
        indexPaths.reserveCapacity(self.count)
        (self as NSIndexSet).enumerate({idx, stop in
            indexPaths.append(IndexPath(item: idx, section: section))
        })
        return indexPaths
    }
}

private extension FSAlbumView {
    
    func changeAsset(_ asset: PHAsset) {
        if self.btnPlayVideo != nil {
            self.btnPlayVideo.isHidden = true
        }
        self.imageCropView.image = nil
        self.phAsset = asset
        
        if shouldPlayVideo {
            if asset.mediaType == .video {
                self.stopVideo(false)
                
                self.fetchVideoUrlAndPlayVideo(asset)
            }
            else {
                self.stopVideo(false)
            }
        }
        else {
            if asset.mediaType == .video {
               self.fetchVideoUrlAndPlayVideo(asset)
            }
        }
       
        DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async(execute: {
            
            let options = PHImageRequestOptions()
            options.isNetworkAccessAllowed = true
            
            self.imageManager?.requestImage(for: asset,
                targetSize: CGSize(width: asset.pixelWidth, height: asset.pixelHeight),
                contentMode: .aspectFill,
                options: options) {
                    result, info in
                    
                    DispatchQueue.main.async(execute: {
                        
                        self.imageCropView.imageSize = CGSize(width: asset.pixelWidth, height: asset.pixelHeight)
                        self.imageCropView.image = result
                    })
            }
        })
    }
    
    // Check the status of authorization for PHPhotoLibrary
    func checkPhotoAuth() {
        
        PHPhotoLibrary.requestAuthorization { (status) -> Void in
            switch status {
            case .authorized:
                self.imageManager = PHCachingImageManager()
                if self.images != nil && self.images.count > 0 {
                    self.shouldPlayVideo = false
                    self.changeAsset(self.images[0] as! PHAsset)
                }
                
            case .restricted, .denied:
                DispatchQueue.main.async(execute: { () -> Void in
                    
                    self.delegate?.albumViewCameraRollUnauthorized()
                    
                })
            default:
                break
            }
        }
    }

    // MARK: - Asset Caching
    
    func resetCachedAssets() {
        
        imageManager?.stopCachingImagesForAllAssets()
        previousPreheatRect = CGRect.zero
    }
 
    func updateCachedAssets() {
        
        var preheatRect = self.collectionView!.bounds
        preheatRect = preheatRect.insetBy(dx: 0.0, dy: -0.5 * preheatRect.height)
        
        let delta = abs(preheatRect.midY - self.previousPreheatRect.midY)
        if delta > self.collectionView!.bounds.height / 3.0 {
            
            var addedIndexPaths: [IndexPath] = []
            var removedIndexPaths: [IndexPath] = []
            
            self.computeDifferenceBetweenRect(self.previousPreheatRect, andRect: preheatRect, removedHandler: {removedRect in
                let indexPaths = self.collectionView.aapl_indexPathsForElementsInRect(removedRect)
                removedIndexPaths += indexPaths
                }, addedHandler: {addedRect in
                    let indexPaths = self.collectionView.aapl_indexPathsForElementsInRect(addedRect)
                    addedIndexPaths += indexPaths
            })
            
            let assetsToStartCaching = self.assetsAtIndexPaths(addedIndexPaths)
            let assetsToStopCaching = self.assetsAtIndexPaths(removedIndexPaths)
            
            self.imageManager?.startCachingImages(for: assetsToStartCaching,
                targetSize: cellSize,
                contentMode: .aspectFill,
                options: nil)
            self.imageManager?.stopCachingImages(for: assetsToStopCaching,
                targetSize: cellSize,
                contentMode: .aspectFill,
                options: nil)
            
            self.previousPreheatRect = preheatRect
        }
    }
    
    func computeDifferenceBetweenRect(_ oldRect: CGRect, andRect newRect: CGRect, removedHandler: (CGRect)->Void, addedHandler: (CGRect)->Void) {
        if newRect.intersects(oldRect) {
            let oldMaxY = oldRect.maxY
            let oldMinY = oldRect.minY
            let newMaxY = newRect.maxY
            let newMinY = newRect.minY
            if newMaxY > oldMaxY {
                let rectToAdd = CGRect(x: newRect.origin.x, y: oldMaxY, width: newRect.size.width, height: (newMaxY - oldMaxY))
                addedHandler(rectToAdd)
            }
            if oldMinY > newMinY {
                let rectToAdd = CGRect(x: newRect.origin.x, y: newMinY, width: newRect.size.width, height: (oldMinY - newMinY))
                addedHandler(rectToAdd)
            }
            if newMaxY < oldMaxY {
                let rectToRemove = CGRect(x: newRect.origin.x, y: newMaxY, width: newRect.size.width, height: (oldMaxY - newMaxY))
                removedHandler(rectToRemove)
            }
            if oldMinY < newMinY {
                let rectToRemove = CGRect(x: newRect.origin.x, y: oldMinY, width: newRect.size.width, height: (newMinY - oldMinY))
                removedHandler(rectToRemove)
            }
        } else {
            addedHandler(newRect)
            removedHandler(oldRect)
        }
    }
    
    func assetsAtIndexPaths(_ indexPaths: [IndexPath]) -> [PHAsset] {
        if indexPaths.count == 0 { return [] }
        
        var assets: [PHAsset] = []
        assets.reserveCapacity(indexPaths.count)
        for indexPath in indexPaths {
            let asset = self.images[indexPath.item] as! PHAsset
            assets.append(asset)
        }
        return assets
    }
    
}
