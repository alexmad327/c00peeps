//
//  FusumaViewController.swift
//  Fusuma
//
//  Created by Yuta Akizuki on 2015/11/14.
//  Copyright © 2015年 ytakzk. All rights reserved.
//

import UIKit
import Photos


@objc public protocol FusumaDelegate: class {
    
    func fusumaImageSelected(_ image: UIImage)
    @objc optional func fusumaDismissedWithImage(_ image: UIImage)
    func fusumaVideoCompleted(withFileURL fileURL: URL)
    func fusumaCameraRollUnauthorized()
    
    @objc optional func fusumaClosed()
}

public var fusumaBaseTintColor   = UIColor.hex("#FFFFFF", alpha: 1.0)
public var fusumaTintColor       = UIColor.hex("#009688", alpha: 1.0)
public var fusumaBackgroundColor = UIColor.hex("#212121", alpha: 1.0)

public var fusumaAlbumImage : UIImage? = nil
public var fusumaCameraImage : UIImage? = nil
public var fusumaVideoImage : UIImage? = nil
public var fusumaCheckImage : UIImage? = nil
public var fusumaCloseImage : UIImage? = nil
public var fusumaFlashOnImage : UIImage? = nil
public var fusumaFlashOffImage : UIImage? = nil
public var fusumaFlipImage : UIImage? = nil
public var fusumaShotImage : UIImage? = nil

public var fusumaVideoStartImage : UIImage? = nil
public var fusumaVideoStopImage : UIImage? = nil

public var fusumaCropImage: Bool = true

public var fusumaAlbumTitle = "Camera Roll"

public var fusumaTintIcons : Bool = true

//@objc public class FusumaViewController: UIViewController, FSCameraViewDelegate, FSAlbumViewDelegate {
public final class FusumaViewController: UIViewController {
    
    var willFilter = true
    var isComingFromDigitalTab = false
    var isComingFromPeepTextFlow = false

    @IBOutlet weak var photoLibraryViewerContainer: UIView!
    
    lazy var albumView  = FSAlbumView.instance()

    fileprivate var hasGalleryPermission: Bool {
        return PHPhotoLibrary.authorizationStatus() == .authorized
    }
    
    public weak var delegate: FusumaDelegate? = nil
    var eventDelegate: CreateEventSecondControllerDelegate?
    var photoAlbumViewController:PhotoAlbumsViewController?

    override public func loadView() {
        
        if let view = UINib(nibName: "FusumaViewController", bundle: Bundle(for: self.classForCoder)).instantiate(withOwner: self, options: nil).first as? UIView {
            
            self.view = view
        }
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        fusumaAlbumTitle = "Camera Roll"
        
        fusumaTintColor = Utilities().themedMultipleColor()[0]
        
        // To Hide back button title of next controller.
        self.navigationItem.title = ""
        
        self.navigationItem.titleView = self.titleViewAlbumSelection(fusumaAlbumTitle, imgUpDown:Utilities().themedImage(img_downArrowGallery))
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Next", style: .plain, target: self, action: #selector(btnNextClicked))
        
        if isComingFromDigitalTab == true
        {
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancel))
        }
        
        albumView.delegate  = self

        self.view.bringSubview(toFront: photoLibraryViewerContainer)
        
        photoLibraryViewerContainer.addSubview(albumView)
        
        //self.extendedLayoutIncludesOpaqueBars = true
    }
    
    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if isGalleryMediaSent {
            self.cancel()
        }
        //self.tabBarController?.tabBar.hidden = true
        
    }

    override public func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        //let pp = CGPointMake(0, 64)
        albumView.frame  = CGRect(origin: CGPoint.zero, size: photoLibraryViewerContainer.frame.size)
        albumView.layoutIfNeeded()
        
        albumView.initialize()
    }
    
    public override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
       albumView.stopVideo(true)
    }

    override public var prefersStatusBarHidden : Bool {
        
        return true
    }
    
    //MARK: - Navigation bar Title View
    func titleViewAlbumSelection(_ title:String, imgUpDown:UIImage) -> UIView {
        let vwTitle:UIView = UIView(frame: CGRect(x: 0,y: 0,width: ApplicationDelegate.window!.frame.size.width - 120, height: 40))

        let btnTitle: UIButton = UIButton()
        btnTitle.titleLabel!.textAlignment = NSTextAlignment.left
        btnTitle.titleLabel!.lineBreakMode = NSLineBreakMode.byTruncatingTail
        btnTitle.setTitle("\(title)", for: UIControlState())
        btnTitle.titleLabel!.textAlignment = NSTextAlignment.center
        btnTitle.titleLabel!.font = ProximaNovaRegular(17)
        btnTitle.setTitleColor(UIColor.white, for: UIControlState())
        let btnTitleWidth = titleWidth(btnTitle.titleLabel!.text! as NSString, height: btnTitle.frame.size.height, font: (btnTitle.titleLabel?.font)!)
        btnTitle.frame = CGRect(x: vwTitle.frame.size.width/2 - (btnTitleWidth + imgUpDown.size.width)/2, y: 0, width: btnTitleWidth, height: 40)
        btnTitle.addTarget(self, action: #selector(titleButtonPressed), for: UIControlEvents.touchUpInside)
        vwTitle.addSubview(btnTitle)
        
        let btnUpDownArrow: UIButton = UIButton()
        btnUpDownArrow.frame = CGRect(x: btnTitle.frame.origin.x + btnTitle.frame.size.width + 5, y: vwTitle.frame.size.height/2 - imgUpDown.size.height/2, width: imgUpDown.size.width, height: imgUpDown.size.height)
        btnUpDownArrow.setImage(imgUpDown, for: UIControlState())
        btnUpDownArrow.addTarget(self, action: #selector(titleButtonPressed), for: UIControlEvents.touchUpInside)
        vwTitle.addSubview(btnUpDownArrow)
        
        return vwTitle
    }

    //MARK: - Navigation bar Title Width
    func titleWidth(_ title:NSString, height:CGFloat, font:UIFont) -> CGFloat {
        var titleSize:CGSize = CGSize(width: 0, height: 0)
        if title != "" {
            titleSize = title.boundingRect(with: CGSize(width: CGFloat.greatestFiniteMagnitude, height: height), options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font:font], context: nil).size
        }
        let navItemsSize:CGFloat = 140.0
        if titleSize.width > ApplicationDelegate.window!.frame.size.width - navItemsSize {
            return ApplicationDelegate.window!.frame.size.width - navItemsSize
        }
        else {
            return titleSize.width
        }
    }

    
    //MARK: - Button Actions
    func titleButtonPressed() {
        if photoAlbumViewController == nil {
            self.openAlbumsView()
        }
        else {
            self.closeAlbumsView()
        }
    }
    
    func openAlbumsView () {
        self.navigationItem.titleView = self.titleViewAlbumSelection(fusumaAlbumTitle, imgUpDown:Utilities().themedImage(img_downArrowGallery))
        
        photoAlbumViewController = PhotoAlbumsViewController(nibName: "PhotoAlbumsViewController", bundle: nil)
        photoAlbumViewController!.delegate = self
        photoAlbumViewController!.albumTitle = fusumaAlbumTitle
        self.view.layer.add(Utilities().addPresentModalAnimation(), forKey:nil)
        self.addChildViewController(photoAlbumViewController!)
        self.view.addSubview(photoAlbumViewController!.view)
        photoAlbumViewController!.didMove(toParentViewController: self)
    }
    
    func closeAlbumsView () {
        self.navigationItem.titleView = self.titleViewAlbumSelection(fusumaAlbumTitle, imgUpDown:Utilities().themedImage(img_downArrowGallery))

        self.view.layer.add(Utilities().addDismissModalAnimation(), forKey:nil)
        photoAlbumViewController!.view.removeFromSuperview()
        photoAlbumViewController!.removeFromParentViewController()
        photoAlbumViewController = nil
    }
    
    @IBAction func closeButtonPressed(_ sender: UIButton) {
        self.dismiss(animated: true, completion: {
            
            self.delegate?.fusumaClosed?()
        })
    }
    
    @IBAction func doneButtonPressed(_ sender: UIButton) {
        let view = albumView.imageCropView

        if fusumaCropImage {
            let normalizedX = (view?.contentOffset.x)! / (view?.contentSize.width)!
            let normalizedY = (view?.contentOffset.y)! / (view?.contentSize.height)!
            
            let normalizedWidth = (view?.frame.width)! / (view?.contentSize.width)!
            let normalizedHeight = (view?.frame.height)! / (view?.contentSize.height)!
            
            let cropRect = CGRect(x: normalizedX, y: normalizedY, width: normalizedWidth, height: normalizedHeight)
            
            DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async(execute: {
                
                let options = PHImageRequestOptions()
                options.deliveryMode = .highQualityFormat
                options.isNetworkAccessAllowed = true
                options.normalizedCropRect = cropRect
                options.resizeMode = .exact
                
                let targetWidth = floor(CGFloat(self.albumView.phAsset.pixelWidth) * cropRect.width)
                let targetHeight = floor(CGFloat(self.albumView.phAsset.pixelHeight) * cropRect.height)
                let dimension = max(min(targetHeight, targetWidth), 1024 * UIScreen.main.scale)
                
                let targetSize = CGSize(width: dimension, height: dimension)
                
                PHImageManager.default().requestImage(for: self.albumView.phAsset, targetSize: targetSize,
                contentMode: .aspectFill, options: options) {
                    result, info in
                    
                    DispatchQueue.main.async(execute: {
                        self.delegate?.fusumaImageSelected(result!)
                        
                        self.dismiss(animated: true, completion: {
                            self.delegate?.fusumaDismissedWithImage?(result!)
                        })
                    })
                }
            })
        } else {
            delegate?.fusumaImageSelected((view?.image)!)
            
            self.dismiss(animated: true, completion: {
                self.delegate?.fusumaDismissedWithImage?((view?.image)!)
            })
        }
    }
    
    func cancel(){
        
        self.dismiss(animated: true, completion: nil)
    }
    
    func btnNextClicked() {
        let view = albumView.imageCropView
        if self.albumView.phAsset.mediaType == .video {
            PHImageManager.default().requestAVAsset(forVideo: self.albumView.phAsset, options: nil, resultHandler: { (asset, audioMix, info) in
                printCustom("video asset:\(asset)")
                
                    if let urlAsset: AVURLAsset? = asset as? AVURLAsset {
                        
                        let localVideoUrl : URL = urlAsset!.url
                        
                        DispatchQueue.main.async(execute: {
                            
                            let asset = AVURLAsset(url: localVideoUrl)
                            let audioDuration = asset.duration
                            let audioDurationSeconds = CMTimeGetSeconds(audioDuration)
                            if( audioDurationSeconds > maxVideoRecordingTime)
                            {
                                let alert:UIAlertController = UIAlertController(title: "", message: alertMsg_MaxVideoLimit, preferredStyle: .alert)
                                let alertAction = UIAlertAction(title: alertBtnTitle_OK, style: .default, handler: nil)
                                alert.addAction(alertAction)
                                self.present(alert, animated: true, completion: nil)
                            }
                            else{
                                
                                self.moveToVideoEdit(localVideoUrl, image: (view?.image)!)
                            }
                        })
                    }
            })
        }
        else {
            if fusumaCropImage {
                let normalizedX = (view?.contentOffset.x)! / (view?.contentSize.width)!
                let normalizedY = (view?.contentOffset.y)! / (view?.contentSize.height)!
                
                let normalizedWidth = (view?.frame.width)! / (view?.contentSize.width)!
                let normalizedHeight = (view?.frame.height)! / (view?.contentSize.height)!
                
                let cropRect = CGRect(x: normalizedX, y: normalizedY, width: normalizedWidth, height: normalizedHeight)
                
                DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async(execute: {
                    
                    let options = PHImageRequestOptions()
                    options.deliveryMode = .highQualityFormat
                    options.isNetworkAccessAllowed = true
                    options.normalizedCropRect = cropRect
                    options.resizeMode = .exact
                    
                    let targetWidth = floor(CGFloat(self.albumView.phAsset.pixelWidth) * cropRect.width)
                    let targetHeight = floor(CGFloat(self.albumView.phAsset.pixelHeight) * cropRect.height)
                    let dimension = max(min(targetHeight, targetWidth), 1024 * UIScreen.main.scale)
                    
                    let targetSize = CGSize(width: dimension, height: dimension)
                    
                        PHImageManager.default().requestImage(for: self.albumView.phAsset, targetSize: targetSize,
                        contentMode: .aspectFill, options: options) {
                            result, info in
                            
                            DispatchQueue.main.async(execute: {
                                self.moveToPhotoEdit(result!)
                                
                            })
                        }
                })
            } else {
                self.moveToPhotoEdit((view?.image)!)
            }
        }
    }
    
    func moveToPhotoEdit(_ image:UIImage) {
        
        mediaType = MediaType.photo
        mediaAttachment = MediaAttachment.photo

        if isComingFromPeepTextFlow == true {
            //mediaAttachment = MediaAttachment.photo
            let st = UIStoryboard(name: "Digital", bundle:nil)
            let obj = st.instantiateViewController(withIdentifier: "PhotoFilterEdit") as! PhotoFilterEdit
            obj.imagePhoto = image
            obj.dataPath = ""
            obj.isComingFromPeepTextFlow = isComingFromPeepTextFlow
            obj.isComingFromEventsFlow = false
            self.navigationController?.pushViewController(obj, animated: true)
        }
        //This will work when coming from digital screen
        else if isComingFromDigitalTab == true
        {
            let st = UIStoryboard(name: "Digital", bundle:nil)
            let obj = st.instantiateViewController(withIdentifier: "PhotoAddAudio") as! PhotoAddAudio
            obj.imagePhoto = image
            obj.isComingFromPeepTextFlow = false
            obj.isComingFromEventsFlow = false
            self.navigationController?.pushViewController(obj, animated: true)
        }
        else //This will work when coming from event screen
        {
            //mediaAttachment = MediaAttachment.photo
            let st = UIStoryboard(name: "Digital", bundle:nil)
            let obj = st.instantiateViewController(withIdentifier: "PhotoAddAudio") as! PhotoAddAudio
            obj.imagePhoto = image
            obj.isComingFromPeepTextFlow = false
            obj.isComingFromEventsFlow = true
            obj.delegate = self.eventDelegate
            self.navigationController?.pushViewController(obj, animated: true)
        }
    }
    
    func moveToVideoEdit(_ url:URL, image:UIImage) {
        
         mediaType = MediaType.video
        mediaAttachment = MediaAttachment.video

        
        if isComingFromPeepTextFlow == true {
            //mediaAttachment = MediaAttachment.video

            let st = UIStoryboard(name: "Digital", bundle:nil)
            let obj = st.instantiateViewController(withIdentifier: "VideoFilterController") as! VideoFilterController
            obj.sampleURL = url
            obj.navigationController?.navigationBar.isHidden = false
            obj.updatedPhoto = image
            obj.isComingFromPeepTextFlow = true
            obj.isComingFromEventsFlow = false
            obj.strPeepText = peepTextConstant
            self.navigationController?.pushViewController(obj, animated: true)
        }
        //This will work when coming from digital screen
        else if isComingFromDigitalTab == true
        {

            let st = UIStoryboard(name: "Digital", bundle:nil)
            let obj = st.instantiateViewController(withIdentifier: "VideoFilterController") as! VideoFilterController
            obj.sampleURL = url
            obj.navigationController?.navigationBar.isHidden = false
            obj.updatedPhoto = image
            obj.isComingFromPeepTextFlow = false
            obj.isComingFromEventsFlow = false
            obj.strPeepText = peepTextConstant
            self.navigationController?.pushViewController(obj, animated: true)
        }
        else //This will work when coming from event screen
        {
           // mediaAttachment = MediaAttachment.video

            let st = UIStoryboard(name: "Digital", bundle:nil)
            let obj = st.instantiateViewController(withIdentifier: "VideoFilterController") as! VideoFilterController
            obj.sampleURL = url
            obj.navigationController?.navigationBar.isHidden = false
            obj.updatedPhoto = image
            obj.isComingFromPeepTextFlow = false
            obj.isComingFromEventsFlow = true
            obj.strPeepText = peepTextConstant
            obj.delegate = self.eventDelegate
            self.navigationController?.pushViewController(obj, animated: true)
        }
    }
    
    func thumbnailVideo(_ url:URL)->UIImage{
        var uiImage = UIImage()
        
        do {
            let asset = AVURLAsset(url: url, options: nil)
            let imgGenerator = AVAssetImageGenerator(asset: asset)
            imgGenerator.appliesPreferredTrackTransform = true
            let cgImage = try imgGenerator.copyCGImage(at: CMTimeMake(0, 1), actualTime: nil)
            uiImage = UIImage(cgImage: cgImage)
            
            uiImage = Utilities().imageResize(uiImage,sizeChange: CGSize(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.width * (4/3)))
            
            uiImage = Utilities().cropToBounds(uiImage, cgwidth: UIScreen.main.bounds.width, cgheight: UIScreen.main.bounds.width)
            
            printCustom("width: \(uiImage.size.width) height \(uiImage.size.height)")
            
            // lay out this image view, or if it already exists, set its image property to uiImage
        } catch let error as NSError {
            printCustom("Error generating thumbnail: \(error)")
        }
        
        return uiImage
    }
    
    func albumChanged(_ albumImages:PHFetchResult<AnyObject>, albumTitle:String) {
        fusumaAlbumTitle = albumTitle
        self.closeAlbumsView()
        albumView.albumChanged(albumImages as! PHFetchResult<PHAsset>, albumTitle: albumTitle)
    }
    
}

extension FusumaViewController: FSAlbumViewDelegate {
    // MARK: FSAlbumViewDelegate
    public func albumViewCameraRollUnauthorized() {
        delegate?.fusumaCameraRollUnauthorized()
    }
}

private extension FusumaViewController {
    
    func highlightButton(_ button: UIButton) {
        
        button.tintColor = fusumaTintColor
        
        button.addBottomBorder(fusumaTintColor, width: 3)
    }
}
