 //
//  Netcom.swift
//  C00Peeps
//
//  Created by OSX on 06/13/16.
//  Copyright © 2016 SOTSYS011. All rights reserved.
//

import Foundation
import Alamofire

enum RequestType {
    
    case request_LOGIN
    case request_CHECKSOCIALUSEREXISTS
    case request_SIGNUP
    case request_FORGOTPASSWORD
    case request_CHANGEPASSWORD
    case request_EDITPROFILE
    case request_GETUSERDETAILS
    case request_SETACCOUNTTYPE
    case request_LOGOUT
    case request_PEEPCODE
    case request_GETCOMMENTS
    case request_ADDCOMMENTS
    case request_SETTHEMETYPE
    case request_FETCHTHEMEIMAGESFOLDERURL
    case request_DOWNLOADTHEMEIMAGESFILE
    case request_UPLOADMEDIA
    case request_SENDPEEPTEXT
    case request_GETCONTACTS
    case request_GETFOLLOWERS
    case request_SEARCHCONTACTS
    case request_USERTYPE
    case report_MEDIA
    case request_LIKEMEDIA
    case request_DISLIKEMEDIA
    case request_GETPEOPLE
    case request_GETLIKERPEOPLE
    case request_GETDISLIKERPEOPLE
    case request_SEARCHPEOPLE
    case request_SENDFOLLOWREQUEST
    case request_SENDUNFOLLOWREQUEST
    case request_GETPUBLICMOVIEMEDIA
    case request_GETPUBLICMUSICMEDIA
    case request_GETPUBLICVIDEOGAMESMEDIA
    case request_SEARCHPUBLICMEDIA
    case request_SENDEVENT
    case request_GETGALLERYMOVIES
    case request_GETGALLERYMUSIC
    case request_GETGALLERYVIDEOGAMES
    case request_GETGALLERYPHOTOS
    case request_GETGALLERYPEEPTEXT
    case request_GETPOSTSLIKED
    case request_FETCHEVENTS
    case request_FETCHEVENTCATEGORIES
    case request_PUSHNOTIFICATIONSETTING
    case request_GETPUSHNOTIFICATIONSETTING
    case request_DELETEEVENT
    case request_DELETEEVENTINVITE
    case request_EDITEVENT
    case request_FETCHNOTREGISTEREDPECXUSERS
    case request_FETCHNOTFOLLOWINGPECXUSERS
    case request_GETNOTIFICATIONLIST
    case request_GETUNREADNOTIFICATIONCOUNT
    case request_SETREADNOTIFICATION
    case request_SHAREMEDIA
    case request_SETFEEDBACK
    case request_LIKEPEEPTEXT
    case request_DISLIKEPEEPTEXT
    case request_APPROVEDISAPPROVEFOLLOWREQUEST
    case request_PEEPCODEVERIFY
    case request_GETPEEPTEXTCONVERSATION
    case request_SENDINVITETOCONTACTS
    case request_ACCEPTEVENTINVITATION
    case request_REJECTEVENTINVITATION
    case request_FETCHCOUNTRIES
    case request_FETCHREGIONS
    case request_FETCHLOCALITIES
    case request_CHECKSUBMERCHANTACCOUNTEXISTS
    case request_CREATESUBMERCHANTACCOUNT
    case request_UPDATESUBMERCHANTACCOUNT
    case request_CREATEBRAINTREETOKEN
    case request_PROCESSBRAINTREEPAYMENT
    case request_GETSAVEDCARDS
    case request_GETFOLLOWINGPEOPLE
    case request_GETFOLLOWERPEOPLE
    case request_GETUSERPOSTS
    case request_GETTOPFIVEUSERS
    case request_UPDATEPACKAGE

}

class Netcom {
    
    var requestType:RequestType!
    var alamofireManager : Alamofire.SessionManager?
    
    init() {
        let configuration = URLSessionConfiguration.default
        
        configuration.timeoutIntervalForResource = 300.0 // 5 minutes
        configuration.timeoutIntervalForRequest = 300.0 // 5 minutes
        self.alamofireManager = Alamofire.SessionManager(configuration: configuration)
    }
    
    //MARK: - Fetch SignUp Form Related Data
    func netComFormDataSignUpPage (_ parameters: [String: AnyObject]?=nil) {
        
        var request : Request
        self.requestType = RequestType.request_USERTYPE
        
        let id = parameters! as NSDictionary
        let str:NSString = id.object(forKey: "signUpType")! as! NSString
        var requestURL = ""
        if (str == "2") {   // FILM
            requestURL = BASE_URL + "users/getfilmspecs"
        }
        if (str == "3") {   // MUSIC
            requestURL = BASE_URL + "users/getmusicspecs"
        }
        if (str == "4") {   // GAMES
            requestURL = BASE_URL + "users/getgamespecs"
        }
        
        request = Alamofire.request(requestURL)
        handleResponse(request as! DataRequest, notificationName:NOTIFICATION_USERTYPE)
        APIManager.sharedInstance.arrRequests.append((self,request))
    }
    
    //MARK: - Login
    func netComLogin(_ parameters: [String: AnyObject]?=nil) {
        
        var request : Request
        self.requestType = RequestType.request_LOGIN
        
        let requestURL = BASE_URL + "users/login"
        
        request = Alamofire.request(requestURL,method: HTTPMethod.post, parameters: parameters, encoding: JSONEncoding.default, headers: nil)
       // request = Alamofire.request(.POST, requestURL, parameters: parameters, encoding:.JSON)
        
        handleResponse(request as! DataRequest, notificationName:NOTIFIACTION_LOGIN)
        
        APIManager.sharedInstance.arrRequests.append((self,request))
        
    }
    
    //MARK: - Check Social User Exists
    func netComCheckSocialUserExists(_ parameters: [String: AnyObject]?=nil) {
        var request : Request
        self.requestType = RequestType.request_CHECKSOCIALUSEREXISTS
        
        let requestURL = BASE_URL + "home/checksocialuserexists"
        
        request = Alamofire.request(requestURL, method: HTTPMethod.post, parameters: parameters, encoding: JSONEncoding.default, headers: nil)

        
        //request = Alamofire.request(.POST, requestURL, parameters: parameters, encoding:.JSON)
        
        handleResponse(request as! DataRequest, notificationName:NOTIFICATION_CHECKSOCIALUSEREXISTS)
        
        APIManager.sharedInstance.arrRequests.append((self,request))
    }

    
    //MARK: - Signup
    func netComSignUp(_ parameters: [String: AnyObject]?=nil) {
        
        var request : Request
        self.requestType = RequestType.request_SIGNUP
        
        let requestURL = BASE_URL + "users"
        request = Alamofire.request(requestURL, method: HTTPMethod.post, parameters: parameters, encoding: JSONEncoding.default, headers: nil)

        //request = Alamofire.request(.POST, requestURL, parameters: parameters, encoding:.JSON)

        
//        request = Alamofire.request(.POST, requestURL, parameters: parameters, encoding:.JSON)
        handleResponse(request as! DataRequest, notificationName:NOTIFIACTION_SIGNUP)
        APIManager.sharedInstance.arrRequests.append((self,request))
    }
    
    //MARK: - Forgot Password
    func netComForgotPassword(_ parameters: [String: AnyObject]?=nil) {
        
        var request : Request
        self.requestType = RequestType.request_FORGOTPASSWORD
        let requestURL = BASE_URL + "users/resetpwd"
        request = Alamofire.request(requestURL, method: HTTPMethod.post, parameters: parameters, encoding: JSONEncoding.default, headers: nil)

       // request = Alamofire.request(.POST, requestURL, parameters: parameters, encoding:.JSON)
        handleResponse(request as! DataRequest, notificationName:NOTIFIACTION_FORGOTPASSWORD)
        APIManager.sharedInstance.arrRequests.append((self,request))
        
    }
    
    //MARK: - Change Password
    func netComChangePassword(_ parameters: [String: AnyObject]?=nil) {
        
        var request : Request
        self.requestType = RequestType.request_CHANGEPASSWORD
        let requestURL = BASE_URL + "users/changepassword"
        request = Alamofire.request(requestURL, method: HTTPMethod.post, parameters: parameters, encoding: JSONEncoding.default, headers: nil)

        //request = Alamofire.request(.POST, requestURL, parameters: parameters, encoding:.JSON)
        handleResponse(request as! DataRequest, notificationName:NOTIFIACTION_CHANGEPASSWORD)
        APIManager.sharedInstance.arrRequests.append((self,request))
    }

    //MARK: - Edit Profile
    func netComEditProfile(_ parameters: [String: AnyObject]?=nil) {
        var request : Request
        self.requestType = RequestType.request_EDITPROFILE
        let requestURL = BASE_URL + "users/editprofile"
        printCustom("requestURL:\(requestURL)")
        printCustom("parameters:\(parameters)")

        request = Alamofire.request(requestURL, method: HTTPMethod.post, parameters: parameters, encoding: JSONEncoding.default, headers: nil)

        //request = Alamofire.request(.POST, requestURL, parameters: parameters, encoding:.JSON)
        handleResponse(request as! DataRequest, notificationName:NOTIFIACTION_EDITPROFILE)
        APIManager.sharedInstance.arrRequests.append((self,request))
    }
    
    //users/getsingleuserdetails)
    //MARK: - Edit Profile
    func netComGetUserDetails(_ parameters: [String: AnyObject]?=nil) {
        var request : Request
        self.requestType = RequestType.request_GETUSERDETAILS
        let requestURL = BASE_URL + "users/getsingleuserdetails"
        printCustom("requestURL:\(requestURL)")
        printCustom("parameters:\(parameters)")
        
        request = Alamofire.request(requestURL, method: HTTPMethod.post, parameters: parameters, encoding: JSONEncoding.default, headers: nil)

        //request = Alamofire.request(.POST, requestURL, parameters: parameters, encoding:.JSON)
        handleResponse(request as! DataRequest, notificationName:NOTIFIACTION_GETUSERDETAILS)
        APIManager.sharedInstance.arrRequests.append((self,request))
    }
    
    //MARK: - Set Account Type
    func netComSaveAccountType(_ parameters: [String: AnyObject]?=nil) {
        
        var request : Request
        self.requestType = RequestType.request_SETACCOUNTTYPE
        let requestURL = BASE_URL + "users/setprivacylevel"

        request = Alamofire.request(requestURL, method: HTTPMethod.post, parameters: parameters, encoding: JSONEncoding.default, headers: nil)

        //request = Alamofire.request(.POST, requestURL, parameters: parameters, encoding:.JSON)
        handleResponse(request as! DataRequest, notificationName:NOTIFIACTION_SETACCOUNTTYPE)
        APIManager.sharedInstance.arrRequests.append((self,request))
        
    }
    
    //MARK: - Set Theme
    func netComThemeType(_ parameters: [String: AnyObject]?=nil) {
        
        var request : Request
        self.requestType = RequestType.request_SETTHEMETYPE
        let requestURL = BASE_URL + "users/setthemeid"
        
        request = Alamofire.request(requestURL, method: HTTPMethod.post, parameters: parameters, encoding: JSONEncoding.default, headers: nil)

        //request = Alamofire.request(.POST, requestURL, parameters: parameters, encoding:.JSON)
        handleResponse(request as! DataRequest, notificationName:NOTIFIACTION_THEMETYPE)
        APIManager.sharedInstance.arrRequests.append((self,request))
        
    }
    
    //MARK: - Download Theme Images
    func netComFetchThemeImagesFolderUrl(_ parameters: [String: AnyObject]?=nil) {
        var request : Request
        self.requestType = RequestType.request_FETCHTHEMEIMAGESFOLDERURL
        let requestURL = BASE_URL + "users/themeimages"
        printCustom("requestURL:\(requestURL)")
        printCustom("parameters:\(parameters)")
        
        request = Alamofire.request(requestURL, method: HTTPMethod.post, parameters: parameters, encoding: JSONEncoding.default, headers: nil)

       // request = Alamofire.request(.POST, requestURL, parameters: parameters, encoding:.JSON)
        handleResponse(request as! DataRequest, notificationName:NOTIFICATION_FETCHTHEMEIMAGESFOLDERURL)
        APIManager.sharedInstance.arrRequests.append((self,request))
    }
    
    func netComDownloadThemeImagesFile(_ folderUrl:String) {
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
        var request = URLRequest(url: URL(string: folderUrl)!)
        request.cachePolicy = URLRequest.CachePolicy.reloadIgnoringLocalCacheData
        
       
        let task = session.dataTask(with: request, completionHandler: { (data, response, error) in
            if (error == nil) {
                // Success
                let statusCode = (response as! HTTPURLResponse).statusCode
                printCustom("Success: \(statusCode)")
                
                // This is your file-variable:
                // data
                NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: NOTIFICATION_DOWNLOADTHEMEIMAGESFILE), object: data)

            }
            else {
                // Failure
                printCustom("Failure: \(error!.localizedDescription)");
            }
        }) 
        task.resume()
    }
    
    //MARK: - Logout
    func netComLogout(_ parameters: [String: AnyObject]?=nil) {
        
        var request : Request
        self.requestType = RequestType.request_LOGOUT
        let requestURL = BASE_URL + "users/logout"

        request = Alamofire.request(requestURL, method: HTTPMethod.post, parameters: parameters, encoding: JSONEncoding.default, headers: nil)

//        request = Alamofire.request(.POST, requestURL, parameters: parameters, encoding:.JSON)
        handleResponse(request as! DataRequest, notificationName:NOTIFIACTION_LOGOUT)
        APIManager.sharedInstance.arrRequests.append((self,request))
        
    }
    
    //MARK: - Set Peep Code
    func netComSetPeepCode(_ parameters: [String: AnyObject]?=nil) {
        
        var request : Request
        self.requestType = RequestType.request_PEEPCODE
        let requestURL = BASE_URL + "users/peepcode"

        request = Alamofire.request(requestURL, method: HTTPMethod.post, parameters: parameters, encoding: JSONEncoding.default, headers: nil)

//        request = Alamofire.request(.POST, requestURL, parameters: parameters, encoding:.JSON)
        handleResponse(request as! DataRequest, notificationName:NOTIFIACTION_SETPEEPCODE)
        APIManager.sharedInstance.arrRequests.append((self,request))
    }
    
    //MARK: - Set PushNotification
    func netComSetPushNotification(_ parameters: [String: AnyObject]?=nil) {
        
        var request : Request
        self.requestType = RequestType.request_PUSHNOTIFICATIONSETTING
        let requestURL = BASE_URL + "users/updatenotificationsettings"
        request = Alamofire.request(requestURL, method: HTTPMethod.post, parameters: parameters, encoding: JSONEncoding.default, headers: nil)

//        request = Alamofire.request(.POST, requestURL, parameters: parameters, encoding:.JSON)
        handleResponse(request as! DataRequest, notificationName:NOTIFIACTION_SETPUSHNOTIFICATION)
        APIManager.sharedInstance.arrRequests.append((self,request))
    }
    //MARK: - Get PushNotification
    func netComGetPushNotification(_ parameters: [String: AnyObject]?=nil) {
        
        var request : Request
        self.requestType = RequestType.request_GETPUSHNOTIFICATIONSETTING
        let requestURL = BASE_URL + "users/getnotificationsettings"
        
        request = Alamofire.request(requestURL, method: HTTPMethod.post, parameters: parameters, encoding: JSONEncoding.default, headers: nil)

//        request = Alamofire.request(.POST, requestURL, parameters: parameters, encoding:.JSON)
        handleResponse(request as! DataRequest, notificationName:NOTIFIACTION_GETPUSHNOTIFICATION)
        APIManager.sharedInstance.arrRequests.append((self,request))
    }
    
    //MARK: - Upgrade Package
    func netComUpgradePackage(_ parameters: [String: AnyObject]?=nil) {
        
        var request : Request
        self.requestType = RequestType.request_UPDATEPACKAGE
        let requestURL = BASE_URL + "home/setpackage"
        printCustom("requestURL:\(requestURL)")
        printCustom("parameters:\(parameters)")
        request = Alamofire.request(requestURL, method: HTTPMethod.post, parameters: parameters, encoding: JSONEncoding.default, headers: nil)

//        request = Alamofire.request(.POST, requestURL, parameters: parameters, encoding:.JSON)
        handleResponse(request as! DataRequest, notificationName:NOTIFICATION_UPDATEPACKAGE)
        APIManager.sharedInstance.arrRequests.append((self,request))
        
    }
    
    //MARK: - GetFeeds
    func netComGetFeeds(_ parameters: [String: AnyObject]?=nil) {
        
        var request : Request
        self.requestType = RequestType.request_PEEPCODE
        let requestURL = BASE_URL + "home/feeds"
        request = Alamofire.request(requestURL, method: HTTPMethod.post, parameters: parameters, encoding: JSONEncoding.default, headers: nil)

//        request = Alamofire.request(.POST, requestURL, parameters: parameters, encoding:.JSON)
        handleResponse(request as! DataRequest, notificationName:NOTIFIACTION_GETFEEDS)
        APIManager.sharedInstance.arrRequests.append((self,request))
    }
    
    //users/postsliked)
    //MARK: - GetPostsLiked
    func netComGetPostsLiked(_ parameters: [String: AnyObject]?=nil) {
        
        var request : Request
        self.requestType = RequestType.request_GETPOSTSLIKED
        let requestURL = BASE_URL + "users/postsliked"
        printCustom("requestURL:\(requestURL)")
        printCustom("parameters:\(parameters)")

        request = Alamofire.request(requestURL, method: HTTPMethod.post, parameters: parameters, encoding: JSONEncoding.default, headers: nil)

//        request = Alamofire.request(.POST, requestURL, parameters: parameters, encoding:.JSON)
        handleResponse(request as! DataRequest, notificationName:NOTIFICATION_GETPOSTSLIKED)
        APIManager.sharedInstance.arrRequests.append((self,request))
    }
    
    
    //MARK: - Add Comment
    func netComAddComment(_ parameters: [String: AnyObject]?=nil) {
        
        var request : Request
        self.requestType = RequestType.request_ADDCOMMENTS
        let requestURL = BASE_URL + "home/addcomments"
        request = Alamofire.request(requestURL, method: HTTPMethod.post, parameters: parameters, encoding: JSONEncoding.default, headers: nil)

        
//        request = Alamofire.request(.POST, requestURL, parameters: parameters, encoding:.JSON)
        handleResponse(request as! DataRequest, notificationName:NOTIFIACTION_ADDCOMMENT)
        APIManager.sharedInstance.arrRequests.append((self,request))
    }
    
    //MARK: - Like
    func netComLikeMedia(_ parameters: [String: AnyObject]?=nil) {
        
        var request : Request
        self.requestType = RequestType.request_LIKEMEDIA
        let requestURL = BASE_URL + "home/likes"
        
        request = Alamofire.request(requestURL, method: HTTPMethod.post, parameters: parameters, encoding: JSONEncoding.default, headers: nil)

//        request = Alamofire.request(.POST, requestURL, parameters: parameters, encoding:.JSON)
        handleResponse(request as! DataRequest, notificationName:NOTIFIACTION_LIKE)
        APIManager.sharedInstance.arrRequests.append((self,request))
    }
    //MARK: - Like PeepText
    func netComLikePeepText(_ parameters: [String: AnyObject]?=nil) {
        
        var request : Request
        self.requestType = RequestType.request_LIKEPEEPTEXT
        let requestURL = BASE_URL + "home/peeplikes"
        
        request = Alamofire.request(requestURL, method: HTTPMethod.post, parameters: parameters, encoding: JSONEncoding.default, headers: nil)

//        request = Alamofire.request(.POST, requestURL, parameters: parameters, encoding:.JSON)
        handleResponse(request as! DataRequest, notificationName:NOTIFIACTION_LIKE_PEEPTEXT)
        APIManager.sharedInstance.arrRequests.append((self,request))
    }
    
    
    //MARK: - Dislike
    func netComDislikeMedia(_ parameters: [String: AnyObject]?=nil) {
        
        var request : Request
        self.requestType = RequestType.request_DISLIKEMEDIA
        let requestURL = BASE_URL + "home/likes"
        
        request = Alamofire.request(requestURL, method: HTTPMethod.post, parameters: parameters, encoding: JSONEncoding.default, headers: nil)

//        request = Alamofire.request(.POST, requestURL, parameters: parameters, encoding:.JSON)
        handleResponse(request as! DataRequest, notificationName:NOTIFIACTION_DISLIKE)
        APIManager.sharedInstance.arrRequests.append((self,request))
    }
    
    //MARK: - Dislike PeepText
    func netComDislikePeepText(_ parameters: [String: AnyObject]?=nil) {
        
        var request : Request
        self.requestType = RequestType.request_DISLIKEPEEPTEXT
        let requestURL = BASE_URL + "home/peeplikes"
        
        request = Alamofire.request(requestURL, method: HTTPMethod.post, parameters: parameters, encoding: JSONEncoding.default, headers: nil)

//        request = Alamofire.request(.POST, requestURL, parameters: parameters, encoding:.JSON)
        handleResponse(request as! DataRequest, notificationName:NOTIFIACTION_DISLIKE_PEEPTEXT)
        APIManager.sharedInstance.arrRequests.append((self,request))
    }
    

    
    //MARK: - GetComments
    func netComGetComments(_ parameters: [String: AnyObject]?=nil) {
        
        var request : Request
        self.requestType = RequestType.request_GETCOMMENTS
        let requestURL = BASE_URL + "home/getcomments"
        
        request = Alamofire.request(requestURL, method: HTTPMethod.post, parameters: parameters, encoding: JSONEncoding.default, headers: nil)

//        request = Alamofire.request(.POST, requestURL, parameters: parameters, encoding:.JSON)
        handleResponse(request as! DataRequest, notificationName:NOTIFIACTION_GETCOMMENTS)
        APIManager.sharedInstance.arrRequests.append((self,request))
    }
    
    //MARK: - GetPeople
    func netComGetPeople(_ parameters: [String: AnyObject]?=nil) {
        
        var request : Request
        self.requestType = RequestType.request_GETPEOPLE
        let requestURL = BASE_URL + "home/people"
        
        request = Alamofire.request(requestURL, method: HTTPMethod.post, parameters: parameters, encoding: JSONEncoding.default, headers: nil)

//        request = Alamofire.request(.POST, requestURL, parameters: parameters, encoding:.JSON)
        handleResponse(request as! DataRequest, notificationName:NOTIFIACTION_GETPEOPLE)
        APIManager.sharedInstance.arrRequests.append((self,request))
    }
    //MARK: - GetLIKERPeople
    func netComGetLikerPeople(_ parameters: [String: AnyObject]?=nil) {
        
        var request : Request
        self.requestType = RequestType.request_GETLIKERPEOPLE
        let requestURL = BASE_URL + "users/getlikers"
        
        request = Alamofire.request(requestURL, method: HTTPMethod.post, parameters: parameters, encoding: JSONEncoding.default, headers: nil)

//        request = Alamofire.request(.POST, requestURL, parameters: parameters, encoding:.JSON)
        handleResponse(request as! DataRequest, notificationName:NOTIFIACTION_GETLIKERPEOPLE)
        APIManager.sharedInstance.arrRequests.append((self,request))
    }
    //MARK: - GetDisLIKERPeople
    func netComGetDisLikerPeople(_ parameters: [String: AnyObject]?=nil) {
        
        var request : Request
        self.requestType = RequestType.request_GETDISLIKERPEOPLE
        let requestURL = BASE_URL + "users/getdislikers"
        
        request = Alamofire.request(requestURL, method: HTTPMethod.post, parameters: parameters, encoding: JSONEncoding.default, headers: nil)

//        request = Alamofire.request(.POST, requestURL, parameters: parameters, encoding:.JSON)
        handleResponse(request as! DataRequest, notificationName:NOTIFIACTION_GETDISLIKERPEOPLE)
        APIManager.sharedInstance.arrRequests.append((self,request))
    }
    //MARK: - SearchPeople
    func netComSearchPeople(_ parameters: [String: AnyObject]?=nil) {
        
        var request : Request
        self.requestType = RequestType.request_SEARCHPEOPLE
        let requestURL = BASE_URL + "home/people"
        
        request = Alamofire.request(requestURL, method: HTTPMethod.post, parameters: parameters, encoding: JSONEncoding.default, headers: nil)

        
//        request = Alamofire.request(.POST, requestURL, parameters: parameters, encoding:.JSON)
        handleResponse(request as! DataRequest, notificationName:NOTIFIACTION_SEARCHPEOPLE)
        APIManager.sharedInstance.arrRequests.append((self,request))
    }
    
    
    //MARK: - FollowPeople
    func netComFollowPeople(_ parameters: [String: AnyObject]?=nil) {
        
        var request : Request
        self.requestType = RequestType.request_SENDFOLLOWREQUEST
        let requestURL = BASE_URL + "home/friend"
        
        request = Alamofire.request(requestURL, method: HTTPMethod.post, parameters: parameters, encoding: JSONEncoding.default, headers: nil)

//        request = Alamofire.request(.POST, requestURL, parameters: parameters, encoding:.JSON)
        handleResponse(request as! DataRequest, notificationName:NOTIFIACTION_FOLLOWPEOPLE)
        APIManager.sharedInstance.arrRequests.append((self,request))
    }
    
    //MARK: - UnFollowPeople
    func netComUnFollowPeople(_ parameters: [String: AnyObject]?=nil) {
        
        var request : Request
        self.requestType = RequestType.request_SENDUNFOLLOWREQUEST
        let requestURL = BASE_URL + "home/unfollowuser"
        
        request = Alamofire.request(requestURL, method: HTTPMethod.post, parameters: parameters, encoding: JSONEncoding.default, headers: nil)

//        request = Alamofire.request(.POST, requestURL, parameters: parameters, encoding:.JSON)
        handleResponse(request as! DataRequest, notificationName:NOTIFIACTION_UNFOLLOWPEOPLE)
        APIManager.sharedInstance.arrRequests.append((self,request))
    }
    
    //MARK: - Upload Digital Media
    func netComUploadDigitalMedia(_ parameters: [String: AnyObject]?=nil) {
        
        var request : Request
        self.requestType = RequestType.request_UPLOADMEDIA
        let requestURL = BASE_URL + "home/uploadmedia"
        
        request = Alamofire.request(requestURL, method: HTTPMethod.post, parameters: parameters, encoding: JSONEncoding.default, headers: nil)

        
//        request = Alamofire.request(.POST, requestURL, parameters: parameters, encoding:.JSON)
        handleResponse(request as! DataRequest, notificationName:NOTIFIACTION_POSTUPLOADDIGITALMEDIA)
        APIManager.sharedInstance.arrRequests.append((self,request))
    }
    
    //MARK: - Send Peep Text
    func netComSendPeepText(_ parameters: [String: AnyObject]?=nil) {
        
        var request : Request
        self.requestType = RequestType.request_SENDPEEPTEXT
        let requestURL = BASE_URL + "home/sendpeeptext"
        printCustom("requestURL:\(requestURL)")
        printCustom("parameters:\(parameters)")
        
        request = Alamofire.request(requestURL, method: HTTPMethod.post, parameters: parameters, encoding: JSONEncoding.default, headers: nil)

//        request = Alamofire.request(.POST, requestURL, parameters: parameters, encoding:.JSON)
        handleResponse(request as! DataRequest, notificationName:NOTIFIACTION_POSTSENDPEEPTEXT)
        APIManager.sharedInstance.arrRequests.append((self,request))
    }
    
    
    //MARK: - Get Contacts
    func netComGetContacts(_ parameters: [String: AnyObject]?=nil) {
        
        var request : Request
        self.requestType = RequestType.request_GETCONTACTS
        let requestURL = BASE_URL + "home/getcontacts"
        
        
        request = Alamofire.request(requestURL, method: HTTPMethod.post, parameters: parameters, encoding: JSONEncoding.default, headers: nil)

//        request = Alamofire.request(.POST, requestURL, parameters: parameters, encoding:.JSON)
        handleResponse(request as! DataRequest, notificationName:NOTIFIACTION_GETCONTACTS)
        APIManager.sharedInstance.arrRequests.append((self,request))
    }
   
    //MARK: - Get Followers
    func netComGetFollowers(_ parameters: [String: AnyObject]?=nil) {
        
        var request : Request
        self.requestType = RequestType.request_GETFOLLOWERS
        let requestURL = BASE_URL + "home/getfollowers"
        
        request = Alamofire.request(requestURL, method: HTTPMethod.post, parameters: parameters, encoding: JSONEncoding.default, headers: nil)

//        request = Alamofire.request(.POST, requestURL, parameters: parameters, encoding:.JSON)
        handleResponse(request as! DataRequest, notificationName:NOTIFIACTION_GETFOLLOWERS)
        APIManager.sharedInstance.arrRequests.append((self,request))
    }
    
    //MARK: - Search Contacts
    func netComSearchContacts(_ parameters: [String: AnyObject]?=nil) {
        
        var request : Request
        self.requestType = RequestType.request_SEARCHCONTACTS
        let requestURL = BASE_URL + "home/getcontacts"
        
        request = Alamofire.request(requestURL, method: HTTPMethod.post, parameters: parameters, encoding: JSONEncoding.default, headers: nil)

//        request = Alamofire.request(.POST, requestURL, parameters: parameters, encoding:.JSON)
        handleResponse(request as! DataRequest, notificationName:NOTIFIACTION_SEARCHCONTACTS)
        APIManager.sharedInstance.arrRequests.append((self,request))
    }
    
    //MARK: - Report Media
    func netComReportMedia(_ parameters: [String: AnyObject]?=nil) {
        
        var request : Request
        self.requestType = RequestType.report_MEDIA
        let requestURL = BASE_URL + "home/report"
        
        request = Alamofire.request(requestURL, method: HTTPMethod.post, parameters: parameters, encoding: JSONEncoding.default, headers: nil)

//        request = Alamofire.request(.POST, requestURL, parameters: parameters, encoding:.JSON)
        handleResponse(request as! DataRequest, notificationName:NOTIFIACTION_REPORTMEDIA)
        APIManager.sharedInstance.arrRequests.append((self,request))
    }
    
    //MARK: - Get Public Movie Media
    func netComGetPublicMovieMedia(_ parameters: [String: AnyObject]?=nil) {
        
        var request : Request
        self.requestType = RequestType.request_GETPUBLICMOVIEMEDIA
        let requestURL = BASE_URL + "home/searchmedia"
        printCustom("parameters:\(String(describing: parameters))")

        request = Alamofire.request(requestURL, method: HTTPMethod.post, parameters: parameters, encoding: JSONEncoding.default, headers: nil)

//        request = Alamofire.request(.POST, requestURL, parameters: parameters, encoding:.JSON)
        handleResponse(request as! DataRequest, notificationName:NOTIFIACTION_GETPUBLICMOVIEMEDIA)
        APIManager.sharedInstance.arrRequests.append((self,request))
    }
    
    //MARK: - Get Public Music Media
    func netComGetPublicMusicMedia(_ parameters: [String: AnyObject]?=nil) {
        
        var request : Request
        self.requestType = RequestType.request_GETPUBLICMUSICMEDIA
        let requestURL = BASE_URL + "home/searchmedia"
        printCustom("parameters:\(String(describing: parameters))")

        request = Alamofire.request(requestURL, method: HTTPMethod.post, parameters: parameters, encoding: JSONEncoding.default, headers: nil)

//        request = Alamofire.request(.POST, requestURL, parameters: parameters, encoding:.JSON)
        handleResponse(request as! DataRequest, notificationName:NOTIFIACTION_GETPUBLICMUSICMEDIA)
        APIManager.sharedInstance.arrRequests.append((self,request))
    }
    
    //MARK: - Get Public Video Games Media
    func netComGetPublicVideoGamesMedia(_ parameters: [String: AnyObject]?=nil) {
        
        var request : Request
        self.requestType = RequestType.request_GETPUBLICVIDEOGAMESMEDIA
        let requestURL = BASE_URL + "home/searchmedia"
        printCustom("parameters:\(String(describing: parameters))")

        request = Alamofire.request(requestURL, method: HTTPMethod.post, parameters: parameters, encoding: JSONEncoding.default, headers: nil)

//        request = Alamofire.request(.POST, requestURL, parameters: parameters, encoding:.JSON)
        handleResponse(request as! DataRequest, notificationName:NOTIFIACTION_GETPUBLICVIDEOGAMESMEDIA)
        APIManager.sharedInstance.arrRequests.append((self,request))
    }
    
    //MARK: - Get Gallery Movies
    func netComGetGalleryMovies(_ parameters: [String: AnyObject]?=nil) {
        
        var request : Request
        self.requestType = RequestType.request_GETGALLERYMOVIES
        
        //TODO:- need to change API
        let requestURL = BASE_URL + "users/getgallery"
        printCustom("parameters:\(String(describing: parameters))")

        request = Alamofire.request(requestURL, method: HTTPMethod.post, parameters: parameters, encoding: JSONEncoding.default, headers: nil)

//        request = Alamofire.request(.POST, requestURL, parameters: parameters, encoding:.JSON)
        handleResponse(request as! DataRequest, notificationName:NOTIFIACTION_GETGALLERYMOVIES)
        APIManager.sharedInstance.arrRequests.append((self,request))
    }
    
    //MARK: - Get Gallery Music
    func netComGetGalleryMusic(_ parameters: [String: AnyObject]?=nil) {
        
        var request : Request
        self.requestType = RequestType.request_GETGALLERYMUSIC
        
        //TODO:- need to change API
        let requestURL = BASE_URL + "users/getgallery"
        
        request = Alamofire.request(requestURL, method: HTTPMethod.post, parameters: parameters, encoding: JSONEncoding.default, headers: nil)

//        request = Alamofire.request(.POST, requestURL, parameters: parameters, encoding:.JSON)
        handleResponse(request as! DataRequest, notificationName:NOTIFIACTION_GETGALLERYMUSIC)
        APIManager.sharedInstance.arrRequests.append((self,request))
    }
    
    //MARK: - Get Gallery Video Games
    func netComGetGalleryVideoGames(_ parameters: [String: AnyObject]?=nil) {
        
        var request : Request
        self.requestType = RequestType.request_GETGALLERYVIDEOGAMES
        
        //TODO:- need to change API
        let requestURL = BASE_URL + "users/getgallery"
        
        request = Alamofire.request(requestURL, method: HTTPMethod.post, parameters: parameters, encoding: JSONEncoding.default, headers: nil)

//        request = Alamofire.request(.POST, requestURL, parameters: parameters, encoding:.JSON)
        handleResponse(request as! DataRequest, notificationName:NOTIFIACTION_GETGALLERYVIDEOGAMES)
        APIManager.sharedInstance.arrRequests.append((self,request))
    }
    
    //MARK: - Get Gallery Photos
    func netComGetGalleryPhotos(_ parameters: [String: AnyObject]?=nil) {
        
        var request : Request
        self.requestType = RequestType.request_GETGALLERYPHOTOS
        
        //TODO:- need to change API
        let requestURL = BASE_URL + "users/getgallery"
        
        request = Alamofire.request(requestURL, method: HTTPMethod.post, parameters: parameters, encoding: JSONEncoding.default, headers: nil)

//        request = Alamofire.request(.POST, requestURL, parameters: parameters, encoding:.JSON)
        handleResponse(request as! DataRequest, notificationName:NOTIFIACTION_GETGALLERYPHOTOS)
        APIManager.sharedInstance.arrRequests.append((self,request))
    }
    
    //MARK: - Get Gallery PeepText
    func netComGetGalleryPeepText(_ parameters: [String: AnyObject]?=nil) {
        
        var request : Request
        self.requestType = RequestType.request_GETGALLERYPEEPTEXT
        
        //TODO:- need to change API
        let requestURL = BASE_URL + "users/getgallery"
        printCustom("requestURL:\(String(describing: requestURL))")

        request = Alamofire.request(requestURL, method: HTTPMethod.post, parameters: parameters, encoding: JSONEncoding.default, headers: nil)
        printCustom("parameters:\(String(describing: parameters))")

//        request = Alamofire.request(.POST, requestURL, parameters: parameters, encoding:.JSON)
        handleResponse(request as! DataRequest, notificationName:NOTIFIACTION_GETGALLERYPEEPTEXT)
        APIManager.sharedInstance.arrRequests.append((self,request))
    }
    
    //MARK: - Get Gallery PeepText
    func netComGetPeepTextConversation(_ parameters: [String: AnyObject]?=nil) {
        
        var request : Request
        self.requestType = RequestType.request_GETPEEPTEXTCONVERSATION
        
        //TODO:- need to change API
        let requestURL = BASE_URL + "users/getpeeptextconversationdetails"
        printCustom("requestURL:\(String(describing: requestURL))")

        request = Alamofire.request(requestURL, method: HTTPMethod.post, parameters: parameters, encoding: JSONEncoding.default, headers: nil)
        printCustom("parameters:\(String(describing: parameters))")

//        request = Alamofire.request(.POST, requestURL, parameters: parameters, encoding:.JSON)
        handleResponse(request as! DataRequest, notificationName:NOTIFIACTION_GETPEEPTEXTCONVERSATION)
        APIManager.sharedInstance.arrRequests.append((self,request))
    }
    
    
    //MARK: - Search Public Media
    func netComSearchPublicMedia(_ parameters: [String: AnyObject]?=nil) {
        
        var request : Request
        self.requestType = RequestType.request_SEARCHPUBLICMEDIA
        let requestURL = BASE_URL + "home/searchmedia"
        
        request = Alamofire.request(requestURL, method: HTTPMethod.post, parameters: parameters, encoding: JSONEncoding.default, headers: nil)

//        request = Alamofire.request(.POST, requestURL, parameters: parameters, encoding:.JSON)
        handleResponse(request as! DataRequest, notificationName:NOTIFIACTION_SEARCHPUBLICMEDIA)
        APIManager.sharedInstance.arrRequests.append((self,request))
    }
    
    //MARK: - Fetch Event Categories
    func netComFetchEventCategories(_ parameters: [String: AnyObject]?=nil) {
        var request : Request
        self.requestType = RequestType.request_FETCHEVENTCATEGORIES
        let requestURL = BASE_URL + "users/geteventcategories"
        
        request = Alamofire.request(requestURL, method: HTTPMethod.get, parameters: parameters, encoding: JSONEncoding.default, headers: nil)

//        request = Alamofire.request(.GET, requestURL, parameters: parameters, encoding:.JSON)
        handleResponse(request as! DataRequest, notificationName:NOTIFICATION_FETCHEVENTCATEGORIES)
        APIManager.sharedInstance.arrRequests.append((self,request))
    }
    
    //MARK: - Send Event
    func netComSendEvent(_ parameters: [String: AnyObject]?=nil) {
        var request : Request
        self.requestType = RequestType.request_SENDEVENT
        let requestURL = BASE_URL + "users/createevent"
        printCustom("requestURL:\(requestURL)")
        printCustom("parameters:\(parameters)")
        
        request = Alamofire.request(requestURL, method: HTTPMethod.post, parameters: parameters, encoding: JSONEncoding.default, headers: nil)

//        request = Alamofire.request(.POST, requestURL, parameters: parameters, encoding:.JSON)
        handleResponse(request as! DataRequest, notificationName:NOTIFICATION_SENDEVENT)
        APIManager.sharedInstance.arrRequests.append((self,request))
    }
    
    //MARK: - Fetch Events
    func netComFetchEvents(_ parameters: [String: AnyObject]?=nil) {
        var request : Request
        self.requestType = RequestType.request_FETCHEVENTS
        let requestURL = BASE_URL + "users/getevents"
        printCustom("requestURL:\(requestURL)")
        printCustom("parameters:\(parameters)")
        
        request = Alamofire.request(requestURL, method: HTTPMethod.post, parameters: parameters, encoding: JSONEncoding.default, headers: nil)

//        request = Alamofire.request(.POST, requestURL, parameters: parameters, encoding:.JSON)
        
        if parameters!["type"] as! Int == 1 {
            handleResponse(request as! DataRequest, notificationName:NOTIFICATION_FETCHMYEVENTS)
        }
        else {
            handleResponse(request as! DataRequest, notificationName:NOTIFICATION_FETCHALLEVENTS)
        }
        
        APIManager.sharedInstance.arrRequests.append((self,request))
    }
    
    //MARK: - Delete Event
    func netComDeleteEvent(_ parameters: [String: AnyObject]?=nil) {
        var request : Request
        self.requestType = RequestType.request_DELETEEVENT
        let requestURL = BASE_URL + "users/deleteevent"
        printCustom("requestURL:\(requestURL)")
        printCustom("parameters:\(parameters)")
        
        request = Alamofire.request(requestURL, method: HTTPMethod.post, parameters: parameters, encoding: JSONEncoding.default, headers: nil)

//        request = Alamofire.request(.POST, requestURL, parameters: parameters, encoding:.JSON)
        handleResponse(request as! DataRequest, notificationName:NOTIFICATION_DELETEEVENT)
        APIManager.sharedInstance.arrRequests.append((self,request))
    }
    
    
    //MARK: - Delete Event Invite
    func netComDeleteEventInvite(_ parameters: [String: AnyObject]?=nil) {
        var request : Request
        self.requestType = RequestType.request_DELETEEVENTINVITE
        let requestURL = BASE_URL + "users/deleteeventinvite"
        printCustom("requestURL:\(requestURL)")
        printCustom("parameters:\(parameters)")
        
        request = Alamofire.request(requestURL, method: HTTPMethod.post, parameters: parameters, encoding: JSONEncoding.default, headers: nil)

       // request = Alamofire.request(.POST, requestURL, parameters: parameters, encoding:.JSON)
        handleResponse(request as! DataRequest, notificationName:NOTIFICATION_DELETEEVENTINVITE)
        APIManager.sharedInstance.arrRequests.append((self,request))
    }

    
    //MARK: - Edit Event
    func netComEditEvent(_ parameters: [String: AnyObject]?=nil) {
        var request : Request
        self.requestType = RequestType.request_EDITEVENT
        let requestURL = BASE_URL + "users/editevent"
        printCustom("requestURL:\(requestURL)")
        printCustom("parameters:\(parameters)")
        
        request = Alamofire.request(requestURL, method: HTTPMethod.post, parameters: parameters, encoding: JSONEncoding.default, headers: nil)

//        request = Alamofire.request(.POST, requestURL, parameters: parameters, encoding:.JSON)
        handleResponse(request as! DataRequest, notificationName:NOTIFICATION_EDITEVENT)
        APIManager.sharedInstance.arrRequests.append((self,request))
    }
   
    //MARK: - Fetch Not Registered PecX Users
    func netComFetchNotRegisteredPecXUsers(_ parameters: [String: AnyObject]?=nil) {
        var request : Request
        self.requestType = RequestType.request_FETCHNOTREGISTEREDPECXUSERS
        var apiUrl = ""
        if parameters!.keys.contains("twitter_ids") {
            apiUrl = "users/synctwitter"
        }
        else {
            apiUrl = "users/syncphone"
        }
        let requestURL = BASE_URL + apiUrl
        printCustom("requestURL:\(requestURL)")
        printCustom("parameters:\(parameters)")
        
        request = Alamofire.request(requestURL, method: HTTPMethod.post, parameters: parameters, encoding: JSONEncoding.default, headers: nil)

//        request = Alamofire.request(.POST, requestURL, parameters: parameters, encoding:.JSON)
        
        handleResponse(request as! DataRequest, notificationName:NOTIFICATION_FETCHNOTREGISTEREDPECXUSERS)
            
        APIManager.sharedInstance.arrRequests.append((self,request))
    }
    
    //MARK: - Fetch Not Following PecX Users
    func netComFetchNotFollowingPecXUsers(_ parameters: [String: AnyObject]?=nil) {
        var request : Request
        self.requestType = RequestType.request_FETCHNOTFOLLOWINGPECXUSERS
        let requestURL = BASE_URL + "users/checknotfollowingpecx"
        printCustom("requestURL:\(requestURL)")
        printCustom("parameters:\(parameters)")
        
        request = Alamofire.request(requestURL, method: HTTPMethod.post, parameters: parameters, encoding: JSONEncoding.default, headers: nil)

//        request = Alamofire.request(.POST, requestURL, parameters: parameters, encoding:.JSON)
        
        handleResponse(request as! DataRequest, notificationName:NOTIFICATION_FETCHNOTFOLLOWINGPECXUSERS)
        
        APIManager.sharedInstance.arrRequests.append((self,request))
    }
    
    //MARK: - Get Notification List
    func netComGetNotificationList(_ parameters: [String: AnyObject]?=nil) {
        var request : Request
        self.requestType = RequestType.request_GETNOTIFICATIONLIST
        let requestURL = BASE_URL + "users/getnotificationslist"
        printCustom("requestURL:\(requestURL)")
        printCustom("parameters:\(parameters)")
        
        request = Alamofire.request(requestURL, method: HTTPMethod.post, parameters: parameters, encoding: JSONEncoding.default, headers: nil)

//        request = Alamofire.request(.POST, requestURL, parameters: parameters, encoding:.JSON)
        handleResponse(request as! DataRequest, notificationName:NOTIFICATION_GETNOTIFICATIONLIST)
        APIManager.sharedInstance.arrRequests.append((self,request))
    }
    
    //MARK: - Unread Notification Count
    func netComGetUnreadNotificationCount(_ parameters: [String: AnyObject]?=nil) {
        var request : Request
        self.requestType = RequestType.request_GETUNREADNOTIFICATIONCOUNT
        let requestURL = BASE_URL + "users/unreadcount"
        printCustom("requestURL:\(requestURL)")
        printCustom("parameters:\(parameters)")
        
        request = Alamofire.request(requestURL, method: HTTPMethod.post, parameters: parameters, encoding: JSONEncoding.default, headers: nil)

//        request = Alamofire.request(.POST, requestURL, parameters: parameters, encoding:.JSON)
        handleResponse(request as! DataRequest, notificationName:NOTIFICATION_GETUNREADNOTIFICATIONCOUNT)
        APIManager.sharedInstance.arrRequests.append((self,request))
    }
    
    //MARK: - Read Notification
    func netComReadNotification(_ parameters: [String: AnyObject]?=nil) {
        var request : Request
        self.requestType = RequestType.request_SETREADNOTIFICATION
        let requestURL = BASE_URL + "users/setread"
        printCustom("requestURL:\(requestURL)")
        printCustom("parameters:\(parameters)")
        
        request = Alamofire.request(requestURL, method: HTTPMethod.post, parameters: parameters, encoding: JSONEncoding.default, headers: nil)

//        request = Alamofire.request(.POST, requestURL, parameters: parameters, encoding:.JSON)
        handleResponse(request as! DataRequest, notificationName:NOTIFICATION_SETREADNOTIFICATION)
        APIManager.sharedInstance.arrRequests.append((self,request))
    }
    
    //MARK: - Share Media
    func netComShareMedia(_ parameters: [String: AnyObject]?=nil) {
        var request : Request
        self.requestType = RequestType.request_SHAREMEDIA
        let requestURL = BASE_URL + "users/sharedmedia"
        printCustom("requestURL:\(requestURL)")
        printCustom("parameters:\(parameters)")
        
        request = Alamofire.request(requestURL, method: HTTPMethod.post, parameters: parameters, encoding: JSONEncoding.default, headers: nil)

//        request = Alamofire.request(.POST, requestURL, parameters: parameters, encoding:.JSON)
        handleResponse(request as! DataRequest, notificationName:NOTIFICATION_SHAREMEDIA)
        APIManager.sharedInstance.arrRequests.append((self,request))
    }
    
    //MARK: - Set feedback
    func netComSetFeedback(_ parameters: [String: AnyObject]?=nil) {
        var request : Request
        self.requestType = RequestType.request_SETFEEDBACK
        let requestURL = BASE_URL + "users/setfeedback"
        printCustom("requestURL:\(requestURL)")
        printCustom("parameters:\(parameters)")
        
        request = Alamofire.request(requestURL, method: HTTPMethod.post, parameters: parameters, encoding: JSONEncoding.default, headers: nil)

//        request = Alamofire.request(.POST, requestURL, parameters: parameters, encoding:.JSON)
        handleResponse(request as! DataRequest, notificationName:NOTIFICATION_SETFEEDBACK)
        APIManager.sharedInstance.arrRequests.append((self,request))
    }
    
    
    //MARK: - Approve disapprove follow request
    func netComApproveDisapproveFollowRequest(_ parameters: [String: AnyObject]?=nil) {
        var request : Request
        self.requestType = RequestType.request_APPROVEDISAPPROVEFOLLOWREQUEST
        let requestURL = BASE_URL + "home/addfriend"
        printCustom("requestURL:\(requestURL)")
        printCustom("parameters:\(parameters)")
        
        request = Alamofire.request(requestURL, method: HTTPMethod.post, parameters: parameters, encoding: JSONEncoding.default, headers: nil)

//        request = Alamofire.request(.POST, requestURL, parameters: parameters, encoding:.JSON)
        handleResponse(request as! DataRequest, notificationName:NOTIFIACTION_APPROVEDISAPPROVEFOLLOW)
        APIManager.sharedInstance.arrRequests.append((self,request))
    }
    
    //MARK: - Peep Code verification request
    func netComPeepCodeVerificationRequest(_ parameters: [String: AnyObject]?=nil) {
        var request : Request
        self.requestType = RequestType.request_PEEPCODEVERIFY
        let requestURL = BASE_URL + "users/verifypeepcode"
        printCustom("requestURL:\(requestURL)")
        printCustom("parameters:\(parameters)")
        
        request = Alamofire.request(requestURL, method: HTTPMethod.post, parameters: parameters, encoding: JSONEncoding.default, headers: nil)

//        request = Alamofire.request(.POST, requestURL, parameters: parameters, encoding:.JSON)
        handleResponse(request as! DataRequest, notificationName:NOTIFIACTION_PEEPCODEVERIFICATION)
        APIManager.sharedInstance.arrRequests.append((self,request))
    }
    
    //MARK: - Send Invite To Contacts
    func netComSendInviteToContacts(_ parameters: [String: AnyObject]?=nil) {
        var request : Request
        self.requestType = RequestType.request_SENDINVITETOCONTACTS
        let requestURL = BASE_URL + "users/sendinvites"
        printCustom("requestURL:\(requestURL)")
        printCustom("parameters:\(parameters)")
        
        
        request = (self.alamofireManager?.request(requestURL, method: HTTPMethod.post, parameters: parameters, encoding: JSONEncoding.default, headers: nil))!
       // request = self.alamofireManager!.request(.POST, requestURL, parameters: parameters, encoding:.JSON)
        handleResponse(request as! DataRequest, notificationName:NOTIFICATION_SENDINVITETOCONTACTS)
        APIManager.sharedInstance.arrRequests.append((self,request))
    }
    
    //MARK: - Accept Event Invitation
    func netComAcceptEventInvitation(_ parameters: [String: AnyObject]?=nil) {
        var request : Request
        self.requestType = RequestType.request_ACCEPTEVENTINVITATION
        let requestURL = BASE_URL + "users/acceptevent"
        printCustom("requestURL:\(requestURL)")
        printCustom("parameters:\(parameters)")
        
        request = Alamofire.request(requestURL, method: HTTPMethod.post, parameters: parameters, encoding: JSONEncoding.default, headers: nil)

//        request = Alamofire.request(.POST, requestURL, parameters: parameters, encoding:.JSON)
        handleResponse(request as! DataRequest, notificationName:NOTIFICATION_ACCEPTEVENTINVITATION)
        APIManager.sharedInstance.arrRequests.append((self,request))
    }
    
    //MARK: - Reject Event Invitation
    func netComRejectEventInvitation(_ parameters: [String: AnyObject]?=nil) {
        var request : Request
        self.requestType = RequestType.request_REJECTEVENTINVITATION
        let requestURL = BASE_URL + "users/rejectevent"
        printCustom("requestURL:\(requestURL)")
        printCustom("parameters:\(parameters)")
        
        request = Alamofire.request(requestURL, method: HTTPMethod.post, parameters: parameters, encoding: JSONEncoding.default, headers: nil)

//        request = Alamofire.request(.POST, requestURL, parameters: parameters, encoding:.JSON)
        handleResponse(request as! DataRequest, notificationName:NOTIFICATION_REJECTEVENTINVITATION)
        APIManager.sharedInstance.arrRequests.append((self,request))
    }
    
    //MARK: - Fetch Countries
    func netComFetchCountries(_ parameters: [String: AnyObject]?=nil) {
        var request : Request
        self.requestType = RequestType.request_FETCHCOUNTRIES
        let requestURL = BASE_URL + "users/countries"
        
        request = Alamofire.request(requestURL, method: HTTPMethod.get, parameters: parameters, encoding: JSONEncoding.default, headers: nil)

//        request = Alamofire.request(.GET, requestURL, parameters: parameters, encoding:.JSON)
        handleResponse(request as! DataRequest, notificationName:NOTIFICATION_FETCHCOUNTRIES)
        APIManager.sharedInstance.arrRequests.append((self,request))
    }
    
    //MARK: - Fetch Regions
    func netComFetchRegions(_ parameters: [String: AnyObject]?=nil) {
        var request : Request
        self.requestType = RequestType.request_FETCHREGIONS
        let requestURL = BASE_URL + "users/regions"
        printCustom("requestURL:\(requestURL)")
        printCustom("parameters:\(parameters)")
        
        request = Alamofire.request(requestURL, method: HTTPMethod.post, parameters: parameters, encoding: JSONEncoding.default, headers: nil)

//        request = Alamofire.request(.POST, requestURL, parameters: parameters, encoding:.JSON)
        handleResponse(request as! DataRequest, notificationName:NOTIFICATION_FETCHREGIONS)
        APIManager.sharedInstance.arrRequests.append((self,request))
    }
    
    /*//MARK: - Fetch Localities
    func netComFetchLocalities(parameters: [String: AnyObject]?=nil) {
        var request : Request
        self.requestType = RequestType.REQUEST_FETCHLOCALITIES
        let requestURL = BASE_URL + "users/locality"
        printCustom("requestURL:\(requestURL)")
        printCustom("parameters:\(parameters)")
        request = Alamofire.request(.POST, requestURL, parameters: parameters, encoding:.JSON)
        handleResponse(request as! DataRequest, notificationName:NOTIFICATION_FETCHLOCALITIES)
        APIManager.sharedInstance.arrRequests.append((self,request))
    }*/
    
    
    //MARK: - Check Submerchant account exists
    func netComCheckSubmerchantAccountExists(_ parameters: [String: AnyObject]?=nil) {
        var request : Request
        self.requestType = RequestType.request_CHECKSUBMERCHANTACCOUNTEXISTS
        let requestURL = BASE_URL + "users/submerchantexists"
        printCustom("requestURL:\(requestURL)")
        printCustom("parameters:\(parameters)")
        
        request = Alamofire.request(requestURL, method: HTTPMethod.post, parameters: parameters, encoding: JSONEncoding.default, headers: nil)

//        request = Alamofire.request(.POST, requestURL, parameters: parameters, encoding:.JSON)
        handleResponse(request as! DataRequest, notificationName:NOTIFICATION_CHECKSUBMERCHANTACCOUNTEXISTS)
        APIManager.sharedInstance.arrRequests.append((self,request))
    }
    
    //MARK: - Create Submerchant Account
    func netComCreateSubmerchantAccount(_ parameters: [String: AnyObject]?=nil) {
        var request : Request
        self.requestType = RequestType.request_CREATESUBMERCHANTACCOUNT
        let requestURL = BASE_URL_LOCAL + "users/createsubmerchant"
        printCustom("requestURL:\(requestURL)")
        printCustom("parameters:\(parameters)")
        
        request = Alamofire.request(requestURL, method: HTTPMethod.post, parameters: parameters, encoding: JSONEncoding.default, headers: nil)

//        request = Alamofire.request(.POST, requestURL, parameters: parameters, encoding:.JSON)
        handleResponse(request as! DataRequest, notificationName:NOTIFICATION_CREATESUBMERCHANTACCOUNT)
        APIManager.sharedInstance.arrRequests.append((self,request))
    }
    
    //MARK: - Create Submerchant Account
    func netComUpdateSubmerchantAccount(_ parameters: [String: AnyObject]?=nil) {
        var request : Request
        self.requestType = RequestType.request_UPDATESUBMERCHANTACCOUNT
        let requestURL = BASE_URL_LOCAL + "users/updatesubmerchant"
        printCustom("requestURL:\(requestURL)")
        printCustom("parameters:\(parameters)")
        
        request = Alamofire.request(requestURL, method: HTTPMethod.post, parameters: parameters, encoding: JSONEncoding.default, headers: nil)

//        request = Alamofire.request(.POST, requestURL, parameters: parameters, encoding:.JSON)
        handleResponse(request as! DataRequest, notificationName:NOTIFICATION_CREATESUBMERCHANTACCOUNT)
        APIManager.sharedInstance.arrRequests.append((self,request))
    }
    
    //MARK: - Create Braintree Token
    func netComCreateCreateBraintreeToken(_ parameters: [String: AnyObject]?=nil) {
        var request : Request
        self.requestType = RequestType.request_CREATEBRAINTREETOKEN
        let requestURL = BASE_URL_LOCAL + "users/createtoken"
        printCustom("requestURL:\(requestURL)")
        printCustom("parameters:\(parameters)")
        
        request = Alamofire.request(requestURL, method: HTTPMethod.get, parameters: parameters, encoding: JSONEncoding.default, headers: nil)

//        request = Alamofire.request(.GET, requestURL, parameters: parameters, encoding:.JSON)
        handleResponse(request as! DataRequest, notificationName:NOTIFICATION_CREATEBRAINTREETOKEN)
        APIManager.sharedInstance.arrRequests.append((self,request))
    }
    
    //MARK: - Process Braintree Payment
    func netComProcessBraintreePayment(_ parameters: [String: AnyObject]?=nil) {
        var request : Request
        self.requestType = RequestType.request_PROCESSBRAINTREEPAYMENT
        let requestURL = BASE_URL_LOCAL + "users/paymentprocess"
        printCustom("requestURL:\(requestURL)")
        printCustom("parameters:\(parameters)")
        
        request = Alamofire.request(requestURL, method: HTTPMethod.post, parameters: parameters, encoding: JSONEncoding.default, headers: nil)

//        request = Alamofire.request(.POST, requestURL, parameters: parameters, encoding:.JSON)
        handleResponse(request as! DataRequest, notificationName:NOTIFICATION_PROCESSBRAINTREEPAYMENT)
        APIManager.sharedInstance.arrRequests.append((self,request))
    }
    
    //MARK: - Get Saved Cards
    func netComGetSavedCards(_ parameters: [String: AnyObject]?=nil) {
        var request : Request
        self.requestType = RequestType.request_GETSAVEDCARDS
        let requestURL = BASE_URL_LOCAL + "users/getsavedcards"
        printCustom("requestURL:\(requestURL)")
        printCustom("parameters:\(parameters)")
        
        request = Alamofire.request(requestURL, method: HTTPMethod.post, parameters: parameters, encoding: JSONEncoding.default, headers: nil)

//        request = Alamofire.request(.POST, requestURL, parameters: parameters, encoding:.JSON)
        handleResponse(request as! DataRequest, notificationName:NOTIFICATION_GETSAVEDCARDS)
        APIManager.sharedInstance.arrRequests.append((self,request))
    }
    
    //MARK: - Get Following People
    func netComGetFollowingPeople(_ parameters: [String: AnyObject]?=nil) {
        
        var request : Request
        self.requestType = RequestType.request_GETFOLLOWINGPEOPLE
        let requestURL = BASE_URL + "users/getfollowingslistview"
        
        request = Alamofire.request(requestURL, method: HTTPMethod.post, parameters: parameters, encoding: JSONEncoding.default, headers: nil)

//        request = Alamofire.request(.POST, requestURL, parameters: parameters, encoding:.JSON)
        handleResponse(request as! DataRequest, notificationName:NOTIFIACTION_GETFOLLOWINGPEOPLE)
        APIManager.sharedInstance.arrRequests.append((self,request))
    }
    
    //MARK: - Get Follower People
    func netComGetFollowerPeople(_ parameters: [String: AnyObject]?=nil) {
        
        var request : Request
        self.requestType = RequestType.request_GETFOLLOWERPEOPLE
        let requestURL = BASE_URL + "users/getfollowerslistview"
        
        request = Alamofire.request(requestURL, method: HTTPMethod.post, parameters: parameters, encoding: JSONEncoding.default, headers: nil)

//        request = Alamofire.request(.POST, requestURL, parameters: parameters, encoding:.JSON)
        handleResponse(request as! DataRequest, notificationName:NOTIFIACTION_GETFOLLOWERPEOPLE)
        APIManager.sharedInstance.arrRequests.append((self,request))
    }
    
    //MARK: - Get User Posts
    func netComGetUserPosts(_ parameters: [String: AnyObject]?=nil) {
        
        var request : Request
        self.requestType = RequestType.request_GETUSERPOSTS
        let requestURL = BASE_URL + "users/postsotheruser"
        
        request = Alamofire.request(requestURL, method: HTTPMethod.post, parameters: parameters, encoding: JSONEncoding.default, headers: nil)

//        request = Alamofire.request(.POST, requestURL, parameters: parameters, encoding:.JSON)
        handleResponse(request as! DataRequest, notificationName:NOTIFIACTION_GETUSERPOSTS)
        APIManager.sharedInstance.arrRequests.append((self,request))
    }
    
    //MARK: - Forward Media
    func netComForwardMedia(_ parameters: [String: AnyObject]?=nil) {
        
        var request : Request
        self.requestType = RequestType.request_GETUSERPOSTS
        let requestURL = BASE_URL + "home/forwardmedia"
        
        request = Alamofire.request(requestURL, method: HTTPMethod.post, parameters: parameters, encoding: JSONEncoding.default, headers: nil)

//        request = Alamofire.request(.POST, requestURL, parameters: parameters, encoding:.JSON)
        handleResponse(request as! DataRequest, notificationName:NOTIFIACTION_FORWARDMEDIA)
        APIManager.sharedInstance.arrRequests.append((self,request))
    }
    
    
    //MARK: - GetTop 5 users
    func netComGetTopFiveUsers(_ parameters: [String: AnyObject]?=nil) {
        
        var request : Request
        self.requestType = RequestType.request_GETTOPFIVEUSERS
        let requestURL = BASE_URL + "users/gettopfiveusers"
        
        request = Alamofire.request(requestURL, method: HTTPMethod.post, parameters: parameters, encoding: JSONEncoding.default, headers: nil)

//        request = Alamofire.request(.POST, requestURL, parameters: parameters, encoding:.JSON)
        handleResponse(request as! DataRequest, notificationName:NOTIFIACTION_GETTOPFIVEUSERS)
        APIManager.sharedInstance.arrRequests.append((self,request))
    }

    
    //MARK: - Handle Response
    func handleResponse(_ request : DataRequest, notificationName:String){
        
//        printCustom("requestURL:\(request.request?.URLString)")
//        printCustom("requestURL:\(request.request?.URLRequest)")
        
       
        request.responseJSON{
            response in
            
            var index = 0
            
            for (netcom,_) in APIManager.sharedInstance.arrRequests{
                
                if netcom.requestType == self.requestType{
                    
                    APIManager.sharedInstance.arrRequests.remove(at: index)
                    
                    printCustom("remove request... \(self.requestType)")
                    
                    break
                }
                
                index += 1
            }
            
            switch response.result
            {
                
             case .success(let JSON):
                printCustom("\(self.requestType): \(JSON)")
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificationName), object: JSON)
                break
                
            case .failure(let error):
                printCustom("Request failed with error: \(error)")
                var errorMsg = ""
                if error.code == -1009 {
                    
                    errorMsg = alertMsg_NoInternetConnection
                    
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificationName), object: [ERROR_KEY: errorMsg])

                }
                else if error.code == -999 {
                    printCustom("Intentionally cancelled request")
                    //NSNotificationCenter.defaultCenter().postNotificationName(notificationName, object: [ERROR_KEY: "Intentionally cancelled request"])

                    //Intentionally cancelled request, don't send notification
                }
                else
                {
                    errorMsg = alertMsg_SomethingWentWrong
                    
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificationName), object: [ERROR_KEY: errorMsg])

                }
                
                break
            }
        }
    }
 }
 
 extension Error {
    var code: Int { return (self as NSError).code }
    var domain: String { return (self as NSError).domain }
 }
