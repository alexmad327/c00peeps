//
//  APIManager.swift
//  C00Peeps
//
//  Created by OSX on 06/13/16.
//  Copyright © 2016 SOTSYS011. All rights reserved.
//

import Foundation
import Alamofire


class APIManager {
    
    private static var __once: () = {
        let sharedInstance = APIManager()

           // Static.instance = APIManager()
        }()
    
    fileprivate var alamoFireManager : Alamofire.SessionManager!
    
    var arrRequests = [(Netcom,Request)]()
    
    static let sharedInstance = APIManager()
    
    //MARK: - Conig
    init()
    {
        setAFconfig()
    }
    
    //MARK: - Config
    func setAFconfig(){
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForResource = 4
        configuration.timeoutIntervalForRequest = 4
        alamoFireManager = Alamofire.SessionManager(configuration: configuration)
    }
    
    //MARK: - Cancel All Requests
    func cancelAllRequests() {
        
        for (_,request) in arrRequests{
            
                request.cancel()
        }
        
        //alamoFireManager.session.invalidateAndCancel()
        arrRequests.removeAll()
        setAFconfig()
    }
    
    //MARK: - Cancel Request
    func cancelRequest(_ requestType: RequestType) {
        
        var i = 0
        
        for (netcom,request) in arrRequests{
            
            if netcom.requestType == requestType{
                
                printCustom("request cancelled...\(requestType)")
                
                request.cancel()
                
                arrRequests.remove(at: i)
                
                break
            }
            
            i += 1
        }
        
        setAFconfig()
    }
    
    //MARK: - Fetch SignUp Form Related Data
    func formDataSignUpPage (_ parameters: [String: AnyObject]?=nil, Target:AnyObject) {
        
        NotificationCenter.default.addObserver(Target, selector: #selector (SignUpVc_Form.responseFetchFields), name:NSNotification.Name(rawValue: NOTIFICATION_USERTYPE), object: nil)
        
        let netcom = Netcom()
        netcom.netComFormDataSignUpPage(parameters)
    }
    
    //MARK: - Login Request
    func requestLogin(_ parameters: [String: AnyObject]?=nil, Target:AnyObject) {
        
        NotificationCenter.default.addObserver(Target, selector: #selector (BaseVC.responseSignIn), name: NSNotification.Name(rawValue: NOTIFIACTION_LOGIN), object: nil)
        
        let netcom = Netcom()
        netcom.netComLogin(parameters)
    }
    
    //MARK: - Check Social User Exists Request
    func requestCheckSocialUserExists(_ parameters: [String: AnyObject]?=nil, Target:AnyObject) {
        
        NotificationCenter.default.addObserver(Target, selector: #selector (BaseVC.responseCheckSocialUserExists), name: NSNotification.Name(rawValue: NOTIFICATION_CHECKSOCIALUSEREXISTS), object: nil)
        
        let netcom = Netcom()
        netcom.netComCheckSocialUserExists(parameters)
    }
    
    //MARK: - Signup Request
    func requestSignup(_ parameters: [String: AnyObject]?=nil, Target:AnyObject) {
        
         NotificationCenter.default.addObserver(Target, selector: #selector (SignUpVc_Form.responseCallAPI_SignUp), name: NSNotification.Name(rawValue: NOTIFIACTION_SIGNUP), object: nil)
        
        let netcom = Netcom()
        netcom.netComSignUp(parameters)
    }
    
    //MARK: - Forgot Password
    func requestForgotPassword(_ parameters: [String: AnyObject]?=nil, Target:AnyObject) {
        
        NotificationCenter.default.addObserver(Target, selector: #selector (ForgotPasswordVC.responseForgotPassword), name: NSNotification.Name(rawValue: NOTIFIACTION_FORGOTPASSWORD), object: nil)
        
        let netcom = Netcom()
        
        netcom.netComForgotPassword(parameters)
    }
    
    
    //MARK: - Change Password
    func requestChangePassword(_ parameters: [String: AnyObject]?=nil, Target:AnyObject) {
        
        NotificationCenter.default.addObserver(Target, selector: #selector (ChangePasscodeViewController.responseChangePassword), name: NSNotification.Name(rawValue: NOTIFIACTION_CHANGEPASSWORD), object: nil)
        
        let netcom = Netcom()
        
        netcom.netComChangePassword(parameters)
    }
    
    //MARK: - Edit Profile
    func requestEditProfile(_ parameters: [String: AnyObject]?=nil, Target:AnyObject) {
        
        NotificationCenter.default.addObserver(Target, selector: #selector (SignUpVc_Form.responseEditProfile), name: NSNotification.Name(rawValue: NOTIFIACTION_EDITPROFILE), object: nil)
        
        let netcom = Netcom()
        
        netcom.netComEditProfile(parameters)
    }
    
    //MARK: - Get User Details
    func requestGetUserDetails(_ parameters: [String: AnyObject]?=nil, Target:AnyObject) {
        
        NotificationCenter.default.addObserver(Target, selector: #selector (GalleryMainViewController.responseGetUserDetails), name: NSNotification.Name(rawValue: NOTIFIACTION_GETUSERDETAILS), object: nil)
        
        let netcom = Netcom()
        
        netcom.netComGetUserDetails(parameters)
    }
    
    //MARK: - Save Account Type
    func requestSaveAccountType(_ parameters: [String: AnyObject]?=nil, Target:AnyObject) {
        
        NotificationCenter.default.addObserver(Target, selector: #selector (AccountAuthenticationVc.responseAccountType), name: NSNotification.Name(rawValue: NOTIFIACTION_SETACCOUNTTYPE), object: nil)
        
        let netcom = Netcom()
        
        netcom.netComSaveAccountType(parameters)
    }
    
    //MARK: - Save Theme Type
    func requestThemeType(_ parameters: [String: AnyObject]?=nil, Target:AnyObject) {
        NotificationCenter.default.addObserver(Target, selector: #selector (CustomizeScreenVc.responseThemeType), name: NSNotification.Name(rawValue: NOTIFIACTION_THEMETYPE), object: nil)
        let netcom = Netcom()
        netcom.netComThemeType(parameters)
    }
    
    //MARK: - Download Updated Theme Images
    func requestFetchThemeImagesFolderUrl(_ parameters: [String: AnyObject]?=nil, Target:AnyObject) {
        NotificationCenter.default.addObserver(Target, selector: #selector (CustomizeScreenVc.responseFetchThemeImagesFolderUrl), name: NSNotification.Name(rawValue: NOTIFICATION_FETCHTHEMEIMAGESFOLDERURL), object: nil)
        let netcom = Netcom()
        netcom.netComFetchThemeImagesFolderUrl(parameters)
    }
    
    func requestDownloadThemeImagesFile(_ folderUrl: String, Target:AnyObject) {
        NotificationCenter.default.addObserver(Target, selector: #selector (CustomizeScreenVc.responseDownloadThemeImagesFile), name: NSNotification.Name(rawValue: NOTIFICATION_DOWNLOADTHEMEIMAGESFILE), object: nil)
        let netcom = Netcom()
        netcom.netComDownloadThemeImagesFile(folderUrl)
    }
    
    //MARK: - Set Peep Code
    func requestSetPeepCode(_ parameters: [String: AnyObject]?=nil, Target:AnyObject) {
        
        NotificationCenter.default.addObserver(Target, selector: #selector (GenrateCodeVc.responsePeepCode), name: NSNotification.Name(rawValue: NOTIFIACTION_SETPEEPCODE), object: nil)
        
        let netcom = Netcom()
        
        netcom.netComSetPeepCode(parameters)
    }
    //MARK: - Set Push Notification Settings
    func requestSetPushNotification(_ parameters: [String: AnyObject]?=nil, Target:AnyObject) {
        
        NotificationCenter.default.addObserver(Target, selector: #selector (PushNotificationSettingsViewController.responseSetPushNotification), name: NSNotification.Name(rawValue: NOTIFIACTION_SETPUSHNOTIFICATION), object: nil)
        
        let netcom = Netcom()
        
        netcom.netComSetPushNotification(parameters)
    }
    //MARK: - Get Push Notification Settings
    func requestGetPushNotification(_ parameters: [String: AnyObject]?=nil, Target:AnyObject) {
        
        NotificationCenter.default.addObserver(Target, selector: #selector (PushNotificationSettingsViewController.responseGetPushNotification), name: NSNotification.Name(rawValue: NOTIFIACTION_GETPUSHNOTIFICATION), object: nil)
        let netcom = Netcom()
        netcom.netComGetPushNotification(parameters)
    }
    //MARK: - Logout
    func requestLogout(_ parameters: [String: AnyObject]?=nil, Target:AnyObject) {
        NotificationCenter.default.addObserver(Target, selector: #selector (BaseVC.responseLogOut), name: NSNotification.Name(rawValue: NOTIFIACTION_LOGOUT), object: nil)

        let netcom = Netcom()
        netcom.netComLogout(parameters)
    }
    
    //MARK: - Upgrade Package
    func requestUpgradePackage(_ parameters: [String: AnyObject]?=nil, Target:AnyObject) {
        
        NotificationCenter.default.addObserver(Target, selector: #selector (ApplicationDelegate.responseUpdatePackage), name: NSNotification.Name(rawValue: NOTIFICATION_UPDATEPACKAGE), object: nil)
        
        let netcom = Netcom()
        
        netcom.netComUpgradePackage(parameters)
    }
    
    //MARK: - GetFeeds
    func requestGetFeeds(_ parameters: [String: AnyObject]?=nil, Target:AnyObject) {
        
         NotificationCenter.default.addObserver(Target, selector: #selector (HomeTabVc.responseGetFeeds), name: NSNotification.Name(rawValue: NOTIFIACTION_GETFEEDS), object: nil)
        
        let netcom = Netcom()
        
        netcom.netComGetFeeds(parameters)
    }
    
    //MARK: - GetPostsLiked
    func requestPostsLiked(_ parameters: [String: AnyObject]?=nil, Target:AnyObject) {
        
        NotificationCenter.default.addObserver(Target, selector: #selector (PostsLikedViewController.responseGetPostsLiked), name: NSNotification.Name(rawValue: NOTIFICATION_GETPOSTSLIKED), object: nil)
        
        let netcom = Netcom()
        
        netcom.netComGetPostsLiked(parameters)
    }

    
    //MARK: - AddComment
    func requestAddComment(_ parameters: [String: AnyObject]?=nil, Target:AnyObject) {
        
        NotificationCenter.default.addObserver(Target, selector: #selector (CommentVc.responseAddComment), name: NSNotification.Name(rawValue: NOTIFIACTION_ADDCOMMENT), object: nil)
        
        let netcom = Netcom()
        
        netcom.netComAddComment(parameters)
    }
    
    //MARK: - Like
    func requestLike(_ parameters: [String: AnyObject]?=nil, Target:AnyObject) {
        
        NotificationCenter.default.addObserver(Target, selector: #selector (UniversalVc.likeMediaResponse), name: NSNotification.Name(rawValue: NOTIFIACTION_LIKE), object: nil)
        
        let netcom = Netcom()
        
        netcom.netComLikeMedia(parameters)
    }
    
    //MARK: - Like PeepText
    func requestLikePeepText(_ parameters: [String: AnyObject]?=nil, Target:AnyObject) {
        
        NotificationCenter.default.addObserver(Target, selector: #selector (UniversalCollectionView.likePeepTextResponse), name: NSNotification.Name(rawValue: NOTIFIACTION_LIKE_PEEPTEXT), object: nil)
        
        let netcom = Netcom()
        
        netcom.netComLikePeepText(parameters)
    }
    
    //MARK: - Dislike
    func requestDislike(_ parameters: [String: AnyObject]?=nil, Target:AnyObject) {
        
        NotificationCenter.default.addObserver(Target, selector: #selector (UniversalVc.dislikeMediaResponse), name: NSNotification.Name(rawValue: NOTIFIACTION_DISLIKE), object: nil)
        let netcom = Netcom()
        netcom.netComDislikeMedia(parameters)
    }
    
    //MARK: - Dislike PeepText
    func requestDislikePeepText(_ parameters: [String: AnyObject]?=nil, Target:AnyObject) {
        
        NotificationCenter.default.addObserver(Target, selector: #selector (UniversalCollectionView.dislikePeepTextResponse), name: NSNotification.Name(rawValue: NOTIFIACTION_DISLIKE_PEEPTEXT), object: nil)
        let netcom = Netcom()
        netcom.netComDislikePeepText(parameters)
    }
    
    
    //MARK: - Get Comments
    func requestGetComments(_ parameters: [String: AnyObject]?=nil, Target:AnyObject) {
        
        NotificationCenter.default.addObserver(Target, selector: #selector (CommentVc.responseGetComments), name: NSNotification.Name(rawValue: NOTIFIACTION_GETCOMMENTS), object: nil)
        
        let netcom = Netcom()
        
        netcom.netComGetComments(parameters)
    }
    
    //MARK: - Get People
    func requestGetPeople(_ parameters: [String: AnyObject]?=nil, Target:AnyObject) {
        
        NotificationCenter.default.addObserver(Target, selector: #selector (SearchViewController.responseGetPeoples), name: NSNotification.Name(rawValue: NOTIFIACTION_GETPEOPLE), object: nil)
        
        let netcom = Netcom()
        
        netcom.netComGetPeople(parameters)
    }
    //MARK: - Get Liker People
    func requestGetLikerPeople(_ parameters: [String: AnyObject]?=nil, Target:AnyObject) {
        
        NotificationCenter.default.addObserver(Target, selector: #selector (LikersViewController.responseGetLikerPeoples), name: NSNotification.Name(rawValue: NOTIFIACTION_GETLIKERPEOPLE), object: nil)
        
        let netcom = Netcom()
        
        netcom.netComGetLikerPeople(parameters)
    }
    
    //MARK: - Get DisLiker People
    func requestGetDisLikerPeople(_ parameters: [String: AnyObject]?=nil, Target:AnyObject) {
        
        NotificationCenter.default.addObserver(Target, selector: #selector (LikersViewController.responseGetDisLikerPeoples), name: NSNotification.Name(rawValue: NOTIFIACTION_GETDISLIKERPEOPLE), object: nil)
        
        let netcom = Netcom()
        
        netcom.netComGetDisLikerPeople(parameters)
    }
    
    //MARK: - Search People
    func requestSearchPeople(_ parameters: [String: AnyObject]?=nil, Target:AnyObject) {
        
        NotificationCenter.default.addObserver(Target, selector: #selector (SearchViewController.responseSearchPeoples), name: NSNotification.Name(rawValue: NOTIFIACTION_SEARCHPEOPLE), object: nil)
        
        let netcom = Netcom()
        
        netcom.netComSearchPeople(parameters)
    }
    
    //MARK: - Send Follow Request
    func requestFollow(_ parameters: [String: AnyObject]?=nil, Target:AnyObject) {
        if Target.isKind(of: SearchViewController.self) {
            NotificationCenter.default.addObserver(Target, selector: #selector (SearchViewController.responseFollowPeople), name: NSNotification.Name(rawValue: NOTIFIACTION_FOLLOWPEOPLE), object: nil)
        }
        else {
            NotificationCenter.default.addObserver(Target, selector: #selector (ContactListVc.responseFollowPeople), name: NSNotification.Name(rawValue: NOTIFIACTION_FOLLOWPEOPLE), object: nil)
        }
        
        let netcom = Netcom()
        
        netcom.netComFollowPeople(parameters)
    }
    
    //MARK: - Send UnFollow Request
    func requestUnFollow(_ parameters: [String: AnyObject]?=nil, Target:AnyObject) {
        
        NotificationCenter.default.addObserver(Target, selector: #selector (SearchViewController.responseUnFollowPeople), name: NSNotification.Name(rawValue: NOTIFIACTION_UNFOLLOWPEOPLE), object: nil)
        
        let netcom = Netcom()
        
        netcom.netComUnFollowPeople(parameters)
    }
    
    //MARK: - Upload Digital Media
    func requestUploadDigitalMedia(_ parameters: [String: AnyObject]?=nil, Target:AnyObject) {
        
        NotificationCenter.default.addObserver(Target, selector: #selector (ContactsScreen.responseUploadDigitalMedia), name: NSNotification.Name(rawValue: NOTIFIACTION_POSTUPLOADDIGITALMEDIA), object: nil)
        
        let netcom = Netcom()
        
        netcom.netComUploadDigitalMedia(parameters)
    }
    
    //MARK: - Send Peep Text
    func requestSendPeepText(_ parameters: [String: AnyObject]?=nil, Target:AnyObject) {
        
        NotificationCenter.default.addObserver(Target, selector: #selector (ContactsScreen.responseSendPeepText), name: NSNotification.Name(rawValue: NOTIFIACTION_POSTSENDPEEPTEXT), object: nil)
        
        let netcom = Netcom()
        
        netcom.netComSendPeepText(parameters)
    }
    
    //MARK: - Get Contacts
    func requestGetContacts(_ parameters: [String: AnyObject]?=nil, Target:AnyObject) {
        
        NotificationCenter.default.addObserver(Target, selector: #selector (ContactsScreen.responseGetContacts), name: NSNotification.Name(rawValue: NOTIFIACTION_GETCONTACTS), object: nil)
        
        let netcom = Netcom()
        
        netcom.netComGetContacts(parameters)
    }
    
    //MARK: - Get Followers
    func requestGetFollowers(_ parameters: [String: AnyObject]?=nil, Target:AnyObject) {
        
        NotificationCenter.default.addObserver(Target, selector: #selector (ContactsScreen.responseGetFollowers), name: NSNotification.Name(rawValue: NOTIFIACTION_GETFOLLOWERS), object: nil)
        
        let netcom = Netcom()
        
        netcom.netComGetFollowers(parameters)
    }
    
    //MARK: - Search Contacts
    func requestSearchContacts(_ parameters: [String: AnyObject]?=nil, Target:AnyObject) {
        
        NotificationCenter.default.addObserver(Target, selector: #selector (ContactsScreen.responseSearchContacts), name: NSNotification.Name(rawValue: NOTIFIACTION_SEARCHCONTACTS), object: nil)
        
        let netcom = Netcom()
        
        netcom.netComSearchContacts(parameters)
    }
    
    //MARK: - Report Media
    func reportMedia(_ parameters: [String: AnyObject]?=nil, Target:AnyObject) {
        
        NotificationCenter.default.addObserver(Target, selector: #selector (UniversalVc.reportMediaResponse), name: NSNotification.Name(rawValue: NOTIFIACTION_REPORTMEDIA), object: nil)
        
        let netcom = Netcom()
        
        netcom.netComReportMedia(parameters)
    }
    
    //MARK: - Get Public Movie Media on Search Screen
    func getPublicMovieMedia(_ parameters: [String: AnyObject]?=nil, Target:AnyObject) {
        
        NotificationCenter.default.addObserver(Target, selector: #selector (SearchViewController.responseGetPublicMovieMedia), name: NSNotification.Name(rawValue: NOTIFIACTION_GETPUBLICMOVIEMEDIA), object: nil)
        
        let netcom = Netcom()
        
        netcom.netComGetPublicMovieMedia(parameters)
    }
   
    //MARK: - Get Public Music Media on Search Screen
    func getPublicMusicMedia(_ parameters: [String: AnyObject]?=nil, Target:AnyObject) {
        
        NotificationCenter.default.addObserver(Target, selector: #selector (SearchViewController.responseGetPublicMusicMedia), name: NSNotification.Name(rawValue: NOTIFIACTION_GETPUBLICMUSICMEDIA), object: nil)
        
        let netcom = Netcom()
        
        netcom.netComGetPublicMusicMedia(parameters)
    }
    
    //MARK: - Get Public Video Games Media on Search Screen
    func getPublicVideoGamesMedia(_ parameters: [String: AnyObject]?=nil, Target:AnyObject) {
        
        NotificationCenter.default.addObserver(Target, selector: #selector (SearchViewController.responseGetPublicVideoGamesMedia), name: NSNotification.Name(rawValue: NOTIFIACTION_GETPUBLICVIDEOGAMESMEDIA), object: nil)
        
        let netcom = Netcom()
        
        netcom.netComGetPublicVideoGamesMedia(parameters)
    }
    
    
    //MARK: - Get Gallery Movies
    func requestGalleryMovies(_ parameters: [String: AnyObject]?=nil, Target:AnyObject) {
        
        NotificationCenter.default.addObserver(Target, selector: #selector (GalleryMainViewController.responseGalleryMovies), name: NSNotification.Name(rawValue: NOTIFIACTION_GETGALLERYMOVIES), object: nil)
        
        let netcom = Netcom()
        
        netcom.netComGetGalleryMovies(parameters)
    }

    //MARK: - Get Gallery Movies
    func requestGalleryMusic(_ parameters: [String: AnyObject]?=nil, Target:AnyObject) {
        
        NotificationCenter.default.addObserver(Target, selector: #selector (GalleryMainViewController.responseGalleryMusic), name: NSNotification.Name(rawValue: NOTIFIACTION_GETGALLERYMUSIC), object: nil)
        
        let netcom = Netcom()
        
        netcom.netComGetGalleryMusic(parameters)
    }
    
    //MARK: - Get Gallery Video Games
    func requestGalleryVideoGames(_ parameters: [String: AnyObject]?=nil, Target:AnyObject) {
        
        NotificationCenter.default.addObserver(Target, selector: #selector (GalleryMainViewController.responseGalleryVideoGames), name: NSNotification.Name(rawValue: NOTIFIACTION_GETGALLERYVIDEOGAMES), object: nil)
        
        let netcom = Netcom()
        
        netcom.netComGetGalleryVideoGames(parameters)
    }
    
    //MARK: - Get Gallery Photos
    func requestGalleryPhotos(_ parameters: [String: AnyObject]?=nil, Target:AnyObject) {
        
        NotificationCenter.default.addObserver(Target, selector: #selector (GalleryMainViewController.responseGalleryPhotos), name: NSNotification.Name(rawValue: NOTIFIACTION_GETGALLERYPHOTOS), object: nil)
        
        let netcom = Netcom()
        
        netcom.netComGetGalleryPhotos(parameters)
    }
    
    //MARK: - Get Gallery Peep Text
    func requestGalleryPeepText(_ parameters: [String: AnyObject]?=nil, Target:AnyObject) {
        
        NotificationCenter.default.addObserver(Target, selector: #selector (GalleryMainViewController.responseGalleryPeepText), name: NSNotification.Name(rawValue: NOTIFIACTION_GETGALLERYPEEPTEXT), object: nil)
        let netcom = Netcom()
        netcom.netComGetGalleryPeepText(parameters)
    }
    //MARK: - Get Peep Text Conversation
    func requestPeepTextConversation(_ parameters: [String: AnyObject]?=nil, Target:AnyObject) {
        
        NotificationCenter.default.addObserver(Target, selector: #selector (PeepTextDetailCollectionViewController.responsePeepTextConversation), name: NSNotification.Name(rawValue: NOTIFIACTION_GETPEEPTEXTCONVERSATION), object: nil)
        
        let netcom = Netcom()
        
        netcom.netComGetPeepTextConversation(parameters)
    }
    //MARK: - Search Media
    func searchPublicMedia(_ parameters: [String: AnyObject]?=nil, Target:AnyObject) {
        
        NotificationCenter.default.addObserver(Target, selector: #selector (SearchViewController.responseSearchPublicMedia), name: NSNotification.Name(rawValue: NOTIFIACTION_SEARCHPUBLICMEDIA), object: nil)
        
        let netcom = Netcom()
        
        netcom.netComSearchPublicMedia(parameters)
    }
    
    //MARK: - Fetch Event Categories
    func fetchEventCategories (_ parameters: [String: AnyObject]?=nil, Target:AnyObject) {
        
        NotificationCenter.default.addObserver(Target, selector: #selector (CreateEventFirstViewController.responseFetchEventCategories), name:NSNotification.Name(rawValue: NOTIFICATION_FETCHEVENTCATEGORIES), object: nil)
        
        let netcom = Netcom()
        netcom.netComFetchEventCategories(parameters)
    }
    
    //MARK: - Send Event
    func requestSendEventInvitation(_ parameters: [String: AnyObject]?=nil, Target:AnyObject) {
        
        NotificationCenter.default.addObserver(Target, selector: #selector (ContactsScreen.responseSendEvent), name: NSNotification.Name(rawValue: NOTIFICATION_SENDEVENT), object: nil)
        
        let netcom = Netcom()
        
        netcom.netComSendEvent(parameters)
    }
    
    //MARK: - Fetch Events
    func fetchEvents (_ parameters: [String: AnyObject]?=nil, Target:AnyObject) {
        if parameters!["type"] as! Int == 1 {
            NotificationCenter.default.addObserver(Target, selector: #selector (EventVc.responseFetchMyEvents(_:)), name:NSNotification.Name(rawValue: NOTIFICATION_FETCHMYEVENTS), object: nil)
        }
        else {
            NotificationCenter.default.addObserver(Target, selector: #selector (EventVc.responseFetchAllEvents(_:)), name:NSNotification.Name(rawValue: NOTIFICATION_FETCHALLEVENTS), object: nil)
        }
        
        let netcom = Netcom()
        netcom.netComFetchEvents(parameters)
    }

    //MARK: - Delete Event
    func deleteEvent (_ parameters: [String: AnyObject]?=nil, Target:AnyObject) {
        
        NotificationCenter.default.addObserver(Target, selector: #selector (EventVc.responseDeleteEvent), name:NSNotification.Name(rawValue: NOTIFICATION_DELETEEVENT), object: nil)
        
        let netcom = Netcom()
        netcom.netComDeleteEvent(parameters)
    }
    
    //MARK: - Delete Event Invite
    func deleteEventInvite (_ parameters: [String: AnyObject]?=nil, Target:AnyObject) {
        
        NotificationCenter.default.addObserver(Target, selector: #selector (EventVc.responseDeleteEventInvite(_:)), name:NSNotification.Name(rawValue: NOTIFICATION_DELETEEVENTINVITE), object: nil)
        
        let netcom = Netcom()
        netcom.netComDeleteEventInvite(parameters)
    }
    
    //MARK: - Edit Event
    func editEvent (_ parameters: [String: AnyObject]?=nil, Target:AnyObject) {
        
        NotificationCenter.default.addObserver(Target, selector: #selector (ContactsScreen.responseEditEvent), name:NSNotification.Name(rawValue: NOTIFICATION_EDITEVENT), object: nil)
        
        let netcom = Netcom()
        netcom.netComEditEvent(parameters)
    }
    
    //MARK: - Fetch Not Registered PecX Users
    func fetchNotRegisteredPecXUsers (_ parameters: [String: AnyObject]?=nil, Target:AnyObject) {
        NotificationCenter.default.addObserver(Target, selector: #selector (ContactListVc.responseFetchNotRegisteredPecXUsers(_:)), name:NSNotification.Name(rawValue: NOTIFICATION_FETCHNOTREGISTEREDPECXUSERS), object: nil)
        
        let netcom = Netcom()
        netcom.netComFetchNotRegisteredPecXUsers(parameters)
    }
    
    //MARK: - Fetch Not Following PecX Users
    func fetchNotFollowingPecXUsers (_ parameters: [String: AnyObject]?=nil, Target:AnyObject) {
        NotificationCenter.default.addObserver(Target, selector: #selector (ContactListVc.responseFetchNotFollowingPecXUsers(_:)), name:NSNotification.Name(rawValue: NOTIFICATION_FETCHNOTFOLLOWINGPECXUSERS), object: nil)
        
        let netcom = Netcom()
        netcom.netComFetchNotFollowingPecXUsers(parameters)
    }

    //MARK: - Get Notification List
    func getNotificationList (_ parameters: [String: AnyObject]?=nil, Target:AnyObject) {
        
        NotificationCenter.default.addObserver(Target, selector: #selector (NotificationViewController.responseGetNotificationList), name:NSNotification.Name(rawValue: NOTIFICATION_GETNOTIFICATIONLIST), object: nil)
        
        let netcom = Netcom()
        netcom.netComGetNotificationList(parameters)
    }
    
    //MARK: - Get Unread Notification Count
    func getUnreadNotificationCount (_ parameters: [String: AnyObject]?=nil, Target:AnyObject) {
        
        NotificationCenter.default.addObserver(Target, selector: #selector (HomeTabVc.responseGetUnreadNotificationCount), name:NSNotification.Name(rawValue: NOTIFICATION_GETUNREADNOTIFICATIONCOUNT), object: nil)
        
        let netcom = Netcom()
        netcom.netComGetUnreadNotificationCount(parameters)
    }
    
    //MARK: - Read Notification
    func readNotification (_ parameters: [String: AnyObject]?=nil, Target:AnyObject) {
        
        NotificationCenter.default.addObserver(Target, selector: #selector (NotificationViewController.responseReadNotification), name:NSNotification.Name(rawValue: NOTIFICATION_SETREADNOTIFICATION), object: nil)
        
        let netcom = Netcom()
        netcom.netComReadNotification(parameters)
    }
    
    //MARK: - Share Media
    func shareMedia (_ parameters: [String: AnyObject]?=nil, Target:AnyObject) {
        
        //Navdeep
        
        NotificationCenter.default.addObserver(Target, selector: #selector(self.selectorNil), name:NSNotification.Name(rawValue: NOTIFICATION_SHAREMEDIA), object: nil)
        
        let netcom = Netcom()
        netcom.netComShareMedia(parameters)
    }
    
    @objc func selectorNil(){
        
    }
    
    //MARK: - SetFeedback
    func setFeedback (_ parameters: [String: AnyObject]?=nil, Target:AnyObject) {
        
        NotificationCenter.default.addObserver(Target, selector: #selector (FeedbackViewController.responseSendFeedback), name:NSNotification.Name(rawValue: NOTIFICATION_SETFEEDBACK), object: nil)
        
        let netcom = Netcom()
        netcom.netComSetFeedback(parameters)
    }
    
    //MARK: - Approve/Disapprove new follow request
    func approveDisapproveFollowRequest (_ parameters: [String: AnyObject]?=nil, Target:AnyObject) {
        
        NotificationCenter.default.addObserver(Target, selector: #selector (NotificationViewController.responseapproveDisapproveFollowRequest), name:NSNotification.Name(rawValue: NOTIFIACTION_APPROVEDISAPPROVEFOLLOW), object: nil)
        
        let netcom = Netcom()
        netcom.netComApproveDisapproveFollowRequest(parameters)
    }
    
    //MARK: - Peepcode verification
    func peepCodeVerificationRequest (_ parameters: [String: AnyObject]?=nil, Target:AnyObject) {
        
        let netcom = Netcom()
        netcom.netComPeepCodeVerificationRequest(parameters)
    }
    
    //MARK: - Send Invite To Contacts
    func sendInviteToContacts (_ parameters: [String: AnyObject]?=nil, Target:AnyObject) {
        NotificationCenter.default.addObserver(Target, selector: #selector (ContactListVc.responseSendInviteToContacts(_:)), name:NSNotification.Name(rawValue: NOTIFICATION_SENDINVITETOCONTACTS), object: nil)
        
        let netcom = Netcom()
        netcom.netComSendInviteToContacts(parameters)
    }
    
    //MARK: - Accept Event Invitation
    func requestAcceptEventInvitation (_ parameters: [String: AnyObject]?=nil, Target:AnyObject) {
        NotificationCenter.default.addObserver(Target, selector: #selector (EventDetailsVc.responseAcceptEventInvitation(_:)), name:NSNotification.Name(rawValue: NOTIFICATION_ACCEPTEVENTINVITATION), object: nil)
        
        let netcom = Netcom()
        netcom.netComAcceptEventInvitation(parameters)
    }
    
    //MARK: - Reject Event Invitation
    func requestRejectEventInvitation (_ parameters: [String: AnyObject]?=nil, Target:AnyObject) {
        NotificationCenter.default.addObserver(Target, selector: #selector (EventDetailsVc.responseRejectEventInvitation(_:)), name:NSNotification.Name(rawValue: NOTIFICATION_REJECTEVENTINVITATION), object: nil)
        
        let netcom = Netcom()
        netcom.netComRejectEventInvitation(parameters)
    }
    
    //MARK: - Fetch Countries
    func fetchCountries (_ parameters: [String: AnyObject]?=nil, Target:AnyObject) {
        NotificationCenter.default.addObserver(Target, selector: #selector (CreateSubmerchantViewController.responseFetchCountries(_:)), name:NSNotification.Name(rawValue: NOTIFICATION_FETCHCOUNTRIES), object: nil)
        
        let netcom = Netcom()
        netcom.netComFetchCountries(parameters)
    }
    
    //MARK: - Fetch Regions
    func fetchRegions (_ parameters: [String: AnyObject]?=nil, Target:AnyObject) {
        NotificationCenter.default.addObserver(Target, selector: #selector (CreateSubmerchantViewController.responseFetchRegions(_:)), name:NSNotification.Name(rawValue: NOTIFICATION_FETCHREGIONS), object: nil)
        
        let netcom = Netcom()
        netcom.netComFetchRegions(parameters)
    }
    
    /*//MARK: - Fetch Localities
    func fetchLocalities (parameters: [String: AnyObject]?=nil, Target:AnyObject) {
        NSNotificationCenter.defaultCenter().addObserver(Target, selector: #selector (CreateSubmerchantViewController.responseFetchLocalities(_:)), name:NOTIFICATION_FETCHLOCALITIES, object: nil)
        
        let netcom = Netcom()
        netcom.netComFetchLocalities(parameters)
    }*/
    
    //MARK: - Check Submerchant Account Exists
    func checkSubMerchantAccountExists (_ parameters: [String: AnyObject]?=nil, Target:AnyObject) {
        NotificationCenter.default.addObserver(Target, selector: #selector (SettingMainViewController.responseCheckSubmerchantAccountExists(_:)), name:NSNotification.Name(rawValue: NOTIFICATION_CHECKSUBMERCHANTACCOUNTEXISTS), object: nil)
        
        let netcom = Netcom()
        netcom.netComCheckSubmerchantAccountExists(parameters)
    }
    
    //MARK: - Create Submerchant Account
    func requestCreateSubMerchantAccount (_ parameters: [String: AnyObject]?=nil, Target:AnyObject) {
        NotificationCenter.default.addObserver(Target, selector: #selector (CreateSubmerchantViewController.responseCreateSubmerchantAccount(_:)), name:NSNotification.Name(rawValue: NOTIFICATION_CREATESUBMERCHANTACCOUNT), object: nil)
        
        let netcom = Netcom()
        netcom.netComCreateSubmerchantAccount(parameters)
    }
    
    //MARK: - Update Submerchant Account
    func requestUpdateSubMerchantAccount (_ parameters: [String: AnyObject]?=nil, Target:AnyObject) {
        NotificationCenter.default.addObserver(Target, selector: #selector (CreateSubmerchantViewController.responseCreateSubmerchantAccount(_:)), name:NSNotification.Name(rawValue: NOTIFICATION_CREATESUBMERCHANTACCOUNT), object: nil)
        
        let netcom = Netcom()
        netcom.netComUpdateSubmerchantAccount(parameters)
    }
    
    //MARK: - Create Braintree Token
    func requestCreateBraintreeToken (_ parameters: [String: AnyObject]?=nil, Target:AnyObject) {
        NotificationCenter.default.addObserver(Target, selector: #selector (ApplicationDelegate.responseCreateBraintreeToken(_:)), name:NSNotification.Name(rawValue: NOTIFICATION_CREATEBRAINTREETOKEN), object: nil)
        
        let netcom = Netcom()
        netcom.netComCreateCreateBraintreeToken(parameters)
    }

    //MARK: - Process Braintree Payment
    func requestProcessBraintreePayment (_ parameters: [String: AnyObject]?=nil, Target:AnyObject) {
        NotificationCenter.default.addObserver(Target, selector: #selector (BaseVC.responseProcessPayment(_:)), name:NSNotification.Name(rawValue: NOTIFICATION_PROCESSBRAINTREEPAYMENT), object: nil)
        
        let netcom = Netcom()
        netcom.netComProcessBraintreePayment(parameters)
    }
    
    //MARK: - Get Saved Cards
    func requestGetSavedCards (_ parameters: [String: AnyObject]?=nil, Target:AnyObject) {
        NotificationCenter.default.addObserver(Target, selector: #selector (EventDetailsVc.responseGetSavedCards(_:)), name:NSNotification.Name(rawValue: NOTIFICATION_GETSAVEDCARDS), object: nil)
        
        let netcom = Netcom()
        netcom.netComGetSavedCards(parameters)
    }

    //MARK: - Get Following People
    func requestGetFollowingPeople(_ parameters: [String: AnyObject]?=nil, Target:AnyObject) {
        
        NotificationCenter.default.addObserver(Target, selector: #selector (LikersViewController.responseGetFollowingPeoples), name: NSNotification.Name(rawValue: NOTIFIACTION_GETFOLLOWINGPEOPLE), object: nil)
        
        let netcom = Netcom()
        
        netcom.netComGetFollowingPeople(parameters)
    }
    
    //MARK: - Get Follower People
    func requestGetFollowerPeople(_ parameters: [String: AnyObject]?=nil, Target:AnyObject) {
        
        NotificationCenter.default.addObserver(Target, selector: #selector (LikersViewController.responseGetFollowerPeoples), name: NSNotification.Name(rawValue: NOTIFIACTION_GETFOLLOWERPEOPLE), object: nil)
        
        let netcom = Netcom()
        
        netcom.netComGetFollowerPeople(parameters)
    }
    
    //MARK: - Get User Posts
    func requestGetUserPosts(_ parameters: [String: AnyObject]?=nil, Target:AnyObject) {
        
        NotificationCenter.default.addObserver(Target, selector: #selector (PostsLikedViewController.responseGetUserPosts), name: NSNotification.Name(rawValue: NOTIFIACTION_GETUSERPOSTS), object: nil)
        
        let netcom = Netcom()
        
        netcom.netComGetUserPosts(parameters)
    }
    
    //MARK: - Forward Media
    func requestForwardMedia(_ parameters: [String: AnyObject]?=nil, Target:AnyObject) {
        
        NotificationCenter.default.addObserver(Target, selector: #selector (ContactsScreen.responseForwardMedia), name: NSNotification.Name(rawValue: NOTIFIACTION_FORWARDMEDIA), object: nil)
        
        let netcom = Netcom()
        
        netcom.netComForwardMedia(parameters)
    }
    
    //MARK: - Get Top Five Users
    func requestGetTopFiveUsers(_ parameters: [String: AnyObject]?=nil, Target:AnyObject) {
        
        NotificationCenter.default.addObserver(Target, selector: #selector (LikersViewController.responseGetTopFiveUsers), name: NSNotification.Name(rawValue: NOTIFIACTION_GETTOPFIVEUSERS), object: nil)
        
        let netcom = Netcom()
        
        netcom.netComGetTopFiveUsers(parameters)
    }
}
