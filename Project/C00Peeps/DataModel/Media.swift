//
//  Media.swift
//  C00Peeps
//
//  Created by Mac on 19/07/16.
//  Copyright © 2016 SOTSYS011. All rights reserved.
//

import Foundation

class Media:NSObject{
    
    var pId = 0
    var pUserId = 0
    var pMediaType = 0
    var pMediaFile = ""
    var pMediaCaption = ""
    var pMediaAttachment = ""
    var pMediaAttachmentType = 0
    var pIsPublishedAll = ""
    var pCreatedDate = ""
    var pExpiryDate = ""
    var pUserName = ""
    var pUserImage = ""
    var pSentUserId = 0
    var pSentUserName = ""
    var pSentUserImage = ""
    var pCountLikes = 0
    var pCountDislikes = 0
    var pCountComments = 0
    var pIsLiked = 0
    var pIsDisliked = 0
    var pIsPlaying = 0
    var pSlug = ""//Share Link Url
    var pIsLocked = -1
    var pPrivacyLevelId = -1
    var pSentToPrivacyLevelId = -1
    var pPeepText = ""

}
