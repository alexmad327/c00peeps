//
//  Comment.swift
//  C00Peeps
//
//  Created by Mac on 19/07/16.
//  Copyright © 2016 SOTSYS011. All rights reserved.
//

import Foundation

class Comment:NSObject{
    
    var pId = 0
    var pUserId = 0
    var pMediaId = 0
    var pCommenterId = 0
    var pComment = ""
    var pCreatedDate = ""
    var pModifiedDate = ""
    var pUserName = ""
    var pUserImage = ""
}