//
//  Notification.swift
//  C00Peeps
//
//  Created by Mac on 10/11/16.
//  Copyright © 2016 SOTSYS011. All rights reserved.
//

import Foundation

class Notification:NSObject{
    
    
    var pCategoryTypeId = 0
    var pCountNotification = 0
    var pCreatedDate = ""
    var pId = 0
    var pIsRead = 0
    var pLikeStatus = 0
    var pMediaDetail = Media()
    var pMediaId = 0
    var pModifiedDate = ""
    var pSentTouserDetail = People()
    var pSentFromUserDetail = [People]() //This could be an array
    var pEventDetail = Event()

}