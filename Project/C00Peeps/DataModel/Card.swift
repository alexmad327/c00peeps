//
//  Card.swift
//  C00Peeps
//
//  Created by Arshdeep on 04/01/17.
//  Copyright © 2017 SOTSYS011. All rights reserved.
//

import UIKit

class Card: NSObject {
    var id = 0
    var token = ""
    var last4 = ""
    var maskedNumber = ""
    var type = ""
    var imageUrl = ""
    var expirationMonth = ""
    var expirationYear = ""

}
/*
 "id":"1","user_id":"50",
 "token":"9wysvj",
 
 "last4":"1881",
 
 "cardType":"Visa",
 
 "imageUrl":"https://assets.braintreegateway.com/payment_method_logo/visa.png?environment=sandbox",
 
 "expirationMonth":"12",
 
 "expirationYear":"2020",
 
 "maskedNumber":"401288******1881"
 */
