//
//  Event.swift
//  C00Peeps
//
//  Created by Arshdeep on 04/10/16.
//  Copyright © 2016 SOTSYS011. All rights reserved.
//

import UIKit

class Event: NSObject {
    var id = 0
    var userId = 0
    var title = ""
    var desc = ""
    var category = ""
    var categoryId = ""
    var location = ""
    var fromDate = ""
    var toDate = ""
    var fromTime = ""
    var toTime = ""
    var ticketCost = ""
    var totalTickets = ""
    var expirationDate = ""
    var eventType = 0//Public or Private
    var mediaImage:UIImage? = nil
    var mediaUrl:URL? = nil
    var mediaAttachmentUrl:URL? = nil
    var mediaType = 0//Photo/Audio/Video/None
    var isMediaLocked = 0
    var surveyLink = ""//"http://coopeeps.trantorinc.com/coopeeps/survey.php?user_id=&event_id="
    var paidStatus = 0
    var isEventAccepted = 0
    var isSurveyAttached = 0
}
