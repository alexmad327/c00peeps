//
//  Parse.swift
//  C00Peeps
//
//  Created by Mac on 19/07/16.
//  Copyright © 2016 SOTSYS011. All rights reserved.
//

import Foundation
import SwiftyJSON

class Parser : NSObject{

    //MARK: - Parse Media Array Data
    class func getParsedMediaArrayFromData(_ feedsArray:JSON)->[Media]{
        
        var mediaArray = [Media]()
    
        for  i in 0..<feedsArray.count
        {
            let media = getParsedMediaFromData(feedsArray[i])
            mediaArray.append(media)
        }
        
        return mediaArray
    
    }

    //MARK: - Parse Media Data
    class func getParsedMediaFromData(_ feed:JSON)->Media{
        
        let media = Media()
        
        media.pId = feed["id"].intValue
        media.pUserId = feed["user_id"].intValue
        
        //Media Type
        if feed["media_type"] != nil
        {
            media.pMediaType = feed["media_type"].intValue
        }
        
        //Media File
        if feed["media_file"] != nil
        {
            media.pMediaFile = feed["media_file"].stringValue
        }
        
        //Media Caption
        if feed["media_caption"] != nil
        {
            media.pMediaCaption = feed["media_caption"].stringValue
        }
        
        //Media Attachment
        if feed["media_attachment"] != nil
        {
            media.pMediaAttachment = feed["media_attachment"].stringValue
        }
        
        //Media Attachment Type
        if feed["media_attachment_type"] != nil
        {
            media.pMediaAttachmentType = feed["media_attachment_type"].intValue
        }
        
        //created_date
        if feed["created_date"] != nil
        {
            media.pCreatedDate = feed["created_date"].stringValue
        }
        
        //expiry_date
        if feed["expiry_date"] != nil
        {
            media.pExpiryDate = feed["expiry_date"].stringValue
        }
        
        //user_name
        if feed["username"] != nil
        {
            media.pUserName = feed["username"].stringValue
        }
        
        //user_image
        if feed["user_image"] != nil
        {
            media.pUserImage = feed["user_image"].stringValue
        }
        //sent_user_id
        if feed["sent_to"] != nil
        {
            media.pSentUserId = feed["sent_to"].intValue
        }
        
        //sent_user_name
        if feed["sent_to_username"] != nil
        {
            media.pSentUserName = feed["sent_to_username"].stringValue
        }
        
        //sent_user_image
        if feed["sent_to_user_image"] != nil
        {
            media.pSentUserImage = feed["sent_to_user_image"].stringValue
        }
        //count_likes
        media.pCountLikes = feed["count_likes"].intValue
        
        //count_dislikes
        media.pCountDislikes = feed["count_dislikes"].intValue
        
        //count_comments
        media.pCountComments = feed["count_comments"].intValue
        
        //is_liked
        media.pIsLiked = feed["is_liked"].intValue
        
        //is_disliked
        media.pIsDisliked = feed["is_disliked"].intValue
        
        //slug
        if feed["slug"] != nil
        {
            media.pSlug = feed["slug"].stringValue
        }
        
        //peep text
        if feed["peep_text"] != nil
        {
            media.pPeepText = feed["peep_text"].stringValue
        }
        
        //privacy level id
         media.pPrivacyLevelId = feed["privacy_level_id"].intValue
        
        //privacy level id
        media.pSentToPrivacyLevelId = feed["sent_to_privacy_level_id"].intValue
        
        //is locked
         media.pIsLocked = feed["is_locked"].intValue
        
        return media
    }
    
    //MARK: - Parse Comment Array Data
    class func getParsedCommentsArrayFromData(_ commentsArray:JSON)->[Comment]{
        
        var commentsArr = [Comment]()
        
        if(commentsArray.count > 0)
        {
            for  i in 0...commentsArray.count-1
            {
                let comment = getParsedCommentFromData(commentsArray[i])
                commentsArr.append(comment)
            }
        }
        
        
        return commentsArr
        
    }
    
    //MARK: - Parse Comment Data
    class func getParsedCommentFromData(_ commentdata:JSON)->Comment{
        
        let comment = Comment()
        
        comment.pId = commentdata["id"].intValue
        comment.pUserId = commentdata["user_id"].intValue
        comment.pMediaId = commentdata["media_id"].intValue
        comment.pCommenterId = commentdata["commenter_id"].intValue

        //commenter_details
        if commentdata["commenter_details"] != nil
        {
            comment.pComment = commentdata["commenter_details"].stringValue
        }
        
        //created_date
        if commentdata["created_date"] != nil
        {
            comment.pCreatedDate = commentdata["created_date"].stringValue
        }
        
        //modified_date
        if commentdata["modified_date"] != nil
        {
            comment.pModifiedDate = commentdata["modified_date"].stringValue
        }
        
        //user_name
        if commentdata["username"] != nil
        {
            comment.pUserName = commentdata["username"].stringValue
        }
        
        //user_image
        if commentdata["user_image"] != nil
        {
            comment.pUserImage = commentdata["user_image"].stringValue
        }
        
        return comment
    }

    //MARK: - Parse Contact Array Data
    class func getParsedContactArrayFromData(_ feedsArray:JSON)->[Contact]{
        
        var contactArray = [Contact]()
        
        for  i in 0...feedsArray.count-1
        {
            let contact = getParsedContactData(feedsArray[i])
            contactArray.append(contact)
        }
        
        return contactArray
    }
    
    //MARK: - Parse Contact Data
    class func getParsedContactData(_ feed:JSON)->Contact{
        
        let contact = Contact()
        
        contact.pId = feed["id"].intValue
        contact.pName = feed["name"].stringValue
        contact.pUserName = feed["username"].stringValue
        contact.pPublicKey = feed["public_key"].stringValue
        contact.pPrivacyLevelId = feed["privacy_level_id"].intValue
        
        return contact
    }
    
    //MARK: - Parse People Array Data
    class func getParsedPeopleArrayFromData(_ peopleArray:JSON)->[People]{
        
        var pArray = [People]()
        
        for  i in 0...peopleArray.count-1
        {
            let people = getParsedPeopleData(peopleArray[i])
            pArray.append(people)
        }
        
        return pArray
    }
    
    //MARK: - Parse People Data
    class func getParsedPeopleData(_ feed:JSON)->People{
        
        let people = People()
        
        people.pId = feed["id"].intValue
        people.pPost = feed["media_count"].intValue
        people.pUserImage = feed["user_image"].stringValue
        people.pUserName = feed["username"].stringValue
        people.pFriendStatus = feed["friends_status"].stringValue
        
        return people
    }
    
    // MARK: - Parse User Details
    class func parseUserDetails(_ responseUserDetail:[String:AnyObject], userDetail:UserDetail) {
        let basicUserDetail = responseUserDetail["basic"]
        let advancedUserDetail = responseUserDetail["profile"]

        //let userDetail:UserDetail = UserDetail()
        if let city = basicUserDetail!["city"] as? String {
            userDetail.city = city
        }
        if let email_id = basicUserDetail!["email_id"] as? String {
            userDetail.email_id = email_id
        }
        if let eot_purchased = basicUserDetail!["eot_purchased"] as? String {
            userDetail.eot_purchased = eot_purchased
        }
        if let id = basicUserDetail!["id"] as? String {
            userDetail.id = id
        }
        if let name = basicUserDetail!["name"] as? String {
            userDetail.name = name
        }
        if let package_id = basicUserDetail!["package_id"] as? String {
            if package_id == "0" {
                userDetail.package_id = "1"
            }
            else {
                userDetail.package_id = package_id
            }
        }
        if let password = basicUserDetail!["password"] as? String {
            userDetail.password = password
        }
        if let peepcode = basicUserDetail!["peepcode"] as? String {
            userDetail.peepcode = peepcode
        }
        if let phone = basicUserDetail!["phone"] as? String {
            userDetail.phone = phone
        }
        if let privacy_level_id = basicUserDetail!["privacy_level_id"] as? String {
            userDetail.privacy_level_id = privacy_level_id
        }
        if let profile_type_id = basicUserDetail!["profile_type_id"] as? String {
            userDetail.profile_type_id = profile_type_id
        }
        else {
            userDetail.profile_type_id = "1"
        }
        if let public_key = basicUserDetail!["public_key"] as? String {
            userDetail.public_key = public_key
        }
        if let social_account_type = basicUserDetail!["social_account_type"] as? String {
            userDetail.social_account_type = social_account_type
        }
        if let social_unique_id = basicUserDetail!["social_unique_id"] as? String {
            userDetail.social_unique_id = social_unique_id
        }
        if let state = basicUserDetail!["state"] as? String {
            userDetail.state = state
        }
        if let theme_id = basicUserDetail!["theme_id"] as? String {
            //userDetail.theme_id = "1"
            userDetail.theme_id = theme_id
        }
        if let title = basicUserDetail!["title"] as? String {
            userDetail.title = title
        }
        if let user_image = basicUserDetail!["user_image"] as? String {
            userDetail.user_image = user_image
        }
        if let username = basicUserDetail!["username"] as? String {
            userDetail.username = username
        }
        if let tagline = basicUserDetail!["tagline"] as? String {
            userDetail.tagline = tagline
        }
        if let website = basicUserDetail!["website"] as? String {
            userDetail.website = website
        }
    
        if let networkAffiliation = advancedUserDetail!["network_affiliation"] as? String {
            userDetail.networkAffiliation = networkAffiliation
        }
        if let recordLabel = advancedUserDetail!["record_label"] as? String {
            userDetail.recordLabel = recordLabel
        }
        if let gamingCompanyAffiliation = advancedUserDetail!["gaming_company_affiliation"] as? String {
            userDetail.gamingCompanyAffiliation = gamingCompanyAffiliation
        }
        if let mediaTypeId = advancedUserDetail!["film_media_type_id"] as? String {
            userDetail.filmMediaTypeId = mediaTypeId
        }
        if let filmFormatId = advancedUserDetail!["film_format_id"] as? String {
            userDetail.filmFormatId = filmFormatId
        }
        if let filmGenreId = advancedUserDetail!["film_genre_id"] as? String {
            userDetail.filmGenreId = filmGenreId
        }
        if let musicGenreId = advancedUserDetail!["music_genre_id"] as? String {
            userDetail.musicGenreId = musicGenreId
        }
        if let gameSpecialityId = advancedUserDetail!["game_speciality_id"] as? String {
            userDetail.gameSpecialityId = gameSpecialityId
        }
        if let gameGenreId = advancedUserDetail!["game_genre_id"] as? String {
            userDetail.gameGenreId = gameGenreId
        }
  
        if let submerchantExists = responseUserDetail["submerchant_exists"] as? Int {
            userDetail.submerchantExists = String(submerchantExists)
        }
        
        Utilities.setUserDetails(userDetail)
        
        printCustom("user details city: \(Utilities.getUserDetails().city)")
        printCustom("user details email_id: \(Utilities.getUserDetails().email_id)")
        printCustom("user details eot_purchased: \(Utilities.getUserDetails().eot_purchased)")
        printCustom("user details id: \(Utilities.getUserDetails().id)")
        printCustom("user details name: \(Utilities.getUserDetails().name)")
        printCustom("user details package_id: \(Utilities.getUserDetails().package_id)")
        printCustom("user details password: \(Utilities.getUserDetails().password)")
        printCustom("user details peepcode: \(Utilities.getUserDetails().peepcode)")
        printCustom("user details phone: \(Utilities.getUserDetails().phone)")
        printCustom("user details privacy_level_id: \(Utilities.getUserDetails().privacy_level_id)")
        printCustom("user details profile_type_id: \(Utilities.getUserDetails().profile_type_id)")
        printCustom("user details public_key: \(Utilities.getUserDetails().public_key)")
        printCustom("user details social_account_type: \(Utilities.getUserDetails().social_account_type)")
        printCustom("user details social_unique_id: \(Utilities.getUserDetails().social_unique_id)")
        printCustom("user details state: \(Utilities.getUserDetails().state)")
        printCustom("user details theme_id: \(Utilities.getUserDetails().theme_id)")
        printCustom("user details title: \(Utilities.getUserDetails().title)")
        printCustom("user details user_image: \(Utilities.getUserDetails().user_image)")
        printCustom("user details username: \(Utilities.getUserDetails().username)")

    }
    
    // MARK: - Parse Submerchant Details
    class func parseSubmerchantDetails(_ responseSubmerchantDetail:[String:AnyObject], submerchant:Submerchant) -> Submerchant {
        if let id = responseSubmerchantDetail["id"] as? String {
            submerchant.id = id
        }
        if let sub_merchant_id = responseSubmerchantDetail["sub_merchant_id"] as? String {
            submerchant.sub_merchant_id = sub_merchant_id
        }
        if let user_id = responseSubmerchantDetail["user_id"] as? String {
            submerchant.user_id = user_id
        }
        if let firstName = responseSubmerchantDetail["firstName"] as? String {
            submerchant.firstName = firstName
        }
        if let lastName = responseSubmerchantDetail["lastName"] as? String {
            submerchant.lastName = lastName
        }
        if let email = responseSubmerchantDetail["email"] as? String {
            submerchant.email = email
        }
        if let dateOfBirth = responseSubmerchantDetail["dateOfBirth"] as? String {
            submerchant.dateOfBirth = dateOfBirth
        }
        if let phone = responseSubmerchantDetail["phone"] as? String {
            submerchant.phone = phone
        }
        if let country = responseSubmerchantDetail["country_id"] as? String {
            submerchant.country = country
        }
        if let region = responseSubmerchantDetail["region"] as? String {
            submerchant.region = region
        }
        if let locality = responseSubmerchantDetail["locality"] as? String {
            submerchant.locality = locality
        }
        if let streetAddress = responseSubmerchantDetail["streetAddress"] as? String {
            submerchant.streetAddress = streetAddress
        }
        if let postalCode = responseSubmerchantDetail["postalCode"] as? String {
            submerchant.postalCode = postalCode
        }
        if let accountNumber = responseSubmerchantDetail["accountNumber"] as? String {
            submerchant.accountNumber = accountNumber
        }
        if let routingNumber = responseSubmerchantDetail["routingNumber"] as? String {
            submerchant.routingNumber = routingNumber
        }
        
        printCustom("submerchant: \(submerchant.sub_merchant_id)")
        
        return submerchant
    }
   
    //MARK: - Parse Events Data
    class func getParsedEventsArrayFromData(_ eventArray:JSON)->[Event]{
        
        var events = [Event]()
        
        for  i in 0...eventArray.count-1
        {
            let event = getParsedEventsData(eventArray[i])
            events.append(event)
        }
        
        return events
    }
    
    class func getParsedEventsData(_ eventJSON:JSON)->Event{
        let event:Event = Event()
        event.id = eventJSON["id"].intValue
        event.userId = eventJSON["creator_id"].intValue
        event.title = eventJSON["title"].stringValue
        event.desc = eventJSON["description"].stringValue
        event.categoryId = eventJSON["category_id"].stringValue
        event.category = eventJSON["category_name"].stringValue
        event.fromDate = Utilities().convertDateToAnotherFormat(eventJSON["start_date"].stringValue, fromFormat: dateFormatServer, toFormat: dateFormatLocal)
        event.toDate = Utilities().convertDateToAnotherFormat(eventJSON["end_date"].stringValue, fromFormat: dateFormatServer, toFormat: dateFormatLocal)
        event.fromTime = Utilities().convertDateToAnotherFormat(eventJSON["start_time"].stringValue, fromFormat: timeFormatServer, toFormat: timeFormatLocal)
        event.toTime = Utilities().convertDateToAnotherFormat(eventJSON["end_time"].stringValue, fromFormat: timeFormatServer, toFormat: timeFormatLocal)
        event.totalTickets = eventJSON["total_tickets"].stringValue
        event.ticketCost = eventJSON["ticket_cost"].stringValue
        event.expirationDate = Utilities().convertDateToAnotherFormat(eventJSON["expiration_date"].stringValue, fromFormat: dateFormatServer, toFormat: dateFormatLocal)
        event.location = eventJSON["location"].stringValue
        if eventJSON["media_file"].stringValue != "" {
            event.mediaUrl = URL(string: eventJSON["media_file"].stringValue)
        }
        if eventJSON["media_attachment_url"].stringValue != "" {
            event.mediaAttachmentUrl = URL(string: eventJSON["media_attachment_url"].stringValue)
        }
        event.mediaType = eventJSON["media_type"].intValue
        event.paidStatus = eventJSON["paid_status"].intValue
        event.isSurveyAttached = eventJSON["survey_add"].intValue
        event.isEventAccepted = eventJSON["accept_status"].intValue
        
        event.isMediaLocked = eventJSON["is_locked"].intValue
        event.eventType = eventJSON["event_type_id"].intValue
        
        //MARK: - TODO
        let base64UserId = Utilities().convertStringToBase64(Utilities.getUserDetails().id)
        let base64EventId = Utilities().convertStringToBase64(String(event.id))
        event.surveyLink = BASE_URL + PARAM_EVENT_SURVEY + "user_id=" + base64UserId + "&event_id=" + base64EventId//"http://coopeeps.trantorinc.com/coopeeps/survey.php?user_id=&event_id="
        printCustom("event.surveyLink:\(event.surveyLink)")
        return event
    }
    
    //MARK: - Parse Cards Data
    class func getParsedSavedCardsArrayFromData(_ cardArray:JSON)->[Card]{
        var cards = [Card]()
        for  i in 0...cardArray.count-1 {
            let card = getParsedSavedCardsData(cardArray[i])
            cards.append(card)
        }
        
        return cards
    }

    class func getParsedSavedCardsData(_ cardJSON:JSON)->Card{
        let card:Card = Card()
        card.id = cardJSON["id"].intValue
        card.token = cardJSON["token"].stringValue
        card.last4 = cardJSON["last4"].stringValue
        card.maskedNumber = cardJSON["maskedNumber"].stringValue
        card.expirationYear = cardJSON["expirationYear"].stringValue
        card.expirationMonth = cardJSON["expirationMonth"].stringValue
        card.type = cardJSON["cardType"].stringValue
        card.imageUrl = cardJSON["imageUrl"].stringValue
        return card
    }
    
    //MARK: - Parse Notification Array Data
    class func getParsedNotificationArrayFromData(_ feedsArray:JSON)->[Notification]{
        
        var notificationArray = [Notification]()
        
        for  i in 0...feedsArray.count-1
        {
            let notification = getParsedNotificationFromData(feedsArray[i])
            notificationArray.append(notification)
        }
        
        return notificationArray
        
    }
    
    //MARK: - Parse Notification Data
    class func getParsedNotificationFromData(_ feed:JSON)->Notification{
        
        let notification = Notification()
        
        notification.pId = feed["id"].intValue
        notification.pIsRead = feed["is_read"].intValue
        notification.pLikeStatus = feed["like_status"].intValue
        notification.pMediaId = feed["media_id"].intValue
        notification.pCountNotification = feed["count_notification"].intValue
        notification.pCategoryTypeId = feed["category_type_id"].intValue
        
        //Sent To user detail
        
        let user = People()
        
        user.pId = feed["sent_to_userdetail"]["id"].intValue
        if feed["sent_to_userdetail"]["user_image"] != nil
        {
            user.pUserImage = feed["sent_to_userdetail"]["user_image"].stringValue
        }
        if feed["sent_to_userdetail"]["username"] != nil
        {
            user.pUserName = feed["sent_to_userdetail"]["username"].stringValue
        }
        
        notification.pSentTouserDetail = user
       
        
        //Sent from user list
        
        var users = [People]()
        
        if feed["sent_from_user_list"]["username"] != nil
        {
            let user1 = People()
            
            user1.pId = feed["sent_from_user_list"]["id"].intValue
            
            if feed["sent_from_user_list"]["user_image"] != nil
            {
                user1.pUserImage = feed["sent_from_user_list"]["user_image"].stringValue
            }
            if feed["sent_from_user_list"]["username"] != nil
            {
                user1.pUserName = feed["sent_from_user_list"]["username"].stringValue
            }
            
            users.append(user1)
        }
        else
        {
            for  i in 0...feed["sent_from_user_list"].count-1
            {
                let user1 = People()
                
                user1.pId = feed["sent_from_user_list"][i]["id"].intValue
                
                if feed["sent_from_user_list"][i]["user_image"] != nil
                {
                    user1.pUserImage = feed["sent_from_user_list"][i]["user_image"].stringValue
                }
                if feed["sent_from_user_list"][i]["username"] != nil
                {
                    user1.pUserName = feed["sent_from_user_list"][i]["username"].stringValue
                }
                
                users.append(user1)
            }
        }
        
        notification.pSentFromUserDetail = users
        
        
        //created_date
        if feed["created_date"] != nil
        {
            notification.pCreatedDate = feed["created_date"].stringValue
        }
        
        //modified_date
        if feed["modified_date"] != nil
        {
            notification.pModifiedDate = feed["modified_date"].stringValue
        }
        
        
        //Media Detail
        
        let media = Media()
        
        
        //Media Type
        if feed["media_details"]["media_type"] != nil
        {
            media.pMediaType = feed["media_details"]["media_type"].intValue
        }
        
        //Media File
        if feed["media_details"]["media_file"] != nil
        {
            media.pMediaFile = feed["media_details"]["media_file"].stringValue
        }
        
        //Media Caption
        if feed["media_details"]["media_caption"] != nil
        {
            media.pMediaCaption = feed["media_details"]["media_caption"].stringValue
        }
        
        //Media Attachment
        if feed["media_details"]["media_attachment"] != nil
        {
            media.pMediaAttachment = feed["media_details"]["media_attachment"].stringValue
        }
        
        //Media Attachment Type
        if feed["media_details"]["media_attachment_type"] != nil
        {
            media.pMediaAttachmentType = feed["media_details"]["media_attachment_type"].intValue
        }
        
        //created_date
        if feed["media_details"]["created_date"] != nil
        {
            media.pCreatedDate = feed["media_details"]["created_date"].stringValue
        }
        
        //expiry_date
        if feed["media_details"]["expiry_date"] != nil
        {
            media.pExpiryDate = feed["media_details"]["expiry_date"].stringValue
        }
        //user_id
        if feed["media_details"]["user_id"] != nil
        {
            media.pUserId = feed["media_details"]["user_id"].intValue
        }
        //user_name
        if feed["media_details"]["username"] != nil
        {
            media.pUserName = feed["media_details"]["username"].stringValue
        }
        
        //user_image
        if feed["media_details"]["user_image"] != nil
        {
            media.pUserImage = feed["media_details"]["user_image"].stringValue
        }
        //sent_user_id
        if feed["media_details"]["sent_to"] != nil
        {
            media.pSentUserId = feed["media_details"]["sent_to"].intValue
        }
        
        //sent_user_name
        if feed["media_details"]["sent_to_username"] != nil
        {
            media.pSentUserName = feed["media_details"]["sent_to_username"].stringValue
        }
        
        //sent_user_image
        if feed["media_details"]["sent_to_user_image"] != nil
        {
            media.pSentUserImage = feed["media_details"]["sent_to_user_image"].stringValue
        }
        //category_id
        //media.pCategoryId = feed["media_details"]["category_id"].intValue
        
        //id
        media.pId = feed["media_details"]["id"].intValue
        
        //count_likes
        media.pCountLikes = feed["media_details"]["count_likes"].intValue
        
        //count_dislikes
        media.pCountDislikes = feed["media_details"]["count_dislikes"].intValue
        
        //count_comments
        media.pCountComments = feed["media_details"]["count_comments"].intValue
        
        //is_liked
        media.pIsLiked = feed["media_details"]["is_liked"].intValue
        
        //is_disliked
        media.pIsDisliked = feed["media_details"]["is_disliked"].intValue
        
        //slug
        if feed["media_details"]["slug"] != nil
        {
            media.pSlug = feed["media_details"]["slug"].stringValue
        }
        
        //peep text
        if feed["media_details"]["peep_text"] != nil
        {
            media.pPeepText = feed["media_details"]["peep_text"].stringValue
        }
        
        //is locked
        media.pIsLocked = feed["media_details"]["is_locked"].intValue
        
        notification.pMediaDetail = media
        
        notification.pEventDetail = self.getParsedEventsData(feed["event_details"])
        
        return notification
    }

}
