//
//  UserDetail.swift
//  C00Peeps
//
//  Created by Arshdeep on 27/09/16.
//  Copyright © 2016 SOTSYS011. All rights reserved.
//

import UIKit

class UserDetail: NSObject {

    var city = ""
    var email_id = ""
    var eot_purchased = ""
    var id = ""
    var name = ""
    var package_id = ""
    var password = ""
    var peepcode = ""
    var phone = ""
    var privacy_level_id = ""
    var profile_type_id = ""
    var public_key = ""
    var social_account_type = ""
    var social_unique_id = ""
    var state = ""
    var theme_id = ""
    var title = ""
    var user_image = ""
    var username = ""
    var followersCount = ""
    var followingsCount = ""
    var postCount = ""
    var tagline = ""
    var website = ""
    var networkAffiliation = ""
    var recordLabel = ""
    var gamingCompanyAffiliation = ""
    var filmMediaTypeId = ""
    var filmFormatId = ""
    var filmGenreId = ""
    var musicGenreId = ""
    var gameSpecialityId = ""
    var gameGenreId = ""
    var submerchantExists = ""

    override init() {
        
    }
    required init?(coder aDecoder: NSCoder) {
        self.city = Utilities().optionalDecodedString(aDecoder, forKey: "city")
        self.email_id = Utilities().optionalDecodedString(aDecoder, forKey: "email_id")
        self.eot_purchased = Utilities().optionalDecodedString(aDecoder, forKey: "eot_purchased")
        self.id = Utilities().optionalDecodedString(aDecoder, forKey: "id")
        self.name = Utilities().optionalDecodedString(aDecoder, forKey: "name")
        self.package_id = Utilities().optionalDecodedString(aDecoder, forKey: "package_id")
        self.password = Utilities().optionalDecodedString(aDecoder, forKey: "password")
        self.peepcode = Utilities().optionalDecodedString(aDecoder, forKey: "peepcode")
        self.phone = Utilities().optionalDecodedString(aDecoder, forKey: "phone")
        self.privacy_level_id = Utilities().optionalDecodedString(aDecoder, forKey: "privacy_level_id")
        self.profile_type_id = Utilities().optionalDecodedString(aDecoder, forKey: "profile_type_id")
        self.public_key = Utilities().optionalDecodedString(aDecoder, forKey: "public_key")
        self.social_account_type = Utilities().optionalDecodedString(aDecoder, forKey: "social_account_type")
        self.social_unique_id = Utilities().optionalDecodedString(aDecoder, forKey: "social_unique_id")
        self.state = Utilities().optionalDecodedString(aDecoder, forKey: "state")
        self.theme_id = Utilities().optionalDecodedString(aDecoder, forKey: "theme_id")
        self.title = Utilities().optionalDecodedString(aDecoder, forKey: "title")
        self.user_image = Utilities().optionalDecodedString(aDecoder, forKey: "user_image")
        self.username = Utilities().optionalDecodedString(aDecoder, forKey: "username")
        self.tagline = Utilities().optionalDecodedString(aDecoder, forKey: "tagline")
        self.website = Utilities().optionalDecodedString(aDecoder, forKey: "website")
        self.networkAffiliation = Utilities().optionalDecodedString(aDecoder, forKey: "network_affiliation")
        self.recordLabel = Utilities().optionalDecodedString(aDecoder, forKey: "record_label")
        self.gamingCompanyAffiliation = Utilities().optionalDecodedString(aDecoder, forKey: "gaming_company_affiliation")
        self.filmMediaTypeId = Utilities().optionalDecodedString(aDecoder, forKey: "film_media_type_id")
        self.filmFormatId = Utilities().optionalDecodedString(aDecoder, forKey: "film_format_id")
        self.filmGenreId = Utilities().optionalDecodedString(aDecoder, forKey: "film_genre_id")
        self.musicGenreId = Utilities().optionalDecodedString(aDecoder, forKey: "music_genre_id")
        self.gameSpecialityId = Utilities().optionalDecodedString(aDecoder, forKey: "game_speciality_id")
        self.gameGenreId = Utilities().optionalDecodedString(aDecoder, forKey: "game_genre_id")
        self.submerchantExists = Utilities().optionalDecodedString(aDecoder, forKey: "submerchant_exists")
    }
    
    func encodeWithCoder(_ aCoder: NSCoder) {
        aCoder.encode(self.city, forKey: "city")
        aCoder.encode(self.email_id, forKey: "email_id")
        aCoder.encode(self.eot_purchased, forKey: "eot_purchased")
        aCoder.encode(self.id, forKey: "id")
        aCoder.encode(self.name, forKey: "name")
        aCoder.encode(self.package_id, forKey: "package_id")
        aCoder.encode(self.password, forKey: "password")
        aCoder.encode(self.peepcode, forKey: "peepcode")
        aCoder.encode(self.phone, forKey: "phone")
        aCoder.encode(self.privacy_level_id, forKey: "privacy_level_id")
        aCoder.encode(self.profile_type_id, forKey: "profile_type_id")
        aCoder.encode(self.public_key, forKey: "public_key")
        aCoder.encode(self.social_account_type, forKey: "social_account_type")
        aCoder.encode(self.social_unique_id, forKey: "social_unique_id")
        aCoder.encode(self.state, forKey: "state")
        aCoder.encode(self.theme_id, forKey: "theme_id")
        aCoder.encode(self.title, forKey: "title")
        aCoder.encode(self.user_image, forKey: "user_image")
        aCoder.encode(self.username, forKey: "username")
        aCoder.encode(self.tagline, forKey: "tagline")
        aCoder.encode(self.website, forKey: "website")
        aCoder.encode(self.networkAffiliation, forKey: "network_affiliation")
        aCoder.encode(self.recordLabel, forKey: "record_label")
        aCoder.encode(self.gamingCompanyAffiliation, forKey: "gaming_company_affiliation")
        aCoder.encode(self.filmMediaTypeId, forKey: "film_media_type_id")
        aCoder.encode(self.filmFormatId, forKey: "film_format_id")
        aCoder.encode(self.filmGenreId, forKey: "film_genre_id")
        aCoder.encode(self.musicGenreId, forKey: "music_genre_id")
        aCoder.encode(self.gameSpecialityId, forKey: "game_speciality_id")
        aCoder.encode(self.gameGenreId, forKey: "game_genre_id")
        aCoder.encode(self.submerchantExists, forKey: "submerchant_exists")
    }
    
   }
