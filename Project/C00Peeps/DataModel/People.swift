//
//  People.swift
//  C00Peeps
//
//  Created by Mac on 06/10/16.
//  Copyright © 2016 SOTSYS011. All rights reserved.
//

import Foundation

class People : NSObject{
    // MARK: Properties
    
    var pId = 0
    var pPost = 10
    var pUserName = ""
    var pUserImage = ""
    var pFriendStatus = ""
    
}
