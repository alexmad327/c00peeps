//
//  Submerchant.swift
//  C00Peeps
//
//  Created by Arshdeep on 14/12/16.
//  Copyright © 2016 SOTSYS011. All rights reserved.
//

import UIKit

class Submerchant: NSObject {
    var id = ""
    var sub_merchant_id = ""
    var user_id = ""
    var firstName = ""
    var lastName = ""
    var email = ""
    var dateOfBirth = ""
    var phone = ""
    var country = ""//US- 231
    var region = ""
    var locality = ""
    var streetAddress = ""
    var postalCode = ""
    var accountNumber = ""
    var routingNumber = ""
}
