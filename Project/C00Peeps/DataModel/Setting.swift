//
//  Setting.swift
//  C00Peeps
//
//  Created by Arshdeep on 19/10/16.
//  Copyright © 2016 SOTSYS011. All rights reserved.
//

import UIKit

class Setting: NSObject {
    var key = ""
    var keyHeaderImage:UIImage? = nil
    var values:[String] = []
}
