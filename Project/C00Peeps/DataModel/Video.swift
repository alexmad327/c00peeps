//
//  Video.swift
//  C00Peeps
//
//  Created by Mac on 06/10/16.
//  Copyright © 2016 SOTSYS011. All rights reserved.
//

import Foundation

class Video {
    // MARK: Properties
    
    var name: String!
    var info: String!
    var video_url: String!
}
