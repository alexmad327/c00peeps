//
//  Contact.swift
//  C00Peeps
//
//  Created by Mac on 30/09/16.
//  Copyright © 2016 SOTSYS011. All rights reserved.
//

import Foundation

class Contact:NSObject{
    var pId = 0
    var pName = ""
    var pUserName = ""
    var pPublicKey = ""
    var pSelected = false
    var pPrivacyLevelId = 3
    
    override init() {
        
    }
    required init?(coder aDecoder: NSCoder) {
        self.pId = Utilities().optionalDecodedInt(aDecoder, forKey: "pId")
        self.pName = Utilities().optionalDecodedString(aDecoder, forKey: "pName")
        self.pUserName = Utilities().optionalDecodedString(aDecoder, forKey: "pUserName")
        self.pPublicKey = Utilities().optionalDecodedString(aDecoder, forKey: "pPublicKey")
        self.pPrivacyLevelId = Utilities().optionalDecodedInt(aDecoder, forKey: "pPrivacyLevelId")
    }
    
    func encodeWithCoder(_ aCoder: NSCoder) {
        aCoder.encode(self.pId, forKey: "pId")
        aCoder.encode(self.pName, forKey: "pName")
        aCoder.encode(self.pUserName, forKey: "pUserName")
        aCoder.encode(self.pPublicKey, forKey: "pPublicKey")
        aCoder.encode(self.pPrivacyLevelId, forKey: "pPrivacyLevelId")

    }
    
}
